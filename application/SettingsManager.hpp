#ifndef _BONEZ_SETTINGSMANAGER_HPP
#define _BONEZ_SETTINGSMANAGER_HPP

#include "parsers/JsonParser.hpp"
#include "bonez/common/cameras/Camera.hpp"

#include <embree/common/sys/filename.h>
#include "bonez/common/Params.hpp"

namespace BnZ {

struct WindowSettings {
    size_t width, height;
};

struct FramebufferSettings {
    size_t width, height;
};

struct SceneSettings {
    embree::FileName path;
    std::string name;
    ParamSet params;
};

struct RendererSettings {
    std::string name;
    ParamSet params;
};

struct CameraSettings {
    std::string name;
    ParamSet params;
};

struct BatchSettings {
    std::string name;
    FramebufferSettings framebuffer;
    std::vector<SceneSettings> scenes;
    std::vector<CameraSettings> cameras;
    std::vector<RendererSettings> renderers;
    float gamma;
};

class SettingsManager {
public:
    SettingsManager(const embree::FileName& path, const std::string& config);

    WindowSettings getWindowSettings() const;

    FramebufferSettings getFramebufferSettings() const;

    float getCameraSpeed() const;

    SceneSettings getSceneSettings() const;

    SceneSettings getSceneSettings(const std::string& name) const;

    std::vector<SceneSettings> getAllSceneSettings() const;

    std::vector<RendererSettings> getAllRendererSettings() const;

    static std::vector<RendererSettings> getAllRendererSettings(const embree::FileName& path);

    CameraSettings getCameraSettings(const std::string& name) const;

    std::vector<CameraSettings> getAllCameraSettings() const;

    const ParamSet& getConfigSettings() const {
        return m_ConfigSettings;
    }

    const embree::FileName& getConfigPath() const {
        return m_ConfigPath;
    }

    std::string saveCameraSettings(const Camera& camera) const;

    BatchSettings getBatchSettings(const std::string& batchName) const;

private:
    embree::FileName m_Path;
    embree::FileName m_ConfigPath;
    JsonParser m_Parser;

    ParamSet m_GlobalSettings;
    ParamSet m_ConfigSettings;

    WindowSettings m_WindowSettings;
    FramebufferSettings m_FramebufferSettings;
    float m_fCameraSpeed;
};

}

#endif // _BONEZ_SETTINGSMANAGER_HPP
