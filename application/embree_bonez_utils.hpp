#ifndef _BONEZ_EMBREE_BONEZ_UTILS_HPP_
#define _BONEZ_EMBREE_BONEZ_UTILS_HPP_

#include "bonez/common/common.hpp"
#include "bonez/renderer/Framebuffer.hpp"

namespace BnZ {

inline void copyFramebuffer(const Framebuffer& fb, embree::Image& image) {
    for (size_t y = 0; y < fb.getHeight(); ++y) {
        for (size_t x = 0; x < fb.getWidth(); ++x) {
            image.set(x, y, fb(x, y));
        }
    }
}

inline void copyImage(const embree::Image& image, Framebuffer& fb) {
    for (size_t y = 0; y < image.height; ++y) {
        for (size_t x = 0; x < image.width; ++x) {
            fb.set(x, y, image.get(x, y));
        }
    }
}

inline void flipVertically(embree::Image& image) {
    for (size_t y = 0; y < image.height / 2; ++y) {
        for (size_t x = 0; x < image.width; ++x) {
            Col3f tmp = image.get(x, y);
            image.set(x, y, image.get(x, image.height - 1 - y));
            image.set(x, image.height - 1 - y, tmp);
        }
    }
}

}

#endif
