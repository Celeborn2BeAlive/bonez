#include "ResultManager.hpp"

#include <fstream>
#include <iomanip>

#include "bonez/common/common.hpp"
#include "bonez/renderer/postprocessing/GammaToneMapping.hpp"

#include "embree_bonez_utils.hpp"

#include <application/sys/string_utils.hpp>

namespace BnZ {

ResultManager::ResultManager(const SettingsManager& settingsManager):
    m_SettingsManager(settingsManager) {
}

void ResultManager::saveResult(
        const Framebuffer& framebuffer, float time, float gamma,
        const ParamSet& rendererParams, const std::string& cameraName,
        const ParamSet& statistics, const std::string& batchName) const {
    embree::Ref<embree::Image> image(
        new embree::Image3f(framebuffer.getWidth(), framebuffer.getHeight()));
    copyFramebuffer(framebuffer, *image);
    
    std::stringstream filenameSS;

    if(batchName != "") {
        filenameSS << batchName << "/";
    }

    filenameSS << "results/" << m_SettingsManager.getSceneSettings().name << "_";
    filenameSS << cameraName << "_";
    filenameSS << rendererParams.name << ".";
    filenameSS << std::setfill('0');
    filenameSS << std::setw(4) << std::time(nullptr) << ".result.bnz";

    embree::FileName filename = embree::FileName(filenameSS.str() + ".js");
    
    embree::FileName exrFilename = embree::FileName(filenameSS.str() + ".exr");
    
    ParamSet params;
    params.set("image", exrFilename.str());
    params.set("gamma", gamma);
    params.set("time", time);
    params.set("renderer", rendererParams);
    params.set("statistics", statistics);

    m_Parser.saveParamSet(params, m_SettingsManager.getConfigPath() + filename);
    std::clog << "ResultManager: Saved result as " << (m_SettingsManager.getConfigPath() + filename) << std::endl;

    embree::storeImage(image, m_SettingsManager.getConfigPath() + exrFilename);
    
    GammaToneMapping postProcess(gamma);
    Framebuffer fb(framebuffer.getWidth(), framebuffer.getHeight());
    
    postProcess.apply(framebuffer, fb);
    copyFramebuffer(fb, *image);
    
    embree::FileName pngFilename = embree::FileName(filenameSS.str() + ".png");
    
    embree::storeImage(image, m_SettingsManager.getConfigPath() + pngFilename);
}

}
