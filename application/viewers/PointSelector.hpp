#ifndef _BONEZ_POINTSELECTOR_HPP
#define _BONEZ_POINTSELECTOR_HPP

#include "bonez/common/common.hpp"
#include "bonez/scene/Intersection.hpp"

namespace BnZ {

class PointSelector {
    static const size_t MAX_COUNT = 8;
    std::vector<Intersection> m_Intersections;
    std::vector<bool> m_Enabled;
    std::vector<bool> m_Selected;
    size_t m_nSelectedForChange;

    bool m_bAutoChange;

    bool m_bDisplayGUI;
    bool m_bDisplayEnablingGUI;
    bool m_bDisplayChangeGUI;
    bool m_bDisplaySelectionGUI;

public:
    PointSelector();

    void setupGUI();

    void teardownGUI();

    void setPoint(const Intersection& I);

    bool isSelected(size_t idx) const {
        return isEnabled(idx) && m_Selected[idx];
    }

    bool isEnabled(size_t idx) const {
        return m_Intersections[idx] && m_Enabled[idx];
    }

    const Intersection& operator [](size_t idx) const {
        return m_Intersections[idx];
    }

    size_t size() const {
        return m_Intersections.size();
    }

    Intersection getSelected(int idx) const {
        for(const auto& point: index(m_Intersections)) {
            if(isSelected(point.first)) {
                if(!idx) {
                    return point.second;
                }
                --idx;
            }
        }
        if(idx < 0) {
            return Intersection();
        }
    }

    size_t selectedCount() const {
        size_t count = 0;
        for(size_t i: range(m_Intersections.size())) {
            if(isSelected(i)) {
                ++count;
            }
        }
        return count;
    }
};

}

#endif // _BONEZ_POINTSELECTOR_HPP
