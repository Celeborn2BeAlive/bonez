#include "ImportanceAnalysisViewer.hpp"
#include "application/gui.hpp"

#include "bonez/common/cameras/ParaboloidCamera.hpp"

namespace BnZ {

ImportanceAnalysisViewer::ImportanceAnalysisViewer(const GLSLProgramBuilder& programBuilder):
    m_Device(nullptr), m_GLScene(nullptr), m_VisualDataRenderer(programBuilder),
    m_bDisplayMaxball(false), m_PointHemisphereRenderer(256, 256) {
}

void ImportanceAnalysisViewer::init(Device* device, GLScene* glScene, const IntegratorPtr& pIntegrator) {
    m_Device = device;
    m_GLScene = glScene;

    const Scene& scene = device->getScene();

    if(scene.voxelSpace) {
        /*
        m_SkeletonVisualData = createVisualData(
                    scene.topology.skeleton().getGraph(),
                    [&scene](GraphNodeIndex node) -> Vec3f {
                        return scene.topology.skeleton()[node].getPosition();
                    },
                    [&scene](GraphNodeIndex node) -> Col3f {
                        return scene.topology.getNodeColor(node);
                    },
                    scene.topology.skeleton().getGrid().getVoxelEdgeLength()
        );

        m_SkelClusteringVisualData = createVisualData(
                    scene.topology.clustering().getGraph(),
                    [&scene](GraphNodeIndex node) -> Vec3f {
                        return scene.topology.clustering()[node].center;
                    },
                    [&scene](GraphNodeIndex node) -> Col3f {
                        return scene.topology.getClusterColor(node);
                    },
                    scene.topology.skeleton().getGrid().getVoxelEdgeLength()
        );*/

        m_CurrentDisplayedSkelDataType = SKELETON;
    } else {
        m_CurrentDisplayedSkelDataType = NONE;
    }

    m_pIntegrator = pIntegrator;
}

void ImportanceAnalysisViewer::onSetup() {

}

void ImportanceAnalysisViewer::onTeardown() {

}

void ImportanceAnalysisViewer::setWindowSize(size_t width, size_t height) {
    m_nWidth = width;
    m_nHeight = height;

    m_MainViewport = gloops::Viewport(0.5 * (m_nWidth - m_Device->getFramebuffer().getWidth()),
                                      20,
                                      m_Device->getFramebuffer().getWidth(),
                                      m_Device->getFramebuffer().getHeight());
}

void ImportanceAnalysisViewer::handleClickEvent(int x, int y) {
    if(!m_Device) {
        std::cerr << "ImportanceAnalysisViewer::handleClickEvent - Not initialized" << std::endl;
        return;
    }

    const Scene& scene = m_Device->getScene();

    if(x < m_MainViewport.x || y < (m_nHeight - m_MainViewport.y - m_MainViewport.height) ||
            x >= m_MainViewport.x + m_MainViewport.width || y >= (m_nHeight - m_MainViewport.y)) {
        return;
    }

    Vec2f rasterPosition = m_Device->getFramebuffer().getRasterPosition(x - m_MainViewport.x, y - (m_nHeight - m_MainViewport.y - m_MainViewport.height));
    Ray ray = m_Device->getCamera().ray(CameraSample(rasterPosition, Vec2f(0.5f, 0.5f)));
    m_PickedIntersection = intersect(ray, scene);

    if(!m_PickedIntersection) {
        return;
    }

    m_PointHemisphereRenderer.init(m_PickedIntersection, -ray.dir,
                                   m_pIntegrator,
                                   [this](const Vec3f& wo) {
                                        return m_PointHemisphereRenderer.m_BRDF.pdf(m_PointHemisphereRenderer.m_Wi, wo);
                                   });
}

void ImportanceAnalysisViewer::drawGUI() {
    if(!m_Device) {
        std::cerr << "VisualDataViewer::drawView - Not initialized" << std::endl;
        return;
    }

    if(m_Device->getScene().voxelSpace) {
        imguiSeparatorLine();
        imguiSeparator();

        imguiLabel("Skeleton");

        if(imguiCheck("Display Maxballs", m_bDisplayMaxball)) {
            m_bDisplayMaxball = !m_bDisplayMaxball;
        }

        if(imguiCheck("Display Skeleton", m_CurrentDisplayedSkelDataType == SKELETON)) {
            m_CurrentDisplayedSkelDataType = SKELETON;
        }

        if(imguiCheck("Display Clustering", m_CurrentDisplayedSkelDataType == CLUSTERING)) {
            m_CurrentDisplayedSkelDataType = CLUSTERING;
        }
    }
}

void ImportanceAnalysisViewer::drawView() {
    if(!m_Device || !m_GLScene) {
        std::cerr << "ImportanceAnalysisViewer::drawView - Not initialized" << std::endl;
        return;
    }

    {
        auto states = m_VisualDataRenderer.startFrame(m_MainViewport.x, m_MainViewport.y, m_MainViewport.width, m_MainViewport.height);

        m_VisualDataRenderer.setProjectionMatrix(computeGLProjectionMatrix(m_Device->getCamera()));
        m_VisualDataRenderer.setViewMatrix(computeGLViewMatrix(m_Device->getCamera()));

        const Scene& scene = m_Device->getScene();

        m_VisualDataRenderer.renderScene(*m_GLScene, scene);

        /*
        Col3f color(1, 0, 0);
        switch(m_CurrentDisplayedSkelDataType) {
        case SKELETON: {
                //m_VisualDataRenderer.render(m_SkeletonVisualData);

                if(m_PickedIntersection) {
                    GraphNodeIndex node = scene.voxelSpace->getSkeleton().getNearestNode(m_PickedIntersection);
                    if(node != UNDEFINED_NODE) {
                        color = scene.voxelSpace->getSkeletonNodeColor(node);
                        m_VisualDataRenderer.renderLine(m_PickedIntersection.P, scene.voxelSpace->getSkeleton()[node].getPosition(),
                                        color, color, 3.f);

                        if(m_bDisplayMaxball) {
                            m_VisualDataRenderer.setDisplayMode(GLVisualDataRenderer::LINE);
                            m_VisualDataRenderer.renderSphere(scene.voxelSpace->getSkeleton()[node].getPosition(),
                                                              embree::sqrt(scene.voxelSpace->getSkeleton()[node].getMaxball()), color);
                            m_VisualDataRenderer.setDisplayMode(GLVisualDataRenderer::FILL);

                            m_VisualDataRenderer.popModelMatrix();
                        }
                    }
               }
            }
            break;
        case CLUSTERING: {
                //m_VisualDataRenderer.render(m_SkelClusteringVisualData);

                if(m_PickedIntersection) {
                    GraphNodeIndex cluster = scene.topology.clustering().getNearestNode(m_PickedIntersection);
                    if(cluster) {
                        color = scene.topology.getClusteringNodeColor(cluster);
                        m_VisualDataRenderer.renderLine(m_PickedIntersection.P, scene.topology.clustering()[cluster].center,
                                            color, color, 3.f);

                        if(m_bDisplayMaxball) {
                            m_VisualDataRenderer.setDisplayMode(GLVisualDataRenderer::LINE);
                            m_VisualDataRenderer.renderSphere(scene.topology.clustering()[cluster].center,
                                                              embree::sqrt(scene.topology.clustering()[cluster].maxball), color);
                            m_VisualDataRenderer.setDisplayMode(GLVisualDataRenderer::FILL);
                        }
                    }
               }
            }
            break;
        default:
            break;
        }*/

        float scaleFactor = embree::length(m_Device->getScene().geometry.boundingBox.size()) * 0.006;

        /*
        if(m_PickedIntersection) {
            m_VisualDataRenderer.renderArrow(m_PickedIntersection.P, m_PickedIntersection.Ns, color, scaleFactor);
        }*/
    }

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;

    if(m_PickedIntersection) {
        m_PointHemisphereRenderer.render(m_Device->getScene());


        gloops::Viewport viewport(0.5 * (m_nWidth - m_Device->getFramebuffer().getWidth()),
                                  40 + m_Device->getFramebuffer().getHeight(),
                                  m_PointHemisphereRenderer.m_nXResolution,
                                  m_PointHemisphereRenderer.m_nYResolution);
        m_ResultRenderer.drawResult(viewport, 1.f, normalize(m_PointHemisphereRenderer.m_FunctionFramebuffer));

        viewport = gloops::Viewport(0.5 * (m_nWidth - m_Device->getFramebuffer().getWidth()) + 20 + m_PointHemisphereRenderer.m_nXResolution,
                                          40 + m_Device->getFramebuffer().getHeight(),
                                          m_PointHemisphereRenderer.m_nXResolution,
                                          m_PointHemisphereRenderer.m_nYResolution);

        m_ResultRenderer.drawResult(viewport, 1.f, normalize(m_PointHemisphereRenderer.m_PdfFramebuffer));


        viewport = gloops::Viewport(0.5 * (m_nWidth - m_Device->getFramebuffer().getWidth())
                                    + 2 * (20 + m_PointHemisphereRenderer.m_nXResolution),
                                  40 + m_Device->getFramebuffer().getHeight(),
                                  m_PointHemisphereRenderer.m_nXResolution,
                                  m_PointHemisphereRenderer.m_nYResolution);

        m_ResultRenderer.drawResult(viewport, 1.f, normalize(m_PointHemisphereRenderer.m_DivisionFramebuffer));
    }
}


}
