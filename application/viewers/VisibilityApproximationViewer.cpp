#include "VisibilityApproximationViewer.hpp"
#include "application/gui.hpp"

#include "bonez/common/Progress.hpp"

namespace BnZ {

#if 0
VisibilityApproximationViewer::VisibilityApproximationViewer():
    m_bIsActive(false),
    m_pDevice(nullptr),
    m_pRenderer(nullptr),
    m_pSkeletonViewer(nullptr),
    m_pClustering(nullptr),
    m_bHasSurfacePoint(false),
    m_bHasOther(false),
    m_bSelectFirstPoint(true) {
}

void VisibilityApproximationViewer::init(Device& device, SkeletonViewer& skeletonViewer,
                                         GLVisualDataRenderer& renderer) {
    m_pDevice = &device;
    m_pSkeletonViewer = &skeletonViewer;
    m_pRenderer = &renderer;
    if(device.getScene().topology) {
        m_pClustering = &device.getScene().topology.clustering();
    }
}

void VisibilityApproximationViewer::onSetup() {

}

void VisibilityApproximationViewer::setWindowSize(size_t width, size_t height) {

}

void VisibilityApproximationViewer::testGeodesicVsEuclideanDistance() {
    if(m_pDevice->getScene().topology) {
        CurvSkelClustering& clustering = m_pDevice->getScene().topology.clustering();

        Progress progress;

        progress.init("All shortest paths computation", clustering.size());

        m_ShortestPaths.resize(clustering.size());

        for(auto i: range(clustering.size())) {
            DijkstraAlgorithm dijkstra;
            auto distance = [clustering](GraphNodeIndex i1, GraphNodeIndex i2) {
                return embree::length(clustering.getNode(i1).P - clustering.getNode(i2).P);
            };
            m_ShortestPaths[i] = dijkstra.computeShortestPaths(clustering.getGraph(), i, distance);

            // check direct visibility between nodes to improve the distances
            Skeleton::Node ni = clustering.getNode(i);
            for(auto j: range(clustering.size())) {
                Skeleton::Node nj = clustering.getNode(j);
                Vec3f w = nj.P - ni.P;
                float l = embree::length(w);
                if(clustering.checkVisibility(i, j)) {
                    m_ShortestPaths[i][j].distance = l;
                }
            }
            //progress.next();
        }

        progress.end();

        // Samples random points
        Random rng(0);

        std::vector<float> ratiosForVisibles;
        std::vector<float> ratiosForNonVisibles;

        size_t nbSuccess = 0;
        size_t nbTry = 0;

        size_t count = 16384;
        while(count--) {
            SurfacePointSample sample1 =
                    m_pDevice->getScene().geometry.sampleSurfacePoint(
                        SurfacePointRandom(rng.getFloat(), rng.getFloat(), rng.getFloat(), Vec2f(rng.getFloat())));

            GraphNodeIndex i1 = clustering.getNearestNode(sample1.value);

            if(i1 == UNDEFINED_NODE) {
                continue;
            }

            {
                SurfacePointSample sample2 =
                        m_pDevice->getScene().geometry.sampleSurfacePoint(
                            SurfacePointRandom(rng.getFloat(), rng.getFloat(), rng.getFloat(), Vec2f(rng.getFloat())));

                GraphNodeIndex i2 = clustering.getNearestNode(sample2.value);
                if(i2 != UNDEFINED_NODE && i2 != i1) {
                    if(i1 == i2) {
                        continue;
                    }

                    Vec3f wo = sample2.value.P - sample1.value.P;

                    float euclideanDist = embree::length(wo);
                    if(euclideanDist >= 1 && m_ShortestPaths[i1][i2].distance != float(embree::inf)) {
                        Skeleton::Node n1 = clustering.getNode(i1);
                        Skeleton::Node n2 = clustering.getNode(i2);

                        float geoDistApprox =
                                embree::length(n1.P - sample1.value.P) +
                                m_ShortestPaths[i1][i2].distance +
                                embree::length(n2.P - sample2.value.P);

                        float ratio = geoDistApprox / euclideanDist;

                        RayCounter c;
                        if(embree::dot(sample1.value.Ns, wo) < 0
                                || embree::dot(sample2.value.Ns, -wo) < 0
                                || occluded(shadow_ray(sample1.value.P, sample2.value.P), m_pDevice->getScene(), c)) {
                            // Not visible
                            ratiosForNonVisibles.push_back(ratio);

                            if(!clustering.checkVisibility(i1, i2)) {
                                ++nbSuccess;
                            }
                            ++nbTry;
                        } else {
                            // Visible
                            ratiosForVisibles.push_back(ratio);

                            if(clustering.checkVisibility(i1, i2)) {
                                ++nbSuccess;
                            }
                            ++nbTry;
                        }
                    }
                }
            }

            {
                Sample3f wo = embree::uniformSampleHemisphere(rng.getFloat(), rng.getFloat(), sample1.value.Ns);
                Intersection I = intersect(secondary_ray(sample1.value.P, wo), m_pDevice->getScene());
                if(I) {
                    GraphNodeIndex i2 = clustering.getNearestNode(I);
                    if(i2 == UNDEFINED_NODE) {
                        continue;
                    }

                    if(i1 == i2) {
                        continue;
                    }

                    float euclideanDist = embree::length(I.P - sample1.value.P);

                    if(euclideanDist < 1 || m_ShortestPaths[i1][i2].distance == float(embree::inf)) {
                        continue;
                    }

                    Skeleton::Node n1 = clustering.getNode(i1);
                    Skeleton::Node n2 = clustering.getNode(i2);

                    float geoDistApprox =
                            embree::length(n1.P - sample1.value.P) +
                            m_ShortestPaths[i1][i2].distance +
                            embree::length(n2.P - I.P);


                    if(clustering.checkVisibility(i1, i2)) {
                        ++nbSuccess;
                    }
                    ++nbTry;

                    ratiosForVisibles.push_back(geoDistApprox / euclideanDist);
                    if(ratiosForVisibles.back() > 2000) {
                        std::cerr << ratiosForVisibles.back() << std::endl;
                        std::cerr << geoDistApprox << std::endl;
                        std::cerr << euclideanDist << std::endl;
                    }
                }
            }
        }

        std::ofstream out1("ratiosForVisibles.dat");
        for(auto v: ratiosForVisibles) {
            out1 << v << std::endl;
        }
        std::cout << "nb visibles = " << ratiosForVisibles.size() << std::endl;

        std::ofstream out2("ratiosForNonVisibles.dat");
        for(auto v: ratiosForNonVisibles) {
            out2 << v << std::endl;
        }

        std::cout << "nb not visibles = " << ratiosForNonVisibles.size() << std::endl;

        std::clog << "Diag scene = " <<
                     embree::length(embree::size(m_pDevice->getScene().geometry.boundingBox)) << std::endl;

        std::cerr << "nb try = " << nbTry << std::endl;
        std::cerr << "nb success = " << nbSuccess << std::endl;
        std::cerr << "ratio = " << (float)nbSuccess / nbTry << std::endl;
    }
}

void VisibilityApproximationViewer::sampleRandomVisible() {
    if(m_bHasSurfacePoint
            && m_pSkeletonViewer->isClusteringCurrentlyDisplayed()
            && !m_ShortestPaths.empty()) {
        m_bHasOther = false;

        Sample3f wo = embree::uniformSampleHemisphere(m_Rng.getFloat(), m_Rng.getFloat(), m_SurfacePoint.Ns);
        Intersection I = intersect(secondary_ray(m_SurfacePoint.P, wo), m_pDevice->getScene());

        if(I) {
            m_bHasOther = true;
            m_Other = I;

            GraphNodeIndex i1 = m_pClustering->getNearestNode(m_SurfacePoint);
            if(i1 == UNDEFINED_NODE) {
                std::cerr << "UNDEFINED NODE FOR THE PICKED POINT" << std::endl;
                return;
            }

            GraphNodeIndex i2 = m_pClustering->getNearestNode(m_Other);
            if(i2 == UNDEFINED_NODE) {
                std::cerr << "UNDEFINED NODE FOR THE RANDOM POINT" << std::endl;
                return;
            }

            if(i1 == i2) {
                std::cerr << "SAME NODE" << std::endl;
                return;
            }

            printDistances();
        }

    }
}

void VisibilityApproximationViewer::printDistances() {
    if(!m_pClustering || !m_bHasSurfacePoint || !m_bHasOther) {
        return;
    }

    GraphNodeIndex i1 = m_pClustering->getNearestNode(m_SurfacePoint);
    GraphNodeIndex i2 = m_pClustering->getNearestNode(m_Other);

    if(i1 == UNDEFINED_NODE || i2 == UNDEFINED_NODE) {
        std::cerr << "VisibilityApproximationViewer::printDistances - " <<
                     "A node is undefined." << std::endl;
        return;
    }

    float euclideanDist = embree::length(m_Other.P - m_SurfacePoint.P);

    Skeleton::Node n1 = m_pClustering->getNode(i1);
    Skeleton::Node n2 = m_pClustering->getNode(i2);

    float geoDistApprox =
            embree::length(n1.P - m_SurfacePoint.P) +
            m_ShortestPaths[i1][i2].distance +
            embree::length(n2.P - m_Other.P);

    std::cerr << "Geodesic distance is : " << geoDistApprox << std::endl;
    std::cerr << "Euclidean distance is : " << euclideanDist << std::endl;
    std::cerr << "Ratio is : " << (geoDistApprox / euclideanDist) << std::endl;
}

void VisibilityApproximationViewer::sampleRandom() {
    if(!m_pClustering
            || !m_bHasSurfacePoint
            || !m_bHasOther
            || !m_pSkeletonViewer->isClusteringCurrentlyDisplayed()
            || m_ShortestPaths.empty()) {
        return;
    }

    m_bHasOther = true;

    m_Other = m_pDevice->getScene().geometry.sampleSurfacePoint(
                SurfacePointRandom(m_Rng.getFloat(), m_Rng.getFloat(), m_Rng.getFloat(), Vec2f(m_Rng.getFloat())));

    GraphNodeIndex i1 = m_pClustering->getNearestNode(m_SurfacePoint);
    if(i1 == UNDEFINED_NODE) {
        std::cerr << "UNDEFINED NODE FOR THE PICKED POINT" << std::endl;
        return;
    }

    GraphNodeIndex i2 = m_pClustering->getNearestNode(m_Other);
    if(i2 == UNDEFINED_NODE) {
        std::cerr << "UNDEFINED NODE FOR THE RANDOM POINT" << std::endl;
        return;
    }

    if(i1 == i2) {
        std::cerr << "SAME NODE" << std::endl;
        return;
    }

    printDistances();
}

void VisibilityApproximationViewer::exentricityDot() {
    Vec3f shadowRay = m_Other.P - m_SurfacePoint.P;
    float dist = embree::length(shadowRay);
    shadowRay /= dist;

    GraphNodeIndex nodeI = m_pClustering->getNearestNode(m_SurfacePoint);
    if(nodeI == UNDEFINED_NODE) {
        return;
    }

    Skeleton::Node node = m_pClustering->getNode(nodeI);
    Vec3f wn = node.P - m_SurfacePoint.P;
    float d = embree::distance(node.P, m_SurfacePoint.P);
    float d2 = d;
    float m2 = node.maxball;
    wn /= d;
    Skeleton::Node bestNode = node;
    float maxdot = embree::dot(wn, shadowRay);
    for(auto neighbour: m_pClustering->neighbours(nodeI)) {
        Skeleton::Node node = m_pClustering->getNode(neighbour);
        Vec3f wn = node.P - m_SurfacePoint.P;
        float d = embree::distance(node.P, m_SurfacePoint.P);
        wn /= d;
        float dot = embree::dot(wn, shadowRay);
        if(dot > maxdot) {
            bestNode = node;
            maxdot = dot;
        }

        float e = node.maxball / d;
        m_pRenderer->renderLine(node.P, m_SurfacePoint.P,
                                sample(HEAT_MAP, e),
                                sample(HEAT_MAP, e),
                                3.f);
    }

    d = embree::distance(bestNode.P, m_SurfacePoint.P);
    float e = bestNode.maxball / d; // exentricity

    /*
    m_pRenderer->renderLine(bestNode.P, m_SurfacePoint.P,
                            Col3f(1, 0, 0),
                            Col3f(0, 1, 0),
                            3.f);
*/
    /*
    m_pRenderer->renderLine(m_Other.P, m_SurfacePoint.P,
                            m_pDevice->getSurfacePointNodeColor(m_Other),
                            m_pDevice->getSurfacePointNodeColor(m_SurfacePoint),
                            3.f);*/

    /*
    RayCounter r;
    if(!occluded(shadow_ray(m_SurfacePoint, shadowRay, dist), m_pDevice->getScene(), r)) {
        m_pRenderer->renderLine(m_Other.P, m_SurfacePoint.P,
                                Col3f(0.f),
                                Col3f(0.f),
                                7.f);
    }
*/
    /*
    std::cerr << e * maxdot << ", " << d << ", " << e << ", " << maxdot << std::endl;
    std::cerr << m2 << ", " << d2 << std::endl;*/
}

void VisibilityApproximationViewer::analyseExcentricity() {
    GraphNodeIndex nodeI = m_pClustering->getNearestNode(m_SurfacePoint);
    if(nodeI == UNDEFINED_NODE) {
        return;
    }

    Skeleton::Node node = m_pClustering->getNode(nodeI);
    Vec3f wn = node.P - m_SurfacePoint.P;
    float d = embree::distance(node.P, m_SurfacePoint.P);
    wn /= d;
    Skeleton::Node bestNode = node;

    float e = node.maxball / d;

    m_pRenderer->renderLine(node.P, m_SurfacePoint.P,
                            sample(HEAT_MAP, e),
                            sample(HEAT_MAP, e),
                            3.f);
    m_pRenderer->setDisplayMode(GLVisualDataRenderer::LINE);
    m_pRenderer->renderSphere(node.P, node.maxball, sample(HEAT_MAP, e));
    m_pRenderer->setDisplayMode(GLVisualDataRenderer::FILL);

    for(auto neighbour: m_pClustering->neighbours(nodeI)) {
        Skeleton::Node node = m_pClustering->getNode(neighbour);
        Vec3f wn = node.P - m_SurfacePoint.P;
        float d = embree::distance(node.P, m_SurfacePoint.P);
        wn /= d;

        float e = node.maxball / d;
        m_pRenderer->renderLine(node.P, m_SurfacePoint.P,
                                sample(HEAT_MAP, e),
                                sample(HEAT_MAP, e),
                                3.f);
        m_pRenderer->setDisplayMode(GLVisualDataRenderer::LINE);
        m_pRenderer->renderSphere(node.P, node.maxball, sample(HEAT_MAP, e));
        m_pRenderer->setDisplayMode(GLVisualDataRenderer::FILL);
    }
}

void VisibilityApproximationViewer::handleClickEvent(int x, int y) {

}

void VisibilityApproximationViewer::setSurfacePoint(const SurfacePoint& point) {
    if(m_bSelectFirstPoint) {
        m_bHasSurfacePoint = true;
        m_SurfacePoint = point;
    } else {
        m_bHasOther = true;
        m_Other = point;
    }

    //sampleRandomVisible();
}

void VisibilityApproximationViewer::displayVisibilityApproximationColors() {
    if(m_bHasSurfacePoint && m_pSkeletonViewer->isClusteringCurrentlyDisplayed()) {
        GraphNodeIndex node = m_pClustering->getNearestNode(m_SurfacePoint);
        if(node != UNDEFINED_NODE) {
            NodeColors colors(m_pClustering->size());
            for(auto i: range(m_pClustering->size())) {
                colors[i] = sample(HEAT_MAP, m_pClustering->getVisibilityFactor(node, i));
            }
            m_pSkeletonViewer->setNodeColors(colors);
        }
    }
}

void VisibilityApproximationViewer::drawGUI() {
    imguiLabel("Visibility approximation");

    if(!m_pSkeletonViewer->isClusteringCurrentlyDisplayed()) {
        return;
    }

    if(imguiCollapse("", "", m_bIsActive)) {
        m_bIsActive = !m_bIsActive;
    }

    if(m_bIsActive) {
        if(imguiButton("Test euclidean vs geodesic")) {
            testGeodesicVsEuclideanDistance();
        }

        if(imguiButton("Sample random visible")) {
            sampleRandomVisible();
        }

        if(imguiButton("Sample random")) {
            sampleRandom();
        }

        if(imguiButton("Set visibility probability colors")) {
            displayVisibilityApproximationColors();
        }

        if(m_bSelectFirstPoint) {
            if(imguiButton("Select second point")) {
                m_bSelectFirstPoint = false;
            }
        } else {
            if(imguiButton("Select first point")) {
                m_bSelectFirstPoint = true;
            }
        }
   }
}

void VisibilityApproximationViewer::drawView() {
    if(!m_bIsActive
            || !m_bHasSurfacePoint
            || !m_pClustering
            || !m_pSkeletonViewer->isClusteringCurrentlyDisplayed()) {
        return;
    }

    analyseExcentricity();

    if(!m_bHasOther) {
        return;
    }

    /*
    GraphNodeIndex i;
    if(UNDEFINED_NODE != (i = m_pClustering->getNearestNode(m_Other))) {
        Col3f color = m_pDevice->getScene().topology.getClusterColor(i);

        Skeleton::Node node = m_pClustering->getNode(i);

        m_pRenderer->renderLine(m_Other.P, node.P,
                            color, color, 2.f);

        GraphNodeIndex j = m_pClustering->getNearestNode(m_SurfacePoint);
        if(j != UNDEFINED_NODE) {
            float geoDist = embree::length(m_Other.P - node.P);

            m_pRenderer->renderLine(m_Other.P, node.P,
                                    Col3f(0.f), Col3f(0.f), 5.f);

            node = m_pClustering->getNode(j);

            geoDist += embree::length(m_SurfacePoint.P - node.P);

            m_pRenderer->renderLine(m_SurfacePoint.P, node.P,
                                    Col3f(0.f), Col3f(0.f), 5.f);

            if(m_pClustering->checkVisibility(i, j)) {
                Skeleton::Node node1 = m_pClustering->getNode(i);
                Skeleton::Node node2 = m_pClustering->getNode(j);

                m_pRenderer->renderLine(node1.P, node2.P, Col3f(0.f), Col3f(0.f), 5.f);
            } else {
                GraphNodeIndex tmp = i;
                while(tmp != j) {
                    GraphNodeIndex parent = m_ShortestPaths[j][tmp].predecessor;

                    Skeleton::Node node1 = m_pClustering->getNode(tmp);
                    Skeleton::Node node2 = m_pClustering->getNode(parent);

                    geoDist += embree::length(node1.P - node2.P);

                    m_pRenderer->renderLine(node1.P, node2.P,
                                            Col3f(0.f), Col3f(0.f), 5.f);

                    tmp = parent;
                }
            }
        }
    }

    m_pRenderer->renderArrow(m_Other.P, m_Other.Ns,
                             m_pDevice->getSurfacePointNodeColor(m_Other),
                             m_pDevice->getSurfacePointScale());
    m_pRenderer->renderLine(m_SurfacePoint.P, m_Other.P,
                           m_pDevice->getSurfacePointNodeColor(m_SurfacePoint),
                           m_pDevice->getSurfacePointNodeColor(m_Other), 3.f);
    */

    exentricityDot();

    m_pRenderer->renderArrow(m_Other.P, m_Other.Ns,
                             m_pDevice->getSurfacePointNodeColor(m_Other),
                             m_pDevice->getSurfacePointScale());
    /*m_pRenderer->renderLine(m_SurfacePoint.P, m_Other.P,
                           m_pDevice->getSurfacePointNodeColor(m_SurfacePoint),
                           m_pDevice->getSurfacePointNodeColor(m_Other), 3.f);*/
}

#endif
}
