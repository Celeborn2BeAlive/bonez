#include "SkelClusteringDeferredViewer.hpp"
#include "application/imgui/imgui.h"

#include "application/gui.hpp"

#include "bonez/common/Progress.hpp"

#include "application/parsers/JsonParser.hpp"
#include "bonez/renderer/factories.hpp"

#include "VisualDataViewer.hpp"

#include "bonez/renderer/VPLRenderer.hpp"
#include "application/glfw/GLFWApplication.hpp"

namespace BnZ {

SkelClusteringDeferredViewer::SkelClusteringDeferredViewer():
    m_pRenderer(nullptr),
    m_pGLScene(nullptr),
    m_pDevice(nullptr),
    m_pVisualDataViewer(nullptr),
    m_pVisibilityClustering(nullptr),
    m_nVPLCountToGenerate(512),
    m_bRenderMode(false) {
}

void SkelClusteringDeferredViewer::init(
        Device& device,
        GLVisualDataRenderer& renderer, GLScene& glScene,
        VisualDataViewer& visualDataViewer,
        VisibilityClustering& visibilityClustering) {
    m_pDevice = &device;
    m_pGLScene = &glScene;
    m_pRenderer = &renderer;
    m_pVisualDataViewer = &visualDataViewer;
    m_pVisibilityClustering = &visibilityClustering;

    m_GLSkelDeferredRenderer.setResolution(device.getFramebuffer().getWidth(), device.getFramebuffer().getHeight());
    m_GLSkelDeferredRenderer.sendSkelClustering(m_pDevice->getScene());
}

void SkelClusteringDeferredViewer::generateVPLs() {
    ParamSet params("VPLSampler",
                    "type", "RandomVPLSampler",
                    "minDepth", 2,
                    "maxDepth", 2,
                    "vplCount", m_nVPLCountToGenerate,
                    "indirectOnly", true,
                    "vplFilter", ParamSet("vplFilter",
                                          "type", "GeorgievVPLFilter",
                                          "viewRayCountX", 8,
                                          "viewRayCountY", 8,
                                          "epsilon", 0.00000001,
                                          "integratorSampler", ParamSet("sampler",
                                                                        "type", "StratifiedSampler",
                                                                        "sppX", 256,
                                                                        "sppY", 128),
                                          "integrator", ParamSet("integrator",
                                                                 "type", "PathtraceIntegrator",
                                                                 "maxDepth", 8,
                                                                 "indirectOnly", true),
                                          "useFiltering", true,
                                          "useResampling", false,
                                          "vplCount", 1024)
                    );
    VPLSamplerPtr sampler = createVPLSampler(params);
    ParamSet stats;
    m_VPLs = sampler->sample(m_pDevice->getCamera(), m_pDevice->getScene(),
                                     m_pDevice->getFramebuffer(), 0, stats);
}

void SkelClusteringDeferredViewer::computeReference() {
    VPLRenderer renderer;
    renderer.setVPLs(m_VPLs);
    renderer.render(m_pDevice->getCamera(),
                    m_pDevice->getScene(),
                    m_pDevice->getFramebuffer(),
                    true, false, false, 0.f, 1);
    GLFWApplication::getInstance()->setResultViewer();
}

void SkelClusteringDeferredViewer::setRenderModeCallback(const void* value, void* clientData) {
    SkelClusteringDeferredViewer* viewer = (SkelClusteringDeferredViewer*) clientData;
    viewer->m_bRenderMode = *(bool*) value;
    if(viewer->m_bRenderMode) {
        viewer->m_GLSkelDeferredRenderer.setup();
    }
}

void SkelClusteringDeferredViewer::getRenderModeCallback(void* value, void* userData) {
    *(bool*) value = ((SkelClusteringDeferredViewer*) userData)->m_bRenderMode;
}

void SkelClusteringDeferredViewer::onSetup() {
    TwBar* bar = TwNewBar("Gate Lighting");

    atb::addVarRW(bar, "VPL Count", m_nVPLCountToGenerate, "");
    atb::addButton(bar, "Generate VPLs", [this]() { generateVPLs(); }, "");

    atb::addVarCB(bar, "Render mode",
                  [this](bool value) { m_bRenderMode = value; if(value) { m_GLSkelDeferredRenderer.setup(); } },
                  [this]() -> bool { return m_bRenderMode; }, "");

    atb::addButton(bar, "Compute reference", [this]() { computeReference(); }, "");
}

void SkelClusteringDeferredViewer::onTeardown() {
    atb::deleteBar(TwGetBarByName("Gate Lighting"));
}

void SkelClusteringDeferredViewer::setWindowSize(size_t width, size_t height) {
    m_nWidth = width;
    m_nHeight = height;
}

void SkelClusteringDeferredViewer::handleClickEvent(int x, int y) {

}

void SkelClusteringDeferredViewer::drawView() {
    if(m_bRenderMode) {
        m_GLSkelDeferredRenderer.setProjectionMatrix(computeGLProjectionMatrix(m_pDevice->getCamera()));
        m_GLSkelDeferredRenderer.setViewMatrix(computeGLViewMatrix(m_pDevice->getCamera()));

        gloops::StateRestorer<GL_VIEWPORT> viewport;

        gloops::Viewport m_Viewport(0.5 * (m_nWidth - m_pDevice->getFramebuffer().getWidth()),
                                  0.5 * (m_nHeight - m_pDevice->getFramebuffer().getHeight()),
                                  m_pDevice->getFramebuffer().getWidth(),
                                  m_pDevice->getFramebuffer().getHeight());

        glViewport(0, 0, m_Viewport.width, m_Viewport.height);

        m_GLSkelDeferredRenderer.geometryPass(*m_pGLScene);

        float w = m_Viewport.width / 3.f;
        float h = m_Viewport.height / 3.f;

        float x = 0.5 * (m_nWidth - 4 * w);
        float y = m_Viewport.y - h;

        m_GLSkelDeferredRenderer.drawBuffers(x, y, w, h);

        gloops::TextureObject texture;
        {
            gloops::TextureBind texBind(GL_TEXTURE_2D, 0, texture);
            texBind.texImage2D(0, GL_RGB16F, m_Viewport.width, m_Viewport.height,
                0, GL_RGB, GL_FLOAT, nullptr);
            texBind.setMinFilter(GL_NEAREST);
            texBind.setMagFilter(GL_NEAREST);
        }

        gloops::FramebufferObject fbo;
        {
            gloops::FramebufferBind fboBind(GL_DRAW_FRAMEBUFFER, fbo);

            fboBind.framebufferTexture2D(GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

            GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
            glDrawBuffers(1, drawBuffers);

            m_GLSkelDeferredRenderer.shadingPass(0, 0, m_Viewport.width, m_Viewport.height, m_VPLs,
                                              m_pDevice->getScene());
//            m_GLLightGateRenderer.drawDiscontinuityBuffer(0, 0, m_Viewport.width, m_Viewport.height);
        }

        gloops::FramebufferBind fboBind(GL_READ_FRAMEBUFFER, fbo);
        glReadBuffer(GL_COLOR_ATTACHMENT0);

        glBlitFramebuffer(0, 0, m_Viewport.width, m_Viewport.height,
            m_Viewport.x, m_Viewport.y, m_Viewport.x + m_Viewport.width, m_Viewport.y + m_Viewport.height, GL_COLOR_BUFFER_BIT, GL_LINEAR);

    } else {
        GraphNodeIndex node = m_pVisualDataViewer->getSelectedNode();
        if(node != UNDEFINED_NODE) {
            if(m_pVisualDataViewer->isSkeletonCurrentlyDisplayed()) {
                node = m_pDevice->getScene().voxelSpace->getClustering().getNodeClusterIndex(node);
            }

            int cluster = m_pVisibilityClustering->getClusterOf(node);
            if(cluster >= 0) {
                for(auto neighbourLink: (*m_pVisibilityClustering)[cluster].getNeighbourLinks()) {
                    auto gate = m_pVisibilityClustering->getGate(neighbourLink);
                    m_pRenderer->renderDisk(gate.center, gate.N, gate.radius, (*m_pVisibilityClustering)[neighbourLink.neighbour].getColor());
                    m_pRenderer->renderArrow(gate.center, gate.N, (*m_pVisibilityClustering)[neighbourLink.neighbour].getColor(), m_pDevice->getSurfacePointScale());
                }
            }
        }

        for(const auto& vpl: m_VPLs.orientedVPLs) {
            Col3f color(0.f);
            GraphNodeIndex node = m_pDevice->getScene().voxelSpace->getClustering().getNearestNode(vpl.P, vpl.N);
            if(node != UNDEFINED_NODE) {
                int cluster = m_pVisibilityClustering->getClusterOf(node);
                if(cluster >= 0) {
                    color = (*m_pVisibilityClustering)[cluster].getColor();
                }
            }

//            float maxColor = embree::reduce_max(vpl.L);
//            float r = embree::sqrt(maxColor / 0.01);

//            m_pRenderer->renderCircle(vpl.P, r, color, 1.f);

            m_pRenderer->renderArrow(vpl.P, vpl.N,
                                     color,
                                     m_pDevice->getSurfacePointScale());
        }
    }
}

}
