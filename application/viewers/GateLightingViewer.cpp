#include "GateLightingViewer.hpp"

#include "application/gui.hpp"

#include "bonez/common/Progress.hpp"

#include "application/parsers/JsonParser.hpp"
#include "bonez/renderer/factories.hpp"

#include "VisualDataViewer.hpp"

#include "bonez/renderer/VPLRenderer.hpp"
#include "application/glfw/GLFWApplication.hpp"

#include "bonez/renderer/instantradiosity/FromPointVPLSampler.hpp"

namespace BnZ {

GateLightingViewer::GateLightingViewer():
    m_pRenderer(nullptr),
    m_pGLScene(nullptr),
    m_pDevice(nullptr),
    m_pVisualDataViewer(nullptr),
    m_pVisibilityClustering(nullptr),
    m_nVPLCountToGenerate(512),
    m_bRenderMode(false),
    m_nDataToDisplay(-1),
    m_bRenderBSpheres(false),
    m_bDrawBuffers(true),
    m_nRefSpp(1) {
}

void GateLightingViewer::init(
        Device& device,
        GLVisualDataRenderer& renderer, GLScene& glScene,
        VisualDataViewer& visualDataViewer,
        VisibilityClustering& visibilityClustering) {
    m_pDevice = &device;
    m_pGLScene = &glScene;
    m_pRenderer = &renderer;
    m_pVisualDataViewer = &visualDataViewer;
    m_pVisibilityClustering = &visibilityClustering;

    m_GLLightGateRenderer.setResolution(device.getFramebuffer().getWidth(), device.getFramebuffer().getHeight());
    m_GLLightGateRenderer.sendVisibilityClusteringGrid(m_pDevice->getScene(), visibilityClustering);
}

void GateLightingViewer::generateVPLs() {
//    ParamSet params("VPLSampler",
//                    "type", "RandomVPLSampler",
//                    "minDepth", 2,
//                    "maxDepth", 2,
//                    "vplCount", m_nVPLCountToGenerate,
//                    "indirectOnly", true,
//                    "vplFilter", ParamSet("vplFilter",
//                                          "type", "GeorgievVPLFilter",
//                                          "viewRayCountX", 8,
//                                          "viewRayCountY", 8,
//                                          "epsilon", 0.00000001,
//                                          "integratorSampler", ParamSet("sampler",
//                                                                        "type", "StratifiedSampler",
//                                                                        "sppX", 256,
//                                                                        "sppY", 128),
//                                          "integrator", ParamSet("integrator",
//                                                                 "type", "PathtraceIntegrator",
//                                                                 "maxDepth", 8,
//                                                                 "indirectOnly", true),
//                                          "useFiltering", true,
//                                          "useResampling", false,
//                                          "vplCount", 1024)
//                    );
//    VPLSamplerPtr sampler = createVPLSampler(params);
//    ParamSet stats;
//    m_VPLs = sampler->sample(m_pDevice->getCamera(), m_pDevice->getScene(),
//                                     m_pDevice->getFramebuffer(), 0, stats);


    static float t = 0.f;

    //m_LightSourcePosition = Vec3f(0.f, 500.f * fabs(cos(t)), 0.f);
    //m_LightSourcePosition = Vec3f(-60.52, 920.05, -34.5759);
    Col3f I(m_fIntensity);

    FromPointVPLSampler sampler(m_LightSourcePosition, I, m_nPathDepth, m_nPathDepth, size_t(embree::inf), m_nVPLCountToGenerate);
    ParamSet stats;
    m_VPLs = sampler.sample(m_pDevice->getCamera(), m_pDevice->getScene(),
                                     m_pDevice->getFramebuffer(), 0, stats);
    m_GLLightGateRenderer.setPrimaryLightSource(m_LightSourcePosition, I, *m_pGLScene);

    t += 0.1f;
}

void GateLightingViewer::computeReference() {
    VPLRenderer renderer;
    renderer.setVPLs(m_VPLs);
    renderer.setPrimaryLightSource(m_LightSourcePosition, Col3f(m_fIntensity));
    renderer.render(m_pDevice->getCamera(),
                    m_pDevice->getScene(),
                    m_pDevice->getFramebuffer(),
                    true, m_GLLightGateRenderer.m_bRenderIrradiance, m_GLLightGateRenderer.m_bDoDirectLightPass,
                    0.f, m_nRefSpp);
    GLFWApplication::getInstance()->setResultViewer();
    GLFWApplication::getInstance()->getResultViewer().setGamma(m_GLLightGateRenderer.getGamma());
}

void GateLightingViewer::onSetup() {
    TwBar* bar = TwNewBar("Gate Lighting");

    atb::addVarRW(bar, "VPL Count", m_nVPLCountToGenerate, "");
    atb::addButton(bar, "Generate VPLs", [this]() { generateVPLs(); }, "");

    atb::addVarCB(bar, "Render mode",
                  [this](bool value) {
                    m_bRenderMode = value;
                    if(value) {
                        m_GLLightGateRenderer.setUp();
                    } else {
                        m_pRenderer->setUp();
                    }
                  },
                  [this]() -> bool { return m_bRenderMode; },
    "");

    atb::addVarRW(bar, "Draw buffers", m_bDrawBuffers, "");
    atb::addVarRW(bar, "Data to display", m_nDataToDisplay, "");
    atb::addVarRW(bar, "Reference spp", m_nRefSpp, "");
    atb::addButton(bar, "Compute reference", [this]() { computeReference(); }, "");
    atb::addVarRW(bar, "Render BSpheres", m_bRenderBSpheres, "");
    atb::addVarRW(bar, "Intensity", m_fIntensity, "");
    atb::addVarRW(bar, "Path Depth", m_nPathDepth, "");

    m_GLLightGateRenderer.exposeIO(bar);
}

void GateLightingViewer::onTeardown() {
    atb::deleteBar(TwGetBarByName("Gate Lighting"));
}

void GateLightingViewer::setWindowSize(size_t width, size_t height) {
    m_nWidth = width;
    m_nHeight = height;
}

void GateLightingViewer::handleClickEvent(int x, int y) {

}

void GateLightingViewer::handleNodeSelection(GraphNodeIndex node) {
    if(m_pVisualDataViewer->isClusteringCurrentlyDisplayed()) {
        m_LightSourcePosition = m_pDevice->getScene().voxelSpace->getClustering().getNode(node).P;
        m_GLLightGateRenderer.m_nSelectedVisibilityCluster = m_pVisibilityClustering->getClusterOf(node);
        std::cerr << "Is Transfert VCluster : " << (*m_pVisibilityClustering)[m_GLLightGateRenderer.m_nSelectedVisibilityCluster].isTransfertCluster() << std::endl;
    }
}

void GateLightingViewer::drawView() {

    /*static float t = 0.f;
    FromPointVPLSampler sampler(Vec3f(100.f * cos(t), 100.f, 100.f * sin(t)), Col3f(3000000.f), 1, 8, size_t(embree::inf), m_nVPLCountToGenerate);
    ParamSet stats;
    m_VPLs = sampler.sample(m_pDevice->getCamera(), m_pDevice->getScene(),
                                     m_pDevice->getFramebuffer(), 0, stats);

    t += 0.1f;
    */



    if(m_bRenderMode) {
        m_GLLightGateRenderer.setProjectionMatrix(computeGLProjectionMatrix(m_pDevice->getCamera()));
        m_GLLightGateRenderer.setViewMatrix(computeGLViewMatrix(m_pDevice->getCamera()));

        m_GLLightGateRenderer.geometryPass(*m_pGLScene);

        gloops::Viewport m_Viewport(
                                  0.5 * (m_nWidth - m_pDevice->getFramebuffer().getWidth()),
                                  0.5 * (m_nHeight - m_pDevice->getFramebuffer().getHeight()),
                                  m_pDevice->getFramebuffer().getWidth(),
                                  m_pDevice->getFramebuffer().getHeight());

        if(m_bDrawBuffers) {
//            m_Viewport = gloops::Viewport(
//                        0.5 * (m_nWidth - m_pDevice->getFramebuffer().getWidth()),
//                        m_nHeight - m_pDevice->getFramebuffer().getHeight(),
//                        m_pDevice->getFramebuffer().getWidth(),
//                        m_pDevice->getFramebuffer().getHeight());

            float w = m_Viewport.width / 4.f;
            float h = m_Viewport.height / 4.f;

            float x = 0.5 * (m_nWidth - 5 * w);
            float y = m_Viewport.y - h;

            m_GLLightGateRenderer.drawBuffers(x, y, w, h);
        }

        gloops::TextureObject texture;
        {
            gloops::TextureBind texBind(GL_TEXTURE_2D, 0, texture);
            texBind.texImage2D(0, GL_RGB16F, m_Viewport.width, m_Viewport.height,
                0, GL_RGB, GL_FLOAT, nullptr);
            texBind.setMinFilter(GL_NEAREST);
            texBind.setMagFilter(GL_NEAREST);
        }

        gloops::FramebufferObject fbo;
        {
            gloops::FramebufferBind fboBind(GL_DRAW_FRAMEBUFFER, fbo);

            fboBind.framebufferTexture2D(GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

            GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
            glDrawBuffers(1, drawBuffers);

            if(m_nDataToDisplay < 0) {
                m_GLLightGateRenderer.shadingPass(0, 0, m_Viewport.width, m_Viewport.height, m_VPLs,
                                                  m_pDevice->getScene(), *m_pVisibilityClustering);
            } else {
                m_GLLightGateRenderer.drawBuffer(0, 0, m_Viewport.width, m_Viewport.height, m_nDataToDisplay);
            }
        }

        m_GLLightGateRenderer.computeTimings();

        gloops::FramebufferBind fboBind(GL_READ_FRAMEBUFFER, fbo);
        glReadBuffer(GL_COLOR_ATTACHMENT0);

        glBlitFramebuffer(0, 0, m_Viewport.width, m_Viewport.height,
            m_Viewport.x, m_Viewport.y, m_Viewport.x + m_Viewport.width, m_Viewport.y + m_Viewport.height, GL_COLOR_BUFFER_BIT, GL_LINEAR);

    } else {
        GraphNodeIndex node = m_pVisualDataViewer->getSelectedNode();
        if(node != UNDEFINED_NODE) {
//            m_pRenderer->renderGridArround(m_pVisualDataViewer->getCurrentlyDisplayedSkeleton()->getNode(node).P, 2,
//                                          m_pDevice->getScene(), m_pVisualDataViewer->getCurrentlyDisplayedSkeleton());

            if(m_pVisualDataViewer->isSkeletonCurrentlyDisplayed()) {
                node = m_pDevice->getScene().voxelSpace->getClustering().getNodeClusterIndex(node);
            }



            int cluster = m_pVisibilityClustering->getClusterOf(node);
            if(cluster >= 0) {
                for(auto neighbourLink: (*m_pVisibilityClustering)[cluster].getNeighbourLinks()) {
                    auto gate = m_pVisibilityClustering->getGate(neighbourLink);
                    m_pRenderer->renderDisk(gate.center, gate.N, gate.radius, (*m_pVisibilityClustering)[neighbourLink.neighbour].getColor());
                    m_pRenderer->renderArrow(gate.center, gate.N, (*m_pVisibilityClustering)[neighbourLink.neighbour].getColor(), m_pDevice->getSurfacePointScale());
                }
            }
        }

        for(const auto& vpl: m_VPLs.orientedVPLs) {
            Col3f color(0.f);
            GraphNodeIndex node = m_pDevice->getScene().voxelSpace->getClustering().getNearestNode(vpl.P, vpl.N);
            if(node != UNDEFINED_NODE) {
                int cluster = m_pVisibilityClustering->getClusterOf(node);
                if(cluster >= 0) {
                    color = (*m_pVisibilityClustering)[cluster].getColor();
                }
            }

//            float maxColor = embree::reduce_max(vpl.L);
//            float r = embree::sqrt(maxColor / 0.01);

//            m_pRenderer->renderCircle(vpl.P, r, color, 1.f);

            m_pRenderer->renderArrow(vpl.P, vpl.N,
                                     color,
                                     m_pDevice->getSurfacePointScale());
        }

        m_pRenderer->renderSphere(m_LightSourcePosition, 0.5f * m_pDevice->getSurfacePointScale(), Col3f(m_fIntensity));

        if(m_bRenderBSpheres) {
            m_GLLightGateRenderer.setUp();

            m_GLLightGateRenderer.setProjectionMatrix(computeGLProjectionMatrix(m_pDevice->getCamera()));
            m_GLLightGateRenderer.setViewMatrix(computeGLViewMatrix(m_pDevice->getCamera()));

            m_GLLightGateRenderer.geometryPass(*m_pGLScene);

            m_GLLightGateRenderer.displayData(*m_pRenderer, m_pDevice->getScene(),
                                              m_VPLs, *m_pVisibilityClustering);
        }

        //generateVPLs();
        //m_GLLightGateRenderer.drawPrimaryLightShadowMap(0, 0, 4 * 64, 3 * 64);
    }
}

}
