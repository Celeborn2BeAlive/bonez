#include "VisualDataViewer.hpp"
#include "application/gui.hpp"

#include "bonez/common/Progress.hpp"

#include "bonez/scene/Pink.hpp"

#include "bonez/common/data/ColorMap.hpp"

namespace BnZ {

VisualDataViewer::VisualDataViewer(const GLSLProgramBuilder& programBuilder):
    m_Device(nullptr), m_GLScene(nullptr),
    m_bDrawScene(true), m_bDrawGrid(false),
    m_bUseClusterGridColors(true),
    m_VisualDataRenderer(programBuilder),
    m_pCurrentlyDisplayedSkeleton(nullptr),
    m_bDisplayMaxball(false),
    m_bDisplayNodes(true),
    m_ColorDataType(CLUSTERING_COLORS),
    m_SurfacePointDataType(NEAREST_NODE) {
}

void VisualDataViewer::init(Device* device, GLScene* glScene) {
    m_Device = device;
    m_GLScene = glScene;

    if(device->getScene().voxelSpace) {
        setSkeleton(device->getScene().voxelSpace->getClustering());

        m_VisibilityClustering = buildVisibilityClustering(m_Device->getScene().voxelSpace->getClustering());

        m_ColorDataType = VISIBILITY_CLUSTER;
        recomputeColors();
    }

    m_Viewer.init(*device, m_VisualDataRenderer, *glScene, *this, m_VisibilityClustering);

    //m_Viewer.init(*device, m_VisualDataRenderer, *glScene, *this);
}

void VisualDataViewer::recomputeColors() {
    assert(m_pCurrentlyDisplayedSkeleton);

    VoxelSpace& space = *m_Device->getScene().voxelSpace;

    switch(m_ColorDataType) {
    default:
        break;
    case RANDOM_COLORS:
        setRandomColors(space);
        break;
    case CLUSTERING_COLORS:
        setClusteringColors(space);
        break;
    case MAXBALL_COLORS:
        setMaxballRadiusColors(space);
        break;
    case LOCAL_MAXBALL_EXTREMAS:
        setLocalMaxballExtremasColors(space);
        break;
    case LINE_MAXBALL_EXTREMAS:
        setLineMaxballExtremasColors(space);
        break;
    case MAXBALL_PER_LINE_COLORS:
        setMaxballRadiusPerLineColors(space);
        break;
    case MAXBALL_VARIANCE_PER_LINE_COLORS:
        setMaxballVariancePerLineColors(space);
        break;
    case CURVATURE_COLORS:
        setCurvatureColors(space);
        break;
    case VISIBILITY_CLUSTER:
        setVisibilityClusteringColors(space, m_VisibilityClustering);
        break;
    case MEYER_WATERSHED:
        setMeyerWatershedColors(space);
        break;
    case VISIBILITY_CLUSTERING_TEST:
        setVisibilityClusteringTestColors(space);
        break;
    }

    m_GLGrid.init(m_Device->getScene(), m_bUseClusterGridColors);
}

void VisualDataViewer::setSkeleton(const Skeleton& skeleton) {
    m_pCurrentlyDisplayedSkeleton = &skeleton;
    recomputeColors();

    if(isClusteringCurrentlyDisplayed()) {
        m_CurrentSkeletonType = CLUSTERING;
    } else {
        m_CurrentSkeletonType = SKELETON;
    }

    if(m_Device->getRenderer()) {
        if(isClusteringCurrentlyDisplayed()) {
            m_CurrentSkeletonType = CLUSTERING;
            m_Device->getRenderer()->setParam("useClustering", makeParam(true));
        } else {
            m_CurrentSkeletonType = SKELETON;
            m_Device->getRenderer()->setParam("useClustering", makeParam(false));
        }
    }
}

void VisualDataViewer::fmmClusterGridCallback(void* userData) {
    VisualDataViewer* viewer = (VisualDataViewer*) userData;
    viewer->computeFMMClusterGrid();
}

void VisualDataViewer::computeFMMClusterGrid() {
    Scene& scene = m_Device->getScene();

    if(scene.voxelSpace && !scene.voxelSpace->getVoxelGrid().empty()) {
//        scene.voxelSpace->getClustering().setGrid(
//                    computeGridWithFMM(scene.voxelSpace->getVoxelGrid(), scene.voxelSpace->getClustering()));

        scene.voxelSpace->getClustering().setGrid(
                      computeGridWrtVoxelGridUsingCoherence(scene.voxelSpace->getVoxelGrid(),
                                                            scene.voxelSpace->getClustering()));

        recomputeColors();

        init(m_Device, m_GLScene);
    }
}

void VisualDataViewer::onSetup() {
    if(!m_Device) {
        std::cerr << "VisualDataViewer::drawView - Not initialized" << std::endl;
        return;
    }

    TwBar* bar1 = TwNewBar("Features");

    atb::addVarRW(bar1, "Draw Scene", m_bDrawScene, "");
    atb::addVarCB(bar1, "Draw grid",
                  [this](bool value) { if(value) { recomputeColors(); } m_bDrawGrid = value; },
                  [this]() -> bool { return m_bDrawGrid; }, "");
    atb::addVarRW(bar1, "Use cluster grid colors", m_bUseClusterGridColors, "");

    m_PointSelector.setupGUI();

    TwBar* skelBar = TwNewBar("Skeleton");

    atb::addVarCB(skelBar, "Type", "Clustering, Skeleton",
                 [this](SkeletonType type) {
        switch(type) {
        case CLUSTERING:
            setSkeleton(m_Device->getScene().voxelSpace->getClustering());
            break;
        case SKELETON:
            setSkeleton(m_Device->getScene().voxelSpace->getSkeleton());
            break;
        }
    }, [this]() -> SkeletonType { return m_CurrentSkeletonType; }, "");

    atb::addVarRW(skelBar, "Display Maxballs", m_bDisplayMaxball, "");
    atb::addVarRW(skelBar, "Display nodes", m_bDisplayNodes, "");

    TwEnumVal nodeColors[] = {
        {RANDOM_COLORS, "Random"},
        {CLUSTERING_COLORS, "Clustering"},
        {MAXBALL_COLORS, "Maxball"},
        {LOCAL_MAXBALL_EXTREMAS, "Local Maxball Extremas"},
        {MAXBALL_PER_LINE_COLORS, "Maxball per Line"},
        {MAXBALL_VARIANCE_PER_LINE_COLORS, "Maxball Variance per Line"},
        {CURVATURE_COLORS, "Curvature"},
        {VISIBILITY_CLUSTER, "Visibility Cluster"},
        {LINE_MAXBALL_EXTREMAS, "Line Maxball Extremas"},
        {MEYER_WATERSHED, "Meyer Watershed"},
        {VISIBILITY_CLUSTERING_TEST, "TEST Visibility Clustering"},
    };

    atb::addVarCB(skelBar, "Node Colors", nodeColors, COLOR_COUNT,
                 [this](ColorDataType type) {
                     m_ColorDataType = type;
                     recomputeColors();
                 },
                 [this]() -> ColorDataType { return m_ColorDataType; },
    "");

    atb::addVarRW(skelBar, "Surface Point Data", "Nearest Node, Neighbouring", m_SurfacePointDataType, "");

    atb::addButton(skelBar, "Select Biggest Node",
                   [this]() { m_SelectedNode = computeBiggestNode(*m_pCurrentlyDisplayedSkeleton); }, "");

    atb::addButton(skelBar, "Compute cluster grid with FMM", [this]() { computeFMMClusterGrid(); }, "");

    atb::addVarRO(skelBar, "Current Node", m_SelectedNode, "");
    atb::addVarRO(skelBar, "Node Position", m_SelectedNodePosition, "");

    m_Viewer.onSetup();
}

void VisualDataViewer::onTeardown() {
    atb::deleteBar(TwGetBarByName("Features"));

    m_PointSelector.teardownGUI();

    atb::deleteBar(TwGetBarByName("Skeleton"));

    m_Viewer.onTeardown();
}

void VisualDataViewer::setWindowSize(size_t width, size_t height) {
    m_nWidth = width;
    m_nHeight = height;

    m_Viewer.setWindowSize(width, height);
}

void VisualDataViewer::handleClickEvent(int x, int y) {
    if(!m_Device) {
        std::cerr << "VisualDataViewer::handleClickEvent - Not initialized" << std::endl;
        return;
    }

    const Scene& scene = m_Device->getScene();

    gloops::Viewport viewport(0.5 * (m_nWidth - m_Device->getFramebuffer().getWidth()),
                              0.5 * (m_nHeight - m_Device->getFramebuffer().getHeight()),
                              m_Device->getFramebuffer().getWidth(),
                              m_Device->getFramebuffer().getHeight());

    if(x >= viewport.x && y >= viewport.y && x < viewport.x + viewport.width && y < viewport.y + viewport.height) {
        GraphNodeIndex node = m_VisualDataRenderer.getNodePickingTexture()(x - viewport.x, y - viewport.y);
        if(node != UNDEFINED_NODE) {
            m_SelectedNode = node;
            m_SelectedNodePosition = m_pCurrentlyDisplayedSkeleton->getNode(m_SelectedNode).P;
            m_Viewer.handleNodeSelection(node);
        } else {
            Vec2f rasterPosition = m_Device->getFramebuffer().getRasterPosition(x - viewport.x, y - viewport.y);

            Intersection I = intersect(m_Device->getCamera().ray(CameraSample(rasterPosition, Vec2f(0.5f, 0.5f))), scene);
            if(!I) {
                return;
            }

            Vec3i voxel = m_Device->getScene().voxelSpace->getVoxel(I.P);
            std::cerr << voxel.x << ", " << voxel.y << ", " << voxel.z << std::endl;

            m_PointSelector.setPoint(I);
        }
    }

    m_Viewer.handleClickEvent(x, y);
}


bool VisualDataViewer::isSkeletonCurrentlyDisplayed() const {
    return m_pCurrentlyDisplayedSkeleton &&
            m_pCurrentlyDisplayedSkeleton == &m_Device->getScene().voxelSpace->getSkeleton();
}

bool VisualDataViewer::isClusteringCurrentlyDisplayed() const {
    return m_pCurrentlyDisplayedSkeleton &&
            m_pCurrentlyDisplayedSkeleton == &m_Device->getScene().voxelSpace->getClustering();
}

void VisualDataViewer::drawLink(const SurfacePoint& point) {
    drawLink(point, m_Device->getSurfacePointNodeColor(point));
}

void VisualDataViewer::drawLink(const SurfacePoint& point, const Col3f& color) {
    GraphNodeIndex i = m_pCurrentlyDisplayedSkeleton->getNearestNode(point);


    //m_VisualDataRenderer.renderGridArround(point.P, 2, m_Device->getScene());

//    m_VisualDataRenderer.renderGridArround(m_Device->getScene().voxelSpace->getClustering().getVoxelSelector(point),
//                                   2, m_Device->getScene(), m_pCurrentlyDisplayedSkeleton);

    if(i != UNDEFINED_NODE) {
        Skeleton::Node node = m_pCurrentlyDisplayedSkeleton->getNode(i);
        m_VisualDataRenderer.renderLine(point.P, node.P,
                            color, color, 2.f);

        if(m_bDisplayMaxball) {
            m_VisualDataRenderer.renderCircle(node.P, node.maxball, color, 3.f);
        }
    }
}

void VisualDataViewer::drawNeighbouring(const SurfacePoint& point) {
    GraphNodeIndex i = m_pCurrentlyDisplayedSkeleton->getNearestNode(point);
    if(i == UNDEFINED_NODE) {
        return;
    }

    auto neighbours = m_pCurrentlyDisplayedSkeleton->neighbours(i);
    neighbours.emplace_back(i);

    for(GraphNodeIndex j: neighbours) {
        Skeleton::Node node = m_pCurrentlyDisplayedSkeleton->getNode(j);

        Col3f color = sample(HEAT_MAP, node.maxball / embree::distance(point.P, node.P));

        Vec3f wi = node.P - point.P;
        float coherence = embree::dot(wi, point.Ns) * node.maxball / embree::dot(wi, wi);

        color = sample(HEAT_MAP, coherence);
        m_VisualDataRenderer.renderLine(point.P, node.P,
                                color, color, 2.f);

        if(m_bDisplayMaxball) {
            m_VisualDataRenderer.renderCircle(node.P, node.maxball, color, 3.f);
        }
    }
}

void VisualDataViewer::drawSurfacePointSkeletonData(const SurfacePoint& point) {
    if(!m_pCurrentlyDisplayedSkeleton) {
        return;
    }

    switch(m_SurfacePointDataType) {
    case NEAREST_NODE:
        drawLink(point);
        break;
    case NEIGHBOURING:
        drawNeighbouring(point);
        break;
    default:
        break;
    }
}

void VisualDataViewer::drawSkeleton() {
    if(m_pCurrentlyDisplayedSkeleton) {
        const Scene& scene = m_Device->getScene();

        m_VisualDataRenderer.renderSkeleton(
                    *m_pCurrentlyDisplayedSkeleton, scene.voxelSpace->getVoxelSize(),
                    getNodeColors(*m_pCurrentlyDisplayedSkeleton, *scene.voxelSpace).data(), m_bDisplayNodes);

        if(m_SelectedNode != UNDEFINED_NODE) {
            Skeleton::Node node = m_pCurrentlyDisplayedSkeleton->getNode(m_SelectedNode);
            m_VisualDataRenderer.renderCircle(node.P, node.maxball,
                                      getNodeColors(*m_pCurrentlyDisplayedSkeleton,
                                                    *m_Device->getScene().voxelSpace)[m_SelectedNode],
                                      3.f);
        }
    }
}

void VisualDataViewer::drawView() {
    if(!m_Device || !m_GLScene) {
        std::cerr << "VisualDataViewer::drawView - Not initialized" << std::endl;
        return;
    }

    if(m_SelectedNode < m_pCurrentlyDisplayedSkeleton->size()) {
        m_SelectedNodePosition = m_pCurrentlyDisplayedSkeleton->getNode(m_SelectedNode).P;
        m_Viewer.handleNodeSelection(m_SelectedNode);
    }

    gloops::Viewport viewport(0.5 * (m_nWidth - m_Device->getFramebuffer().getWidth()),
                              0.5 * (m_nHeight - m_Device->getFramebuffer().getHeight()),
                              m_Device->getFramebuffer().getWidth(),
                              m_Device->getFramebuffer().getHeight());

    auto states = m_VisualDataRenderer.startFrame(viewport.x, viewport.y, viewport.width, viewport.height);

    if(m_Viewer.isRenderMode()) {
        m_Viewer.drawView();
        return;
    }

    m_VisualDataRenderer.setProjectionMatrix(computeGLProjectionMatrix(m_Device->getCamera()));
    m_VisualDataRenderer.setViewMatrix(computeGLViewMatrix(m_Device->getCamera()));

    const Scene& scene = m_Device->getScene();

    if(m_bDrawScene) {
        m_VisualDataRenderer.renderScene(*m_GLScene, scene);
    }

    if(m_bDrawGrid) {
        m_VisualDataRenderer.renderGrid(m_GLGrid);
    }

    for(size_t i = 0; i < m_PointSelector.size(); ++i) {
        if(m_PointSelector.isEnabled(i)) {
            Intersection I = m_PointSelector[i];
            m_VisualDataRenderer.renderArrow(I.P, I.Ns,
                                             m_Device->getSurfacePointNodeColor(I),
                                             m_Device->getScene().voxelSpace->getVoxelSize()
                                             /*m_Device->getSurfacePointScale()*/);

            /*
            m_VisualDataRenderer.setDisplayMode(GLVisualDataRenderer::LINE);
            m_VisualDataRenderer.renderSphere(I.P,
                                              embree::sqrt(m_Device->getSurfacePointScale()),
                                              Col3f(1, 0, 0));
            m_VisualDataRenderer.setDisplayMode(GLVisualDataRenderer::FILL);*/

            drawSurfacePointSkeletonData(I);
        }
    }

    drawSkeleton();
    m_Viewer.drawView();

    // Node picking
    m_VisualDataRenderer.startNodePicking();

    m_VisualDataRenderer.renderScene(*m_GLScene, scene);
    drawSkeleton();

    m_VisualDataRenderer.endNodePicking();
}

void VisualDataViewer::setColorDataTypeCallback(const void* value, void* userData) {
    VisualDataViewer* viewer = (VisualDataViewer*) userData;
    viewer->m_ColorDataType = *(ColorDataType*) value;
    viewer->recomputeColors();
}

void VisualDataViewer::getColorDataTypeCallback(void* value, void* userData) {
    VisualDataViewer* viewer = (VisualDataViewer*) userData;
    *(ColorDataType*) value = viewer->m_ColorDataType;
}

void VisualDataViewer::selectBiggestNodeCallback(void* userData) {
    VisualDataViewer* viewer = (VisualDataViewer*) userData;
    viewer->m_SelectedNode = computeBiggestNode(*viewer->m_pCurrentlyDisplayedSkeleton);
}


}
