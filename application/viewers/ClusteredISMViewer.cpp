#include "ClusteredISMViewer.hpp"

#include "application/gui.hpp"

#include "bonez/common/Progress.hpp"

#include "application/parsers/JsonParser.hpp"
#include "bonez/renderer/factories.hpp"

#include "bonez/renderer/instantradiosity/FromPointVPLSampler.hpp"

#include "VisualDataViewer.hpp"

#include "bonez/renderer/VPLRenderer.hpp"
#include "application/glfw/GLFWApplication.hpp"

#include "bonez/common/sampling/Sampler.hpp"

namespace BnZ {

ClusteredISMViewer::ClusteredISMViewer():
    m_pRenderer(nullptr),
    m_pGLScene(nullptr),
    m_pDevice(nullptr),
    m_pVisualDataViewer(nullptr),
    m_nVPLCountToGenerate(512),
    m_bRenderMode(false),
    m_nDataToDisplay(-1),
    m_bRenderBSpheres(false),
    m_fIntensity(3000000),
    m_nPathDepth(1),
    m_nRefSpp(1),
    m_bDrawPrimaryLightShadowMap(false),
    m_bDisplayISMs(false),
    m_nSelectedClusterNode(UNDEFINED_NODE) {
}

static const int NB_CUBE_SM = 1;

void ClusteredISMViewer::init(
        Device& device,
        GLVisualDataRenderer& renderer, GLScene& glScene,
        VisualDataViewer& visualDataViewer) {
    m_pDevice = &device;
    m_pGLScene = &glScene;
    m_pRenderer = &renderer;
    m_pVisualDataViewer = &visualDataViewer;

    m_GLVPLRenderer.setResolution(m_pDevice->getFramebuffer().getWidth(),
                                  m_pDevice->getFramebuffer().getHeight());

    m_ShadowMaps.init(128, 128, 1);
    m_CubeShadowMaps.init(512, NB_CUBE_SM);

    sampleScene();
}

void ClusteredISMViewer::generateVPLs() {
//    ParamSet params("VPLSampler",
//                    "type", "RandomVPLSampler",
//                    "minDepth", 2,
//                    "maxDepth", 2,
//                    "vplCount", m_nVPLCountToGenerate,
//                    "indirectOnly", true,
//                    "vplFilter", ParamSet("vplFilter",
//                                          "type", "GeorgievVPLFilter",
//                                          "viewRayCountX", 8,
//                                          "viewRayCountY", 8,
//                                          "epsilon", 0.00000001,
//                                          "integratorSampler", ParamSet("sampler",
//                                                                        "type", "StratifiedSampler",
//                                                                        "sppX", 256,
//                                                                        "sppY", 128),
//                                          "integrator", ParamSet("integrator",
//                                                                 "type", "PathtraceIntegrator",
//                                                                 "maxDepth", 8,
//                                                                 "indirectOnly", true),
//                                          "useFiltering", true,
//                                          "useResampling", false,
//                                          "vplCount", 1024)
//                    );
//    VPLSamplerPtr sampler = createVPLSampler(params);
//    ParamSet stats;
//    m_VPLs = sampler->sample(m_pDevice->getCamera(), m_pDevice->getScene(),
//                                     m_pDevice->getFramebuffer(), 0, stats);


    Col3f I(m_fIntensity);

    FromPointVPLSampler sampler(m_LightSourcePosition, I, m_nPathDepth, m_nPathDepth, size_t(embree::inf), m_nVPLCountToGenerate);
    ParamSet stats;
    m_VPLs = sampler.sample(m_pDevice->getCamera(), m_pDevice->getScene(),
                                     m_pDevice->getFramebuffer(), 0, stats);
    m_GLVPLRenderer.setPrimaryLightSource(m_LightSourcePosition, I, *m_pGLScene);
}

void ClusteredISMViewer::computeReference() {
    VPLRenderer renderer;
    renderer.setVPLs(m_VPLs);
    renderer.setPrimaryLightSource(m_LightSourcePosition, Col3f(m_fIntensity));
    renderer.render(m_pDevice->getCamera(),
                    m_pDevice->getScene(),
                    m_pDevice->getFramebuffer(),
                    true,
                    m_GLVPLRenderer.doesRenderIrradiance(),
                    m_GLVPLRenderer.doesRenderDirectLight(),
                    m_GLVPLRenderer.getDistanceClamp(),
                    m_nRefSpp);
    GLFWApplication::getInstance()->setResultViewer();
}

void ClusteredISMViewer::sampleScene() {
    m_GLVPLRenderer.sampleScene(m_pDevice->getScene(), m_pDevice->getCamera());
}

void ClusteredISMViewer::onSetup() {
    TwBar* bar = TwNewBar("ClusteredISMViewer");

    atb::addVarRW(bar, "VPL Count", m_nVPLCountToGenerate, "");
    atb::addButton(bar, "Generate VPLs", [this]() { generateVPLs(); }, "");
    atb::addVarCB(bar, "Render mode",
                  [this](bool value) {
        m_bRenderMode = value;
        if(value) {
            m_GLVPLRenderer.setUp();
        } else {
            m_pRenderer->setUp();
        }
    }, [this]() -> bool { return m_bRenderMode; }, "");

    atb::addVarRW(bar, "Draw buffers", m_bDrawBuffers, "");
    atb::addVarRW(bar, "Render BSpheres", m_bRenderBSpheres, "");
    atb::addVarRW(bar, "Data to display", m_nDataToDisplay, "");
    atb::addVarRO(bar, "Selected Cluster Node", m_nSelectedClusterNode, "");
    atb::addButton(bar, "Compute reference", [this]() { computeReference(); }, "");

    atb::addVarRW(bar, "Reference spp", m_nRefSpp, "");
    atb::addVarRW(bar, "Intensity", m_fIntensity, "");
    atb::addVarRW(bar, "Path Depth", m_nPathDepth, "");

    atb::addVarRW(bar, "Draw Primary Light Shadow Map", m_bDrawPrimaryLightShadowMap, "");

    atb::addButton(bar, "Sample scene", [this]() { sampleScene(); }, "");

    atb::addVarRW(bar, "Display ISMs", m_bDisplayISMs, "");

    m_GLVPLRenderer.exposeIO(bar);
}

void ClusteredISMViewer::onTeardown() {
    atb::deleteBar(TwGetBarByName("VPL Skel Shadow Map Viewer"));
}

void ClusteredISMViewer::setWindowSize(size_t width, size_t height) {
    m_nWidth = width;
    m_nHeight = height;
}

void ClusteredISMViewer::handleClickEvent(int x, int y) {
    if(m_bRenderMode) {
        y = m_nHeight - y - 1;

        gloops::Viewport viewport(0.5 * (m_nWidth - m_pDevice->getFramebuffer().getWidth()),
                                  0.5 * (m_nHeight - m_pDevice->getFramebuffer().getHeight()),
                                  m_pDevice->getFramebuffer().getWidth(),
                                  m_pDevice->getFramebuffer().getHeight());

        if(m_bDrawBuffers) {
            viewport = gloops::Viewport(
                        0.5 * (m_nWidth - m_pDevice->getFramebuffer().getWidth()),
                        m_nHeight - m_pDevice->getFramebuffer().getHeight(),
                        m_pDevice->getFramebuffer().getWidth(),
                        m_pDevice->getFramebuffer().getHeight());
        }

        if(x < viewport.x || y < viewport.y || x >= viewport.x + viewport.width || y >= viewport.y + viewport.height) {
            return;
        }

        m_GLVPLRenderer.processPixel(x - viewport.x, y - viewport.y);
    }
}

void ClusteredISMViewer::handleNodeSelection(GraphNodeIndex node) {
    m_nSelectedClusterNode = node;
    if(m_pVisualDataViewer->isClusteringCurrentlyDisplayed()) {
        m_LightSourcePosition = m_pDevice->getScene().voxelSpace->getClustering().getNode(node).P;
    } else if(m_pVisualDataViewer->isSkeletonCurrentlyDisplayed()) {
        m_LightSourcePosition = m_pDevice->getScene().voxelSpace->getSkeleton().getNode(node).P;
    }
}

void ClusteredISMViewer::drawView() {
    if(m_bRenderMode) {
        m_GLVPLRenderer.setProjectionMatrix(computeGLProjectionMatrix(m_pDevice->getCamera()));
        m_GLVPLRenderer.setViewMatrix(computeGLViewMatrix(m_pDevice->getCamera()));

        m_GLVPLRenderer.geometryPass(*m_pGLScene);

        gloops::Viewport m_Viewport(
                                  0.5 * (m_nWidth - m_pDevice->getFramebuffer().getWidth()),
                                  0.5 * (m_nHeight - m_pDevice->getFramebuffer().getHeight()),
                                  m_pDevice->getFramebuffer().getWidth(),
                                  m_pDevice->getFramebuffer().getHeight());

        if(m_bDrawBuffers) {
            m_Viewport = gloops::Viewport(
                        0.5 * (m_nWidth - m_pDevice->getFramebuffer().getWidth()),
                        m_nHeight - m_pDevice->getFramebuffer().getHeight(),
                        m_pDevice->getFramebuffer().getWidth(),
                        m_pDevice->getFramebuffer().getHeight());

            float w = m_Viewport.width / 4.f;
            float h = m_Viewport.height / 4.f;

            float x = 0.5 * (m_nWidth - 5 * w);
            float y = m_Viewport.y - h;

            m_GLVPLRenderer.drawBuffers(x, y, w, h);
        }

        gloops::TextureObject texture;
        {
            gloops::TextureBind texBind(GL_TEXTURE_2D, 0, texture);
            texBind.texImage2D(0, GL_RGB16F, m_Viewport.width, m_Viewport.height,
                0, GL_RGB, GL_FLOAT, nullptr);
            texBind.setMinFilter(GL_NEAREST);
            texBind.setMagFilter(GL_NEAREST);
        }

        gloops::FramebufferObject fbo;
        {
            gloops::FramebufferBind fboBind(GL_DRAW_FRAMEBUFFER, fbo);

            fboBind.framebufferTexture2D(GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

            GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
            glDrawBuffers(1, drawBuffers);

            if(m_nDataToDisplay < 0) {
                m_GLVPLRenderer.shadingPass(0, 0, m_Viewport.width, m_Viewport.height, m_VPLs,
                                            m_pDevice->getScene(), m_pDevice->getCamera());
            } else {
                m_GLVPLRenderer.drawBuffer(0, 0, m_Viewport.width, m_Viewport.height, m_nDataToDisplay);
            }
        }

        m_GLVPLRenderer.computeTimings();

        // Final rendering on screen
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

        gloops::FramebufferBind fboBind(GL_READ_FRAMEBUFFER, fbo);
        glReadBuffer(GL_COLOR_ATTACHMENT0);

        glBlitFramebuffer(0, 0, m_Viewport.width, m_Viewport.height,
            m_Viewport.x, m_Viewport.y, m_Viewport.x + m_Viewport.width, m_Viewport.y + m_Viewport.height, GL_COLOR_BUFFER_BIT, GL_LINEAR);

        if(m_bDisplayISMs) {
            m_GLVPLRenderer.drawISMs(0, 0, 256, 256);
        }
    } else {
        m_pRenderer->setUp();

        GraphNodeIndex node = m_pVisualDataViewer->getSelectedNode();
        if(node != UNDEFINED_NODE) {
            if(m_pVisualDataViewer->isSkeletonCurrentlyDisplayed()) {
                node = m_pDevice->getScene().voxelSpace->getClustering().getNodeClusterIndex(node);
            }
        }

        for(const auto& vpl: m_VPLs.orientedVPLs) {
            Col3f color(0.f);
            if(m_pDevice->getScene().voxelSpace) {
                GraphNodeIndex node = m_pDevice->getScene().voxelSpace->getClustering().getNearestNode(vpl.P, vpl.N);
                if(node != UNDEFINED_NODE) {
                    color = m_pDevice->getSurfacePointNodeColor(SurfacePoint(vpl.P, vpl.N));
                }
            } else {
                color = vpl.L;
            }

//            float maxColor = embree::reduce_max(vpl.L);
//            float r = embree::sqrt(maxColor / 0.01);

//            m_pRenderer->renderCircle(vpl.P, r, color, 1.f);

            m_pRenderer->renderArrow(vpl.P, vpl.N,
                                     color,
                                     m_pDevice->getSurfacePointScale());
        }

        m_pRenderer->renderSphere(m_LightSourcePosition, 0.5f * m_pDevice->getSurfacePointScale(), Col3f(m_fIntensity));

        if(m_bRenderBSpheres) {
            m_GLVPLRenderer.setUp();

            m_GLVPLRenderer.setProjectionMatrix(computeGLProjectionMatrix(m_pDevice->getCamera()));
            m_GLVPLRenderer.setViewMatrix(computeGLViewMatrix(m_pDevice->getCamera()));

            m_GLVPLRenderer.geometryPass(*m_pGLScene);

            m_GLVPLRenderer.displayData(*m_pRenderer, m_pDevice->getScene());
        }

//            std::vector<glm::mat4> viewMatrices;

//            for(int i = 0; i < NB_CUBE_SM; ++i) {
//                viewMatrices.emplace_back(computeGLViewMatrix(m_pDevice->getCamera()));
//            }

//            m_CubeShadowMapRenderer.render(*m_pGLScene, m_CubeShadowMaps, viewMatrices.data(), viewMatrices.size());

//            /*
//            glm::mat4 viewMatrix = computeGLViewMatrix(m_pDevice->getCamera());
//            m_ShadowMapRenderer.render(*m_pGLScene, m_CubeShadowMaps, &viewMatrix, 1);
//    */

//            glViewport(0, 0, 4 * 128, 3 * 128);
//            m_CubeShadowMapDrawer.drawShadowMap(m_CubeShadowMaps, 0);
    }
}

}

