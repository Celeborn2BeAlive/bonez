#include "DeferredViewer.hpp"

#include "application/gui.hpp"

#include "bonez/renderer/instantradiosity/RandomVPLSampler.hpp"

#include "bonez/renderer/factories.hpp"

namespace BnZ {

DeferredViewer::DeferredViewer(const GLSLProgramBuilder& programBuilder):
    m_Device(nullptr),
    m_GLScene(nullptr),
    m_DeferredRenderer(programBuilder),
    m_bDisplayGBuffer(true) {
}

void DeferredViewer::init(Device* device, GLScene* glScene) {
    m_Device = device;
    m_GLScene = glScene;

    m_DeferredRenderer.setResolution(device->getFramebuffer().getWidth(), device->getFramebuffer().getHeight());
}

void DeferredViewer::onSetup() {
    TwBar* bar = TwNewBar("Deferred");
    atb::addVarRW(bar, "Display GBuffer", m_bDisplayGBuffer, "");
}

void DeferredViewer::onTeardown() {
    atb::deleteBar(TwGetBarByName("Deferred"));
}

void DeferredViewer::setWindowSize(size_t width, size_t height) {
    m_nWidth = width;
    m_nHeight = height;
}

void DeferredViewer::handleClickEvent(int x, int y) {

}

void DeferredViewer::drawView() {
    if(!m_Device || !m_GLScene) {
        std::cerr << "DeferredViewer::drawView - Not initialized" << std::endl;
        return;
    }

    m_DeferredRenderer.setProjectionMatrix(computeGLProjectionMatrix(m_Device->getCamera()));
    m_DeferredRenderer.setViewMatrix(computeGLViewMatrix(m_Device->getCamera()));

    gloops::Viewport viewport = gloops::getViewport();

    gloops::Viewport m_Viewport(0.5 * (m_nWidth - m_Device->getFramebuffer().getWidth()),
                              0.5 * (m_nHeight - m_Device->getFramebuffer().getHeight()),
                              m_Device->getFramebuffer().getWidth(),
                              m_Device->getFramebuffer().getHeight());

    glViewport(0, 0, m_Viewport.width, m_Viewport.height);

    m_DeferredRenderer.geometryPass(*m_GLScene);

    if(m_bDisplayGBuffer) {
        gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

        glClear(GL_COLOR_BUFFER_BIT);

        m_DeferredRenderer.getGBuffer().bindForReading();
        GLsizei halfWidth = m_Viewport.width / 2.f;
        GLsizei halfHeight = m_Viewport.height / 2.f;

        m_DeferredRenderer.getGBuffer().setReadBuffer(GBuffer::TEX_NORMAL_DEPTH);
        glBlitFramebuffer(0, 0, m_Viewport.width, m_Viewport.height,
                          m_Viewport.x, m_Viewport.y + halfHeight,
                          m_Viewport.x + halfWidth, m_Viewport.y + m_Viewport.height,
                          GL_COLOR_BUFFER_BIT, GL_LINEAR);

        m_DeferredRenderer.getGBuffer().setReadBuffer(GBuffer::TEX_DIFFUSE);
        glBlitFramebuffer(0, 0, m_Viewport.width, m_Viewport.height,
                          m_Viewport.x + halfWidth, m_Viewport.y + halfHeight,
                          m_Viewport.x + m_Viewport.width, m_Viewport.y + m_Viewport.height,
                          GL_COLOR_BUFFER_BIT, GL_LINEAR);

        m_DeferredRenderer.getGBuffer().setReadBuffer(GBuffer::TEX_GLOSSY_SHININESS);
        glBlitFramebuffer(0, 0, m_Viewport.width, m_Viewport.height,
                          m_Viewport.x + halfWidth, m_Viewport.y,
                          m_Viewport.x + m_Viewport.width, m_Viewport.y + halfHeight,
                          GL_COLOR_BUFFER_BIT, GL_LINEAR);

    } else {
        if(m_VPLs.empty()) {
            /*
            RandomVPLSampler vplSampler(4, 4, embree::inf, 512, true, nullptr);

            ParamSet stats;
            m_VPLs = vplSampler.sample(m_Device->getCamera(), m_Device->getScene(),
                                               m_Device->getFramebuffer(), 0, stats);
            */

            ParamSet params("VPLSampler",
                            "type", "RandomVPLSampler",
                            "minDepth", 6,
                            "maxDepth", 6,
                            "vplCount", 512,
                            "indirectOnly", true,
                            "vplFilter", ParamSet("vplFilter",
                                                  "type", "GeorgievVPLFilter",
                                                  "viewRayCountX", 8,
                                                  "viewRayCountY", 8,
                                                  "epsilon", 0.00000001,
                                                  "integratorSampler", ParamSet("sampler",
                                                                                "type", "StratifiedSampler",
                                                                                "sppX", 256,
                                                                                "sppY", 128),
                                                  "integrator", ParamSet("integrator",
                                                                         "type", "PathtraceIntegrator",
                                                                         "maxDepth", 8,
                                                                         "indirectOnly", true),
                                                  "useFiltering", true,
                                                  "useResampling", false,
                                                  "vplCount", 1024)
                            );
            VPLSamplerPtr sampler = createVPLSampler(params);
            ParamSet stats;
            m_VPLs = sampler->sample(m_Device->getCamera(), m_Device->getScene(),
                                     m_Device->getFramebuffer(), 0, stats);
        }

        gloops::TextureObject texture;
        {
            gloops::TextureBind texBind(GL_TEXTURE_2D, 0, texture);
            texBind.texImage2D(0, GL_RGB16F, m_Viewport.width, m_Viewport.height,
                0, GL_RGB, GL_FLOAT, nullptr);
            texBind.setMinFilter(GL_NEAREST);
            texBind.setMagFilter(GL_NEAREST);
        }

        gloops::FramebufferObject fbo;
        {
            gloops::FramebufferBind fboBind(GL_DRAW_FRAMEBUFFER, fbo);

            fboBind.framebufferTexture2D(GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

            GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
            glDrawBuffers(1, drawBuffers);

            m_DeferredRenderer.shadingPass(0, 0, m_Viewport.width, m_Viewport.height, m_VPLs);
        }

        gloops::FramebufferBind fboBind(GL_READ_FRAMEBUFFER, fbo);
        glReadBuffer(GL_COLOR_ATTACHMENT0);

        glBlitFramebuffer(0, 0, m_Viewport.width, m_Viewport.height,
            m_Viewport.x, m_Viewport.y, m_Viewport.x + m_Viewport.width, m_Viewport.y + m_Viewport.height, GL_COLOR_BUFFER_BIT, GL_LINEAR);
    }

    gloops::setViewport(viewport);
}

}
