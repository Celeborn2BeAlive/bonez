#include "PointSelector.hpp"
#include "application/imgui/imgui.h"

namespace BnZ {

PointSelector::PointSelector():
    m_Intersections(MAX_COUNT),
    m_Enabled(MAX_COUNT, false),
    m_Selected(MAX_COUNT, false),
    m_nSelectedForChange(0),
    m_bAutoChange(false),
    m_bDisplayGUI(false),
    m_bDisplayEnablingGUI(false),
    m_bDisplayChangeGUI(false),
    m_bDisplaySelectionGUI(false) {
}

void PointSelector::setupGUI() {

}

void PointSelector::teardownGUI() {

}

void PointSelector::setPoint(const Intersection& I) {
    m_Intersections[m_nSelectedForChange] = I;
    m_Enabled[m_nSelectedForChange] = true;
    if(m_bAutoChange) {
        m_nSelectedForChange = (m_nSelectedForChange + 1) % size();
    }
}

}
