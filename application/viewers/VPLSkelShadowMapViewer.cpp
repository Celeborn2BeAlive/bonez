#include "VPLSkelShadowMapViewer.hpp"

#include "application/gui.hpp"

#include "bonez/common/Progress.hpp"

#include "application/parsers/JsonParser.hpp"
#include "bonez/renderer/factories.hpp"

#include "VisualDataViewer.hpp"

#include "bonez/renderer/VPLRenderer.hpp"
#include "application/glfw/GLFWApplication.hpp"

namespace BnZ {

VPLSkelShadowMapViewer::VPLSkelShadowMapViewer():
    m_pRenderer(nullptr),
    m_pGLScene(nullptr),
    m_pDevice(nullptr),
    m_pVisualDataViewer(nullptr),
    m_nVPLCountToGenerate(512),
    m_bRenderMode(false),
    m_nDataToDisplay(-1),
    m_bDrawBuffers(true),
    m_bRenderBSpheres(false),
    m_bDrawClusterNodeVisibility(false) {
}

static const int NB_CUBE_SM = 1;

void VPLSkelShadowMapViewer::init(
        Device& device,
        GLVisualDataRenderer& renderer, GLScene& glScene,
        VisualDataViewer& visualDataViewer) {
    m_pDevice = &device;
    m_pGLScene = &glScene;
    m_pRenderer = &renderer;
    m_pVisualDataViewer = &visualDataViewer;

    m_GLVPLRenderer.setResolution(m_pDevice->getFramebuffer().getWidth(),
                                  m_pDevice->getFramebuffer().getHeight());

    m_GLVPLRenderer.sendClusteredSkeletonData(m_pDevice->getScene(), *m_pGLScene);

    m_ShadowMaps.init(512, 512, 1);
    m_CubeShadowMaps.init(512, NB_CUBE_SM);
}

void VPLSkelShadowMapViewer::generateVPLs() {
    ParamSet params("VPLSampler",
                    "type", "RandomVPLSampler",
                    "minDepth", 2,
                    "maxDepth", 2,
                    "vplCount", m_nVPLCountToGenerate,
                    "indirectOnly", true,
                    "vplFilter", ParamSet("vplFilter",
                                          "type", "GeorgievVPLFilter",
                                          "viewRayCountX", 8,
                                          "viewRayCountY", 8,
                                          "epsilon", 0.00000001,
                                          "integratorSampler", ParamSet("sampler",
                                                                        "type", "StratifiedSampler",
                                                                        "sppX", 256,
                                                                        "sppY", 128),
                                          "integrator", ParamSet("integrator",
                                                                 "type", "PathtraceIntegrator",
                                                                 "maxDepth", 8,
                                                                 "indirectOnly", true),
                                          "useFiltering", true,
                                          "useResampling", false,
                                          "vplCount", 1024)
                    );
    VPLSamplerPtr sampler = createVPLSampler(params);
    ParamSet stats;
    m_VPLs = sampler->sample(m_pDevice->getCamera(), m_pDevice->getScene(),
                                     m_pDevice->getFramebuffer(), 0, stats);
}

void VPLSkelShadowMapViewer::computeReference() {
    VPLRenderer renderer;
    renderer.setVPLs(m_VPLs);
    renderer.render(m_pDevice->getCamera(),
                    m_pDevice->getScene(),
                    m_pDevice->getFramebuffer(),
                    true, m_GLVPLRenderer.m_bRenderIrradiance, false, 0.f, 1);
    GLFWApplication::getInstance()->setResultViewer();
}

void VPLSkelShadowMapViewer::onSetup() {
    TwBar* bar = TwNewBar("VPL Skel Shadow Map Viewer");

    atb::addVarRW(bar, "VPL Count", m_nVPLCountToGenerate, "");
    atb::addButton(bar, "Generate VPLs", [this]() { generateVPLs(); }, "");
    atb::addVarCB(bar, "Render mode",
                  [this](bool value) {
        m_bRenderMode = value;
        if(value) {
            m_GLVPLRenderer.setUp();
        } else {
            m_pRenderer->setUp();
        }
    }, [this]() -> bool { return m_bRenderMode; }, "");

    atb::addVarRW(bar, "Draw cluster node visibility", m_bDrawClusterNodeVisibility, "");

    atb::addVarRW(bar, "Render irradiance", m_GLVPLRenderer.m_bRenderIrradiance, "");
    atb::addVarRW(bar, "Display cost", m_GLVPLRenderer.m_bDisplayCostPerPixel, "");
    atb::addVarRW(bar, "Draw buffers", m_bDrawBuffers, "");
    atb::addVarRW(bar, "Render BSpheres", m_bRenderBSpheres, "");
    atb::addVarRW(bar, "Data to display", m_nDataToDisplay, "");
    atb::addVarRO(bar, "Selected Cluster Node", m_GLVPLRenderer.m_nSelectedClusterNode, "");
    atb::addButton(bar, "Compute reference", [this]() { computeReference(); }, "");

    TwAddSeparator(bar, "VPLSkelShadowMapViewerBar1", "");

    atb::addVarRO(bar, "Send Clustered Skeleton Data Pass", m_Timings.sendClusteredSkeletonDataPass, "");
    atb::addVarRO(bar, "Geometry Pass", m_Timings.geometryPass, "");
    atb::addVarRO(bar, "Find Unique Cluster Pass", m_Timings.findUniqueClustersPass, "");
    atb::addVarRO(bar, "Count Unique Cluster Pass", m_Timings.countUniqueClustersPass, "");
    atb::addVarRO(bar, "Light Assignment Pass", m_Timings.lightAssignementPass, "");
    atb::addVarRO(bar, "Transform VPL Pass", m_Timings.lightTransformPass, "");
    atb::addVarRO(bar, "Shading Pass", m_Timings.shadingPass, "");
    atb::addVarRO(bar, "Total time", m_Timings.totalTime, "");
}

void VPLSkelShadowMapViewer::onTeardown() {
    atb::deleteBar(TwGetBarByName("VPL Skel Shadow Map Viewer"));
}

void VPLSkelShadowMapViewer::setWindowSize(size_t width, size_t height) {
    m_nWidth = width;
    m_nHeight = height;
}

void VPLSkelShadowMapViewer::handleClickEvent(int x, int y) {

}

void VPLSkelShadowMapViewer::handleNodeSelection(GraphNodeIndex node) {
    m_GLVPLRenderer.m_nSelectedClusterNode = node;
}

void VPLSkelShadowMapViewer::drawView() {
    if(m_bRenderMode) {
        m_GLVPLRenderer.setProjectionMatrix(computeGLProjectionMatrix(m_pDevice->getCamera()));
        m_GLVPLRenderer.setViewMatrix(computeGLViewMatrix(m_pDevice->getCamera()));

        m_GLVPLRenderer.geometryPass(*m_pGLScene);

        gloops::Viewport m_Viewport(
                                  0.5 * (m_nWidth - m_pDevice->getFramebuffer().getWidth()),
                                  0.5 * (m_nHeight - m_pDevice->getFramebuffer().getHeight()),
                                  m_pDevice->getFramebuffer().getWidth(),
                                  m_pDevice->getFramebuffer().getHeight());

        if(m_bDrawBuffers) {
            m_Viewport = gloops::Viewport(
                        0.5 * (m_nWidth - m_pDevice->getFramebuffer().getWidth()),
                        m_nHeight - m_pDevice->getFramebuffer().getHeight(),
                        m_pDevice->getFramebuffer().getWidth(),
                        m_pDevice->getFramebuffer().getHeight());

            float w = m_Viewport.width / 4.f;
            float h = m_Viewport.height / 4.f;

            float x = 0.5 * (m_nWidth - 5 * w);
            float y = m_Viewport.y - h;

            m_GLVPLRenderer.drawBuffers(x, y, w, h);
        }

        gloops::TextureObject texture;
        {
            gloops::TextureBind texBind(GL_TEXTURE_2D, 0, texture);
            texBind.texImage2D(0, GL_RGB16F, m_Viewport.width, m_Viewport.height,
                0, GL_RGB, GL_FLOAT, nullptr);
            texBind.setMinFilter(GL_NEAREST);
            texBind.setMagFilter(GL_NEAREST);
        }

        gloops::FramebufferObject fbo;
        {
            gloops::FramebufferBind fboBind(GL_DRAW_FRAMEBUFFER, fbo);

            fboBind.framebufferTexture2D(GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

            GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
            glDrawBuffers(1, drawBuffers);

            if(m_nDataToDisplay < 0) {

                if(m_bDrawClusterNodeVisibility) {
                    std::cerr << "Position of node = " <<
                                 m_pDevice->getScene().voxelSpace->getClustering().getNode(m_GLVPLRenderer.m_nSelectedClusterNode).P << std::endl;
                    m_GLVPLRenderer.drawClusterNodeVisibilityPass(0, 0, m_Viewport.width, m_Viewport.height,
                                                m_pDevice->getScene(), m_GLVPLRenderer.m_nSelectedClusterNode);
                } else {
                    m_GLVPLRenderer.shadingPass(0, 0, m_Viewport.width, m_Viewport.height, m_VPLs,
                                                      m_pDevice->getScene());
                }
            } else {
                m_GLVPLRenderer.drawBuffer(0, 0, m_Viewport.width, m_Viewport.height, m_nDataToDisplay);
            }
        }

        m_GLVPLRenderer.getTimings(m_Timings);
        m_Timings.updateTotalTime();

        gloops::FramebufferBind fboBind(GL_READ_FRAMEBUFFER, fbo);
        glReadBuffer(GL_COLOR_ATTACHMENT0);

        glBlitFramebuffer(0, 0, m_Viewport.width, m_Viewport.height,
            m_Viewport.x, m_Viewport.y, m_Viewport.x + m_Viewport.width, m_Viewport.y + m_Viewport.height, GL_COLOR_BUFFER_BIT, GL_LINEAR);

    } else {
        m_pRenderer->setUp();

        GraphNodeIndex node = m_pVisualDataViewer->getSelectedNode();
        if(node != UNDEFINED_NODE) {
            m_pRenderer->renderGridArround(m_pVisualDataViewer->getCurrentlyDisplayedSkeleton()->getNode(node).P, 2,
                                          m_pDevice->getScene(), m_pVisualDataViewer->getCurrentlyDisplayedSkeleton());

            if(m_pVisualDataViewer->isSkeletonCurrentlyDisplayed()) {
                node = m_pDevice->getScene().voxelSpace->getClustering().getNodeClusterIndex(node);
            }
        }

        for(const auto& vpl: m_VPLs.orientedVPLs) {
            Col3f color(0.f);
            GraphNodeIndex node = m_pDevice->getScene().voxelSpace->getClustering().getNearestNode(vpl.P, vpl.N);
            if(node != UNDEFINED_NODE) {
                color = m_pDevice->getSurfacePointNodeColor(SurfacePoint(vpl.P, vpl.N));
            }

//            float maxColor = embree::reduce_max(vpl.L);
//            float r = embree::sqrt(maxColor / 0.01);

//            m_pRenderer->renderCircle(vpl.P, r, color, 1.f);

            m_pRenderer->renderArrow(vpl.P, vpl.N,
                                     color,
                                     m_pDevice->getSurfacePointScale());
        }


        if(m_bRenderBSpheres) {
            m_GLVPLRenderer.setUp();

            m_GLVPLRenderer.setProjectionMatrix(computeGLProjectionMatrix(m_pDevice->getCamera()));
            m_GLVPLRenderer.setViewMatrix(computeGLViewMatrix(m_pDevice->getCamera()));

            m_GLVPLRenderer.geometryPass(*m_pGLScene);

            m_GLVPLRenderer.displayGClusters(*m_pRenderer, m_pDevice->getScene());
        }

//        if(m_bRenderNodeShadowMap) {
//            m_GLVPLRenderer.drawClusterNodeShadowMap(0, 0, 4 * 128, 3 * 128, m_GLVPLRenderer.m_nSelectedClusterNode);
//        }


//        if(m_GLVPLRenderer.m_nSelectedClusterNode != UNDEFINED_NODE) {
//            m_GLVPLRenderer.drawClusterNodeShadowMap(0, 0, 4 * 128, 3 * 128, m_GLVPLRenderer.m_nSelectedClusterNode);
//        } else {
//            std::vector<glm::mat4> viewMatrices;

//            for(int i = 0; i < NB_CUBE_SM; ++i) {
//                viewMatrices.emplace_back(computeGLViewMatrix(m_pDevice->getCamera()));
//            }

//            m_CubeShadowMapRenderer.render(*m_pGLScene, m_CubeShadowMaps, viewMatrices.data(), viewMatrices.size());

//            /*
//            glm::mat4 viewMatrix = computeGLViewMatrix(m_pDevice->getCamera());
//            m_ShadowMapRenderer.render(*m_pGLScene, m_CubeShadowMaps, &viewMatrix, 1);
//    */

//            glViewport(0, 0, 4 * 128, 3 * 128);
//            m_CubeShadowMapDrawer.drawShadowMap(m_CubeShadowMaps, 0);


//            /*
//            glm::mat4 viewMatrix = computeGLViewMatrix(m_pDevice->getCamera());
//            m_ShadowMapRenderer.render(*m_pGLScene, m_ShadowMaps, &viewMatrix, 1);

//            glViewport(0, 0, 512, 512);
//            m_ShadowMapDrawer.drawShadowMap(m_ShadowMaps, 0);*/
//        }
    }
}

}

