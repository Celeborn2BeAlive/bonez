#ifndef _BONEZ_DEFERREDVIEWER_HPP
#define _BONEZ_DEFERREDVIEWER_HPP

#include "Viewer.hpp"

#include "application/Device.hpp"

#include "bonez/opengl/scene/GLScene.hpp"
#include "bonez/opengl/deferred/GLDeferredRenderer.hpp"

namespace BnZ {

class DeferredViewer: public Viewer {
public:
    DeferredViewer(const GLSLProgramBuilder& programBuilder);

    void init(Device* device, GLScene* glScene);

    virtual void onSetup();

    virtual void onTeardown();

    virtual void setWindowSize(size_t width, size_t height);

    virtual void handleClickEvent(int x, int y);

    virtual void drawView();

private:
    size_t m_nWidth, m_nHeight;

    Device* m_Device;
    GLScene* m_GLScene;

    GLDeferredRenderer m_DeferredRenderer;

    bool m_bDisplayGBuffer;

    VPLContainer m_VPLs;
};

}

#endif // _BONEZ_DEFERREDVIEWER_HPP
