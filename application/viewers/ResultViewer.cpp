#include "ResultViewer.hpp"

#include "application/gui.hpp"

#include "bonez/renderer/postprocessing/GammaToneMapping.hpp"

namespace BnZ {

ResultViewer::ResultViewer(float gamma):
    m_Device(nullptr), m_fGamma(gamma),
    m_bInteractive(false), m_Renderer() {
}

void ResultViewer::init(Device* device) {
    m_Device = device;
}

void ResultViewer::setInteractive(bool value) {
    m_bInteractive = value;
}

void ResultViewer::onSetup() {
    TwBar* bar = TwNewBar("Result");

    atb::addVarRW(bar, "Gamma", m_fGamma, " min=0.1 step=0.1 precision=1 ");
    atb::addButton(bar, "Save", [this]() { m_Device->saveResult(m_fGamma); }, "");
}

void ResultViewer::onTeardown() {
    atb::deleteBar(TwGetBarByName("Result"));
}

void ResultViewer::setWindowSize(size_t width, size_t height) {
    m_nWidth = width;
    m_nHeight = height;
}

void ResultViewer::handleClickEvent(int x, int y) {
    if(!m_Device) {
        std::cerr << "ResultViewer::handleClickEvent - Not initialized" << std::endl;
        return;
    }

    gloops::Viewport viewport(0.5 * (m_nWidth - m_Device->getFramebuffer().getWidth()),
                              0.5 * (m_nHeight - m_Device->getFramebuffer().getHeight()),
                              m_Device->getFramebuffer().getWidth(),
                              m_Device->getFramebuffer().getHeight());

    if(x < viewport.x || y < viewport.y || x >= viewport.x + viewport.width || y >= viewport.y + viewport.height) {
        return;
    }

    std::clog << "Value = " << m_Device->getFramebuffer()(x - viewport.x, y - viewport.y) << std::endl;
}

void ResultViewer::drawView() {
    if(!m_Device) {
        std::cerr << "ResultViewer::drawView - Not initialized" << std::endl;
        return;
    }

    if(m_bInteractive) {
        m_Device->render(m_bInteractive);
    }

    gloops::Viewport viewport(0.5 * (m_nWidth - m_Device->getFramebuffer().getWidth()),
                              0.5 * (m_nHeight - m_Device->getFramebuffer().getHeight()),
                              m_Device->getFramebuffer().getWidth(),
                              m_Device->getFramebuffer().getHeight());

    m_Renderer.drawResult(viewport, m_fGamma, normalize(m_Device->getFramebuffer()));
}

}
