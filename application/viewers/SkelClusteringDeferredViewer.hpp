#ifndef _BONEZ_SKELCLUSTERINGDEFERREDVIEWER_HPP
#define _BONEZ_SKELCLUSTERINGDEFERREDVIEWER_HPP

#include "bonez/opengl/analysis/GLVisualDataRenderer.hpp"
#include "Viewer.hpp"
#include "application/Device.hpp"
#include "bonez/scene/topology/VisibilityClustering.hpp"

#include "bonez/opengl/lightgates/GLSkelDeferredRenderer.hpp"

namespace BnZ {

class VisualDataViewer;

class SkelClusteringDeferredViewer: public Viewer {
public:
    SkelClusteringDeferredViewer();

    void init(Device& device,
              GLVisualDataRenderer& renderer,
              GLScene& glScene,
              VisualDataViewer& visualDataViewer,
              VisibilityClustering& visibilityClustering);

    virtual void onSetup();

    virtual void onTeardown();

    virtual void setWindowSize(size_t width, size_t height);

    virtual void handleClickEvent(int x, int y);

    virtual void drawView();

    bool isRenderMode() const {
        return m_bRenderMode;
    }

private:
    void generateVPLs();
    void computeReference();

    GLVisualDataRenderer* m_pRenderer;
    GLScene* m_pGLScene;
    Device* m_pDevice;
    VisualDataViewer* m_pVisualDataViewer;
    VisibilityClustering* m_pVisibilityClustering;

    uint32_t m_nVPLCountToGenerate;
    VPLContainer m_VPLs;
    // For each gate of each cluster, record an estimation of the incident power on this gate
    std::vector<std::vector<Col3f>> m_GatePower;
    std::vector<std::vector<uint32_t>> m_ClusterVPLs;

    static void setRenderModeCallback(const void* value, void* clientData);

    static void getRenderModeCallback(void* value, void* userData);

    bool m_bRenderMode;
    GLSkelDeferredRenderer m_GLSkelDeferredRenderer;

    size_t m_nWidth, m_nHeight;
};

}

#endif // _BONEZ_GateLightingViewer_HPP
