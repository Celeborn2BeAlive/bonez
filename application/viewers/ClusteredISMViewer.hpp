#ifndef _BONEZ_CLUSTEREDISMVIEWER_HPP
#define _BONEZ_CLUSTEREDISMVIEWER_HPP

#include "Viewer.hpp"
#include "bonez/opengl/analysis/GLVisualDataRenderer.hpp"
#include "application/Device.hpp"

#include "opengl/deferred/GLVPLDeferredRenderer.hpp"

#include "opengl/GLShadowMapRenderer.hpp"
#include "opengl/GLISMRenderer.hpp"
#include "opengl/GLClusteredISMRenderer.hpp"
#include "opengl/GLClusteredISMRenderer2.hpp"
#include "opengl/icism/GLIrradianceCacheISMRenderer.hpp"
#include "opengl/icism/GLIrradianceCacheISMRenderer2.hpp"

#include "opengl/GLISMRenderer.hpp"

namespace BnZ {

class VisualDataViewer;

class ClusteredISMViewer: public Viewer {
public:
    ClusteredISMViewer();

    void init(Device& device,
              GLVisualDataRenderer& renderer,
              GLScene& glScene,
              VisualDataViewer& visualDataViewer);

    virtual void onSetup();

    virtual void onTeardown();

    virtual void setWindowSize(size_t width, size_t height);

    virtual void handleClickEvent(int x, int y);

    virtual void handleNodeSelection(GraphNodeIndex node);

    virtual void drawView();

    bool isRenderMode() const {
        return m_bRenderMode;
    }

private:
    GLVisualDataRenderer* m_pRenderer;
    GLScene* m_pGLScene;
    Device* m_pDevice;
    VisualDataViewer* m_pVisualDataViewer;

    void generateVPLs();

    void computeReference();

    void sampleScene();

    uint32_t m_nVPLCountToGenerate;
    VPLContainer m_VPLs;

    bool m_bRenderMode;

    size_t m_nWidth, m_nHeight;

    int m_nDataToDisplay;
    bool m_bDrawBuffers = false;
    bool m_bRenderBSpheres;
    float m_fIntensity;
    uint32_t m_nPathDepth;
    int m_nRefSpp;
    bool m_bDrawPrimaryLightShadowMap;

    bool m_bDisplayISMs;

    Vec3f m_LightSourcePosition = Vec3f(0.f);

    GraphNodeIndex m_nSelectedClusterNode;

    GLIrradianceCacheISMRenderer2 m_GLVPLRenderer;

    GLShadowMapContainer m_ShadowMaps;
    GLShadowMapDrawer m_ShadowMapDrawer;
    GLOmniShadowMapRenderer m_ShadowMapRenderer;

    GLCubeShadowMapContainer m_CubeShadowMaps;
    GLCubeShadowMapRenderer m_CubeShadowMapRenderer;
    GLCubeShadowMapDrawer m_CubeShadowMapDrawer;

    GLISMDrawer m_ISMDrawer;

    GLPoints m_SampledScene;
};

}

#endif // _BONEZ_VPLSKELSHADOWMAPVIEWER_HPP
