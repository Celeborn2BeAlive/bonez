#ifndef _BONEZ_VISUALDATAVIEWER_HPP
#define _BONEZ_VISUALDATAVIEWER_HPP

#include "Viewer.hpp"

#include "application/Device.hpp"

#include "bonez/opengl/scene/GLScene.hpp"
#include "bonez/opengl/analysis/GLVisualDataRenderer.hpp"
#include "bonez/opengl/analysis/visual_data.hpp"

#include "bonez/common/data/KdTree.hpp"

#include "bonez/common/algorithms/DijkstraAlgorithm.hpp"

#include "GateLightingViewer.hpp"
#include "SkelClusteringDeferredViewer.hpp"
#include "VisibilityApproximationViewer.hpp"
#include "VPLSkelShadowMapViewer.hpp"
#include "ClusteredISMViewer.hpp"

#include "PointSelector.hpp"

namespace BnZ {

class VisualDataViewer: public Viewer {
public:
    VisualDataViewer(const GLSLProgramBuilder& programBuilder);

    void init(Device* device, GLScene* glScene);

    virtual void onSetup();

    virtual void onTeardown();

    virtual void setWindowSize(size_t width, size_t height);

    virtual void handleClickEvent(int x, int y);

    virtual void drawView();

    GraphNodeIndex getSelectedNode() const {
        return m_SelectedNode;
    }

    bool isSkeletonCurrentlyDisplayed() const;

    bool isClusteringCurrentlyDisplayed() const;

    const Skeleton* getCurrentlyDisplayedSkeleton() {
        return m_pCurrentlyDisplayedSkeleton;
    }

public:
    size_t m_nWidth, m_nHeight;

    Device* m_Device;
    GLScene* m_GLScene;
    GLGrid m_GLGrid;

    bool m_bDrawScene;
    bool m_bDrawGrid;
    bool m_bUseClusterGridColors;

    GLVisualDataRenderer m_VisualDataRenderer;
    //VPLSkelShadowMapViewer m_Viewer;
    GateLightingViewer m_Viewer;
    //ClusteredISMViewer m_Viewer;

    VPLContainer m_VPLContainer;
    KdTree m_VPLKdTree;

    PointSelector m_PointSelector;

    // Skeleton view:
    void drawSkeleton();

    void setSkeleton(const Skeleton& skeleton);

    void recomputeColors();

    void drawLink(const SurfacePoint& P);

    void drawLink(const SurfacePoint& P, const Col3f& color);

    void drawNeighbouring(const SurfacePoint& P);

    void drawSurfacePointSkeletonData(const SurfacePoint& point);

    enum SkeletonType {
        CLUSTERING, SKELETON
    };
    SkeletonType m_CurrentSkeletonType;
    const Skeleton* m_pCurrentlyDisplayedSkeleton;

    bool m_bDisplayMaxball;
    bool m_bDisplayNodes;

    static void setColorDataTypeCallback(const void* value, void* userData);

    static void getColorDataTypeCallback(void* value, void* userData);

    enum ColorDataType {
        RANDOM_COLORS,
        CLUSTERING_COLORS,
        MAXBALL_COLORS,
        LOCAL_MAXBALL_EXTREMAS,
        MAXBALL_PER_LINE_COLORS,
        MAXBALL_VARIANCE_PER_LINE_COLORS,
        CURVATURE_COLORS,
        VISIBILITY_CLUSTER,
        LINE_MAXBALL_EXTREMAS,
        MEYER_WATERSHED,
        VISIBILITY_CLUSTERING_TEST,
        COLOR_COUNT
    };
    ColorDataType m_ColorDataType;

    enum SurfacePointDataType {
        NEAREST_NODE,
        NEIGHBOURING,
        SURFACE_POINT_DATACOUNT
    };
    SurfacePointDataType m_SurfacePointDataType;

    static void selectBiggestNodeCallback(void* userData);

    GraphNodeIndex m_SelectedNode = 53;
    Vec3f m_SelectedNodePosition;

    VisibilityClustering m_VisibilityClustering;

    static void fmmClusterGridCallback(void* userData);

    void computeFMMClusterGrid();
};

}

#endif // _BONEZ_VISUALDATAVIEWER_HPP
