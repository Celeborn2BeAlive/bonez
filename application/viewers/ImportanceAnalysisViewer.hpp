#ifndef _BONEZ_IMPORTANCEANALYSISVIEWER_HPP
#define _BONEZ_IMPORTANCEANALYSISVIEWER_HPP

#include "Viewer.hpp"

#include "application/Device.hpp"

#include "bonez/opengl/scene/GLScene.hpp"
#include "bonez/opengl/analysis/GLVisualDataRenderer.hpp"
#include "bonez/opengl/analysis/visual_data.hpp"
#include "bonez/opengl/GLResultRenderer.hpp"

#include "bonez/renderer/PointHemisphereRenderer.hpp"

namespace BnZ {

class ImportanceAnalysisViewer: public Viewer {
public:
    ImportanceAnalysisViewer(const GLSLProgramBuilder& programBuilder);

    void init(Device* device, GLScene* glScene, const IntegratorPtr& pIntegrator);

    virtual void onSetup();

    virtual void onTeardown();

    virtual void setWindowSize(size_t width, size_t height);

    virtual void handleClickEvent(int x, int y);

    virtual void drawGUI();

    virtual void drawView();

private:
    gloops::Viewport m_MainViewport;

    size_t m_nWidth, m_nHeight;

    Device* m_Device;
    GLScene* m_GLScene;

    GLVisualDataRenderer m_VisualDataRenderer;

    // Visual data
    enum SkelDataType {
        NONE, SKELETON, CLUSTERING
    };
    SkelDataType m_CurrentDisplayedSkelDataType;
    //GLGraphVisualDataPtr m_SkeletonVisualData, m_SkelClusteringVisualData;

    Intersection m_PickedIntersection;

    bool m_bDisplayMaxball;

    IntegratorPtr m_pIntegrator;
    PointHemisphereRenderer m_PointHemisphereRenderer;

    GLResultRenderer m_ResultRenderer;
};

}

#endif // _BONEZ_IMPORTANCEANALYSISVIEWER_HPP
