#ifndef _BONEZ_RESULTVIEWER_HPP
#define _BONEZ_RESULTVIEWER_HPP

#include "Viewer.hpp"
#include "bonez/renderer/Framebuffer.hpp"
#include "application/Device.hpp"
#include "application/ResultManager.hpp"
#include "bonez/opengl/shaders/GLSLProgramBuilder.hpp"
#include "bonez/opengl/GLResultRenderer.hpp"

#include <gloops/gloops.hpp>

namespace BnZ {

class ResultViewer: public Viewer {
public:
    ResultViewer(float gamma);

    void init(Device* device);

    void setInteractive(bool value = true);

    void setGamma(float gamma) {
        m_fGamma = gamma;
    }

    virtual void onSetup();

    virtual void onTeardown();

    virtual void setWindowSize(size_t width, size_t height);

    virtual void handleClickEvent(int x, int y);

    virtual void drawView();

private:
    size_t m_nWidth, m_nHeight;

    Device* m_Device;

    float m_fGamma;
    bool m_bInteractive;

    GLResultRenderer m_Renderer;
};

}

#endif // _BONEZ_RESULTVIEWER_HPP
