#ifndef _BONEZ_VPLSKELSHADOWMAPVIEWER_HPP
#define _BONEZ_VPLSKELSHADOWMAPVIEWER_HPP

#include "Viewer.hpp"
#include "bonez/opengl/analysis/GLVisualDataRenderer.hpp"
#include "application/Device.hpp"

#include "opengl/deferred/GLVPLDeferredRenderer.hpp"

#include "opengl/GLShadowMapRenderer.hpp"
#include "opengl/GLSkelSMVPLRenderer.hpp"
#include "opengl/GLSkelSMVPLRenderer2.hpp"
#include "opengl/GLSkelSMVPLRenderer3.hpp"
#include "opengl/GLSkelSMAORenderer.hpp"

namespace BnZ {

class VisualDataViewer;

class VPLSkelShadowMapViewer: public Viewer {
public:
    VPLSkelShadowMapViewer();

    void init(Device& device,
              GLVisualDataRenderer& renderer,
              GLScene& glScene,
              VisualDataViewer& visualDataViewer);

    virtual void onSetup();

    virtual void onTeardown();

    virtual void setWindowSize(size_t width, size_t height);

    virtual void handleClickEvent(int x, int y);

    virtual void handleNodeSelection(GraphNodeIndex node);

    virtual void drawView();

    bool isRenderMode() const {
        return m_bRenderMode;
    }

private:
    GLVisualDataRenderer* m_pRenderer;
    GLScene* m_pGLScene;
    Device* m_pDevice;
    VisualDataViewer* m_pVisualDataViewer;

    void generateVPLs();

    void computeReference();

    uint32_t m_nVPLCountToGenerate;
    VPLContainer m_VPLs;

    bool m_bRenderMode;

    size_t m_nWidth, m_nHeight;

    int m_nDataToDisplay;
    bool m_bDrawBuffers;
    bool m_bRenderBSpheres;
    bool m_bDrawClusterNodeVisibility;

    GLSkelSMAORenderer m_GLVPLRenderer;
    GLSkelSMAORenderer::Timings m_Timings;

    GLShadowMapContainer m_ShadowMaps;
    GLShadowMapDrawer m_ShadowMapDrawer;
    GLOmniShadowMapRenderer m_ShadowMapRenderer;

    GLCubeShadowMapContainer m_CubeShadowMaps;
    GLCubeShadowMapRenderer m_CubeShadowMapRenderer;
    GLCubeShadowMapDrawer m_CubeShadowMapDrawer;
};

}

#endif // _BONEZ_VPLSKELSHADOWMAPVIEWER_HPP
