#ifndef _BONEZ_VISIBILITYAPPROXIMATIONVIEWER_HPP
#define _BONEZ_VISIBILITYAPPROXIMATIONVIEWER_HPP

/*
#include "Viewer.hpp"

#include "application/Device.hpp"

#include "bonez/opengl/scene/GLScene.hpp"
#include "bonez/opengl/analysis/GLVisualDataRenderer.hpp"
#include "bonez/opengl/analysis/visual_data.hpp"

#include "bonez/common/algorithms/DijkstraAlgorithm.hpp"

namespace BnZ {

class VisibilityApproximationViewer: public Viewer {
public:
    VisibilityApproximationViewer();

    void init(Device& device, SkeletonViewer& skeletonViewer,
              GLVisualDataRenderer& visualDataRenderer);

    virtual void onSetup();

    virtual void setWindowSize(size_t width, size_t height);

    virtual void handleClickEvent(int x, int y);

    virtual void drawGUI();

    virtual void drawView();

    void setSurfacePoint(const SurfacePoint& point);

    void exentricityDot();

    void analyseExcentricity();

private:
    void testGeodesicVsEuclideanDistance();

    void testGeodesicVsEuclideanDistanceOnPair();

    void sampleRandomVisible();

    void sampleRandom();

    void printDistances();

    void displayVisibilityApproximationColors();

    bool m_bIsActive;

    Device* m_pDevice;
    GLVisualDataRenderer* m_pRenderer;
    SkeletonViewer* m_pSkeletonViewer;

    CurvSkelClustering* m_pClustering;

    bool m_bHasSurfacePoint;
    SurfacePoint m_SurfacePoint;

    Random m_Rng;
    std::vector<DijkstraAlgorithm::ShortestPathVector> m_ShortestPaths;
    bool m_bHasOther;
    SurfacePoint m_Other;

    bool m_bSelectFirstPoint;
};

}
*/

#endif // _BONEZ_VISUALDATAVIEWER_HPP
