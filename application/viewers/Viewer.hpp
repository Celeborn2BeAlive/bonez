#ifndef _BONEZ_VIEWER_HPP
#define _BONEZ_VIEWER_HPP

#include <cstdlib>
#include <atb.hpp>

#include <embree/common/image/image.h>
#include "bonez/scene/topology/Skeleton.hpp"

namespace BnZ {

class Viewer {
public:
    virtual ~Viewer() {
    }

    virtual void onSetup() = 0;

    virtual void onTeardown() = 0;

    virtual void setWindowSize(size_t width, size_t height) = 0;

    virtual void handleClickEvent(int x, int y) = 0;

    virtual void handleNodeSelection(GraphNodeIndex node) {
    }

    virtual void drawView() = 0;
};

}

#endif // _BONEZ_VIEWER_HPP
