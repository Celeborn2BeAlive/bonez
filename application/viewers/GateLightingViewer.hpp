#ifndef _BONEZ_GATELIGHTINGVIEWER_HPP
#define _BONEZ_GATELIGHTINGVIEWER_HPP

#include "Viewer.hpp"
#include "bonez/opengl/analysis/GLVisualDataRenderer.hpp"
#include "application/Device.hpp"
#include "bonez/scene/topology/VisibilityClustering.hpp"

#include "bonez/opengl/lightgates/GLLightGateRenderer.hpp"
#include "bonez/opengl/lightgates/GLLightGateRenderer2.hpp"
#include "bonez/opengl/lightgates/GLLightGateRenderer3.hpp"
#include "bonez/opengl/lightgates/GLLightGateRenderer4.hpp"
#include "bonez/opengl/lightgates/GLLightGateRenderer5.hpp"
#include "bonez/opengl/lightgates/GLLightGateRenderer6.hpp"
#include "bonez/opengl/lightgates/GLLightGateRenderer7.hpp"
#include "bonez/opengl/lightgates/GLLightGateRenderer8.hpp"

namespace BnZ {

class VisualDataViewer;

class GateLightingViewer: public Viewer {
public:
    GateLightingViewer();

    void init(Device& device,
              GLVisualDataRenderer& renderer,
              GLScene& glScene,
              VisualDataViewer& visualDataViewer,
              VisibilityClustering& visibilityClustering);

    virtual void onSetup();

    virtual void onTeardown();

    virtual void setWindowSize(size_t width, size_t height);

    virtual void handleClickEvent(int x, int y);

    virtual void handleNodeSelection(GraphNodeIndex node);

    virtual void drawView();

    bool isRenderMode() const {
        return m_bRenderMode;
    }

private:
    GLVisualDataRenderer* m_pRenderer;
    GLScene* m_pGLScene;
    Device* m_pDevice;
    VisualDataViewer* m_pVisualDataViewer;
    VisibilityClustering* m_pVisibilityClustering;

    void generateVPLs();

    void computeReference();

    uint32_t m_nVPLCountToGenerate;
    VPLContainer m_VPLs;
    // For each gate of each cluster, record an estimation of the incident power on this gate
    std::vector<std::vector<Col3f>> m_GatePower;
    std::vector<std::vector<uint32_t>> m_ClusterVPLs;

    bool m_bRenderMode;
    GLLightGateRenderer7 m_GLLightGateRenderer;

    size_t m_nWidth, m_nHeight;

    int m_nDataToDisplay;

    bool m_bRenderBSpheres;
    bool m_bDrawBuffers;
    float m_fIntensity = 3e+06;
    uint32_t m_nPathDepth = 8;
    int m_nRefSpp;

    Vec3f m_LightSourcePosition;
};

}

#endif // _BONEZ_GateLightingViewer_HPP
