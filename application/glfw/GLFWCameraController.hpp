#ifndef _BONEZ_GLFWCAMERACONTROLLER_HPP
#define _BONEZ_GLFWCAMERACONTROLLER_HPP

#include "bonez/common/cameras/Camera.hpp"

namespace BnZ {

class GLFWCameraController {
public:
    GLFWCameraController():
        m_Camera(nullptr), m_AngleX(0), m_AngleY(0), m_Speed(3.f),
        m_LeftButtonPressed(false), m_LastX(0), m_LastY(0),
        m_bHasMoved(false) {
    }

    void setCamera(Camera& camera) {
        m_AngleX = 0;
        m_AngleY = 0;
        m_Camera = &camera;
    }

    void setSpeed(float speed) {
        m_Speed = speed;
    }

    float getSpeed() const {
        return m_Speed;
    }

    void lookAt(const embree::Vec3f& eye, const embree::Vec3f& point) {
        if(m_Camera) {
            m_AngleX = 0;
            m_AngleY = 0;
            m_Camera->lookAt(eye, point, embree::Vec3f(0.f, 1.f, 0.f));
        }
    }

    void startRotation(int cursorX, int cursorY) {
        m_LeftButtonPressed = true;
        m_LastX = cursorX;
        m_LastY = cursorY;
    }

    void stopRotation() {
        m_LeftButtonPressed = false;
    }

    void setCursorPosition(int cursorX, int cursorY) {
        if(m_LeftButtonPressed) {
            int dX = cursorX - m_LastX, // control the angle around the Y axis: m_AngleY
                dY = cursorY - m_LastY; // control the angle aroung the X axis: m_AngleX
            m_AngleX -= dY * 0.01; // Angle around X decrease when the mouse move bottom
            m_AngleY -= dX * 0.01; // Angle around Y decrease when the mouse move right
            m_LastX = cursorX;
            m_LastY = cursorY;
            Vec3f localViewVector(sin(m_AngleY) * cos(m_AngleX), sin(m_AngleX), cos(m_AngleY) * cos(m_AngleX));
            m_Camera->lookAt(m_Camera->getPosition(),
                            m_Camera->getPosition() + xfmVector(m_Camera->getLocalToWorldTransform(), localViewVector),
                            embree::Vec3f(0.f, 1.f, 0.f));
            m_AngleX = 0.f;
            m_AngleY = 0.f;
            m_bHasMoved = true;
        }
    }

    typedef uint32_t DirectionMask;

    static const DirectionMask FRONT = 1 << 0;
    static const DirectionMask BACK = 1 << 1;
    static const DirectionMask LEFT = 1 << 2;
    static const DirectionMask RIGHT = 1 << 3;
    static const DirectionMask UP = 1 << 4;
    static const DirectionMask DOWN = 1 << 5;

    void move(float elapsedTime, DirectionMask directions) {
        if(m_Camera) {
            static const Vec3f front(0.f, 0.f, 1.f); // Vector pointing in front of the camera
            static const Vec3f left(1.f, 0.f, 0.f); // Vector pointing at left of the camera
            static const Vec3f up(0, 1, 0);
            Vec3f localTranslationVector = embree::zero;

            if (directions & FRONT) {
                localTranslationVector += m_Speed * elapsedTime * front;
            }

            if (directions & BACK) {
                localTranslationVector -= m_Speed * elapsedTime * front;
            }

            if (directions & LEFT) {
                localTranslationVector += m_Speed * elapsedTime * left;
            }

            if (directions & RIGHT) {
                localTranslationVector -= m_Speed * elapsedTime * left;
            }

            if (directions & UP) {
                localTranslationVector += m_Speed * elapsedTime * up;
            }

            if (directions & DOWN) {
                localTranslationVector -= m_Speed * elapsedTime * up;
            }

            Vec3f position = m_Camera->getPosition() + xfmVector(m_Camera->getLocalToWorldTransform(), localTranslationVector);
            m_Camera->lookAt(position, position + xfmVector(m_Camera->getLocalToWorldTransform(), front), embree::Vec3f(0.f, 1.f, 0.f));

            if (localTranslationVector != Vec3f(embree::zero)) {
                m_bHasMoved = true;
            }
        }
    }

    void increaseSpeed(float delta) {
        m_Speed += delta;
        if(m_Speed < 0.f) {
            m_Speed = 0.f;
        }
    }

    float getCameraSpeed() const {
        return m_Speed;
    }

    // Return the flag to control if the camera has moved. After the call, the flag is reset.
    bool hasMoved() {
        if (m_bHasMoved) {
            m_bHasMoved = false;
            return true;
        }

        return false;
    }

    const Camera* getCamera() const {
        return m_Camera;
    }

private:
    Camera* m_Camera;
    float m_AngleX, m_AngleY;
    float m_Speed;
    bool m_LeftButtonPressed;
    int m_LastX, m_LastY;
    bool m_bHasMoved; // A flag to control if the camera has moved
};

}

#endif // _BONEZ_GLFWCAMERACONTROLLER_HPP
