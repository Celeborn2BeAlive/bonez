#ifndef _BONEZ_GLFWAPPLICATION_HPP_
#define _BONEZ_GLFWAPPLICATION_HPP_

#include "application/Device.hpp"
#include "application/SettingsManager.hpp"
#include "application/ResultManager.hpp"
#include "application/parsers/ParamsParser.hpp"

#include "application/viewers/Viewer.hpp"
#include "application/viewers/VisualDataViewer.hpp"
#include "application/viewers/DeferredViewer.hpp"
#include "application/viewers/ResultViewer.hpp"
#include "application/viewers/ImportanceAnalysisViewer.hpp"

#include "application/GlewInit.hpp"

#include <GLFW/glfw3.h>

#include "GLFWCameraController.hpp"

namespace BnZ {

class GLDevice;
class GUIState;

struct GLData;

struct GLFWHandle {
    GLFWwindow* m_pWindow;

    GLFWHandle(const WindowSettings& windowSettings, const char* title);

    GLFWHandle(const GLFWHandle&) = delete;

    GLFWHandle& operator =(const GLFWHandle&) = delete;

    ~GLFWHandle();
};

class GLFWApplication {
    friend class GUIState;

    embree::FileName m_ApplicationPath;
    GLSLProgramBuilder m_ProgramBuilder;

    Device& m_Device;
    const SettingsManager& m_SettingsManager;

    int m_nViewportX, m_nViewportY;
    WindowSettings m_WindowSettings;

    GLFWHandle m_GLFW;

    GLScene m_GLScene;

    ProjectiveCamera m_Camera;
    GLFWCameraController m_CameraController;

    double m_fFPS;
    double m_fLastFrameTime;
    double m_fBestFrameTime;
    double m_fWorstFrameTime;
    double m_fAvgFrameTime;
    uint32_t m_nFrameCount;

    VisualDataViewer m_VisualDataViewer;
    DeferredViewer m_DeferredViewer;
    ResultViewer m_ResultViewer;
    ImportanceAnalysisViewer m_ImportanceAnalysisViewer;

    enum ViewerType { VISUALDATA_VIEWER, DEFERRED_VIEWER, INTERACTIVE_VIEWER, RESULT_VIEWER };
    Viewer* m_CurrentViewer;
    ViewerType m_CurrentViewerType;

    std::string m_CurrentCameraName;
    std::string m_CurrentRendererName;

    void setupGUI();

    bool drawGUI();

    void setCurrentViewer(Viewer& viewer);

    void setCamera(const CameraSettings* settings);

    void saveCamera();

    static void changeCameraCallback(void *clientData);

    static void saveCameraCallback(void *clientData);

    void updateCamerasGUI();

    void setRenderer(const RendererSettings* settings);

    void render();

    static void renderCallback(void *clientData);

    static void changeRendererCallback(void* clientData);

    void updateRenderersGUI();

    static GLFWApplication* g_Instance;

    bool m_bMustScreenshot;

public:
    GLFWApplication(const embree::FileName& applicationPath,
                    Device& giDevice,
                    const SettingsManager& settingsManager);

    static GLFWApplication* getInstance() {
        return g_Instance;
    }

    ResultViewer& getResultViewer() {
        return m_ResultViewer;
    }

    void setWindowSettings(const WindowSettings& windowSettings);

    void onMouseButtonAction(int glfwButton, int glfwAction);

    int run();

    void loadRenderer();

    void setResultViewer() {
        m_ResultViewer.setInteractive(false);
        setCurrentViewer(m_ResultViewer);
    }
};

}

#endif

