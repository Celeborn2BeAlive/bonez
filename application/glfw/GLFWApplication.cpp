#include "GLFWApplication.hpp"

#include "application/gui.hpp"

#include "bonez/renderer/factories.hpp"
#include "application/embree_bonez_utils.hpp"

#include <atb.hpp>

namespace BnZ {

GLFWApplication* GLFWApplication::g_Instance = nullptr;
static int g_KMod = 0;
static bool g_EventCharTw = false;

void TwEventKeyGLFW(GLFWwindow* pWindow, int glfwKey, int scancode,
                   int glfwAction, int mods)
{
    // Register of modifiers state
    if( glfwAction==GLFW_PRESS )
    {
        switch( glfwKey )
        {
        case GLFW_KEY_LEFT_SHIFT:
        case GLFW_KEY_RIGHT_SHIFT:
            g_KMod |= TW_KMOD_SHIFT;
            break;
        case GLFW_KEY_LEFT_CONTROL:
        case GLFW_KEY_RIGHT_CONTROL:
            g_KMod |= TW_KMOD_CTRL;
            break;
        case GLFW_KEY_LEFT_ALT:
        case GLFW_KEY_RIGHT_ALT:
            g_KMod |= TW_KMOD_ALT;
            break;
        }
    }
    else
    {
        switch( glfwKey )
        {
        case GLFW_KEY_LEFT_SHIFT:
        case GLFW_KEY_RIGHT_SHIFT:
            g_KMod &= ~TW_KMOD_SHIFT;
            break;
        case GLFW_KEY_LEFT_CONTROL:
        case GLFW_KEY_RIGHT_CONTROL:
            g_KMod &= ~TW_KMOD_CTRL;
            break;
        case GLFW_KEY_LEFT_ALT:
        case GLFW_KEY_RIGHT_ALT:
            g_KMod &= ~TW_KMOD_ALT;
            break;
        }
    }

    // Process key pressed
    if( glfwAction==GLFW_PRESS )
    {
        int mod = g_KMod;
        int testkp = ((mod&TW_KMOD_CTRL) || (mod&TW_KMOD_ALT)) ? 1 : 0;

        if( (mod&TW_KMOD_CTRL) && glfwKey>0/* && glfwKey<GLFW_KEY_SPECIAL*/ )   // CTRL cases
            TwKeyPressed(glfwKey, mod);
        else/* if( glfwKey>=GLFW_KEY_SPECIAL )*/
        {
            int k = 0;

            if( glfwKey>=GLFW_KEY_F1 && glfwKey<=GLFW_KEY_F15 )
                k = TW_KEY_F1 + (glfwKey-GLFW_KEY_F1);
            else if( testkp && glfwKey>=GLFW_KEY_KP_0 && glfwKey<=GLFW_KEY_KP_9 )
                k = '0' + (glfwKey-GLFW_KEY_KP_0);
            else
            {
                switch( glfwKey )
                {
                case GLFW_KEY_ESCAPE:
                    k = TW_KEY_ESCAPE;
                    glfwSetWindowShouldClose(pWindow, 1);
                    break;
                case GLFW_KEY_UP:
                    k = TW_KEY_UP;
                    break;
                case GLFW_KEY_DOWN:
                    k = TW_KEY_DOWN;
                    break;
                case GLFW_KEY_LEFT:
                    k = TW_KEY_LEFT;
                    break;
                case GLFW_KEY_RIGHT:
                    k = TW_KEY_RIGHT;
                    break;
                case GLFW_KEY_TAB:
                    k = TW_KEY_TAB;
                    break;
                case GLFW_KEY_ENTER:
                    k = TW_KEY_RETURN;
                    break;
                case GLFW_KEY_BACKSPACE:
                    k = TW_KEY_BACKSPACE;
                    break;
                case GLFW_KEY_INSERT:
                    k = TW_KEY_INSERT;
                    break;
                case GLFW_KEY_DELETE:
                    k = TW_KEY_DELETE;
                    break;
                case GLFW_KEY_PAGE_UP:
                    k = TW_KEY_PAGE_UP;
                    break;
                case GLFW_KEY_PAGE_DOWN:
                    k = TW_KEY_PAGE_DOWN;
                    break;
                case GLFW_KEY_HOME:
                    k = TW_KEY_HOME;
                    break;
                case GLFW_KEY_END:
                    k = TW_KEY_END;
                    break;
                case GLFW_KEY_KP_ENTER:
                    k = TW_KEY_RETURN;
                    break;
                case GLFW_KEY_KP_DIVIDE:
                    if( testkp )
                        k = '/';
                    break;
                case GLFW_KEY_KP_MULTIPLY:
                    if( testkp )
                        k = '*';
                    break;
                case GLFW_KEY_KP_SUBTRACT:
                    if( testkp )
                        k = '-';
                    break;
                case GLFW_KEY_KP_ADD:
                    if( testkp )
                        k = '+';
                    break;
                case GLFW_KEY_KP_DECIMAL:
                    if( testkp )
                        k = '.';
                    break;
                case GLFW_KEY_KP_EQUAL:
                    if( testkp )
                        k = '=';
                    break;
                }
            }

            if( k>0 )
                TwKeyPressed(k, mod);
        }
    }
}

void TwEventCharGLFW(GLFWwindow* pWindow, uint32_t glfwChar)
{
    if((glfwChar & 0xff00)==0) {
        g_EventCharTw = TwKeyPressed(glfwChar, g_KMod);
    }
}

static void onWindowResize(GLFWwindow* pWindow, int width, int height) {
    assert(GLFWApplication::getInstance());
    GLFWApplication::getInstance()->setWindowSettings({ width, height });
}

static void onMouseButton(GLFWwindow* pWindow, int button, int action, int mods) {
    assert(GLFWApplication::getInstance());
    GLFWApplication::getInstance()->onMouseButtonAction(button, action);
}

void FormatDebugOutputARB(char outStr[], size_t outStrSize, GLenum source, GLenum type,
    GLuint id, GLenum severity, const char *msg)
{
    char sourceStr[32];
    const char *sourceFmt = "UNDEFINED(0x%04X)";
    switch(source)

    {
    case GL_DEBUG_SOURCE_API_ARB:             sourceFmt = "API"; break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB:   sourceFmt = "WINDOW_SYSTEM"; break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: sourceFmt = "SHADER_COMPILER"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY_ARB:     sourceFmt = "THIRD_PARTY"; break;
    case GL_DEBUG_SOURCE_APPLICATION_ARB:     sourceFmt = "APPLICATION"; break;
    case GL_DEBUG_SOURCE_OTHER_ARB:           sourceFmt = "OTHER"; break;
    }

    snprintf(sourceStr, 32, sourceFmt, source);

    char typeStr[32];
    const char *typeFmt = "UNDEFINED(0x%04X)";
    switch(type)
    {

    case GL_DEBUG_TYPE_ERROR_ARB:               typeFmt = "ERROR"; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: typeFmt = "DEPRECATED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB:  typeFmt = "UNDEFINED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_PORTABILITY_ARB:         typeFmt = "PORTABILITY"; break;
    case GL_DEBUG_TYPE_PERFORMANCE_ARB:         typeFmt = "PERFORMANCE"; break;
    case GL_DEBUG_TYPE_OTHER_ARB:               typeFmt = "OTHER"; break;
    }
    snprintf(typeStr, 32, typeFmt, type);


    char severityStr[32];
    const char *severityFmt = "UNDEFINED";
    switch(severity)
    {
    case GL_DEBUG_SEVERITY_HIGH_ARB:   severityFmt = "HIGH";   break;
    case GL_DEBUG_SEVERITY_MEDIUM_ARB: severityFmt = "MEDIUM"; break;
    case GL_DEBUG_SEVERITY_LOW_ARB:    severityFmt = "LOW"; break;
    }

    snprintf(severityStr, 32, severityFmt, severity);

    snprintf(outStr, outStrSize, "OpenGL: %s [source=%s type=%s severity=%s id=%d]",
        msg, sourceStr, typeStr, severityStr, id);
}

void foo() {
    std::cerr << "error" << std::endl;
}

void DebugCallbackARB(GLenum source, GLenum type, GLuint id, GLenum severity,
                     GLsizei length, const GLchar *message, GLvoid *userParam)
{
    if(131218 == id) { // "OpenGL: Program/shader state performance warning: Fragment Shader is going to be recompiled because the shader key based on GL state mismatches."
        return;
    } else if(131186 == id) { // "Buffer performance warning: Buffer object 24 (bound to GL_SHADER_STORAGE_BUFFER, usage hint is GL_STATIC_DRAW) is being copied/moved from VIDEO memory to HOST memory."
        return;
    } else {
        char finalMessage[256];
        FormatDebugOutputARB(finalMessage, 256, source, type, id, severity, message);
        std::cerr << finalMessage << std::endl;
        foo();
    }
}

GLFWHandle::GLFWHandle(const WindowSettings &windowSettings, const char *title):
    m_pWindow(nullptr) {
    if(!glfwInit()) {
        throw std::runtime_error("Unable to initialize GLFW");
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
    if(!(m_pWindow = glfwCreateWindow(windowSettings.width, windowSettings.height, title,
                                      nullptr, nullptr))) {
        glfwTerminate();
        throw std::runtime_error("Unable to open GLFW window");
    }

    glfwSetWindowSizeCallback(m_pWindow, onWindowResize);
    glfwSetMouseButtonCallback(m_pWindow, onMouseButton);
    glfwSetKeyCallback(m_pWindow, TwEventKeyGLFW);
    glfwSetCharCallback(m_pWindow, TwEventCharGLFW);

    glfwMakeContextCurrent(m_pWindow);

    glewExperimental = true;
    GLenum error = glewInit();
    if(GLEW_OK != error) {
        std::cerr << glewGetErrorString(error) << std::endl;
        throw std::runtime_error("Glew initialization error");
    }
    // Depush gl error generated by glew
    glGetError();

    if (glewIsSupported("GL_ARB_debug_output")) {
        //glDebugMessageCallbackARB(DebugCallbackARB, nullptr); // print debug output to stderr
        //glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
    } else {
        std::cerr << "GL_ARB_debug_output not supported" << std::endl;
    }
}

GLFWHandle::~GLFWHandle() {
    glfwTerminate();
}

static GLFWCameraController::DirectionMask getGLFWDirectionMask(GLFWwindow* pWindow) {
    GLFWCameraController::DirectionMask directions = 0;

    if(glfwGetKey(pWindow, GLFW_KEY_W)) {
        directions |= GLFWCameraController::FRONT;
    }
    if(glfwGetKey(pWindow, GLFW_KEY_S)) {
        directions |= GLFWCameraController::BACK;
    }
    if(glfwGetKey(pWindow, GLFW_KEY_A)) {
        directions |= GLFWCameraController::LEFT;
    }
    if(glfwGetKey(pWindow, GLFW_KEY_D)) {
        directions |= GLFWCameraController::RIGHT;
    }
    if(glfwGetKey(pWindow, GLFW_KEY_UP)) {
        directions |= GLFWCameraController::UP;
    }
    if(glfwGetKey(pWindow, GLFW_KEY_DOWN)) {
        directions |= GLFWCameraController::DOWN;
    }
    return directions;
}

GLFWApplication::GLFWApplication(const embree::FileName& applicationPath,
                                 Device& giDevice,
                                 const SettingsManager& settingsManager):
    m_ApplicationPath(applicationPath),
    m_ProgramBuilder(applicationPath + embree::FileName("shaders")),
    m_Device(giDevice),
    m_SettingsManager(settingsManager),
    m_WindowSettings(m_SettingsManager.getWindowSettings()),
    m_GLFW(m_WindowSettings, "BoneZ"),
    m_fFPS(0.f),
    m_VisualDataViewer(m_ProgramBuilder),
    m_DeferredViewer(m_ProgramBuilder),
    m_ResultViewer(1.f),
    m_ImportanceAnalysisViewer(m_ProgramBuilder),
    m_CurrentViewer(nullptr),
    m_bMustScreenshot(false) {
    m_GLScene.init(m_Device.getScene());

    m_VisualDataViewer.init(&m_Device, &m_GLScene);

    ParamSet importanceAnalysisIntegratorParams =
            m_SettingsManager.getConfigSettings().get<ParamSet>("importanceAnalysisIntegrator", ParamSet());
    if(importanceAnalysisIntegratorParams.empty()) {
        importanceAnalysisIntegratorParams.set("type", std::string("PathtraceIntegrator"));
        importanceAnalysisIntegratorParams.set("maxDepth", 4);
        importanceAnalysisIntegratorParams.set("indirectOnly", false);
    }
    m_ImportanceAnalysisViewer.init(&m_Device, &m_GLScene, createIntegrator(importanceAnalysisIntegratorParams));

    m_DeferredViewer.init(&m_Device, &m_GLScene);
    m_ResultViewer.init(&m_Device);

    m_CameraController.setSpeed(m_SettingsManager.getCameraSpeed());
    loadRenderer();

    g_Instance = this;
}

void GLFWApplication::loadRenderer() {
    auto renderers = m_SettingsManager.getAllRendererSettings();
    if(renderers.empty()) {
        return;
    }
    m_Device.setRenderer(renderers[0].params);
}

static void CopyStdStringToClient(std::string& destinationClientString, const std::string& sourceLibraryString)
{
      // Copy the content of souceString handled by the AntTweakBar library to destinationClientString handled by your application
      destinationClientString = sourceLibraryString;
}

void GLFWApplication::setCamera(const CameraSettings* settings) {
    m_Device.setCamera(settings->params);
    m_Camera = m_Device.getCamera();
    m_CameraController.setCamera(m_Camera);
    m_CurrentCameraName = settings->name;
}

void GLFWApplication::saveCamera() {
    m_CurrentCameraName = m_SettingsManager.saveCameraSettings(m_Device.getCamera());
}

void GLFWApplication::changeCameraCallback(void *clientData)
{
    CameraSettings* settings = (CameraSettings*) clientData;
    g_Instance->setCamera(settings);
}

void GLFWApplication::saveCameraCallback(void *clientData)
{
    g_Instance->saveCamera();
    g_Instance->updateCamerasGUI();
}

void GLFWApplication::updateCamerasGUI() {
    static std::vector<CameraSettings> cameras;

    TwBar* cameraBar = TwGetBarByName("Cameras");
    assert(cameraBar);
    TwRemoveAllVars(cameraBar);

    TwAddVarRO(cameraBar, "Current", TW_TYPE_STDSTRING, &m_CurrentCameraName, "");
    TwAddButton(cameraBar, "Save", saveCameraCallback,
                nullptr, "");

    TwAddSeparator(cameraBar, "", "");
    cameras = m_SettingsManager.getAllCameraSettings();
    for(auto& camera: cameras) {
        TwAddButton(cameraBar, camera.name.c_str(), changeCameraCallback,
                    &camera, "");
    }
}

void GLFWApplication::render() {
    m_Device.render();
    m_ResultViewer.setInteractive(false);
    setCurrentViewer(m_ResultViewer);
}

void GLFWApplication::setRenderer(const RendererSettings* settings) {
    m_Device.setRenderer(settings->params);
    m_CurrentRendererName = settings->name;
}

void GLFWApplication::renderCallback(void *clientData) {
    g_Instance->render();
}

void GLFWApplication::changeRendererCallback(void* clientData) {
    RendererSettings* settings = (RendererSettings*) clientData;
    g_Instance->setRenderer(settings);
}

void GLFWApplication::updateRenderersGUI() {
    static std::vector<RendererSettings> renderers;

    TwBar* renderersBar = TwGetBarByName("Renderers");
    assert(renderersBar);
    TwRemoveAllVars(renderersBar);

    TwAddVarRO(renderersBar, "Current", TW_TYPE_STDSTRING, &m_CurrentRendererName, "");
    TwAddButton(renderersBar, "Render", renderCallback,
                nullptr, "");

    TwAddSeparator(renderersBar, "", "");
    renderers = m_SettingsManager.getAllRendererSettings();
    for(auto& renderer: renderers) {
        TwAddButton(renderersBar, renderer.name.c_str(), changeRendererCallback,
                    &renderer, "");
    }
}

void GLFWApplication::setupGUI() {
    TwInit(TW_OPENGL_CORE, nullptr);
    TwCopyStdStringToClientFunc(CopyStdStringToClient);

    TwBar* perBar = TwNewBar("Global");

    atb::addButton(perBar, "Screenshot", [this]() {
        m_bMustScreenshot = true;
    }, "");
    atb::addVarRO(perBar, "FPS", m_fFPS, " precision=2 help='Frame per second' ");
    atb::addVarRO(perBar, "Worst frame time", m_fWorstFrameTime, " precision=4 help='in milliseconds' ");
    atb::addVarRO(perBar, "Best frame time", m_fBestFrameTime, " precision=4 help='in milliseconds' ");
    atb::addVarRO(perBar, "Average frame time", m_fAvgFrameTime, " precision=4 help='in milliseconds' ");

    TwAddSeparator(perBar, "", "");

    atb::addVarCB(perBar, "Viewer", "Visual Data, Deferred, Interactive, Result",
        [this](ViewerType type) {
            switch(type) {
            case VISUALDATA_VIEWER:
                setCurrentViewer(m_VisualDataViewer);
                m_CurrentViewerType = VISUALDATA_VIEWER;
                break;
            case INTERACTIVE_VIEWER:
                m_ResultViewer.setInteractive(true);
                setCurrentViewer(m_ResultViewer);
                m_CurrentViewerType = INTERACTIVE_VIEWER;
                break;
            case RESULT_VIEWER:
                m_ResultViewer.setInteractive(false);
                setCurrentViewer(m_ResultViewer);
                m_CurrentViewerType = RESULT_VIEWER;
                break;
            case DEFERRED_VIEWER:
                setCurrentViewer(m_DeferredViewer);
                m_CurrentViewerType = DEFERRED_VIEWER;
            }
        },
        [this]() -> ViewerType {
            return m_CurrentViewerType;
        },
    "");

    TwNewBar("Cameras");
    TwDefine(" Cameras iconified=true ");
    updateCamerasGUI();

    TwNewBar("Renderers");
    TwDefine(" Renderers iconified=true ");
    updateRenderersGUI();
}

bool GLFWApplication::drawGUI() {
    gloops::CapabilityRestorer blending(GL_BLEND, GL_TRUE);
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, GL_FALSE);

    glViewport(0, 0, m_WindowSettings.width, m_WindowSettings.height);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    double fMouseX, fMouseY;
    glfwGetCursorPos(m_GLFW.m_pWindow, &fMouseX, &fMouseY);

    int mouseX = std::floor(fMouseX);
    int mouseY = m_WindowSettings.height - std::floor(fMouseY);

    bool hasFocus = TwMouseMotion(mouseX, m_WindowSettings.height - mouseY);

    TwWindowSize(m_WindowSettings.width, m_WindowSettings.height);
    TwDraw();

    return hasFocus;
}

void GLFWApplication::setCurrentViewer(Viewer& viewer) {
    if(m_CurrentViewer) {
        m_CurrentViewer->onTeardown();
    }

    m_CurrentViewer = &viewer;

    m_CurrentViewer->onSetup();
    m_CurrentViewer->setWindowSize(m_WindowSettings.width, m_WindowSettings.height);
}

int GLFWApplication::run() {
    setupGUI();

    setCurrentViewer(m_VisualDataViewer);
    m_CurrentViewerType = VISUALDATA_VIEWER;

    m_Camera = m_Device.getCamera();
    m_CameraController.setCamera(m_Camera);

    size_t fbWidth = m_Device.getFramebuffer().getWidth(),
        fbHeight = m_Device.getFramebuffer().getHeight();

    // Render at the center of the window
    m_nViewportX = 0.5 * (m_WindowSettings.width - fbWidth);
    m_nViewportY = 0.5 * (m_WindowSettings.height - fbHeight);

    m_fFPS = 0.0;
    m_fLastFrameTime = 0.0;
    m_fBestFrameTime = double(embree::inf);
    m_fWorstFrameTime = 0.0;
    m_fAvgFrameTime = 0.0;
    m_nFrameCount = 0;

    bool bLeftButtonPressed = false;

    double timer = embree::getSeconds();
    double frameTimeSum = 0.0;

    while(!glfwWindowShouldClose(m_GLFW.m_pWindow)) {
        double frameStartTime = embree::getSeconds();

        glClear(GL_COLOR_BUFFER_BIT);

        m_CurrentViewer->drawView();

        if(m_bMustScreenshot) {
            embree::Ref<embree::Image3c> image = new embree::Image3c(m_WindowSettings.width, m_WindowSettings.height);

            GLenum format = GL_RGB;
            if(sizeof(embree::Col3c) == 4 * sizeof(uint8_t)) {
                format = GL_RGBA;
            }

            glReadPixels(0, 0, m_WindowSettings.width, m_WindowSettings.height, format, GL_UNSIGNED_BYTE, image->ptr());

            flipVertically(*image.cast<embree::Image>());

            std::stringstream ss;
            ss << "screenshot" << uint64_t(embree::getSeconds() * 1000.f) << ".png";
            embree::storeImage(image.cast<embree::Image>(), ss.str());

            m_bMustScreenshot = false;
        }

        bool guiHasFocus = drawGUI();

        glfwSwapBuffers(m_GLFW.m_pWindow);

        glfwPollEvents();

        if(!guiHasFocus) {
            double cursorX, cursorY;
            glfwGetCursorPos(m_GLFW.m_pWindow, &cursorX, &cursorY);

            bool leftButtonState = glfwGetMouseButton(m_GLFW.m_pWindow, GLFW_MOUSE_BUTTON_LEFT);

            if(!bLeftButtonPressed && leftButtonState) {
                bLeftButtonPressed = true;
                m_CameraController.startRotation(cursorX, cursorY);
            } else if(bLeftButtonPressed && !leftButtonState) {
                bLeftButtonPressed = false;
                m_CameraController.stopRotation();
            } else if(bLeftButtonPressed) {
                m_CameraController.setCursorPosition(std::floor(cursorX), std::floor(cursorY));
            }
        }

        if(!g_EventCharTw) {
            m_CameraController.move(m_fLastFrameTime, getGLFWDirectionMask(m_GLFW.m_pWindow));
        }

        if(m_CameraController.hasMoved()) {
            m_Device.setCamera(m_Camera);
            m_CurrentCameraName = "";
        }

        m_fLastFrameTime = embree::getSeconds() - frameStartTime;
        m_fBestFrameTime = embree::min(m_fLastFrameTime, m_fBestFrameTime);
        m_fWorstFrameTime = embree::max(m_fLastFrameTime, m_fWorstFrameTime);

        ++m_nFrameCount;
        frameTimeSum += m_fLastFrameTime;

        if(embree::getSeconds() - timer > 1.0) {
            m_fBestFrameTime = double(embree::inf);
            m_fWorstFrameTime = 0.0;

            m_fAvgFrameTime = frameTimeSum / m_nFrameCount;
            m_fFPS = 1.f / m_fAvgFrameTime;

            frameTimeSum = 0.0;
            m_nFrameCount = 0;
            timer = embree::getSeconds();
        }
    }

    TwTerminate();

    imguiRenderGLDestroy();

    return EXIT_SUCCESS;
}

void GLFWApplication::setWindowSettings(const WindowSettings& windowSettings) {
    m_WindowSettings = windowSettings;
    m_nViewportX = 0.5 * (m_WindowSettings.width - m_Device.getFramebuffer().getWidth());
    m_nViewportY = 0.5 * (m_WindowSettings.height - m_Device.getFramebuffer().getHeight());
    m_CurrentViewer->setWindowSize(m_WindowSettings.width, m_WindowSettings.height);
}

void GLFWApplication::onMouseButtonAction(int glfwButton, int glfwAction) {

    TwMouseAction action = (glfwAction==GLFW_PRESS) ? TW_MOUSE_PRESSED : TW_MOUSE_RELEASED;
    if( glfwButton==GLFW_MOUSE_BUTTON_LEFT )
        TwMouseButton(action, TW_MOUSE_LEFT);
    else if( glfwButton==GLFW_MOUSE_BUTTON_RIGHT )
        TwMouseButton(action, TW_MOUSE_RIGHT);
    else if( glfwButton==GLFW_MOUSE_BUTTON_MIDDLE )
        TwMouseButton(action, TW_MOUSE_MIDDLE);

    if(glfwAction == GLFW_RELEASE && glfwButton==GLFW_MOUSE_BUTTON_RIGHT) {
        double cursorX, cursorY;
        glfwGetCursorPos(m_GLFW.m_pWindow, &cursorX, &cursorY);
        m_CurrentViewer->handleClickEvent(cursorX, cursorY);
    }
}

}

