#ifndef _BONEZ_GLEWINIT_HPP
#define _BONEZ_GLEWINIT_HPP

#include <GL/glew.h>

namespace BnZ {

struct GlewInit {
    GlewInit() {
        glewExperimental = true;
        GLenum error = glewInit();
        if(GLEW_OK != error) {
            std::cerr << glewGetErrorString(error) << std::endl;
            throw std::runtime_error("Glew initialization error");
        }
    }
};

}

#endif // _BONEZ_GLEWINIT_HPP
