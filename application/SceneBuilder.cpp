#include "SceneBuilder.hpp"

#include "scene/loaders/assimp/AssimpSceneLoader.hpp"
#include "scene/loaders/skeletons/VSkelLoader.hpp"

#include "bonez/scene/lights/AreaLight.hpp"
#include "bonez/scene/lights/EnvironmentLight.hpp"

#include "bonez/common/Progress.hpp"
#include "bonez/scene/Pink.hpp"

namespace BnZ {

static AffineSpace3f getTransform(const ParamSet& params) {
    AffineSpace3f space = AffineSpace3f(params.get<Vec3f>("vx"), params.get<Vec3f>("vy"), params.get<Vec3f>("vz"),
        params.get<Vec3f>("p"));
    if(params.hasParam("scale")) {
        float scale = params.get<float>("scale");
        space *= AffineSpace3f::scale(Vec3f(scale));
    }
    return space;
}

static void addLight(SceneGeometry& geometry, std::vector<LightPtr>& lights, const ParamSet& params,
    const embree::FileName& path) {
    std::string type = params.get<std::string>("type");

    if (type == "QuadLight") {
        
        Vec3f tmp = params.get<Vec3f>("Le");
        Col3f Le(tmp.x, tmp.y, tmp.z);
        
        AffineSpace3f transform = getTransform(params.get<ParamSet>("transform"));
        
        Vec3f v0(-0.5f, -0.5f, 0.f), v1(0.5f, -0.5f, 0.f), 
            v2(0.5f, 0.5f, 0.f), v3(-0.5f, 0.5f, 0.f);
        
        Vec3f N = embree::normalize(embree::cross(v1 - v0, v3 - v0));
        
        v0 = xfmPoint(transform, v0);
        v1 = xfmPoint(transform, v1);
        v2 = xfmPoint(transform, v2);
        v3 = xfmPoint(transform, v3);
        
        N = embree::normalize(xfmNormal(transform, N));
        
        TriangleMesh light;
        
        light.addVertex(v0, N, Vec2f(0.f, 0.f));
        light.addVertex(v1, N, Vec2f(1.f, 0.f));
        light.addVertex(v2, N, Vec2f(1.f, 1.f));
        light.addVertex(v3, N, Vec2f(0.f, 1.f));
        light.addTriangle(0, 1, 2);
        light.addTriangle(0, 2, 3);
        
        size_t meshIdx = geometry.size();
        
        light.setLight(std::make_shared<AreaLight>(Le, meshIdx));
        geometry.addTriangleMesh(light);
        
        lights.push_back(light.getLight());
        
    }
    
    if (type == "EnvLight") {
        embree::Ref<embree::Image> image;
        Col3f Le(embree::zero);
        
        if(params.hasParam("image")) {
            image = embree::loadImage(path + params.get<std::string>("image"), true);
            Le = Col3f(embree::one);
        }
        
        if(params.hasParam("Le")) {
            Vec3f tmp = params.get<Vec3f>("Le");
            Le = Col3f(tmp.x, tmp.y, tmp.z);
        }
        
        if(Le != Col3f(embree::zero)) {
            lights.push_back(std::make_shared<EnvironmentLight>(image, Le));
        }
    }
}
    
static void addModel(SceneGeometry& geometry, Scene::MaterialContainer& materials, 
    const std::string& file, const embree::FileName& path) {
    bool loadTextures = true;

    /*
    if (params.hasParam("loadTextures")) {
        loadTextures = params.get<bool>("loadTextures");
    }
*/
    AssimpSceneLoader loader(path);
    std::clog << "loading file " << (path + file) << "..." << std::endl;
    loader.loadModel(file, geometry, materials, loadTextures);
    std::clog << "done" << std::endl;
}

static void runCurvSkelPipeline(const VoxelGrid& grid) {

}

Scene buildScene(const ParamSet& params, const embree::FileName& path) {
    Scene scene;
    std::vector<LightPtr> lights;
    
    // Load the model
    if (params.hasParam("models")) {
        ParamArray models = params.get<ParamArray>("models");
        std::cerr << models.size() << std::endl;
        for (const ParamPtr& value: models) {
            addModel(scene.geometry, scene.materials,value->get<std::string>(), path);
        }
    }

    // Load the lights
    if (params.hasParam("lights")) {
        ParamArray lightsArray = params.get<ParamArray>("lights");

        for (const ParamPtr& value: lightsArray) {
            addLight(scene.geometry, lights, value->get<ParamSet>(), path);
        }
    }

    // Load the skeleton
    if (params.hasParam("skeleton")) {
        VSkelLoader loader(path);
        scene.voxelSpace = loader.loadVoxelSpaceAndCurvSkel(
            params.get<std::string>("skeleton"));
    }

    if(params.hasParam("voxelGrid")) {
        scene.voxelSpace->setVoxelGrid(
                    loadVoxelGridFromPGM(
                        path + params.get<std::string>("voxelGrid")
                        )
                    );
    }

    scene.lights = LightContainer(std::move(lights), scene);
    
    if(scene.voxelSpace && !scene.voxelSpace->getVoxelGrid().empty()) {
        /*scene.voxelSpace->getClustering().
                setGrid(computeGridWrtVoxelGrid(scene.voxelSpace->getVoxelGrid(),
                                                scene.voxelSpace->getClustering()));*/

        //scene.voxelSpace->getClustering().correctGridPostProcess();

//        scene.voxelSpace->getClustering().
//                        setGrid(computeGridWithFMM(scene.voxelSpace->getVoxelGrid(),
//                                                   scene.voxelSpace->getClustering()));

/*
        std::cerr << "Recompute node association..." << std::endl;
        scene.topology.skeleton().setGrid(computeGridWrtVoxelGrid(scene.voxelGrid, scene.topology.skeleton()));
        std::cerr << "Done" << std::endl;*/
    }

    return scene;
}

}
