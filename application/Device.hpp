#ifndef _BONEZ_GIDEVICE_HPP_
#define _BONEZ_GIDEVICE_HPP_

#include <embree/common/sys/filename.h>

#include "bonez/common/common.hpp"
#include "bonez/common/cameras/ProjectiveCamera.hpp"
#include "bonez/renderer/Framebuffer.hpp"
#include "bonez/common/Params.hpp"
#include "bonez/renderer/Renderer.hpp"
#include "bonez/opengl/scene/GLScene.hpp"

#include "parsers/JsonParser.hpp"

#include "SettingsManager.hpp"
#include "ResultManager.hpp"

namespace BnZ {

class Device {
private:
    JsonParser m_ParamsParser;

    std::string m_CameraName;
    ProjectiveCamera m_Camera;
    Framebuffer m_Framebuffer;
    
    Scene m_Scene;
    
    RendererPtr m_Renderer;
    ParamSet m_RendererParams;
    float m_fRenderTime;
    ParamSet m_RenderStatistics;
    
    const SettingsManager& m_SettingsManager;
    ResultManager m_ResultManager;

public:
    Device(const SettingsManager& settingsManager);

    void loadApplicationSettings();

    void setCamera(const ParamSet& params);

    void setCamera(const ProjectiveCamera& camera);

    const ProjectiveCamera& getCamera() const;

    void setDefaultCamera();
    
    void setFramebuffer(size_t w, size_t h);
    
    void setRenderer(const ParamSet& params);
    
    const RendererPtr& getRenderer() const;
    
    const ParamSet getRendererParams() const;
    
    void setScene(const ParamSet& params, const embree::FileName& path);
    
    const Scene& getScene() const;

    Scene& getScene();

    Col3f getSurfacePointNodeColor(const SurfacePoint& point) const;

    float getSurfacePointScale() const;

    const Framebuffer& getFramebuffer() const;
    
    Framebuffer& getFramebuffer() {
        return m_Framebuffer;
    }

    void render(bool interactive = false);

    bool getVPLs(VPLContainer& container);

    void saveResult(float gamma, const std::string& batchName = "");

    int runBatch(const std::string& batchName);
};
    
}

#endif
