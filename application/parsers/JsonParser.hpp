#ifndef _BONEZ_APPLICATION_PARSERS_JSONPARSER_HPP_
#define _BONEZ_APPLICATION_PARSERS_JSONPARSER_HPP_

#include <embree/common/sys/filename.h>

#include "ParamsParser.hpp"

#define BONEZ_JSON(str) #str
#define BONEZ_PARAMS(str) JsonParser().fromJSONString(#str)

namespace BnZ {

class JsonParser: public ParamsParser {
public:
    /**
     * Load a set of parameters from a file.
     */
    virtual ParamSet loadParamSet(const embree::FileName& file, const std::string& name) const;
    
    /**
     * Load a list of parameter set from the files of a directory that match the given extension.
     */
    virtual std::vector<ParamSet> loadAllParamSets(const embree::FileName& directory, 
        const std::string& extension, const std::string& name) const;
    
    /**
     * Save a set of parameters in a file.
     */
    virtual void saveParamSet(const ParamSet& params, const embree::FileName& file) const;

    ParamSet fromJSONString(const std::string& json) const;
};

}

#endif
