#include "JsonParser.hpp"
#include "JsonBox.h"

#include "application/sys/Filesystem.hpp"
#include "application/sys/string_utils.hpp"

namespace BnZ {

static ParamSet convert(const JsonBox::Object& object, const embree::FileName& path, const JsonParser& parser);

static bool isNumeric(const JsonBox::Value& value, float& fvalue) {
    if(value.isInteger()) {
        fvalue = value.getInt();
        return true;
    } 
    
    if(value.isDouble()) {
        fvalue = value.getDouble();
        return true;
    }
    
    return false;
}

static bool isVec2f(const JsonBox::Array& array, Vec2f& vec2f) {
    return array.size() == 2 && isNumeric(array[0], vec2f.x) && isNumeric(array[1], vec2f.y);
}

static bool isVec3f(const JsonBox::Array& array, Vec3f& vec3f) {
    return array.size() == 3 && isNumeric(array[0], vec3f.x) && isNumeric(array[1], vec3f.y) 
        && isNumeric(array[2], vec3f.z);
}

static ParamPtr convert(const JsonBox::Value& value, const embree::FileName& path, const JsonParser& parser) {
    if(value.isInteger()) {
        return makeParam(value.getInt());
    }
    
    if(value.isDouble()) {
        return makeParam(static_cast<float>(value.getDouble()));
    }

    if(value.isString()) {
        std::string str = value.getString();
        std::string prefix = "file://";
        if(beginWith(str, prefix)) {
            return makeParam(parser.loadParamSet(path + (str.substr(prefix.size())), ""));
        }
        
        return makeParam(str);
    }
    
    if(value.isBoolean()) {
        return makeParam(value.getBoolean());
    }
    
    if(value.isObject()) {
        return makeParam(convert(value.getObject(), path, parser));
    }
    
    if(value.isArray()) {
        const JsonBox::Array& array = value.getArray();
        Vec2f vec2f;
        if(isVec2f(array, vec2f)) {
            return makeParam(vec2f);
        }
        Vec3f vec3f;
        if(isVec3f(array, vec3f)) {
            return makeParam(vec3f);
        }
        ParamArray paramArray;
        for(const JsonBox::Value& v: array) {
            paramArray.push_back(convert(v, path, parser));
        }
        return makeParam(paramArray);
    }
    
    return nullptr;
}

static ParamSet convert(const JsonBox::Object& object, const embree::FileName& path, const JsonParser& parser) {
    ParamSet params;
    for(auto keyvalue: object) {
        params.set(keyvalue.first, convert(keyvalue.second, path, parser));
    }
    return params;
}

static JsonBox::Array convert(const Vec2f& vec) {
    JsonBox::Array array;
    array.push_back(vec.x);
    array.push_back(vec.y);
    return array;
}

static JsonBox::Array convert(const Vec3f& vec) {
    JsonBox::Array array;
    array.push_back(vec.x);
    array.push_back(vec.y);
    array.push_back(vec.z);
    return array;
}

static JsonBox::Value convert(const ParamPtr& param);

static JsonBox::Object convert(const ParamSet& params);

static JsonBox::Array convert(const ParamArray& paramArray) {
    JsonBox::Array array;
    for(const ParamPtr& param: paramArray) {
        array.push_back(convert(param));
    }
    return array;
}

static JsonBox::Value convert(const ParamPtr& param) {
    if(param->type == ParamValue::INT) {
        return JsonBox::Value(param->get<int>());
    }
    
    if(param->type == ParamValue::FLOAT) {
        return JsonBox::Value(param->get<float>());
    }
    
    if(param->type == ParamValue::VEC2F) {
        return convert(param->get<Vec2f>());
    }
    
    if(param->type == ParamValue::VEC3F) {
        return convert(param->get<Vec3f>());
    }
    
    if(param->type == ParamValue::BOOL) {
        return JsonBox::Value(param->get<bool>());
    }
    
    if(param->type == ParamValue::STRING) {
        return JsonBox::Value(param->get<std::string>());
    }
    
    if(param->type == ParamValue::ARRAY) {
        return convert(param->get<ParamArray>());
    }
    
    if(param->type == ParamValue::PARAMSET) {
        return JsonBox::Value(convert(param->get<ParamSet>()));
    }
    
    return JsonBox::Value();
}

static JsonBox::Object convert(const ParamSet& params) {
    JsonBox::Object object;
    for(auto keyvalue: params.map) {
        object[keyvalue.first] = convert(keyvalue.second);
    }
    return object;
}

/**
 * Load a set of parameters from a file.
 */
ParamSet JsonParser::loadParamSet(const embree::FileName& file, const std::string& name) const {
    JsonBox::Value value;
    value.loadFromFile(file);
    if(!value.isObject()) {
        std::cerr << "JsonParser: the file " << file << " does not contain an object" << std::endl;
        return ParamSet();
    }
    ParamSet params = convert(value.getObject(), file.path(), *this);
    params.name = name;
    return params;
}

/**
 * Load a list of parameter set from the files of a directory that match the given extension.
 */
std::vector<ParamSet> JsonParser::loadAllParamSets(const embree::FileName& directory, 
    const std::string& extension, const std::string& name) const {
    std::vector<ParamSet> sets;
    Filesystem fs(directory);
    Filesystem::Directory dir = fs.openDirectory(".");
    while(Filesystem::Entry entry = dir.next()) {
        if(entry.isRegular() && endWith(entry.path(), extension)) {
            sets.push_back(loadParamSet(entry.completePath(), name));
        }
    }
    return sets;
}

/**
 * Save a set of parameters in a file.
 */
void JsonParser::saveParamSet(const ParamSet& params, const embree::FileName& file) const {
    JsonBox::Value value(convert(params));
    value.writeToFile(file);
}

ParamSet JsonParser::fromJSONString(const std::string& json) const {
    JsonBox::Value value(json);
    if(!value.isObject()) {
        std::cerr << "JsonParser: the string " << json << " is not a JSON object" << std::endl;
        return ParamSet();
    }
    ParamSet params = convert(value.getObject(), "", *this);
    params.name = "";
    return params;
}

}
