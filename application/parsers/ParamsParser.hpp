#ifndef _BONEZ_PARAMSPARSER_HPP_
#define _BONEZ_PARAMSPARSER_HPP_

#include <embree/common/sys/filename.h>
#include "bonez/common/Params.hpp"

namespace BnZ {

class ParamsParser {
public:
    /**
     * Load a set of parameters from a file.
     */
    virtual ParamSet loadParamSet(const embree::FileName& file, const std::string& name) const = 0;
    
    /**
     * Load a list of parameter set from the files of a directory that match the given extension.
     */
    virtual std::vector<ParamSet> loadAllParamSets(const embree::FileName& directory, 
        const std::string& extension, const std::string& name) const = 0;
    
    /**
     * Save a set of parameters in a file.
     */
    virtual void saveParamSet(const ParamSet& params, const embree::FileName& file) const = 0;
};

}

#endif
