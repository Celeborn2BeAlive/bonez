#ifndef _BONEZ_APPLICATION_RESULTMANAGER_HPP_
#define _BONEZ_APPLICATION_RESULTMANAGER_HPP_

#include <embree/common/image/image.h>

#include "bonez/common/Params.hpp"
#include "bonez/common/cameras/Camera.hpp"
#include "bonez/renderer/Framebuffer.hpp"

#include "sys/Filesystem.hpp"
#include "parsers/JsonParser.hpp"
#include "SettingsManager.hpp"

namespace BnZ {

struct Result {
    embree::FileName file; // The file associated to the result (json format)
    Framebuffer framebuffer; // The image
    float time; // Time to render the result
    ParamSet params; // The parameters associated with the result
};

class ResultManager {
public:
    ResultManager(const SettingsManager& settingsManager);

    void saveResult(const Framebuffer& framebuffer, float time, float gamma,
        const ParamSet& rendererParams, const std::string& cameraName,
        const ParamSet& statistics, const std::string& batchName = "") const;

private:
    const SettingsManager& m_SettingsManager;
    JsonParser m_Parser;
};

}

#endif
