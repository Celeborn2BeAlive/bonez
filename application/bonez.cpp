#include <iostream>
#include <sstream>
#include <cstdlib>
#include <queue>

#include <embree/common/sys/filename.h>

#include "parsers/JsonParser.hpp"

#include "glfw/GLFWApplication.hpp"
#include "Device.hpp"
#include "SettingsManager.hpp"
#include "ResultManager.hpp"

using namespace embree;

namespace BnZ {

int main(int argc, char** argv) {
    embree::FileName applicationPath = embree::FileName { argv[0] }.path();

    SettingsManager settingsManager { argv[1], argv[2] };

    Device giDevice { settingsManager };

    if(argc >= 4) {
        return giDevice.runBatch(argv[3]);
    }

    giDevice.loadApplicationSettings();
    GLFWApplication application { applicationPath, giDevice, settingsManager };

    return application.run();
}

}

int main(int argc, char** argv) {
    return BnZ::main(argc, argv);
}
