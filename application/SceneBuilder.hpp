#ifndef _BONEZ_SCENEBUILDER_HPP_
#define _BONEZ_SCENEBUILDER_HPP_

#include <embree/common/sys/filename.h>

#include "bonez/common/common.hpp"
#include "bonez/common/Params.hpp"
#include "bonez/scene/Scene.hpp"

namespace BnZ {

Scene buildScene(const ParamSet& params, const embree::FileName& path);

}

#endif
