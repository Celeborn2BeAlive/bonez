#include "SettingsManager.hpp"
#include <stdexcept>

#include "sys/Filesystem.hpp"
#include "sys/string_utils.hpp"

using namespace embree;

namespace BnZ {

SettingsManager::SettingsManager(const FileName& path, const std::string& config):
    m_Path(path), m_ConfigPath(path + config) {
    FileName globalSettingFile = m_Path + FileName("settings.bnz.js");
    m_GlobalSettings = m_Parser.loadParamSet(globalSettingFile, "globalSettings");
    if(m_GlobalSettings.empty()) {
        throw std::runtime_error("Fail to load " + globalSettingFile.str());
    }
    FileName configSettingFile = m_ConfigPath + FileName("settings.bnz.js");
    m_ConfigSettings = m_Parser.loadParamSet(configSettingFile, "configSettings");
    if(m_ConfigSettings.empty()) {
        throw std::runtime_error("Fail to load " + configSettingFile.str());
    }

    ParamSet windowParams = m_GlobalSettings.get<ParamSet>("window", ParamSet());
    if(!windowParams.empty()) {
        m_WindowSettings.width = windowParams.get<size_t>("width", 1024);
        m_WindowSettings.height = windowParams.get<size_t>("height", 512);
    } else {
        m_WindowSettings.width = 1024;
        m_WindowSettings.height = 512;
    }

    ParamSet framebufferParams = m_ConfigSettings.get<ParamSet>("framebuffer", ParamSet());
    if(!framebufferParams.empty()) {
        m_FramebufferSettings.width = framebufferParams.get<size_t>("width", m_WindowSettings.width);
        m_FramebufferSettings.height = framebufferParams.get<size_t>("height", m_WindowSettings.height);
    } else {
        m_FramebufferSettings.width = m_WindowSettings.width;
        m_FramebufferSettings.height = m_WindowSettings.height;
    }

    m_fCameraSpeed = m_ConfigSettings.get<float>("cameraSpeed", 10.f);
}

WindowSettings SettingsManager::getWindowSettings() const {
    return m_WindowSettings;
}

FramebufferSettings SettingsManager::getFramebufferSettings() const {
    return m_FramebufferSettings;
}

float SettingsManager::getCameraSpeed() const {
    return m_fCameraSpeed;
}

SceneSettings SettingsManager::getSceneSettings(const std::string& name) const {
    SceneSettings settings;
    settings.path = m_ConfigPath + FileName("scenes");
    settings.name = name;
    FileName file = settings.path + FileName(settings.name + ".scene.bnz.js");
    settings.params = m_Parser.loadParamSet(file, settings.name);
    if(settings.params.empty()) {
        throw std::runtime_error("Fail to load " + file.str());
    }
    return settings;
}

SceneSettings SettingsManager::getSceneSettings() const {
    return getSceneSettings(m_ConfigSettings.get<std::string>("scene"));
}

std::vector<SceneSettings> SettingsManager::getAllSceneSettings() const {
    Filesystem fs(m_ConfigPath + FileName("scenes"));
    Filesystem::Directory directory = fs.openDirectory(".");
    std::vector<SceneSettings> container;
    std::string suffix {".scene.bnz.js"};
    while(Filesystem::Entry entry = directory.next()) {
        if(entry.isRegular() && endWith(entry.path().str(), suffix)) {
            SceneSettings settings;
            settings.path = fs.basePath();
            settings.name = entry.path().str().substr(2, entry.path().str().size() - suffix.size() - 2);
            FileName file = entry.completePath();
            settings.params = m_Parser.loadParamSet(file, settings.name);
            if(settings.params.empty()) {
                throw std::runtime_error("Fail to load " + file.str());
            }
            container.emplace_back(settings);
        }
    }
    return container;
}

std::vector<RendererSettings> SettingsManager::getAllRendererSettings(const embree::FileName& path) {
    JsonParser parser;

    Filesystem fs(path);
    Filesystem::Directory directory = fs.openDirectory(".");
    std::vector<RendererSettings> container;
    std::string suffix {".renderer.bnz.js"};
    while(Filesystem::Entry entry = directory.next()) {
        if(entry.isRegular() && endWith(entry.path().str(), suffix)) {
            RendererSettings settings;
            settings.name = entry.path().str().substr(2, entry.path().str().size() - suffix.size() - 2);
            FileName file = entry.completePath();
            settings.params = parser.loadParamSet(file, settings.name);
            if(settings.params.empty()) {
                throw std::runtime_error("Fail to load " + file.str());
            }
            container.emplace_back(settings);
        }
    }
    return container;
}

std::vector<RendererSettings> SettingsManager::getAllRendererSettings() const {
    return getAllRendererSettings(m_Path + FileName("renderers"));
}

CameraSettings SettingsManager::getCameraSettings(const std::string& name) const {
    CameraSettings settings;
    settings.name = name;
    settings.params = m_Parser.loadParamSet(m_ConfigPath + FileName("cameras/" + name + ".camera.bnz.js"), settings.name);
    return settings;
}

std::vector<CameraSettings> SettingsManager::getAllCameraSettings() const {
    Filesystem fs(m_ConfigPath + FileName("cameras"));
    Filesystem::Directory directory = fs.openDirectory(".");
    std::vector<CameraSettings> container;
    std::string suffix {".camera.bnz.js"};
    while(Filesystem::Entry entry = directory.next()) {
        if(entry.isRegular() && endWith(entry.path().str(), suffix)) {
            CameraSettings settings;
            settings.name = entry.path().str().substr(2, entry.path().str().size() - suffix.size() - 2);
            FileName file = entry.completePath();
            settings.params = m_Parser.loadParamSet(file, settings.name);
            if(settings.params.empty()) {
                throw std::runtime_error("Fail to load " + file.str());
            }
            container.emplace_back(settings);
        }
    }
    return container;
}

std::string SettingsManager::saveCameraSettings(const Camera& camera) const {
    Vec3f eye = camera.getPosition();
    Vec3f point = xfmPoint(camera.getLocalToWorldTransform(), Vec3f(0, 0, 1));
    Vec3f up = xfmVector(camera.getLocalToWorldTransform(), Vec3f(0, 1, 0));

    ParamSet params;

    params.set("eye", eye);
    params.set("point", point);
    params.set("up", up);

    std::string name;
    {
        std::stringstream ss;
        ss << "camera" << getAllCameraSettings().size();
        name = ss.str();
    }

    m_Parser.saveParamSet(params, m_ConfigPath + FileName("cameras") + FileName(name + ".camera.bnz.js"));

    return name;
}

BatchSettings SettingsManager::getBatchSettings(const std::string& batchName) const {
    BatchSettings batchSettings;

    batchSettings.name = batchName;

    FileName batchPath = m_ConfigPath + batchName;
    FileName settingsFile = batchPath + FileName("settings.bnz.js");

    ParamSet settings = m_Parser.loadParamSet(settingsFile, batchName);
    if(settings.empty()) {
        throw std::runtime_error("Fail to load " + settingsFile.str());
    }

    ParamSet framebufferParams = settings.get<ParamSet>("framebuffer", ParamSet());
    if(framebufferParams.empty()) {
        throw std::runtime_error("No framebuffer defined in " + settingsFile.str());
    }

    batchSettings.framebuffer.width = framebufferParams.get<size_t>("width");
    batchSettings.framebuffer.height = framebufferParams.get<size_t>("height");

    batchSettings.gamma = settings.get<float>("gamma", 1.f);

    ParamArray scenes = settings.get<ParamArray>("scenes");
    if(scenes.empty()) {
        batchSettings.scenes = getAllSceneSettings();
    } else {
        for(const ParamPtr& param: scenes) {
            batchSettings.scenes.emplace_back(getSceneSettings(param->get<std::string>()));
        }
    }

    ParamArray cameras = settings.get<ParamArray>("cameras");
    if(cameras.empty()) {
        batchSettings.cameras = getAllCameraSettings();
    } else {
        for(const ParamPtr& param: cameras) {
            batchSettings.cameras.emplace_back(getCameraSettings(param->get<std::string>()));
        }
    }

    batchSettings.renderers = getAllRendererSettings(batchPath + FileName("renderers"));

    return batchSettings;
}

}
