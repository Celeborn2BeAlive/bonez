#ifndef _BONEZ_GUI_HPP
#define _BONEZ_GUI_HPP

#include <string>
#include <sstream>
#include "imgui/imgui.h"
#include "imgui/imguiRenderGL3.h"

#include "bonez/common/common.hpp"
#include "atb.hpp"

namespace BnZ {

template<typename T>
static std::string toString(T value) {
    std::stringstream ss;
    ss << value;
    return ss.str();
}

template<typename T, typename... Ts>
static std::string toString(T value, Ts... values) {
    std::stringstream ss;
    ss << value;
    return ss.str() + toString(values...);
}

template<typename... T>
static void imguiLabelFromValues(T... values) {
    std::string str = toString(values...);
    imguiLabel(str.c_str());
}

}

namespace atb {

ATB_GEN_TYPE_HELPER(BnZ::Vec3f, TW_TYPE_DIR3F);

}

#endif // _BONEZ_GUI_HPP
