#include "Device.hpp"

#include "parsers/JsonParser.hpp"
#include "bonez/renderer/factories.hpp"
#include "scene/loaders/assimp/AssimpSceneLoader.hpp"

#include "SceneBuilder.hpp"

#include "bonez/common/cameras/HemisphereCamera.hpp"
#include "bonez/common/cameras/ParaboloidCamera.hpp"

namespace BnZ {

Device::Device(const SettingsManager& settingsManager):
    m_CameraName(""), m_fRenderTime(-1), m_SettingsManager(settingsManager), m_ResultManager(settingsManager) {
}

void Device::loadApplicationSettings() {
    SceneSettings scene = m_SettingsManager.getSceneSettings();
    if(scene.params.empty()) {
        std::cerr << "No scene" << std::endl;
        return;
    }
    setScene(scene.params, scene.path);

    FramebufferSettings framebuffer = m_SettingsManager.getFramebufferSettings();
    setFramebuffer(framebuffer.width, framebuffer.height);

    setDefaultCamera();
}

void Device::setCamera(const ParamSet& params) {
    if(m_Framebuffer.empty()) {
        throw std::runtime_error("No framebuffer.");
    }

    Vec3f eye = params.get<Vec3f>("eye");
    Vec3f point = params.get<Vec3f>("point");
    Vec3f up = params.get<Vec3f>("up");

    m_Camera = ProjectiveCamera(AffineSpace3f::lookAtPoint(eye, point, up),
                                m_Framebuffer.getWidth(), m_Framebuffer.getHeight(),
                                0.0f, 1.f,
                                getProjectionMatrix(m_Framebuffer));

    m_CameraName = params.name;

    m_Framebuffer.clear();
}

void Device::setCamera(const ProjectiveCamera& camera) {
    m_Camera = camera;
    m_CameraName = "";
    m_Framebuffer.clear();
}

void Device::setDefaultCamera() {
    if(m_Framebuffer.empty()) {
        throw std::runtime_error("No framebuffer.");
    }
    
    Vec3f eye = embree::zero;
    Vec3f point(0.f, 0.f, 1.f);
    Vec3f up(0.f, 1.f, 0.f);
    
    setCamera(ProjectiveCamera(
                  AffineSpace3f::lookAtPoint(eye, point, up),
                  m_Framebuffer.getWidth(), m_Framebuffer.getHeight(),
                  0.0f, 1.f,
                  getProjectionMatrix(m_Framebuffer)));
}

const ProjectiveCamera& Device::getCamera() const {
    return m_Camera;
}

void Device::setFramebuffer(size_t w, size_t h) {
    m_Framebuffer = Framebuffer(w, h);
}

void Device::setRenderer(const ParamSet& params) {
    m_RendererParams = params;
    m_Renderer = createRenderer(m_RendererParams);
    m_Framebuffer.clear();
}

const RendererPtr& Device::getRenderer() const {
    return m_Renderer;
}

const ParamSet Device::getRendererParams() const {
    return m_RendererParams;
}

Col3f Device::getSurfacePointNodeColor(const SurfacePoint& point) const {
    GraphNodeIndex node;
    if(m_Scene.voxelSpace
            && UNDEFINED_NODE != (node = m_Scene.voxelSpace->getClustering().getNearestNode(point))) {
        return m_Scene.voxelSpace->getClusteringNodeColor(node);
    }
    return Col3f(1, 0, 0);
}

float Device::getSurfacePointScale() const {
    return embree::length(m_Scene.geometry.boundingBox.size()) * 0.006;
}

void Device::setScene(const ParamSet& params, const embree::FileName& path) {
    m_Scene = buildScene(params, path);

    // Build the acceleration structure
    m_Scene.preprocess();
}

const Scene& Device::getScene() const {
    return m_Scene;
}

Scene& Device::getScene() {
    return m_Scene;
}


const Framebuffer& Device::getFramebuffer() const {
    return m_Framebuffer;
}

void Device::render(bool interactive) {
    if(!m_Renderer) {
        std::cerr << "No renderer set." << std::endl;
        return;
    }
    
    if(m_Framebuffer.empty()) {
        std::cerr << "No framebuffer set." << std::endl;
        return;
    }
    
    if(!interactive) {
        std::clog << "Global Illumination rendering..." << std::endl;

        setRenderer(m_RendererParams); // Reset the renderer
        m_Framebuffer.clear(); // Reset the framebuffer
    }

    double start = embree::getSeconds();
    ParamSet renderStatistics("rendererStatistics");

    m_Renderer->render(m_Camera, m_Scene, m_Framebuffer, !interactive, &renderStatistics);
    m_fRenderTime = embree::getSeconds() - start;

    if(!interactive) {
        std::clog << "time: " << m_fRenderTime << " s" << std::endl;
    }
    
    m_RenderStatistics = renderStatistics;
}

bool Device::getVPLs(VPLContainer& container) {
    setRenderer(m_RendererParams); // Reset the renderer
    return m_Renderer->getVPLs(m_Camera, m_Scene, m_Framebuffer, container);
}

void Device::saveResult(float gamma, const std::string& batchName) {
    m_ResultManager.saveResult(m_Framebuffer, m_fRenderTime, gamma, m_RendererParams,
                               m_CameraName, m_RenderStatistics, batchName);
}

int Device::runBatch(const std::string& batchName) {
    BatchSettings settings = m_SettingsManager.getBatchSettings(batchName);

    setFramebuffer(settings.framebuffer.width, settings.framebuffer.height);

    std::clog << "Start batch " << batchName << std::endl;

    for(const auto& scene: settings.scenes) {
        setScene(scene.params, scene.path);

        for(const auto& renderer: settings.renderers) {
            setRenderer(renderer.params);

            for(const auto& camera: settings.cameras) {
                setCamera(camera.params);

                render(false);

                saveResult(settings.gamma, batchName);
            }
        }
    }

    return 0;
}

}
