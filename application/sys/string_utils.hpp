#ifndef _BONEZ_STRING_UTILS_HPP_
#define _BONEZ_STRING_UTILS_HPP_

#include <string>

namespace BnZ {

inline bool endWith(const std::string& str, const std::string& suffix) {
    int offset = (int) str.size() - (int) suffix.size();
    return offset >= 0 && str.substr(offset, suffix.size()) == suffix;
}

inline bool beginWith(const std::string& str, const std::string& prefix) {
    return str.size() >= prefix.size() && str.substr(0, prefix.size()) == prefix;
}

}

#endif
