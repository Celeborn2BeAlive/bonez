#ifndef _BONEZ_FILESYSTEM_HPP_
#define _BONEZ_FILESYSTEM_HPP_

#include <embree/common/sys/filename.h>

// Use Posix API
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

namespace BnZ {

class Filesystem {
public:
    class Entry {
    public:
        Entry():
            m_bExists(false) {
        }
        
        Entry(const embree::FileName& completePath, const embree::FileName& path):
            m_CompletePath(completePath), m_Path(path) {
            if(0 == stat(m_CompletePath.c_str(), &m_Stat)) {
                m_bExists = true;
            } else {
                m_bExists = false;
            }
        }
        
        operator bool() const {
            return m_bExists;
        }
        
        bool isRegular() const {
            return m_bExists && S_ISREG(m_Stat.st_mode);
        }
        
        bool isDirectory() const {
            return m_bExists && S_ISDIR(m_Stat.st_mode);
        }
        
        const embree::FileName& path() const {
            return m_Path;
        }
        
        const embree::FileName& completePath() const {
            return m_CompletePath;
        }
        
    private:
        embree::FileName m_CompletePath;
        embree::FileName m_Path;
        bool m_bExists;
        struct stat m_Stat;
    };

    class Directory {
    public:
        Directory(const embree::FileName& completePath, const embree::FileName& path):
            m_CompletePath(completePath), m_Path(path), m_pDir(opendir(m_CompletePath.c_str())) {
        }
        
        ~Directory() {
            if(m_pDir) {
                closedir(m_pDir);
            }
        }
        
        operator bool() const {
            return m_pDir;
        }
        
        const embree::FileName& path() const {
            return m_Path;
        }
        
        const embree::FileName& completePath() const {
            return m_CompletePath;
        }
        
        Entry next() {
            struct dirent* entry = readdir(m_pDir);
            if(!entry) {
                return Entry();
            }
            embree::FileName filename = entry->d_name;
            return Entry(m_CompletePath + filename, 
                m_Path + filename);
        }
        
    private:
        embree::FileName m_CompletePath;
        embree::FileName m_Path;
        DIR* m_pDir;
    };

    Filesystem(const embree::FileName& basePath = ""):
        m_BasePath(basePath) {
    }
    
    Directory openDirectory(const embree::FileName& path) const {
        return Directory(m_BasePath + path, path);
    }

    Entry getEntry(const embree::FileName& path) const {
        return Entry(m_BasePath + path, path);
    }
    
    const embree::FileName& basePath() const {
        return m_BasePath;
    }
    
private:
    embree::FileName m_BasePath;
};

inline void mkdir(const embree::FileName& path) {
    ::mkdir(path.c_str(), S_IRUSR | S_IWUSR | S_IXUSR | S_IROTH | S_IWOTH | S_IXOTH);
}

}

#endif
