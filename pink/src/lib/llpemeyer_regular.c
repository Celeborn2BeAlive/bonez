/* $Id: llpemeyer_regular.c,v 1.1.1.1 2008-11-25 08:01:41 mcouprie Exp $ */
/* 
   Operateur de calcul de la ligne de partage des eaux
   d'apres "Un algorithme optimal de ligne de partage des eaux"
           F. Meyer - actes du 8eme congres AFCET - Lyon-Villeurbanne
           1991 
   variante de la section VI (ligne d'epaisseur 1 pixel)

   Utilise une File d'Attente Hierarchique.

   Michel Couprie - juin 1997 

   Update janvier 2000 : generation d'una animation (flag ANIMATE)
   Update janvier 2001 : mise a jour 2D-3D 
   Update juin 2004  : "�criture" d'une version sans ligne
*/

/*
*/
//#define ANIMATE

#define PARANO                 /* even paranoid people have ennemies */
#define VERBOSE

#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/types.h>
#include <stdlib.h>
#include <mccodimage.h>
#include <mcimage.h>
#include <mcfah.h>
#include <mcindic.h>
#include <llpemeyer.h>
#include <mckhalimsky2d.h>

#define EN_FAH   0

/* ==================================== */
static
int32_t NotIn(int32_t e, int32_t *list, int32_t n)                       
/* ==================================== */
{
/* renvoie 1 si e n'est pas dans list, 0 sinon */
/* e : l'element a rechercher */
/* list : la liste (tableau d'entiers) */
/* n : le nombre d'elements dans la liste */
  while (n > 0)
    if (list[--n] == e) return 0;
  return 1;
} /* NotIn() */

/* ==================================== */
int32_t llpemeyer_regular_llpemeyer(
        struct xvimage *image,
        struct xvimage *marqueurs,
        struct xvimage *marqueursfond,
        struct xvimage *masque,
        int32_t connex)
/* ==================================== */
#undef F_NAME
#define F_NAME "llpemeyer"
{
  register int32_t i;                       /* index muet */
  register int32_t x;                       /* index muet de pixel */
  register int32_t y;                       /* index muet (generalement un voisin de x) */
  register int32_t w;                       /* index muet (generalement un voisin de x) */
  register int32_t k;                       /* index muet */
  int32_t j;
  int32_t rs = rowsize(image);     /* taille ligne */
  int32_t cs = colsize(image);     /* taille colonne */
  int32_t N = rs * cs;             /* taille image */
  uint8_t *F = UCHARDATA(image);      /* l'image de depart */
  uint8_t *B = UCHARDATA(marqueurs);       /* l'image de marqueurs */
  uint8_t *BF;                             /* l'image de marqueurs du fond */
  uint8_t *MA;                             /* l'image de masque */
  uint32_t *M;             /* l'image d'etiquettes */
  int32_t nlabels;                 /* nombre de labels differents */
  Fah * FAH;                   /* la file d'attente hierarchique */
  int32_t etiqcc[4];
  int32_t ncc;  
  int32_t incr_vois;
#ifdef ANIMATE
  int32_t curlev = -1, nimage = 0; 
  char imname[128];
  struct xvimage *animimage;
  uint8_t *A;
#endif

  if (depth(image) != 1) 
  {
    fprintf(stderr, "%s: cette version ne traite pas les images volumiques\n", F_NAME);
    exit(0);
  }

  if ((rowsize(marqueurs) != rs) || (colsize(marqueurs) != cs))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }

  if (marqueursfond && ((rowsize(marqueursfond) != rs) || (colsize(marqueursfond) != cs)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (masque && ((rowsize(masque) != rs) || (colsize(masque) != cs)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (masque) MA = UCHARDATA(masque);
  if (marqueursfond) BF = UCHARDATA(marqueursfond);

  IndicsInit(N);
  FAH = CreeFahVide(N+1);
  if (FAH == NULL)
  {   fprintf(stderr, "%s : CreeFah failed\n", F_NAME);
      return(0);
  }

  switch (connex)
  {
    case 4: incr_vois = 2; break;
    case 8: incr_vois = 1; break;
    default: 
      fprintf(stderr, "%s: mauvaise connexite: %d\n", F_NAME, connex);
      return 0;
  } /* switch (connex) */    

  /* ================================================ */
  /* CREATION DES LABELS INITIAUX                     */
  /* ================================================ */

  M = (uint32_t *)calloc(N, sizeof(int32_t));
  if (M == NULL)
  {   fprintf(stderr, "%s : calloc failed\n", F_NAME);
      return(0);
  }
  nlabels = 0;

  if (marqueursfond)
  {
    nlabels += 1;                 /* tous les marqueurs du fond ont le meme label (1) */
    for (x = 0; x < N; x++)
    {
      if (BF[x] && (M[x] == 0) && (!masque || MA[x]))
      {
        M[x] = nlabels;
        FahPush(FAH, x, 0);
        while (! FahVide(FAH))
        {
          w = FahPop(FAH);
          for (k = 0; k < 8; k += incr_vois)
          {
            y = voisin(w, k, rs, N);
            if ((y != -1) &&  BF[y] && (M[y] == 0) && (!masque || MA[y]))
            {
              M[y] = nlabels;
              FahPush(FAH, y, 0);
            } /* if y ... */
          } /* for k ... */
        } /* while (! FahVide(FAH)) */
      } /* if (M[x] == 0) */
    } /* for (x = 0; x < N; x++) */
  } /* if (marqueursfond) */

  for (x = 0; x < N; x++)
  {
    if (B[x] && (M[x] == 0) && (!masque || MA[x]))
    {
      nlabels += 1;
      M[x] = nlabels;
      FahPush(FAH, x, 0);
      while (! FahVide(FAH))
      {
        w = FahPop(FAH);
        for (k = 0; k < 8; k += incr_vois)
        {
          y = voisin(w, k, rs, N);
          if ((y != -1) &&  B[y] && (M[y] == 0) && (!masque || MA[y]))
          {
            M[y] = nlabels;
            FahPush(FAH, y, 0);
          } /* if y ... */
        } /* for k ... */
      } /* while (! FahVide(FAH)) */
    } /* if (M[x] == 0) */
  } /* for (x = 0; x < N; x++) */

  /* ================================================ */
  /* INITIALISATION DE LA FAH                         */
  /* ================================================ */

  FahFlush(FAH);
  FahPush(FAH, -1, 0);   /* force la creation du niveau 0 dans la Fah. */
                         /* NECESSAIRE pour eviter la creation prematuree */
                         /* de la file d'urgence */ 

  for (x = 0; x < N; x++)
  {
    if (M[x] && (!masque || MA[x]))            /* on va empiler les voisins des regions marquees */
    {
      for (k = 0; k < 8; k += incr_vois)
      {
        y = voisin(x, k, rs, N);
        if ((y != -1) && !M[y] && !IsSet(y, EN_FAH))
        {        
          FahPush(FAH, y, F[y]);
          Set(y, EN_FAH);
        }
      } /* for (k = 0; k < 8; k += 2) */
    } /* if (M[x]) */
  } /* for (x = 0; x < N; x++) */

  x = FahPop(FAH);
#ifdef PARANO
  if (x != -1)
  {   
     fprintf(stderr,"%s : ORDRE FIFO NON RESPECTE PAR LA FAH !!!\n", F_NAME);
     return(0);
  }
#endif

  /* ================================================ */
  /* INONDATION                                       */
  /* ================================================ */

#ifdef ANIMATE
  animimage = copyimage(image);
  A = UCHARDATA(animimage);
#endif
  nlabels += 1;          /* cree le label pour les points de la LPE */
  while (! FahVide(FAH))
  {
    x = FahPop(FAH);
    UnSet(x, EN_FAH);
#ifdef ANIMATE
    if (F[x] > curlev)
    {
      printf("Niveau %d\n", F[x]);
      sprintf(imname, "anim%03d.pgm", nimage); nimage++;
      for (y = 0; y < N; y++)
        if ((M[y] == nlabels) || (M[y] == 0)) A[y] = 255; else A[y] = 0;
      writeimage(animimage, imname);
      curlev = F[x];
    }
#endif

    ncc = 0;
    for (k = 0; k < 8; k += incr_vois)
    {
      y = voisin(x, k, rs, N);
      if ((y != -1) && (M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
      {
        etiqcc[ncc] = M[y];        
        ncc += 1;
      }
    } /* for k */

    if (ncc == 1)
    {
      M[x] = etiqcc[0];
      for (k = 0; k < 8; k += incr_vois)
      {
        y = voisin(x, k, rs, N);     
        if ((y != -1) && (M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
        {          
          FahPush(FAH, y, F[y]); 
          Set(y, EN_FAH);
        } /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
      } /* for k */
    } 
    else 
    if (ncc > 1)
    {
      M[x] = nlabels;
    }

  } /* while (! FahVide(FAH)) */
  /* FIN PROPAGATION */

  for (x = 0; x < N; x++)
  {
    if ((M[x] == nlabels) || (M[x] == 0)) F[x] = 255; else F[x] = 0;
  }

  /* ================================================ */
  /* UN PEU DE MENAGE                                 */
  /* ================================================ */

  IndicsTermine();
  FahTermine(FAH);
  free(M);
  return(1);
} /*  /* llpemeyer() */

/* ==================================== */
int32_t llpemeyer_regular_llpemeyer2(
        struct xvimage *image,
        struct xvimage *marqueurs,
        struct xvimage *masque,
        int32_t connex)
/* ==================================== */
#undef F_NAME
#define F_NAME "llpemeyer2"
{
  register int32_t i;                       /* index muet */
  register int32_t x;                       /* index muet de pixel */
  register int32_t y;                       /* index muet (generalement un voisin de x) */
  register int32_t w;                       /* index muet (generalement un voisin de x) */
  register int32_t k;                       /* index muet */
  int32_t j;
  int32_t rs = rowsize(image);     /* taille ligne */
  int32_t cs = colsize(image);     /* taille colonne */
  int32_t N = rs * cs;             /* taille image */
  uint8_t *F = UCHARDATA(image);  /* l'image de depart */
  uint32_t *M = ULONGDATA(marqueurs);   /* l'image de marqueurs */
  uint8_t *MA;                         /* l'image de masque */
  Fah * FAH;                                 /* la file d'attente hierarchique */
  int32_t etiqcc[4];
  int32_t ncc;  
  int32_t incr_vois;
  int32_t nlabels;

  if (depth(image) != 1) 
  {
    fprintf(stderr, "%s: cette version ne traite pas les images volumiques\n", F_NAME);
    exit(0);
  }

  if ((rowsize(marqueurs) != rs) || (colsize(marqueurs) != cs))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (masque && ((rowsize(masque) != rs) || (colsize(masque) != cs)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (masque) MA = UCHARDATA(masque);

  if (datatype(marqueurs) != VFF_TYP_4_BYTE)
  {
    fprintf(stderr, "%s: marker image must be uint32_t\n", F_NAME);
    return 0;
  }

  IndicsInit(N);
  FAH = CreeFahVide(N+1);
  if (FAH == NULL)
  {   fprintf(stderr, "%s : CreeFah failed\n", F_NAME);
      return(0);
  }

  switch (connex)
  {
    case 4: incr_vois = 2; break;
    case 8: incr_vois = 1; break;
    default: 
      fprintf(stderr, "%s: mauvaise connexite: %d\n", F_NAME, connex);
      return 0;
  } /* switch (connex) */    

  nlabels = 0;
  for (x = 0; x < N; x++) if (M[x] > nlabels) nlabels = M[x];

  /* ================================================ */
  /* INITIALISATION DE LA FAH                         */
  /* ================================================ */

  FahFlush(FAH);
  FahPush(FAH, -1, 0);   /* force la creation du niveau 0 dans la Fah. */
                         /* NECESSAIRE pour eviter la creation prematuree */
                         /* de la file d'urgence */ 

  for (x = 0; x < N; x++)
  {
    if (M[x] && (!masque || MA[x]))            /* on va empiler les voisins des regions marquees */
    {
      for (k = 0; k < 8; k += incr_vois)
      {
        y = voisin(x, k, rs, N);
        if ((y != -1) && !M[y] && !IsSet(y, EN_FAH))
        {        
          FahPush(FAH, y, F[y]);
          Set(y, EN_FAH);
        }
      } /* for (k = 0; k < 8; k += 2) */
    } /* if (M[x]) */
  } /* for (x = 0; x < N; x++) */

  x = FahPop(FAH);
#ifdef PARANO
  if (x != -1)
  {   
     fprintf(stderr,"%s : ORDRE FIFO NON RESPECTE PAR LA FAH !!!\n", F_NAME);
     return(0);
  }
#endif

  /* ================================================ */
  /* INONDATION                                       */
  /* ================================================ */

  nlabels += 1;          /* cree le label pour les points de la LPE */
  while (! FahVide(FAH))
  {
    x = FahPop(FAH);
    UnSet(x, EN_FAH);

    ncc = 0;
    for (k = 0; k < 8; k += incr_vois)
    {
      y = voisin(x, k, rs, N);
      if ((y != -1) && (M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
      {
        etiqcc[ncc] = M[y];        
        ncc += 1;
      }
    } /* for k */

    if (ncc == 1)
    {
      M[x] = etiqcc[0];
      for (k = 0; k < 8; k += incr_vois)
      {
        y = voisin(x, k, rs, N);     
        if ((y != -1) && (M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
        {          
          FahPush(FAH, y, F[y]); 
          Set(y, EN_FAH);
        } /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
      } /* for k */
    } 
    else 
    if (ncc > 1)
    {
      M[x] = nlabels;
    }
  } /* while (! FahVide(FAH)) */
  /* FIN PROPAGATION */

  for (x = 0; x < N; x++)
  {
    if ((M[x] == nlabels) || (M[x] == 0)) F[x] = 255; else F[x] = 0;
  }

  /* ================================================ */
  /* UN PEU DE MENAGE                                 */
  /* ================================================ */

  IndicsTermine();
  FahTermine(FAH);
  return(1);
} /*  /* llpemeyer2() */

 /* ==================================== */
 int32_t llpemeyer_regular_llpemeyerkhalimsky(
         struct xvimage *image,
         struct xvimage *marqueurs,
         struct xvimage *marqueursfond,
         struct xvimage *masque)
 /* ==================================== */
#undef F_NAME
#define F_NAME "llpemeyerkhalimsky"
 /* 
 Pour forcer la lpe a passer par des elements de rang < 2,
 l'image "image" doit avoir ete generee a partir de Z2
 en utilisant la  strategie Max pour passer dans la grille de Khalimsky.
 De plus, les elements de rang 0 et 1, a ndg egal, ont une priorite 
 superieure a celle des elements de rang 2.
 */

 {
   register int32_t i;                       /* index muet */
   register int32_t x;                       /* index muet de pixel */
   register int32_t y;                       /* index muet (generalement un voisin de x) */
   register int32_t w;                       /* index muet (generalement un voisin de x) */
   register int32_t k;                       /* index muet */
   int32_t j;
   int32_t rs = rowsize(image);     /* taille ligne */
   int32_t cs = colsize(image);     /* taille colonne */
   int32_t N = rs * cs;             /* taille image */
   uint8_t *F = UCHARDATA(image);      /* l'image de depart */
   uint8_t *B = UCHARDATA(marqueurs);       /* l'image de marqueurs */
   uint8_t *BF;                             /* l'image de marqueurs du fond */
   uint8_t *MA;                             /* l'image de masque */
   uint32_t *M;             /* l'image d'etiquettes */
   int32_t nlabels;                 /* nombre de labels differents */
   Fah * FAH;                   /* la file d'attente hierarchique */
   int32_t etiqcc[4];
   int32_t ncc;  
   int32_t tab[27]; 
   int32_t n;

   if (depth(image) != 1) 
   {
     fprintf(stderr, "%s: cette version ne traite pas les images volumiques\n", F_NAME);
     exit(0);
   }

   if ((rowsize(marqueurs) != rs) || (colsize(marqueurs) != cs))
   {
     fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
     return 0;
   }

   if (marqueursfond && ((rowsize(marqueursfond) != rs) || (colsize(marqueursfond) != cs)))
   {
     fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
     return 0;
   }
   if (marqueursfond) BF = UCHARDATA(marqueursfond);
   if (masque && ((rowsize(masque) != rs) || (colsize(masque) != cs)))
   {
     fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
     return 0;
   }
   if (masque) MA = UCHARDATA(masque);

   IndicsInit(N);
   FAH = CreeFahVide(N+1);
   if (FAH == NULL)
   {   fprintf(stderr, "%s() : CreeFah failed\n", F_NAME);
       return(0);
   }

   /* ================================================ */
   /* CREATION DES LABELS INITIAUX                     */
   /* ================================================ */

   M = (uint32_t *)calloc(N, sizeof(int32_t));
   if (M == NULL)
   {   fprintf(stderr, "%s() : calloc failed\n", F_NAME);
       return(0);
   }
   nlabels = 0;

   if (marqueursfond)
   {
     nlabels += 1;                 /* tous les marqueurs du fond ont le meme label (1) */
     for (x = 0; x < N; x++)
     {
       if (BF[x] && (M[x] == 0) && (!masque || MA[x]))
       {
         M[x] = nlabels;
         FahPush(FAH, x, 0);
         while (! FahVide(FAH))
         {
           w = FahPop(FAH);
           Thetacarre2d(rs, cs, w%rs, w/rs, tab, &n);
           for (k = 0; k < n; k++) /* parcourt les eventuels theta-voisins */
           {
             y = tab[k];
             if (BF[y] && (M[y] == 0) && (!masque || MA[y]))
             {
               M[y] = nlabels;
               FahPush(FAH, y, 0);
             } /* if y ... */
           } /* for k ... */
         } /* while (! FahVide(FAH)) */
       } /* if (M[x] == 0) */
     } /* for (x = 0; x < N; x++) */
   } /* if (marqueursfond) */

   for (x = 0; x < N; x++)
   {
     if (B[x] && (M[x] == 0) && (!masque || MA[x]))
     {
       nlabels += 1;
       M[x] = nlabels;
       FahPush(FAH, x, 0);
       while (! FahVide(FAH))
       {
         w = FahPop(FAH);
         Thetacarre2d(rs, cs, w%rs, w/rs, tab, &n);
         for (k = 0; k < n; k++) /* parcourt les eventuels theta-voisins */
         {
           y = tab[k];
           if (B[y] && (M[y] == 0) && (!masque || MA[y]))
           {
             M[y] = nlabels;
             FahPush(FAH, y, 0);
           } /* if y ... */
         } /* for k ... */
       } /* while (! FahVide(FAH)) */
     } /* if (M[x] == 0) */
   } /* for (x = 0; x < N; x++) */

   /* ================================================ */
   /* INITIALISATION DE LA FAH                         */
   /* ================================================ */

   FahFlush(FAH);
   FahPush(FAH, -1, 0);   /* force la creation du niveau 0 dans la Fah. */
                          /* NECESSAIRE pour eviter la creation prematuree */
                          /* de la file d'urgence */ 

   for (x = 0; x < N; x++)
   {
     if (M[x] && (!masque || MA[x]))            /* on va empiler les voisins des regions marquees */
     {
       Thetacarre2d(rs, cs, x%rs, x/rs, tab, &n);
       for (k = 0; k < n; k++) /* parcourt les eventuels theta-voisins */
       {
         y = tab[k]; 
         if (!M[y] && !IsSet(y, EN_FAH))
         { 
           if (CARRE(y%rs,y/rs))           
           FahPush(FAH, y, F[y]*2);
           else FahPush(FAH, y, F[y]*2+1); //plus grande priorite pour les elements de rang <2
           Set(y, EN_FAH);
         }
       } /* for (k = 0; k < 8; k += 2) */
     } /* if (M[x]) */
   } /* for (x = 0; x < N; x++) */

   x = FahPop(FAH);
 #ifdef PARANO
   if (x != -1)
   {   
      fprintf(stderr,"%s : ORDRE FIFO NON RESPECTE PAR LA FAH !!!\n", F_NAME);
      return(0);
   }
 #endif

   /* ================================================ */
   /* INONDATION                                       */
   /* ================================================ */

   nlabels += 1;          /* cree le label pour les points de la LPE */
   while (! FahVide(FAH))
   {
     x = FahPop(FAH);
     UnSet(x, EN_FAH);

     ncc = 0;
     Thetacarre2d(rs, cs, x%rs, x/rs, tab, &n);
     for (k = 0; k < n; k++) /* parcourt les eventuels theta-voisins */
     {
       y = tab[k];
       if ((M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
       {
         etiqcc[ncc] = M[y];        
         ncc += 1;
       }
     } /* for k */

     if (ncc == 1)
     {
       M[x] = etiqcc[0];
       Thetacarre2d(rs, cs, x%rs, x/rs, tab, &n);
       for (k = 0; k < n; k++) /* parcourt les eventuels theta-voisins */
       {
         y = tab[k];
         if ((M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
         {
           if (CARRE(y%rs,y/rs))    
           FahPush(FAH, y, F[y]*2);
           else FahPush(FAH, y, F[y]*2+1); // priorite superieure pour les element de rang <2
           Set(y, EN_FAH);
         } /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
       } /* for k */
     } 
     else 
     if (ncc > 1)
     {
       M[x] = nlabels;
     }

   } /* while (! FahVide(FAH)) */
   /* FIN PROPAGATION */

   for (x = 0; x < N; x++)
   {
     if ((M[x] == nlabels) || (M[x] == 0)) F[x] = 255; else F[x] = 0;
   }

   /* ================================================ */
   /* UN PEU DE MENAGE                                 */
   /* ================================================ */

   IndicsTermine();
   FahTermine(FAH);
   free(M);
   return(1);
 } /* llpemeyerkhalimsky() */

/* ==================================== */
int32_t llpemeyer_regular_llpemeyersansligne(
        struct xvimage *image,
        struct xvimage *marqueurs,
        struct xvimage *marqueursfond,
        struct xvimage *masque,
        int32_t connex,
        struct xvimage *result	
)
/* ==================================== */
#undef F_NAME
#define F_NAME "llpemeyersansligne"
{
  register int32_t i;                       /* index muet */
  register int32_t x;                       /* index muet de pixel */
  register int32_t y;                       /* index muet (generalement un voisin de x) */
  register int32_t w;                       /* index muet (generalement un voisin de x) */
  register int32_t k;                       /* index muet */
  int32_t j;
  int32_t rs = rowsize(image);     /* taille ligne */
  int32_t cs = colsize(image);     /* taille colonne */
  int32_t N = rs * cs;             /* taille image */
  uint8_t *F = UCHARDATA(image);      /* l'image de depart */
  uint8_t *B = UCHARDATA(marqueurs);       /* l'image de marqueurs */
  uint8_t *BF;                             /* l'image de marqueurs du fond */
  uint8_t *MA;                             /* l'image de masque */
  uint32_t *M;             /* l'image d'etiquettes */
  int32_t nlabels;                 /* nombre de labels differents */
  Fah * FAH;                   /* la file d'attente hierarchique */
  int32_t etiqcc[4];
  int32_t ncc;  
  int32_t incr_vois;
#ifdef ANIMATE
  int32_t curlev = -1, nimage = 0; 
  char imname[128];
  struct xvimage *animimage;
  uint8_t *A;
#endif

  if (depth(image) != 1) 
  {
    fprintf(stderr, "%s: cette version ne traite pas les images volumiques\n", F_NAME);
    exit(0);
  }

  if (datatype(result) != VFF_TYP_4_BYTE) 
  {
    fprintf(stderr, "%s: le resultat doit etre de type VFF_TYP_4_BYTE\n", F_NAME);
    return 0;
  }

  if ((rowsize(marqueurs) != rs) || (colsize(marqueurs) != cs))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }

  if (marqueursfond && ((rowsize(marqueursfond) != rs) || (colsize(marqueursfond) != cs)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (masque && ((rowsize(masque) != rs) || (colsize(masque) != cs)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (masque) MA = UCHARDATA(masque);
  if (marqueursfond) BF = UCHARDATA(marqueursfond);

  IndicsInit(N);
  FAH = CreeFahVide(N+1);
  if (FAH == NULL)
  {   fprintf(stderr, "%s : CreeFah failed\n", F_NAME);
      return(0);
  }

  switch (connex)
  {
    case 4: incr_vois = 2; break;
    case 8: incr_vois = 1; break;
    default: 
      fprintf(stderr, "%s: mauvaise connexite: %d\n", F_NAME, connex);
      return 0;
  } /* switch (connex) */    

  /* ================================================ */
  /* CREATION DES LABELS INITIAUX                     */
  /* ================================================ */

  M = ULONGDATA(result);
  memset(M, 0, N*sizeof(int32_t));
  nlabels = 0;

  if (marqueursfond)
  {
    nlabels += 1;                 /* tous les marqueurs du fond ont le meme label (1) */
    for (x = 0; x < N; x++)
    {
      if (BF[x] && (M[x] == 0) && (!masque || MA[x]))
      {
        M[x] = nlabels;
        FahPush(FAH, x, 0);
        while (! FahVide(FAH))
        {
          w = FahPop(FAH);
          for (k = 0; k < 8; k += incr_vois)
          {
            y = voisin(w, k, rs, N);
            if ((y != -1) &&  BF[y] && (M[y] == 0) && (!masque || MA[y]))
            {
              M[y] = nlabels;
              FahPush(FAH, y, 0);
            } /* if y ... */
          } /* for k ... */
        } /* while (! FahVide(FAH)) */
      } /* if (M[x] == 0) */
    } /* for (x = 0; x < N; x++) */
  } /* if (marqueursfond) */

  for (x = 0; x < N; x++)
  {
    if (B[x] && (M[x] == 0) && (!masque || MA[x]))
    {
      nlabels += 1;
      M[x] = nlabels;
      FahPush(FAH, x, 0);
      while (! FahVide(FAH))
      {
        w = FahPop(FAH);
        for (k = 0; k < 8; k += incr_vois)
        {
          y = voisin(w, k, rs, N);
          if ((y != -1) &&  B[y] && (M[y] == 0) && (!masque || MA[y]))
          {
            M[y] = nlabels;
            FahPush(FAH, y, 0);
          } /* if y ... */
        } /* for k ... */
      } /* while (! FahVide(FAH)) */
    } /* if (M[x] == 0) */
  } /* for (x = 0; x < N; x++) */

  /* ================================================ */
  /* INITIALISATION DE LA FAH                         */
  /* ================================================ */

  FahFlush(FAH);
  FahPush(FAH, -1, 0);   /* force la creation du niveau 0 dans la Fah. */
                         /* NECESSAIRE pour eviter la creation prematuree */
                         /* de la file d'urgence */ 

  for (x = 0; x < N; x++)
  {
    if (M[x] && (!masque || MA[x]))            /* on va empiler les voisins des regions marquees */
    {
      for (k = 0; k < 8; k += incr_vois)
      {
        y = voisin(x, k, rs, N);
        if ((y != -1) && !M[y] && !IsSet(y, EN_FAH))
        {        
          FahPush(FAH, y, F[y]);
          Set(y, EN_FAH);
        }
      } /* for (k = 0; k < 8; k += 2) */
    } /* if (M[x]) */
  } /* for (x = 0; x < N; x++) */

  x = FahPop(FAH);
#ifdef PARANO
  if (x != -1)
  {   
     fprintf(stderr,"%s : ORDRE FIFO NON RESPECTE PAR LA FAH !!!\n", F_NAME);
     return(0);
  }
#endif

  /* ================================================ */
  /* INONDATION                                       */
  /* ================================================ */

#ifdef ANIMATE
  animimage = copyimage(image);
  A = UCHARDATA(animimage);
#endif
  nlabels += 1;          /* cree le label pour les points de la LPE */
  while (! FahVide(FAH))
  {
    x = FahPop(FAH);
    UnSet(x, EN_FAH);
#ifdef ANIMATE
    if (F[x] > curlev)
    {
      printf("Niveau %d\n", F[x]);
      sprintf(imname, "anim%03d.pgm", nimage); nimage++;
      for (y = 0; y < N; y++)
        if ((M[y] == nlabels) || (M[y] == 0)) A[y] = 255; else A[y] = 0;
      writeimage(animimage, imname);
      curlev = F[x];
    }
#endif

    ncc = 0;
    for (k = 0; k < 8; k += incr_vois)
    {
      y = voisin(x, k, rs, N);
      if ((y != -1) && (M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
      {
        etiqcc[ncc] = M[y];        
        ncc += 1;
      }
    } /* for k */

    /* Here is the labelling */
    M[x] = etiqcc[0];
    for (k = 0; k < 8; k += incr_vois)
    {
      y = voisin(x, k, rs, N);     
      if ((y != -1) && (M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
      {          
	FahPush(FAH, y, F[y]); 
	Set(y, EN_FAH);
      } /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
    } /* for k */

  } /* while (! FahVide(FAH)) */
  /* FIN PROPAGATION */

  /* ================================================ */
  /* UN PEU DE MENAGE                                 */
  /* ================================================ */

  IndicsTermine();
  FahTermine(FAH);
  return(1);
} /*  /* llpemeyersansligne() */

/* ==================================== */
int32_t llpemeyer_regular_llpemeyer3d(
        struct xvimage *image,
        struct xvimage *marqueurs,
        struct xvimage *marqueursfond,
        struct xvimage *masque,
        int32_t connex)
/* ==================================== */
#undef F_NAME
#define F_NAME "llpemeyer3d"
{
  register int32_t i;                       /* index muet */
  register int32_t x;                       /* index muet de pixel */
  register int32_t y;                       /* index muet (generalement un voisin de x) */
  register int32_t w;                       /* index muet (generalement un voisin de x) */
  register int32_t k;                       /* index muet */
  int32_t j;
  int32_t rs = rowsize(image);     /* taille ligne */
  int32_t cs = colsize(image);     /* taille colonne */
  int32_t d = depth(image);        /* nb plans */
  int32_t n = rs * cs;             /* taille plan */
  int32_t N = n * d;               /* taille image */
  uint8_t *F = UCHARDATA(image);      /* l'image de depart */
  uint8_t *B = UCHARDATA(marqueurs);       /* l'image de marqueurs */
  uint8_t *BF;                             /* l'image de marqueurs du fond */
  uint8_t *MA;                             /* l'image de masque */
  uint32_t *M;             /* l'image d'etiquettes */
  int32_t nlabels;                 /* nombre de labels differents */
  Fah * FAH;                   /* la file d'attente hierarchique */
  int32_t etiqcc[6];
  int32_t ncc;  

  if ((rowsize(marqueurs) != rs) || (colsize(marqueurs) != cs))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }

  if (marqueursfond && ((rowsize(marqueursfond) != rs) || (colsize(marqueursfond) != cs) || (depth(marqueursfond) != d)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (marqueursfond) BF = UCHARDATA(marqueursfond);
  if (masque && ((rowsize(masque) != rs) || (colsize(masque) != cs) || (depth(masque) != d)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (masque) MA = UCHARDATA(masque);

  IndicsInit(N);
  FAH = CreeFahVide(N+1);
  if (FAH == NULL)
  {   fprintf(stderr, "%s() : CreeFah failed\n", F_NAME);
      return(0);
  }

  /* ================================================ */
  /* CREATION DES LABELS INITIAUX                     */
  /* ================================================ */

  M = (uint32_t *)calloc(N, sizeof(int32_t));
  if (M == NULL)
  {   fprintf(stderr, "%s() : calloc failed\n", F_NAME);
      return(0);
  }
  nlabels = 0;

  if (marqueursfond)
  {
    nlabels += 1;                 /* tous les marqueurs du fond ont le meme label (1) */
    for (x = 0; x < N; x++)
    {
      if (BF[x] && (M[x] == 0) && (!masque || MA[x]))
      {
        M[x] = nlabels;
        FahPush(FAH, x, 0);
        while (! FahVide(FAH))
        {
          w = FahPop(FAH);
          switch (connex)
          {
	    case 6:
              for (k = 0; k <= 10; k += 2) /* parcourt les 6 voisins */
              {
                y = voisin6(w, k, rs, n, N);
                if ((y != -1) && BF[y] && (M[y] == 0) && (!masque || MA[y]))
                { M[y] = nlabels; FahPush(FAH, y, 0); }
              } /* for k ... */
              break;
	    case 18:
              for (k = 0; k < 18; k += 1) /* parcourt les 18 voisins */
              {
                y = voisin18(w, k, rs, n, N);
                if ((y != -1) && BF[y] && (M[y] == 0) && (!masque || MA[y]))
                { M[y] = nlabels; FahPush(FAH, y, 0); }
              } /* for k ... */
              break;
	    case 26:
              for (k = 0; k < 26; k += 1) /* parcourt les 26 voisins */
              {
                y = voisin26(w, k, rs, n, N);
                if ((y != -1) && BF[y] && (M[y] == 0) && (!masque || MA[y]))
                { M[y] = nlabels; FahPush(FAH, y, 0); }
              } /* for k ... */
              break;
	  } /* switch (connex) */
        } /* while (! FahVide(FAH)) */
      } /* if (M[x] == 0) */
    } /* for (x = 0; x < N; x++) */
  } /* if (marqueursfond) */

  for (x = 0; x < N; x++)
  {
    if (B[x] && (M[x] == 0) && (!masque || MA[x]))
    {
      nlabels += 1;
      M[x] = nlabels;
      FahPush(FAH, x, 0);
      while (! FahVide(FAH))
      {
        w = FahPop(FAH);
        switch (connex)
        {
	  case 6:
            for (k = 0; k <= 10; k += 2)
            {
              y = voisin6(w, k, rs, n, N);
              if ((y!=-1) && (M[y]==0) && (B[y]==B[w]) && (!masque || MA[y]))
              { M[y] = nlabels; FahPush(FAH, y, 0); } /* if y ... */
            } /* for k ... */
            break;
	  case 18:
            for (k = 0; k < 18; k += 1) /* parcourt les 18 voisins */
            {
              y = voisin18(w, k, rs, n, N);
              if ((y!=-1) && (M[y]==0) && (B[y]==B[w]) && (!masque || MA[y]))
              { M[y] = nlabels; FahPush(FAH, y, 0); } /* if y ... */
            } /* for k ... */
            break;
	  case 26:
            for (k = 0; k < 26; k += 1) /* parcourt les 26 voisins */
            {
              y = voisin26(w, k, rs, n, N);
              if ((y!=-1) && (M[y]==0) && (B[y]==B[w]) && (!masque || MA[y]))
              { M[y] = nlabels; FahPush(FAH, y, 0); } /* if y ... */
            } /* for k ... */
            break;
	} /* switch (connex) */
      } /* while (! FahVide(FAH)) */
    } /* if (M[x] == 0) */
  } /* for (x = 0; x < N; x++) */

  /* ================================================ */
  /* INITIALISATION DE LA FAH                         */
  /* ================================================ */

  FahFlush(FAH);
  FahPush(FAH, -1, 0);   /* force la creation du niveau 0 dans la Fah. */
                         /* NECESSAIRE pour eviter la creation prematuree */
                         /* de la file d'urgence */ 

  for (x = 0; x < N; x++)
  {
    if (M[x] && (!masque || MA[x]))    /* on va empiler les voisins des regions marquees */
    {
      switch (connex)
      {
        case 6:
          for (k = 0; k <= 10; k += 2)
          {
            y = voisin6(x, k, rs, n, N);
            if ((y!=-1)&&!M[y]&&!IsSet(y,EN_FAH)){ FahPush(FAH, y, F[y]); Set(y, EN_FAH); }
          } /* for (k = 0; k < 8; k += 2) */
          break;
	  case 18:
            for (k = 0; k < 18; k += 1) /* parcourt les 18 voisins */
            {
              y = voisin18(x, k, rs, n, N);
              if ((y!=-1)&&!M[y]&&!IsSet(y,EN_FAH)){ FahPush(FAH, y, F[y]); Set(y, EN_FAH); }
            } /* for (k = 0; k < 18; k += 1) */
          break;
	  case 26:
            for (k = 0; k < 26; k += 1) /* parcourt les 26 voisins */
            {
              y = voisin26(x, k, rs, n, N);
              if ((y!=-1)&&!M[y]&&!IsSet(y,EN_FAH)){ FahPush(FAH, y, F[y]); Set(y, EN_FAH); }
            } /* for (k = 0; k < 26; k += 1) */
          break;
      } /* switch (connex) */
    } /* if (B[x]) */
  } /* for (x = 0; x < N; x++) */

  x = FahPop(FAH);
#ifdef PARANO
  if (x != -1)
  {   
     fprintf(stderr,"%s : ORDRE FIFO NON RESPECTE PAR LA FAH !!!\n", F_NAME);
     return(0);
  }
#endif

  /* ================================================ */
  /* INONDATION                                       */
  /* ================================================ */

  nlabels += 1;          /* cree le label pour les points de la LPE */
  while (! FahVide(FAH))
  {
    x = FahPop(FAH);
    UnSet(x, EN_FAH);

    ncc = 0;
    switch (connex)
    {
      case 6:
        for (k = 0; k <= 10; k += 2)
        {
          y = voisin6(x, k, rs, n, N);
          if ((y != -1) && (M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
          {
            etiqcc[ncc] = M[y];        
            ncc += 1;
          }
        } /* for k */
        break;
      case 18:
        for (k = 0; k < 18; k += 1) /* parcourt les 18 voisins */
        {
          y = voisin18(x, k, rs, n, N);
          if ((y != -1) && (M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
          {
            etiqcc[ncc] = M[y];        
            ncc += 1;
          }
        } /* for k */
        break;
      case 26:
        for (k = 0; k < 26; k += 1) /* parcourt les 26 voisins */
        {
          y = voisin26(x, k, rs, n, N);
          if ((y != -1) && (M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
          {
            etiqcc[ncc] = M[y];        
            ncc += 1;
          }
        } /* for k */
        break;
    } /* switch (connex) */

    if (ncc == 1)
    {
      M[x] = etiqcc[0];
      switch (connex)
      {
        case 6:
          for (k = 0; k <= 10; k += 2)
          {
            y = voisin6(x, k, rs, n, N);     
            if ((y != -1) && (M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
            {
              FahPush(FAH, y, F[y]);
              Set(y, EN_FAH);
            } /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
          } /* for k */
          break;
        case 18:
          for (k = 0; k < 18; k += 1) /* parcourt les 18 voisins */
          {
            y = voisin18(x, k, rs, n, N);
            if ((y != -1) && (M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
            {
              FahPush(FAH, y, F[y]);
              Set(y, EN_FAH);
            } /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
          } /* for k */
          break;
        case 26:
          for (k = 0; k < 26; k += 1) /* parcourt les 26 voisins */
          {
            y = voisin26(x, k, rs, n, N);
            if ((y != -1) && (M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
            {
              FahPush(FAH, y, F[y]);
              Set(y, EN_FAH);
            } /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
          } /* for k */
          break;
      } /* switch (connex) */
    } 
    else 
    if (ncc > 1)
    {
      M[x] = nlabels;
    }
  } /* while (! FahVide(FAH)) */
  /* FIN PROPAGATION */

  for (x = 0; x < N; x++)
  {
    if ((M[x] == nlabels) || (M[x] == 0)) F[x] = 255; else F[x] = 0;
  }

  /* ================================================ */
  /* UN PEU DE MENAGE                                 */
  /* ================================================ */

  IndicsTermine();
  FahTermine(FAH);
  free(M);
  return(1);
} /* llpemeyer3d() */

/* ==================================== */
int32_t llpemeyer_regular_llpemeyer3dsansligne(
        struct xvimage *image,
        struct xvimage *marqueurs,
        struct xvimage *marqueursfond,
        struct xvimage *masque,
        int32_t connex,
        struct xvimage *result	
)
/* ==================================== */
#undef F_NAME
#define F_NAME "llpemeyer3dsansligne"
{
  register int32_t i;                       /* index muet */
  register int32_t x;                       /* index muet de pixel */
  register int32_t y;                       /* index muet (generalement un voisin de x) */
  register int32_t w;                       /* index muet (generalement un voisin de x) */
  register int32_t k;                       /* index muet */
  int32_t j;
  int32_t rs = rowsize(image);     /* taille ligne */
  int32_t cs = colsize(image);     /* taille colonne */
  int32_t d = depth(image);        /* nb plans */
  int32_t n = rs * cs;             /* taille plan */
  int32_t N = n * d;               /* taille image */
  uint8_t *F = UCHARDATA(image);      /* l'image de depart */
  uint8_t *B = UCHARDATA(marqueurs);       /* l'image de marqueurs */
  uint8_t *BF;                             /* l'image de marqueurs du fond */
  uint8_t *MA;                             /* l'image de masque */
  uint32_t *M;             /* l'image d'etiquettes */
  int32_t nlabels;                 /* nombre de labels differents */
  Fah * FAH;                   /* la file d'attente hierarchique */
  int32_t etiqcc[6];
  int32_t ncc;  

  if (depth(image) == 1) 
  {
    fprintf(stderr, "%s: cette version ne traite que les images volumiques\n", F_NAME);
    exit(0);
  }

  if (datatype(result) != VFF_TYP_4_BYTE) 
  {
    fprintf(stderr, "%s: le resultat doit etre de type VFF_TYP_4_BYTE\n", F_NAME);
    return 0;
  }

  if ((rowsize(marqueurs) != rs) || (colsize(marqueurs) != cs))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }

  if (marqueursfond && ((rowsize(marqueursfond) != rs) || (colsize(marqueursfond) != cs) || (depth(marqueursfond) != d)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (marqueursfond) BF = UCHARDATA(marqueursfond);
  if (masque && ((rowsize(masque) != rs) || (colsize(masque) != cs) || (depth(masque) != d)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (masque) MA = UCHARDATA(masque);

  IndicsInit(N);
  FAH = CreeFahVide(N+1);
  if (FAH == NULL)
  {   fprintf(stderr, "%s() : CreeFah failed\n", F_NAME);
      return(0);
  }

  /* ================================================ */
  /* CREATION DES LABELS INITIAUX                     */
  /* ================================================ */

  M = ULONGDATA(result);
  memset(M, 0, N*sizeof(int32_t));
  nlabels = 0;

  if (marqueursfond)
  {
    nlabels += 1;                 /* tous les marqueurs du fond ont le meme label (1) */
    for (x = 0; x < N; x++)
    {
      if (BF[x] && (M[x] == 0) && (!masque || MA[x]))
      {
        M[x] = nlabels;
        FahPush(FAH, x, 0);
        while (! FahVide(FAH))
        {
          w = FahPop(FAH);
          switch (connex)
          {
	    case 6:
              for (k = 0; k <= 10; k += 2) /* parcourt les 6 voisins */
              {
                y = voisin6(w, k, rs, n, N);
                if ((y != -1) && BF[y] && (M[y] == 0) && (!masque || MA[y]))
                { M[y] = nlabels; FahPush(FAH, y, 0); }
              } /* for k ... */
              break;
	    case 18:
              for (k = 0; k < 18; k += 1) /* parcourt les 18 voisins */
              {
                y = voisin18(w, k, rs, n, N);
                if ((y != -1) && BF[y] && (M[y] == 0) && (!masque || MA[y]))
                { M[y] = nlabels; FahPush(FAH, y, 0); }
              } /* for k ... */
              break;
	    case 26:
              for (k = 0; k < 26; k += 1) /* parcourt les 26 voisins */
              {
                y = voisin26(w, k, rs, n, N);
                if ((y != -1) && BF[y] && (M[y] == 0) && (!masque || MA[y]))
                { M[y] = nlabels; FahPush(FAH, y, 0); }
              } /* for k ... */
              break;
	  } /* switch (connex) */
        } /* while (! FahVide(FAH)) */
      } /* if (M[x] == 0) */
    } /* for (x = 0; x < N; x++) */
  } /* if (marqueursfond) */

  for (x = 0; x < N; x++)
  {
    if (B[x] && (M[x] == 0) && (!masque || MA[x]))
    {
      nlabels += 1;
      M[x] = nlabels;
      FahPush(FAH, x, 0);
      while (! FahVide(FAH))
      {
        w = FahPop(FAH);
        switch (connex)
        {
	  case 6:
            for (k = 0; k <= 10; k += 2)
            {
              y = voisin6(w, k, rs, n, N);
              if ((y!=-1) && (M[y]==0) && (B[y]==B[w]) && (!masque || MA[y]))
              { M[y] = nlabels; FahPush(FAH, y, 0); } /* if y ... */
            } /* for k ... */
            break;
	  case 18:
            for (k = 0; k < 18; k += 1) /* parcourt les 18 voisins */
            {
              y = voisin18(w, k, rs, n, N);
              if ((y!=-1) && (M[y]==0) && (B[y]==B[w]) && (!masque || MA[y]))
              { M[y] = nlabels; FahPush(FAH, y, 0); } /* if y ... */
            } /* for k ... */
            break;
	  case 26:
            for (k = 0; k < 26; k += 1) /* parcourt les 26 voisins */
            {
              y = voisin26(w, k, rs, n, N);
              if ((y!=-1) && (M[y]==0) && (B[y]==B[w]) && (!masque || MA[y]))
              { M[y] = nlabels; FahPush(FAH, y, 0); } /* if y ... */
            } /* for k ... */
            break;
	} /* switch (connex) */
      } /* while (! FahVide(FAH)) */
    } /* if (M[x] == 0) */
  } /* for (x = 0; x < N; x++) */

  /* ================================================ */
  /* INITIALISATION DE LA FAH                         */
  /* ================================================ */

  FahFlush(FAH);
  FahPush(FAH, -1, 0);   /* force la creation du niveau 0 dans la Fah. */
                         /* NECESSAIRE pour eviter la creation prematuree */
                         /* de la file d'urgence */ 

  for (x = 0; x < N; x++)
  {
    if (M[x] && (!masque || MA[x]))    /* on va empiler les voisins des regions marquees */
    {
      switch (connex)
      {
        case 6:
          for (k = 0; k <= 10; k += 2)
          {
            y = voisin6(x, k, rs, n, N);
            if ((y!=-1)&&!M[y]&&!IsSet(y,EN_FAH)){ FahPush(FAH, y, F[y]); Set(y, EN_FAH); }
          } /* for (k = 0; k < 8; k += 2) */
          break;
	  case 18:
            for (k = 0; k < 18; k += 1) /* parcourt les 18 voisins */
            {
              y = voisin18(x, k, rs, n, N);
              if ((y!=-1)&&!M[y]&&!IsSet(y,EN_FAH)){ FahPush(FAH, y, F[y]); Set(y, EN_FAH); }
            } /* for (k = 0; k < 18; k += 1) */
          break;
	  case 26:
            for (k = 0; k < 26; k += 1) /* parcourt les 26 voisins */
            {
              y = voisin26(x, k, rs, n, N);
              if ((y!=-1)&&!M[y]&&!IsSet(y,EN_FAH)){ FahPush(FAH, y, F[y]); Set(y, EN_FAH); }
            } /* for (k = 0; k < 26; k += 1) */
          break;
      } /* switch (connex) */
    } /* if (B[x]) */
  } /* for (x = 0; x < N; x++) */

  x = FahPop(FAH);
#ifdef PARANO
  if (x != -1)
  {   
     fprintf(stderr,"%s : ORDRE FIFO NON RESPECTE PAR LA FAH !!!\n", F_NAME);
     return(0);
  }
#endif

  /* ================================================ */
  /* INONDATION                                       */
  /* ================================================ */

  nlabels += 1;          /* cree le label pour les points de la LPE */
  while (! FahVide(FAH))
  {
    x = FahPop(FAH);
    UnSet(x, EN_FAH);

    ncc = 0;
    switch (connex)
    {
      case 6:
        for (k = 0; k <= 10; k += 2)
        {
          y = voisin6(x, k, rs, n, N);
          if ((y != -1) && (M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
          {
            etiqcc[ncc] = M[y];        
            ncc += 1;
          }
        } /* for k */
        break;
      case 18:
        for (k = 0; k < 18; k += 1) /* parcourt les 18 voisins */
        {
          y = voisin18(x, k, rs, n, N);
          if ((y != -1) && (M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
          {
            etiqcc[ncc] = M[y];        
            ncc += 1;
          }
        } /* for k */
        break;
      case 26:
        for (k = 0; k < 26; k += 1) /* parcourt les 26 voisins */
        {
          y = voisin26(x, k, rs, n, N);
          if ((y != -1) && (M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
          {
            etiqcc[ncc] = M[y];        
            ncc += 1;
          }
        } /* for k */
        break;
    } /* switch (connex) */

    /* Label */
      M[x] = etiqcc[0];
      switch (connex)
      {
        case 6:
          for (k = 0; k <= 10; k += 2)
          {
            y = voisin6(x, k, rs, n, N);     
            if ((y != -1) && (M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
            {
              FahPush(FAH, y, F[y]);
              Set(y, EN_FAH);
            } /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
          } /* for k */
          break;
        case 18:
          for (k = 0; k < 18; k += 1) /* parcourt les 18 voisins */
          {
            y = voisin18(x, k, rs, n, N);
            if ((y != -1) && (M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
            {
              FahPush(FAH, y, F[y]);
              Set(y, EN_FAH);
            } /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
          } /* for k */
          break;
        case 26:
          for (k = 0; k < 26; k += 1) /* parcourt les 26 voisins */
          {
            y = voisin26(x, k, rs, n, N);
            if ((y != -1) && (M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
            {
              FahPush(FAH, y, F[y]);
              Set(y, EN_FAH);
            } /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
          } /* for k */
          break;
      } /* switch (connex) */
  } /* while (! FahVide(FAH)) */
  /* FIN PROPAGATION */

  /* ================================================ */
  /* UN PEU DE MENAGE                                 */
  /* ================================================ */

  IndicsTermine();
  FahTermine(FAH);
  return(1);
} /* llpemeyer3dsansligne() */

/* ==================================== */
int32_t llpemeyer_regular_llpemeyer3d2(
        struct xvimage *image,
        struct xvimage *marqueurs,
        struct xvimage *masque,
        int32_t connex)
/* ==================================== */
#undef F_NAME
#define F_NAME "llpemeyer3d2"
{
  fprintf(stderr, "%s: not yet implemented\n", F_NAME);
  return 0;

  return(1);
} /* llpemeyer3d2() */



/* ==================================== */
int32_t llpemeyer_regular_llpemeyerbiconnecte(
        struct xvimage *image,
        struct xvimage *marqueurs,
        struct xvimage *marqueursfond,
        struct xvimage *masque,
        int32_t parite)
/* ==================================== */
#undef F_NAME
#define F_NAME "llpemeyerbiconnecte"
{
  register int32_t i;                       /* index muet */
  register int32_t x;                       /* index muet de pixel */
  register int32_t y;                       /* index muet (generalement un voisin de x) */
  register int32_t w;                       /* index muet (generalement un voisin de x) */
  register int32_t k;                       /* index muet */
  int32_t j;
  int32_t rs = rowsize(image);     /* taille ligne */
  int32_t cs = colsize(image);     /* taille colonne */
  int32_t N = rs * cs;             /* taille image */
  uint8_t *F = UCHARDATA(image);      /* l'image de depart */
  uint8_t *B = UCHARDATA(marqueurs);       /* l'image de marqueurs */
  uint8_t *BF;                             /* l'image de marqueurs du fond */
  uint8_t *MA;                             /* l'image de masque */
  uint32_t *M;             /* l'image d'etiquettes */
  int32_t nlabels;                 /* nombre de labels differents */
  Fah * FAH;                   /* la file d'attente hierarchique */
  int32_t etiqcc[4];
  int32_t ncc;  
  const int32_t incr_vois=1;
#ifdef ANIMATE
  int32_t curlev = -1, nimage = 0; 
  char imname[128];
  struct xvimage *animimage;
  uint8_t *A;
#endif
  if (depth(image) != 1) 
  {
    fprintf(stderr, "%s: cette version ne traite pas les images volumiques\n", F_NAME);
    exit(0);
  }

  if ((rowsize(marqueurs) != rs) || (colsize(marqueurs) != cs))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }

  if (marqueursfond && ((rowsize(marqueursfond) != rs) || (colsize(marqueursfond) != cs)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (masque && ((rowsize(masque) != rs) || (colsize(masque) != cs)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (masque) MA = UCHARDATA(masque);
  if (marqueursfond) BF = UCHARDATA(marqueursfond);

  IndicsInit(N);
  FAH = CreeFahVide(N+1);
  if (FAH == NULL)
  {   fprintf(stderr, "%s : CreeFah failed\n", F_NAME);
      return(0);
  }


  /* ================================================ */
  /* CREATION DES LABELS INITIAUX                     */
  /* ================================================ */

  M = (uint32_t *)calloc(N, sizeof(int32_t));
  if (M == NULL)
  {   fprintf(stderr, "%s : calloc failed\n", F_NAME);
      return(0);
  }
  nlabels = 0;

  if (marqueursfond)
  {
    nlabels += 1;                 /* tous les marqueurs du fond ont le meme label (1) */
    for (x = 0; x < N; x++)
    {
      if (BF[x] && (M[x] == 0) && (!masque || MA[x]))
      {
        M[x] = nlabels;
        FahPush(FAH, x, 0);
        while (! FahVide(FAH))
        {
          w = FahPop(FAH);
          for (k = 0; k < 6; k += incr_vois)
          {
            y = voisin6b(w, k, rs, N, parite);
            if ((y != -1) &&  BF[y] && (M[y] == 0) && (!masque || MA[y]))
            {
              M[y] = nlabels;
              FahPush(FAH, y, 0);
            } /* if y ... */
          } /* for k ... */
        } /* while (! FahVide(FAH)) */
      } /* if (M[x] == 0) */
    } /* for (x = 0; x < N; x++) */
  } /* if (marqueursfond) */

  for (x = 0; x < N; x++)
  {
    if (B[x] && (M[x] == 0) && (!masque || MA[x]))
    {
      nlabels += 1;
      M[x] = nlabels;
      FahPush(FAH, x, 0);
      while (! FahVide(FAH))
      {
        w = FahPop(FAH);
        for (k = 0; k < 6; k += incr_vois)
        {
          y = voisin6b(w, k, rs, N, parite);
          if ((y != -1) &&  B[y] && (M[y] == 0) && (!masque || MA[y]))
          {
            M[y] = nlabels;
            FahPush(FAH, y, 0);
          } /* if y ... */
        } /* for k ... */
      } /* while (! FahVide(FAH)) */
    } /* if (M[x] == 0) */
  } /* for (x = 0; x < N; x++) */

  /* ================================================ */
  /* INITIALISATION DE LA FAH                         */
  /* ================================================ */

  FahFlush(FAH);
  FahPush(FAH, -1, 0);   /* force la creation du niveau 0 dans la Fah. */
                         /* NECESSAIRE pour eviter la creation prematuree */
                         /* de la file d'urgence */ 

  for (x = 0; x < N; x++)
  {
    if (M[x] && (!masque || MA[x]))            /* on va empiler les voisins des regions marquees */
    {
      for (k = 0; k < 6; k += incr_vois)
      {
        y = voisin6b(x, k, rs, N, parite);
        if ((y != -1) && !M[y] && !IsSet(y, EN_FAH))
        {        
          FahPush(FAH, y, F[y]);
          Set(y, EN_FAH);
        }
      } /* for (k = 0; k < 8; k += 2) */
    } /* if (M[x]) */
  } /* for (x = 0; x < N; x++) */

  x = FahPop(FAH);
#ifdef PARANO
  if (x != -1)
  {   
     fprintf(stderr,"%s : ORDRE FIFO NON RESPECTE PAR LA FAH !!!\n", F_NAME);
     return(0);
  }
#endif

  /* ================================================ */
  /* INONDATION                                       */
  /* ================================================ */

#ifdef ANIMATE
  animimage = copyimage(image);
  A = UCHARDATA(animimage);
#endif
  nlabels += 1;          /* cree le label pour les points de la LPE */
  while (! FahVide(FAH))
  {
    x = FahPop(FAH);
    UnSet(x, EN_FAH);
#ifdef ANIMATE
    if (F[x] > curlev)
    {
      printf("Niveau %d\n", F[x]);
      sprintf(imname, "anim%03d.pgm", nimage); nimage++;
      for (y = 0; y < N; y++)
        if ((M[y] == nlabels) || (M[y] == 0)) A[y] = 255; else A[y] = 0;
      writeimage(animimage, imname);
      curlev = F[x];
    }
#endif

    ncc = 0;
    for (k = 0; k < 6; k += incr_vois)
    {
      y = voisin6b(x, k, rs, N, parite);
      if ((y != -1) && (M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
      {
        etiqcc[ncc] = M[y];        
        ncc += 1;
      }
    } /* for k */

    if (ncc == 1)
    {
      M[x] = etiqcc[0];
      for (k = 0; k < 6; k += incr_vois)
      {
        y = voisin6b(x, k, rs, N, parite);     
        if ((y != -1) && (M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
        {          
          FahPush(FAH, y, F[y]); 
          Set(y, EN_FAH);
        } /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
      } /* for k */
    } 
    else 
    if (ncc > 1)
    {
      M[x] = nlabels;
    }

  } /* while (! FahVide(FAH)) */
  /* FIN PROPAGATION */

  for (x = 0; x < N; x++)
  {
    if ((M[x] == nlabels) || (M[x] == 0)) F[x] = 255; else F[x] = 0;
  }

  /* ================================================ */
  /* UN PEU DE MENAGE                                 */
  /* ================================================ */

  IndicsTermine();
  FahTermine(FAH);
  free(M);
  return(1);
} /*  /* llpemeyer() */


/* ==================================== */
int32_t llpemeyer_regular_llpemeyerbiconnecte3d(
        struct xvimage *image,
        struct xvimage *marqueurs,
        struct xvimage *marqueursfond,
        struct xvimage *masque)
/* ==================================== */
#undef F_NAME
#define F_NAME "llpemeyerbiconnecte3d"
{
  register int32_t i;                       /* index muet */
  register int32_t x;                       /* index muet de pixel */
  register int32_t y;                       /* index muet (generalement un voisin de x) */
  register int32_t w;                       /* index muet (generalement un voisin de x) */
  register int32_t k;                       /* index muet */
  int32_t j;
  int32_t rs = rowsize(image);     /* taille ligne */
  int32_t cs = colsize(image);     /* taille colonne */
  int32_t d = depth(image);        /* nb plans */
  int32_t n = rs * cs;             /* taille plan */
  int32_t N = n * d;               /* taille image */
  uint8_t *F = UCHARDATA(image);      /* l'image de depart */
  uint8_t *B = UCHARDATA(marqueurs);       /* l'image de marqueurs */
  uint8_t *BF;                             /* l'image de marqueurs du fond */
  uint8_t *MA;                             /* l'image de masque */
  uint32_t *M;             /* l'image d'etiquettes */
  int32_t nlabels;                 /* nombre de labels differents */
  Fah * FAH;                   /* la file d'attente hierarchique */
  int32_t etiqcc[6];
  int32_t ncc;  

  if ((rowsize(marqueurs) != rs) || (colsize(marqueurs) != cs))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }

  if (marqueursfond && ((rowsize(marqueursfond) != rs) || (colsize(marqueursfond) != cs) || (depth(marqueursfond) != d)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (marqueursfond) BF = UCHARDATA(marqueursfond);
  if (masque && ((rowsize(masque) != rs) || (colsize(masque) != cs) || (depth(masque) != d)))
  {
    fprintf(stderr, "%s: incompatible image sizes\n", F_NAME);
    return 0;
  }
  if (masque) MA = UCHARDATA(masque);

  IndicsInit(N);
  FAH = CreeFahVide(N+1);
  if (FAH == NULL)
  {   fprintf(stderr, "%s() : CreeFah failed\n", F_NAME);
      return(0);
  }

  /* ================================================ */
  /* CREATION DES LABELS INITIAUX                     */
  /* ================================================ */
  printf("%s: creation des labels initiaux\n", F_NAME);
  M = (uint32_t *)calloc(N, sizeof(int32_t));
  if (M == NULL)
  {   fprintf(stderr, "%s() : calloc failed\n", F_NAME);
      return(0);
  }
  nlabels = 0;

  if (marqueursfond)
  {
    nlabels += 1;                 /* tous les marqueurs du fond ont le meme label (1) */
    for (x = 0; x < N; x++)
    {
      if (BF[x] && (M[x] == 0) && (!masque || MA[x]))
      {
        M[x] = nlabels;
        FahPush(FAH, x, 0);
        while (! FahVide(FAH))
        {
          w = FahPop(FAH);
	  for (k = 0; k < 14; k ++ ) /* parcourt les 14 voisins */
              {
                y = voisin14b(w, k, rs, n, N);
                if ((y != -1) && BF[y] && (M[y] == 0) && (!masque || MA[y]))
                { M[y] = nlabels; FahPush(FAH, y, 0); }
              } /* for k ... */
        } /* while (! FahVide(FAH)) */
      } /* if (M[x] == 0) */
    } /* for (x = 0; x < N; x++) */
  } /* if (marqueursfond) */

  for (x = 0; x < N; x++)
  {
    if (B[x] && (M[x] == 0) && (!masque || MA[x]))
    {
      nlabels += 1;
      M[x] = nlabels;
      FahPush(FAH, x, 0);
      while (! FahVide(FAH))
      {
        w = FahPop(FAH);
	for (k = 0; k < 14; k ++)
            {
              y = voisin14b(w, k, rs, n, N);
              if ((y!=-1) && (M[y]==0) && (B[y]==B[w]) && (!masque || MA[y]))
              {M[y] = nlabels; FahPush(FAH, y, 0); } /* if y ... */
            } /* for k ... */
      } /* while (! FahVide(FAH)) */
    } /* if (M[x] == 0) */
  } /* for (x = 0; x < N; x++) */
  
  /* ================================================ */
  /* INITIALISATION DE LA FAH                         */
  /* ================================================ */

  printf("%s: initialisation FAH\n", F_NAME);
  FahFlush(FAH);
  FahPush(FAH, -1, 0);   /* force la creation du niveau 0 dans la Fah. */
                         /* NECESSAIRE pour eviter la creation prematuree */
                         /* de la file d'urgence */ 

  for (x = 0; x < N; x++)
  {
    if (M[x] && (!masque || MA[x]))    /* on va empiler les voisins des regions marquees */
    {
      for (k = 0; k < 14; k ++)
      {
	y = voisin14b(x, k, rs, n, N);
	if ((y!=-1)&&!M[y]&&!IsSet(y,EN_FAH)){FahPush(FAH, y, F[y]); Set(y, EN_FAH); }
      } /* for (k = 0; k < 8; k += 2) */
    } /* if (B[x]) */
  } /* for (x = 0; x < N; x++) */  
  x = FahPop(FAH);
#ifdef PARANO
  if (x != -1)
  {   
    fprintf(stderr,"%s : ORDRE FIFO NON RESPECTE PAR LA FAH !!!\n", F_NAME);
    return(0);
  }
#endif
  
  /* ================================================ */
  /* INONDATION                                       */
  /* ================================================ */
  printf("%s: Innondation \n", F_NAME);
  nlabels += 1;          /* cree le label pour les points de la LPE */
  while (! FahVide(FAH))
  {
    x = FahPop(FAH);
    UnSet(x, EN_FAH);
    
    ncc = 0;
    
    for (k = 0; k < 14; k ++)
    {
      y = voisin14b(x, k, rs, n, N);
      if(y >= N)
      {
	printf("%s: Attention on innonde en dehors de l'image %d %d rs %d, n %d ,N %d \n", F_NAME, x,k, rs, n ,N);
	exit(-1);
      }
      if ((y != -1) && (M[y] != 0) && (M[y] != nlabels) && NotIn(M[y], etiqcc, ncc)) 
      {
	etiqcc[ncc] = M[y];        	
	ncc += 1;
      }
    } /* for k */
        
    if (ncc == 1)
    {
      M[x] = etiqcc[0];
      
      for (k = 0; k < 14; k ++)
      {
	y = voisin14b(x, k, rs, n, N);     
	if ((y != -1) && (M[y] == 0) && (! IsSet(y, EN_FAH)) && (!masque || MA[y]))
	{
	  FahPush(FAH, y, F[y]);
	  Set(y, EN_FAH);
	} /* if ((y != -1) && (! IsSet(y, EN_FAH))) */
      } /* for k */
      
    } 
    else 
      if (ncc > 1)
      {
	M[x] = nlabels;
      }
  } /* while (! FahVide(FAH)) */
  /* FIN PROPAGATION */
  
  for (x = 0; x < N; x++)
  {
    if ((M[x] == nlabels) || (M[x] == 0)) F[x] = 255; else F[x] = 0;
  }

  /* ================================================ */
  /* UN PEU DE MENAGE                                 */
  /* ================================================ */

  IndicsTermine();
  FahTermine(FAH);
  free(M);
  return(1);
} /* llpemeyerbiconnecte3d() */

