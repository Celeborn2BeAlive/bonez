/* $Id: lhtkernu.h,v 1.1.1.1 2008-11-25 08:02:37 mcouprie Exp $ */
/* ============== */
/* prototype for lhtkernu.c */
/* ============== */

extern int32_t lhtkernu_lhtkernu(
        struct xvimage *image,
        int32_t nitermax,
        int32_t connex
);
