/* $Id: llvkern.h,v 1.1.1.1 2008-11-25 08:02:37 mcouprie Exp $ */
/* ============== */
/* prototype for llvkern.c */
/* ============== */

extern int32_t llvkern(
        struct xvimage *image,
        int32_t nitermax,
        int32_t connex
);

