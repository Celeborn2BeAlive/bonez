/* $Id: llpemeyer3d.h,v 1.1.1.1 2008-11-25 08:02:37 mcouprie Exp $ */
/* ============== */
/* prototype for llpemeyer3d.c */
/* ============== */

extern int32_t llpemeyer3d_llpemeyer3d(
        struct xvimage *image,
        struct xvimage *marqueurs,
        int32_t trace
);

