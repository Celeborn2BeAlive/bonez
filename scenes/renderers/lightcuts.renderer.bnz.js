{
    "type": "IntegrationRenderer",
    "pixelSampler": {
        "type": "JitteredPixelSampler",
        "spp": 4
    },
    "integrationAlgorithm": {
        "type": "LightcutsIntegrator",
        "vplSampling": {
            "maxDepth" : 6,
            "minDepth" : 6,
            "nbVPLs" : 32000,
            "indirectOnly": true,
            
            "importanceEstimation": {
                "viewRayCount": 16384,
                "scatteringRayCount": 32,
                "useSkeleton": true,
                "skelImportanceFilter": "maxBallMean",
                "useSkelClustering": false,
                "useEnvMap": false,
                "epsilon": 0.00000001
            },
            
            "_georgievResampling": {
                "epsilon": 0.00000001,
                "viewRayCountX": 20,
                "viewRayCountY": 10,
                
                "ptViewRayCountX": 10,
                "ptViewRayCountY": 5,
                "ptMaxDepth": 8,
                
                "_incidentPowerEstimate": [
                    0.280832,
                    0.2487168,
                    0.1699628
                ]
            }
        },
        "distanceClamping": 0,
        "relativeErrorBound": 0.02,
        "lightcutMaxSize": 2000,
        "shading": {
            "model": {
                "type": "PhongShadingModel"
            },
            "sampler": {
                "type": "ShadingModelSampler"
            }
        }
    }
}
