{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 1,
        "sppY": 1
    },
    "integrator": {
        "type": "SkelImportanceDistributionIntegrator",
        "viewRayCountX": 256,
        "viewRayCountY": 128,
        "scatteringRayCountX": 8,
        "scatteringRayCountY": 8,
        "useClustering": false,
        "importanceFilter": "maxBallMean"
    }
}
