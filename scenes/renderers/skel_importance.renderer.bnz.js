{
    "type": "IntegrationRenderer",
    "pixelSampler": {
        "type": "JitteredPixelSampler",
        "spp": 4
    },
    "integrationAlgorithm": {
        "type": "SkelImportanceDistributionIntegrator",
        "viewRayCount": 16384,
        "scatteringRayCount": 32,
        "dataType": "densityOnMax",
        "filterType": "identity",
        "useSkelClustering": true,
        "shading": {
            "model": {
                "type": "PhongShadingModel"
            },
            "sampler": {
                "type": "ShadingModelSampler"
            }
        }
    }
}
