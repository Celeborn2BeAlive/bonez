{
    "type": "IntegrationRenderer",
    "pixelSampler": {
        "type": "JitteredPixelSampler",
        "spp": 4
    },
    "integrationAlgorithm": {
        "type": "ImportanceIntegrator",
        "viewRayCount": 128,
        "shading": {
            "model": {
                "type": "PhongShadingModel"
            },
            "sampler": {
                "type": "ShadingModelSampler"
            }
        }
    }
}
