{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 1,
        "sppY": 1
    },
    "integrator": {
        "type": "VPLIntegrator",
        "vplSampler": {
            "type": "RandomVPLSampler",
            "minDepth": 2,
            "maxDepth": 2,
            "vplCount": 1,
            "indirectOnly": false,
        },
        "distanceClamping": 0,
        "rrTreshold": 0
    }
}
