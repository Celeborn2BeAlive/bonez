{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 2,
        "sppY": 2
    },
    "integrator": {
        "type": "SkelNodeIntegrator",
    }
}
