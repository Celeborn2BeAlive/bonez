{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 4,
        "sppY": 4
    },
    "integrator": {
        "type": "VPLIntegrator",
        "vplSampler": {
            "type": "CurvSkelVPLSampler",
            "minDepth": 6,
            "maxDepth": 6,
            "vplCount": 1024,
            "indirectOnly": true,
            
            "skelImportance": {
                "viewRayCountX": 256,
                "viewRayCountY": 128,
                "scatteringRayCountX": 8,
                "scatteringRayCountY": 8,
                "epsilon": 0.00000001,
                "importanceFilter": "maxBallMean"
            },
            
            "georgiev": {
                "type": "GeorgievVPLFilter",
                "viewRayCountX": 8,
                "viewRayCountY": 8,
                "epsilon": 0.00000001,
                "integratorSampler": {
                    "type": "StratifiedSampler",
                    "sppX": 256,
                    "sppY": 128
                },
                "integrator": {
                    "type": "PathtraceIntegrator",
                    "maxDepth": 8,
                    "indirectOnly": true
                },
                "useFiltering": true,
                "useResampling": false,
                "vplCount": 8192
            },
            
            "sensorCountPerNode": 4,
        },
        "distanceClamping": 0,
        "rrTreshold": 0
    }
}
