{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 1,
        "sppY": 1
    },
    "integrator": {
        "type": "BidirPathtraceIntegrator",
        "maxDepth": 4,
        "indirectOnly": false,
    }
}
