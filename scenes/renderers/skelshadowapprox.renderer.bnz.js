{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 1,
        "sppY": 1
    },
    "integrator": {
        "type": "SkelPTShadowApproxIntegrator",
        "maxDepth": 8,
        "indirectOnly": true
    }
}
