{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 32,
        "sppY": 32
    },
    "integrator": {
        "type": "BidirPathtraceIntegrator",
        "maxDepth": 4,
        "indirectOnly": false,
    }
}
