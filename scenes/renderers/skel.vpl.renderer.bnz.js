{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 4,
        "sppY": 4
    },
    "integrator": {
        "type": "VPLIntegrator",
        "vplSampler": {
            "type": "RandomVPLSampler",
            "minDepth": 6,
            "maxDepth": 6,
            "vplCount": 1000000,
            "indirectOnly": true,
            
            "vplFilter": {
                "type": "CurvSkelVPLFilter",
                "viewRayCountX": 256,
                "viewRayCountY": 128,
                "scatteringRayCountX": 8,
                "scatteringRayCountY": 8,
                "useClustering": false,
                "importanceFilter": "maxBallMean50",
                "epsilon": 0.00000001,
                "useFiltering": false,
                "useResampling": true,
                "vplCount": 8192
            }
        },
        "distanceClamping": 0,
        "rrTreshold": 0
    }
}
