{
    "models": [ 
        "../assets/..."
    ],
    
    "skeleton": "../assets/...vskel",
    
    "lights": [
        {
            "type": "QuadLight",
            "Le" : [12000, 12000, 12000],
            "transform" : {
                "vx": [50, 0, 0],
                "vy": [0, 0, 50],
                "vz": [0, -1, 0],
                "p": [1050, 780, 375]
            }
        }
    ]
}
