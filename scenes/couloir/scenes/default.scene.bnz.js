{
    "models": [ 
        "../assets/vence_couloirS2.obj"
    ],
    
    "skeleton": "../assets/skelcurv2.vskel",
    
    "lights": [
        {
            "type": "QuadLight",
            "Le" : [50, 50, 50],
            "transform": {
                "vx": [1, 0, 0],
                "vy": [0, 1, 0],
                "vz": [0, 0, 1],
                "p": [-12.7, 7.0, 7.13]
            }
        }
    ]
}
