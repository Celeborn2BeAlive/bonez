{
	"gamma" : 1,
	"image" : "results/light-from-bell-tower_camera2_pathtrace.1369320307.result.bnz.exr",
	"renderer" : {
		"integrator" : {
			"indirectOnly" : false,
			"maxDepth" : 8,
			"type" : "PathtraceIntegrator"
		},
		"sampler" : {
			"sppX" : 1,
			"sppY" : 1,
			"type" : "StratifiedSampler"
		},
		"type" : "IntegratorRenderer"
	},
	"statistics" : {
		"frameIdx" : 0,
		"incidentPowerEstimate" : [
			0.478311,
			0.358374,
			0.271868
		],
		"integratorPreprocess" : {},
		"sumLi" : [
			182701,
			136889,
			103846
		]
	},
	"time" : 0.282147
}