{
	"gamma" : 1,
	"image" : "results/light-from-bell-tower__normal.1369324367.result.bnz.exr",
	"renderer" : {
		"integrator" : {
			"type" : "NormalIntegrator"
		},
		"sampler" : {
			"sppX" : 1,
			"sppY" : 1,
			"type" : "StratifiedSampler"
		},
		"type" : "IntegratorRenderer"
	},
	"statistics" : {
		"frameIdx" : 0,
		"incidentPowerEstimate" : [
			-0.607889,
			0.125477,
			-0.10256
		],
		"integratorPreprocess" : {},
		"sumLi" : [
			-232196,
			47928.8,
			-39175.2
		]
	},
	"time" : 0.021487
}