{
	"gamma" : 1,
	"image" : "results/light-from-bell-tower_camera0_pathtrace.1369320236.result.bnz.exr",
	"renderer" : {
		"integrator" : {
			"indirectOnly" : false,
			"maxDepth" : 8,
			"type" : "PathtraceIntegrator"
		},
		"sampler" : {
			"sppX" : 1,
			"sppY" : 1,
			"type" : "StratifiedSampler"
		},
		"type" : "IntegratorRenderer"
	},
	"statistics" : {
		"frameIdx" : 0,
		"incidentPowerEstimate" : [
			0.57148,
			0.440974,
			0.334835
		],
		"integratorPreprocess" : {},
		"sumLi" : [
			218289,
			168440,
			127897
		]
	},
	"time" : 0.284217
}