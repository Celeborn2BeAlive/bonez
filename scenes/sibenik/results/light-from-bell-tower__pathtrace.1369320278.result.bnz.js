{
	"gamma" : 1,
	"image" : "results/light-from-bell-tower__pathtrace.1369320278.result.bnz.exr",
	"renderer" : {
		"integrator" : {
			"indirectOnly" : false,
			"maxDepth" : 8,
			"type" : "PathtraceIntegrator"
		},
		"sampler" : {
			"sppX" : 1,
			"sppY" : 1,
			"type" : "StratifiedSampler"
		},
		"type" : "IntegratorRenderer"
	},
	"statistics" : {
		"frameIdx" : 0,
		"incidentPowerEstimate" : [
			0.273901,
			0.208067,
			0.154076
		],
		"integratorPreprocess" : {},
		"sumLi" : [
			104623,
			79475.8,
			58852.5
		]
	},
	"time" : 0.27453
}