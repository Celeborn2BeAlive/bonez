{
    "models": [ 
        "../assets/sibenik.obj"
    ],
    
    "skeleton": "../assets/sibenik.vskel",
    
    "lights": [
        {
            "type": "QuadLight",
            "Le": [100, 100, 100],
            "transform": {
                "vx": [0, 0, -4],
                "vy": [4, 0, 0],
                "vz": [0, -4, 0],
                "p": [7.26959, 10.7325, -0.124931]
            }
        }
    ]
}
