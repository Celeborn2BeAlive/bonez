{
	"gamma" : 3,
	"image" : "batch1/results/light-from-bell-tower_camera3_pathtrace.1369324642.result.bnz.exr",
	"renderer" : {
		"integrator" : {
			"indirectOnly" : false,
			"maxDepth" : 8,
			"type" : "PathtraceIntegrator"
		},
		"sampler" : {
			"sppX" : 1,
			"sppY" : 1,
			"type" : "StratifiedSampler"
		},
		"type" : "IntegratorRenderer"
	},
	"statistics" : {
		"frameIdx" : 0,
		"incidentPowerEstimate" : [
			0,
			0,
			0
		],
		"integratorPreprocess" : {},
		"sumLi" : [
			0,
			0,
			0
		]
	},
	"time" : 0.025471
}