{
	"gamma" : 3,
	"image" : "batch1/results/light-from-bell-tower_camera0_normal.1369324640.result.bnz.exr",
	"renderer" : {
		"integrator" : {
			"type" : "NormalIntegrator"
		},
		"sampler" : {
			"sppX" : 1,
			"sppY" : 1,
			"type" : "StratifiedSampler"
		},
		"type" : "IntegratorRenderer"
	},
	"statistics" : {
		"frameIdx" : 0,
		"incidentPowerEstimate" : [
			-0.595312,
			0.140254,
			-0.197734
		],
		"integratorPreprocess" : {},
		"sumLi" : [
			-227392,
			53573.1,
			-75528.7
		]
	},
	"time" : 0.0179539
}