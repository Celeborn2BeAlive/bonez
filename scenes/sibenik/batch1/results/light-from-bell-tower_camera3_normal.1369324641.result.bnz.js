{
	"gamma" : 3,
	"image" : "batch1/results/light-from-bell-tower_camera3_normal.1369324641.result.bnz.exr",
	"renderer" : {
		"integrator" : {
			"type" : "NormalIntegrator"
		},
		"sampler" : {
			"sppX" : 1,
			"sppY" : 1,
			"type" : "StratifiedSampler"
		},
		"type" : "IntegratorRenderer"
	},
	"statistics" : {
		"frameIdx" : 0,
		"incidentPowerEstimate" : [
			0.599008,
			0.121157,
			0.188677
		],
		"integratorPreprocess" : {},
		"sumLi" : [
			228804,
			46278.6,
			72069.3
		]
	},
	"time" : 0.0134199
}