{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 32,
        "sppY": 32
    },
    "integrator": {
        "type": "SkelMISPathtraceIntegrator",
        "maxDepth": 4,
        "indirectOnly": false,
        "skelStrength": 16,
        "skelProb": 0.5
    }
}
