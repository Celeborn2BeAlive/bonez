{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 32,
        "sppY": 32
    },
    "integrator": {
        "type": "PathtraceIntegrator",
        "maxDepth": 4,
        "indirectOnly": false
    }
}
