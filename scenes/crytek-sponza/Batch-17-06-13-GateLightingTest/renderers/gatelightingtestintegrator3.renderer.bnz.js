{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 2,
        "sppY": 2
    },
    "integrator": {
        "type": "GateLightingTestIntegrator",
        "vplSampler": {
            "type": "RandomVPLSampler",
            "minDepth": 6,
            "maxDepth": 6,
            "vplCount": 16384,
            "indirectOnly": true,
            "vplFilter": {
                "type": "GeorgievVPLFilter",
                "viewRayCountX": 8,
                "viewRayCountY": 8,
                "epsilon": 0.00000001,
                "integratorSampler": {
                    "type": "StratifiedSampler",
                    "sppX": 512,
                    "sppY": 256
                },
                "integrator": {
                    "type": "PathtraceIntegrator",
                    "maxDepth": 8,
                    "indirectOnly": true
                },
                "useFiltering": true,
                "useResampling": false,
                "vplCount": 8192
            }
        }
    }
}
