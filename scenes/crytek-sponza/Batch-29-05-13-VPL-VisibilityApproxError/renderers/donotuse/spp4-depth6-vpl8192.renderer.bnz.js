{
    "type": "IntegratorRenderer",
    "sampler": {
        "type": "StratifiedSampler",
        "sppX": 2,
        "sppY": 2
    },
    "integrator": {
        "type": "VPLVisibilityApproximationErrorIntegrator",
        "vplSampler": {
            "type": "RandomVPLSampler",
            "minDepth": 6,
            "maxDepth": 6,
            "vplCount": 8192,
            "vplFilter": {
                "type": "GeorgievVPLFilter",
                "viewRayCountX": 8,
                "viewRayCountY": 8,
                "epsilon": 0.00000001,
                "integratorSampler": {
                    "type": "StratifiedSampler",
                    "sppX": 256,
                    "sppY": 128
                },
                "integrator": {
                    "type": "PathtraceIntegrator",
                    "maxDepth": 8,
                    "indirectOnly": true
                },
                "useFiltering": true,
                "useResampling": false,
                "vplCount": 8192
            },
            "indirectOnly": true,
        },
        "treshold": 0.5,
        "visibilityApproximationMethod": "clusterVisibility"
    }
}
