{
    "models": [ 
        "../assets/cornell_box.obj"
    ],
    
    "lights": [
        {
            "type": "QuadLight",
            "Le" : [50, 50, 50],
            "transform" : {
                "vx": [100, 0, 0],
                "vy": [0, 0, 100],
                "vz": [0, -1, 0],
                "p": [213, 535, 227]
            }
        }
    ]
}
