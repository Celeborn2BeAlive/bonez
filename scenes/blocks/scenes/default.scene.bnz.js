{
    "models": [ 
        "../assets/blocks.obj"
    ],
    
    "lights": [
        {
            "type": "EnvLight",
            "image": "../../textures/skylight-surreal.exr",
            "Le": [ 1, 1, 1 ]
        }
    ]
}
