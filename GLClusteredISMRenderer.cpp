#include "GLClusteredISMRenderer.hpp"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "bonez/opengl/utils/embree_opengl.hpp"

#include "bonez/common/Progress.hpp"
#include "bonez/common/sampling/Sampler.hpp"

namespace BnZ {

using namespace gloops;

const GLchar* GLClusteredISMRenderer::GeometryPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec3 aPosition;
    layout(location = 1) in vec3 aNormal;
    layout(location = 2) in vec2 aTexCoords;

    uniform mat4 uMVPMatrix;
    uniform mat4 uMVMatrix;
    uniform mat3 uNormalMatrix;

    out vec3 vPosition;
    out vec3 vNormal;
    out vec2 vTexCoords;
    out vec3 vWorldSpaceNormal;
    out vec3 vWorldSpacePosition;

    void main() {
        vWorldSpacePosition = aPosition;
        vWorldSpaceNormal = aNormal;
        vPosition = vec3(uMVMatrix * vec4(aPosition, 1));
        vNormal = uNormalMatrix * aNormal;
        vTexCoords = aTexCoords;

        gl_Position = uMVPMatrix * vec4(aPosition, 1);
    }
);

const GLchar* GLClusteredISMRenderer::GeometryPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    in vec3 vPosition;
    in vec3 vNormal;
    in vec2 vTexCoords;
    in vec3 vWorldSpacePosition;
    in vec3 vWorldSpaceNormal;

    struct Material {
        vec3 Kd;
        vec3 Ks;
        float shininess;
        sampler2D KdSampler;
        sampler2D KsSampler;
        sampler2D shininessSampler;
    };

    uniform Material uMaterial;

    uniform float uSy;
    uniform float uHalfFOVY;
    uniform float uZNear;
    uniform float uZFar;

    layout(location = 0) out vec4 fNormalDepth;
    layout(location = 1) out vec3 fDiffuse;
    layout(location = 2) out vec4 fGlossyShininess;

    const float PI = 3.14159265358979323846264;

    void main() {
        vec3 nNormal = normalize(vNormal);
        fNormalDepth.xyz = nNormal;
        fNormalDepth.w = -vPosition.z / uZFar; // Normalized depth value

        fDiffuse = uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb / PI;

        float shininess = uMaterial.shininess + texture(uMaterial.shininessSampler, vTexCoords).r;
        fGlossyShininess.rgb = uMaterial.Ks * texture(uMaterial.KsSampler, vTexCoords).rgb * (shininess + 2) / (2 * PI);
        fGlossyShininess.a = shininess;
    }
);

GLClusteredISMRenderer::GeometryPassData::GeometryPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_MaterialUniforms(m_Program),
    m_uMVPMatrix(m_Program, "uMVPMatrix", true),
    m_uMVMatrix(m_Program, "uMVMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true),
    m_uSy(m_Program, "uSy", true),
    m_uHalfFOVY(m_Program, "uHalfFOVY", true),
    m_uZNear(m_Program, "uZNear", true),
    m_uZFar(m_Program, "uZFar", true) {
}

// This shader sort the clusters of each tile (32x32 pixels) to compute the number of clusters
// in each tile, compute a new local index for each and link back each pixel to this index.
// At the end, we have 0 <= uClustersImage(x, y) <= uClusterCountsImage(tx, ty) - 1
const GLchar* GLClusteredISMRenderer::FindUniqueGeometryClusterPassData::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
    };

    uniform GBuffer uGBuffer;

    // Outputs of the shader:

    // Link each pixel to a cluster index local to the tile
    layout(r32ui) uniform writeonly uimage2D uClustersImage;

    uniform uint* uClusterCountsBuffer;
    uniform vec3* uClusterBBoxLowerBuffer;
    uniform vec3* uClusterBBoxUpperBuffer;
    uniform vec3* uClusterNormalBuffer;

    uniform mat4 uInvProjMatrix;
    uniform mat4 uRcpViewMatrix;
    uniform vec2 uScreenSize;

    uniform bool uUseNormalClustering;

    uniform uint uSy;
    uniform float uZNear;
    uniform float uZFar;
    uniform float uHalfFOVY;

    // 32x32 is the size of a tile in screen space
    layout(local_size_x = 32, local_size_y = 32) in;

    const uint cNbThreads = gl_WorkGroupSize.x * gl_WorkGroupSize.y;

    shared uint sSampleIndices[cNbThreads];
    shared uint sClusterBuffer[cNbThreads];
    shared uint sClusterOffsets[cNbThreads];

    shared vec3 sViewSpaceBBoxLower[cNbThreads];
    shared vec3 sViewSpaceBBoxUpper[cNbThreads];
    //shared vec3 sViewSpaceNormals[cNbThreads];

    void oddEvenSortClusters() {
        for(uint i = 0; i < cNbThreads; ++i) {
            uint j;
            if(i % 2 == 0) {
                j = 2 * gl_LocalInvocationIndex;
            } else {
                j = 2 * gl_LocalInvocationIndex + 1;
            }

            uint lhs;
            uint rhs;

            if(j < cNbThreads - 1) {
                lhs = sClusterBuffer[j];
                rhs = sClusterBuffer[j + 1];
            }

            barrier();

            if(j < cNbThreads - 1 && lhs > rhs) {
                sClusterBuffer[j + 1] = lhs;
                sClusterBuffer[j] = rhs;

                uint tmp = sSampleIndices[j + 1];
                sSampleIndices[j + 1] = sSampleIndices[j];
                sSampleIndices[j] = tmp;
            }

            barrier();
        }
    }

    void mergeSortClusters() {
        uint tid = gl_LocalInvocationIndex;

        for(uint i = 1; i < cNbThreads; i *= 2) {
            uint listSize = i;
            uint listIdx = tid / i;
            uint listOffset = listIdx * listSize;
            uint otherListOffset;
            uint newListOffset;

            if(listIdx % 2 == 0) {
                otherListOffset = listOffset + listSize;
                newListOffset = listOffset;
            } else {
                otherListOffset = listOffset - listSize;
                newListOffset = otherListOffset;
            }

            // On doit calculer la position finale de l'element courant
            uint indexInMyList = tid - listOffset;

            uint indexInOtherList = 0;

            uint value = sClusterBuffer[tid];
            uint sIdx = sSampleIndices[tid];

            uint a = 0;
            uint b = listSize;
            uint middle;

            for(uint j = 1; j < listSize; j *= 2) {
                middle = (a + b) / 2;
                uint otherValue = sClusterBuffer[otherListOffset + middle];
                if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
                    b = middle;
                } else {
                    a = middle;
                }
            }

            uint otherValue = sClusterBuffer[otherListOffset + a];
            if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
                indexInOtherList = a;
            } else {
                indexInOtherList = b;
            }

            uint finalIndex = indexInMyList + indexInOtherList;

            barrier();

            sClusterBuffer[newListOffset + finalIndex] = value;
            sSampleIndices[newListOffset + finalIndex] = sIdx;

            barrier();
        }
    }

    void blellochExclusiveAddScan() {
        // Up-sweep phase
        uint a = 2 * gl_LocalInvocationIndex;
        uint b = 2 * gl_LocalInvocationIndex + 1;

        for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
            if(gl_LocalInvocationIndex < s) {
                sClusterOffsets[b] += sClusterOffsets[a];

                a = a * 2 + 1;
                b = b * 2 + 1;
            }
            barrier();
        }

        // Down-sweep phase
        if(gl_LocalInvocationIndex == 0) {
            sClusterOffsets[cNbThreads - 1] = 0;
        }
        barrier();

        for(uint s = 1; s < cNbThreads; s *= 2) {
            if(gl_LocalInvocationIndex < s) {
                a = (a - 1) / 2;
                b = (b - 1) / 2;

                uint tmp = sClusterOffsets[b];
                sClusterOffsets[b] += sClusterOffsets[a];
                sClusterOffsets[a] = tmp;
            }
            barrier();
        }
    }

    int getFaceIdx(vec3 wi) {
        vec3 absWi = abs(wi);

        float maxComponent = max(absWi.x, max(absWi.y, absWi.z));

        if(maxComponent == absWi.x) {
            if(wi.x > 0) {
                return 0;
            }
            return 1;
        }

        if(maxComponent == absWi.y) {
            if(wi.y > 0) {
                return 2;
            }
            return 3;
        }

        if(maxComponent == absWi.z) {
            if(wi.z > 0) {
                return 4;
            }
            return 5;
        }

        return -1;
    }

    vec3 getConeAxis(vec3 normal) {
        int faceIdx = getFaceIdx(normal);

        vec3 axis[] = vec3[](
                vec3(1, 0, 0),
                vec3(-1, 0, 0),
                vec3(0, 1, 0),
                vec3(0, -1, 0),
                vec3(0, 0, 1),
                vec3(0, 0, -1)
        );

        return axis[faceIdx];
    }

    void main() {
        // Local ID of the current thread
        uint tid = gl_LocalInvocationIndex;

        vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, ivec2(gl_GlobalInvocationID.xy), 0);
        uint cluster = uint(floor(log(normalDepth.w * uZFar / uZNear) / log(1 + 2 * tan(uHalfFOVY) / uSy)));
        if(uUseNormalClustering) {
            int faceIdx = getFaceIdx(normalDepth.xyz);
            cluster = 6 * cluster + faceIdx;
        }

        // Initialize the shared memory
        sClusterBuffer[gl_LocalInvocationIndex] = cluster;
        sClusterOffsets[gl_LocalInvocationIndex] = 0;
        sSampleIndices[gl_LocalInvocationIndex] = gl_LocalInvocationIndex;

        barrier();

        // Local sorting
        //oddEvenSortClusters();
        mergeSortClusters();

        bool isUniqueCluster = gl_LocalInvocationIndex == 0 ||
                sClusterBuffer[gl_LocalInvocationIndex] != sClusterBuffer[gl_LocalInvocationIndex - 1];

        // Initialization for scan
        if(isUniqueCluster) {
            sClusterOffsets[gl_LocalInvocationIndex] = 1;
        }
        barrier();

        // Compaction step
        blellochExclusiveAddScan();

        // Compute offset
        if(!isUniqueCluster) {
            sClusterOffsets[gl_LocalInvocationIndex] -= 1;
        }
        barrier();

        // Write the number of clusters for this tile in the output image
        if(gl_LocalInvocationIndex == 0) {
            uint clusterCount = sClusterOffsets[cNbThreads - 1] + 1;
            uClusterCountsBuffer[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_WorkGroupSize.x] = clusterCount;
        }

        // Original view sample that have been moved to this cell by the sort step
        uint sampleIndex = sSampleIndices[gl_LocalInvocationIndex];

        // Offset associated to the cluster of this sample
        uint clusterOffset = sClusterOffsets[gl_LocalInvocationIndex];

        // Coordinates of this sample
        uint x = sampleIndex % gl_WorkGroupSize.x;
        uint y = (sampleIndex - x) / gl_WorkGroupSize.x;
        ivec2 globalCoords = ivec2(gl_WorkGroupID.x * gl_WorkGroupSize.x + x,
                             gl_WorkGroupID.y * gl_WorkGroupSize.y + y);

        // Write the offset for this cluster in the new cluster image
        imageStore(uClustersImage, globalCoords, uvec4(clusterOffset, 0, 0, 0));

        // Read geometric information of the original sample
        normalDepth = texelFetch(uGBuffer.normalDepthSampler, globalCoords, 0);
        vec2 pixelNDC = vec2(-1.f, -1.f) + 2.f * (vec2(globalCoords) + vec2(0.5f, 0.5f)) / uScreenSize;
        vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(pixelNDC, 1.f, 1.f);
        farPointViewSpaceCoords = farPointViewSpaceCoords / farPointViewSpaceCoords.w;
        vec4 samplePointViewSpaceCoords = vec4(normalDepth.w * farPointViewSpaceCoords.xyz, 1);

        sViewSpaceBBoxLower[gl_LocalInvocationIndex] = vec3(uRcpViewMatrix * samplePointViewSpaceCoords);
        sViewSpaceBBoxUpper[gl_LocalInvocationIndex] = sViewSpaceBBoxLower[gl_LocalInvocationIndex];

        barrier();

        // Reduce step to get bounds of the clusters
        for(uint s = 1; s < cNbThreads; s *= 2) {
            uint other = tid + s;
            if(other < cNbThreads && sClusterOffsets[other] == clusterOffset) {
                //sData[tid] = max(sData[tid], sData[other]);
                sViewSpaceBBoxLower[tid] = min(sViewSpaceBBoxLower[tid], sViewSpaceBBoxLower[other]);
                sViewSpaceBBoxUpper[tid] = max(sViewSpaceBBoxUpper[tid], sViewSpaceBBoxUpper[other]);
            }
            barrier();
        }

        // Write bounds to ouput images
        if(isUniqueCluster) {
            uint globalOffset = (gl_WorkGroupID.x + gl_NumWorkGroups.x * gl_WorkGroupID.y) * cNbThreads + clusterOffset;

            uClusterBBoxLowerBuffer[globalOffset] = sViewSpaceBBoxLower[tid];
            uClusterBBoxUpperBuffer[globalOffset] = sViewSpaceBBoxUpper[tid];
            uClusterNormalBuffer[globalOffset] = getConeAxis(vec3(uRcpViewMatrix * vec4(normalDepth.xyz, 0)));
        }
    }
);

GLClusteredISMRenderer::FindUniqueGeometryClusterPassData::FindUniqueGeometryClusterPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uGBuffer(m_Program),
    m_uClustersImage(m_Program, "uClustersImage", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uScreenSize(m_Program, "uScreenSize", true),
    m_uClusterCountsBuffer(m_Program, "uClusterCountsBuffer", true),
    m_uClusterBBoxLowerBuffer(m_Program, "uClusterBBoxLowerBuffer", true),
    m_uClusterBBoxUpperBuffer(m_Program, "uClusterBBoxUpperBuffer", true),
    m_uClusterNormalBuffer(m_Program, "uClusterNormalBuffer", true),
    m_uUseNormalClustering(m_Program, "uUseNormalClustering", true),
    m_uSy(m_Program, "uSy", true),
    m_uHalfFOVY(m_Program, "uHalfFOVY", true),
    m_uZNear(m_Program, "uZNear", true),
    m_uZFar(m_Program, "uZFar", true) {

    m_Program.use();
    m_uClustersImage.set(IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS);
}

const GLchar* GLClusteredISMRenderer::CountGeometryClusterPassData::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    // Inputs
    // Number of screen space tiles
    uniform uint uTileCount;

    // For each tile (Tx, Ty), uClusterCountsBuffer[Tx + Ty * Sx] is the number of cluster contained in the tile
    uniform uint* uClusterCountsBuffer;

    // Outputs
    // For each tile (Tx, Ty), uClusterTilesOffsetsBuffer[Tx + Ty * Sx] will be the global index of the first cluster
    // contained in the tile.
    uniform uint* uClusterTilesOffsetsBuffer;
    // Will be the total number of clusters
    uniform uint* uClusterCount;
    // Will contains for each cluster the index of the associated tile
    uniform uint* uClusterToTileBuffer;

    layout(local_size_x = 1024) in;

    const uint cNbThreads = gl_WorkGroupSize.x;

    shared uint sGroupData[cNbThreads];

    void main() {
        uint tileClusterCount = 0;
        // Init
        if(gl_GlobalInvocationID.x >= uTileCount) {
            sGroupData[gl_LocalInvocationIndex] = 0;
        } else {
            tileClusterCount = sGroupData[gl_LocalInvocationIndex] = uClusterCountsBuffer[gl_GlobalInvocationID.x];
        }

        barrier();

        // Scan to compute offsets and cluster count

        // Up-sweep phase
        uint a = 2 * gl_LocalInvocationIndex;
        uint b = 2 * gl_LocalInvocationIndex + 1;

        for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
            if(gl_LocalInvocationIndex < s) {
                sGroupData[b] += sGroupData[a];

                a = a * 2 + 1;
                b = b * 2 + 1;
            }
            barrier();
        }

        // Down-sweep phase
        if(gl_LocalInvocationIndex == 0) {
            sGroupData[cNbThreads - 1] = 0;
        }
        barrier();

        for(uint s = 1; s < cNbThreads; s *= 2) {
            if(gl_LocalInvocationIndex < s) {
                a = (a - 1) / 2;
                b = (b - 1) / 2;

                uint tmp = sGroupData[b];
                sGroupData[b] += sGroupData[a];
                sGroupData[a] = tmp;
            }
            barrier();
        }

        if(gl_GlobalInvocationID.x < uTileCount) {
            uint tileOffset = uClusterTilesOffsetsBuffer[gl_GlobalInvocationID.x] = sGroupData[gl_LocalInvocationIndex];

            if(gl_GlobalInvocationID.x == uTileCount - 1) {
                *uClusterCount = sGroupData[gl_LocalInvocationIndex] + uClusterCountsBuffer[gl_GlobalInvocationID.x];
            }

            // Write correspondance from cluster to tile
            for(uint i = 0; i < tileClusterCount; ++i) {
                uClusterToTileBuffer[tileOffset + i] = gl_LocalInvocationIndex;
            }

            barrier();
        }
    }
);

GLClusteredISMRenderer::CountGeometryClusterPassData::CountGeometryClusterPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uTileCount(m_Program, "uTileCount", true),
    m_uClusterCountsBuffer(m_Program, "uClusterCountsBuffer", true),
    m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
    m_uClusterCount(m_Program, "uClusterCount", true),
    m_uClusterToTileBuffer(m_Program, "uClusterToTileBuffer", true) {
}

const GLchar* GLClusteredISMRenderer::LightAssignmentPassData::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_bindless_texture : enable\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    struct VPL {
        vec3 P;
        vec3 N;
        vec3 L;
    };

    uniform uint uVPLCount;
    uniform VPL* uVPLBuffer;

    // Outputs

    uniform ivec2* uVPLOffsetCountsBuffer;
    uniform uint* uVPLIndexBuffer;

    uniform uint uClusterCount;

    uniform uint* uClusterTilesOffsetsBuffer;
    uniform uint* uClusterToTileBuffer;
    uniform vec3* uClusterBBoxLowerBuffer;
    uniform vec3* uClusterBBoxUpperBuffer;

    layout(local_size_x = 1024) in;

    uniform bool uNoShadow;

    uint tileID;
    uint clusterIdx;
    uint clusterLocalID;
    vec3 bboxLower;
    vec3 bboxUpper;
    vec3 bboxCenter;
    float bboxRadius;

    bool acceptVPL(VPL vpl) {
        if(uNoShadow) {
            return true;
        }
        return false;
    }

    void main() {
        clusterIdx = uint(gl_GlobalInvocationID.x);

        if(clusterIdx >= uClusterCount) {
            return;
        }

        tileID = uClusterToTileBuffer[clusterIdx];
        clusterLocalID = clusterIdx - uClusterTilesOffsetsBuffer[tileID];

        uint dataIdx = tileID * 1024 + clusterLocalID;

        bboxLower = uClusterBBoxLowerBuffer[dataIdx];
        bboxUpper = uClusterBBoxUpperBuffer[dataIdx];
        bboxCenter = (bboxLower + bboxUpper) * 0.5f;
        bboxRadius = length(bboxLower - bboxUpper) * 0.5f;

        uint offset = uVPLCount * clusterIdx;
        uint c = offset;
        uint nbAcceptedLights = 0;
        for(uint i = 0; i < uVPLCount; ++i) {
            if(acceptVPL(uVPLBuffer[i])) {
                uVPLIndexBuffer[c++] = i;
                ++nbAcceptedLights;
            }
        }

        uVPLOffsetCountsBuffer[clusterIdx] = ivec2(int(offset), int(nbAcceptedLights));

        memoryBarrier();
    }

    );

    GLClusteredISMRenderer::LightAssignmentPassData::LightAssignmentPassData():
        m_Program(buildComputeProgram(s_ComputeShader)),
        m_uVPLCount(m_Program, "uVPLCount", true),
        m_uClusterCount(m_Program, "uClusterCount", true),
        m_uNoShadow(m_Program, "uNoShadow", true),
        m_uVPLBuffer(m_Program, "uVPLBuffer", true),
        m_uVPLOffsetCountsBuffer(m_Program, "uVPLOffsetCountsBuffer", true),
        m_uVPLIndexBuffer(m_Program, "uVPLIndexBuffer", true),
        m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
        m_uClusterToTileBuffer(m_Program, "uClusterToTileBuffer", true),
        m_uClusterBBoxLowerBuffer(m_Program, "uClusterBBoxLowerBuffer", true),
        m_uClusterBBoxUpperBuffer(m_Program, "uClusterBBoxUpperBuffer", true) {
    }

    const GLchar* GLClusteredISMRenderer::TransformVPLToViewSpacePassData::s_ComputeShader =
    "#version 430\n"
    GLOOPS_STRINGIFY(

    struct VPL {
        vec3 P;
        vec3 N;
        vec3 L;
    };

    uniform uint uVPLCount;

    // Contains all the VPLs
    layout(std430, binding = 0) coherent buffer VPLBuffer {
        VPL bVPLBuffer[];
    };

    uniform mat4 uViewMatrix;
    uniform mat3 uNormalMatrix;

    layout(local_size_x = 1024) in;

    void main() {
        uint idx = uint(gl_GlobalInvocationID.x);

        // A thread represents a cluster
        if(idx < uVPLCount) {
            bVPLBuffer[idx].P = vec3(uViewMatrix * vec4(bVPLBuffer[idx].P, 1));
            bVPLBuffer[idx].N = normalize(uNormalMatrix * bVPLBuffer[idx].N);
        }

        memoryBarrier();
    }
);

GLClusteredISMRenderer::TransformVPLToViewSpacePassData::TransformVPLToViewSpacePassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uViewMatrix(m_Program, "uViewMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true) {
}

const GLchar* GLClusteredISMRenderer::VPLShadingPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    out vec3 vFarPointViewSpaceCoords;

    uniform mat4 uInvProjMatrix;

    void main() {
        vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
        vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

        gl_Position = vec4(aPosition, 0, 1);
    }

    );

const GLchar* GLClusteredISMRenderer::VPLShadingPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_bindless_texture : enable\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

    uniform vec4 uViewport;

    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
        //usampler2D geometryClusterSampler;
    };

    uniform GBuffer uGBuffer;

    uniform usampler2D uGeometryClusterSampler;

    uniform uvec2 uTileCount;

    struct VPL {
        vec3 P;
        vec3 N;
        vec3 L;
    };

    uniform uint uVPLCount;
    uniform bool uDisplayCostPerPixel;
    uniform bool uRenderIrradiance;

    uniform VPL* uVPLBuffer;
    uniform uint* uVPLIndexBuffer;
    uniform uint* uClusterTilesOffsetsBuffer;

    uniform float uDistClamp;

    vec3 randomColor(uint value) {
        return fract(
                    sin(
                        vec3(float(value) * 12.9898,
                             float(value) * 78.233,
                             float(value) * 56.128)
                        )
                    * 43758.5453
                    );
    }

    vec3 heatColor(float t) {
        vec3 map[] = vec3[](vec3(0, 0, 1), vec3(0, 1, 0), vec3(1, 0, 0));
        t *= 3;
        int i = clamp(int(t), 0, 2);
        int j = clamp(i + 1, 0, 2);
        float r = t - i;
        return (1 - r) * map[i] + r * map[j];
    }

    in vec3 vFarPointViewSpaceCoords;

    out vec3 fFragColor;

    uniform ivec2* uVPLOffsetCountsBuffer;
    uniform mat4 uRcpViewMatrix;

    void main() {
        vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

        ivec2 tileCoords = ivec2(texCoords * vec2(textureSize(uGeometryClusterSampler, 0)));
        tileCoords /= 32;

        uint tileIdx = tileCoords.x + tileCoords.y * uTileCount.x;

        uint clusterLocalID = texture(uGeometryClusterSampler, texCoords).r;
        uint clusterGlobalID = uClusterTilesOffsetsBuffer[tileIdx] + clusterLocalID;

        // Read fragment attributes
        vec4 normalDepth = texture(uGBuffer.normalDepthSampler, texCoords);
        vec3 fragPosition = normalDepth.w * vFarPointViewSpaceCoords;
        vec3 Kd;
        vec4 KsShininess;

        if(uRenderIrradiance) {
            Kd = vec3(1.f / 3.14f);
            KsShininess = vec4(0.f, 0.f, 0.f, 1.f);
        } else {
            Kd = texture(uGBuffer.diffuseSampler, texCoords).rgb;
            KsShininess = texture(uGBuffer.glossyShininessSampler, texCoords);
        }

        fFragColor = vec3(0, 0, 0);

        ivec2 offsetCount = uVPLOffsetCountsBuffer[clusterGlobalID];

        if(uDisplayCostPerPixel) {
            fFragColor = heatColor(float(offsetCount.y) / uVPLCount);
            return;
        }

        int end = offsetCount.x + offsetCount.y;

        for(int j = offsetCount.x; j < end; ++j) {
            uint vplIdx = uVPLIndexBuffer[j];
            VPL vpl = uVPLBuffer[vplIdx];

            vec3 wi = vpl.P - fragPosition;
            float dist = length(wi);
            wi /= dist;

            float cos_o = max(0, dot(-wi, vpl.N));
            float cos_i = max(0, dot(wi, normalDepth.xyz));

            dist = max(dist, uDistClamp);

            float G = cos_i * cos_o / (dist * dist);

            vec3 r = reflect(-wi, normalDepth.xyz);

            vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);

            fFragColor = fFragColor + brdf * G * vpl.L;
        }
    }
);

GLClusteredISMRenderer::VPLShadingPassData::VPLShadingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uTileCount(m_Program, "uTileCount", true),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uDisplayCostPerPixel(m_Program, "uDisplayCostPerPixel", true),
    m_uVPLBuffer(m_Program, "uVPLBuffer", true),
    m_uVPLIndexBuffer(m_Program, "uVPLIndexBuffer", true),
    m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
    m_uVPLOffsetCountsBuffer(m_Program, "uVPLOffsetCountsBuffer", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uDistClamp(m_Program, "uDistClamp", true),
    m_uGBuffer(m_Program),
    m_uGeometryClusterSampler(m_Program, "uGeometryClusterSampler", true),
    m_uRenderIrradiance(m_Program, "uRenderIrradiance", true) {
    m_Program.use();
    m_uGeometryClusterSampler.set(TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
}



const GLchar* GLClusteredISMRenderer::DirectLightingPassData::s_VertexShader =
"#version 430\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vFarPointViewSpaceCoords;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLClusteredISMRenderer::DirectLightingPassData::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
    //usampler2D geometryClusterSampler;
};

uniform GBuffer uGBuffer;

uniform mat4 uFaceProjectionMatrices[6];
uniform samplerCubeShadow uShadowMapSampler;
uniform float uShadowMapOffset;
uniform float uDistClamp;

uniform vec3 uLightPosition;
uniform vec3 uLightIntensity;
uniform mat4 uLightViewMatrix;

uniform mat4 uRcpViewMatrix;

uniform bool uRenderIrradiance;

in vec3 vFarPointViewSpaceCoords;

out vec3 fFragColor;

int getFaceIdx(vec3 wi) {
    vec3 absWi = abs(wi);

    float maxComponent = max(absWi.x, max(absWi.y, absWi.z));

    if(maxComponent == absWi.x) {
        if(wi.x > 0) {
            return 0;
        }
        return 1;
    }

    if(maxComponent == absWi.y) {
        if(wi.y > 0) {
            return 2;
        }
        return 3;
    }

    if(maxComponent == absWi.z) {
        if(wi.z > 0) {
            return 4;
        }
        return 5;
    }

    return -1;
}

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uGBuffer.normalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vFarPointViewSpaceCoords;

    vec3 Kd;
    vec4 KsShininess;

    if(uRenderIrradiance) {
        Kd = vec3(1.f / 3.14f);
        KsShininess = vec4(0.f, 0.f, 0.f, 1.f);
    } else {
        Kd = texture(uGBuffer.diffuseSampler, texCoords).rgb;
        KsShininess = texture(uGBuffer.glossyShininessSampler, texCoords);
    }

    vec3 wi = uLightPosition - fragPosition;
    float dist = length(wi);
    wi /= dist;

    float cos_i = max(0, dot(wi, normalDepth.xyz));

    dist = max(dist, uDistClamp);

    float G = cos_i / (dist * dist);

    vec3 fragPos_ws = vec3(uRcpViewMatrix * vec4(fragPosition, 1));
    vec3 lightPos_ws = vec3(uRcpViewMatrix * vec4(uLightPosition, 1));

    vec3 fragPos_ls = vec3(uLightViewMatrix * vec4(fragPos_ws, 1));
    vec3 shadowRay = normalize(fragPos_ws - lightPos_ws);
    int face = getFaceIdx(shadowRay);
    vec4 faceClipSpacePosition = uFaceProjectionMatrices[face] * vec4(fragPos_ls, 1);
    float depthRef = faceClipSpacePosition.z / faceClipSpacePosition.w;
    depthRef = 0.5 * (depthRef + 1) - uShadowMapOffset;

    float shadowFactor = texture(uShadowMapSampler, vec4(shadowRay, depthRef));

    vec3 r = reflect(-wi, normalDepth.xyz);

    vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
    fFragColor = brdf * shadowFactor * G * uLightIntensity;
}

);

GLClusteredISMRenderer::DirectLightingPassData::DirectLightingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uShadowMapSampler(m_Program, "uShadowMapSampler", true),
    m_uLightPosition(m_Program, "uLightPosition", true),
    m_uLightIntensity(m_Program, "uLightIntensity", true),
    m_uLightViewMatrix(m_Program, "uLightViewMatrix", true),
    m_uFaceProjectionMatrices(m_Program, "uFaceProjectionMatrices", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uShadowMapOffset(m_Program, "uShadowMapOffset", true),
    m_uDistClamp(m_Program, "uDistClamp", true),
    m_uRenderIrradiance(m_Program, "uRenderIrradiance", true),
    m_uGBuffer(m_Program),
    m_DoDirectLightingPass(false) {

    // Initialize constant uniforms
    m_Program.use();
}


const GLchar* GLClusteredISMRenderer::DrawBuffersPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    void main() {
        gl_Position = vec4(aPosition, 0, 1);
    }
);

const GLchar* GLClusteredISMRenderer::DrawBuffersPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    uniform vec4 uViewport;

    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
    };

    uniform GBuffer uGBuffer;

    uniform usampler2D uGeometryClusterSampler;

    uniform uint* uClusterTilesOffsetsBuffer;
    //uniform vec3* bClusterBBoxLowerBuffer;
    //uniform vec3* bClusterBBoxUpperBuffer;
    uniform uint* uClusterToTileBuffer;

    layout(std430, binding = 3) buffer ClusterBBoxLowerBuffer {
        vec3 bClusterBBoxLowerBuffer[];
    };

    layout(std430, binding = 4) buffer ClusterBBoxUpperBuffer {
        vec3 bClusterBBoxUpperBuffer[];
    };

    layout(std430, binding = 5) buffer ClusterNormalBuffer {
        vec3 bClusterNormalBuffer[];
    };

    uniform float uZFar;
    uniform uvec2 uTileCount;

    uniform int uDataToDisplay;

    out vec3 fFragColor;

    vec3 randomColor(uint value) {
        return fract(
                    sin(
                        vec3(float(value) * 12.9898,
                             float(value) * 78.233,
                             float(value) * 56.128)
                        )
                    * 43758.5453
                    );
    }

    vec3 randomColor(uvec3 value) {
        return fract(
                    sin(
                        vec3(float(value.x * value.y) * 12.9898,
                             float(value.y * value.z) * 78.233,
                             float(value.z * value.x) * 56.128)
                        )
                    * 43758.5453
                    );
    }

    void main() {
        vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

        ivec2 tileCoords = ivec2(texCoords * vec2(textureSize(uGeometryClusterSampler, 0).xy));
        tileCoords /= 32;

        uint tileIdx = tileCoords.x + tileCoords.y * uTileCount.x;

        if(uDataToDisplay == 0) {
            fFragColor = texture(uGBuffer.normalDepthSampler, texCoords).rgb;
        } else if(uDataToDisplay == 1) {
            fFragColor = vec3(texture(uGBuffer.normalDepthSampler, texCoords).a);
        } else if(uDataToDisplay == 2) {
            fFragColor = texture(uGBuffer.diffuseSampler, texCoords).rgb;
        } else if(uDataToDisplay == 3) {
            fFragColor = texture(uGBuffer.glossyShininessSampler, texCoords).rgb;
        } else if(uDataToDisplay == 4) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            fFragColor = randomColor(uClusterTilesOffsetsBuffer[tileIdx] + geometryCluster);
        } else if (uDataToDisplay == 5) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            vec3 bboxLower = bClusterBBoxLowerBuffer[tileIdx * 1024 + geometryCluster];
            fFragColor = vec3(-bboxLower.z / uZFar);
        } else if (uDataToDisplay == 6) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            vec3 bboxUpper = bClusterBBoxUpperBuffer[tileIdx * 1024 + geometryCluster];
            fFragColor = vec3(-bboxUpper.z / uZFar);
        } else if (uDataToDisplay == 7) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            vec3 normal = bClusterNormalBuffer[tileIdx * 1024 + geometryCluster];
            fFragColor = normal;

//            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
//            uint globalID = uClusterTilesOffsetsBuffer[tileIdx] + uint(geometryCluster);
//            fFragColor = randomColor(uClusterToTileBuffer[globalID]);
        }
    }
);

GLClusteredISMRenderer::DrawBuffersPassData::DrawBuffersPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uGBuffer(m_Program),
    m_uGeometryClusterSampler(m_Program, "uGeometryClusterSampler", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uDataToDisplay(m_Program, "uDataToDisplay", true),
    m_uTileCount(m_Program, "uTileCount", true),
    m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
    //m_uClusterBBoxLowerBuffer(m_Program, "bClusterBBoxLowerBuffer", true),
    //m_uClusterBBoxUpperBuffer(m_Program, "bClusterBBoxUpperBuffer", true),
    m_uClusterToTileBuffer(m_Program, "uClusterToTileBuffer", true) {
    m_Program.use();
    m_uGeometryClusterSampler.set(TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
}

GLClusteredISMRenderer::GLClusteredISMRenderer():
    m_ProjectionMatrix(1.f), m_ViewMatrix(1.f),
    m_VPLISMContainer(128),
    m_bDoVPLShadingPass(true),
    m_bNoShadow(false),
    m_bDoDirectLightPass(true),
    m_bRenderIrradiance(true),
    m_bDisplayCostPerPixel(false),
    m_fGamma(1.f),
    m_fShadowMapOffset(0.0001f),
    m_fDistClamp(100.f),
    m_bUseNormalClustering(true),
    m_bDoPullPush(false),
    m_nSceneSampleCount(1000000) {
}

void GLClusteredISMRenderer::setResolution(size_t width, size_t height) {
    {
        GLTexFormat formats[GBUFFER_COUNT] = {
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
           // { GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT }
        };
        m_GBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }

    // Number of tiles in each dimension
    uint32_t Sx = (width / TILE_SIZE) + (width % TILE_SIZE != 0);
    uint32_t Sy = (height / TILE_SIZE) + (height % TILE_SIZE != 0);

    m_TileCount = glm::ivec2(Sx, Sy);

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GeometryClustersTexture.glId());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI, width, height, 0,
                 GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    GLuint size = Sx * Sy;

    m_GClusterCountsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterCountsBuffer.makeResident(GL_READ_WRITE);

    m_GClusterTilesOffsetsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterTilesOffsetsBuffer.makeResident(GL_READ_WRITE);

    m_GClusterTotalCountBuffer.setSize(sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterTotalCountBuffer.makeResident(GL_READ_WRITE);

    size_t imageSize = width * height;

    m_GClusterToTileBuffer.setSize(imageSize * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterToTileBuffer.makeResident(GL_READ_WRITE);

    m_ClusterBBoxLowerBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterBBoxLowerBuffer.makeResident(GL_READ_WRITE);

    m_ClusterBBoxUpperBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterBBoxUpperBuffer.makeResident(GL_READ_WRITE);

    m_ClusterNormalBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterNormalBuffer.makeResident(GL_READ_WRITE);
}

void GLClusteredISMRenderer::setUp() {
    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_NORMALDEPTH);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_DIFFUSE);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_DIFFUSE).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_GLOSSYSHININESS);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).glId());

    //glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
    //glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GEOMETRYCLUSTERS).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GeometryClustersTexture.glId());

    glActiveTexture(GL_TEXTURE0);
}

void GLClusteredISMRenderer::sampleScene(const Scene& scene) {
    Progress progress;
    progress.init("Scene sampling", 1);
    Sampler sampler;
    std::vector<glm::vec3> samples;
    for(auto i = 0u; i < m_nSceneSampleCount; ++i) {
        SurfacePointRandom rdm(sampler.get1DSample(), sampler.get1DSample(),
                               sampler.get1DSample(), sampler.get2DSample());
        SurfacePointSample s = scene.geometry.sampleSurfacePoint(rdm);
        samples.emplace_back(convert(s.value.P));
    }
    progress.end();

    m_SampledScene.fill(samples.size(), &samples[0]);
}

void GLClusteredISMRenderer::findUniqueGeometryClusters() {
    {
        GLTimer timer(m_FindUniqueGeometryClusterPassData.m_TimeElapsedQuery);

        m_FindUniqueGeometryClusterPassData.m_Program.use();

        glBindImageTexture(IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS, m_GeometryClustersTexture.glId(),
                           0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32UI);

        m_FindUniqueGeometryClusterPassData.m_uClusterCountsBuffer.set(m_GClusterCountsBuffer.getGPUAddress());
        m_FindUniqueGeometryClusterPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
        m_FindUniqueGeometryClusterPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());
        m_FindUniqueGeometryClusterPassData.m_uClusterNormalBuffer.set(m_ClusterNormalBuffer.getGPUAddress());

        glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
        m_FindUniqueGeometryClusterPassData.m_uInvProjMatrix.set(rcpProjMatrix);
        m_FindUniqueGeometryClusterPassData.m_uRcpViewMatrix.set(glm::inverse(m_ViewMatrix));

        m_FindUniqueGeometryClusterPassData.m_uScreenSize.set(float(m_GBuffer.getWidth()),
                                            float(m_GBuffer.getHeight()));

        m_FindUniqueGeometryClusterPassData.m_uUseNormalClustering.set(m_bUseNormalClustering);

        m_FindUniqueGeometryClusterPassData.m_uSy.set(m_TileCount.y);
        m_FindUniqueGeometryClusterPassData.m_uHalfFOVY.set(glm::radians(0.5f * getFOVY()));
        m_FindUniqueGeometryClusterPassData.m_uZNear.set(getZNear());
        m_FindUniqueGeometryClusterPassData.m_uZFar.set(getZFar());

        glDispatchCompute(m_TileCount.x, m_TileCount.y, 1);
    }

    {
        GLTimer timer(m_CountGeometryClusterPassData.m_TimeElapsedQuery);

        m_CountGeometryClusterPassData.m_Program.use();

        uint32_t tileCount = m_TileCount.x * m_TileCount.y;
        uint32_t workGroupSize = 1024;
        uint32_t workGroupCount = (tileCount / workGroupSize) + (tileCount % workGroupSize != 0);

        m_CountGeometryClusterPassData.m_uClusterTilesOffsetsBuffer.set(m_GClusterTilesOffsetsBuffer.getGPUAddress());
        m_CountGeometryClusterPassData.m_uClusterCountsBuffer.set(m_GClusterCountsBuffer.getGPUAddress());
        m_CountGeometryClusterPassData.m_uClusterToTileBuffer.set(m_GClusterToTileBuffer.getGPUAddress());
        m_CountGeometryClusterPassData.m_uClusterCount.set(m_GClusterTotalCountBuffer.getGPUAddress());

        // ONLY WORKS FOR IMAGE WITH LESS THAN 1024 TILES
        // Can be extended for more but require more compute shader code to handle the parallel
        // segmented scan
        assert(workGroupCount <= 1);

        m_CountGeometryClusterPassData.m_uTileCount.set(tileCount);

        glDispatchCompute(workGroupCount, 1, 1);

        m_GClusterTotalCountBuffer.makeNonResident();
        m_GClusterTotalCountBuffer.getData(1, &m_nGClusterCount);
        m_GClusterTotalCountBuffer.makeResident(GL_READ_WRITE);
    }
}

void GLClusteredISMRenderer::geometryPass(const GLScene& scene) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    {
        GLTimer timer(m_GeometryPassData.m_TimeElapsedQuery);

        glViewport(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight());

        m_GeometryPassData.m_Program.use();

        m_GBuffer.bindForDrawing();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 MVPMatrix = m_ProjectionMatrix * m_ViewMatrix;
        glm::mat4 MVMatrix = m_ViewMatrix;
        glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

        m_GeometryPassData.m_uMVPMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVPMatrix));
        m_GeometryPassData.m_uMVMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVMatrix));
        m_GeometryPassData.m_uNormalMatrix.setMatrix3(1, GL_FALSE, glm::value_ptr(NormalMatrix));

        m_GeometryPassData.m_uSy.set(float(m_TileCount.y));
        m_GeometryPassData.m_uHalfFOVY.set(glm::radians(0.5f * getFOVY()));
        m_GeometryPassData.m_uZNear.set(getZNear());
        m_GeometryPassData.m_uZFar.set(getZFar());

        GLMaterialManager::TextureUnits units(TEXUNIT_MAT_DIFFUSE, TEXUNIT_MAT_GLOSSY, TEXUNIT_MAT_SHININESS);

        scene.render(m_GeometryPassData.m_MaterialUniforms, units);
    }

    findUniqueGeometryClusters();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLClusteredISMRenderer::computeVPLISMs(const VPLContainer& vpls) {
    GLTimer timer(m_ComputeVPLISMsTimeElapsedQuery);

    //if(!m_bNoShadow) {
        std::vector<glm::mat4> viewMatrices;

        for(const auto& vpl: vpls.orientedVPLs) {
            glm::vec3 eye = convert(vpl.P + vpl.N * 0.1);
            glm::vec3 point = convert(vpl.P + vpl.N);
            glm::vec3 up;
            if(vpl.N.x != 0) {
                up = glm::vec3(-(vpl.N.y + vpl.N.z) / vpl.N.x, 1, 1);
            } else if(vpl.N.y != 0) {
                up = glm::vec3(1, -(vpl.N.x + vpl.N.z) / vpl.N.y, 1);
            } else {
                up = glm::vec3(1, 1, -(vpl.N.x + vpl.N.y) / vpl.N.z);
            }

            viewMatrices.emplace_back(glm::lookAt(eye, point, up));
        }

        m_ISMRenderer.render(m_SampledScene, m_VPLISMContainer, viewMatrices.data(),
                             viewMatrices.size(), m_bDoPullPush);
    //}
}

void GLClusteredISMRenderer::sendVPLs(const VPLContainer& vpls, const Scene& scene) {
    uint32_t workGroupSize = 1024;
    uint32_t workItemCount;

    computeVPLISMs(vpls);

    {
        GLTimer timer(m_LightAssignmentPassData.m_TimeElapsedQuery);

        std::vector<glm::vec4> vplBuffer;

        for(auto idx: range(vpls.orientedVPLs.size())) {
            auto vpl = vpls.orientedVPLs[idx];

            glm::vec4 lightPosition = glm::vec4(convert(vpl.P), 1);
            glm::vec3 lightNormal = convert(vpl.N);

            vplBuffer.emplace_back(lightPosition);
            vplBuffer.emplace_back(glm::vec4(lightNormal, 0));
            vplBuffer.emplace_back(glm::vec4(vpl.L.r, vpl.L.g, vpl.L.b, 0));
        }

        m_VPLBuffer.setData(vplBuffer, GL_DYNAMIC_DRAW);
        m_VPLBuffer.makeResident(GL_READ_ONLY);

        m_VPLOffsetsCountsBuffer.setSize(sizeof(glm::ivec2) * m_nGClusterCount, GL_STATIC_DRAW);
        m_VPLOffsetsCountsBuffer.makeResident(GL_READ_WRITE);

        m_VPLIndexBuffer.setSize(vpls.orientedVPLs.size() * m_nGClusterCount * sizeof(GLuint), GL_STATIC_DRAW);
        m_VPLIndexBuffer.makeResident(GL_READ_WRITE);

        m_LightAssignmentPassData.m_Program.use();

        // Set Uniforms
        m_LightAssignmentPassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
        m_LightAssignmentPassData.m_uClusterCount.set(GLuint(m_nGClusterCount));
        m_LightAssignmentPassData.m_uNoShadow.set(m_bNoShadow);
        m_LightAssignmentPassData.m_uVPLBuffer.set(m_VPLBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uVPLOffsetCountsBuffer.set(m_VPLOffsetsCountsBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uVPLIndexBuffer.set(m_VPLIndexBuffer.getGPUAddress());

        m_LightAssignmentPassData.m_uClusterTilesOffsetsBuffer.set(m_GClusterTilesOffsetsBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterToTileBuffer.set(m_GClusterToTileBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());

        // Launch compute shader
        workItemCount = (m_nGClusterCount / workGroupSize) + (m_nGClusterCount % workGroupSize != 0);
        glDispatchCompute(workItemCount, 1, 1);

        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }

    {
        GLTimer timer(m_TransformVPLToViewSpacePassData.m_TimeElapsedQuery);

        glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

        m_TransformVPLToViewSpacePassData.m_Program.use();

        m_VPLBuffer.bindBase(0);

        m_TransformVPLToViewSpacePassData.m_uViewMatrix.set(m_ViewMatrix);
        m_TransformVPLToViewSpacePassData.m_uNormalMatrix.set(NormalMatrix);
        m_TransformVPLToViewSpacePassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));

        uint32_t taskCount = uint32_t(vpls.orientedVPLs.size());
        workItemCount = (taskCount / workGroupSize) + (taskCount % workGroupSize != 0);
        glDispatchCompute(workItemCount, 1, 1);

        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }
}

void GLClusteredISMRenderer::displayGClusters(GLVisualDataRenderer& renderer, const Scene& scene) {
    size_t imageSize = m_GBuffer.getWidth() * m_GBuffer.getHeight();
    size_t tileCount = m_TileCount.x * m_TileCount.y;

    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);

    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<glm::vec4> normals(imageSize);
    std::vector<GLuint> clusterCount(tileCount);

    m_GClusterCountsBuffer.makeNonResident();
    m_GClusterCountsBuffer.getData(clusterCount.size(), clusterCount.data());
    m_GClusterCountsBuffer.makeResident(GL_READ_WRITE);

    m_ClusterBBoxLowerBuffer.makeNonResident();
    m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_ClusterBBoxLowerBuffer.makeResident(GL_READ_WRITE);

    m_ClusterBBoxUpperBuffer.makeNonResident();
    m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());
    m_ClusterBBoxUpperBuffer.makeResident(GL_READ_WRITE);

    m_ClusterNormalBuffer.makeNonResident();
    m_ClusterNormalBuffer.getData(normals.size(), normals.data());
    m_ClusterNormalBuffer.makeResident(GL_READ_WRITE);

    renderer.setUp();

    for(auto pair: index(clusterCount)) {
        GLuint offset = 1024 * pair.first;
        for(auto i: range(pair.second)) {
            glm::vec3 upper = glm::vec3(bboxUpper[offset + i]), lower = glm::vec3(bboxLower[offset + i]);
            float radius = glm::length(upper - lower) * 0.5f;
            glm::vec3 normal = glm::vec3(normals[offset + i]);
            Vec3f center = 0.5f * convert(upper + lower);
            Col3f color(convert(glm::abs(normal)));
            renderer.renderSphere(center, radius, color);
            renderer.renderArrow(center + radius * convert(normal), convert(normal), color, radius);
        }
    }
}

void GLClusteredISMRenderer::shadingPass(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls, const Scene& scene) {

    sendVPLs(vpls, scene);


    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::CapabilityRestorer blend(GL_BLEND, true);

    gloops::StateRestorer<GL_VIEWPORT> viewport;

    glViewport(x, y, width, height);

    glClear(GL_COLOR_BUFFER_BIT);

    glBlendFunc(GL_ONE, GL_ONE);
    glBlendEquation(GL_FUNC_ADD);

    if(m_bDoVPLShadingPass) {
        GLTimer timer(m_VPLShadingPassData.m_TimeElapsedQuery);

        m_VPLShadingPassData.m_Program.use();

        // Send matrices
        m_VPLShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
        m_VPLShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);
        m_VPLShadingPassData.m_uTileCount.set(m_TileCount);
        m_VPLShadingPassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
        m_VPLShadingPassData.m_uDisplayCostPerPixel.set(m_bDisplayCostPerPixel);
        m_VPLShadingPassData.m_uVPLBuffer.set(m_VPLBuffer.getGPUAddress());
        m_VPLShadingPassData.m_uVPLIndexBuffer.set(m_VPLIndexBuffer.getGPUAddress());
        m_VPLShadingPassData.m_uClusterTilesOffsetsBuffer.set(m_GClusterTilesOffsetsBuffer.getGPUAddress());
        m_VPLShadingPassData.m_uVPLOffsetCountsBuffer.set(m_VPLOffsetsCountsBuffer.getGPUAddress());
        m_VPLShadingPassData.m_uRcpViewMatrix.set(rcpViewMatrix);
        m_VPLShadingPassData.m_uRenderIrradiance.set(m_bRenderIrradiance);

        m_VPLShadingPassData.m_uDistClamp.set(m_fDistClamp);

        m_ScreenTriangle.render();
    }

    if(m_bDoDirectLightPass && m_DirectLightingPassData.m_DoDirectLightingPass) {
        GLTimer timer(m_DirectLightingPassData.m_TimeElapsedQuery);

        m_DirectLightingPassData.m_Program.use();

        m_DirectLightingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
        m_DirectLightingPassData.m_uInvProjMatrix.set(rcpProjMatrix);

        m_DirectLightingPassData.m_uLightPosition.set(
                    glm::vec3(m_ViewMatrix * glm::vec4(m_DirectLightingPassData.m_LightPosition, 1.f))
                    );
        m_DirectLightingPassData.m_uLightIntensity.set(m_DirectLightingPassData.m_LightIntensity);
        m_DirectLightingPassData.m_uLightViewMatrix.set(m_DirectLightingPassData.m_LightViewMatrix);
        m_DirectLightingPassData.m_uShadowMapSampler.set(TEXUNIT_TEXCUBEMAP_SHADOWMAP);
        m_DirectLightingPassData.m_uFaceProjectionMatrices.setMatrix4(
                    6, GL_FALSE, glm::value_ptr(m_DirectLightingPassData.m_ShadowMapRenderer.getFaceProjectionMatrices()[0]));
        m_DirectLightingPassData.m_uRcpViewMatrix.set(rcpViewMatrix);
        m_DirectLightingPassData.m_uDistClamp.set(m_fDistClamp);
        m_DirectLightingPassData.m_uShadowMapOffset.set(m_fShadowMapOffset);
        m_DirectLightingPassData.m_uRenderIrradiance.set(m_bRenderIrradiance);

        m_ScreenTriangle.render();
    }
}

void GLClusteredISMRenderer::drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uTileCount.set(m_TileCount);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(dataToDisplay);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);

    //m_DrawBuffersPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
    //m_DrawBuffersPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());

    m_DrawBuffersPassData.m_uClusterTilesOffsetsBuffer.set(m_GClusterTilesOffsetsBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterToTileBuffer.set(m_GClusterToTileBuffer.getGPUAddress());

    m_ClusterBBoxLowerBuffer.bindBase(3);
    m_ClusterBBoxUpperBuffer.bindBase(4);
    m_ClusterNormalBuffer.bindBase(5);

    m_ScreenTriangle.render();
}

void GLClusteredISMRenderer::drawBuffers(int x, int y, size_t width, size_t height) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLTimer timer(m_DrawBuffersPassData.m_TimeElapsedQuery);

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uTileCount.set(m_TileCount);

    //m_DrawBuffersPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
    //m_DrawBuffersPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());

    m_DrawBuffersPassData.m_uClusterTilesOffsetsBuffer.set(m_GClusterTilesOffsetsBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterToTileBuffer.set(m_GClusterToTileBuffer.getGPUAddress());

    m_ClusterBBoxLowerBuffer.bindBase(3);
    m_ClusterBBoxUpperBuffer.bindBase(4);
    m_ClusterNormalBuffer.bindBase(5);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    // Draw normals
    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);
    m_DrawBuffersPassData.m_uDataToDisplay.set(0);

    m_ScreenTriangle.render();

    // Draw depth
    glViewport(x + width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(1);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw Cluster node
    glViewport(x + 2 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(2);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster buffer
    glViewport(x + 4 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(4);
    m_DrawBuffersPassData.m_uViewport.set(fX + 4 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow lower point depth buffer
    glViewport(x, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(5);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow upper point depth buffer
    glViewport(x + width, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(6);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    glViewport(x + 2 * width, y - height, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(7);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    glViewport(x + 3 * width, y - height, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(8);
    m_DrawBuffersPassData.m_uViewport.set(fX + 3 * fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();
}

void GLClusteredISMRenderer::computeTimings() {
    float scale = 1.f / 1000000;

    m_Timings.geometryPass = m_GeometryPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.findUniqueClustersPass = m_FindUniqueGeometryClusterPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.countUniqueClustersPass = m_CountGeometryClusterPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.lightAssignementPass = m_LightAssignmentPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.lightTransformPass = m_TransformVPLToViewSpacePassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.vplShadingPass = m_VPLShadingPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.directLightShadingPass = m_DirectLightingPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.drawBuffersPass = m_DrawBuffersPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.computeVPLISMPass = m_ComputeVPLISMsTimeElapsedQuery.waitResult() * scale;

    m_Timings.updateTotalTime();
}

void GLClusteredISMRenderer::setPrimaryLightSource(const Vec3f& P, const Col3f& L, const GLScene& glScene) {
    m_DirectLightingPassData.m_DoDirectLightingPass = true;
    m_DirectLightingPassData.m_LightPosition = convert(P);
    m_DirectLightingPassData.m_LightIntensity = convert(L);
    m_DirectLightingPassData.m_LightViewMatrix =
            glm::translate(glm::mat4(1), -m_DirectLightingPassData.m_LightPosition);

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEXCUBEMAP_SHADOWMAP);

    m_DirectLightingPassData.m_ShadowMapContainer.init(2048, 1);
    m_DirectLightingPassData.m_ShadowMapRenderer.render(
                glScene, m_DirectLightingPassData.m_ShadowMapContainer, &m_DirectLightingPassData.m_LightViewMatrix, 1);

    m_DirectLightingPassData.m_ShadowMapContainer.bindForShadowTest(0);
}

void GLClusteredISMRenderer::drawPrimaryLightShadowMap(int x, int y, size_t width, size_t height) {
    glViewport(x, y, width, height);
    m_DirectLightingPassData.m_ShadowMapDrawer.drawShadowMap(m_DirectLightingPassData.m_ShadowMapContainer, 0, TEXUNIT_TEXCUBEMAP_SHADOWMAP);

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEXCUBEMAP_SHADOWMAP);
    m_DirectLightingPassData.m_ShadowMapContainer.bindForShadowTest(0);
}

void GLClusteredISMRenderer::exposeIO(TwBar* bar) {
    TwAddSeparator(bar, "GLClusteredISMRenderer::Separator0", "");

    atb::addVarRW(bar, "Do VPL Shading Pass", m_bDoVPLShadingPass, "");
    atb::addVarRW(bar, "Do Direct Light Pass", m_bDoDirectLightPass, "");
    atb::addVarRW(bar, "No Shadow", m_bNoShadow, "");
    atb::addVarRW(bar, "Render irradiance", m_bRenderIrradiance, "");
    atb::addVarRW(bar, "Display cost per pixel", m_bDisplayCostPerPixel, "");
    atb::addVarRW(bar, "Gamma", m_fGamma, "");
    atb::addVarRW(bar, "Shadow Map Offset", m_fShadowMapOffset, "");
    atb::addVarRW(bar, "Distance clamp", m_fDistClamp, "");
    atb::addVarRW(bar, "Use normal clustering", m_bUseNormalClustering, "");
    atb::addVarRO(bar, "Geometry cluster count", m_nGClusterCount, "");
    atb::addVarRW(bar, "Do Pull Push", m_bDoPullPush, "");
    atb::addVarRW(bar, "Scene Sample Count", m_nSceneSampleCount, "");

    TwAddSeparator(bar, "GLClusteredISMRenderer::Separator1", "");

    atb::addVarRO(bar, "Geometry Pass", m_Timings.geometryPass, "");
    atb::addVarRO(bar, "Find Unique Clusters Pass", m_Timings.findUniqueClustersPass, "");
    atb::addVarRO(bar, "Count Unique Clusters Pass", m_Timings.countUniqueClustersPass, "");
    atb::addVarRO(bar, "Light Assignement Pass", m_Timings.lightAssignementPass, "");
    atb::addVarRO(bar, "Light Transform Pass", m_Timings.lightTransformPass, "");
    atb::addVarRO(bar, "VPL Shading Pass", m_Timings.vplShadingPass, "");
    atb::addVarRO(bar, "Direct Light Shading Pass", m_Timings.directLightShadingPass, "");
    atb::addVarRO(bar, "Draw Buffers Pass", m_Timings.drawBuffersPass, "");
    atb::addVarRO(bar, "Compute VPL ISMs Pass", m_Timings.computeVPLISMPass, "");
    atb::addVarRO(bar, "Total time", m_Timings.totalTime, "");

    TwAddSeparator(bar, "GLClusteredISMRenderer::Separator2", "");
}

}


