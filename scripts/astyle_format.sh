#!/bin/sh

# A lancer depuis la racine du projet
astyle --options="scripts/format.astylerc" --recursive "*.cpp" "*.hpp"
