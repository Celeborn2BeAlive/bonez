#ifndef _BONEZ_FROMPOINTVPLSAMPLER_HPP
#define _BONEZ_FROMPOINTVPLSAMPLER_HPP

#include "VPLSampler.hpp"

namespace BnZ {

class FromPointVPLSampler {
public:
    Vec3f m_P;
    Col3f m_I;
    size_t m_nMinDepth;
    size_t m_nMaxDepth;
    size_t m_nLightPathCount;
    size_t m_nVPLCount;

    FromPointVPLSampler(Vec3f P, Col3f I, size_t minDepth, size_t maxDepth,
                        size_t lightPathCount, size_t vplCount):
        m_P(P), m_I(I), m_nMinDepth(minDepth), m_nMaxDepth(maxDepth), m_nLightPathCount(lightPathCount),
        m_nVPLCount(vplCount) {
    }

    virtual VPLContainer sample(const Camera& camera, const Scene& scene,
        const Framebuffer& framebuffer, size_t frameID, ParamSet& statistics);

    virtual std::string toString() const {
        return "FromPointVPLSampler";
    }
};

}

#endif // _BONEZ_FROMPOINTVPLSAMPLER_HPP
