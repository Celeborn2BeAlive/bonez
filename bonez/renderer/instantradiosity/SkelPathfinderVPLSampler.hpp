#ifndef _BONEZ_SKELPATHFINDERVPLSAMPLER_HPP_
#define _BONEZ_SKELPATHFINDERVPLSAMPLER_HPP_

#include "VPLFilter.hpp"
#include "VPLSampler.hpp"
#include "bonez/common/sampling/Sampler.hpp"

#include "bonez/scene/topology/CurvSkelImportanceDistribution.hpp"
#include "bonez/scene/topology/CurvSkelSensorNetwork.hpp"

#include "bonez/scene/topology/GraphImportancePoints.hpp"

#include "GeorgievVPLFilter.hpp"

#include "bonez/scene/topology/SkeletonPathfinder.hpp"

namespace BnZ {

class SkelPathfinderVPLSampler: public VPLSampler {
public:
    SkelPathfinderVPLSampler(size_t minDepth, size_t maxDepth, size_t lightPathCount, 
        size_t vplCount, float pskelThreshold, bool indirectOnly, const GeorgievVPLFilterPtr& vplFilter):
        m_nMinDepth(minDepth),
        m_nMaxDepth(maxDepth),
        m_nLightPathCount(lightPathCount),
        m_nVPLCount(vplCount),
        m_bIndirectOnly(indirectOnly),
        m_SkeletonPathfinder(pskelThreshold),
        m_GeorgievVPLFilter(vplFilter),
        m_bIsPreprocessed(false)
    {
    }
    
    struct LightPathVertex {
        Intersection I;
        BRDF brdf;
        Vec3f wi;
        size_t depth;
        Col3f L;
        
        enum SamplingStrategy {
            SKELETON_STRATEGY, BRDF_STRATEGY
        };
        SamplingStrategy samplingStrategy; // Sampling strategy choosed to sample the vertex
    };
    
    static OrientedVPL getVPL(const LightPathVertex& vertex);
    
    bool accept(OrientedVPL& vpl, Sampler& sampler);
    
    bool extendLightPath(const Scene& scene, LightPathVertex& vertex, Sampler& sampler);
    
    virtual VPLContainer sample(const Camera& camera, const Scene& scene,
        const Framebuffer& framebuffer, size_t frameID, ParamSet& statistics);
    
    virtual std::string toString() const;
    
    size_t m_nMinDepth;
    size_t m_nMaxDepth;
    size_t m_nLightPathCount;
    size_t m_nVPLCount;
    bool m_bIndirectOnly;
    
    const Scene* m_pScene;
    
    SkeletonPathfinder m_SkeletonPathfinder;
    
    GraphImportancePoints m_ImportancePoints;
 
    GeorgievVPLFilterPtr m_GeorgievVPLFilter;
    
    bool m_bIsPreprocessed;
};

}

#endif
