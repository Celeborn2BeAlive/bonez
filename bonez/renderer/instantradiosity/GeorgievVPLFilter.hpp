#ifndef _BONEZ_GEORGIEVVPLFILTER_HPP_
#define _BONEZ_GEORGIEVVPLFILTER_HPP_

#include "VPLFilter.hpp"
#include "bonez/renderer/Integrator.hpp"

namespace BnZ {

/**
 * A filter that implements the method described by Georgiev and al. in:
 * 
 */
class GeorgievVPLFilter: public VPLFilter {
public:
    /**
     * Constructor.
     * \param epsilon An epsilon added to the acceptance probability to avoid bias.
     * \param viewRayCountX The number of view rays used in X direction to estimate VPL contribution.
     * \param viewRayCountY The number of view rays used in Y direction to estimate VPL constribution.
     * \param incidentPowerEstimate An estimation of the incident power affecting the camera.
     * 
     * \remarks This version uses a provided incident power estimate.
     */
    GeorgievVPLFilter(float epsilon, size_t viewRayCountX, size_t viewRayCountY,
        Col3f incidentPowerEstimate, bool useFiltering, bool useResampling, size_t vplCount);
    
    /**
     * Constructor.
     * \param epsilon An epsilon added to the acceptance probability to avoid bias.
     * \param viewRayCountX The number of view rays used in X direction to estimate VPL contribution.
     * \param viewRayCountY The number of view rays used in Y direction to estimate VPL constribution.
     * on the camera.
     * \param sampler A sampler for the integrator.
     * \param integrator The Integrator that will be used to estimate the incident power on the camera.
     * 
     * \remarks This version compute an estimation of the incident power with a provided integrator
     */
    GeorgievVPLFilter(float epsilon, size_t viewRayCountX, size_t viewRayCountY,
        const IntegratorSamplerPtr& sampler, const IntegratorPtr& integrator,
        bool useFiltering, bool useResampling, size_t vplCount);

    /**
     * Initialize the filter for a given frame.
     */
    virtual void init(const Camera& camera, const Scene& scene,
        const Framebuffer& framebuffer, size_t frameID, ParamSet& stats);
    
    /**
     * Test if an OrientedVPL is accepted. If it is accepted, it's
     * intensity can be changed.
     * 
     * \return true if the VPL has been accepted.
     */
    virtual bool accept(OrientedVPL& vpl, Sampler& sampler);
    
    /**
     * Test if a DirectionalVPL is accepted. If it is accepted, it's
     * intensity can be changed.
     * 
     * \return true if the VPL has been accepted.
     */
    virtual bool accept(DirectionalVPL& vpl, Sampler& sampler);

    virtual VPLContainer resample(const VPLContainer& container, Sampler& sampler);

    virtual std::string toString() const {
        return "GeorgievFilter";
    }

private:
    template<typename VPL>
    float computeTransmittedPower(VPL& vpl);

    /**
     * Test if a VPL is accepted. If it is accepted, it's
     * intensity can be changed.
     * 
     * \return true if the VPL has been accepted.
     */
    template<typename VPL>
    bool genericAccept(VPL& vpl, Sampler& sampler);

    /**
     * Compute an estimation of the incident power on the camera.
     */
    void computeIncidentPowerEstimate(const Camera& camera, const Scene& scene,
        size_t frameID);
    
    /**
     * Compute the array of intersections that will be used to estimated
     * VPL power transmited to the camera.
     */
    void computeIntersections(const Camera& camera, const Scene& scene,
        size_t frameID);

    //! An epsilon added to acceptance probabilities to avoid bias
    float m_fEpsilon;
    //! The number of view rays used in X direction to estimate VPL contribution
    size_t m_nViewRayCountX;
    //! The number of view rays used in Y direction to estimate VPL constribution
    size_t m_nViewRayCountY;
    
    //! True if the incident power estimate has been provided
    bool m_bIncidentPowerEstimateProvided;
    //! An estimation of the incident power affecting the camera
    Col3f m_IncidentPowerEstimate;
    
    /**
     * Only used if m_bIncidentPowerEstimateProvided == false:
     */
    
    //! A sampler for the integrator
    IntegratorSamplerPtr m_pIntegratorSampler;
    //! The Integrator that will be used to estimate the incident power on the camera.
    IntegratorPtr m_pIntegrator;
    
    bool m_bUseFiltering;
    
    bool m_bUseResampling;
    
    size_t m_nVPLCount;
    
    //! An array of intersections used to estimate the power transmitted by a VPL to the camera
    std::unique_ptr<Intersection[]> m_Intersections;
    //! An array of BRDFs, each one corresponding to an intersection
    std::unique_ptr<BRDF[]> m_BRDFs;
    //! The number of intersections
    size_t m_nIntersectionCount;
    //! The reciprocal estimation of the desired VPL Power
    float m_fRcpDesiredVPLPowerEstimation;
    
    const Camera* m_pCamera;
    const Scene* m_pScene;
};

typedef std::shared_ptr<GeorgievVPLFilter> GeorgievVPLFilterPtr;

}

#endif
