#include <sstream>
#include <iomanip>

#include "RandomVPLSampler.hpp"

#include "bonez/common/Progress.hpp"

#include "bonez/common/sampling/Sampler.hpp"

namespace BnZ {

std::string RandomVPLSampler::toString() const {
    std::stringstream ss;
    ss << std::setfill('0') << "minDepth=" << std::setw(3) << m_nMinDepth << "_"
       << "maxDepth=" << std::setw(3) << m_nMaxDepth;
    
    if(m_nLightPathCount != size_t(embree::inf)) {
        ss << "_lightPathCount=" << std::setw(6) << m_nLightPathCount;
    }
    
    if(m_nVPLCount != size_t(embree::inf)) {
        ss << "_vplCount=" << std::setw(6) << m_nVPLCount;
    }
    
    if(m_bIndirectOnly) {
        ss << "_indirectOnly";
    }
    
    if(m_pFilter) {
        ss << "_" << m_pFilter->toString();
    }
    
    return ss.str();
}

OrientedVPL RandomVPLSampler::getVPL(const LightPathVertex& vertex) {
    return OrientedVPL(vertex.I.P, vertex.I.Ns, vertex.L * vertex.brdf.diffuseTerm());
}

bool RandomVPLSampler::extendLightPath(const Scene& scene, LightPathVertex& vertex, Sampler& sampler) {
    if(++vertex.depth > m_nMaxDepth) {
        return false;
    }

    // Sample a new exitant direction
    Sample3f wo;
    
    Col3f reflectionTerm = vertex.brdf.sample(vertex.wi, wo, sampler.get2DSample());
    
    if(reflectionTerm == Col3f(0.f) || wo.pdf <= 0.f) {
        return false;
    }
    
    float cos_o = embree::dot(vertex.I.Ns, wo.value);
    
    // Monte-Carlo estimation of the reflection factor
    reflectionTerm *= embree::rcp(wo.pdf) * cos_o;

    // If the depth is beyond minDepth, use russian roulette to eventually stop the path
    // if its contribution is too low
    if (vertex.depth > m_nMinDepth) {
        // Acceptance probability is the luminance of the reflection factor
        float rrProb = embree::min(1.f, embree::luminance(reflectionTerm));

        if (sampler.get1DSample() > rrProb) {
            return false;
        }

        // Next intersection accepted: update the reflection factor
        // to take into account the russian roulette
        reflectionTerm *= embree::rcp(rrProb);
    }

    // Compute the next intersection
    if(!(vertex.I = bounce(vertex.I, wo.value, scene))) {
        return false;
    }
    
    // Apply the reflection factor to the current power
    vertex.L *= reflectionTerm;
    // Incident direction for the next VPL
    vertex.wi = -wo.value;
    
    vertex.brdf = scene.shade(vertex.I);
    
    return true;
}

VPLContainer RandomVPLSampler::sample(const Camera& camera, const Scene& scene,
    const Framebuffer& framebuffer, size_t frameID, ParamSet& statistics) {
    std::clog << "Sampling of the VPLs..." << std::endl;

    Progress progress;
    progress.init("VPL Sampling", m_nLightPathCount);

    double start = embree::getSeconds();

    VPLContainer container;
    Sampler sampler(frameID);
    
    if(m_pFilter) {
        ParamSet vplFilterStats;
        m_pFilter->init(camera, scene, framebuffer, frameID, vplFilterStats);
        statistics.set("vplFilter", vplFilterStats);
    }

    size_t lightPathCount = 0;
    size_t vplCount = 0;
    size_t rejectedVPLCount = 0;

    // Sample enough lightpaths or VPLs according to the specified parameters
    while (lightPathCount < m_nLightPathCount && vplCount < m_nVPLCount) {
        //progress.set(lightPathCount);

        LightPathVertex currentVertex;
        
        EmitterVPL srcVPL = scene.lights.sampleVPL(scene, VPLRandom(sampler.get1DSample(), sampler.get1DSample(),
            sampler.get2DSample()));
        
        switch(srcVPL.type) {
        case EmitterVPL::DIRECTIONAL: {
            // If direct illumination is required, add a directional VPL
            if(!m_bIndirectOnly) {
                if(!m_pFilter || m_pFilter->accept(srcVPL.directional, sampler)) {
                    ++vplCount;
                    container.addVPL(srcVPL.directional);
                } else if(m_pFilter) {
                    ++rejectedVPLCount;
                }
            }
            
            // Sample the position of the ray
            Sample3f P = scene.sampleRayOrigin(srcVPL.directional.D, sampler.get2DSample());
            
            // Next VPL's intersection
            currentVertex.I = intersect(Ray(P.value, srcVPL.directional.D), scene);
            // Incident direction for the next VPL
            currentVertex.wi = -srcVPL.directional.D;
            // Monte carlo estimation of the power carried by the ray
            currentVertex.L = srcVPL.directional.L * embree::rcp(P.pdf);

        }
        break;
        
        case EmitterVPL::ORIENTED: {
            // If direct illumination is required, try adding an oriented VPL
            if(!m_bIndirectOnly) {
                if(!m_pFilter || m_pFilter->accept(srcVPL.oriented, sampler)) {
                    ++vplCount;
                    container.addVPL(srcVPL.oriented);
                } else if(m_pFilter) {
                    ++rejectedVPLCount;
                }
            }
            
            // Sample an exitant direction
            Vec2f dirSample = sampler.get2DSample();
            Sample3f wo = embree::cosineSampleHemisphere(dirSample.x, dirSample.y, srcVPL.oriented.N);

            // Monte-carlo estimation of the power carried by the ray
            currentVertex.L = srcVPL.oriented.L * embree::dot(srcVPL.oriented.N, wo.value) * embree::rcp(wo.pdf);
            // Next VPL's intersection
            currentVertex.I = intersect(light_ray(srcVPL.oriented.P, wo.value), scene);
            // Incident direction for the next VPL
            currentVertex.wi = -wo.value;
        }        
        break;
        }
        
        if(currentVertex.I) {
            // BRDF of the next VPL
            currentVertex.brdf = scene.shade(currentVertex.I);
            // Depth of the next VPL
            currentVertex.depth = 1;

            // Extend the path
            while (currentVertex.depth <= m_nMaxDepth && vplCount < m_nVPLCount) {
                // If no intersection, the path is over
                if(!currentVertex.I) {
                    break;
                }
                
                // Create a new oriented diffuse VPL
                OrientedVPL vpl = getVPL(currentVertex);
                
                if(!m_pFilter || m_pFilter->accept(vpl, sampler)) {
                    ++vplCount;
                    container.addVPL(vpl);
                } else if(m_pFilter) {
                    ++rejectedVPLCount;
                }

                if(vplCount == m_nVPLCount || !extendLightPath(scene, currentVertex, sampler)) {
                    break;
                }
            }
        }

        ++lightPathCount;
    }
    
    progress.end();
    
    // Apply the monte-carlo estimation by dividing all the estimated powers
    // by the number of light path that have been drawn:

    float rcpLightPathCount = embree::rcp(float(lightPathCount));

    Col3f vplTotalPower(0.f);
        
    for(OrientedVPL& vpl: container.orientedVPLs) {
        vpl.L *= rcpLightPathCount;
        vplTotalPower += vpl.L;
    }
    
    for(DirectionalVPL& vpl: container.directionalVPLs) {
        vpl.L *= rcpLightPathCount;
        vplTotalPower += vpl.L;
    }
    
    double time = embree::getSeconds() - start;
    
    std::clog << "Number of accepted VPLs: " << vplCount << std::endl;
    std::clog << "Number of rejected VPLs: " << rejectedVPLCount << std::endl;
    std::clog << "Acceptance ratio: " << 100.f * vplCount / (vplCount + rejectedVPLCount) << std::endl;
    
    statistics.set("time", time);
    statistics.set("directionalVPLCount", container.directionalVPLs.size());
    statistics.set("orientedVPLCount", container.orientedVPLs.size());
    statistics.set("VPLCount", container.size());
    statistics.set("rejectedVPLCount", rejectedVPLCount);
    statistics.set("acceptanceRatio", 100.f * vplCount / (vplCount + rejectedVPLCount));
    statistics.set("VPLTotalPower", vplTotalPower);
    statistics.set("VPLMeanPower", vplTotalPower / (float) container.size());
    
    if(m_pFilter) {
        std::clog << "Resampling..." << std::endl;
        
        container = m_pFilter->resample(container, sampler);
    }

    std::clog << "Number of VPLs: " << container.size() << std::endl;

    return container;
}
    
}
