#include <sstream>
#include <iomanip>

#include "CurvSkelVPLSampler.hpp"

#include "bonez/common/Progress.hpp"

#include "bonez/common/sampling/Sampler.hpp"

namespace BnZ {

std::string CurvSkelVPLSampler::toString() const {
    std::stringstream ss;
    ss << std::setfill('0') << "minDepth=" << std::setw(3) << m_nMinDepth << "_"
       << "maxDepth=" << std::setw(3) << m_nMaxDepth;
    
    if(m_nLightPathCount != size_t(embree::inf)) {
        ss << "_lightPathCount=" << std::setw(6) << m_nLightPathCount;
    }
    
    if(m_nVPLCount != size_t(embree::inf)) {
        ss << "_vplCount=" << std::setw(6) << m_nVPLCount;
    }
    
    if(m_bIndirectOnly) {
        ss << "_indirectOnly";
    }
    
    return ss.str();
}

bool CurvSkelVPLSampler::accept(OrientedVPL& vpl, Sampler& sampler) {
    if(m_GeorgievVPLFilter) {
        return m_GeorgievVPLFilter->accept(vpl, sampler);
    }
    
    // Use the skeleton to estimate the importance of the VPL and reject/accept it based on this importance
    float skelRRProb = 0.f;
    GraphNodeIndex node = m_pScene->voxelSpace->getSkeleton().getNearestNode(vpl.P, vpl.N);

    if (node != UNDEFINED_NODE && 0.f != m_SkelImportanceDistribution.getMaxDensity()) {
        // Formula 1
        // skelRRProb = embree::min(1.f, m_ImportanceEstimationParams.acceptanceEpsilon + skelImportanceDistribution.getDensity(node) * embree::luminance(vpl.L));

        // Formula 2
        skelRRProb = embree::min(1.f, m_SkelImportanceDistributionParams.m_fEpsilon + m_SkelImportanceDistribution.getDensity(node) * embree::rcp(m_SkelImportanceDistribution.getMaxDensity()));
    
        // Formula 3
        // skelRRProb = embree::min(1.f, m_ImportanceEstimationParams.acceptanceEpsilon + skelImportanceDistribution.getDensity(node) * embree::rcp(skelImportanceDistribution.getMeanDensity()));
        
        // Formula 4
        // skelRRProb = embree::min(1.f, m_ImportanceEstimationParams.acceptanceEpsilon + skelImportanceDistribution.getDensity(node) * embree::rcp(skelImportanceDistribution.getTotalDensity()));
    
    } else {
        skelRRProb = embree::min(1.f, m_SkelImportanceDistributionParams.m_fEpsilon);
    }
    
    if (sampler.get1DSample() < skelRRProb) {
        if (skelRRProb > 0) {
            vpl.L *= embree::rcp(skelRRProb);
        }
        return true;
    }
    return false;
}

OrientedVPL CurvSkelVPLSampler::getVPL(const LightPathVertex& vertex) {
    return OrientedVPL(vertex.I.P, vertex.I.Ns, vertex.L * vertex.brdf.diffuseTerm());
}

Sample3f sampleSkeletonDirection(const CurvSkelVPLSampler::LightPathVertex& vertex, const Vec3f& importancePoint,
    const CurvilinearSkeleton& skeleton, Sampler& sampler) {
    
    
    CurvilinearSkeleton::Node node = skeleton[skeleton.getNearestNode(importancePoint)];
    float radius = embree::sqrt(node.getMaxball());
    
    Vec2f diskSample = embree::uniformSampleDisk(sampler.get2DSample(), radius);
    
    Vec3f dir = importancePoint - vertex.I.P;
    float length = embree::length(dir);
    dir /= length;
    LinearSpace3f frame = coordinateSystem(dir);
    
    Vec3f wo = embree::normalize(
        node.getPosition() + frame.vx * diskSample.x + frame.vz * diskSample.y - vertex.I.P
    );
    
    return Sample3f(
        wo,
        embree::sqr(length) * embree::rcp(embree::dot(dir, wo) * embree::sqr(radius) * float(embree::pi))
    );
}

float pdfSkeletonDirection(const Vec3f& wo, const CurvSkelVPLSampler::LightPathVertex& vertex, const Vec3f& importancePoint,
    const CurvilinearSkeleton& skeleton) {
    GraphNodeIndex nodeIdx = skeleton.getNearestNode(importancePoint);
    
    if(!nodeIdx) {
        return 0.f;
    }
    
    CurvilinearSkeleton::Node node = skeleton[nodeIdx];
    float radius = embree::sqrt(node.getMaxball());
    
    Vec3f dir = importancePoint - vertex.I.P;
    
    if(embree::dot(dir, vertex.I.Ns) < 0.f) {
        return 0.f;
    }
    
    float length = embree::length(dir);
    dir /= length;
    
    float minCos = length / embree::sqrt(length * length + radius * radius);
    
    if(embree::dot(wo, dir) < minCos) {
        return 0.f;
    }
    
    return embree::sqr(length) * embree::rcp(embree::dot(dir, wo) * embree::sqr(radius) * float(embree::pi));
}

#if 0

bool CurvSkelVPLSampler::extendLightPath(const Scene& scene, LightPathVertex& vertex, Sampler& sampler) {
    if(++vertex.depth > m_nMaxDepth) {
        return false;
    }
    
    Col3f reflectionTerm;
    Sample3f wo;
    
    NodeIndex nodeIdx = scene.voxelSpace->getSkeleton().getNearestNode(vertex.I);
    
    Vec3f importancePoints[NB_IMPORTANCE_POINT_SETS];
    size_t nbImportancePoints = 0;
    
    float pskel = 0.f;
    if(nodeIdx != NO_NODE()) {
        for(size_t i = 0; i < NB_IMPORTANCE_POINT_SETS; ++i) {
            if(m_SkelImportancePoints[i].hasImportancePoint(nodeIdx)) {
                importancePoints[nbImportancePoints++] = m_SkelImportancePoints[i][nodeIdx];
            }
        }
        pskel = 0.5f;
    }
    
    if(sampler.get1DSample() < pskel) {
        float impPointSample = sampler.get1DSample() * nbImportancePoints;
        
        size_t impPointIdx = impPointSample;
        
        Vec3f importanceDirection = embree::normalize(importancePoints[impPointIdx] - vertex.I.position);
        
        if(embree::dot(importanceDirection, vertex.I.Ns) <= 0.f) {
            return false;
        }
        
        //wo = CosineHemisphereSampler::sample(sampler.get1DSample(), sampler.get1DSample(), importanceDirection);
        
        wo = sampleSkeletonDirection(vertex, importancePoints[impPointIdx], scene.voxelSpace->getSkeleton(), sampler);
        
        if(wo.pdf == 0.f) {
            return false;
        }
        
        float cos_o = embree::dot(vertex.I.Ns, wo.value);
        if(cos_o <= 0.f) {
            return false;
        }
        
        reflectionTerm = vertex.brdf.eval(vertex.wi, wo.value);
        
        if(reflectionTerm == Col3f(0.f)) {
            return false;
        }
        
        float pdf = 0.f;
        for(size_t i = 0; i < nbImportancePoints; ++i) {
            //Vec3f importanceDirection = embree::normalize(importancePoints[i] - vertex.I.position);
            //pdf += pskel * (1.f / nbImportancePoints) * CosineHemisphereSampler::pdf(wo.value, importanceDirection);
            
            pdf += pskel * (1.f / nbImportancePoints) * pdfSkeletonDirection(wo.value, vertex, 
                importancePoints[i], scene.voxelSpace->getSkeleton());
        }
        
        pdf += (1 - pskel) * vertex.brdf.pdf(vertex.wi, wo);
        reflectionTerm *= embree::rcp(pdf) * cos_o;
        
        vertex.samplingStrategy = LightPathVertex::SKELETON_STRATEGY;
        
    } else {
        reflectionTerm = vertex.brdf.sample(vertex.wi, wo, sampler.get2DSample());
        
        if(reflectionTerm == Col3f(0.f) || wo.pdf <= 0.f) {
            return false;
        }
        
        float cos_o = embree::dot(vertex.I.Ns, wo.value);
        if(cos_o < 0.f) {
            return false;
        }
        
        float pdf = 0.f;
        if(nbImportancePoints) {
            for(size_t i = 0; i < nbImportancePoints; ++i) {
                //Vec3f importanceDirection = embree::normalize(importancePoints[i] - vertex.I.position);
                //pdf += embree::max(0.f, CosineHemisphereSampler::pdf(wo.value, importanceDirection));
                
                pdf += pdfSkeletonDirection(wo.value, vertex, importancePoints[i], scene.voxelSpace->getSkeleton());
            }
            pdf *= pskel * (1.f / nbImportancePoints);
            pdf += (1 - pskel) * wo.pdf;
        } else {
            pdf = wo.pdf;
        }
        
        reflectionTerm *= embree::rcp(pdf) * cos_o;
        
        vertex.samplingStrategy = LightPathVertex::BRDF_STRATEGY;
    }
    
    // If the depth is beyond minDepth, use russian roulette to eventually stop the path
    // if its contribution is too low
    if (vertex.depth > m_nMinDepth) {
        // Acceptance probability is the luminance of the reflection factor
        float rrProb = embree::min(1.f, embree::luminance(reflectionTerm));

        if (sampler.get1DSample() > rrProb) {
            return false;
        }

        // Next intersection accepted: update the reflection factor
        // to take into account the russian roulette
        reflectionTerm *= embree::rcp(rrProb);
    }

    // Compute the next intersection
    if(!(vertex.I = bounce(vertex.I, wo.value, scene))) {
        return false;
    }
    
    // Apply the reflection factor to the current power
    vertex.L *= reflectionTerm;
    // Incident direction for the next VPL
    vertex.wi = -wo.value;
    
    vertex.brdf = scene.shade(vertex.I);
    
    return true;
}

#else

bool CurvSkelVPLSampler::extendLightPath(const Scene& scene, LightPathVertex& vertex, Sampler& sampler) {
    if(++vertex.depth > m_nMaxDepth) {
        return false;
    }
    
    Col3f reflectionTerm;
    Sample3f wo;
    
    GraphNodeIndex clusterIdx = scene.voxelSpace->getClustering().getNearestNode(vertex.I);
    
    float pskel = 0.f;
    if(clusterIdx && m_ImportancePoints.hasImportancePoint(clusterIdx)) {
        if(scene.voxelSpace->getSkeleton().getNearestNode(m_ImportancePoints[clusterIdx])) {
            Vec3f importanceDirection = embree::normalize(m_ImportancePoints[clusterIdx] - vertex.I.P);
            if(embree::dot(importanceDirection, vertex.I.Ns) > 0.f) {
                pskel = 0.5f;
            }
        }
    }
    
    if(sampler.get1DSample() < pskel) {
        wo = sampleSkeletonDirection(vertex, m_ImportancePoints[clusterIdx], scene.voxelSpace->getSkeleton(), sampler);
        
        if(wo.pdf == 0.f) {
            return false;
        }
        
        float cos_o = embree::dot(vertex.I.Ns, wo.value);
        if(cos_o <= 0.f) {
            return false;
        }
        
        reflectionTerm = vertex.brdf.eval(vertex.wi, wo.value);
        
        if(reflectionTerm == Col3f(0.f)) {
            return false;
        }
        
        float pdf = pskel * wo.pdf + (1 - pskel) * vertex.brdf.pdf(vertex.wi, wo);
        reflectionTerm *= embree::rcp(pdf) * cos_o;
        
        vertex.samplingStrategy = LightPathVertex::SKELETON_STRATEGY;
        
    } else {
        reflectionTerm = vertex.brdf.sample(vertex.wi, wo, sampler.get2DSample());
        
        if(reflectionTerm == Col3f(0.f) || wo.pdf <= 0.f) {
            return false;
        }
        
        float cos_o = embree::dot(vertex.I.Ns, wo.value);
        if(cos_o < 0.f) {
            return false;
        }
        
        float pdf = wo.pdf;
        
        if(pskel) {
            pdf = (1 - pskel) * pdf + pskel * pdfSkeletonDirection(wo.value, vertex, m_ImportancePoints[clusterIdx], scene.voxelSpace->getSkeleton());
        }
        
        reflectionTerm *= embree::rcp(pdf) * cos_o;
        
        vertex.samplingStrategy = LightPathVertex::BRDF_STRATEGY;
    }
    
    // If the depth is beyond minDepth, use russian roulette to eventually stop the path
    // if its contribution is too low
    if (vertex.depth > m_nMinDepth) {
        // Acceptance probability is the luminance of the reflection factor
        float rrProb = embree::min(1.f, embree::luminance(reflectionTerm));

        if (sampler.get1DSample() > rrProb) {
            return false;
        }

        // Next intersection accepted: update the reflection factor
        // to take into account the russian roulette
        reflectionTerm *= embree::rcp(rrProb);
    }

    // Compute the next intersection
    if(!(vertex.I = bounce(vertex.I, wo.value, scene))) {
        return false;
    }
    
    // Apply the reflection factor to the current power
    vertex.L *= reflectionTerm;
    // Incident direction for the next VPL
    vertex.wi = -wo.value;
    
    vertex.brdf = scene.shade(vertex.I);
    
    return true;
}


#endif

#if 0
void CurvSkelVPLSampler::displayRayData(const Ray& ray, const Scene& scene, GLRenderer& renderer) {
    if(!m_bIsPreprocessed) {
        return;
    }
    
    renderer.removeObject(m_nGLObjectGroup);
    
    Intersection I = intersect(ray, scene);
    if(!I) {
        return;
    }
    
    GLRenderGroupPtr group = std::make_shared<GLRenderGroup>();

/*
    CurvilinearSkeleton::NodeIndex nodeIdx = scene.voxelSpace->getSkeleton().getNearestNode(I);
    if(nodeIdx == CurvilinearSkeleton::NO_NODE()) {
        return;
    }
    
    Vec3f importanceDirection = m_SkelImportancePoints[0][nodeIdx] - I.position;
        
    if(embree::dot(importanceDirection, I.Ns) <= 0.f) {
        return;
    }

    std::shared_ptr<GLLine> line = std::make_shared<GLLine>(renderer.getProgramBuilder()); 
    line->build(I.position, I.position + importanceDirection, Col3f(1, 0, 0), Col3f(0, 1, 0));
    
    group->addObject(line);
*/

    Sampler sampler(0);
    
    for(size_t i = 0; i < 128; ++i) {
        LightPathVertex vertex, previousVertex;
        vertex.I = I;
        vertex.brdf = scene.shade(I);
        vertex.wi = -ray.dir;
        vertex.depth = 0;
        vertex.L = embree::Col3f(1.f);
        
        previousVertex = vertex;
        
        while(extendLightPath(scene, vertex, sampler)) {
            // If no intersection, the path is over
            if(!vertex.I) {
                break;
            }
            
            std::shared_ptr<GLLine> line = std::make_shared<GLLine>(renderer.getProgramBuilder());
            
            Col3f color = Col3f(1, 0, 0);
            if(vertex.samplingStrategy == LightPathVertex::BRDF_STRATEGY) {
                color = Col3f(0, 1, 0);
            }
            
            line->build(previousVertex.I.P, vertex.I.P, color, color);
            
            group->addObject(line);
            
            // Create a new oriented diffuse VPL
            /*OrientedVPL vpl = getVPL(vertex);
            
            if(accept(vpl, sampler)) {
                ++vplCount;
                container.addVPL(vpl);
            } else {
                ++rejectedVPLCount;
            }*/

            previousVertex = vertex;
        }
    }
    

    renderer.setObject(group, m_nGLRayObjectGroup);
}
#endif

VPLContainer CurvSkelVPLSampler::sample(const Camera& camera, const Scene& scene,
    const Framebuffer& framebuffer, size_t frameID, ParamSet& statistics) {
    
    /*
    if(glRenderer) {
        ClusterVector minimas = computeLocalMinimas(scene.topology.clustering);
        
        std::cerr << "Number of minimas = " << minimas.size() << std::endl;
        
        GLRenderGroupPtr group = std::make_shared<GLRenderGroup>();
        for(CurvSkelClustering::ClusterIndex idx: minimas) {
            std::shared_ptr<GLSphere> glSphere =
                std::make_shared<GLSphere>(glRenderer->getProgramBuilder());
            
            glSphere->build(scene.topology.clustering.getCluster(idx).center,
                scene.topology.clustering.getCluster(idx).meanMaxBallRadius, Col3f(1, 1, 0));
            
            group->addObject(glSphere);
        }
        glRenderer->setObject(group, m_nGLObjectGroup);
    }
    */
    
    std::clog << "Sampling of the VPLs..." << std::endl;
    
    Random rng(frameID);
    //m_SkelSensorNetwork.build(m_nSensorCountPerNode, scene, rng);
    
    m_pScene = &scene;
    
    // Build skel importance distribution
    ParamSet skelImpStats;
    computeCurvSkelImportanceDistribution(m_SkelImportanceDistribution, m_SkelImportanceDistributionParams.m_nViewRayCountX,
        m_SkelImportanceDistributionParams.m_nViewRayCountY, m_SkelImportanceDistributionParams.m_nScatteringRayCountX,
        m_SkelImportanceDistributionParams.m_nScatteringRayCountY, false, scene, camera, rng, &skelImpStats);
    m_SkelImportanceDistribution.filter(scene, m_SkelImportanceDistributionParams.m_sSkelImportanceFilter, &skelImpStats);
    
    statistics.set("skelImportance", skelImpStats);
    
#if 0
    std::vector<size_t> mostImportantNodes;
    
    CurvSkelImportanceDistribution copy =  m_SkelImportanceDistribution;
    
    for(size_t i = 0; i < NB_IMPORTANCE_POINT_SETS; ++i) {
        float max = 0.f;
        NodeIndex best = NO_NODE();
        
        for(NodeIndex node = 0; node < scene.voxelSpace->getSkeleton().size(); ++node) {
            if(copy.getDensity(node) > max) {
                best = node;
                max = copy.getDensity(node);
            }
        }
        
        mostImportantNodes.push_back(best);
        
        const CurvSkelClustering::Cluster& cluster = 
            scene.topology.getNodeCluster(best);
        
        for(size_t j = 0; j < cluster.nodes.size(); ++j) {
            copy.setDensity(cluster.nodes[j], 0);
        }
    }
    
    for(size_t i = 0; i < NB_IMPORTANCE_POINT_SETS; ++i) {
        m_SkelImportancePoints[i].computeFromSkeleton(scene, scene.voxelSpace->getSkeleton()[mostImportantNodes[i]].getPosition());
        
        if(glRenderer) {
            /*
            auto positionFunctor = [&scene](NodeIndex node) -> Vec3f {
                return scene.topology.skeleton[node].getPosition();
            };
            
            GraphImportancePoints importancePoints;
            importancePoints.compute(scene.topology.skeleton.getGraph(), scene,
                scene.voxelSpace->getSkeleton()[mostImportantNodes[i]].getPosition(), mostImportantNodes[i],
                positionFunctor
            );
            
            std::shared_ptr<GLGraphImportanceDirections> glImportanceDirections =
                std::make_shared<GLGraphImportanceDirections>(glRenderer->getProgramBuilder());
            
            glImportanceDirections->build(scene.topology.skeleton.getGraph(), importancePoints,
                positionFunctor);
            
            glRenderer->setObject(glImportanceDirections, m_nGLObjectGroup);
            */
            
            auto positionFunctor = [&scene](NodeIndex node) -> Vec3f {
                return scene.topology.clustering[node].center;
            };
            
            GraphNodeIndex cluster = scene.topology.getNodeClusterIndex(mostImportantNodes[i]);
            
            GraphImportancePoints importancePoints;
            importancePoints.compute(scene.topology.clustering.getGraph(), scene,
                scene.topology.clustering[cluster].center, cluster,
                positionFunctor
            );

            std::shared_ptr<GLGraphImportanceDirections> glImportanceDirections =
                std::make_shared<GLGraphImportanceDirections>(glRenderer->getProgramBuilder());
            
            glImportanceDirections->build(scene.topology.clustering.getGraph(), importancePoints,
                positionFunctor);
            
            glRenderer->setObject(glImportanceDirections, m_nGLObjectGroup);
            
            
            /*
            GLRenderGroupPtr group = std::make_shared<GLRenderGroup>();
            
            std::shared_ptr<GLImportanceDirections> glImportanceDirections =
                std::make_shared<GLImportanceDirections>(glRenderer->getProgramBuilder());
            
            glImportanceDirections->build(scene.voxelSpace->getSkeleton(), m_SkelImportancePoints[i]);
            
            group->addObject(glImportanceDirections);
            
            std::shared_ptr<GLSphere> glSphere =
                std::make_shared<GLSphere>(glRenderer->getProgramBuilder());
            
            glSphere->build(scene.voxelSpace->getSkeleton()[mostImportantNodes[i]].getPosition(),
                embree::sqrt(scene.voxelSpace->getSkeleton()[mostImportantNodes[i]].getMaxball()), Col3f(1, 1, 0));
            
            group->addObject(glSphere);
            
            glRenderer->setObject(group, m_nGLObjectGroup);*/
        }
    }
#else
    auto positionFunctor = [&scene](GraphNodeIndex node) -> Vec3f {
        return scene.voxelSpace->getClustering()[node].center;
    };
    
    GraphNodeIndex cluster = scene.voxelSpace->getClustering().getNearestNode(camera.getPosition());
    
    m_ImportancePoints.compute(scene.voxelSpace->getClustering().getGraph(), scene,
        scene.voxelSpace->getClustering()[cluster].center, cluster,
        positionFunctor
    );


    /*
    if(glRenderer) {
        std::shared_ptr<GLGraphImportanceDirections> glImportanceDirections =
            std::make_shared<GLGraphImportanceDirections>(glRenderer->getProgramBuilder());
        
        glImportanceDirections->build(scene.topology.clustering.getGraph(), m_ImportancePoints,
            positionFunctor);
        
        glRenderer->setObject(glImportanceDirections, m_nGLObjectGroup);
    }*/
#endif

    
    
    if(m_GeorgievVPLFilter) {
        m_GeorgievVPLFilter->init(camera, scene, framebuffer, frameID, statistics);
    }

    Progress progress;
    progress.init("VPL Sampling", m_nLightPathCount);

    double start = embree::getSeconds();

    VPLContainer container;
    Sampler sampler(frameID);
    
    size_t lightPathCount = 0;
    size_t vplCount = 0;
    size_t rejectedVPLCount = 0;

    // Sample enough lightpaths or VPLs according to the specified parameters
    while (lightPathCount < m_nLightPathCount && vplCount < m_nVPLCount) {
        //progress.set(lightPathCount);

        LightPathVertex currentVertex;
        
        EmitterVPL srcVPL = scene.lights.sampleVPL(scene, VPLRandom(sampler.get1DSample(), sampler.get1DSample(),
            sampler.get2DSample()));
        
        switch(srcVPL.type) {
        case EmitterVPL::DIRECTIONAL: {
            // If direct illumination is required, add a directional VPL
            if(!m_bIndirectOnly) {
                if(!m_GeorgievVPLFilter || m_GeorgievVPLFilter->accept(srcVPL.directional, sampler)) {
                    ++vplCount;
                    container.addVPL(srcVPL.directional);
                } else if(m_GeorgievVPLFilter) {
                    ++rejectedVPLCount;
                }
            }
            
            // Sample the position of the ray
            Sample3f P = scene.sampleRayOrigin(srcVPL.directional.D, sampler.get2DSample());
            
            // Next VPL's intersection
            currentVertex.I = intersect(Ray(P.value, srcVPL.directional.D), scene);
            // Incident direction for the next VPL
            currentVertex.wi = -srcVPL.directional.D;
            // Monte carlo estimation of the power carried by the ray
            currentVertex.L = srcVPL.directional.L * embree::rcp(P.pdf);
            
        }
        break;
        
        case EmitterVPL::ORIENTED: {
            // If direct illumination is required, try adding an oriented VPL
            if(!m_bIndirectOnly) {
                if(!m_GeorgievVPLFilter || m_GeorgievVPLFilter->accept(srcVPL.oriented, sampler)) {
                    ++vplCount;
                    container.addVPL(srcVPL.oriented);
                } else if(m_GeorgievVPLFilter) {
                    ++rejectedVPLCount;
                }
            }
            
            // Sample an exitant direction
            Vec2f dirSample = sampler.get2DSample();
            Sample3f wo = embree::cosineSampleHemisphere(dirSample.x, dirSample.y, srcVPL.oriented.N);

            // Monte-carlo estimation of the power carried by the ray
            currentVertex.L = srcVPL.oriented.L * embree::dot(srcVPL.oriented.N, wo.value) * embree::rcp(wo.pdf);
            // Next VPL's intersection
            currentVertex.I = intersect(light_ray(srcVPL.oriented.P, wo.value), scene);
            // Incident direction for the next VPL
            currentVertex.wi = -wo.value;
        }        
        break;
        }
        
        if(currentVertex.I) {
            // BRDF of the next VPL
            currentVertex.brdf = scene.shade(currentVertex.I);
            // Depth of the next VPL
            currentVertex.depth = 1;

            // Extend the path
            while (currentVertex.depth <= m_nMaxDepth && vplCount < m_nVPLCount) {
                // If no intersection, the path is over
                if(!currentVertex.I) {
                    break;
                }
                
                // Create a new oriented diffuse VPL
                OrientedVPL vpl = getVPL(currentVertex);
                
                if(accept(vpl, sampler)) {
                    ++vplCount;
                    container.addVPL(vpl);
                } else {
                    ++rejectedVPLCount;
                }

                if(vplCount == m_nVPLCount || !extendLightPath(scene, currentVertex, sampler)) {
                    break;
                }
            }
        }

        ++lightPathCount;
    }
 
    progress.end();
    
    // Apply the monte-carlo estimation by dividing all the estimated powers
    // by the number of light path that have been drawn:

    float rcpLightPathCount = embree::rcp(float(lightPathCount));

    Col3f vplTotalPower(0.f);
        
    for(OrientedVPL& vpl: container.orientedVPLs) {
        vpl.L *= rcpLightPathCount;
        vplTotalPower += vpl.L;
    }
    
    for(DirectionalVPL& vpl: container.directionalVPLs) {
        vpl.L *= rcpLightPathCount;
        vplTotalPower += vpl.L;
    }
    
    double time = embree::getSeconds() - start;
    
    std::clog << "Number of accepted VPLs: " << vplCount << std::endl;
    std::clog << "Number of rejected VPLs: " << rejectedVPLCount << std::endl;
    std::clog << "Acceptance ratio: " << 100.f * vplCount / (vplCount + rejectedVPLCount) << std::endl;
    
    statistics.set("time", time);
    statistics.set("directionalVPLCount", container.directionalVPLs.size());
    statistics.set("orientedVPLCount", container.orientedVPLs.size());
    statistics.set("VPLCount", container.size());
    statistics.set("rejectedVPLCount", rejectedVPLCount);
    statistics.set("acceptanceRatio", 100.f * vplCount / (vplCount + rejectedVPLCount));
    statistics.set("VPLTotalPower", vplTotalPower);
    statistics.set("VPLMeanPower", vplTotalPower / (float) container.size());

    std::clog << "Number of VPLs: " << container.size() << std::endl;

    m_bIsPreprocessed = true;
    
    return container;
}
    
}
