#include "VPLIntegrator.hpp"

#include "bonez/scene/shading/BRDF.hpp"

namespace BnZ {

std::string VPLIntegrator::toString() const {
    std::stringstream ss;
    ss << "VPLIntegrator_" <<
        "geometryClamp=" << m_fGeometryClamping << "_" <<
          "rrThreshold=" << m_fRRTreshold << "_" <<
          "useSkelShadowApprix=" << m_bUseSkelShadowApprox;
    return ss.str() + "_" + m_pVPLSampler->toString();
}

void VPLIntegrator::preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
    size_t frameIdx, ParamSet& statistics) {
    m_VPLContainer = m_pVPLSampler->sample(camera, scene, framebuffer, frameIdx, statistics);
}

Col3f VPLIntegrator::Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
    RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
    Intersection intersection = intersect(ray, scene, rayCounter);

    if (!intersection) {
        return scene.lights.Le(-ray.dir);
    }

    GraphNodeIndex node1 = 0;
    if(m_bUseSkelShadowApprox) {
        node1 = scene.voxelSpace->getClustering().getNearestNode(intersection);
        if(node1 == UNDEFINED_NODE) {
            return Col3f(0.95, 0, 0.63);
        }
    }

    Vec3f wo = -ray.dir;
    Col3f L = intersection.Le;

    BRDF brdf = scene.shade(intersection);

    for(const DirectionalVPL& vpl: m_VPLContainer.directionalVPLs) {
        Vec3f wi = -vpl.D;
        
        float cos_i = embree::dot(intersection.Ns, wi);
        if(cos_i <= 0.f) {
            continue;
        }
        
        Col3f reflectionFactor = brdf.eval(wi, wo);
        if(reflectionFactor == Col3f(embree::zero)) {
            continue;
        }
        
        if(occluded(shadow_ray(intersection, wi, embree::inf), scene, rayCounter)) {
            continue;
        }
        
        L += reflectionFactor * cos_i * vpl.L;
    }
    
    for(const OrientedVPL& vpl: m_VPLContainer.orientedVPLs) {
        Ray shadowRay;
        float geometryTerm = embree::min(m_fGeometryClamping, 
            G(intersection, vpl, shadowRay));
        
        if(geometryTerm == 0.f) {
            continue;
        }
        
        Col3f reflectionTerm = brdf.eval(shadowRay.dir, wo);
        if(reflectionTerm == Col3f(embree::zero)) {
            continue;
        }

        Col3f vplContrib = vpl.L * geometryTerm * reflectionTerm;

        float luminance = embree::luminance(vplContrib);
        if (luminance < m_fRRTreshold) {
            float rrProb = embree::min(1.f, luminance);

            if (sample.getRandomFloat() > rrProb) {
                continue;
            }

            vplContrib *= embree::rcp(rrProb);
        }

#if 1
        if(m_bUseSkelShadowApprox) {
            GraphNodeIndex node2 = scene.voxelSpace->getClustering().getNearestNode(vpl.P, vpl.N);
            if(node2 == UNDEFINED_NODE) {
                continue;
            }

            // Binary visibility
            /*if(!scene.voxelSpace->getClustering().checkVisibility(node1, node2)) {
                continue;
            }*/

            // Multi cluster
            Skeleton::Node node = scene.voxelSpace->getClustering().getNode(node1);
            float weight = 1.f / embree::distance(node.P, intersection.P);
            float factor = weight * scene.voxelSpace->getClustering().getVisibilityFactor(node1, node2);
            float sumWeights = weight;
            for(auto neighbour: scene.voxelSpace->getClustering().neighbours(node1)) {
                node = scene.voxelSpace->getClustering().getNode(neighbour);
                weight = 1.f / embree::distance(node.P, intersection.P);
                factor += weight * scene.voxelSpace->getClustering().getVisibilityFactor(neighbour, node2);
                sumWeights += weight;
            }
            factor /= sumWeights;
            if(factor < 0.5) {
                continue;
            }


            // Float visibility
            // vplContrib *= scene.voxelSpace->getClustering().getVisibilityFactor(node1, node2);

            // Russian roulette visibility

            /*
            float rrProb = embree::min(1.f, scene.voxelSpace->getClustering().getVisibilityFactor(node1, node2));

            if (sample.getRandomFloat() > rrProb) {
                continue;
            }

            if(rrProb < 1.f) {
                if(occluded(shadowRay, scene, rayCounter)) {
                    continue;
                }
            }
            vplContrib *= embree::rcp(rrProb);
            */

            // With sensors
            /*if(!scene.voxelSpace->getClustering().checkVisibilityWithSensors(node1, vpl.P, vpl.N)
                    || !scene.voxelSpace->getClustering().checkVisibilityWithSensors(node2, intersection.P, intersection.Ns)) {
                continue;
            }*/

            // Try another thing
            /*
            Skeleton::Node n1 = scene.voxelSpace->getClustering().getNode(node1);
            Skeleton::Node n2 = scene.voxelSpace->getClustering().getNode(node2);

            Ray r1 = shadow_ray(vpl.P, n1.P);
            Ray r2 = shadow_ray(intersection.P, n2.P);

            if(occluded(r1, scene, rayCounter) || occluded(r2, scene, rayCounter)) {
                continue;
            }
            */
        } else {
            if(occluded(shadowRay, scene, rayCounter)) {
                continue;
            }
        }
#endif
        L += vplContrib;
    }
    
    return L;
}

}
