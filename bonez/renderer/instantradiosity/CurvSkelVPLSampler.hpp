#ifndef _BONEZ_CURVSKELVPLSAMPLER_HPP_
#define _BONEZ_CURVSKELVPLSAMPLER_HPP_

#include "VPLFilter.hpp"
#include "VPLSampler.hpp"
#include "bonez/common/sampling/Sampler.hpp"

#include "bonez/scene/topology/CurvSkelSensorNetwork.hpp"
#include "bonez/scene/topology/CurvSkelImportanceDistribution.hpp"

#include "bonez/scene/topology/GraphImportancePoints.hpp"

#include "GeorgievVPLFilter.hpp"

namespace BnZ {

class CurvSkelVPLSampler: public VPLSampler {
public:
    struct CurvSkelImportanceDistributionParams {
        float m_fEpsilon;
        size_t m_nViewRayCountX;
        size_t m_nViewRayCountY;
        size_t m_nScatteringRayCountX;
        size_t m_nScatteringRayCountY;
        std::string m_sSkelImportanceFilter;
    };

    CurvSkelVPLSampler(size_t minDepth, size_t maxDepth, size_t lightPathCount, 
        size_t vplCount, bool indirectOnly, const CurvSkelImportanceDistributionParams& skelImportanceParams,
        size_t sensorCountPerNode, const GeorgievVPLFilterPtr& vplFilter):
        m_nMinDepth(minDepth),
        m_nMaxDepth(maxDepth),
        m_nLightPathCount(lightPathCount),
        m_nVPLCount(vplCount),
        m_bIndirectOnly(indirectOnly),
        m_SkelImportanceDistributionParams(skelImportanceParams),
        m_nSensorCountPerNode(sensorCountPerNode),
        m_GeorgievVPLFilter(vplFilter),
        m_bIsPreprocessed(false)
    {
    }
    
    struct LightPathVertex {
        Intersection I;
        BRDF brdf;
        Vec3f wi;
        size_t depth;
        Col3f L;
        
        enum SamplingStrategy {
            SKELETON_STRATEGY, BRDF_STRATEGY
        };
        SamplingStrategy samplingStrategy; // Sampling strategy choosed to sample the vertex
    };
    
    static OrientedVPL getVPL(const LightPathVertex& vertex);
    
    bool accept(OrientedVPL& vpl, Sampler& sampler);
    
    bool extendLightPath(const Scene& scene, LightPathVertex& vertex, Sampler& sampler);
    
    virtual VPLContainer sample(const Camera& camera, const Scene& scene,
        const Framebuffer& framebuffer, size_t frameID, ParamSet& statistics);
    
    virtual std::string toString() const;
    
    size_t m_nMinDepth;
    size_t m_nMaxDepth;
    size_t m_nLightPathCount;
    size_t m_nVPLCount;
    bool m_bIndirectOnly;
    
    /*
    static const size_t NB_IMPORTANCE_POINT_SETS = 1;
    CurvilinearSkeletonImportancePoints m_SkelImportancePoints[NB_IMPORTANCE_POINT_SETS];
    */
    
    const Scene* m_pScene;
    
    GraphImportancePoints m_ImportancePoints;
    
    CurvSkelImportanceDistributionParams m_SkelImportanceDistributionParams;
    CurvSkelImportanceDistribution m_SkelImportanceDistribution;
    
    size_t m_nSensorCountPerNode;
    //CurvSkelSensorNetwork m_SkelSensorNetwork;
    
    GeorgievVPLFilterPtr m_GeorgievVPLFilter;
    
    bool m_bIsPreprocessed;
};

}

#endif
