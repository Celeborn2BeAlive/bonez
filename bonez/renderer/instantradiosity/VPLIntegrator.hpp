#ifndef _BONEZ_VPLINTEGRATOR_HPP_
#define _BONEZ_VPLINTEGRATOR_HPP_

#include "bonez/renderer/Integrator.hpp"
#include "bonez/scene/lights/vpls.hpp"

#include "VPLSampler.hpp"

namespace BnZ {

class VPLIntegrator: public Integrator {
public:
    VPLIntegrator(float geometryClamping, float rrTreshold, const VPLSamplerPtr& vplSampler,
                  bool bUseSkelShadowApprox):
        m_fGeometryClamping(geometryClamping), m_fRRTreshold(rrTreshold), m_pVPLSampler(vplSampler),
        m_bUseSkelShadowApprox(bUseSkelShadowApprox) {
    }
    
    virtual void preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
        size_t frameIdx, ParamSet& statistics);
    
    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const;
    
    virtual std::string toString() const;
    
    virtual std::string getType() const {
        return "VPLIntegrator";
    }
    
    float m_fGeometryClamping; // Clamping factor for the geometry term
    float m_fRRTreshold; // Russian roulette threshold for the evaluation of a VPL based on it's contribution
    
    VPLSamplerPtr m_pVPLSampler;
    VPLContainer m_VPLContainer;

    bool m_bUseSkelShadowApprox;
};

}

#endif
