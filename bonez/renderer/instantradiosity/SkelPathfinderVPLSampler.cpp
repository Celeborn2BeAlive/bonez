#include <sstream>
#include <iomanip>

#include "SkelPathfinderVPLSampler.hpp"

#include "bonez/common/Progress.hpp"

#include "bonez/common/sampling/Sampler.hpp"

namespace BnZ {

std::string SkelPathfinderVPLSampler::toString() const {
    std::stringstream ss;
    ss << "PathfinderVPLSampler_" << std::setfill('0') << "minDepth=" << std::setw(3) << m_nMinDepth << "_"
       << "maxDepth=" << std::setw(3) << m_nMaxDepth;
    
    if(m_nLightPathCount != size_t(embree::inf)) {
        ss << "_lightPathCount=" << std::setw(6) << m_nLightPathCount;
    }
    
    if(m_nVPLCount != size_t(embree::inf)) {
        ss << "_vplCount=" << std::setw(6) << m_nVPLCount;
    }
    
    if(m_bIndirectOnly) {
        ss << "_indirectOnly";
    }
    
    return ss.str();
}

bool SkelPathfinderVPLSampler::accept(OrientedVPL& vpl, Sampler& sampler) {
    if(m_GeorgievVPLFilter) {
        return m_GeorgievVPLFilter->accept(vpl, sampler);
    }
    
    return true;
}

OrientedVPL SkelPathfinderVPLSampler::getVPL(const LightPathVertex& vertex) {
    return OrientedVPL(vertex.I.P, vertex.I.Ns, vertex.L * vertex.brdf.diffuseTerm());
}

bool SkelPathfinderVPLSampler::extendLightPath(const Scene& scene, LightPathVertex& vertex, Sampler& sampler) {
    if(++vertex.depth > m_nMaxDepth) {
        return false;
    }
    
    Col3f reflectionTerm;
    Sample3f wo;
     
    float pskel = m_SkeletonPathfinder.pskel(vertex.I);

    if(sampler.get1DSample() < pskel) {
        wo = m_SkeletonPathfinder.sample(vertex.I, sampler.get2DSample());
        
        if(wo.pdf == 0.f) {
            return false;
        }
        
        float cos_o = embree::dot(vertex.I.Ns, wo.value);
        if(cos_o <= 0.f) {
            return false;
        }
        
        reflectionTerm = vertex.brdf.eval(vertex.wi, wo.value);
        
        if(reflectionTerm == Col3f(0.f)) {
            return false;
        }
        
        float pdf = pskel * wo.pdf + (1 - pskel) * vertex.brdf.pdf(vertex.wi, wo);
        reflectionTerm *= embree::rcp(pdf) * cos_o;
        
        vertex.samplingStrategy = LightPathVertex::SKELETON_STRATEGY;
        
    } else {
        reflectionTerm = vertex.brdf.sample(vertex.wi, wo, sampler.get2DSample());
        
        if(reflectionTerm == Col3f(0.f) || wo.pdf <= 0.f) {
            return false;
        }
        
        float cos_o = embree::dot(vertex.I.Ns, wo.value);
        if(cos_o < 0.f) {
            return false;
        }
        
        float pdf = wo.pdf;
        
        if(pskel) {
            pdf = (1 - pskel) * pdf + pskel * m_SkeletonPathfinder.pdf(vertex.I, wo.value);
        }
        
        reflectionTerm *= embree::rcp(pdf) * cos_o;
        
        vertex.samplingStrategy = LightPathVertex::BRDF_STRATEGY;
    }
    
    // If the depth is beyond minDepth, use russian roulette to eventually stop the path
    // if its contribution is too low
    if (vertex.depth > m_nMinDepth) {
        // Acceptance probability is the luminance of the reflection factor
        float rrProb = embree::min(1.f, embree::luminance(reflectionTerm));

        if (sampler.get1DSample() > rrProb) {
            return false;
        }

        // Next intersection accepted: update the reflection factor
        // to take into account the russian roulette
        reflectionTerm *= embree::rcp(rrProb);
    }

    // Compute the next intersection
    if(!(vertex.I = bounce(vertex.I, wo.value, scene))) {
        return false;
    }
    
    // Apply the reflection factor to the current power
    vertex.L *= reflectionTerm;
    // Incident direction for the next VPL
    vertex.wi = -wo.value;
    
    vertex.brdf = scene.shade(vertex.I);
    
    return true;
}

#if 0
void SkelPathfinderVPLSampler::displayRayData(const Ray& ray, const Scene& scene, GLRenderer& renderer) {
    if(!m_bIsPreprocessed) {
        return;
    }
    
    renderer.removeObject(m_nGLObjectGroup);
    
    Intersection I = intersect(ray, scene);
    if(!I) {
        return;
    }
    
    GLRenderGroupPtr group = std::make_shared<GLRenderGroup>();

/*
    CurvilinearSkeleton::NodeIndex nodeIdx = scene.getSkeleton().getNearestNode(I);
    if(nodeIdx == CurvilinearSkeleton::NO_NODE()) {
        return;
    }
    
    Vec3f importanceDirection = m_SkelImportancePoints[0][nodeIdx] - I.position;
        
    if(embree::dot(importanceDirection, I.Ns) <= 0.f) {
        return;
    }

    std::shared_ptr<GLLine> line = std::make_shared<GLLine>(renderer.getProgramBuilder()); 
    line->build(I.position, I.position + importanceDirection, Col3f(1, 0, 0), Col3f(0, 1, 0));
    
    group->addObject(line);
*/

    Sampler sampler(0);
    
    for(size_t i = 0; i < 128; ++i) {
        LightPathVertex vertex, previousVertex;
        vertex.I = I;
        vertex.brdf = scene.shade(I);
        vertex.wi = -ray.dir;
        vertex.depth = 0;
        vertex.L = embree::Col3f(1.f);
        
        previousVertex = vertex;
        
        while(extendLightPath(scene, vertex, sampler)) {
            // If no intersection, the path is over
            if(!vertex.I) {
                break;
            }
            
            std::shared_ptr<GLLine> line = std::make_shared<GLLine>(renderer.getProgramBuilder());
            
            Col3f color = Col3f(1, 0, 0);
            if(vertex.samplingStrategy == LightPathVertex::BRDF_STRATEGY) {
                color = Col3f(0, 1, 0);
            }
            
            line->build(previousVertex.I.P, vertex.I.P, color, color);
            
            group->addObject(line);
            
            // Create a new oriented diffuse VPL
            /*OrientedVPL vpl = getVPL(vertex);
            
            if(accept(vpl, sampler)) {
                ++vplCount;
                container.addVPL(vpl);
            } else {
                ++rejectedVPLCount;
            }*/

            previousVertex = vertex;
        }
    }
    

    renderer.setObject(group, m_nGLRayObjectGroup);
}
#endif

VPLContainer SkelPathfinderVPLSampler::sample(const Camera& camera, const Scene& scene,
    const Framebuffer& framebuffer, size_t frameID, ParamSet& statistics) {
    std::clog << "Sampling of the VPLs..." << std::endl;
    
    Random rng(frameID);

    m_pScene = &scene;
    
    // Build skel importance distribution
    /*
    ParamSet skelImpStats;
    computeCurvSkelImportanceDistribution(m_SkelImportanceDistribution, m_SkelImportanceDistributionParams.m_nViewRayCountX,
        m_SkelImportanceDistributionParams.m_nViewRayCountY, m_SkelImportanceDistributionParams.m_nScatteringRayCountX,
        m_SkelImportanceDistributionParams.m_nScatteringRayCountY, false, scene, camera, rng, &skelImpStats);
    m_SkelImportanceDistribution.filter(scene, m_SkelImportanceDistributionParams.m_sSkelImportanceFilter, &skelImpStats);
    
    statistics.set("skelImportance", skelImpStats);
    */
    
    if(!m_SkeletonPathfinder.preprocess(camera.getPosition(), scene)) {
        throw "error";
    }

    /*
    if(glRenderer) {
        GLRenderGroupPtr group = std::make_shared<GLRenderGroup>();
        
        for(GraphNodeIndex i = 0; i < scene.topology.clustering.size(); ++i) {
            GraphNodeIndex j = m_SkeletonPathfinder.getNearestVisibleMinima(i);
            if(NO_CLUSTER() != j) {
                std::shared_ptr<GLSphere> sphere = std::make_shared<GLSphere>(glRenderer->getProgramBuilder());
                sphere->build(scene.topology.clustering[i].center, 5.f, scene.topology.getClusterColor(j));
                group->addObject(sphere);
                
                std::shared_ptr<GLLine> line = std::make_shared<GLLine>(glRenderer->getProgramBuilder());
                line->build(scene.topology.clustering[i].center, scene.topology.clustering[j].center,
                    Col3f(1), Col3f(1));
                group->addObject(line);
            }
        }
        
        glRenderer->setObject(group, m_nGLObjectGroup);
    }
    */

    if(m_GeorgievVPLFilter) {
        m_GeorgievVPLFilter->init(camera, scene, framebuffer, frameID, statistics);
    }

    Progress progress;
    progress.init("VPL Sampling", m_nLightPathCount);

    double start = embree::getSeconds();

    VPLContainer container;
    Sampler sampler(frameID);
    
    size_t lightPathCount = 0;
    size_t vplCount = 0;
    size_t rejectedVPLCount = 0;

    // Sample enough lightpaths or VPLs according to the specified parameters
    while (lightPathCount < m_nLightPathCount && vplCount < m_nVPLCount) {
        //progress.set(lightPathCount);
        
        LightPathVertex currentVertex;
        
        EmitterVPL srcVPL = scene.lights.sampleVPL(scene, VPLRandom(sampler.get1DSample(), sampler.get1DSample(),
            sampler.get2DSample()));
        
        switch(srcVPL.type) {
        case EmitterVPL::DIRECTIONAL: {
            // If direct illumination is required, add a directional VPL
            if(!m_bIndirectOnly) {
                if(!m_GeorgievVPLFilter || m_GeorgievVPLFilter->accept(srcVPL.directional, sampler)) {
                    ++vplCount;
                    container.addVPL(srcVPL.directional);
                } else if(m_GeorgievVPLFilter) {
                    ++rejectedVPLCount;
                }
            }
            
            // Sample the position of the ray
            Sample3f P = scene.sampleRayOrigin(srcVPL.directional.D, sampler.get2DSample());
            
            // Next VPL's intersection
            currentVertex.I = intersect(Ray(P.value, srcVPL.directional.D), scene);
            // Incident direction for the next VPL
            currentVertex.wi = -srcVPL.directional.D;
            // Monte carlo estimation of the power carried by the ray
            currentVertex.L = srcVPL.directional.L * embree::rcp(P.pdf);
            
        }
        break;
        
        case EmitterVPL::ORIENTED: {
            // If direct illumination is required, try adding an oriented VPL
            if(!m_bIndirectOnly) {
                if(!m_GeorgievVPLFilter || m_GeorgievVPLFilter->accept(srcVPL.oriented, sampler)) {
                    ++vplCount;
                    container.addVPL(srcVPL.oriented);
                } else if(m_GeorgievVPLFilter) {
                    ++rejectedVPLCount;
                }
            }
            
            // Sample an exitant direction
            Vec2f dirSample = sampler.get2DSample();
            Sample3f wo = embree::cosineSampleHemisphere(dirSample.x, dirSample.y, srcVPL.oriented.N);

            // Monte-carlo estimation of the power carried by the ray
            currentVertex.L = srcVPL.oriented.L * embree::dot(srcVPL.oriented.N, wo.value) * embree::rcp(wo.pdf);
            // Next VPL's intersection
            currentVertex.I = intersect(light_ray(srcVPL.oriented.P, wo.value), scene);
            // Incident direction for the next VPL
            currentVertex.wi = -wo.value;
        }        
        break;
        }
        
        if(currentVertex.I) {
            // BRDF of the next VPL
            currentVertex.brdf = scene.shade(currentVertex.I);
            // Depth of the next VPL
            currentVertex.depth = 1;

            // Extend the path
            while (currentVertex.depth <= m_nMaxDepth && vplCount < m_nVPLCount) {
                // If no intersection, the path is over
                if(!currentVertex.I) {
                    break;
                }
                
                // Create a new oriented diffuse VPL
                OrientedVPL vpl = getVPL(currentVertex);
                
                if(accept(vpl, sampler)) {
                    ++vplCount;
                    container.addVPL(vpl);
                } else {
                    ++rejectedVPLCount;
                }

                if(vplCount == m_nVPLCount || !extendLightPath(scene, currentVertex, sampler)) {
                    break;
                }
            }
        }

        ++lightPathCount;
    }
    
    progress.end();
    
    // Apply the monte-carlo estimation by dividing all the estimated powers
    // by the number of light path that have been drawn:

    float rcpLightPathCount = embree::rcp(float(lightPathCount));

    Col3f vplTotalPower(0.f);
        
    for(OrientedVPL& vpl: container.orientedVPLs) {
        vpl.L *= rcpLightPathCount;
        vplTotalPower += vpl.L;
    }
    
    for(DirectionalVPL& vpl: container.directionalVPLs) {
        vpl.L *= rcpLightPathCount;
        vplTotalPower += vpl.L;
    }
    
    double time = embree::getSeconds() - start;
    
    std::clog << "Number of accepted VPLs: " << vplCount << std::endl;
    std::clog << "Number of rejected VPLs: " << rejectedVPLCount << std::endl;
    std::clog << "Acceptance ratio: " << 100.f * vplCount / (vplCount + rejectedVPLCount) << std::endl;
    
    statistics.set("time", time);
    statistics.set("directionalVPLCount", container.directionalVPLs.size());
    statistics.set("orientedVPLCount", container.orientedVPLs.size());
    statistics.set("VPLCount", container.size());
    statistics.set("rejectedVPLCount", rejectedVPLCount);
    statistics.set("acceptanceRatio", 100.f * vplCount / (vplCount + rejectedVPLCount));
    statistics.set("VPLTotalPower", vplTotalPower);
    statistics.set("VPLMeanPower", vplTotalPower / (float) container.size());

    std::clog << "Number of VPLs: " << container.size() << std::endl;

    /*
    if(glRenderer) {
        m_bIsPreprocessed = true;
    }*/
    
    return container;
}
    
}
