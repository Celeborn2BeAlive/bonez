#include "VPLVisibilityApproximationErrorIntegrator.hpp"

#include "bonez/scene/shading/BRDF.hpp"

namespace BnZ {

std::string VPLVisibilityApproximationErrorIntegrator::toString() const {
    std::stringstream ss;
    ss << "VPLVisibilityApproximationErrorIntegrator";
    return ss.str() + "_" + m_pVPLSampler->toString();
}

void VPLVisibilityApproximationErrorIntegrator::preprocess(const Camera& camera, const Scene& scene,
                                                           const Framebuffer& framebuffer,
    size_t frameIdx, ParamSet& statistics) {
    m_VPLContainer = m_pVPLSampler->sample(camera, scene, framebuffer, frameIdx, statistics);
}

Col3f VPLVisibilityApproximationErrorIntegrator::Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
    RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
    Intersection I = intersect(ray, scene, rayCounter);

    if (!I) {
        return Col3f(0.f);
    }

    GraphNodeIndex nodeI = scene.voxelSpace->getClustering().getNearestNode(I);
    if(nodeI == UNDEFINED_NODE) {
        return BAD_COLOR;
    }

    Vec3f wo = -ray.dir;
    float error = 0.f;

    BRDF brdf = scene.shade(I);

    size_t nbTry = 0;
    for(const OrientedVPL& vpl: m_VPLContainer.orientedVPLs) {
        Ray shadowRay;
        float geometryTerm = G(I, vpl, shadowRay);

        if(geometryTerm == 0.f) {
            continue;
        }

        Col3f reflectionTerm = brdf.eval(shadowRay.dir, wo);
        if(reflectionTerm == Col3f(embree::zero)) {
            continue;
        }

        Col3f vplContrib = vpl.L * geometryTerm;
/*
        float luminance = embree::luminance(vplContrib);
        if (luminance < m_fRRTreshold) {
            float rrProb = embree::min(1.f, luminance);

            if (sample.getRandomFloat() > rrProb) {
                continue;
            }

            vplContrib *= embree::rcp(rrProb);
        }
*/

        GraphNodeIndex nodeVPL = scene.voxelSpace->getClustering().getNearestNode(vpl.P, vpl.N);
        if(nodeVPL == UNDEFINED_NODE) {
            continue;
        }

        float VApprox = 0;

        switch(m_VisibilityMethod) {
        case CLUSTER_VISIBILITY:
            VApprox = scene.voxelSpace->getClustering().checkVisibility(nodeI, nodeVPL);
            break;
        case CLUSTER_VISIBILITY_FACTOR_THRESHOLD:
            VApprox = scene.voxelSpace->getClustering().getVisibilityFactor(nodeI, nodeVPL) > m_fThreshold;
            break;
        case MULTI_CLUSTER_VISIBILITY_FACTOR_THRESHOLD: {
            Skeleton::Node node = scene.voxelSpace->getClustering().getNode(nodeI);
            float weight = 1.f / embree::distance(node.P, I.P);
            float factor = weight * scene.voxelSpace->getClustering().getVisibilityFactor(nodeI, nodeVPL);
            float sumWeights = weight;
            for(auto neighbour: scene.voxelSpace->getClustering().neighbours(nodeI)) {
                node = scene.voxelSpace->getClustering().getNode(neighbour);
                weight = 1.f / embree::distance(node.P, I.P);
                factor += weight * scene.voxelSpace->getClustering().getVisibilityFactor(neighbour, nodeVPL);
                sumWeights += weight;
            }
            factor /= sumWeights;
            VApprox = factor > m_fThreshold;
        }
            break;
        case EXENTRICITY_ORIENTATION: {
            Skeleton::Node node = scene.voxelSpace->getClustering().getNode(nodeI);
            Vec3f wn = node.P - I.P;
            float d = embree::distance(node.P, I.P);
            wn /= d;
            Skeleton::Node bestNode = node;
            float maxdot = embree::dot(wn, shadowRay.dir);
            for(auto neighbour: scene.voxelSpace->getClustering().neighbours(nodeI)) {
                Skeleton::Node node = scene.voxelSpace->getClustering().getNode(neighbour);
                Vec3f wn = node.P - I.P;
                float d = embree::distance(node.P, I.P);
                wn /= d;
                float dot = embree::dot(wn, shadowRay.dir);
                if(dot > maxdot) {
                    bestNode = node;
                    maxdot = dot;
                }
            }

            d = embree::distance(bestNode.P, I.P);
            float e = bestNode.maxball / d; // exentricity

            error += embree::luminance(vplContrib * reflectionTerm) * e * maxdot;
            ++nbTry;
            continue;
        }
            break;
        }

        // Float visibility
        // vplContrib *= scene.voxelSpace->getClustering().getVisibilityFactor(node1, node2);

        // Russian roulette visibility

        /*
        float rrProb = embree::min(1.f, scene.voxelSpace->getClustering().getVisibilityFactor(node1, node2));

        if (sample.getRandomFloat() > rrProb) {
            continue;
        }

        if(rrProb < 1.f) {
            if(occluded(shadowRay, scene, rayCounter)) {
                continue;
            }
        }
        vplContrib *= embree::rcp(rrProb);
        */

        // With sensors
        /*if(!scene.voxelSpace->getClustering().checkVisibilityWithSensors(node1, vpl.P, vpl.N)
                || !scene.voxelSpace->getClustering().checkVisibilityWithSensors(node2, intersection.P, intersection.Ns)) {
            continue;
        }*/

        // Try another thing
        /*
        Skeleton::Node n1 = scene.voxelSpace->getClustering().getNode(node1);
        Skeleton::Node n2 = scene.voxelSpace->getClustering().getNode(node2);

        Ray r1 = shadow_ray(vpl.P, n1.P);
        Ray r2 = shadow_ray(intersection.P, n2.P);

        if(occluded(r1, scene, rayCounter) || occluded(r2, scene, rayCounter)) {
            continue;
        }
        */

        float V = occluded(shadowRay, scene, rayCounter);

        error += embree::luminance(vplContrib) * embree::abs(VApprox - V);
        ++nbTry;
    }

    if(!nbTry) {
        return Col3f(1.f);
    }

    return Col3f(error);
}

}
