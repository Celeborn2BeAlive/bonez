#ifndef _BONEZ_RANDOMVPLSAMPLER_HPP_
#define _BONEZ_RANDOMVPLSAMPLER_HPP_

#include "VPLFilter.hpp"
#include "VPLSampler.hpp"
#include "bonez/common/sampling/Sampler.hpp"

namespace BnZ {

class RandomVPLSampler: public VPLSampler {
public:
    RandomVPLSampler(size_t minDepth, size_t maxDepth, size_t lightPathCount, 
        size_t vplCount, bool indirectOnly, const VPLFilterPtr& filter):
        m_nMinDepth(minDepth),
        m_nMaxDepth(maxDepth),
        m_nLightPathCount(lightPathCount),
        m_nVPLCount(vplCount),
        m_bIndirectOnly(indirectOnly),
        m_pFilter(filter) {
    }
    
    struct LightPathVertex {
        Intersection I;
        BRDF brdf;
        Vec3f wi;
        size_t depth;
        Col3f L;
    };
    
    static OrientedVPL getVPL(const LightPathVertex& vertex);
    
    bool extendLightPath(const Scene& scene, LightPathVertex& vertex, Sampler& sampler);
    
    virtual VPLContainer sample(const Camera& camera, const Scene& scene,
        const Framebuffer& framebuffer, size_t frameID, ParamSet& statistics);
    
    virtual std::string toString() const;
    
    size_t m_nMinDepth;
    size_t m_nMaxDepth;
    size_t m_nLightPathCount;
    size_t m_nVPLCount;
    bool m_bIndirectOnly;
    VPLFilterPtr m_pFilter;
};

}

#endif
