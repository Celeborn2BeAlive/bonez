#ifndef _BONEZ_CURVSKELVPLFILTER_HPP_
#define _BONEZ_CURVSKELVPLFILTER_HPP_

#include "VPLFilter.hpp"

#include "bonez/scene/topology/CurvSkelImportanceDistribution.hpp"

namespace BnZ {
    
class CurvSkelVPLFilter: public VPLFilter {
public:
    CurvSkelVPLFilter(float epsilon, size_t viewRayCountX, size_t viewRayCountY,
        size_t scatteringRayCountX, size_t scatteringRayCountY, bool useClustering,
        const std::string& skelImportanceFilter, bool useFiltering, bool useResampling, size_t vplCount);
    
    /**
     * Initialize the filter for a given frame.
     */
    virtual void init(const Camera& camera, const Scene& scene,
        const Framebuffer& framebuffer, size_t frameID, ParamSet& stats);
    
    /**
     * Test if an OrientedVPL is accepted. If it is accepted, it's
     * intensity can be changed.
     * 
     * \return true if the VPL has been accepted.
     */
    virtual bool accept(OrientedVPL& vpl, Sampler& sampler);
    
    /**
     * Test if a DirectionalVPL is accepted. If it is accepted, it's
     * intensity can be changed.
     * 
     * \return true if the VPL has been accepted.
     */
    virtual bool accept(DirectionalVPL& vpl, Sampler& sampler);
    
    virtual VPLContainer resample(const VPLContainer& container, Sampler& sampler);
    
    virtual std::string toString() const {
        return "CurvSkelFilter";
    }
    
private:
    float m_fEpsilon;
    size_t m_nViewRayCountX;
    size_t m_nViewRayCountY;
    size_t m_nScatteringRayCountX;
    size_t m_nScatteringRayCountY;

    bool m_bUseClustering;
    
    std::string m_sSkelImportanceFilter;
    
    bool m_bUseFiltering;
    bool m_bUseResampling;
    size_t m_nVPLCount;
    
    CurvSkelImportanceDistribution m_SkelImportanceDistribution;
    
    const Scene* m_pScene;
};

}

#endif
