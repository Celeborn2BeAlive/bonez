#ifndef _BONEZ_VPLSAMPLER_HPP_
#define _BONEZ_VPLSAMPLER_HPP_

#include <memory>

#include "bonez/common/common.hpp"
#include "bonez/common/cameras/Camera.hpp"
#include "bonez/renderer/Framebuffer.hpp"
#include "bonez/scene/Scene.hpp"

#include "bonez/scene/lights/vpls.hpp"

namespace BnZ {

class VPLSampler {
public:
    virtual ~VPLSampler() {
    }

    virtual VPLContainer sample(const Camera& camera, const Scene& scene,
        const Framebuffer& framebuffer, size_t frameID, ParamSet& statistics) = 0;
    
    virtual std::string toString() const = 0;
};
    
typedef std::shared_ptr<VPLSampler> VPLSamplerPtr;

}

#endif
