#ifndef _BONEZ_VPLVISIBILITYAPPROXIMATIONERRORINTEGRATOR_HPP_
#define _BONEZ_VPLVISIBILITYAPPROXIMATIONERRORINTEGRATOR_HPP_

#include "bonez/renderer/Integrator.hpp"
#include "bonez/scene/lights/vpls.hpp"

#include "VPLSampler.hpp"

namespace BnZ {

class VPLVisibilityApproximationErrorIntegrator: public Integrator {
public:
    VPLVisibilityApproximationErrorIntegrator(const std::string& visibilityApproxMethod,
                                              float threshold,
                                              const VPLSamplerPtr& vplSampler):
        m_pVPLSampler(vplSampler), m_fThreshold(threshold) {
        if(visibilityApproxMethod == "clusterVisibility") {
            m_VisibilityMethod = CLUSTER_VISIBILITY;
        } else if(visibilityApproxMethod == "clusterVisibilityFactorThreshold") {
            m_VisibilityMethod = CLUSTER_VISIBILITY_FACTOR_THRESHOLD;
        } else if(visibilityApproxMethod == "multiClusterVisibilityFactorThreshold") {
            m_VisibilityMethod = MULTI_CLUSTER_VISIBILITY_FACTOR_THRESHOLD;
        } else if(visibilityApproxMethod == "exentricityOrientation") {
            m_VisibilityMethod = EXENTRICITY_ORIENTATION;
        } else {
            std::cerr << "Unkown visibility approximation method " << visibilityApproxMethod << std::endl;
        }
    }

    virtual void preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
        size_t frameIdx, ParamSet& statistics);

    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const;

    virtual std::string toString() const;

    virtual std::string getType() const {
        return "VPLVisibilityApproximationErrorIntegrator";
    }

    VPLSamplerPtr m_pVPLSampler;
    VPLContainer m_VPLContainer;

    enum VisibilityApproximationMethod {
        CLUSTER_VISIBILITY,
        CLUSTER_VISIBILITY_FACTOR_THRESHOLD,
        MULTI_CLUSTER_VISIBILITY_FACTOR_THRESHOLD,
        EXENTRICITY_ORIENTATION
    };

    VisibilityApproximationMethod m_VisibilityMethod;
    float m_fThreshold;
};

}

#endif
