#include "FromPointVPLSampler.hpp"

#include "bonez/common/Progress.hpp"
#include "bonez/common/sampling/Sampler.hpp"

namespace BnZ {

struct LightPathVertex {
    Intersection I;
    BRDF brdf;
    Vec3f wi;
    size_t depth;
    Col3f L;
};

static OrientedVPL getVPL(const LightPathVertex& vertex) {
    return OrientedVPL(vertex.I.P, vertex.I.Ns, vertex.L * vertex.brdf.diffuseTerm());
}

static bool extendLightPath(const Scene& scene, LightPathVertex& vertex, Sampler& sampler,
                            size_t minDepth, size_t maxDepth) {
    if(++vertex.depth > maxDepth) {
        return false;
    }

    // Sample a new exitant direction
    Sample3f wo;

    Col3f reflectionTerm = vertex.brdf.sample(vertex.wi, wo, sampler.get2DSample());

    if(reflectionTerm == Col3f(0.f) || wo.pdf <= 0.f) {
        return false;
    }

    float cos_o = embree::dot(vertex.I.Ns, wo.value);

    // Monte-Carlo estimation of the reflection factor
    reflectionTerm *= embree::rcp(wo.pdf) * cos_o;

    // If the depth is beyond minDepth, use russian roulette to eventually stop the path
    // if its contribution is too low
    if (vertex.depth > minDepth) {
        // Acceptance probability is the luminance of the reflection factor
        float rrProb = embree::min(1.f, embree::luminance(reflectionTerm));

        if (sampler.get1DSample() > rrProb) {
            return false;
        }

        // Next intersection accepted: update the reflection factor
        // to take into account the russian roulette
        reflectionTerm *= embree::rcp(rrProb);
    }

    // Compute the next intersection
    if(!(vertex.I = bounce(vertex.I, wo.value, scene))) {
        return false;
    }

    // Apply the reflection factor to the current power
    vertex.L *= reflectionTerm;
    // Incident direction for the next VPL
    vertex.wi = -wo.value;

    vertex.brdf = scene.shade(vertex.I);

    return true;
}

VPLContainer FromPointVPLSampler::sample(const Camera &camera, const Scene &scene,
                                         const Framebuffer &framebuffer,
                                         size_t frameID,
                                         ParamSet &statistics) {
    std::clog << "Sampling of the VPLs..." << std::endl;

    Progress progress;
    progress.init("From point VPL Sampling", 1);

    double start = embree::getSeconds();

    VPLContainer container;
    Sampler sampler(frameID);

    size_t lightPathCount = 0;
    size_t vplCount = 0;
    size_t rejectedVPLCount = 0;

    // Sample enough lightpaths or VPLs according to the specified parameters
    while (lightPathCount < m_nLightPathCount && vplCount < m_nVPLCount) {
        //progress.set(lightPathCount);

        LightPathVertex currentVertex;

        // Sample an exitant direction
        Vec2f dirSample = sampler.get2DSample();
        Sample3f wo = embree::uniformSampleSphere(dirSample.x, dirSample.y);

        // Monte-carlo estimation of the power carried by the ray
        currentVertex.L = m_I * embree::rcp(wo.pdf);
        // Next VPL's intersection
        currentVertex.I = intersect(light_ray(m_P, wo.value), scene);
        // Incident direction for the next VPL
        currentVertex.wi = -wo.value;

        if(currentVertex.I) {
            // BRDF of the next VPL
            currentVertex.brdf = scene.shade(currentVertex.I);
            // Depth of the next VPL
            currentVertex.depth = 1;

            // Extend the path
            while (currentVertex.depth <= m_nMaxDepth && vplCount < m_nVPLCount) {
                // If no intersection, the path is over
                if(!currentVertex.I) {
                    break;
                }

                // Create a new oriented diffuse VPL
                OrientedVPL vpl = getVPL(currentVertex);

                ++vplCount;
                container.addVPL(vpl);

                if(vplCount == m_nVPLCount ||
                        !extendLightPath(scene, currentVertex, sampler, m_nMinDepth, m_nMaxDepth)) {
                    break;
                }
            }
        }

        ++lightPathCount;
    }

    progress.end();

    // Apply the monte-carlo estimation by dividing all the estimated powers
    // by the number of light path that have been drawn:

    float rcpLightPathCount = embree::rcp(float(lightPathCount));

    Col3f vplTotalPower(0.f);

    for(OrientedVPL& vpl: container.orientedVPLs) {
        vpl.L *= rcpLightPathCount;
        vplTotalPower += vpl.L;
    }

    double time = embree::getSeconds() - start;

    std::clog << "Number of accepted VPLs: " << vplCount << std::endl;
    std::clog << "Number of rejected VPLs: " << rejectedVPLCount << std::endl;
    std::clog << "Acceptance ratio: " << 100.f * vplCount / (vplCount + rejectedVPLCount) << std::endl;

    statistics.set("time", time);
    statistics.set("directionalVPLCount", container.directionalVPLs.size());
    statistics.set("orientedVPLCount", container.orientedVPLs.size());
    statistics.set("VPLCount", container.size());
    statistics.set("rejectedVPLCount", rejectedVPLCount);
    statistics.set("acceptanceRatio", 100.f * vplCount / (vplCount + rejectedVPLCount));
    statistics.set("VPLTotalPower", vplTotalPower);
    statistics.set("VPLMeanPower", vplTotalPower / (float) container.size());

    std::clog << "Number of VPLs: " << container.size() << std::endl;

    return container;
}

}
