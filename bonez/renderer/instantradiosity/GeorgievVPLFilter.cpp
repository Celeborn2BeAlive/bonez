#include "GeorgievVPLFilter.hpp"

#include "bonez/common/Progress.hpp"
#include "bonez/renderer/pathtracing/PathtraceIntegrator.hpp"
#include "bonez/common/sampling/patterns.hpp"

namespace BnZ {

/**
 * Constructor.
 * \param epsilon An epsilon added to the acceptance probability to avoid bias.
 * \param viewRayCountX The number of view rays used in X direction to estimate VPL contribution.
 * \param viewRayCountY The number of view rays used in Y direction to estimate VPL constribution.
 * \param incidentPowerEstimate An estimation of the incident power affecting the camera.
 * 
 * \remarks This version uses a provided incident power estimate.
 */
GeorgievVPLFilter::GeorgievVPLFilter(float epsilon, size_t viewRayCountX, size_t viewRayCountY,
    Col3f incidentPowerEstimate, bool useFiltering, bool useResampling, size_t vplCount):
    m_fEpsilon(epsilon),
    m_nViewRayCountX(viewRayCountX),
    m_nViewRayCountY(viewRayCountY),
    m_bIncidentPowerEstimateProvided(true),
    m_IncidentPowerEstimate(incidentPowerEstimate),
    m_pIntegratorSampler(nullptr),
    m_pIntegrator(nullptr),
    m_bUseFiltering(useFiltering),
    m_bUseResampling(useResampling),
    m_nVPLCount(vplCount),
    m_pCamera(nullptr),
    m_pScene(nullptr) {
}
    
/**
 * Constructor.
 * \param epsilon An epsilon added to the acceptance probability to avoid bias.
 * \param viewRayCountX The number of view rays used in X direction to estimate VPL contribution.
 * \param viewRayCountY The number of view rays used in Y direction to estimate VPL constribution.
 * on the camera.
 * \param sampler A sampler for the integrator.
 * \param integrator The Integrator that will be used to estimate the incident power on the camera.
 * 
 * \remarks This version compute an estimation of the incident power with a provided integrator
 */
GeorgievVPLFilter::GeorgievVPLFilter(float epsilon, size_t viewRayCountX, size_t viewRayCountY,
    const IntegratorSamplerPtr& sampler, const IntegratorPtr& integrator, 
    bool useFiltering, bool useResampling, size_t vplCount):
    m_fEpsilon(epsilon),
    m_nViewRayCountX(viewRayCountX),
    m_nViewRayCountY(viewRayCountY),
    m_bIncidentPowerEstimateProvided(false),
    m_IncidentPowerEstimate(0.f),
    m_pIntegratorSampler(sampler),
    m_pIntegrator(integrator),
    m_bUseFiltering(useFiltering),
    m_bUseResampling(useResampling),
    m_nVPLCount(vplCount),
    m_pCamera(nullptr),
    m_pScene(nullptr) {
}

/**
 * Initialize the filter for a given frame.
 */
void GeorgievVPLFilter::init(const Camera& camera, const Scene& scene,
    const Framebuffer& framebuffer, size_t frameID, ParamSet& stats) {
    if(!m_bIncidentPowerEstimateProvided) {
        computeIncidentPowerEstimate(camera, scene, frameID);
    }
    
    m_fRcpDesiredVPLPowerEstimation = embree::rcp(
            embree::luminance(m_IncidentPowerEstimate)
        );
    
    computeIntersections(camera, scene, frameID);
    
    m_pCamera = &camera;
    m_pScene = &scene;
    
    stats.set("incidentPowerEstimate", m_IncidentPowerEstimate);
    stats.set("rcpDesiredVPLPower", m_fRcpDesiredVPLPowerEstimation);
}

template<typename VPL>
float GeorgievVPLFilter::computeTransmittedPower(VPL& vpl) {
    Col3f P(0.f);

    RayCounter rayCounter;

    for(size_t i = 0; i < m_nIntersectionCount; ++i) {
        if(!m_Intersections[i]) {
            continue;
        }
        
        Vec3f wo = embree::normalize(m_pCamera->getPosition() - m_Intersections[i].P);
        
        Ray shadowRay;
        float geometryTerm = G(m_Intersections[i], vpl, shadowRay);
        
        if(geometryTerm == 0.f) {
            continue;
        }
        
        if(occluded(shadowRay, *m_pScene, rayCounter)) {
            continue;
        }
        
        P += vpl.L * geometryTerm * m_BRDFs[i].eval(shadowRay.dir, wo);
    }

    return embree::luminance(P * m_pCamera->area() / m_nIntersectionCount);
}

/**
 * Test if a VPL is accepted. If it is accepted, it's
 * intensity can be changed.
 * 
 * \return true if the VPL has been accepted.
 */
template<typename VPL>
bool GeorgievVPLFilter::genericAccept(VPL& vpl, Sampler& sampler) {
    if(!m_bUseFiltering) {
        return true;
    }

    float rrProb = embree::min(
        m_fEpsilon + computeTransmittedPower(vpl) * m_fRcpDesiredVPLPowerEstimation, 1.f
    );

    if (sampler.get1DSample() < rrProb) {
        vpl.L *= embree::rcp(rrProb);
        return true;
    }

    return false;
}
    
/**
 * Test if an OrientedVPL is accepted. If it is accepted, it's
 * intensity can be changed.
 * 
 * \return true if the VPL has been accepted.
 */
bool GeorgievVPLFilter::accept(OrientedVPL& vpl, Sampler& sampler) {
    return genericAccept(vpl, sampler);
}
    
/**
 * Test if a DirectionalVPL is accepted. If it is accepted, it's
 * intensity can be changed.
 * 
 * \return true if the VPL has been accepted.
 */
bool GeorgievVPLFilter::accept(DirectionalVPL& vpl, Sampler& sampler) {
    return genericAccept(vpl, sampler);
}

VPLContainer GeorgievVPLFilter::resample(const VPLContainer& container, Sampler& sampler) {
    if(!m_bUseResampling) {
        return container;
    }
    
    DiscreteDistribution distribution;
    
    for(size_t i = 0; i < container.orientedVPLs.size(); ++i) {
        float power = computeTransmittedPower(container.orientedVPLs[i]);
        distribution.add(m_fEpsilon + power);
    }
    
    for(size_t i = 0; i < container.directionalVPLs.size(); ++i) {
        distribution.add(m_fEpsilon + computeTransmittedPower(container.directionalVPLs[i]));
    }
    
    VPLContainer newContainer;
    
    for(size_t i = 0; i < m_nVPLCount; ++i) {
        float f = sampler.get1DSample();
        Sample1ui s = distribution.sample(f);
        
        if(s.value < container.orientedVPLs.size()) {
            OrientedVPL vpl = container.orientedVPLs[s.value];
            vpl.L /= (s.pdf * m_nVPLCount);
            newContainer.addVPL(vpl);
        } else {
            DirectionalVPL vpl = container.directionalVPLs[container.orientedVPLs.size() - s.value];
            vpl.L /= (s.pdf * m_nVPLCount);
            newContainer.addVPL(vpl);
        }
    }
    
    return newContainer;
}

/**
 * Compute an estimation of the incident power on the camera.
 */
void GeorgievVPLFilter::computeIncidentPowerEstimate(const Camera& camera, const Scene& scene,
    size_t frameID) {
    std::clog << "GeorgievVPLFilter: Compute incident power estimate" << std::endl;
    // Incident power
    Col3f P(embree::zero);
    
    m_pIntegrator->requestSamples(*m_pIntegratorSampler);
    size_t sampleCount = m_pIntegratorSampler->init(frameID, 0, 0, 1, 1);

    Progress progress;
    progress.init("Incident power estimatation", sampleCount);
    
    RayCounter rayCounter;
    
    IntegratorSample sample;
    Integrator::StoragePtr storage = m_pIntegrator->getStorage();
    while(m_pIntegratorSampler->getNextSample(sample)) {
        P += m_pIntegrator->Li(camera.ray(sample), scene, sample, rayCounter, storage);
        //progress.next();
    }

    progress.end();
    
    m_IncidentPowerEstimate = camera.area() * P / (float) sampleCount;
    
    std::clog << "GeorgievVPLFilter: Result: " << m_IncidentPowerEstimate << std::endl;
}

/**
 * Compute the array of intersections that will be used to estimated
 * VPL power transmited to the camera.
 */
void GeorgievVPLFilter::computeIntersections(const Camera& camera, const Scene& scene,
    size_t frameID) {
    m_nIntersectionCount = m_nViewRayCountX * m_nViewRayCountY;
    
    m_Intersections.reset(new Intersection[m_nIntersectionCount]);
    m_BRDFs.reset(new BRDF[m_nIntersectionCount]);
    
    Random rng(frameID);
    std::unique_ptr<Vec2f[]> viewSamples(new Vec2f[m_nIntersectionCount]);
    createJittered2DPattern(viewSamples.get(), m_nViewRayCountX, 
        m_nViewRayCountY, rng);
    
    for(size_t i = 0; i < m_nIntersectionCount; ++i) {
        m_Intersections[i] = intersect(camera.ray(viewSamples[i]), scene);
        if(m_Intersections[i]) {
            m_BRDFs[i] = scene.shade(m_Intersections[i]);
        }
    }
}

}
