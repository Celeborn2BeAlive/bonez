#ifndef _BONEZ_VPLFILTER_HPP_
#define _BONEZ_VPLFILTER_HPP_

#include <memory>

#include "bonez/common/cameras/Camera.hpp"
#include "bonez/renderer/Framebuffer.hpp"
#include "bonez/scene/Scene.hpp"
#include "bonez/scene/lights/vpls.hpp"
#include "bonez/common/sampling/Sampler.hpp"

namespace BnZ {

/**
 * A VPLFilter accept or reject VPLs according to a given criterion.
 */
class VPLFilter {
public:
    /**
     * Initialize the filter for a given frame.
     */
    virtual void init(const Camera& camera, const Scene& scene,
        const Framebuffer& framebuffer, size_t frameID, ParamSet& stats) = 0;
    
    /**
     * Test if an OrientedVPL is accepted. If it is accepted, it's
     * intensity can be changed.
     * 
     * \return true if the VPL has been accepted.
     */
    virtual bool accept(OrientedVPL& vpl, Sampler& sampler) = 0;
    
    /**
     * Test if a DirectionalVPL is accepted. If it is accepted, it's
     * intensity can be changed.
     * 
     * \return true if the VPL has been accepted.
     */
    virtual bool accept(DirectionalVPL& vpl, Sampler& sampler) = 0;
    
    virtual VPLContainer resample(const VPLContainer& container, Sampler& sampler) = 0;
    
    virtual std::string toString() const = 0;
};

typedef std::shared_ptr<VPLFilter> VPLFilterPtr;

}

#endif
