#include "CurvSkelVPLFilter.hpp"

namespace BnZ {

CurvSkelVPLFilter::CurvSkelVPLFilter(float epsilon, size_t viewRayCountX, size_t viewRayCountY,
    size_t scatteringRayCountX, size_t scatteringRayCountY, bool useClustering,
    const std::string& skelImportanceFilter, bool useFiltering, bool useResampling, size_t vplCount):
    m_fEpsilon(epsilon),
    m_nViewRayCountX(viewRayCountX),
    m_nViewRayCountY(viewRayCountY),
    m_nScatteringRayCountX(scatteringRayCountX),
    m_nScatteringRayCountY(scatteringRayCountY),
    m_bUseClustering(useClustering),
    m_sSkelImportanceFilter(skelImportanceFilter),
    m_bUseFiltering(useFiltering),
    m_bUseResampling(useResampling),
    m_nVPLCount(vplCount),
    m_pScene(nullptr) {
}
    
/**
 * Initialize the filter for a given frame.
 */
void CurvSkelVPLFilter::init(const Camera& camera, const Scene& scene,
    const Framebuffer& framebuffer, size_t frameID, ParamSet& stats) {
    std::clog << "CurvSkelVPLFilter: Compute importance distribution" << std::endl;
    Random rng(frameID);
    computeCurvSkelImportanceDistribution(m_SkelImportanceDistribution,
        m_nViewRayCountX, m_nViewRayCountY, m_nScatteringRayCountX, m_nScatteringRayCountY, m_bUseClustering,
        scene, camera, rng, &stats);
    m_SkelImportanceDistribution.filter(scene, m_sSkelImportanceFilter, nullptr);
    
    m_pScene = &scene;
    
    std::clog << "CurvSkelVPLFilter: Done" << std::endl;
}
    
/**
 * Test if an OrientedVPL is accepted. If it is accepted, it's
 * intensity can be changed.
 * 
 * \return true if the VPL has been accepted.
 */
bool CurvSkelVPLFilter::accept(OrientedVPL& vpl, Sampler& sampler) {
    // If there is no skeleton importance density, the VPL is accepted
    if(!m_bUseFiltering || m_SkelImportanceDistribution.empty()) {
        return true;
    }
    
    // Use the skeleton to estimate the importance of the VPL and reject/accept it based on this importance
    float skelRRProb = 0.f;
    GraphNodeIndex node = m_pScene->voxelSpace->getSkeleton().getNearestNode(vpl.P, vpl.N);

    if (node != UNDEFINED_NODE && 0.f != m_SkelImportanceDistribution.getMaxDensity()) {
        // Formula 1
        // skelRRProb = embree::min(1.f, m_ImportanceEstimationParams.acceptanceEpsilon + skelImportanceDistribution.getDensity(node) * embree::luminance(vpl.L));

        // Formula 2
        skelRRProb = embree::min(1.f, m_fEpsilon + m_SkelImportanceDistribution.getDensity(node) * embree::rcp(m_SkelImportanceDistribution.getMaxDensity()));
    
        // Formula 3
        // skelRRProb = embree::min(1.f, m_ImportanceEstimationParams.acceptanceEpsilon + skelImportanceDistribution.getDensity(node) * embree::rcp(skelImportanceDistribution.getMeanDensity()));
        
        // Formula 4
        // skelRRProb = embree::min(1.f, m_ImportanceEstimationParams.acceptanceEpsilon + skelImportanceDistribution.getDensity(node) * embree::rcp(skelImportanceDistribution.getTotalDensity()));
    
    } else {
        skelRRProb = embree::min(1.f, m_fEpsilon);
    }
    
    if (sampler.get1DSample() < skelRRProb) {
        if (skelRRProb > 0) {
            vpl.L *= embree::rcp(skelRRProb);
        }
        return true;
    }
    return false;
}
    
/**
 * Test if a DirectionalVPL is accepted. If it is accepted, it's
 * intensity can be changed.
 * 
 * \return true if the VPL has been accepted.
 */
bool CurvSkelVPLFilter::accept(DirectionalVPL& vpl, Sampler& sampler) {
    return true;
}

VPLContainer CurvSkelVPLFilter::resample(const VPLContainer& container, Sampler& sampler) {
    if(!m_bUseResampling) {
        return container;
    }
    
    DiscreteDistribution distribution;
    
    for(size_t i = 0; i < container.orientedVPLs.size(); ++i) {
        GraphNodeIndex node = m_pScene->voxelSpace->getSkeleton().
            getNearestNode(container.orientedVPLs[i].P, container.orientedVPLs[i].N);

        float value = m_fEpsilon;
        if (!node) {
            value += m_SkelImportanceDistribution.getDensity(node);
        }
        distribution.add(value);
    }
    
    VPLContainer newContainer;
    newContainer.directionalVPLs = container.directionalVPLs;
    
    for(size_t i = 0; i < m_nVPLCount; ++i) {
        Sample1ui s = distribution.sample(sampler.get1DSample());
        OrientedVPL vpl = container.orientedVPLs[s.value];
        vpl.L /= (s.pdf * m_nVPLCount);
        newContainer.addVPL(vpl);
    }
    
    return newContainer;
}

}
