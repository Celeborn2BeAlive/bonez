#include "factories.hpp"

#include "IntegratorRenderer.hpp"

#include "integrators/DepthIntegrator.hpp"
#include "integrators/NormalIntegrator.hpp"
#include "integrators/ImportanceIntegrator.hpp"
#include "integrators/SkelClusteringIntegrator.hpp"
#include "integrators/SkelNodeIntegrator.hpp"
#include "integrators/SkelImportanceDistributionIntegrator.hpp"
#include "integrators/SkelNodeCoherenceIntegrator.hpp"
#include "integrators/VisibilityClusteringIntegrator.hpp"
#include "integrators/GateLightingTestIntegrator.hpp"

#include "pathtracing/PathtraceIntegrator.hpp"
#include "pathtracing/SkelPathtraceIntegrator.hpp"
#include "pathtracing/SkelMISPathtraceIntegrator.hpp"
#include "pathtracing/BidirPathtraceIntegrator.hpp"
#include "pathtracing/SkelPTShadowApproxIntegrator.hpp"

#include "instantradiosity/VPLIntegrator.hpp"
#include "instantradiosity/VPLSampler.hpp"
#include "instantradiosity/RandomVPLSampler.hpp"
#include "instantradiosity/CurvSkelVPLSampler.hpp"
#include "instantradiosity/SkelPathfinderVPLSampler.hpp"
#include "instantradiosity/GeorgievVPLFilter.hpp"
#include "instantradiosity/CurvSkelVPLFilter.hpp"
#include "instantradiosity/VPLVisibilityApproximationErrorIntegrator.hpp"

#include "lightcuts/LightcutsIntegrator.hpp"

#include "bonez/common/sampling/StratifiedSampler.hpp"

#include "GenericFactory.hpp"
#include "PostProcess.hpp"
#include "postprocessing/GammaToneMapping.hpp"
#include "postprocessing/ScalingPostProcess.hpp"
#include "postprocessing/RescalingPostProcess.hpp"

namespace BnZ {

typedef GenericFactory<PostProcess, GammaToneMapping, ScalingPostProcess, RescalingPostProcess> PostProcessFactory;

PostProcessPtr createPostProcess(const ParamSet& params) {
    return PostProcessFactory::create(params);
}

IntegratorSamplerPtr createIntegratorSampler(const ParamSet& params);

IntegratorPtr createIntegrator(const ParamSet& params);

VPLFilterPtr createVPLFilter(const ParamSet& params) {
    std::string type = params.get<std::string>("type");
    
    if(type == "GeorgievVPLFilter") {
        if(params.hasParam("incidentPowerEstimate")) {
            return std::make_shared<GeorgievVPLFilter>(
                params.get<float>("epsilon"),
                params.get<size_t>("viewRayCountX"),
                params.get<size_t>("viewRayCountY"),
                params.get<Col3f>("incidentPowerEstimate"),
                params.get<bool>("useFiltering"),
                params.get<bool>("useResampling"),
                params.get<size_t>("vplCount", 0)
            );
        } else {
            return std::make_shared<GeorgievVPLFilter>(
                params.get<float>("epsilon"),
                params.get<size_t>("viewRayCountX"),
                params.get<size_t>("viewRayCountY"),
                createIntegratorSampler(params.get<ParamSet>("integratorSampler")),
                createIntegrator(params.get<ParamSet>("integrator")),
                params.get<bool>("useFiltering"),
                params.get<bool>("useResampling"),
                params.get<size_t>("vplCount", 0)
            );
        }
    }
    
    if(type == "CurvSkelVPLFilter") {
        return std::make_shared<CurvSkelVPLFilter>(
            params.get<float>("epsilon"),
            params.get<size_t>("viewRayCountX"),
            params.get<size_t>("viewRayCountY"),
            params.get<size_t>("scatteringRayCountX"),
            params.get<size_t>("scatteringRayCountY"),
            params.get<bool>("useClustering"),
            params.get<std::string>("importanceFilter"),
            params.get<bool>("useFiltering"),
            params.get<bool>("useResampling"),
            params.get<size_t>("vplCount", 0)
        );
    }
    
    std::cerr << "Unkown VPL filter " << type << std::endl;
    return nullptr;
}

VPLSamplerPtr createVPLSampler(const ParamSet& params) {
    std::string type = params.get<std::string>("type");
    
    if(type == "RandomVPLSampler") {
        VPLFilterPtr vplFilter;
        if(params.hasParam("vplFilter")) {
            vplFilter = createVPLFilter(params.get<ParamSet>("vplFilter"));
        }
        
        size_t lightPathCount = size_t(embree::inf);
        size_t vplCount = size_t(embree::inf);
        
        if(params.hasParam("lightPathCount")) {
            lightPathCount = params.get<size_t>("lightPathCount");
        }
        
        if(params.hasParam("vplCount")) {
            vplCount = params.get<size_t>("vplCount");
        }
        
        return std::make_shared<RandomVPLSampler>(params.get<size_t>("minDepth"),
            params.get<size_t>("maxDepth"), lightPathCount, vplCount, params.get<bool>("indirectOnly"), vplFilter);
    }
    
    if(type == "CurvSkelVPLSampler") {
        ParamSet skelImpParams = params.get<ParamSet>("skelImportance");
        
        CurvSkelVPLSampler::CurvSkelImportanceDistributionParams skelImportanceParams;
        
        skelImportanceParams.m_fEpsilon = skelImpParams.get<float>("epsilon");
        skelImportanceParams.m_nViewRayCountX = skelImpParams.get<size_t>("viewRayCountX");
        skelImportanceParams.m_nViewRayCountY = skelImpParams.get<size_t>("viewRayCountY");
        skelImportanceParams.m_nScatteringRayCountX = skelImpParams.get<size_t>("scatteringRayCountX");
        skelImportanceParams.m_nScatteringRayCountY = skelImpParams.get<size_t>("scatteringRayCountY");
        skelImportanceParams.m_sSkelImportanceFilter = skelImpParams.get<std::string>("importanceFilter");
        
        
        size_t lightPathCount = size_t(embree::inf);
        size_t vplCount = size_t(embree::inf);
        
        if(params.hasParam("lightPathCount")) {
            lightPathCount = params.get<size_t>("lightPathCount");
        }
        
        if(params.hasParam("vplCount")) {
            vplCount = params.get<size_t>("vplCount");
        }
        
        GeorgievVPLFilterPtr vplFilter;
        if(params.hasParam("georgiev")) {
            ParamSet georgievParams = params.get<ParamSet>("georgiev");
            
            vplFilter = std::make_shared<GeorgievVPLFilter>(
                georgievParams.get<float>("epsilon"),
                georgievParams.get<size_t>("viewRayCountX"),
                georgievParams.get<size_t>("viewRayCountY"),
                createIntegratorSampler(georgievParams.get<ParamSet>("integratorSampler")),
                createIntegrator(georgievParams.get<ParamSet>("integrator")),
                georgievParams.get<bool>("useFiltering"),
                georgievParams.get<bool>("useResampling"),
                georgievParams.get<size_t>("vplCount", 0)
            );
        }
        
        return std::make_shared<CurvSkelVPLSampler>(
            params.get<size_t>("minDepth"),
            params.get<size_t>("maxDepth"), 
            lightPathCount, 
            vplCount, 
            params.get<bool>("indirectOnly"),
            skelImportanceParams,
            params.get<size_t>("sensorCountPerNode"),
            vplFilter);
    }
    
    if(type == "SkelPathfinderVPLSampler") {
        size_t lightPathCount = size_t(embree::inf);
        size_t vplCount = size_t(embree::inf);
        
        if(params.hasParam("lightPathCount")) {
            lightPathCount = params.get<size_t>("lightPathCount");
        }
        
        if(params.hasParam("vplCount")) {
            vplCount = params.get<size_t>("vplCount");
        }
        
        GeorgievVPLFilterPtr vplFilter;
        if(params.hasParam("georgiev")) {
            ParamSet georgievParams = params.get<ParamSet>("georgiev");
            
            vplFilter = std::make_shared<GeorgievVPLFilter>(
                georgievParams.get<float>("epsilon"),
                georgievParams.get<size_t>("viewRayCountX"),
                georgievParams.get<size_t>("viewRayCountY"),
                createIntegratorSampler(georgievParams.get<ParamSet>("integratorSampler")),
                createIntegrator(georgievParams.get<ParamSet>("integrator")),
                georgievParams.get<bool>("useFiltering"),
                georgievParams.get<bool>("useResampling"),
                georgievParams.get<size_t>("vplCount", 0)
            );
        }
        
        return std::make_shared<SkelPathfinderVPLSampler>(
            params.get<size_t>("minDepth"),
            params.get<size_t>("maxDepth"), 
            lightPathCount, 
            vplCount, 
            params.get<float>("pskelThreshold"),
            params.get<bool>("indirectOnly"),
            vplFilter);
    }
    
    std::cerr << "Unkown VPL sampler " << type << std::endl;
    return nullptr;
}

IntegratorPtr createIntegrator(const ParamSet& params) {
    std::string type = params.get<std::string>("type");
    if(type == "PathtraceIntegrator") {
        return std::make_shared<PathtraceIntegrator>(params.get<int>("maxDepth"), params.get<bool>("indirectOnly"));
    }

    if(type == "SkelPTShadowApproxIntegrator") {
        return std::make_shared<SkelPTShadowApproxIntegrator>(params.get<int>("maxDepth"), params.get<bool>("indirectOnly"));
    }
    
    if(type == "SkelPathtraceIntegrator") {
        return std::make_shared<SkelPathtraceIntegrator>(
            params.get<int>("maxDepth"), 
            params.get<bool>("indirectOnly"),
            params.get<float>("skelStrength")
        );
    }
    
    if(type == "SkelMISPathtraceIntegrator") {
        return std::make_shared<SkelMISPathtraceIntegrator>(
            params.get<int>("maxDepth"), 
            params.get<bool>("indirectOnly"),
            params.get<float>("skelStrength"),
            params.get<float>("skelProb")
        );
    }
    
    if(type == "BidirPathtraceIntegrator") {
        /*return std::make_shared<BidirPathtraceIntegrator>(
            params.get<int>("minEyeDepth"), 
            params.get<int>("maxEyeDepth"), 
            params.get<int>("minLightDepth"),
            params.get<int>("maxLightDepth"),
            params.get<bool>("indirectOnly")
        );*/
        return std::make_shared<BidirPathtraceIntegrator>(
            params.get<int>("maxDepth"), 
            params.get<bool>("indirectOnly")
        );
    }

    if(type == "GateLightingTestIntegrator") {
        return std::make_shared<GateLightingTestIntegrator>(createVPLSampler(params.get<ParamSet>("vplSampler")));
    }
    
    if(type == "VPLIntegrator") {
        float distanceClamping = params.get<float>("distanceClamping");
        float geometryClamping = embree::inf;
        if(distanceClamping > 0) {
            geometryClamping = embree::rcp(distanceClamping);
        }
        VPLSamplerPtr vplSampler = createVPLSampler(params.get<ParamSet>("vplSampler"));
        bool useSkelShadowApprox = params.get<bool>("useSkelShadowApprox", false);

        return std::make_shared<VPLIntegrator>(geometryClamping, params.get<float>("rrTreshold"), vplSampler, useSkelShadowApprox);
    }

    if(type == "VPLVisibilityApproximationErrorIntegrator") {
        VPLSamplerPtr vplSampler = createVPLSampler(params.get<ParamSet>("vplSampler"));
        return std::make_shared<VPLVisibilityApproximationErrorIntegrator>(params.get<std::string>("visibilityApproximationMethod"),
                                               params.get<float>("treshold", 0.f), vplSampler);
    }
    
    if(type == "LightcutsIntegrator") {
        float distanceClamping = params.get<float>("distanceClamping");
        float geometryClamping = embree::inf;
        if(distanceClamping > 0) {
            geometryClamping = embree::rcp(distanceClamping);
        }
        VPLSamplerPtr vplSampler = createVPLSampler(params.get<ParamSet>("vplSampler"));
        return std::make_shared<LightcutsIntegrator>(params.get<size_t>("lightcutMaxSize"), geometryClamping, 
            params.get<float>("relativeErrorBound"), vplSampler);
    }
    
    if(type == "DepthIntegrator") {
        return std::make_shared<DepthIntegrator>();
    }
    
    if(type == "NormalIntegrator") {
        return std::make_shared<NormalIntegrator>();
    }
    
    if(type == "ImportanceIntegrator") {
        return std::make_shared<ImportanceIntegrator>(params.get<size_t>("viewRayCountX"),
            params.get<size_t>("viewRayCountY"));
    }
    
    if(type == "SkelClusteringIntegrator") {
        return std::make_shared<SkelClusteringIntegrator>();
    }

    if(type == "SkelNodeIntegrator") {
        return std::make_shared<SkelNodeIntegrator>(
            params.get<bool>("useClustering", false)
        );
    }

    if(type == "VisibilityClusteringIntegrator") {
        return std::make_shared<VisibilityClusteringIntegrator>();
    }
    
    if(type == "SkelImportanceDistributionIntegrator") {
        return std::make_shared<SkelImportanceDistributionIntegrator>(
            params.get<size_t>("viewRayCountX"),
            params.get<size_t>("viewRayCountY"),
            params.get<size_t>("scatteringRayCountX"),
            params.get<size_t>("scatteringRayCountY"),
            params.get<bool>("useClustering"),
            params.get<std::string>("importanceFilter")
        );
    }

    if(type == "SkelNodeCoherenceIntegrator") {
        return std::make_shared<SkelNodeCoherenceIntegrator>(
            params.get<bool>("useClustering", false)
        );
    }

    if(type == "SkelNodeDistanceIntegrator") {
        return std::make_shared<SkelNodeDistanceIntegrator>(
            params.get<bool>("useClustering", false)
        );
    }

    std::cerr << "Unkown integrator " << type << std::endl;
    return nullptr;
}

IntegratorSamplerPtr createIntegratorSampler(const ParamSet& params) {
    std::string type = params.get<std::string>("type");
    if(type == "StratifiedSampler") {
       return std::make_shared<StratifiedSampler>(params.get<size_t>("sppX"), params.get<size_t>("sppY"));
    }
    std::cerr << "Unkown sampler " << type << std::endl;
    return nullptr;
}

RendererPtr createIntegratorRenderer(const ParamSet& params) {
    IntegratorSamplerPtr sampler = createIntegratorSampler(params.get<ParamSet>("sampler"));
    IntegratorPtr integrator = createIntegrator(params.get<ParamSet>("integrator"));
    
    return std::make_shared<IntegratorRenderer>(integrator, sampler);
}

RendererPtr createRenderer(const ParamSet& params) {
    std::string type = params.get<std::string>("type");
    
    if(type == "IntegratorRenderer") {
        return createIntegratorRenderer(params);
    }

    std::cerr << "Unkown renderer " << type << std::endl;
    return nullptr;
}

}
