#ifndef _BONEZ_VPLRENDERER_HPP
#define _BONEZ_VPLRENDERER_HPP

#include "bonez/common/common.hpp"
#include "bonez/common/Progress.hpp"
#include "bonez/scene/lights/vpls.hpp"
#include "bonez/common/cameras/Camera.hpp"
#include "Framebuffer.hpp"

namespace BnZ {

class VPLRenderer {
public:
    VPLRenderer(): m_LightPosition(0.f), m_LightIntensity(0.f) {
    }

    void render(const Camera& camera, const Scene& scene,
        Framebuffer& framebuffer, bool showProgress, bool showIrradiance, bool showDirectLight,
        float distClamp, int spp);

    void setVPLs(VPLContainer vpls) {
        m_VPLContainer = vpls;
    }

    void setPrimaryLightSource(Vec3f lightPosition, Col3f lightIntensity) {
        m_LightPosition = lightPosition;
        m_LightIntensity = lightIntensity;
    }

private:
    VPLContainer m_VPLContainer;

    Col3f Li(const Ray& ray);

    static void runRenderThread(const embree::TaskScheduler::ThreadInfo& thread,
                                VPLRenderer* self, size_t threadID) {
        self->renderThread(threadID);
    }

#if BONEZ_MULTITHREADING_ENABLED
    embree::Atomic m_nTileID;
#else
    size_t m_nTileID;
#endif

    static const size_t TILE_SIZE = 32; // A thread process a paquet of TILE_SIZE pixels

    ThreadSafeProgress m_Progress;

    const Camera* m_pCamera; // The camera seeing the scene
    const Scene* m_pScene; // The scene to render
    Framebuffer* m_pFramebuffer; // The framebuffer where to draw the image

    bool m_bShowProgress; // A boolean indicating if the rendering status must be shown on console
    bool m_bShowIrradiance;
    bool m_bShowDirectLight;
    int m_nSppEdge;
    float m_fDistClamp;

    size_t m_nTileX, m_nTileY;

    Vec3f m_LightPosition;
    Col3f m_LightIntensity;

    // This function is called by each rendering thread. It loops over the tiles to render until each one is
    // done.
    void renderThread(size_t threadID);
};

}

#endif // _BONEZ_VPLRENDERER_HPP
