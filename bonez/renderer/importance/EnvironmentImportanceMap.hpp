#ifndef _BONEZ_ENVIRONMENTIMPORTANCEMAP_HPP_
#define _BONEZ_ENVIRONMENTIMPORTANCEMAP_HPP_

#include <memory>

#include "bonez/common.hpp"
#include "bonez/maths/maths.hpp"

namespace BnZ {

class EnvironmentImportanceMap {
public:
    EnvironmentImportanceMap():
        m_nWidth(0), m_nHeight(0), 
        m_Map(new float[m_nWidth * m_nHeight]) {
    }
    
    bool empty() const {
        return !m_Map;
    }

    void init(const EnvironmentLight& envLight, float epsilon) {
        if(envLight.empty()) {
            return;
        }
        
        m_nWidth = envLight.width();
        m_nHeight = envLight.height();
        m_Map.reset(new float[m_nWidth * m_nHeight]);
        
        std::fill(m_Map.get(), m_Map.get() + m_nWidth * m_nHeight, epsilon);
        
        m_fMaxImportance = 0.f;
        m_fTotalImportance = 0.f;
        m_fMeanImportance = 0.f;
    }
    
    /**
     * Accumulate importance for a exitant direction of the environment light.
     * \param wo The exitant direction.
     * \param importance The value to accumulate.
     */
    void accumulate(Vec3f wo, float importance) {
        if(!m_Map) {
            return;
        }
        
        Vec2f s = cartesianToSpherial(-wo);
    
        float x = s.x * (float) embree::one_over_two_pi * m_nWidth;
        float y = s.y * (float) embree::one_over_pi * m_nHeight;
        
        size_t i = embree::clamp(size_t(x), size_t(0), size_t(m_nWidth - 1));
        size_t j = embree::clamp(size_t(y), size_t(0), size_t(m_nHeight - 1));
        
        m_Map[i + j * m_nWidth] += importance;
    }
    
    /**
     * Return the value of importance of a pixel.
     */
    float get(size_t x, size_t y) const {
        return m_Map[x + y * m_nWidth];
    }
    
    const float* data() const {
        return m_Map.get();
    }
    
    /**
     * Return the value of importance of an exitant direction.
     */
    float get(Vec3f wo) const {
        Vec2f s = cartesianToSpherial(-wo);
        
        float x = s.x * (float) embree::one_over_two_pi * m_nWidth;
        float y = s.y * (float) embree::one_over_pi * m_nHeight;
        
        return get(embree::clamp(size_t(x), size_t(0), size_t(m_nWidth - 1)),
            embree::clamp(size_t(y), size_t(0), size_t(m_nHeight - 1)));
    }
    
    void computeStatistics() {
        m_fMaxImportance = 0.f;
        m_fTotalImportance = 0.f;
        m_fMeanImportance = 0.f;
        
        size_t nbBlackPixels = 0;
        
        for(const float *v = m_Map.get(), *end = m_Map.get() + m_nWidth * m_nHeight;
            v != end; ++v) {
            if(*v > m_fMaxImportance) {
                m_fMaxImportance = *v;
            }
            m_fTotalImportance += *v;
            if(*v == 0.f) {
                ++nbBlackPixels;
            }
        }
        m_fMeanImportance = m_fTotalImportance * embree::rcp((float) m_nWidth * m_nHeight);
        
        std::clog << "Environement Importance Map Statistics: " << std::endl;
        std::clog << "\t Max importance = " << m_fMaxImportance << std::endl;
        std::clog << "\t Total importance = " << m_fTotalImportance << std::endl;
        std::clog << "\t Mean importance = " << m_fMeanImportance << std::endl;
        
        std::clog << "\t Nb black pixels = " << nbBlackPixels << " / " << (m_nWidth * m_nHeight) << std::endl;
    }
    
    float getMaxImportance() const {
        return m_fMaxImportance;
    }
    
    float getTotalImportance() const {
        return m_fTotalImportance;
    }
    
    float getMeanImportance() const {
        return m_fMeanImportance;
    }
    
    void blur() {
        std::unique_ptr<float[]> buffer(new float[m_nWidth * m_nHeight]);
        for(int y = 0; y < (int)m_nHeight; ++y) {
            for(int x = 0; x < (int)m_nWidth; ++x) {
                float value = 0.f;
                for(int j = -1; j < 2; ++j) {
                    for(int i = -1; i < 2; ++i) {
                        int x0 = embree::clamp(x + i, 0, (int) m_nWidth - 1);
                        int y0 = embree::clamp(y + j, 0, (int) m_nHeight - 1);
                        value += m_Map[x0 + y0 * m_nWidth];
                    }
                }
                buffer[x + y * m_nWidth] = value;
            }
        }
        m_Map = std::move(buffer);
    }
    
private:
    size_t m_nWidth, m_nHeight;
    std::unique_ptr<float[]> m_Map;
    
    float m_fMaxImportance;
    float m_fTotalImportance;
    float m_fMeanImportance;
};

}

#endif
