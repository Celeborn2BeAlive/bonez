#include <sstream>

#include "IntegratorRenderer.hpp"

#include "bonez/common/sampling/Sampler.hpp"

#include "bonez/renderer/instantradiosity/VPLIntegrator.hpp"

namespace BnZ {

void IntegratorRenderer::renderThread(size_t threadID) {
    const Camera* camera = m_pCamera;
    const Scene* scene = m_pScene;
    Framebuffer* framebuffer = m_pFramebuffer;
    size_t width = framebuffer->getWidth();
    size_t height = framebuffer->getHeight();
    size_t tileCount = m_nTileX * m_nTileY;
    embree::Vec2f rcpDimensions(embree::rcp(embree::Vec2f(width, height)));

    Integrator::StoragePtr storage = m_pIntegrator->getStorage();
    IntegratorSamplerPtr sampler = m_pSampler->clone();
    IntegratorSample sample;
    
    RayCounter rayCounter;
    
    Col3f sumLi(0.f);
    size_t sampleCount = 0;
    
    while (true) {
        size_t tileID = m_nTileID++;

        if (tileID >= tileCount) {
            break;
        }

        size_t tileX = tileID % m_nTileX;
        size_t tileY = (tileID - tileX) / m_nTileX;
        
        size_t xstart = embree::min(tileX * TILE_SIZE, width);
        size_t xend = embree::min(xstart + TILE_SIZE, width);
        size_t ystart = embree::min(tileY * TILE_SIZE, height);
        size_t yend = embree::min(ystart + TILE_SIZE, height);

        sampleCount += sampler->init(m_nFrameIdx, xstart, ystart, xend, yend);

        while(sampler->getNextSample(sample)) {
            Col3f value = m_pIntegrator->Li(camera->ray(sample),
                            *scene, sample, rayCounter, storage);
            sumLi += value;
            Vec2i pixel = sample.getPixel();
            framebuffer->accumulate(Vec2i(embree::clamp(pixel.x, 0, (int) width - 1), embree::clamp(pixel.y, 0, (int) height - 1)), value);
        }

        if (m_bShowProgress) {
            m_Progress.next();
        }
    }
    
    embree::Lock<embree::MutexSys> lock(m_StatsMutex);
    m_SumLi += sumLi;
    m_nSampleCount += sampleCount;
}

void IntegratorRenderer::render(const Camera& camera, const Scene& scene, 
    Framebuffer& framebuffer, bool showProgress, ParamSet* statistics) {
    m_nTileID = 0;
    m_pCamera = &camera;
    m_pScene = &scene;
    m_pFramebuffer = &framebuffer;
    m_bShowProgress = showProgress;

    m_nTileX = (framebuffer.getWidth() + TILE_SIZE - 1) / TILE_SIZE;
    m_nTileY = (framebuffer.getHeight() + TILE_SIZE - 1) / TILE_SIZE;

    m_SumLi = Col3f(0.f);
    m_nSampleCount = 0;

    if (showProgress) {
        m_Progress.init(m_nTileX * m_nTileY);
        m_Progress.start();
    }

    ParamSet integratorPreprocessStatistics;
    m_pIntegrator->preprocess(camera, scene, framebuffer, m_nFrameIdx, integratorPreprocessStatistics);

#if BONEZ_MULTITHREADING_ENABLED
    embree::scheduler->start();
    embree::scheduler->addTask(embree::TaskScheduler::ThreadInfo(), embree::TaskScheduler::GLOBAL_FRONT,
                               (embree::TaskScheduler::runFunction) &runRenderThread, this, embree::scheduler->getNumThreads(),
                               nullptr, nullptr, "render::tile");
    embree::scheduler->stop();
#else
    renderThread(0);
#endif

    if (showProgress) {
        m_Progress.end();
    }
    
    if(statistics) {
        statistics->set("frameIdx", (int) m_nFrameIdx);
        statistics->set("integratorPreprocess", integratorPreprocessStatistics);
        statistics->set("sumLi", m_SumLi);
        
        Col3f incidentPower = camera.area() * m_SumLi / float(m_nSampleCount);
        statistics->set("incidentPowerEstimate", incidentPower);
    }

    ++m_nFrameIdx;
}

std::string IntegratorRenderer::getRenderString() const {
    return m_pSampler->toString() + "_" + m_pIntegrator->toString();
}

bool IntegratorRenderer::getVPLs(const Camera& camera, const Scene& scene, 
    const Framebuffer& framebuffer, VPLContainer& vpls) {
    if(VPLIntegrator* integrator = dynamic_cast<VPLIntegrator*>(m_pIntegrator.get())) {
        ParamSet stats;
        integrator->preprocess(camera, scene, framebuffer, m_nFrameIdx, stats);
        vpls = integrator->m_VPLContainer;
        return true;
    }
    return false;
}

std::string IntegratorRenderer::getType() const {
    return m_pIntegrator->getType();
}

}
