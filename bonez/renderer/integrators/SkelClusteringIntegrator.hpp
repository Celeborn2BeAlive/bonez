#ifndef _BONEZ_SKELCLUSTERINGINTEGRATOR2_HPP_
#define _BONEZ_SKELCLUSTERINGINTEGRATOR2_HPP_

#include <vector>
#include <embree/common/math/permutation.h>
#include "bonez/renderer/Integrator.hpp"

namespace BnZ {

class SkelClusteringIntegrator: public Integrator {
public:
    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
        Intersection intersection = intersect(ray, scene, rayCounter);

        if (!intersection) {
            return Col3f(0.f);
        }

        GraphNodeIndex idx = scene.voxelSpace->getClustering().getNearestNode(intersection);
        if(idx == UNDEFINED_NODE) {
            return Col3f(0.f);
        }

        /*
        Vec3f direction = embree::normalize(scene.topology.clustering[idx].center - intersection.position);
        return Col3f(embree::dot(direction, intersection.Ns));
        */
        
        /*
        Vec3f direction = scene.topology.clustering[idx].center - intersection.position;
        return Col3f(embree::length(direction) / (2 * scene.topology.clustering[idx].meanMaxBallRadius));
        */
        
        return embree::dot(-ray.dir, intersection.Ns) * scene.voxelSpace->getClusteringNodeColor(idx);
    }
    
    virtual std::string toString() const {
        return "SkelClusteringIntegrator";
    }
    
    virtual std::string getType() const {
        return "SkelClusteringIntegrator";
    }
};

}

#endif
