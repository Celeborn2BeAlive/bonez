#ifndef _BONEZ_GATELIGHTINGTESTINTEGRATOR_HPP_
#define _BONEZ_GATELIGHTINGTESTINTEGRATOR_HPP_

#include <vector>
#include <embree/common/math/permutation.h>
#include "bonez/renderer/Integrator.hpp"
#include "bonez/scene/topology/VisibilityClustering.hpp"
#include "bonez/renderer/instantradiosity/VPLSampler.hpp"

namespace BnZ {

class GateLightingTestIntegrator: public Integrator {
public:
    float m_fGeometryClamping;
    float m_fRRTreshold;
    VisibilityClustering m_VisibilityClustering;
    VPLSamplerPtr m_pVPLSampler;
    VPLContainer m_VPLContainer;
    std::vector<std::vector<Col3f>> m_GatePower;
    std::vector<std::vector<uint32_t>> m_ClusterVPLs;

    GateLightingTestIntegrator(const VPLSamplerPtr& vplSampler):
        m_fGeometryClamping(embree::inf), m_fRRTreshold(0.f), m_pVPLSampler(vplSampler) {
    }

    virtual void preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
        size_t frameIdx, ParamSet& statistics) {
        if(frameIdx == 0) {
            m_VisibilityClustering = buildVisibilityClustering(scene.voxelSpace->getClustering());
        }
        m_VPLContainer = m_pVPLSampler->sample(camera, scene, framebuffer, frameIdx, statistics);

        // Reset gate power array
        m_GatePower.resize(m_VisibilityClustering.size());
        for(const auto& cluster: index(m_VisibilityClustering)) {
            m_GatePower[cluster.first].resize(cluster.second.getNeighbourLinks().size(), Col3f(0.f));
        }

        // Compute the VPLs contained in each cluster
        m_ClusterVPLs.clear();
        m_ClusterVPLs.resize(m_VisibilityClustering.size());

        for(const auto& vpl: index(m_VPLContainer.orientedVPLs)) {
            GraphNodeIndex idx = scene.voxelSpace->getClustering().getNearestNode(vpl.second.P, vpl.second.N);
            if(idx == UNDEFINED_NODE) {
                continue;
            }
            int clusterIdx = m_VisibilityClustering.getClusterOf(idx);
            if(clusterIdx < 0) {
                continue;
            }
            m_ClusterVPLs[clusterIdx].push_back(vpl.first);
        }

        // Compute gate powers
        Random rng(0);
        size_t Nsample = 8;
        for(const auto& cluster: index(m_VisibilityClustering)) {
            for(const auto& neighbourLink: index(m_VisibilityClustering[cluster.first].getNeighbourLinks())) {
                auto gate = m_VisibilityClustering.getGate(neighbourLink.second);
                Col3f power(embree::zero);
                float gateArea = float(embree::pi) * gate.radius * gate.radius;

                repeat(Nsample, [&] {
                    Vec2f gateSample = embree::uniformSampleDisk(Vec2f(rng.getFloat(), rng.getFloat()), gate.radius);
                    AffineSpace3f frame = coordinateSystem(gate.center, gate.N);
                    Vec3f P = embree::xfmPoint(frame, Vec3f(gateSample.x, 0, gateSample.y));

                    for(uint32_t vplIdx: m_ClusterVPLs[cluster.first]) {
                        const OrientedVPL& vpl = m_VPLContainer.orientedVPLs[vplIdx];
                        Vec3f wi = P - vpl.P;
                        float dot_i = embree::dot(wi, vpl.N);
                        if(dot_i < 0.f) {
                            continue;
                        }
                        float distSquared = lengthSquared(wi);
                        float dot_o = embree::abs(embree::dot(-wi, gate.N));

                        float G = dot_i * dot_o / (distSquared * distSquared);

                        power += vpl.L * G;
                    }
                });

                power = gateArea * power / Nsample;

                int gateIdx = -1;

                // Search the index of the link that connect the neighbour to this cluster in the link list of the neighbours
                for(const auto& neighbourLink2: index(m_VisibilityClustering[neighbourLink.second.neighbour].getNeighbourLinks())) {
                    if(neighbourLink2.second.gateIdx == neighbourLink.second.gateIdx) {
                        gateIdx = neighbourLink2.first;
                    }
                }

                if(gateIdx >= 0) {
                    m_GatePower[neighbourLink.second.neighbour][gateIdx] = power;

                    /*
                    std::cerr << "power of gate " << cluster.first << ", " << neighbourLink.second.neighbour << " / " << neighbourLink.second.gateIdx
                                 << m_GatePower[neighbourLink.second.neighbour][gateIdx] << std::endl;
                    */
                } else {
                    std::cerr << "gate idx < 0 for " << cluster.first << ", " << neighbourLink.second.neighbour << " / " << neighbourLink.second.gateIdx << std::endl;
                }
            }
        }
    }

    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
        Intersection I = intersect(ray, scene, rayCounter);

        if (!I) {
            return Col3f(0.f);
        }

        GraphNodeIndex idx = scene.voxelSpace->getClustering().getNearestNode(I);
        if(idx == UNDEFINED_NODE) {
            return BAD_COLOR;
        }

        int vcluster = m_VisibilityClustering.getClusterOf(idx);
        if(vcluster < 0 || m_VisibilityClustering[vcluster].getNeighbourLinks().empty()) {
            return Col3f(1.f) - BAD_COLOR;
        }

        BRDF brdf = scene.shade(I);
        Vec3f wo = -ray.dir;

        Col3f L(0.f);

        for(uint32_t i: m_ClusterVPLs[vcluster]) {
            const OrientedVPL& vpl = m_VPLContainer.orientedVPLs[i];

            Ray shadowRay;
            float geometryTerm = embree::min(m_fGeometryClamping,
                G(I, vpl, shadowRay));

            if(geometryTerm == 0.f) {
                continue;
            }

            Col3f reflectionTerm = brdf.eval(shadowRay.dir, wo);
            if(reflectionTerm == Col3f(embree::zero)) {
                continue;
            }

            Col3f vplContrib = vpl.L * geometryTerm * reflectionTerm;

            /*
            if(!occluded(shadowRay, scene, rayCounter)) {
                L += vplContrib;
            }*/

            L += vplContrib;
        }

        Random rng(0);
        for(const auto& neighbourLink: index(m_VisibilityClustering[vcluster].getNeighbourLinks())) {
            auto gate = m_VisibilityClustering.getGate(neighbourLink.second);
            Col3f color(0.f);

            Vec2f gateSample = embree::uniformSampleDisk(Vec2f(rng.getFloat(), rng.getFloat()), gate.radius);
            AffineSpace3f frame = coordinateSystem(gate.center, gate.N);
            Vec3f P = embree::xfmPoint(frame, Vec3f(gateSample.x, 0, gateSample.y));
            Vec3f wi = P - I.P;
            float dot_i = embree::dot(wi, I.Ns);
            if(dot_i >= 0.f) {
                float distSquared = lengthSquared(wi);
                float dot_o = embree::abs(embree::dot(-wi, gate.N));

                float G = dot_i * dot_o / (distSquared * distSquared);

                wi /= sqrt(distSquared);

                color = brdf.eval(wi, wo) * m_GatePower[vcluster][neighbourLink.first] * G;
                L += color;
            }
        }

        /*
        RayCounter counter;
        for(const OrientedVPL& vpl: m_VPLContainer.orientedVPLs) {
            GraphNodeIndex node = scene.voxelSpace->getClustering().getNearestNode(vpl.P, vpl.N);
            if(node != UNDEFINED_NODE) {
                int cluster = m_VisibilityClustering.getClusterOf(node);
                if(cluster == vcluster) {
                    continue;
                }
            }

            Ray shadowRay;
            float geometryTerm = G(I, vpl, shadowRay);

            if(geometryTerm == 0.f) {
                continue;
            }

            Col3f reflectionTerm = brdf.eval(shadowRay.dir, wo);
            if(reflectionTerm == Col3f(embree::zero)) {
                continue;
            }

            Col3f vplContrib = vpl.L * geometryTerm * reflectionTerm;
            if(!occluded(shadowRay, scene, counter)) {
                L += vplContrib;
            }
        }
        */

        return L;

#if 0
        Col3f L(0.f);
        for(const OrientedVPL& vpl: m_VPLContainer.orientedVPLs) {
            GraphNodeIndex idx = scene.topology.getNearestCluster(vpl.P, vpl.N);
            if(idx == UNDEFINED_NODE) {
                continue;
            }
            int vplCluster = m_VisibilityClustering.getClusterOf(idx);
            if(vplCluster < 0) {
                continue;
            }

            Ray shadowRay;
            float geometryTerm = embree::min(m_fGeometryClamping,
                G(I, vpl, shadowRay));

            if(geometryTerm == 0.f) {
                continue;
            }

            Col3f reflectionTerm = brdf.eval(shadowRay.dir, wo);
            if(reflectionTerm == Col3f(embree::zero)) {
                continue;
            }

            Col3f vplContrib = vpl.L * geometryTerm * reflectionTerm;

            float luminance = embree::luminance(vplContrib);
            if (luminance < m_fRRTreshold) {
                float rrProb = embree::min(1.f, luminance);

                if (sample.getRandomFloat() > rrProb) {
                    continue;
                }

                vplContrib *= embree::rcp(rrProb);
            }

            bool isVisible = true;
            if(cluster != vplCluster) {
                isVisible = false;
                for(const VisibilityCluster::Gate& gate: m_VisibilityClustering[cluster].getGates()) {
                    if(vplCluster == (int) gate.neighbourCluster) {
                        float denom = embree::dot(gate.N, ray.dir);
                        if(denom == 0.f) {
                            continue;
                        }
                        float t = - embree::dot(gate.N, ray.org - gate.center) / denom;
                        if(t < 0.f) {
                            continue;
                        }
                        Vec3f P = ray.org + t * ray.dir;
                        if(distanceSquared(P, gate.center) < gate.radius * gate.radius) {
                            isVisible = true;
                        }
                    }
                }
            }

            //isVisible = !occluded(shadowRay, scene, rayCounter);

            /*
            if(occluded(shadowRay, scene, rayCounter)) {
                continue;
            }*/

            if(isVisible) {
                L += vplContrib;
            }
        }

        return L;
#endif
    }

    virtual std::string toString() const {
        return "GateLightingTestIntegrator";
    }

    virtual std::string getType() const {
        return "GateLightingTestIntegrator";
    }
};

}

#endif
