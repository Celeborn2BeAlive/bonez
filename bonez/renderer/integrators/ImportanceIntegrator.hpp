#ifndef _BONEZ_IMPORTANCEINTEGRATOR_HPP_
#define _BONEZ_IMPORTANCEINTEGRATOR_HPP_

#include <memory>
#include <sstream>
#include <iomanip>

#include "bonez/renderer/Integrator.hpp"
#include "bonez/common/sampling/patterns.hpp"

namespace BnZ {

class ImportanceIntegrator: public Integrator {
    struct Importon {
        Intersection intersection; // Intersection in the scene where the importon is located
        float I; // Importance carried by the importon
        Vec3f wo; // Direction from the importon to the camera
    };
    
    size_t m_nViewRayCountX;
    size_t m_nViewRayCountY;
     
    std::unique_ptr<Importon[]> m_Importons;
    size_t m_nImportonCount;
    
public:
    ImportanceIntegrator(size_t viewRayCountX, size_t viewRayCountY):
        m_nViewRayCountX(viewRayCountX),
        m_nViewRayCountY(viewRayCountY),
        m_nImportonCount(0) {
    }

    virtual void preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
        size_t frameIdx, ParamSet& statistics) {
        std::clog << "Generation of importons..." << std::endl;
        double start = embree::getSeconds();
        
        size_t viewRayCount = m_nViewRayCountX * m_nViewRayCountY;
        
        m_Importons.reset(new Importon[viewRayCount]);
        
        std::unique_ptr<Vec2f[]> viewSamples(new Vec2f[viewRayCount]);
        Random rng(frameIdx);
        createJittered2DPattern(viewSamples.get(), m_nViewRayCountX, m_nViewRayCountY, rng);
        
        float importance = embree::rcp((float) viewRayCount);
        
        size_t importonIdx = 0;
        
        for (size_t i = 0; i < viewRayCount; ++i) {
            Ray viewRay = camera.ray(viewSamples[i]);
            Importon importon;
            importon.intersection = intersect(viewRay, scene);

            if (!importon.intersection) {
                continue;
            }

            importon.I = importance;
            importon.wo = -viewRay.dir;
            
            m_Importons[importonIdx++] = importon;
        }
        
        m_nImportonCount = importonIdx;
        
        std::clog << "OK. Time = " << (embree::getSeconds() - start) << " seconds." << std::endl;
    }
    
    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
        Intersection intersection = intersect(ray, scene, rayCounter);

        if (!intersection) {
            return Col3f(1.f, 0.f, 0.f);
        }
        
        float I = 0.f;
        for(const Importon *importon = m_Importons.get(), *end = m_Importons.get() + m_nImportonCount;
            importon != end; ++importon) {
            Vec3f wi = importon->intersection.P - intersection.P;
            float wiLength = embree::length(wi);
            wi /= wiLength;
            
            float cos_i = embree::dot(intersection.Ns, wi);
            if(cos_i <= 0.f) {
                continue;
            }
            
            float cos_o = embree::dot(importon->intersection.Ns, -wi);
            if(cos_o <= 0.f) {
                continue;
            }
            
            if(occluded(shadow_ray(intersection, wi, wiLength), scene, rayCounter)) {
                continue;
            }
            
            float G = cos_i * cos_o * embree::rcp(embree::sqr(wiLength * wiLength));
            
            BRDF brdf = scene.shade(importon->intersection);
            
            float reflection = embree::luminance(brdf.eval(-wi, importon->wo));
            
            I += importon->I * reflection * G;
        }
        
        return Col3f(I);
    }
    
    virtual std::string toString() const {
        std::stringstream ss;
        ss << "ImportanceIntegrator_" <<
            "viewRayX=" << std::setw(4) << m_nViewRayCountX << "_" <<
            "viewRayY=" << std::setw(4) << m_nViewRayCountY;
        return ss.str();
    }
    
    virtual std::string getType() const {
        return "ImportanceIntegrator";
    }
};

}

#endif
