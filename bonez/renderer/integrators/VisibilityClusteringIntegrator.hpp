#ifndef _BONEZ_VISIBILITYCLUSTERINGINTEGRATOR_HPP
#define _BONEZ_VISIBILITYCLUSTERINGINTEGRATOR_HPP

#include <vector>
#include "bonez/common/data/ColorMap.hpp"
#include "bonez/renderer/Integrator.hpp"
#include "bonez/scene/topology/VisibilityClustering.hpp"

namespace BnZ {

class VisibilityClusteringIntegrator: public Integrator {
public:
    VisibilityClustering m_VisibilityClustering;
    std::vector<Col3f> m_ClusterColors;

    virtual void preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
        size_t frameIdx, ParamSet& statistics) {
        if(0 == frameIdx) {
            m_VisibilityClustering = buildVisibilityClustering(scene.voxelSpace->getClustering());
            m_ClusterColors = buildRandomColorMap(m_VisibilityClustering.size());
        }
    }

    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
        Intersection intersection = intersect(ray, scene, rayCounter);

        if (!intersection) {
            return Col3f(0.f);
        }

        GraphNodeIndex idx = scene.voxelSpace->getClustering().getNearestNode(intersection);
        if(idx == UNDEFINED_NODE) {
            return Col3f(0.f);
        }

        int vcluster = m_VisibilityClustering.getClusterOf(idx);
        if(vcluster < 0) {
            return Col3f(0.f);
        }

        return embree::dot(-ray.dir, intersection.Ns) * m_ClusterColors[vcluster];
    }

    virtual std::string toString() const {
        return "VisibilityClusteringIntegrator";
    }

    virtual std::string getType() const {
        return "VisibilityClusteringIntegrator";
    }
};

}

#endif // _BONEZ_VISIBILITYCLUSTERINGINTEGRATOR_HPP
