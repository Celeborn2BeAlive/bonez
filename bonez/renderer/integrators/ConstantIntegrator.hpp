#ifndef _BONEZ_CONSTANTINTEGRATOR_HPP_
#define _BONEZ_CONSTANTINTEGRATOR_HPP_

#include "bonez/api/Integrator.hpp"

namespace BnZ {

class ConstantIntegrator: public Integrator {
public:
    Col3f m_L;

    ConstantIntegrator(const Col3f& L = Col3f(1.f)):
        m_L(L) {
    }

    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
        return m_L;
    }

    virtual std::string toString() const {
        return "ConstantIntegrator";
    }

    virtual std::string getType() const {
        return "ConstantIntegrator";
    }
};

}

#endif
