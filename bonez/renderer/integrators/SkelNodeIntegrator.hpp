#ifndef _BONEZ_SKELNODEINTEGRATOR_HPP_
#define _BONEZ_SKELNODEINTEGRATOR_HPP_

#include <vector>
#include <embree/common/math/permutation.h>
#include "bonez/renderer/Integrator.hpp"

namespace BnZ {

class SkelNodeIntegrator: public Integrator {
public:
    bool m_bUseClustering;

    SkelNodeIntegrator(bool useClustering):
        m_bUseClustering(useClustering) {
    }

    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
        Intersection intersection = intersect(ray, scene, rayCounter);

        if (!intersection) {
            return Col3f(0.f);
        }

        if(m_bUseClustering) {
            GraphNodeIndex idx = scene.voxelSpace->getClustering().getNearestNode(intersection);
            if(idx == UNDEFINED_NODE) {
                return Col3f(0.f);
            }

            return embree::dot(-ray.dir, intersection.Ns) * scene.voxelSpace->getClusteringNodeColor(idx);
        } else {
            GraphNodeIndex idx = scene.voxelSpace->getSkeleton().getNearestNode(intersection);
            if(idx == UNDEFINED_NODE) {
                return Col3f(0.f);
            }

            return embree::dot(-ray.dir, intersection.Ns) * scene.voxelSpace->getSkeletonNodeColor(idx);
        }
    }

    virtual std::string toString() const {
        return "SkelNodeIntegrator";
    }

    virtual std::string getType() const {
        return "SkelNodeIntegrator";
    }

    virtual void setParam(const std::string& name, const ParamPtr& value) {
        if(name == "useClustering") {
            m_bUseClustering = value->get<bool>();
        }
    }
};

}

#endif
