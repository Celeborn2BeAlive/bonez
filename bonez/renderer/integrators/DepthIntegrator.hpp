#ifndef _BONEZ_DEPTHINTEGRATOR_HPP_
#define _BONEZ_DEPTHINTEGRATOR_HPP_

#include "bonez/renderer/Integrator.hpp"

namespace BnZ {

class DepthIntegrator: public Integrator {
public:
    AffineSpace3f m_WorldToCamera;
    
    virtual void preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
        size_t frameIdx, ParamSet& statistics) {
        m_WorldToCamera = embree::rcp(camera.getLocalToWorldTransform());
    }

    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
        Intersection intersection = intersect(ray, scene, rayCounter);

        if (!intersection) {
            return Col3f(1, 0, 0);
        }
        
        Vec3f position = embree::xfmPoint(m_WorldToCamera, intersection.P);
        return Col3f(position.z);
    }
    
    virtual std::string toString() const {
        return "DepthIntegrator";
    }
    
    virtual std::string getType() const {
        return "DepthIntegrator";
    }
};

}

#endif
