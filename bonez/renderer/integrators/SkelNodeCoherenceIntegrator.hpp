#ifndef _BONEZ_INTEGRATORS_SKELNODECOHERENCEINTEGRATOR_HPP_
#define _BONEZ_INTEGRATORS_SKELNODECOHERENCEINTEGRATOR_HPP_

#include "bonez/renderer/Integrator.hpp"

namespace BnZ {

class SkelNodeCoherenceIntegrator: public Integrator {
public:
    bool m_bUseClustering;

    SkelNodeCoherenceIntegrator(bool useClustering):
        m_bUseClustering(useClustering) {
    }

    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
        Intersection I = intersect(ray, scene, rayCounter);

        if (!I) {
            return Col3f(0.f);
        }

        const Skeleton* pSkeleton = nullptr;
        if(!m_bUseClustering) {
            pSkeleton = &scene.voxelSpace->getSkeleton();
        } else {
            pSkeleton = &scene.voxelSpace->getClustering();
        }

        GraphNodeIndex i = pSkeleton->getNearestNode(I);

        if (UNDEFINED_NODE == i) {
            return Col3f(1.f, 0.f, 0.f);
        }

        Skeleton::Node node = pSkeleton->getNode(i);

        Vec3f wi = node.P - I.P;
        float wiLength = embree::length(wi);
        wi /= wiLength;

        float dot = embree::dot(wi, I.Ns);
        if(dot < 0.f) {
            return Col3f(0.f, 1.0f, 0.f);
        }

        if(occluded(shadow_ray(I, wi, wiLength), scene, rayCounter)) {
            return Col3f(0.f, 0.f, 1.f);
        }

        return Col3f(dot);
    }

    virtual std::string toString() const {
        return "SkelNodeCoherenceIntegrator";
    }

    virtual std::string getType() const {
        return "SkelNodeCoherenceIntegrator";
    }

    virtual void setParam(const std::string& name, const ParamPtr& value) {
        if(name == "useClustering") {
            m_bUseClustering = value->get<bool>();
        }
    }
};

class SkelNodeDistanceIntegrator: public Integrator {
public:
    bool m_bUseClustering;

    SkelNodeDistanceIntegrator(bool useClustering):
        m_bUseClustering(useClustering) {
    }

    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
        Intersection I = intersect(ray, scene, rayCounter);

        if (!I) {
            return Col3f(0.f);
        }

        const Skeleton* pSkeleton = nullptr;
        if(!m_bUseClustering) {
            pSkeleton = &scene.voxelSpace->getSkeleton();
        } else {
            pSkeleton = &scene.voxelSpace->getClustering();
        }

        GraphNodeIndex i = pSkeleton->getNearestNode(I);

        if (UNDEFINED_NODE == i) {
            return Col3f(1.f);
        }

        Skeleton::Node node = pSkeleton->getNode(i);

        Vec3f wi = node.P - I.P;
        float wiLength = embree::length(wi);
        wi /= wiLength;

        if(embree::dot(wi, I.Ns) < 0.f) {
            return Col3f(0.f, 1.0f, 0.f);
        }

        if(occluded(shadow_ray(I, wi, wiLength), scene, rayCounter)) {
            return Col3f(0.f, 0.f, 1.f);
        }

        return Col3f(node.maxball / wiLength);
    }

    virtual std::string toString() const {
        return "SkelNodeDistanceIntegrator";
    }

    virtual std::string getType() const {
        return "SkelNodeDistanceIntegrator";
    }

    virtual void setParam(const std::string& name, const ParamPtr& value) {
        if(name == "useClustering") {
            m_bUseClustering = value->get<bool>();
        }
    }
};

}

#endif
