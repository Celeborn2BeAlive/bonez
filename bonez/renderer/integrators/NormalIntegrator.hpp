#ifndef _BONEZ_NORMALINTEGRATOR_HPP_
#define _BONEZ_NORMALINTEGRATOR_HPP_

#include "bonez/renderer/Integrator.hpp"

namespace BnZ {

class NormalIntegrator: public Integrator {
public:
    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
        Intersection intersection = intersect(ray, scene, rayCounter);

        if (!intersection) {
            return Col3f(0, 0, 0);
        }
        
        return Col3f(intersection.Ns.x, intersection.Ns.y, intersection.Ns.z);
    }
    
    virtual std::string toString() const {
        return "NormalIntegrator";
    }
    
    virtual std::string getType() const {
        return "NormalIntegrator";
    }
};

}

#endif
