#ifndef _BONEZ_SKELIMPORTANCEDENSITYINTEGRATOR_HPP_
#define _BONEZ_SKELIMPORTANCEDENSITYINTEGRATOR_HPP_

#include <sstream>
#include <iomanip>

#include "bonez/common/common.hpp"
#include "bonez/renderer/Integrator.hpp"

#include "bonez/scene/topology/GraphImportanceEstimation.hpp"

namespace BnZ {

class SkelImportanceDistributionIntegrator: public Integrator { 
    size_t m_nViewRayCountX;
    size_t m_nViewRayCountY;
    size_t m_nScatteringRayCountX;
    size_t m_nScatteringRayCountY;
    bool m_bUseClustering;
    std::string m_SkelImportanceFilter;
    
    GraphImportanceEstimation m_Estimation;
    GraphImportanceEstimationStats m_Stats;
public:
    SkelImportanceDistributionIntegrator(size_t viewRayCountX, size_t viewRayCountY,
        size_t scatteringRayCountX, size_t scatteringRayCountY, bool useClustering,
        const std::string& skelImportanceFilter):
        m_nViewRayCountX(viewRayCountX),
        m_nViewRayCountY(viewRayCountY),
        m_nScatteringRayCountX(scatteringRayCountX),
        m_nScatteringRayCountY(scatteringRayCountY),
        m_bUseClustering(useClustering),
        m_SkelImportanceFilter(skelImportanceFilter) {
    }

    virtual void preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
        size_t frameIdx, ParamSet& statistics) {
        if(frameIdx == 0) {
            Random rng(frameIdx);

            m_Estimation = computeImportanceEstimation(
                Vec2ui(m_nViewRayCountX, m_nViewRayCountY), Vec2ui(m_nScatteringRayCountX, m_nScatteringRayCountY),
                scene, camera, rng, scene.voxelSpace->getClustering().getGraph(), [&scene](const Intersection& I) -> GraphNodeIndex {
                    return scene.voxelSpace->getClustering().getNearestNode(I);
                }
            );
            m_Stats = computeStatistics(m_Estimation);
        }
    }
    
    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
        Intersection intersection = intersect(ray, scene, rayCounter);

        if (!intersection) {
            return Col3f(0.f, 1.f, 0.f);
        }

        GraphNodeIndex idx = scene.voxelSpace->getClustering().getNearestNode(intersection);
        
        if (!idx) {
            return Col3f(1.f, 0.f, 0.f);
        }
        
        float density = m_Estimation[idx] / m_Stats.max;
        
        return Col3f(density);
    }
    
    virtual std::string toString() const {
        std::stringstream ss;
        ss << "SkelImportanceDistributionIntegrator_" <<
            "viewRayX=" << std::setw(4) << m_nViewRayCountX << "_" <<
            "viewRayY=" << std::setw(4) << m_nViewRayCountY << "_" <<
            "scattRayX=" << std::setw(3) << m_nScatteringRayCountX << "_" <<
            "scattRayY=" << std::setw(3) << m_nScatteringRayCountY << "_";
        if(m_bUseClustering) {
            ss << "useClustering_";
        }
        ss << "filter=" << m_SkelImportanceFilter;
        return ss.str();
    }
    
    virtual std::string getType() const {
        return "SkelImportanceDistributionIntegrator";
    }
};

}

#endif
