#ifndef _BONEZ_FACTORIES_HPP_
#define _BONEZ_FACTORIES_HPP_

#include "Renderer.hpp"
#include "Integrator.hpp"
#include "instantradiosity/VPLSampler.hpp"
#include "bonez/common/Params.hpp"

namespace BnZ {

PostProcessPtr createPostProcess(const ParamSet& params);

IntegratorPtr createIntegrator(const ParamSet& params);

RendererPtr createIntegratorRenderer(const ParamSet& params);

RendererPtr createRenderer(const ParamSet& params);

VPLSamplerPtr createVPLSampler(const ParamSet& params);

}

#endif
