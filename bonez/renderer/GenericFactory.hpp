#ifndef _BONEZ_FACTORIES_GENERICFACTORY_HPP_
#define _BONEZ_FACTORIES_GENERICFACTORY_HPP_

#include <memory>
#include <string>
#include "bonez/common/Params.hpp"

namespace BnZ {

template<typename Interface, typename... Implementations>
class GenericFactory {
public:
    static std::unique_ptr<Interface> create(const ParamSet& params) {
        throw std::runtime_error("GenericFactory: Unknow type " + params.get<std::string>("type"));
    }
};

template<typename Interface, typename Implementation, typename... Implementations>
class GenericFactory<Interface, Implementation, Implementations...>  {
public:
    static std::unique_ptr<Interface> create(const ParamSet& params) {
        if (params.get<std::string>("type") == Implementation::getType()) {
            return std::unique_ptr<Interface>(new Implementation(params));
        }

        return GenericFactory<Interface, Implementations...>::create(params);
    }
};

}

#endif
