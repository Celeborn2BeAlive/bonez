#ifndef _BONEZ_POSTPROCESSING_SCALINGPOSTPROCESS_HPP_
#define _BONEZ_POSTPROCESSING_SCALINGPOSTPROCESS_HPP_

#include "bonez/common/common.hpp"
#include "bonez/renderer/PostProcess.hpp"

namespace BnZ {

class ScalingPostProcess: public PostProcess {
public:
    ScalingPostProcess(const ParamSet& params):
        m_fScale(params.get<float>("scale")) {
    }

    static std::string getType() {
        return "ScalingPostProcess";
    }

    virtual void apply(const Framebuffer& framebuffer, Framebuffer& result) const {
        for (size_t y = 0; y < framebuffer.getHeight(); ++y) {
            for (size_t x = 0; x < framebuffer.getWidth(); ++x) {
                result.set(x, y, framebuffer(x, y) * m_fScale);
            }
        }
    }

private:
    float m_fScale;
};

}

#endif
