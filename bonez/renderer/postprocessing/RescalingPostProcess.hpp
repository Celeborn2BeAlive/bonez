#ifndef _BONEZ_POSTPROCESSING_RESCALINGPOSTPROCESS_HPP_
#define _BONEZ_POSTPROCESSING_RESCALINGPOSTPROCESS_HPP_

#include "bonez/common/common.hpp"
#include "bonez/renderer/PostProcess.hpp"

namespace BnZ {

/**
 * Re-scale all the red values of a framebuffer to fit in the range [0, 1]
 */
class RescalingPostProcess: public PostProcess {
public:
    RescalingPostProcess(const ParamSet& params) {
    }

    static std::string getType() {
        return "RescalingPostProcess";
    }

    virtual void apply(const Framebuffer& framebuffer, Framebuffer& result) const {
        float max = 0.f;
        float value;

        for (size_t y = 0; y < framebuffer.getHeight(); ++y) {
            for (size_t x = 0; x < framebuffer.getWidth(); ++x) {
                if ((value = framebuffer(x, y).r) > max) {
                    max = value;
                }
            }
        }

        float scale;

        if (max == 0.f) {
            scale = 1.f;
        }
        else {
            scale = embree::rcp(max);
        }

        for (size_t y = 0; y < framebuffer.getHeight(); ++y) {
            for (size_t x = 0; x < framebuffer.getWidth(); ++x) {
                result.set(x, y, framebuffer(x, y) * scale);
            }
        }
    }
};

}

#endif
