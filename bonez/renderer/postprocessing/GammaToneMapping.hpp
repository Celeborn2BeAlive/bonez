#ifndef _BONEZ_POSTPROCESSING_GAMMATONEMAPPING_HPP_
#define _BONEZ_POSTPROCESSING_GAMMATONEMAPPING_HPP_

#include "bonez/common/common.hpp"
#include "bonez/renderer/PostProcess.hpp"

namespace BnZ {

class GammaToneMapping: public PostProcess {
public:
    GammaToneMapping(const ParamSet& params):
        m_fRcpGamma(embree::rcp(params.get<float>("gamma"))) {
    }

    GammaToneMapping(float gamma):
        m_fRcpGamma(embree::rcp(gamma)) {
    }

    static std::string getType() {
        return "GammaToneMapping";
    }

    virtual void apply(const Framebuffer& framebuffer, Framebuffer& result) const {
        for (size_t y = 0; y < framebuffer.getHeight(); ++y) {
            for (size_t x = 0; x < framebuffer.getWidth(); ++x) {
                result.set(x, y, embree::pow(framebuffer(x, y), m_fRcpGamma));
            }
        }
    }

private:
    float m_fRcpGamma;
};

}

#endif
