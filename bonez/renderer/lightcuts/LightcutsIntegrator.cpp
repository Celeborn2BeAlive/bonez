#include "LightcutsIntegrator.hpp"

namespace BnZ {

LightcutsIntegrator::LightcutsIntegrator(size_t lightCutMaxSize, float geometryClamping, float relativeErrorBound,
    const VPLSamplerPtr& vplSampler):
    m_nLightcutMaxSize(lightCutMaxSize),
    m_fGeometryClamping(geometryClamping),
    m_fRelativeErrorBound(relativeErrorBound),
    m_pVPLSampler(vplSampler) {
}

void LightcutsIntegrator::preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
    size_t frameIdx, ParamSet& statistics) {
    ParamSet vplSamplerStats("vplSampler");
    m_VPLContainer = m_pVPLSampler->sample(camera, scene, framebuffer, frameIdx, vplSamplerStats);
    
    std::clog << "LightcutsIntegrator: number of VPLs " << m_VPLContainer.size() << std::endl;

    double start = embree::getSeconds();
    std::clog << "Build the light tree..." << std::endl;
    
    Sampler sampler(frameIdx);
    m_LightTree.build(m_VPLContainer, sampler);

    std::clog << "Done. Time = " << (embree::getSeconds() - start) << " sec." << std::endl;
    
    statistics.set("vplSampler", vplSamplerStats);
}

Integrator::StoragePtr LightcutsIntegrator::getStorage() const {
    return std::make_shared<Storage>(m_LightTree, m_VPLContainer, m_nLightcutMaxSize);
}

Col3f LightcutsIntegrator::Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
    RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
    Storage* pStorage = static_cast<Storage*>(storage.get());

    Intersection I = intersect(ray, scene, rayCounter);
    if(!I) {
        return scene.lights.Le(-ray.dir);
    }
    
    return I.Le + pStorage->m_LightcutEvalutator.evalLightcutEstimation(I, -ray.dir, scene, 
        m_fGeometryClamping, m_fRelativeErrorBound);
}

std::string LightcutsIntegrator::toString() const {
    return "LightcutIntegrator_" + m_pVPLSampler->toString();
}

}
