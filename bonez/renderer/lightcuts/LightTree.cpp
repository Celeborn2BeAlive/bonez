#include "LightTree.hpp"

namespace BnZ {

__forceinline float disimilarity(const LightTree::Cluster& A,
    const LightTree::Cluster& B, float c) {
    Vec3f diag = embree::size(embree::merge(A.bbox, B.bbox));
    Vec3f dirDiag = embree::size(embree::merge(A.directionalBBox, B.directionalBBox));
    return embree::luminance((A.I + B.I) * (embree::dot(diag, diag)
        + c * c * embree::dot(dirDiag, dirDiag)));
}

typedef LightTree::Cluster Cluster;
typedef LightTree::ClusterArray ClusterArray;
typedef std::pair<size_t, size_t> ClusterPair;

static void computeLeafs(const std::vector<OrientedVPL>& vpls,
    ClusterArray& leafs, ClusterArray& allClusters) {
    // Create all the leaf clusters corresponding to individual oriented VPLs
    const size_t clusterCount = allClusters.size();
    size_t i = 0;
    for (const OrientedVPL& vpl: vpls) {
        Cluster cluster;
        cluster.index = clusterCount - 1 - i;
        // No children :
        cluster.leftChild = -1;
        cluster.rightChild = -1;
        cluster.representativeVPL = i;
        cluster.I = vpl.L;
        cluster.bbox = BBox3f(vpl.P);
        cluster.directionalBBox = BBox3f(vpl.N);

        allClusters[cluster.index] = cluster;
        leafs[i] = cluster;
         
        ++i;
    }
}

static void computeLeafs(const std::vector<DirectionalVPL>& vpls,
    ClusterArray& leafs, ClusterArray& allClusters) {
    // Create all the leaf clusters corresponding to individual directionnal VPLs
    const size_t clusterCount = allClusters.size();
    size_t i = 0;
    for (const DirectionalVPL& vpl: vpls) {
        Cluster cluster;
        cluster.index = clusterCount - 1 - i;
        // No children :
        cluster.leftChild = -1;
        cluster.rightChild = -1;
        cluster.representativeVPL = i;
        cluster.I = vpl.L;
        cluster.bbox = BBox3f(vpl.D);
        cluster.directionalBBox = BBox3f(Vec3f(0.f));

        allClusters[cluster.index] = cluster;
        leafs[i] = cluster;
         
        ++i;
    }
}

static size_t selectBestCluster(size_t clusterIdx, const LightTree::ClusterArray& clusters) {
    float currentDisimilarity = float(embree::inf);
    size_t currentCluster = 0;
    
    for(size_t i = 0; i < clusters.size(); ++i) {
        if(i != clusterIdx) {
            float tmp = disimilarity(clusters[clusterIdx], clusters[i], 1.f);
            if(tmp < currentDisimilarity) {
                currentDisimilarity = tmp;
                currentCluster = i;
            }
        }
    }
    return currentCluster;
}

template<typename VPL>
ClusterArray locallyOrderedClustering(const std::vector<VPL>& vpls, Sampler& sampler) {
    if(vpls.empty()) {
        return ClusterArray();
    }
    
    ClusterArray clusters(2 * vpls.size() - 1);
    
    ClusterArray clusterQueue(vpls.size());
    computeLeafs(vpls, clusterQueue, clusters);
    
    size_t lastClusterIdx = clusters.size() - 1;
    size_t totalClusterCount = clusterQueue.size();

    // Pair of potential clusters to merge
    ClusterPair pair(0, selectBestCluster(0, clusterQueue));

    // At the end, only one cluster must be in the queue, it is the root of the tree
    while (clusterQueue.size() > 1) {
        size_t bestClusterCandidate = selectBestCluster(pair.second, clusterQueue);
        
        if(bestClusterCandidate == pair.first) {
            /**
             *  Choose the representative of the new cluster based on intensity
             */
            
            // Indices of the two representative VPLs
            size_t vpl_left = clusterQueue[pair.first].representativeVPL;
            size_t vpl_right = clusterQueue[pair.second].representativeVPL;
            
            // Intensity of the two representative VPLs
            Col3f I_left = vpls[vpl_left].L;
            Col3f I_right = vpls[vpl_right].L;
            
            // Luminance of the intensities
            float lum_left = embree::luminance(I_left);
            float lum_right = embree::luminance(I_right);
            
            float randomNumber = sampler.get1DSample();

            size_t chosenRepresentative;

            if (randomNumber < lum_left * embree::rcp(lum_left + lum_right)) {
                chosenRepresentative = vpl_left;
            }
            else {
                chosenRepresentative = vpl_right;
            }

            size_t leftIndex = clusterQueue[pair.first].index; // The global cluster index of the left child
            size_t rightIndex = clusterQueue[pair.second].index; // The global cluster index of the right child

            /**
             *  Remove the selected clusters from the queue
             */

            // Order of swapping with last elements of the queue
            size_t firstSwap = pair.first, secondSwap = pair.second;
            
            // If the second to be swap is already the last element, then swap it first
            // to avoid errors
            if (secondSwap == clusterQueue.size() - 1) {
                std::swap(firstSwap, secondSwap);
            }
            
            std::swap(clusterQueue[firstSwap], clusterQueue.back());
            clusterQueue.pop_back();
            std::swap(clusterQueue[secondSwap], clusterQueue.back());
            clusterQueue.pop_back();

            /**
             * Update the cluster arrays
             */

            // Add the new cluster to the global array of clusters
            clusters[lastClusterIdx - totalClusterCount] = {
                lastClusterIdx - totalClusterCount,
                (int) leftIndex,
                (int) rightIndex,
                chosenRepresentative,
                clusters[leftIndex].I + clusters[rightIndex].I,
                embree::merge(clusters[leftIndex].bbox, clusters[rightIndex].bbox),
                embree::merge(clusters[leftIndex].directionalBBox, clusters[rightIndex].directionalBBox)
            };
            // Add the new cluster to the queue of clusters to merge
            clusterQueue.push_back(clusters[lastClusterIdx - totalClusterCount]);
            ++totalClusterCount;
            
            pair.first = clusterQueue.size() - 1;
            pair.second = selectBestCluster(pair.first, clusterQueue);
        } else {
            pair.first = pair.second;
            pair.second = bestClusterCandidate;
        }
    }

    assert(totalClusterCount == lastClusterIdx);

    // Add the cluster root
    clusters[0] = clusterQueue[0];
    
    return clusters;
}

void LightTree::build(const VPLContainer& vplContainer, Sampler& sampler) {
    m_VPLContainer = &vplContainer;
    orientedClusters = locallyOrderedClustering(vplContainer.orientedVPLs, sampler);
    directionalClusters = locallyOrderedClustering(vplContainer.directionalVPLs, sampler);
}

}
