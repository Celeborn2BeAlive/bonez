#ifndef _BONEZ_LIGHTCUT_HPP_
#define _BONEZ_LIGHTCUT_HPP_

#include "LightTree.hpp"
#include "bonez/scene/shading/BRDF.hpp"

namespace BnZ {

class LightcutEvaluator {
    struct Node {
        size_t clusterIdx; // Index of the cluster corresponding to the node
        float errorUpperBound; // Upper bound on the error for this cluster and a given intersection
        Col3f vplContributionFactor; // Contribution factor of the representative vpl for this cluster
        
        __forceinline bool operator <(const Node& node) const {
            if (errorUpperBound == node.errorUpperBound) {
                return clusterIdx > node.clusterIdx; // Choose nodes that are higher in the tree in priority
            }

            return errorUpperBound < node.errorUpperBound;
        }
    };
    
    typedef std::vector<Node> NodeBuffer;
public:
    LightcutEvaluator(const LightTree& lightTree, const VPLContainer& vplContainer, size_t lightcutMaxSize):
        m_VPLContainer(vplContainer), 
        m_LightTree(lightTree), 
        m_Buffer(lightcutMaxSize) {
    }
    
    Col3f computeVPLContributionFactor(const Intersection& intersection, Vec3f wo, const Scene& scene,
        const BRDF& brdf, float geometryClamping, size_t clusterIdx);
    
    float computeErrorUpperBound(const Intersection& intersection, Vec3f wo, const Scene& scene,
        const BRDF& brdf, float geometricClamping, size_t clusterIdx) const;
    
    Col3f evalLightcutEstimation(const Intersection& intersection, Vec3f wo, const Scene& scene,
        float geometricClamping, float relativeErrorBound);
    
private:
    const VPLContainer& m_VPLContainer;
    const LightTree& m_LightTree;
    NodeBuffer m_Buffer;
};

}

#endif
