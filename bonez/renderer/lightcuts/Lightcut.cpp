#include "Lightcut.hpp"
#include "bonez/scene/shading/BRDF.hpp"

namespace BnZ {

float LightcutEvaluator::computeErrorUpperBound(const Intersection& intersection, Vec3f wo, const Scene& scene,
    const BRDF& brdf, float geometryClamping, size_t clusterIdx) const {
    if(clusterIdx < m_LightTree.orientedClusters.size()) {
        // Oriented VPL Cluster
        
        const LightTree::Cluster& cluster = m_LightTree.orientedClusters[clusterIdx];
        
        // Leaf, no error
        if (cluster.leftChild == -1) {
            return 0.f;
        }

        float MUpperBound = embree::luminance(brdf.upperBound(wo, cluster.bbox));
        // TODO ADD COSINE BOUNDING

        Vec3f d = embree::max(Vec3f(embree::zero), 
            embree::max(cluster.bbox.lower - intersection.P, intersection.P - cluster.bbox.upper));
        float minSqrDist = embree::dot(d, d);

        if (minSqrDist == 0) {
            minSqrDist = float(embree::ulp);
        }

        // ADD COSINUS BOUNDING
        float GUpperBound = embree::min(geometryClamping, embree::rcp(minSqrDist));

        return MUpperBound * GUpperBound * embree::luminance(cluster.I);
    } else {
        // Directional VPL Cluster
        clusterIdx -= m_LightTree.orientedClusters.size();
        
        const LightTree::Cluster& cluster = m_LightTree.directionalClusters[clusterIdx];
        
        // Leaf, no error
        if (cluster.leftChild == -1) {
            return 0.f;
        }

        float MUpperBound = embree::luminance(brdf.upperBound(wo));
        // TODO ADD COSINE BOUNDING

        return MUpperBound * embree::luminance(cluster.I);
    }
}

Col3f LightcutEvaluator::computeVPLContributionFactor(const Intersection& intersection, Vec3f wo, const Scene& scene,
    const BRDF& brdf, float geometryClamping, size_t clusterIdx) {
    if(clusterIdx < m_LightTree.orientedClusters.size()) {
        // Oriented VPL Cluster
        
        const OrientedVPL& vpl = m_VPLContainer.orientedVPLs[
            m_LightTree.orientedClusters[clusterIdx].representativeVPL];
    
        Vec3f wi = vpl.P - intersection.P;
        float distance = embree::length(wi);
        
        wi /= distance;
        
        Col3f reflectionFactor = brdf.eval(wi, wo);
        if(reflectionFactor == Col3f(embree::zero)) {
            return Col3f(0.f);
        }
        
        float cos_i = embree::dot(intersection.Ns, wi);
        if(cos_i <= 0.f) {
            return Col3f(0.f);
        }
        
        float cos_o = embree::dot(vpl.N, -wi);
        
        if(cos_o <= 0.f) {
            return Col3f(0.f);
        }
        
        float geometryFactor = embree::min(geometryClamping, 
            cos_o * embree::rcp(embree::sqr(distance)));
        
        if(scene.occluded(shadow_ray(intersection, wi, distance))) {
            return Col3f(0.f);
        }
        
        return reflectionFactor * cos_i * geometryFactor;
        
    }
    else {
        // Directional VPL Cluster
        clusterIdx -= m_LightTree.orientedClusters.size();
        
        const DirectionalVPL& vpl = m_VPLContainer.directionalVPLs[
            m_LightTree.directionalClusters[clusterIdx].representativeVPL];
        
        Vec3f wi = -vpl.D;
                
        Col3f reflectionFactor = brdf.eval(wi, wo);
        if(reflectionFactor == Col3f(embree::zero)) {
            return Col3f(0.f);
        }
        
        float cos_i = embree::dot(intersection.Ns, wi);
        if(cos_i <= 0.f) {
            return Col3f(0.f);
        }
        
        if(scene.occluded(shadow_ray(intersection, wi, embree::inf))) {
            return Col3f(0.f);
        }
        
        return reflectionFactor * cos_i;
    }
}

Col3f LightcutEvaluator::evalLightcutEstimation(const Intersection& intersection, Vec3f wo, const Scene& scene, 
    float geometricClamping, float relativeErrorBound) {
    typedef NodeBuffer::iterator LightcutIterator;
    BRDF brdf = scene.shade(intersection);
    
    const LightTree::ClusterArray& orientedClusters = m_LightTree.orientedClusters;
    const LightTree::ClusterArray& directionalClusters = m_LightTree.directionalClusters;
    
    size_t lightCutSize = 0;
    Col3f totalEstimation(0.f);
    
    if(!orientedClusters.empty()) {
        m_Buffer[lightCutSize].clusterIdx = 0;
        m_Buffer[lightCutSize].errorUpperBound = 
            computeErrorUpperBound(intersection, wo, scene, brdf, geometricClamping, 
               0);
        m_Buffer[lightCutSize].vplContributionFactor = computeVPLContributionFactor(intersection, wo, 
            scene, brdf, geometricClamping, 0);
        
        totalEstimation += m_Buffer[lightCutSize].vplContributionFactor * orientedClusters[0].I;
        ++lightCutSize;
    }
    
    if(!directionalClusters.empty()) {
        m_Buffer[lightCutSize].clusterIdx = orientedClusters.size();
        m_Buffer[lightCutSize].errorUpperBound = 
            computeErrorUpperBound(intersection, wo, scene, brdf, geometricClamping, 
               orientedClusters.size());
        m_Buffer[lightCutSize].vplContributionFactor = computeVPLContributionFactor(intersection, wo, 
            scene, brdf, geometricClamping, orientedClusters.size());
        
        totalEstimation += m_Buffer[lightCutSize].vplContributionFactor * directionalClusters[0].I;
        ++lightCutSize;
    }
    
    LightcutIterator begin = std::begin(m_Buffer);

    std::make_heap(begin, begin + lightCutSize);

    while (relativeErrorBound * embree::luminance(totalEstimation) < begin->errorUpperBound 
        && lightCutSize < m_Buffer.size()) {
        
        // Get the node of the cut with the highest upper bound and remove it from the queue
        Node node = *begin;
        std::pop_heap(begin, begin + lightCutSize);
        --lightCutSize;

        const LightTree::Cluster* clusters = m_LightTree.orientedClusters.data();
        size_t offset = 0;

        if(node.clusterIdx >= m_LightTree.orientedClusters.size()) {
            offset = m_LightTree.orientedClusters.size();
            node.clusterIdx -= offset;
            clusters = m_LightTree.directionalClusters.data();
        }

        // If it's not a leaf (individual VPL), refines the cut by substituing it with it's two children
        if (clusters[node.clusterIdx].leftChild != -1) {
            // Substract etimation due to this node
            totalEstimation -= node.vplContributionFactor * clusters[node.clusterIdx].I;
            
            // Refine the lightcut
            Node left, right;
            left.clusterIdx = clusters[node.clusterIdx].leftChild + offset;
            right.clusterIdx = clusters[node.clusterIdx].rightChild + offset;
            
            left.errorUpperBound = computeErrorUpperBound(intersection, wo, scene, brdf, geometricClamping, 
                left.clusterIdx);
            right.errorUpperBound = computeErrorUpperBound(intersection, wo, scene, brdf, geometricClamping, 
                right.clusterIdx);

            if(clusters[node.clusterIdx].representativeVPL == clusters[left.clusterIdx - offset].representativeVPL) {
                left.vplContributionFactor = node.vplContributionFactor;
                right.vplContributionFactor = computeVPLContributionFactor(intersection, wo, scene, brdf, 
                    geometricClamping, right.clusterIdx);
            } else {
                left.vplContributionFactor = computeVPLContributionFactor(intersection, wo, scene, brdf, 
                    geometricClamping, left.clusterIdx);
                right.vplContributionFactor = node.vplContributionFactor;
            }

            // Compute the new total estimation
            totalEstimation += left.vplContributionFactor * clusters[left.clusterIdx - offset].I
                + right.vplContributionFactor * clusters[right.clusterIdx - offset].I;
            
            // Add the new nodes to the queue
            m_Buffer[lightCutSize] = left;
            ++lightCutSize;
            std::push_heap(begin, begin + lightCutSize);
            m_Buffer[lightCutSize] = right;
            ++lightCutSize;
            std::push_heap(begin, begin + lightCutSize);
        }
    }
    
    return totalEstimation;
}

}
