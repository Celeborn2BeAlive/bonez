#ifndef _BONEZ_LIGHTTREE_HPP_
#define _BONEZ_LIGHTTREE_HPP_

#include <vector>

#include "bonez/common/sampling/Sampler.hpp"
#include "bonez/scene/lights/vpls.hpp"

namespace BnZ {

class LightTree {
public:
    struct Cluster {
        size_t index; // Used to retrieve the absolute index of a cluster during construction of the tree
        int leftChild; // Index of the left child, -1 if the cluster is a leaf
        int rightChild; // Index of the right child, -1 if the cluster is a leaf
        size_t representativeVPL; // Index of the representative VPL
        Col3f I; // Total intensity of the cluster
        BBox3f bbox; // Bounding box of the cluster
        //BCone bcone; // Bounding cone for the directions or the cluster
        
        BBox3f directionalBBox; // A bbox that containts the maximum emission directions of the cluster
        
        Cluster() {
            // do nothing
        }
        
        Cluster(size_t index, int leftChild, int rightChild, size_t representativeVPL,
            Col3f I, BBox3f bbox, BBox3f directionalBBox):
            index(index), leftChild(leftChild), rightChild(rightChild), representativeVPL(representativeVPL),
            I(I), bbox(bbox), directionalBBox(directionalBBox) {
        }
    };
    
    typedef std::vector<Cluster> ClusterArray;
    
    LightTree():
        m_VPLContainer(nullptr) {
    }
    
    void build(const VPLContainer& vplContainer, Sampler& sampler);

    ClusterArray orientedClusters;
    ClusterArray directionalClusters;
    
private:
    const VPLContainer* m_VPLContainer;
};

}

#endif
