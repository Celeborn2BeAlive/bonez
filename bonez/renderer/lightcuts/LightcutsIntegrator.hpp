#ifndef _BONEZ_LIGHTCUTSINTEGRATOR_HPP_
#define _BONEZ_LIGHTCUTSINTEGRATOR_HPP_

#include <memory>

#include "bonez/common/common.hpp"
#include "bonez/renderer/Integrator.hpp"

#include "bonez/renderer/instantradiosity/VPLSampler.hpp"

#include "LightTree.hpp"
#include "Lightcut.hpp"

namespace BnZ {

class LightcutsIntegrator: public Integrator {
public:
    struct Storage: public Integrator::Storage {
        LightcutEvaluator m_LightcutEvalutator;
        
        Storage(const LightTree& lightTree, const VPLContainer& vplContainer, size_t lightCutMaxSize):
            m_LightcutEvalutator(lightTree, vplContainer, lightCutMaxSize) {
        }
    };

    LightcutsIntegrator(size_t lightCutMaxSize, float geometryClamping, float relativeErrorBound,
        const VPLSamplerPtr& vplSampler);

    virtual void preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
        size_t frameIdx, ParamSet& statistics);

    virtual StoragePtr getStorage() const;

    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const Integrator::StoragePtr& storage) const;
    
    virtual std::string toString() const;

    virtual std::string getType() const {
        return "LightcutsIntegrator";
    }

private:
    size_t m_nLightcutMaxSize;
    float m_fGeometryClamping; // Clamping factor for the geometry term
    float m_fRelativeErrorBound;

    VPLSamplerPtr m_pVPLSampler;
    
    VPLContainer m_VPLContainer;
    LightTree m_LightTree;
};

}

#endif
