#ifndef _BONEZ_POINTHEMISPHERERENDERER_HPP
#define _BONEZ_POINTHEMISPHERERENDERER_HPP

#include "bonez/common/common.hpp"
#include "bonez/common/cameras/ParaboloidCamera.hpp"
#include "bonez/scene/shading/BRDF.hpp"
#include "Integrator.hpp"
#include "bonez/common/sampling/StratifiedSampler.hpp"

namespace BnZ {

/**
 * @brief The PointHemisphereRenderer class is used to analyse a sampling strategy
 * and compare it with the function to sample.
 */
class PointHemisphereRenderer
{
public:
#if BONEZ_MULTITHREADING_ENABLED
    embree::Atomic m_nTileID;
#else
    size_t m_nTileID;
#endif
    static const size_t TILE_SIZE = 32; // A thread process a paquet of TILE_SIZE pixels

    size_t m_nTileX, m_nTileY;

    const Scene* m_pScene;

    typedef std::function<float (const Vec3f&)> PDF;

    ParaboloidCamera m_PointCamera; // The camera for the hemisphere of the point
    BRDF m_BRDF; // Contains the brdf and the point
    Vec3f m_Wi; // Incident direction

    // The integrator is used to estimate the function
    IntegratorPtr m_pIntegrator;

    // The sampling function that we want to analyse
    PDF m_PDF;

    size_t m_nXResolution, m_nYResolution;
    Framebuffer m_FunctionFramebuffer; // Contains the values of m_BRDF * cos * m_pIntegrator->Li
    Framebuffer m_PdfFramebuffer; // Contains the values of m_PDF
    Framebuffer m_DivisionFramebuffer; // Contains the values of the division of the two others

    StratifiedSampler m_Sampler;

    size_t m_nFrameCount;

    void renderThread(size_t threadID) {
        size_t tileCount = m_nTileX * m_nTileY;
        embree::Vec2f rcpDimensions(embree::rcp(embree::Vec2f(m_nXResolution, m_nYResolution)));

        Integrator::StoragePtr storage = m_pIntegrator->getStorage();
        IntegratorSamplerPtr sampler = m_Sampler.clone();
        IntegratorSample sample;

        RayCounter rayCounter;

        while (true) {
            size_t tileID = m_nTileID++;

            if (tileID >= tileCount) {
                break;
            }

            size_t tileX = tileID % m_nTileX;
            size_t tileY = (tileID - tileX) / m_nTileX;

            size_t xstart = embree::min(tileX * TILE_SIZE, m_nXResolution);
            size_t xend = embree::min(xstart + TILE_SIZE, m_nXResolution);
            size_t ystart = embree::min(tileY * TILE_SIZE, m_nYResolution);
            size_t yend = embree::min(ystart + TILE_SIZE, m_nYResolution);

            sampler->init(m_nFrameCount, xstart, ystart, xend, yend);

            while(sampler->getNextSample(sample)) {
                Ray wo = m_PointCamera.ray(sample);
                Col3f value = m_pIntegrator->Li(wo, *m_pScene, sample, rayCounter, storage);

                Vec2i pixel = sample.getPixel();
                pixel = Vec2i(embree::clamp(pixel.x, 0, (int) m_nXResolution - 1),
                              embree::clamp(pixel.y, 0, (int) m_nYResolution - 1));

                float pdf = m_PDF(wo.dir);
                m_PdfFramebuffer.accumulate(pixel, Col3f(pdf));
                if(value != Col3f(0.f)) {
                    value *= m_BRDF.eval(m_Wi, wo.dir) * embree::dot(wo.dir, m_BRDF.P.Ns);
                    m_FunctionFramebuffer.accumulate(pixel, value);
                    m_DivisionFramebuffer.accumulate(pixel, value / pdf);
                }
            }

        }
    }

    PointHemisphereRenderer(size_t xResolution, size_t yResolution):
        m_nXResolution(xResolution), m_nYResolution(yResolution),
        m_FunctionFramebuffer(xResolution, yResolution),
        m_PdfFramebuffer(xResolution, yResolution),
        m_DivisionFramebuffer(xResolution, yResolution),
        m_Sampler(1, 1),
        m_nFrameCount(0) {

        m_nTileX = (m_nXResolution + TILE_SIZE - 1) / TILE_SIZE;
        m_nTileY = (m_nYResolution + TILE_SIZE - 1) / TILE_SIZE;
    }

    void clear() {
        m_FunctionFramebuffer.clear();
        m_PdfFramebuffer.clear();
        m_DivisionFramebuffer.clear();
    }

    template<typename PDFFunctor>
    void init(const SurfacePoint& point, const Vec3f& wi,
              const IntegratorPtr& integrator, PDFFunctor pdf) {
        m_BRDF = BRDF(point);
        m_PointCamera = ParaboloidCamera(point.P, point.Ns, m_nXResolution, m_nYResolution);
        m_Wi = wi;
        clear();
        m_nFrameCount = 0;

        m_pIntegrator = integrator;
        m_pIntegrator->requestSamples(m_Sampler);

        m_PDF = pdf;
    }

    void render(const Scene& scene) {
        m_nTileID = 0;

        ParamSet stats;
        m_pIntegrator->preprocess(m_PointCamera, scene, m_FunctionFramebuffer, m_nFrameCount, stats);

        m_pScene = &scene;

    #if BONEZ_MULTITHREADING_ENABLED
        embree::scheduler->start();
        embree::scheduler->addTask(embree::TaskScheduler::ThreadInfo(), embree::TaskScheduler::GLOBAL_FRONT,
                                   (embree::TaskScheduler::runFunction) &runRenderThread, this, embree::scheduler->getNumThreads(),
                                   nullptr, nullptr, "render::tile");
        embree::scheduler->stop();
    #else
        renderThread(0);
    #endif

        ++m_nFrameCount;
    }

    static void runRenderThread(const embree::TaskScheduler::ThreadInfo& thread,
                                PointHemisphereRenderer* self, size_t threadID) {
        self->renderThread(threadID);
    }
};

}

#endif // _BONEZ_POINTHEMISPHERERENDERER_HPP
