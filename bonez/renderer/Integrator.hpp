#ifndef _BONEZ_API_INTEGRATOR_HPP_
#define _BONEZ_API_INTEGRATOR_HPP_

#include <memory>

#include "bonez/common/common.hpp"
#include "bonez/common/cameras/Camera.hpp"
#include "Framebuffer.hpp"
#include "bonez/scene/Scene.hpp"
#include "bonez/common/sampling/IntegratorSampler.hpp"
#include "bonez/common/Params.hpp"

namespace BnZ {

class Integrator;

/**
 * A interface for classes that implements various type of integration of
 * light.
 */
class Integrator {
public:
    class Storage {
    public:
        virtual ~Storage() {
        }
    };
    
    typedef std::shared_ptr<Storage> StoragePtr;

    virtual ~Integrator() {
    }
    
    virtual void requestSamples(IntegratorSampler& sampler) {
    }
    
    virtual void preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
        size_t frameIdx, ParamSet& statistics) {
    }
    
    virtual StoragePtr getStorage() const {
        // By default no storage is used
        return nullptr;
    }
    
    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter, const StoragePtr& storage) const = 0;
    
    virtual std::string toString() const = 0;
    
    virtual std::string getType() const = 0;

    virtual void setParam(const std::string& name, const ParamPtr& value) {
    }
};

typedef std::shared_ptr<Integrator> IntegratorPtr;

}

#endif
