#ifndef _BONEZ_API_INTEGRATORRENDERER_HPP_
#define _BONEZ_API_INTEGRATORRENDERER_HPP_

#include "bonez/common/common.hpp"
#include "Renderer.hpp"
#include "Integrator.hpp"
#include "bonez/common/Progress.hpp"
#include "bonez/common/sampling/IntegratorSampler.hpp"

namespace BnZ {

class IntegratorRenderer: public Renderer {
public:
    IntegratorPtr m_pIntegrator;
    IntegratorSamplerPtr m_pSampler;
    
    static void runRenderThread(const embree::TaskScheduler::ThreadInfo& thread,
                                IntegratorRenderer* self, size_t threadID) {
        self->renderThread(threadID);
    }

#if BONEZ_MULTITHREADING_ENABLED
    embree::Atomic m_nTileID;
#else
    size_t m_nTileID;
#endif

    static const size_t TILE_SIZE = 32; // A thread process a paquet of TILE_SIZE pixels

    ThreadSafeProgress m_Progress;

    const Camera* m_pCamera; // The camera seeing the scene
    const Scene* m_pScene; // The scene to render
    Framebuffer* m_pFramebuffer; // The framebuffer where to draw the image

    size_t m_nFrameIdx; // The current frame

    bool m_bShowProgress; // A boolean indicating if the rendering status must be shown on console

    size_t m_nTileX, m_nTileY;

    Col3f m_SumLi;
    size_t m_nSampleCount;
    embree::MutexSys m_StatsMutex;

    // This function is called by each rendering thread. It loops over the tiles to render until each one is
    // done.
    void renderThread(size_t threadID);

public:
    IntegratorRenderer(const IntegratorPtr& integrator, const IntegratorSamplerPtr& sampler):
        m_pIntegrator(integrator), m_pSampler(sampler), m_nFrameIdx(0) {
        m_pIntegrator->requestSamples(*m_pSampler);
    }
    
    virtual void render(const Camera& camera, const Scene& scene, 
        Framebuffer& framebuffer, bool showProgress, ParamSet* statistics);
    
    virtual bool getVPLs(const Camera& camera, const Scene& scene, 
        const Framebuffer& framebuffer, VPLContainer& vpls);
    
    virtual std::string getRenderString() const;
    
    virtual std::string getType() const;

    virtual void setParam(const std::string& name, const ParamPtr& value) {
        m_pIntegrator->setParam(name, value);
    }
};

}

#endif
