#ifndef _BONEZ_API_POSTPROCESS_HPP_
#define _BONEZ_API_POSTPROCESS_HPP_

#include <memory>
#include "Framebuffer.hpp"

namespace BnZ {

class PostProcess {
public:
    virtual ~PostProcess() {
    }

    // Apply the post process on a framebuffer
    virtual void apply(const Framebuffer& framebuffer, Framebuffer& result) const = 0;
};

typedef std::shared_ptr<PostProcess> PostProcessPtr;

}

#endif
