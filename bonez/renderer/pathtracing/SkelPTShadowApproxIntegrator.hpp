#ifndef _BONEZ_SKELPTSHADOWAPPROXINTEGRATOR_HPP
#define _BONEZ_SKELPTSHADOWAPPROXINTEGRATOR_HPP

#include "bonez/renderer/Integrator.hpp"

namespace BnZ {

class SkelPTShadowApproxIntegrator : public Integrator
{
public:
    SkelPTShadowApproxIntegrator(size_t maxDepth, bool indirectOnly):
        m_nMaxDepth(maxDepth), m_bIndirectOnly(indirectOnly) {
    }

    void requestSamples(IntegratorSampler& sampler);

    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample, RayCounter& rayCounter,
        const Integrator::StoragePtr& storage) const;

    Col3f Lr(Intersection intersection, const Vec3f& wo, const Scene& scene, size_t depth,
        const IntegratorSample& sample, RayCounter& rayCounter) const;

    virtual std::string toString() const;

    virtual std::string getType() const {
        return "SkelPTShadowApproxIntegrator";
    }

    size_t m_nMaxDepth;
    bool m_bIndirectOnly;

    size_t m_nBRDFSampleSet;
    size_t m_nLight1DSampleSet;
    size_t m_nLight2DSampleSet;
};

}

#endif // _BONEZ_SKELPTSHADOWAPPROXINTEGRATOR_HPP
