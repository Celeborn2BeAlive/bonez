#include <iostream>
#include <sstream>
#include <iomanip>

#include "SkelMISPathtraceIntegrator.hpp"

#include "bonez/scene/lights/vpls.hpp"

#include "bonez/common/sampling/Sampler.hpp"

#include "bonez/renderer/radiometry.hpp"

namespace BnZ {

std::string SkelMISPathtraceIntegrator::toString() const {
    std::stringstream ss;
    ss << "SkelMISPathtraceIntegrator_" <<
        "maxDepth=" << std::setfill('0') << std::setw(3) << m_nMaxDepth <<
        "_skelStrength=" << m_fSkelStrength <<
        "_skelProb=" << m_fSkelProb;
    if(m_bIndirectOnly) {
        ss << "_indirectOnly";
    }
    return ss.str();
}

void SkelMISPathtraceIntegrator::requestSamples(IntegratorSampler& sampler) {
    size_t count = sampler.roundSampleCount(m_nMaxDepth - 1);
    m_nBRDFSampleSet = sampler.request2DSamples(count);
    m_nMISSampleSet = sampler.request1DSamples(count);
    m_nLight1DSampleSet = sampler.request1DSamples(count);
    m_nLight2DSampleSet = sampler.request2DSamples(count);
}

void SkelMISPathtraceIntegrator::preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
    size_t frameIdx, ParamSet& statistics) {
    Sampler sampler(frameIdx);

    LightPointRandom random(sampler.get1DSample(), sampler.get1DSample(), sampler.get2DSample());
    Sample3f P = scene.lights.samplePoint(scene, random);
    
    GraphNodeIndex node = scene.voxelSpace->getSkeleton().getNearestNode(P.value);
    if(node == UNDEFINED_NODE) {
        std::cerr << "Error in SkelPathtraceIntegrator: no node for the light source" << std::endl;
        return;
    }
    
    m_ImportancePoints.compute(scene.voxelSpace->getSkeleton().getGraph(), scene, P.value, node,
        [&scene](GraphNodeIndex node) -> Vec3f {
            return scene.voxelSpace->getSkeleton()[node].getPosition();
        }
    );
}

Col3f SkelMISPathtraceIntegrator::Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample, 
    RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
    Intersection intersection = intersect(ray, scene, rayCounter);

    if (!intersection) {
        return scene.lights.Le(-ray.dir);
    }

    // First intersection: Add Le for handling 1 length path and compute reflected radiance
    return intersection.Le + Lr(intersection, -ray.dir, scene, 1, sample, rayCounter);
}

Col3f SkelMISPathtraceIntegrator::Lr(Intersection intersection, const Vec3f& wo, const Scene& scene, size_t depth,
    const IntegratorSample& sample, RayCounter& rayCounter) const {
    if (depth == m_nMaxDepth) {
        // End of recursion
        return embree::zero;
    }
    
    BRDF brdf = scene.shade(intersection);

    // Compute direct illumination (paths of length m_nMaxDepth - count + 1)
    Col3f L(embree::zero);

    if(depth != 1 || !m_bIndirectOnly) {
        IncidentRayRandom random(sample.getRandomFloat(), sample.getRandomFloat(), sample.getRandomVec2f());
        L += directIllumination(wo, brdf, scene, random, rayCounter);
    }

    if (depth + 1 < m_nMaxDepth) {
        Sample3f wi;

        Col3f brdfValue;
        
        GraphNodeIndex node = scene.voxelSpace->getSkeleton().getNearestNode(intersection);
        
        if(node == UNDEFINED_NODE || !m_ImportancePoints.hasImportancePoint(node)) {
            brdfValue = brdf.sample(wo, wi, sample.get2DSample(m_nBRDFSampleSet, depth - 1));
        } else {
            Vec3f importancePoint = m_ImportancePoints[node];
            
            Vec3f importanceDirection = embree::normalize(importancePoint - intersection.P);
            if(embree::dot(importanceDirection, intersection.Ns) < 0) {
                brdfValue = brdf.sample(wo, wi, sample.get2DSample(m_nBRDFSampleSet, depth - 1));
            } else {
                // Multiple importance sampling on the skeleton sampling strategy and the BRDF sampling strategy
                float sample1D = sample.get1DSample(m_nMISSampleSet, depth - 1);
                Vec2f ss = sample.get2DSample(m_nBRDFSampleSet, depth - 1);
                
                if(sample1D < m_fSkelProb) {
                    // Sample with the skeleton strategy
                    wi = powerCosineSampleHemisphere(ss.x, ss.y, importanceDirection, m_fSkelStrength);
                    if(embree::dot(wi.value, intersection.Ns) < 0.f) {
                        brdfValue = Col3f(0.f); 
                    } else {
                        brdfValue = brdf.eval(wi.value, wo);
                        // Balance heuristic
                        wi.pdf = m_fSkelProb * wi.pdf + (1 - m_fSkelProb) * brdf.pdf(wo, wi.value);
                    }
                    
                } else {
                    // Sample with the brdf strategy
                    brdfValue = brdf.sample(wo, wi, ss);
                    
                    // Balance heuristic
                    wi.pdf = (1 - m_fSkelProb) * wi.pdf + m_fSkelProb * powerCosineSampleHemispherePDF(wi.value, importanceDirection, m_fSkelStrength);
                }
            }
        }

        if (wi.pdf == 0.f || brdfValue == Col3f(embree::zero)) {
            return L;
        }

        Intersection I;
        if (!(I = bounce(intersection, wi.value, scene, rayCounter))) {
            return L;
        }

        // Indirect illumination
        L += brdfValue * embree::dot(wi.value, intersection.Ns) * embree::rcp(wi.pdf) * Lr(I, -wi.value, scene, depth + 1, sample, rayCounter);
    }

    return L;
}

}
