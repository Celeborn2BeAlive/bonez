#ifndef _BONEZ_PATH_HPP_
#define _BONEZ_PATH_HPP_

namespace BnZ {

class PathEmitter {
public:
    enum Type {
        DIRECTIONAL, ORIENTED
    }
    
    virtual ~PathEmitter() {
    }
    
    virtual Type type() const = 0;
};

class DirectionalPathEmitter {
public:
    Vec3f D;
    Col3f Le;
    float pdf;
    
    Light* light;
    
    virtual Type type() const {
        return DIRECTIONAL;
    }
};

class OrientedPathEmitter {
public:
    SurfacePoint P;
    Col3f Le;
    float pdf;
    
    virtual Type type() const {
        return ORIENTED;
    }
};

class PathVertex {
public:
    Vec3f wi; //! Incident direction of the vertex
    Intersection I; //! Intersection corresponding to the vertex
    float pdf; //! pdf of the vertex
};

}

#endif
