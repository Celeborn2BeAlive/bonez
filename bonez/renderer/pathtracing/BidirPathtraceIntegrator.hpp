#pragma once

#include "bonez/renderer/Integrator.hpp"
#include "bonez/scene/shading/BRDF.hpp"

namespace BnZ {

class BidirPathtraceIntegrator: public Integrator {
public:
    struct Vertex {
        Intersection I;
        BRDF brdf;
        Vec3f wi;
        Col3f contrib;
        
        Vertex() {
        }
        
        Vertex(const Intersection& I, const BRDF& brdf, const Vec3f& wi, const Col3f& contrib):
            I(I), brdf(brdf), wi(wi), contrib(contrib) {
        }
    };
    
    typedef std::vector<Vertex> Path;
    
    struct PathsStorage: public Integrator::Storage {
        Path lightPath;
        Path eyePath;
        
        PathsStorage(size_t maxLightDepth, size_t maxEyeDepth) {
            lightPath.reserve(maxLightDepth);
            eyePath.reserve(maxEyeDepth);
        }
        
        void clear() {
            lightPath.clear();
            eyePath.clear();
        }
    };

    BidirPathtraceIntegrator(size_t maxDepth, bool indirectOnly);
    
    virtual void preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
        size_t frameIdx, ParamSet& statistics);
    
    virtual void requestSamples(IntegratorSampler& sampler);
    
    virtual StoragePtr getStorage() const {
        return std::make_shared<PathsStorage>(m_nMaxLightDepth, m_nMaxEyeDepth);
    }
    
    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample, RayCounter& rayCounter,
        const Integrator::StoragePtr& storage) const;
    
    void sampleLightPath(Path& path, const Scene& scene, const IntegratorSample& sample, 
        RayCounter& rayCounter) const;
    
    Col3f sampleEyePath(Path& path, const Ray& ray, const Scene& scene, const IntegratorSample& sample,
        RayCounter& rayCounter) const;
    
    virtual std::string toString() const;
    
    virtual std::string getType() const {
        return "BidirPathtraceIntegrator";
    }
    
    size_t m_nMaxDepth;
    
    size_t m_nMaxEyeDepth;
    size_t m_nMaxLightDepth;
    
    bool m_bIndirectOnly;
    
    size_t m_nBRDFSampleSet;
    size_t m_nLight1DSampleSet;
    size_t m_nLight2DSampleSet;
    
    std::vector<float> m_Weights;
};

}
