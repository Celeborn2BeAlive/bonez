#include "BidirPathtraceIntegrator.hpp"
#include "bonez/renderer/radiometry.hpp"

#include <sstream>

namespace BnZ {

BidirPathtraceIntegrator::BidirPathtraceIntegrator(size_t maxDepth, bool indirectOnly):
    m_nMaxDepth(maxDepth),
    m_nMaxEyeDepth(maxDepth - 1),
    m_nMaxLightDepth(maxDepth - 2),
    m_bIndirectOnly(indirectOnly) {
}

void BidirPathtraceIntegrator::requestSamples(IntegratorSampler& sampler) {
    
}

void BidirPathtraceIntegrator::preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
    size_t frameIdx, ParamSet& statistics) {
    // Precomputation of the weights associated with each path sample
    // The weight m_Weight[k] is one over the number of paths of length k
    // that can be built with a light path of length m_nMaxLightDepth and an eye path of length m_nMaxEyeDepth
    m_Weights.resize(m_nMaxEyeDepth + m_nMaxLightDepth + 1, 0.f);
    for(size_t e = 0; e < m_nMaxEyeDepth; ++e) {
        m_Weights[e] = 1.f / (embree::min(m_nMaxLightDepth, e) + 1);
        
        for(size_t l = 0; l < m_nMaxLightDepth; ++l) {
            m_Weights[l + e + 1] = 
                1.f / (embree::min(l + 1, m_nMaxEyeDepth - 1 - e) + embree::min(m_nMaxLightDepth - (l + 1), e) + 1);
        }
    }
}

Col3f BidirPathtraceIntegrator::Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample, RayCounter& rayCounter,
    const Integrator::StoragePtr& storage) const {
    PathsStorage* paths = static_cast<PathsStorage*>(storage.get());
    paths->clear();
    
    sampleLightPath(paths->lightPath, scene, sample, rayCounter);
    Col3f Le = sampleEyePath(paths->eyePath, ray, scene, sample, rayCounter);
    
    // No intersection
    if(paths->eyePath.empty()) {
        return scene.lights.Le(-ray.dir);
    }
    
    // An optional depth filter used to display only the image for path of a certain depth
    size_t depthFilter = 0;
    
    Col3f L(Le); // Emission of the intersection point
    
    // Accumulate the contribution of all subpaths
    for(size_t e = 0; e < paths->eyePath.size(); ++e) {
        // The direct illumination at a vertex is computed directly
        if(!depthFilter || (e + 2) == depthFilter) {
            if((e + 2) <= m_nMaxDepth && (e > 0 || !m_bIndirectOnly)) {
                IncidentRayRandom random(sample.getRandomFloat(), sample.getRandomFloat(), sample.getRandomVec2f());
                L += m_Weights[e] * paths->eyePath[e].contrib * 
                    directIllumination(paths->eyePath[e].wi, paths->eyePath[e].brdf, scene, random, rayCounter);
            }
        }
        
        // Indirect illumination is computed with the vertices of the light path
        for(size_t l = 0; l < paths->lightPath.size(); ++l) {
            const Vertex& vl = paths->lightPath[l];
            const Vertex& ve = paths->eyePath[e];
            Vec3f w = vl.brdf.P.P - ve.brdf.P.P;
            float d = embree::length(w);
            w /= d;
            float G = embree::max(0.f, embree::dot(vl.brdf.P.Ns, -w)) * 
                embree::max(0.f, embree::dot(ve.brdf.P.Ns, w));

            if(G == 0.f) {
                continue;
            }
            G /= (d * d);
            
            if(occluded(shadow_ray(ve.I, w, d), scene, rayCounter)) {
                continue;
            }
            
            size_t depth = l + e + 3;
            if(depth <= m_nMaxDepth && (!depthFilter || depth == depthFilter)) {
                L += m_Weights[l + e + 1] * (vl.contrib * vl.brdf.eval(vl.wi, -w) * G * ve.brdf.eval(w, ve.wi) * ve.contrib);
            }
        }
    }

    return L;
}

static bool expand(BidirPathtraceIntegrator::Vertex& v, const Vec2f& dirSample, const Scene& scene) {
    Sample3f wo;
    Col3f reflect = v.brdf.sample(v.wi, wo, dirSample);

    if(reflect == Col3f(0.f) || wo.pdf == 0.f) {
        return false;
    }
    
    reflect *= embree::max(0.f, embree::dot(v.I.Ns, wo.value)) / wo.pdf;
    
    Intersection I = bounce(v.I, wo.value, scene);

    if(!I) {
        return false;
    }
    
    v = BidirPathtraceIntegrator::Vertex(I, BRDF(I), -wo.value, v.contrib * reflect);
    return true;
}

void BidirPathtraceIntegrator::sampleLightPath(Path& path, const Scene& scene, const IntegratorSample& sample, 
    RayCounter& rayCounter) const {
    if(!m_nMaxLightDepth) {
        return;
    }
    
    // Sample a ray that emits light
    ExitantRayRandom random(sample.getRandomFloat(), sample.getRandomFloat(), sample.getRandomVec2f(), sample.getRandomVec2f());
    RaySample raySample;
    Col3f L = scene.lights.sampleExitantRay(scene, random, raySample);
    
    if(L == Col3f(0.f) || raySample.pdf == 0.f) {
        return;
    }
    
    // Find intersection with the scene
    Intersection I = intersect(raySample.value, scene, rayCounter);
    if(!I) {
        return;
    }
    
    // Create a vertex for that intersection. It correspond to a 1-depth vertex along the light path
    Vertex v(I, BRDF(I), -raySample.value.dir, L / raySample.pdf);
    path.push_back(v);
    
    // Expand the path until it gains its max depth
    size_t rest = m_nMaxLightDepth - 1;
    while(rest && expand(v, sample.getRandomVec2f(), scene)) {
        path.push_back(v);
        --rest;
    }
}
    
Col3f BidirPathtraceIntegrator::sampleEyePath(Path& path, const Ray& viewRay, const Scene& scene, const IntegratorSample& sample,
    RayCounter& rayCounter) const {
    // Compute the first intersection along the viewRay
    Intersection I = intersect(viewRay, scene, rayCounter);
    if(!I) {
        return scene.lights.Le(-viewRay.dir);
    }
    
    // Create a vertex for that intersection. It correspond to a 1-depth vertex along the eye path
    Vertex v(I, BRDF(I), -viewRay.dir, Col3f(1.f));
    path.push_back(v);
    
    // Expand the path until it gains its max depth
    size_t rest = m_nMaxEyeDepth - 1;
    while(rest && expand(v, sample.getRandomVec2f(), scene)) {
        path.push_back(v);
        --rest;
    }
    
    return I.Le;
}
    
std::string BidirPathtraceIntegrator::toString() const {
    std::stringstream ss;
    ss << "BidirPathtraceIntegrator_maxDepth=" << m_nMaxDepth;
    if(m_bIndirectOnly) {
        ss << "_indirectOnly" << std::endl;
    }
    return ss.str();
}

}
