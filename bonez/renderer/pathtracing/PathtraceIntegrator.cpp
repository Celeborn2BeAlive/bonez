#include <iostream>
#include <sstream>
#include <iomanip>

#include "PathtraceIntegrator.hpp"
#include "bonez/renderer/radiometry.hpp"

#define PT_RECURSIVE
//#define PT_ITERATIVE

namespace BnZ {

std::string PathtraceIntegrator::toString() const {
    std::stringstream ss;
    ss << "PathtraceIntegrator_" <<
        "maxDepth=" << std::setfill('0') << std::setw(3) << m_nMaxDepth;
    if(m_bIndirectOnly) {
        ss << "_indirectOnly";
    }
    return ss.str();
}

void PathtraceIntegrator::requestSamples(IntegratorSampler& sampler) {
    size_t count = sampler.roundSampleCount(m_nMaxDepth - 1);
    m_nBRDFSampleSet = sampler.request2DSamples(count);
    m_nLight1DSampleSet = sampler.request1DSamples(count);
    m_nLight2DSampleSet = sampler.request2DSamples(count);
}

#ifdef PT_RECURSIVE

Col3f PathtraceIntegrator::Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample, 
    RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
    Intersection intersection = intersect(ray, scene, rayCounter);

    if (!intersection) {
        return scene.lights.Le(-ray.dir);
    }

    // First intersection: Add Le for handling 1 length path and compute reflected radiance
    return intersection.Le + Lr(intersection, -ray.dir, scene, 1, sample, rayCounter);
}

#endif

#ifdef PT_ITERATIVE

Col3f PathtraceIntegrator::Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample, 
    RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
    Col3f L = embree::zero;
    Col3f throughput(embree::one);

    Intersection intersection = intersect(ray, scene, rayCounter);
    if(!intersection) {
        return scene.Le(ray.dir);
    }
    
    L += intersection.Le;
    Vec3f wo = -ray.dir;

    size_t depth = 1;
    
    while(depth < m_nMaxDepth && intersection) {
        BRDF brdf = scene.shade(intersection);
        
        if(depth != 1 || !m_bIndirectOnly) {
            IncidentRayRandom random(sample.getFloat(), sample.getFloat(), sample.getFloat2());
            L += directIllumination(wo, brdf, scene, random, rayCounter);
        }

        if(depth + 1 < m_nMaxDepth) {
            Sample3f wi;
        
            Col3f brdfValue = brdf.sample(wo, wi, sample.get2DSample(m_nBRDFSampleSet, depth - 1));

            if (wi.pdf == 0.f || brdfValue == Col3f(embree::zero)) {
                break;
            }

            throughput *= brdfValue * embree::dot(wi.value, intersection.Ns) * embree::rcp(wi.pdf);

            intersection = bounce(intersection, wi, scene, rayCounter);
            wo = -wi.value;
        }
        ++depth;
    }

    return L;
}

#endif

Col3f PathtraceIntegrator::Lr(Intersection intersection, const Vec3f& wo, const Scene& scene, size_t depth,
    const IntegratorSample& sample, RayCounter& rayCounter) const {
    if (depth == m_nMaxDepth) {
        // End of recursion
        return embree::zero;
    }
    
    BRDF brdf = scene.shade(intersection);

    // Compute direct illumination (paths of length m_nMaxDepth - count + 1)
    Col3f L(embree::zero);

    if(depth != 1 || !m_bIndirectOnly) {
        IncidentRayRandom random(sample.getRandomFloat(), sample.getRandomFloat(), sample.getRandomVec2f());
        L += directIllumination(wo, brdf, scene, random, rayCounter);
    }

    if (depth + 1 < m_nMaxDepth) {
        Sample3f wi;
        Col3f brdfValue = brdf.sample(wo, wi, sample.get2DSample(m_nBRDFSampleSet, depth - 1));

        if (wi.pdf == 0.f || brdfValue == Col3f(embree::zero)) {
            return L;
        }

        Intersection I;
        if (!(I = bounce(intersection, wi.value, scene, rayCounter))) {
            return L;
        }

        // Indirect illumination
        L += brdfValue * embree::dot(wi.value, intersection.Ns) * embree::rcp(wi.pdf) * Lr(I, -wi.value, scene, depth + 1, sample, rayCounter);
    }

    return L;
}

}
