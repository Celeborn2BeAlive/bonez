#include <iostream>
#include <sstream>
#include <iomanip>

#include "SkelPTShadowApproxIntegrator.hpp"
#include "bonez/renderer/radiometry.hpp"

namespace BnZ {

std::string SkelPTShadowApproxIntegrator::toString() const {
    std::stringstream ss;
    ss << "SkelPTShadowApproxIntegrator_" <<
        "maxDepth=" << std::setfill('0') << std::setw(3) << m_nMaxDepth;
    if(m_bIndirectOnly) {
        ss << "_indirectOnly";
    }
    return ss.str();
}

void SkelPTShadowApproxIntegrator::requestSamples(IntegratorSampler& sampler) {
    size_t count = sampler.roundSampleCount(m_nMaxDepth - 1);
    m_nBRDFSampleSet = sampler.request2DSamples(count);
    m_nLight1DSampleSet = sampler.request1DSamples(count);
    m_nLight2DSampleSet = sampler.request2DSamples(count);
}

Col3f SkelPTShadowApproxIntegrator::Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample,
    RayCounter& rayCounter, const Integrator::StoragePtr& storage) const {
    Intersection intersection = intersect(ray, scene, rayCounter);

    if (!intersection) {
        return scene.lights.Le(-ray.dir);
    }

    // First intersection: Add Le for handling 1 length path and compute reflected radiance
    return intersection.Le + Lr(intersection, -ray.dir, scene, 1, sample, rayCounter);
}

Col3f SkelPTShadowApproxIntegrator::Lr(Intersection intersection, const Vec3f& wo, const Scene& scene, size_t depth,
    const IntegratorSample& sample, RayCounter& rayCounter) const {
    if (depth == m_nMaxDepth) {
        // End of recursion
        return embree::zero;
    }

    BRDF brdf = scene.shade(intersection);

    // Compute direct illumination (paths of length m_nMaxDepth - count + 1)
    Col3f L(embree::zero);

    if(depth != 1 || !m_bIndirectOnly) {
        IncidentRayRandom random(sample.getRandomFloat(), sample.getRandomFloat(), sample.getRandomVec2f());
        L += directIlluminationSkelApprox(wo, brdf, scene, random, rayCounter);
    }

    if (depth + 1 < m_nMaxDepth) {
        Sample3f wi;
        Col3f brdfValue = brdf.sample(wo, wi, sample.get2DSample(m_nBRDFSampleSet, depth - 1));

        if (wi.pdf == 0.f || brdfValue == Col3f(embree::zero)) {
            return L;
        }

        Intersection I;
        if (!(I = bounce(intersection, wi.value, scene, rayCounter))) {
            return L;
        }

        // Indirect illumination
        L += brdfValue * embree::dot(wi.value, intersection.Ns) * embree::rcp(wi.pdf) * Lr(I, -wi.value, scene, depth + 1, sample, rayCounter);
    }

    return L;
}

}
