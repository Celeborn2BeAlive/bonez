#ifndef _BONEZ_SKELMISPATHTRACEINTEGRATOR_HPP_
#define _BONEZ_SKELMISPATHTRACEINTEGRATOR_HPP_

#include "bonez/renderer/Integrator.hpp"
#include "bonez/scene/shading/BRDF.hpp"

#include "bonez/scene/topology/GraphImportancePoints.hpp"

namespace BnZ {

class SkelMISPathtraceIntegrator: public Integrator {
public:
    SkelMISPathtraceIntegrator(size_t maxDepth, bool indirectOnly, float skelStrength, float skelProb):
        m_nMaxDepth(maxDepth), m_bIndirectOnly(indirectOnly), m_fSkelStrength(skelStrength), m_fSkelProb(skelProb) {
    }
    
    void requestSamples(IntegratorSampler& sampler);
    
    void preprocess(const Camera& camera, const Scene& scene, const Framebuffer& framebuffer,
        size_t frameIdx, ParamSet& statistics);
    
    virtual Col3f Li(const Ray& ray, const Scene& scene, const IntegratorSample& sample, RayCounter& rayCounter,
        const Integrator::StoragePtr& storage) const;
    
    Col3f Lr(Intersection intersection, const Vec3f& wo, const Scene& scene, size_t depth, 
        const IntegratorSample& sample, RayCounter& rayCounter) const;
    
    virtual std::string toString() const;
    
    virtual std::string getType() const {
        return "SkelMISPathtraceIntegrator";
    }
    
    size_t m_nMaxDepth;
    bool m_bIndirectOnly;
    
    float m_fSkelStrength;
    float m_fSkelProb;
    
    size_t m_nBRDFSampleSet;
    size_t m_nMISSampleSet;
    size_t m_nLight1DSampleSet;
    size_t m_nLight2DSampleSet;
    
    GraphImportancePoints m_ImportancePoints;
};

}

#endif
