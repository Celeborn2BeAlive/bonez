#include "VPLRenderer.hpp"

namespace BnZ {

Col3f VPLRenderer::Li(const Ray& ray) {
    RayCounter rayCounter;
    Intersection I = intersect(ray, *m_pScene, rayCounter);

    if (!I) {
        return m_pScene->lights.Le(-ray.dir);
    }

    Vec3f wo = -ray.dir;
    Col3f L = I.Le;

    BRDF brdf = m_pScene->shade(I);

    for(const DirectionalVPL& vpl: m_VPLContainer.directionalVPLs) {
        Vec3f wi = -vpl.D;

        float cos_i = embree::dot(I.Ns, wi);
        if(cos_i <= 0.f) {
            continue;
        }

        Col3f reflectionFactor = brdf.eval(wi, wo);
        if(reflectionFactor == Col3f(embree::zero)) {
            continue;
        }

        if(occluded(shadow_ray(I, wi, embree::inf), *m_pScene, rayCounter)) {
            continue;
        }

        L += reflectionFactor * cos_i * vpl.L;
    }

    for(const OrientedVPL& vpl: m_VPLContainer.orientedVPLs) {
        Ray shadowRay;
        float geometryTerm = Gclamped(I, vpl, m_fDistClamp, shadowRay);

        if(geometryTerm == 0.f) {
            continue;
        }

        Col3f reflectionTerm;
        if(m_bShowIrradiance) {
            reflectionTerm = Col3f(1.f / 3.14f);
        } else {
            reflectionTerm = brdf.eval(shadowRay.dir, wo);
        }

        if(reflectionTerm == Col3f(embree::zero)) {
            continue;
        }

        if(occluded(shadowRay, *m_pScene, rayCounter)) {
            continue;
        }

        L += vpl.L * geometryTerm * reflectionTerm;
    }

    if(m_bShowDirectLight && m_LightIntensity != Col3f(0.f)) {
        Ray shadowRay;
        float geometryTerm = Gclamped(I, m_LightPosition, m_fDistClamp, shadowRay);

        if(geometryTerm != 0.f) {
            Col3f reflectionTerm;
            if(m_bShowIrradiance) {
                reflectionTerm = Col3f(1.f / 3.14f);
            } else {
                reflectionTerm = brdf.eval(shadowRay.dir, wo);
            }

            if(reflectionTerm != Col3f(embree::zero) && !occluded(shadowRay, *m_pScene, rayCounter)) {
                L += m_LightIntensity * geometryTerm * reflectionTerm;
            }
        }
    }

    return L;
}

void VPLRenderer::renderThread(size_t threadID) {
    const Camera* camera = m_pCamera;
    Framebuffer* framebuffer = m_pFramebuffer;
    size_t width = framebuffer->getWidth();
    size_t height = framebuffer->getHeight();
    size_t tileCount = m_nTileX * m_nTileY;

    while (true) {
        size_t tileID = m_nTileID++;

        if (tileID >= tileCount) {
            break;
        }

        size_t tileX = tileID % m_nTileX;
        size_t tileY = (tileID - tileX) / m_nTileX;

        size_t xstart = embree::min(tileX * TILE_SIZE, width);
        size_t xend = embree::min(xstart + TILE_SIZE, width);
        size_t ystart = embree::min(tileY * TILE_SIZE, height);
        size_t yend = embree::min(ystart + TILE_SIZE, height);

        int nbSampleX = m_nSppEdge, nbSampleY = m_nSppEdge;
        float dX = 1.f / (nbSampleX + 1), dY = 1.f / (nbSampleY + 1);

        for(int y = ystart; y < int(yend); ++y) {
            for(int x = xstart; x < int(xend); ++x) {
                for(int i = 1; i <= nbSampleX; ++i) {
                    for(int j = 1; j <= nbSampleY; ++j) {
                        Vec2f P(1.f - (x + dX * i) / width,
                                1.f - (y + dY * j) / height);

                        Col3f value = Li(camera->ray(P));
                        framebuffer->accumulate(Vec2i(embree::clamp(x, 0, (int) width - 1),
                                                      embree::clamp(y, 0, (int) height - 1)), value);
                    }
                }
            }
        }

        if (m_bShowProgress) {
            m_Progress.next();
        }
    }
}

void VPLRenderer::render(const Camera& camera, const Scene& scene,
    Framebuffer& framebuffer, bool showProgress, bool showIrradiance, bool showDirectLight,
    float distClamp, int spp) {
    m_nTileID = 0;
    m_pCamera = &camera;
    m_pScene = &scene;
    m_pFramebuffer = &framebuffer;
    m_bShowProgress = showProgress;
    m_bShowIrradiance = showIrradiance;
    m_bShowDirectLight = showDirectLight;
    m_nSppEdge = embree::sqrt(float(spp));
    m_fDistClamp = distClamp;

    if(m_nSppEdge * m_nSppEdge < spp) {
        m_nSppEdge += 1;
    }

    m_nTileX = (framebuffer.getWidth() + TILE_SIZE - 1) / TILE_SIZE;
    m_nTileY = (framebuffer.getHeight() + TILE_SIZE - 1) / TILE_SIZE;

    if (showProgress) {
        m_Progress.init(m_nTileX * m_nTileY);
        m_Progress.start();
    }

#if BONEZ_MULTITHREADING_ENABLED
    embree::scheduler->start();
    embree::scheduler->addTask(embree::TaskScheduler::ThreadInfo(), embree::TaskScheduler::GLOBAL_FRONT,
                               (embree::TaskScheduler::runFunction) &runRenderThread, this, embree::scheduler->getNumThreads(),
                               nullptr, nullptr, "render::tile");
    embree::scheduler->stop();
#else
    renderThread(0);
#endif

    if (showProgress) {
        m_Progress.end();
    }
}

}
