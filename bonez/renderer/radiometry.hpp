#ifndef _BONEZ_API_RADIOMETRY_HPP_
#define _BONEZ_API_RADIOMETRY_HPP_

#include "bonez/common/common.hpp"
#include "bonez/scene/Scene.hpp"

namespace BnZ {

/**
 * Estimate direct illumination at a point using Monte-Carlo.
 */
__forceinline Col3f directIllumination(const Vec3f& wo, const BRDF& brdf, 
    const Scene& scene, const IncidentRayRandom& sample, RayCounter& rayCounter) {
    RaySample wi;
    Col3f L = scene.lights.sampleIncidentRay(scene, brdf.P, sample, wi);

    if(L == Col3f(embree::zero) || wi.pdf == 0.f) {
        return Col3f(embree::zero);
    }
    
    float cos_i = embree::dot(wi.value.dir, brdf.P.Ns);
    if (cos_i <= 0.f) {
        return Col3f(embree::zero);
    }

    if (occluded(wi, scene, rayCounter)) {
        return Col3f(embree::zero);
    }

    return brdf.eval(wi.value.dir, wo) * cos_i * L * embree::rcp(wi.pdf);
}

__forceinline Col3f directIlluminationSkelApprox(const Vec3f& wo, const BRDF& brdf,
    const Scene& scene, const IncidentRayRandom& sample, RayCounter& rayCounter) {
    RaySample wi;
    Col3f L = scene.lights.sampleIncidentRay(scene, brdf.P, sample, wi);

    if(L == Col3f(embree::zero) || wi.pdf == 0.f) {
        return Col3f(embree::zero);
    }

    float cos_i = embree::dot(wi.value.dir, brdf.P.Ns);
    if (cos_i <= 0.f) {
        return Col3f(embree::zero);
    }



    // If infinite ray, we can't use the skeleton to approximate visibility
    if(wi.value.far == float(embree::inf)) {
        if (occluded(wi, scene, rayCounter)) {
            return Col3f(embree::zero);
        }

        return brdf.eval(wi.value.dir, wo) * cos_i * L * embree::rcp(wi.pdf);
    }

    // Here we approximate the visibility by using visibiliy between nodes and points
    Vec3f P = wi.value.org + wi.value.far * wi.value.dir;

    /*
    GraphNodeIndex I1 = scene.topology.getNearestCluster(brdf.P);
    GraphNodeIndex I2 = scene.topology.getNearestCluster(P);

    if(I1 == NO_CLUSTER() || I2 == NO_CLUSTER()) {
        if (occluded(wi, scene, rayCounter)) {
            return Col3f(embree::zero);
        }

        return brdf.eval(wi.value.dir, wo) * cos_i * L * embree::rcp(wi.pdf);
    }
*/

    GraphNodeIndex I1 = scene.voxelSpace->getSkeleton().getNearestNode(brdf.P);
    GraphNodeIndex I2 = scene.voxelSpace->getSkeleton().getNearestNode(P);

    if(I1 == UNDEFINED_NODE || I2 == UNDEFINED_NODE) {
        return brdf.eval(wi.value.dir, wo) * cos_i * L * embree::rcp(wi.pdf);

        /*if (occluded(wi, scene, rayCounter)) {
            return Col3f(embree::zero);
        }

        return brdf.eval(wi.value.dir, wo) * cos_i * L * embree::rcp(wi.pdf);*/
    }

    //Vec3f C = scene.topology.getCluster(I1).center;
    Vec3f C = scene.voxelSpace->getSkeleton()[I1].getPosition();
    if(occluded(shadow_ray(C, P), scene, rayCounter)) {
        return Col3f(embree::zero);
    }

    //C = scene.topology.getCluster(I2).center;
    C = scene.voxelSpace->getSkeleton()[I2].getPosition();
    if(occluded(shadow_ray(C, brdf.P.P), scene, rayCounter)) {
        return Col3f(embree::zero);
    }

    return brdf.eval(wi.value.dir, wo) * cos_i * L * embree::rcp(wi.pdf);
}

/**
 * Estimate direct illumination at an intersection using Monte-Carlo.
 */
__forceinline Col3f directIllumination(const Vec3f& wo, const BRDF& brdf, const Scene& scene, 
    const IncidentRayRandom* begin, const IncidentRayRandom* end, RayCounter& rayCounter) {
    Col3f L(0.f);
    for(auto it = begin; it != end; ++it) {
        L += directIllumination(wo, brdf, scene, *it, rayCounter);
    }
    return L / float(end - begin);
}

}

#endif
