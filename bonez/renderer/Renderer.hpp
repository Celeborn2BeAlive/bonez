#ifndef _BONEZ_API_RENDERER_HPP_
#define _BONEZ_API_RENDERER_HPP_

#include <string>
#include <memory>

#include "bonez/common/cameras/Camera.hpp"
#include "Framebuffer.hpp"
#include "PostProcess.hpp"
#include "bonez/scene/Scene.hpp"

#include "bonez/common/Params.hpp"
#include "bonez/scene/lights/vpls.hpp"

namespace BnZ {

struct RendererContext {
    const Camera* camera;
    const Scene* scene;
    Framebuffer* framebuffer;

    size_t frameIndex;

    ParamSet* statistics;
    bool showProgress;
};

class Renderer {
public:
    virtual ~Renderer() {
    }

    // Render the image of a scene as seen from a camera into framebuffer.
    virtual void render(const Camera& camera, const Scene& scene, Framebuffer& framebuffer,
                        bool showProgress, ParamSet* statistics) = 0;
    
    virtual bool getVPLs(const Camera& camera, const Scene& scene, 
        const Framebuffer& framebuffer, VPLContainer& vpls) = 0;
    
    /**
     * @brief getRenderString
     * @return A string that resume the rendering parameters.
     */
    virtual std::string getRenderString() const = 0;
    
    /**
     * @brief getType
     * @return The type of the renderer.
     */
    virtual std::string getType() const = 0;

    virtual void setParam(const std::string& name, const ParamPtr& value) {
    }
};

// A shared pointer to a Renderer
typedef std::shared_ptr<Renderer> RendererPtr;

}

#endif
