#ifndef _BONEZ_API_FRAMEBUFFER_HPP_
#define _BONEZ_API_FRAMEBUFFER_HPP_

#include <vector>
#include <algorithm>
#include <embree/common/image/image.h>
#include <embree/default.h>
#include "bonez/common/common.hpp"

namespace BnZ {

class Framebuffer {
    std::vector<Vec4f> m_Buffer;
    size_t m_nWidth, m_nHeight;
public:
    Framebuffer():
        m_nWidth(0), m_nHeight(0) {
    }

    Framebuffer(size_t width, size_t height):
        m_Buffer(width * height, Vec4f(0.f)),
        m_nWidth(width), m_nHeight(height) {
    }

    size_t getWidth() const {
        return m_nWidth;
    }

    size_t getHeight() const {
        return m_nHeight;
    }

    size_t getSize() const {
        return m_nWidth * m_nHeight;
    }

    float getAspectRatio() const {
        return m_nWidth / (float) m_nHeight;
    }

    bool empty() const {
        return m_Buffer.empty();
    }

    void fill(const Col3f& color) {
        std::fill(begin(m_Buffer), end(m_Buffer), Vec4f(color.r, color.g, color.b, 1.f));
    }

    Vec2f getRasterPosition(size_t x, size_t y) const {
        return Vec2f(x + 0.5, y + 0.5);
    }

    void clear() {
        std::fill(begin(m_Buffer), end(m_Buffer), Vec4f(0.f, 0.f, 0.f, 0.f));
    }

    const Col3f operator ()(size_t x, size_t y) const {
        size_t idx = x + y * m_nWidth;
        Vec4f value = m_Buffer[idx];
        return Col3f(value.x, value.y, value.z) * (1.f / value.w);
    }

    void set(size_t x, size_t y, const Col3f& color) {
        size_t idx = x + y * m_nWidth;
        m_Buffer[idx] = Vec4f(color.r, color.g, color.b, 1.f);
    }

    void accumulate(const embree::Vec2i& pixel, const embree::Col3f& color) {
        size_t idx = pixel.x + pixel.y * m_nWidth;
        m_Buffer[idx] += Vec4f(color.r, color.g, color.b, 1.f);
    }

    void set(const embree::Image& image) {
        *this = Framebuffer(image.width, image.height);
        for (size_t y = 0; y < image.height; ++y) {
            for (size_t x = 0; x < image.width; ++x) {
                set(x, y, image.get(x, y));
            }
        }
    }

    void normalize() {
        for(auto& value: m_Buffer) {
            value /= value.w;
        }
    }

    const Vec4f* getPixels() const {
        return m_Buffer.data();
    }
};

inline Framebuffer normalize(Framebuffer fb) {
    fb.normalize();
    return fb;
}

}

#endif
