#ifndef _BONEZ_MATHS_HPP_
#define _BONEZ_MATHS_HPP_

#include "common.hpp"

namespace BnZ {

/**
 * Build a coordinate system.
 * \param yAxis The y-axis of the coordinate system. It must be normalized.
 * \return A linear space representing the coordinate system.
 */
__forceinline LinearSpace3f coordinateSystem(const Vec3f& yAxis) {
    LinearSpace3f ls;
    ls.vy = yAxis;
    if(embree::abs(yAxis.y) > embree::abs(yAxis.x)) {
        float rcpLength = embree::rcp(embree::length(Vec2f(ls.vy.x, ls.vy.y)));
        ls.vx = rcpLength * Vec3f(ls.vy.y, -ls.vy.x, 0.f);
    } else {
        float rcpLength = embree::rcp(embree::length(Vec2f(ls.vy.x, ls.vy.z)));
        ls.vx = rcpLength * Vec3f(ls.vy.z, 0.f, -ls.vy.x);
    }
    ls.vz = embree::cross(ls.vx, ls.vy);
    return ls;
}

__forceinline AffineSpace3f coordinateSystem(const Vec3f& origin, const Vec3f& yAxis) {
    return AffineSpace3f(coordinateSystem(yAxis), origin);
}

/**
 * Compute the bounding sphere of a bounding box.
 * \param bbox The bounding box.
 * \param out center Contains the center of the sphere after the call.
 * \param out radius Contains the radius of the sphere after the call.
 */
__forceinline void boundingSphere(const BBox3f& bbox, Vec3f& center, float& radius) {
    center = embree::center(bbox);
    radius = embree::distance(center, bbox.upper);
}

/**
 * Sample uniformly barycentric coordinates on a triangle.
 * \param r0 A random float on [0, 1) used to choose the point.
 * \param r1 A random float on [0, 1) used to choose the point.
 * \param out u Contains the u coordinate after the call.
 * \param out v Contains the v coordinate after the call.
 */
__forceinline void uniformSampleTriangle(float r0, float r1, float& u, float& v) {
    float s = embree::sqrt(r0);
    u = 1 - s;
    v = r1 * s;
}

__forceinline Vec3f sphericalToCartesian(float phi, float theta) {
    float cos_phi = embree::cos(phi), sin_phi = embree::sin(phi);
    float cos_theta = embree::cos(theta), sin_theta = embree::sin(theta);
    return embree::Vec3f(sin_theta * sin_phi, cos_theta, sin_theta * cos_phi);
}

__forceinline Vec2f cartesianToSpherial(Vec3f v) {
    Vec2f s;
    s.y = embree::acos(v.y); // In range [0, pi]
    s.x = embree::atan2(v.x, v.z);
    if(s.x < 0.f) {
        s.x += (float) embree::two_pi;
    }
    return s;
}

__forceinline float lengthSquared(Vec3f v) {
    return embree::dot(v, v);
}

__forceinline float distanceSquared(Vec3f P, Vec3f Q) {
    return lengthSquared(Q - P);
}

__forceinline float distanceToLine(Vec3f P1, Vec3f P2, Vec3f P) {
    Vec3f u = P2 - P1;
    Vec3f v = P - P1;
    return embree::length(embree::cross(u, v)) / embree::length(u);
}

}

#endif
