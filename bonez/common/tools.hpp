#ifndef _BONEZ_TOOLS_HPP_
#define _BONEZ_TOOLS_HPP_

#include "common.hpp"

namespace BnZ {

inline Vec2f getPixelNormalizedPosition(size_t width, size_t height, size_t x, size_t y) {
    return (Vec2f(x, y) + Vec2f(0.5f, 0.5f)) * embree::rcp(Vec2f(width, height));
}

}

#endif
