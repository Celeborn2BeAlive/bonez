#ifndef _BONEZ_TYPES_HPP_
#define _BONEZ_TYPES_HPP_

#include <cstdint>
#include <embree/default.h>
#include <embree/rtcore/common/ray.h>
#include <embree/rtcore/common/hit.h>
#include <embree/common/math/bbox.h>
#include <embree/common/math/random.h>
#include <embree/common/sys/sync/mutex.h>

namespace BnZ {
    
typedef embree::Col3f Col3f;
typedef embree::Vec2f Vec2f;
typedef embree::Vec3f Vec3f;
typedef embree::Vec4f Vec4f;
typedef embree::Vec3<unsigned int> Vec3ui;
typedef embree::Vec2<int> Vec2i;
typedef embree::Vec2<uint32_t> Vec2ui;
typedef embree::Vec3<int> Vec3i;
typedef embree::BBox3f BBox3f;
typedef embree::LinearSpace3f LinearSpace3f;
typedef embree::AffineSpace3f AffineSpace3f;
typedef embree::MutexSys MutexSys;
typedef embree::Ray Ray;
typedef embree::Hit Hit;
typedef embree::Random Random;
typedef embree::Sample2f Sample2f;
typedef embree::Sample3f Sample3f;
typedef embree::Sample<uint32_t> Sample1ui;
typedef embree::Sample<Ray> RaySample;

#define BAD_COLOR Col3f(0.95, 0, 0.63)

struct RayCounter {
    size_t shadowRayCount;
    size_t reflectionRayCount;
    size_t viewRayCount;
    
    RayCounter():
        shadowRayCount(0), reflectionRayCount(0), viewRayCount(0) {
    }
    
    size_t totalRayCount() {
        return shadowRayCount + reflectionRayCount + viewRayCount;
    }
};

}

#endif
