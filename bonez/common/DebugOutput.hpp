#ifndef _BONEZ_DEBUGOUTPUT_HPP
#define _BONEZ_DEBUGOUTPUT_HPP

#include <ostream>

namespace BnZ {

template<bool enabled>
class DebugOutput {
    std::ostream& m_Out;
public:
    DebugOutput(std::ostream& out = std::cerr): m_Out(out) {
    }

    template<typename T>
    friend DebugOutput& operator <<(DebugOutput& out, T&& value) {
        out.m_Out << value;
        return out;
    }
};

template<>
class DebugOutput<false> {
public:
    DebugOutput(std::ostream& out = std::cerr) {
    }

    template<typename T>
    friend DebugOutput& operator <<(DebugOutput& out, T&& value) {
        return out;
    }
};

}

#endif // _BONEZ_DEBUGOUTPUT_HPP
