#ifndef _BONEZ_SAMPLING_SAMPLER_HPP_
#define _BONEZ_SAMPLING_SAMPLER_HPP_

#include <cassert>
#include <vector>
#include <embree/common/math/random.h>
#include <embree/common/sys/stl/array2d.h>
#include <embree/common/math/permutation.h>

#include "bonez/common/common.hpp"

namespace BnZ {

class Sampler {
public:
    Sampler() = default;

    Sampler(size_t seed):
        m_Rng(seed), m_Rng1D(seed + 16) {
    }

    void setSeed(size_t seed) {
        m_Rng.setSeed(seed);
        m_Rng1D.setSeed(seed + 16);
    }

    Vec2f get2DSample() {
        return embree::Vec2f(m_Rng.getFloat(), m_Rng.getFloat());
    }

    float get1DSample() {
        return m_Rng1D.getFloat();
    }

private:
    embree::Random m_Rng;
    embree::Random m_Rng1D;
};

#if 0

inline float getHaltonValue(int idx, int base) {
    float f = 1.f / base;
    float result = 0.f;

    for (int i = idx; i > 0; ) {
        result += f * (i % base);
        i = i / base;
        f = f / base;
    }

    return result;
}

class Sampler {
public:
    Sampler() = default;

    Sampler(size_t seed):
/*m_CurrentHaltonValueBase2(1 / 2.f), m_CurrentHaltonValueBase3(1 / 3.f), m_CurrentHaltonValueBase5(1 / 5.f),*/
        m_2DHaltonIndex(1), m_1DHaltonIndex(1) {
    }

    Vec2f get2DSample() {
        Vec2f r = Vec2f(getHaltonValue(m_2DHaltonIndex, 2), getHaltonValue(m_2DHaltonIndex, 3));
        std::cerr << r << std::endl;
        ++m_2DHaltonIndex;
        return r;
    }

    float get1DSample() {
        return getHaltonValue(m_1DHaltonIndex++, 5);
    }

    LightSample getLightSample(const TriangleMesh& mesh) {
        return LightSampler::sample(mesh, get1DSample(), get1DSample());
    }

private:
    /*embree::Random m_Rng;
    embree::Random m_Rng1D;*/
    //float m_CurrentHaltonValueBase2, m_CurrentHaltonValueBase3, m_CurrentHaltonValueBase5;
    int m_2DHaltonIndex, m_1DHaltonIndex;
    //float m_2DHaltonFactor, m_1DHaltonFactor;
};

#endif

#if 0

class Sampler {
public:
    static const size_t NB_2D_SAMPLES_PER_EDGE = 128;
    static const size_t NB_2D_SAMPLES = NB_2D_SAMPLES_PER_EDGE * NB_2D_SAMPLES_PER_EDGE;

    Vec2f m_2DSamples[NB_2D_SAMPLES];

    Sampler() = default;

    Sampler(size_t seed):
        m_Rng(seed), m_Rng1D(seed + 16), m_LightSampler(seed + 32) {
        generate2DSamples();
    }

    void setSeed(size_t seed) {
        m_Rng.setSeed(seed);
        m_Rng1D.setSeed(seed + 16);
        m_LightSampler.setSeed(seed + 32);
        generate2DSamples();
    }

    static void multiJittered(Vec2f* samples, const size_t b, embree::Random& rng) {
        embree::Array2D<Vec2f> grid(b, b);
        embree::vector_t<size_t> numbers(b);

        for (uint32 i = 0; i < b; i++) {
            numbers[i] = i;
        }

        for (size_t i = 0; i < b; i++) {
            numbers.shuffle(rng);

            for (size_t j = 0; j < b; j++) {
                ((Vec2f**)grid)[i][j][0] = float(i) / float(b) + (numbers[j] + rng.getFloat()) / float(b * b);
            }
        }

        for (size_t i = 0; i < b; i++) {
            numbers.shuffle(rng);

            for (size_t j = 0; j < b; j++) {
                ((Vec2f**)grid)[j][i][1] = float(i) / float(b) + (numbers[j] + rng.getFloat()) / float(b * b);
            }
        }

        embree::Permutation permutation(b * b, rng);
        uint32 counter = 0;

        for (size_t j = 0; j < b; j++)  {
            for (size_t i = 0; i < b; i++) {
                samples[permutation[counter++]] = grid.get(i, j);
            }
        }
    }

    void generate2DSamples() {
        m_nCurrent2DSample = 0;
        multiJittered(m_2DSamples, NB_2D_SAMPLES_PER_EDGE, m_Rng);
    }

    embree::Vec2f get2DSample() {
        //return embree::Vec2f(m_Rng.getFloat(), m_Rng.getFloat());
        Vec2f s = m_2DSamples[m_nCurrent2DSample];

        if (++m_nCurrent2DSample == NB_2D_SAMPLES) {
            m_nCurrent2DSample = 0;
        }

        return s;
    }

    float get1DSample() {
        return m_Rng1D.getFloat();
    }

    LightSample getLightSample(const TriangleMesh& mesh) {
        return m_LightSampler.sample(mesh);
    }

private:
    embree::Random m_Rng;
    embree::Random m_Rng1D;
    LightSampler m_LightSampler;

    size_t m_nCurrent2DSample;
};

#endif

}

#endif
