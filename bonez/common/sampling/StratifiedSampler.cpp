#include <sstream>
#include <iomanip>

#include "StratifiedSampler.hpp"

#include "patterns.hpp"

namespace BnZ {

StratifiedSampler::StratifiedSampler(size_t sppX, size_t sppY):
    m_nSppX(sppX), m_nSppY(sppY), m_nSpp(sppX * sppY) {
}

IntegratorSamplerPtr StratifiedSampler::clone() const {
    StratifiedSamplerPtr sampler = std::make_shared<StratifiedSampler>(m_nSppX, m_nSppY);
    sampler->m_1DRequestedCounts = m_1DRequestedCounts;
    sampler->m_2DRequestedCounts = m_2DRequestedCounts;
    return sampler;
}

size_t StratifiedSampler::init(size_t frameIdx, size_t xstart, size_t ystart, size_t xend, size_t yend) {
    m_nFrameIdx = frameIdx;
    m_nXStart = xstart;
    m_nYStart = ystart;
    m_nXEnd = xend;
    m_nYEnd = yend;
    m_Rng.setSeed(m_nFrameIdx * (xend - xstart) * (yend - ystart) + xstart + ystart * (xend - xstart));
    
    m_CurrentPixel = Vec2i(m_nXStart, m_nYStart);
    m_nCurrentSample = 0;
    
    if(!m_RasterBuffer) {
        m_RasterBuffer.reset(new Vec2f[m_nSpp]);
    }

    return m_nSpp * (m_nXEnd - m_nXStart) * (m_nYEnd - m_nYStart);
}

#if 0

int StratifiedSampler::getNextSamples(IntegratorSampleBuffer& buffer) {
    size_t sampleCount = buffer.m_nMaxCount;

    if(!m_FloatBuffer) {
        m_FloatBuffer.reset(new float[2 * sampleCount]);
    }
    
    if(m_CurrentPixel.y >= m_nYEnd) {
        return -1;
    }
    
    std::fill(buffer.m_FloatBuffer.get(), buffer.m_FloatBuffer.get() 
        + buffer.m_nFloatCountPerSample * sampleCount, 0.f);
    
    float* rasterSamples = m_FloatBuffer.get();
    createJittered2DPattern(rasterSamples, m_nSppX, m_nSppY, m_Rng);
    
    for(size_t i = 0; i < sampleCount; ++i) {
        rasterSamples[2 * i] += m_CurrentPixel.x;
        rasterSamples[2 * i + 1] += m_CurrentPixel.y;
    }
    
    /*
    float* integrator1DSamples = rasterSamples + 2 * sampleCount;
    float* integratorSamples = integrator1DSamples;
    
    for(size_t count: m_1DRequestedCounts) {
        createJittered1DPattern(integratorSamples, sampleCount * count, m_Rng);
        shuffle1D(integratorSamples, buffer.m_nMaxCount * count, m_Rng);
        integratorSamples += sampleCount * count;
    }
    
    float* integrator2DSamples = integratorSamples;
    
    for(size_t count: m_2DRequestedCounts) {
        createRandom2DPattern(integratorSamples, sampleCount * count, m_Rng);
        integratorSamples += sampleCount * 2 * count;
    }
    */
    
    float* ptr = buffer.m_FloatBuffer.get();
    for(size_t i = 0; i < sampleCount; ++i) {
        
        ptr[0] = rasterSamples[2 * i];
        ptr[1] = rasterSamples[2 * i + 1];

        createRandom1DPattern(ptr + 2, buffer.m_nFloatCountPerSample - 2, m_Rng);
/*
        size_t sampleSetIdx = 0;
        float* ptr1D = integrator1DSamples + i * sampleCount;
        for(size_t count: m_1DRequestedCounts) {
            std::copy(ptr1D, ptr1D + count, ptr + buffer.m_SampleSetsOffsets[sampleSetIdx]);
            ptr1D += sampleCount * count;
            ++sampleSetIdx;
        }
        
        float* ptr2D = integrator2DSamples + i * 2 * sampleCount;
        for(size_t count: m_2DRequestedCounts) {
            std::copy(ptr2D, ptr2D + 2 * count, ptr + buffer.m_SampleSetsOffsets[sampleSetIdx]);
            ptr2D += sampleCount * 2 * count;
            ++sampleSetIdx;
        }
*/
        ptr += buffer.m_nFloatCountPerSample;
    }
    
    ++m_CurrentPixel.x;
    if(m_CurrentPixel.x >= m_nXEnd) {
        m_CurrentPixel.x = m_nXStart;
        ++m_CurrentPixel.y;
    }
    
    return sampleCount;
}

#endif

#if 0
int StratifiedSampler::getNextSamples(IntegratorSampleBuffer& buffer) {
    size_t sampleCount = buffer.m_nMaxCount;

    if(m_CurrentPixel.y >= m_nYEnd) {
        return -1;
    }
    
    float* rasterSamples = buffer.m_FloatBuffer.get();
    createJittered2DPattern(rasterSamples, m_nSppX, m_nSppY, m_Rng);
    
    for(size_t i = 0; i < sampleCount; ++i) {
        rasterSamples[2 * i] += m_CurrentPixel.x;
        rasterSamples[2 * i + 1] += m_CurrentPixel.y;
    }
    buffer.m_Rng = &m_Rng;
    
    ++m_CurrentPixel.x;
    if(m_CurrentPixel.x >= m_nXEnd) {
        m_CurrentPixel.x = m_nXStart;
        ++m_CurrentPixel.y;
    }
    
    return sampleCount;
}
#endif

bool StratifiedSampler::getNextSample(IntegratorSample& sample) {
    if(m_CurrentPixel.y >= m_nYEnd) {
        return false;
    }
    
    if(m_nCurrentSample == 0) {
        //m_Rng.setSeed(m_nFrameIdx);
        
        // Generate samples
        Vec2f* rasterSamples = m_RasterBuffer.get();
        createJittered2DPattern(rasterSamples, m_nSppX, m_nSppY, m_Rng);
        
        for(size_t i = 0; i < m_nSpp; ++i) {
            (*rasterSamples).x += m_CurrentPixel.x;
            (*rasterSamples).y += m_CurrentPixel.y;
            ++rasterSamples;
        }
    }

    sample = IntegratorSample(m_Rng, m_RasterBuffer[m_nCurrentSample],
                              Vec2f(m_Rng.getFloat(), m_Rng.getFloat()));

    if(++m_nCurrentSample >= m_nSpp) {
        m_nCurrentSample = 0;
        if(++m_CurrentPixel.x >= m_nXEnd) {
            m_CurrentPixel.x = m_nXStart;
            ++m_CurrentPixel.y;
        }
    }
    
    return true;
}

std::string StratifiedSampler::toString() const {
    std::stringstream ss;
    ss << std::setfill('0') << "spp=" << std::setw(5) << m_nSpp;
    return ss.str();
}

}
