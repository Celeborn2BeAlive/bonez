#ifndef _BONEZ_STRATIFIEDSAMPLER_HPP_
#define _BONEZ_STRATIFIEDSAMPLER_HPP_

#include "IntegratorSampler.hpp"

namespace BnZ {

class StratifiedSampler: public IntegratorSampler {
public:
    /**
     * Constructor.
     * \param sppX Number of samples per pixel in the x direction.
     * \param sppY Number of samples per pixel in the y direction.
     */
    StratifiedSampler(size_t sppX, size_t sppY);

    virtual IntegratorSamplerPtr clone() const;

    virtual size_t init(size_t frameIdx, size_t xstart, size_t ystart, size_t xend, size_t yend);

    virtual bool getNextSample(IntegratorSample& sample);
    
    /**
     * Return a string that resume the parameters of the sampler
     * and can be used in a filename.
     */
    virtual std::string toString() const;
    
private:
    size_t m_nSppX, m_nSppY;
    size_t m_nSpp;
    size_t m_nFrameIdx;
    int m_nXStart, m_nYStart, m_nXEnd, m_nYEnd;
    embree::Random m_Rng;
 
    std::unique_ptr<Vec2f[]> m_RasterBuffer;
    
    Vec2i m_CurrentPixel;
    size_t m_nCurrentSample;
};

typedef std::shared_ptr<StratifiedSampler> StratifiedSamplerPtr;

}

#endif
