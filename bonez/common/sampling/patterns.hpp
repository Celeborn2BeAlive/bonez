#ifndef _BONEZ_PATTERNS_HPP_
#define _BONEZ_PATTERNS_HPP_

#include "bonez/common/common.hpp"
#include <embree/common/math/random.h>

namespace BnZ {

void createJittered1DPattern(float* samples, size_t n, embree::Random& rng);

void createJittered2DPattern(Vec2f* samples, size_t nx, size_t ny, embree::Random& rng);

void createJittered2DPattern(float* samples, size_t nx, size_t ny, embree::Random& rng);

void createRandom1DPattern(float* samples, size_t n, embree::Random& rng);

void createRandom2DPattern(float* samples, size_t n, embree::Random& rng);

void shuffle1D(float* samples, size_t n, embree::Random& rng);

void shuffle2D(float* samples, size_t n, embree::Random& rng);

}

#endif
