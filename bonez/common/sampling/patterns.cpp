#include "patterns.hpp"

#include <embree/common/math/permutation.h>

namespace BnZ {

void createJittered1DPattern(float* samples, size_t n, embree::Random& rng) {
    float rcpCount = 1.f / n;
    for(size_t i = 0; i < n; ++i) {
        *samples++ = (i + rng.getFloat()) * rcpCount;
    }
}

void createJittered2DPattern(Vec2f* samples, size_t nx, size_t ny, embree::Random& rng) {
    Vec2f rcpDim(1.f / nx, 1.f / ny);
    for(size_t y = 0; y < ny; ++y) {
        for(size_t x = 0; x < nx; ++x) {
            *samples++ = Vec2f(x + rng.getFloat(), y + rng.getFloat()) * rcpDim;
        }
    }
}

void createJittered2DPattern(float* samples, size_t nx, size_t ny, embree::Random& rng) {
    Vec2f rcpDim(1.f / nx, 1.f / ny);
    for(size_t y = 0; y < ny; ++y) {
        for(size_t x = 0; x < nx; ++x) {
            *samples++ = (x + rng.getFloat()) * rcpDim.x;
            *samples++ = (y + rng.getFloat()) * rcpDim.y;
        }
    }
}

void createRandom1DPattern(float* samples, size_t n, embree::Random& rng) {
    while(n--) {
        *samples++ = rng.getFloat();
    }
}

void createRandom2DPattern(float* samples, size_t n, embree::Random& rng) {
    while(n--) {
        *samples++ = rng.getFloat();
        *samples++ = rng.getFloat();
    }
}

void shuffle1D(float* samples, size_t n, embree::Random& rng) {
    embree::Permutation p(n, rng);
    for(size_t i = 0; i < n; ++i) {
        std::swap(samples[i], samples[p[i]]);
    }
}

void shuffle2D(float* samples, size_t n, embree::Random& rng) {
    embree::Permutation p(n, rng);
    
    for(size_t i = 0; i < n; ++i) {
        std::swap(samples[2 * i], samples[p[2 * i]]);
        std::swap(samples[2 * i + 1], samples[p[2 * i + 1]]);
    }
}

}
