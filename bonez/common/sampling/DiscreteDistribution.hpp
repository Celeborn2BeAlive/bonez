#ifndef _BONEZ_SAMPLING_DISCRETEDISTRIBUTION_HPP_
#define _BONEZ_SAMPLING_DISCRETEDISTRIBUTION_HPP_

#include <vector>
#include <algorithm>
#include "bonez/common/common.hpp"

namespace BnZ {

class DiscreteDistribution {
    typedef std::vector<float> CDFContainer;
public:
    DiscreteDistribution():
        m_CDF(1, 0.f) {
    }

    __forceinline void add(float value) {
        m_CDF.push_back(m_CDF.back() + value);
        //std::cerr << "back = " << m_CDF.back() << std::endl;
    }

    __forceinline size_t size() const {
        return m_CDF.size() - 1;
    }

    __forceinline Sample1ui sample(float r) const {
        if(m_CDF.back() == 0.f) {
            return Sample1ui(0, 0.f);
        }

        //std::cerr << "CDF " << m_CDF.back() << std::endl;
        auto it = std::lower_bound(std::begin(m_CDF), std::end(m_CDF), r * m_CDF.back(), 
            [](float a, float b) -> bool {
                return a <= b;
            });
        uint32_t offset = it - std::begin(m_CDF);
        if((offset - 1) >= m_CDF.size() - 1) {
            std::cerr << "R: " << r << "; " << m_CDF.back() << std::endl;
        }
        
        return Sample1ui(offset - 1, (m_CDF[offset] - m_CDF[offset - 1]) * embree::rcp(m_CDF.back()));
    }

    __forceinline float pdf(uint32_t element) const {
        return (m_CDF[element + 1] - m_CDF[element]) * embree::rcp(m_CDF.back());
    }

    __forceinline float cdf(uint32_t element) const {
        return m_CDF[element] * embree::rcp(m_CDF.back());
    }

private:
    CDFContainer m_CDF; // Contains the cumulative distribution (un-normalized)
};

}

#endif
