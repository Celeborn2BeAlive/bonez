#ifndef _BONEZ_CAMERASAMPLE_HPP
#define _BONEZ_CAMERASAMPLE_HPP

#include "bonez/common/common.hpp"

namespace BnZ {

class CameraSample
{
public:
    CameraSample() {
    }

    CameraSample(const Vec2f& rasterPosition, const Vec2f& lensPosition):
        m_RasterPosition(rasterPosition), m_LensPosition(lensPosition) {
    }

    Vec2ui getPixel() const {
        return Vec2ui(m_RasterPosition);
    }

    const Vec2f& getRasterPosition() const {
        return m_RasterPosition;
    }

    const Vec2f& getLensPosition() const {
        return m_LensPosition;
    }

private:
    Vec2f m_RasterPosition; // Position of the sample in raster space
    Vec2f m_LensPosition; // Position of the sample in the lens of the camera
};

}

#endif // _BONEZ_CAMERASAMPLE_HPP
