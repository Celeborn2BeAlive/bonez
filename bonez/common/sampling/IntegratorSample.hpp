#ifndef _BONEZ_INTEGRATORSAMPLE_HPP_
#define _BONEZ_INTEGRATORSAMPLE_HPP_

#include "bonez/common/common.hpp"
#include "CameraSample.hpp"

namespace BnZ {

#if 0
/**
 * A multidimensional sample used by the IntegratorRenderer to sample the image plane
 * and to give the possibility to integrators to sample 1D and 2D domains.
 */
class IntegratorSample {
public:
    IntegratorSample(const size_t* p1DOffsets, const size_t* p2DOffsets, const float* pValues):
        m_p1DOffsets(p1DOffsets), m_p2DOffsets(p2DOffsets), m_pValues(pValues) {
    }

    Vec2i getPixel() const {
        return Vec2i(m_pValues[0], m_pValues[1]);
    }
    
    Vec2f getRasterPosition() const {
        return Vec2f(m_pValues[0], m_pValues[1]);
    }
    
    float get1DSample(size_t sampleSet, size_t sampleIdx) const {
        return m_pValues[m_p1DOffsets[sampleSet] + sampleIdx];
    }
    
    Vec2f get2DSample(size_t sampleSet, size_t sampleIdx) const {
        const float* ptr = m_pValues + m_p2DOffsets[sampleSet] + 2 * sampleIdx;
        return Vec2f(ptr[0], ptr[1]);
    }
    
public:
    const size_t* m_p1DOffsets;
    const size_t* m_p2DOffsets;

    const float* m_pValues;
};

#endif

class IntegratorSample: public CameraSample {
public:
    IntegratorSample():
        m_Rng(nullptr) {
    }

    IntegratorSample(embree::Random& rng, Vec2f rasterPosition, Vec2f lensPosition):
        CameraSample(rasterPosition, lensPosition), m_Rng(&rng) {
    }
    
    float get1DSample(size_t sampleSet, size_t sampleIdx) const {
        return m_Rng->getFloat();
    }
    
    Vec2f get2DSample(size_t sampleSet, size_t sampleIdx) const {
        return Vec2f(m_Rng->getFloat(), m_Rng->getFloat());
    }
    
    float getRandomFloat() const {
        return m_Rng->getFloat();
    }
    
    Vec2f getRandomVec2f() const {
        return Vec2f(m_Rng->getFloat(), m_Rng->getFloat());
    }
    
public:
    embree::Random* m_Rng;
};

}

#endif
