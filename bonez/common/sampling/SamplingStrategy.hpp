#ifndef _BONEZ_SAMPLINGSTRATEGY_HPP
#define _BONEZ_SAMPLINGSTRATEGY_HPP

#include "common.hpp"

namespace BnZ {

/**
 * @brief SamplingStrategy class is an interface to sample
 * a domain of values according to function.
 */
template<typename DomainType, typename RandomVariableType>
class SamplingStrategy {
public:
    typedef embree::Sample<DomainType> SampleType;

    virtual ~SamplingStrategy() {
    }

    virtual SampleType sample(const RandomVariableType& random) const = 0;

    virtual float pdf(const DomainType& value) const = 0;
};

}

#endif // _BONEZ_SAMPLINGSTRATEGY_HPP
