#ifndef _BONEZ_INTEGRATORSAMPLER_HPP_
#define _BONEZ_INTEGRATORSAMPLER_HPP_

#include <memory>

#include "IntegratorSample.hpp"

namespace BnZ {

class IntegratorSampler;
class IntegratorSampleBuffer;

typedef std::shared_ptr<IntegratorSampler> IntegratorSamplerPtr;

class IntegratorSampler {
public:
    friend class IntegratorSampleBuffer;

    virtual ~IntegratorSampler() {
    }
    
    /**
     * This method must be called before calling request2DSamples() to get
     * an appropriate number of 2D samples that match the sampler needs.
     * The result must be passed to request2DSamples instead of "count".
     */
    virtual size_t roundSampleCount(size_t count) {
        return count;
    }
    
    /**
     * Request a set of 2D samples for integration.
     * \param count The number of requested samples.
     * \return The index of the set.
     */
    size_t request2DSamples(size_t count) {
        m_2DRequestedCounts.push_back(count);
        return m_2DRequestedCounts.size() - 1;
    }
    
    /**
     * Request a set of 1D samples for integration.
     * \param count The number of requested samples.
     * \return The index of the set.
     */
    size_t request1DSamples(size_t count) {
        m_1DRequestedCounts.push_back(count);
        return m_1DRequestedCounts.size() - 1;
    }
    
    /**
     * Clone the sampler. Must be called by the threads in a multiple threading context.
     */
    virtual IntegratorSamplerPtr clone() const = 0;
    
    /**
     * Initialize the sampler to process a tile.
     * Must be called before the getNextSample() method.
     * \return The number of samples that will be returned by getNextSample() method.
     */
    virtual size_t init(size_t frameIdx, size_t xstart, size_t ystart, size_t xend, size_t yend) = 0;
    
    /**
     * Return the next sample.
     */
    virtual bool getNextSample(IntegratorSample& sample) = 0;

    /**
     * Return a string that resume the parameters of the sampler
     * and can be used in a filename.
     */
    virtual std::string toString() const = 0;

protected:
    std::vector<size_t> m_1DRequestedCounts;
    std::vector<size_t> m_2DRequestedCounts;
};

#if 0

class IntegratorSampleBuffer {
public:
    std::unique_ptr<float[]> m_FloatBuffer;
    
    size_t m_nMaxCount;
    size_t m_nSampleSet1DCount, m_nSampleSet2DCount;
    size_t m_nSampleSetCount;
    size_t m_nFloatCountPerSample;
    
    std::unique_ptr<size_t[]> m_SampleSetsOffsets;

    IntegratorSampleBuffer(IntegratorSampler& sampler) {
        m_nMaxCount = sampler.getMaxCount();
        
        m_nSampleSet1DCount = sampler.m_1DRequestedCounts.size();
        m_nSampleSet2DCount = sampler.m_2DRequestedCounts.size();
         
        m_nSampleSetCount = m_nSampleSet1DCount + m_nSampleSet2DCount;
        
        if(m_nSampleSetCount) {
            m_SampleSetsOffsets.reset(new size_t[m_nSampleSetCount]);
        }

        size_t* ptr = m_SampleSetsOffsets.get();
        
        size_t sample1DCount = 0;
        for(size_t count: sampler.m_1DRequestedCounts) {
            *ptr++ = 2 + sample1DCount; // 2 float for the raster position
            sample1DCount += count;
        }
        
        size_t sample2DCount = 0;
        for(size_t count: sampler.m_2DRequestedCounts) {
            *ptr++ = 2 + sample1DCount + 2 * sample2DCount;
            sample2DCount += count;
        }

        // 2 float for raster position and 2 float for each sample 2D
        m_nFloatCountPerSample = 2.f +
            sample1DCount + 2.f * sample2DCount;
        
        m_FloatBuffer.reset(new float[m_nMaxCount * m_nFloatCountPerSample]);
    }

    IntegratorSample operator [](size_t idx) const {
        assert(idx < m_nMaxCount);

        return IntegratorSample(m_SampleSetsOffsets.get(), m_SampleSetsOffsets.get() + m_nSampleSet1DCount,
            m_FloatBuffer.get() + m_nFloatCountPerSample * idx);
    }
    
    size_t size() const {
        return m_nMaxCount;
    }
};

#endif

#if 0
class IntegratorSampleBuffer {
public:
    embree::Random* m_Rng;
    std::unique_ptr<float[]> m_FloatBuffer;
    size_t m_nMaxCount;
    
    IntegratorSampleBuffer(IntegratorSampler& sampler): m_Rng(0){
        m_nMaxCount = sampler.getMaxCount();
        m_FloatBuffer.reset(new float[m_nMaxCount * 2]);
    }

    IntegratorSample operator [](size_t idx) const {
        assert(idx < m_nMaxCount);

        return IntegratorSample(*m_Rng, Vec2f(m_FloatBuffer[2 * idx], m_FloatBuffer[2 * idx + 1]));
    }
    
    size_t size() const {
        return m_nMaxCount;
    }
};

#endif

}

#endif
