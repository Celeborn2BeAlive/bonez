#ifndef _BONEZ_API_PROGRESS_HPP_
#define _BONEZ_API_PROGRESS_HPP_

#include <ostream>
#include <iomanip>
#include <embree/common/sys/sync/atomic.h>
#include <embree/common/sys/sync/mutex.h>

namespace BnZ {

class ThreadSafeProgress {
    embree::MutexSys m_Mutex;
    size_t m_nElementCount;
    size_t m_nProceedCount;
public:
    ThreadSafeProgress():
        m_nElementCount(0), m_nProceedCount(0) {
    }

    void init(size_t elementCount) {
        m_nElementCount = elementCount;
        m_nProceedCount = 0;
    }

    void start() {
    }
    
    void print() {
        float percentage = m_nProceedCount * 100.f / m_nElementCount;
        std::cout << "\r" << percentage << " %" << std::flush;
    }

    void next() {
        embree::Lock<embree::MutexSys> lock(m_Mutex);
        ++m_nProceedCount;
        print();
    }
    
    void advance(size_t count) {
        embree::Lock<embree::MutexSys> lock(m_Mutex);
        m_nProceedCount += count;
        print();
    }
    
    void set(size_t count) {
        embree::Lock<embree::MutexSys> lock(m_Mutex);
        m_nProceedCount = count;
        print();
    }

    void end() {
        std::cout << std::endl;
    }
};

class Progress {
    std::string m_sName;
    size_t m_nElementCount;
    size_t m_nProceedCount;
    double m_fStart;
public:
    Progress():
        m_nElementCount(0), m_nProceedCount(0) {
    }

    void init(const std::string& name, size_t elementCount) {
        m_sName = name;
        m_nElementCount = elementCount;
        m_nProceedCount = 0;
        m_fStart = embree::getSeconds();
    }

    void print() {
        float percentage = m_nProceedCount * 100.f / m_nElementCount;
        std::cout << "\r" << percentage << " %" << std::flush;
    }

    void next() {
        ++m_nProceedCount;
        print();
    }
    
    void advance(size_t count) {
        m_nProceedCount += count;
        print();
    }
    
    void set(size_t count) {
        m_nProceedCount = count;
        print();
    }

    void end() {
        std::cout << m_sName << ": " << (embree::getSeconds() - m_fStart) << " seconds." << std::endl;
    }
};

}

#endif
