#ifndef _BONEZ_API_PARAMS_HPP_
#define _BONEZ_API_PARAMS_HPP_

#include <unordered_map>
#include <memory>
#include <vector>
#include <typeinfo>
#include "types.hpp"

namespace BnZ {

class ParamValue;

typedef std::shared_ptr<ParamValue> ParamPtr;
typedef std::vector<ParamPtr> ParamArray;

template<typename T>
inline ParamPtr makeParam(const T& value) {
    return std::make_shared<ParamValue>(value);
}

/**
 * Represents a set of named parameters.
 */
class ParamSet {
public:
    typedef std::unordered_map<std::string, ParamPtr> Map;
    
    std::string name; // The name of the set of parameters
    Map map; // The container for the parameters
 
    ParamSet(const std::string& name = ""):
        name(name) {
    }

    template<typename... T>
    ParamSet(const std::string& name, T&&... args):
        name(name) {
        set(std::forward<T>(args)...);
    }
    
    size_t size() const {
        return map.size();
    }
    
    bool empty() const {
        return map.empty();
    }
    
    void clear() {
        map.clear();
    }
    
    /**
     * Return true if a parameter with the given name exists in the set
     */
    bool hasParam(const std::string& name) const {
        return map.find(name) != std::end(map);
    }
    
    template<typename T>
    T get(const std::string& name, const T& defaultValue) const;
    
    template<typename T>
    T get(const std::string& name) const;
    
    template<typename T>
    void set(const std::string& name, const T& value) {
        map[name] = makeParam(value);
    }
    
    void set(const std::string& name, const ParamPtr& value) {
        map[name] = value;
    }

    template<typename T, typename... Ts>
    void set(const std::string& name, const T& value, Ts&&... values) {
        set(name, value);
        set(std::forward<Ts>(values)...);
    }

    void set() {
    }
};

class ParamValue {
public:
    enum Type {
        INT, FLOAT, VEC2F, VEC3F, STRING, BOOL, PARAMSET, ARRAY
    };
    
    Type type;
    
    union {
        int64_t intV;
        float floatV;
        Vec2f vec2fV;
        Vec3f vec3fV;
        bool boolV;
    };
    
    std::string stringV;
    ParamSet paramSetV;
    ParamArray arrayV;
    
    ParamValue(int v):
        type(INT), intV(v) {
    }

    ParamValue(uint32_t v):
        type(INT), intV(v) {
    }
    
    ParamValue(size_t v):
        type(INT), intV(v) {
    }
    
    ParamValue(int64_t v):
        type(INT), intV(v) {
    }
    
    ParamValue(float v):
        type(FLOAT), floatV(v) {
    }
    
    ParamValue(double v):
        type(FLOAT), floatV(v) {
    }
    
    ParamValue(Vec2f v):
        type(VEC2F), vec2fV(v) {
    }
    
    ParamValue(Vec3f v):
        type(VEC3F), vec3fV(v) {
    }
    
    ParamValue(Col3f v):
        type(VEC3F), vec3fV(v.r, v.g, v.b) {
    }
    
    ParamValue(bool v):
        type(BOOL), boolV(v) {
    }
    
    ParamValue(const char* v):
        type(STRING), stringV(v) {
    }
    
    ParamValue(const std::string& v):
        type(STRING), stringV(v) {
    }
    
    ParamValue(const ParamSet& v):
        type(PARAMSET), paramSetV(v) {
    }
    
    ParamValue(const ParamArray& v):
        type(ARRAY), arrayV(v) {
    }
    
    template<typename T>
    void set(const T& v) {
        *this = ParamValue(v);
    }
    
    void get(int& v) const {
        if(type == INT) {
            v = intV;
            return;
        }
        if(type == FLOAT) {
            v = floatV;
            return;
        }
        throw std::bad_cast();
    }
    
    void get(int64_t& v) const {
        if(type == INT) {
            v = intV;
            return;
        }
        if(type == FLOAT) {
            v = floatV;
            return;
        }
        throw std::bad_cast();
    }
    
    void get(size_t& v) const {
        if(type == INT) {
            v = intV;
            return;
        }
        if(type == FLOAT) {
            v = floatV;
            return;
        }
        throw std::bad_cast();
    }
    
    void get(float& v) const {
        if(type == INT) {
            v = intV;
            return;
        }
        if(type == FLOAT) {
            v = floatV;
            return;
        }
        throw std::bad_cast();
    }
    
    void get(Vec2f& v) const {
        if(type == VEC2F) {
            v = vec2fV;
            return;
        }
        throw std::bad_cast();
    }
    
    void get(Vec3f& v) const {
        if(type == VEC3F) {
            v = vec3fV;
            return;
        }
        throw std::bad_cast();
    }
    
    void get(Col3f& v) const {
        if(type == VEC3F) {
            v = Col3f(vec3fV.x, vec3fV.y, vec3fV.z);
            return;
        }
        throw std::bad_cast();
    }
    
    void get(bool& v) const {
        if(type == BOOL) {
            v = boolV;
            return;
        }
        throw std::bad_cast();
    }
    
    void get(std::string& v) const {
        if(type == STRING) {
            v = stringV;
            return;
        }
        throw std::bad_cast();
    }
    
    void get(ParamSet& v) const {
        if(type == PARAMSET) {
            v = paramSetV;
            return;
        }
        throw std::bad_cast();
    }
    
    void get(ParamArray& v) const {
        if(type == ARRAY) {
            v = arrayV;
            return;
        }
        if(type == VEC2F) {
            v.resize(2);
            v[0] = makeParam(vec2fV.x);
            v[1] = makeParam(vec2fV.y);
            return;
        }
        if(type == VEC3F) {
            v.resize(3);
            v[0] = makeParam(vec3fV.x);
            v[1] = makeParam(vec3fV.y);
            v[2] = makeParam(vec3fV.y);
            return;
        }
        throw std::bad_cast();
    }

    template<typename T>
    T get() const {
        T v;
        get(v);
        return v;
    }
};


template<typename T>
T ParamSet::get(const std::string& name, const T& defaultValue) const {
    auto it = map.find(name);
    if(it == std::end(map)) {
        std::clog << "ParamSet: parameter " << name << " not found in " << 
            this->name << ", return default value." << std::endl;
        return defaultValue;
    }
    T value;
    (*it).second->get(value);
    return value;
}

template<typename T>
T ParamSet::get(const std::string& name) const {
    auto it = map.find(name);
    if(it == std::end(map)) {
        throw std::runtime_error("ParamSet: parameter " + name + " not found in " + this->name);
    }
    T value;
    (*it).second->get(value);
    return value;
}

}

#endif
