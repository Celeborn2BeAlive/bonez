#ifndef _BONEZ_CAMERA_HPP
#define _BONEZ_CAMERA_HPP

#include "bonez/common/common.hpp"
#include "bonez/common/sampling/CameraSample.hpp"

namespace BnZ {

class Camera
{
public:
    virtual ~Camera() {
    }

    Camera() {
    }

    Camera(const AffineSpace3f& localToWorld):
        m_LocalToWorld(localToWorld) {
    }

    virtual Ray ray(const CameraSample& sample) const = 0;

    /* Position in [0, 1] * [0, 1] */
    virtual Ray ray(const Vec2f& position) const = 0;

    const Vec3f& getPosition() const {
        return m_LocalToWorld.p;
    }

    Vec3f getPoint() const {
        return embree::xfmPoint(m_LocalToWorld, Vec3f(0, 0, 1));
    }

    Vec3f getUp() const {
        return embree::xfmPoint(m_LocalToWorld, Vec3f(0, 1, 0));
    }

    virtual float area() const = 0;

    const AffineSpace3f& getLocalToWorldTransform() const {
        return m_LocalToWorld;
    }

    void lookAt(const Vec3f& eye, const Vec3f& point, const Vec3f& up) {
        m_LocalToWorld = AffineSpace3f::lookAtPoint(eye, point, up);
    }

protected:
    AffineSpace3f m_LocalToWorld;
};

}

#endif // _BONEZ_CAMERA_HPP
