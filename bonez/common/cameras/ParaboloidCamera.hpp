#ifndef _BONEZ_PARABOLOIDCAMERA_HPP
#define _BONEZ_PARABOLOIDCAMERA_HPP

#include "Camera.hpp"

namespace BnZ {

class ParaboloidCamera: public Camera {
public:
    ParaboloidCamera() {
    }

    ParaboloidCamera(const Vec3f& P, const Vec3f& N,
                     size_t xResolution, size_t yResolution):
        Camera(AffineSpace3f(embree::frame(N), P)), m_RasterToNDCScale(-2.f / xResolution, -2.f / yResolution) {
    }

    ParaboloidCamera(AffineSpace3f frame, size_t xResolution, size_t yResolution):
        Camera(frame), m_RasterToNDCScale(-2.f / xResolution, -2.f / yResolution) {
    }

    virtual Ray ray(const CameraSample& sample) const {
        Vec2f ndcPosition = Vec2f(1, 1) + sample.getRasterPosition() * m_RasterToNDCScale;

        /*
        float x = ndcPosition.x, y = ndcPosition.y;
        float r, phi;
        if(x > -y && x > y) {
            r = x;
            phi = float(embree::pi) * 0.25 * y / x;
        }
        else if(x < y && x > -y) {
            r = y;
            phi = float(embree::pi) * 0.25 * (2 - x / y);
        }
        else if(x < -y && x < y) {
            r = -x;
            phi = float(embree::pi) * 0.25 * (4 + y / x);
        }
        else if(x > y && x < -y) {
            r = -y;
            phi = float(embree::pi) * 0.25 * (6 - x / y);
        }

        ndcPosition = r * Vec2f(cos(phi), sin(phi));
        float z = 0.5 - 0.5 * r; // Paraboloid equation

        // N is the normal vector of the point (x, y, z) of the paraboloid
        Vec3f N = embree::normalize(Vec3f(ndcPosition.x, ndcPosition.y, 1.f));
        Vec3f d = 2 * N.z * N - Vec3f(0.f, 0.f, 1.f);
        return Ray(m_LocalToWorld.p, xfmVector(m_LocalToWorld, d));
        */

        float r = embree::sqr(ndcPosition.x) + embree::sqr(ndcPosition.y);
        float z = 0.5 - 0.5 * r; // Paraboloid equation

        if(z < 0) {
            return Ray(Vec3f(0.f), Vec3f(0.f), 0.f, 0.f);
        }

        // N is the normal vector of the point (x, y, z) of the paraboloid
        Vec3f N = embree::normalize(Vec3f(ndcPosition.x, ndcPosition.y, 1.f));
        Vec3f d = 2 * N.z * N - Vec3f(0.f, 0.f, 1.f);

        return Ray(m_LocalToWorld.p, xfmVector(m_LocalToWorld, d),
                   embree::reduce_max(embree::abs(m_LocalToWorld.p)) * 128.f * float(embree::ulp));
    }

    /* Position in [0, 1] * [0, 1] */
    virtual Ray ray(const Vec2f& position) const {
        Vec2f ndcPosition = Vec2f(1, 1) - position * 2.f;
        float r = embree::sqr(ndcPosition.x) + embree::sqr(ndcPosition.y);
        float z = 0.5 - 0.5 * r; // Paraboloid equation

        if(z < 0) {
            return Ray(Vec3f(0.f), Vec3f(0.f), 0.f, 0.f);
        }

        // N is the normal vector of the point (x, y, z) of the paraboloid
        Vec3f N = embree::normalize(Vec3f(ndcPosition.x, ndcPosition.y, 1.f));
        Vec3f d = 2 * N.z * N - Vec3f(0.f, 0.f, 1.f);

        return Ray(m_LocalToWorld.p, xfmVector(m_LocalToWorld, d),
                   embree::reduce_max(embree::abs(m_LocalToWorld.p)) * 128.f * float(embree::ulp));
    }

    virtual float area() const {
        return float(embree::two_pi);
    }

private:
    Vec2f m_RasterToNDCScale;
};

}

#endif // _BONEZ_PARABOLOIDCAMERA_HPP
