#ifndef _BONEZ_HEMISPHERECAMERA_HPP
#define _BONEZ_HEMISPHERECAMERA_HPP

#include "Camera.hpp"
#include "bonez/common/maths.hpp"

namespace BnZ {

class HemisphereCamera: public Camera {
public:
    HemisphereCamera() {
    }

    HemisphereCamera(const Vec3f& P, const Vec3f& N,
                     size_t xResolution, size_t yResolution):
        Camera(coordinateSystem(P, N)), m_RcpResolution(1.f / xResolution, 1.f / yResolution) {
    }

    virtual Ray ray(const CameraSample& sample) const {
        Vec2f ndcPosition = sample.getRasterPosition() * m_RcpResolution;
        float theta = float(embree::pi) * 0.5f * ndcPosition.y;
        float phi = ndcPosition.x * float(embree::two_pi);
        Vec3f worldPosition = xfmPoint(m_LocalToWorld, sphericalToCartesian(phi, theta));
        return Ray(m_LocalToWorld.p, embree::normalize(worldPosition - m_LocalToWorld.p));
    }

    /* Position in [0, 1] * [0, 1] */
    virtual Ray ray(const Vec2f& ndcPosition) const {
        float theta = float(embree::pi) * 0.5f * ndcPosition.y;
        float phi = ndcPosition.x * float(embree::two_pi);
        Vec3f worldPosition = xfmPoint(m_LocalToWorld, sphericalToCartesian(phi, theta));
        return Ray(m_LocalToWorld.p, embree::normalize(worldPosition - m_LocalToWorld.p));
    }

    virtual float area() const {
        return float(embree::two_pi);
    }

private:
    Vec2f m_RcpResolution;
};

}

#endif // _BONEZ_HEMISPHERECAMERA_HPP
