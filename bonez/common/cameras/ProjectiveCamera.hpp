#ifndef _BONEZ_PROJECTIVECAMERA_HPP_
#define _BONEZ_PROJECTIVECAMERA_HPP_

#include "Camera.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace BnZ {

class ProjectiveCamera: public Camera {
public:
    ProjectiveCamera() {
    }

    ProjectiveCamera(const AffineSpace3f& localToWorld,
                     size_t xResolution, size_t yResolution,
                     float lensRadius, float focalLength,
                     const glm::mat4& projectionMatrix):
        Camera(localToWorld), m_ProjectionMatrix(projectionMatrix),
        m_fLensRadius(lensRadius), m_fFocalLength(focalLength)
    {
        glm::mat4 rasterToNDC = glm::mat4(
                    glm::vec4(-2.f / xResolution, 0, 0, 0),
                    glm::vec4(0, -2.f / yResolution, 0, 0),
                    glm::vec4(0, 0, 1, 0),
                    glm::vec4(1, 1, 0, 1)
        );
        m_NDCToCamera = glm::inverse(projectionMatrix);
        m_RasterToCamera = m_NDCToCamera * rasterToNDC;
    }

    const glm::mat4 getProjectionMatrix() const {
        return m_ProjectionMatrix;
    }

    virtual float area() const {
        glm::vec4 lower(-1, -1, -1, 1), upper(1, 1, -1, 1);
        glm::vec4 localLower = m_NDCToCamera * lower, localUpper = m_NDCToCamera * upper;
        localLower /= localLower.w;
        localUpper /= localUpper.w;
        return embree::abs(localUpper.x - localLower.x) * embree::abs(localUpper.y - localLower.y);
    }

    virtual Ray ray(const CameraSample& sample) const {
        glm::vec4 glmLocalPosition = m_RasterToCamera *
                glm::vec4(sample.getRasterPosition().x, sample.getRasterPosition().y, -1.f, 1.f);
        glmLocalPosition /= glmLocalPosition.w;

        Vec3f localPosition(glmLocalPosition.x, glmLocalPosition.y, -glmLocalPosition.z);

        if(m_fLensRadius > 0.f) {
           Vec2f lensSample = embree::uniformSampleDisk(sample.getLensPosition(), m_fLensRadius);
           float t = m_fFocalLength / localPosition.z;
           Vec3f Pfocus = t * localPosition;

           Vec3f localOrigin(lensSample.x, lensSample.y, 0.f);
           Vec3f localDir = embree::normalize(Pfocus - localOrigin);

           return Ray(xfmPoint(m_LocalToWorld, localOrigin), xfmVector(m_LocalToWorld, localDir));
        }

        Vec3f worldPosition = embree::xfmPoint(m_LocalToWorld, localPosition);
        return Ray(m_LocalToWorld.p, embree::normalize(worldPosition - m_LocalToWorld.p));
    }

    virtual Ray ray(const Vec2f& position) const {
        glm::vec4 localPosition = m_NDCToCamera * glm::vec4(-1.f + 2.f * position.x, -1.f + 2.f * position.y, -1.f, 1.f);
        localPosition /= localPosition.w;
        Vec3f worldPosition = embree::xfmPoint(m_LocalToWorld, Vec3f(localPosition.x, localPosition.y, -localPosition.z));
        return Ray(m_LocalToWorld.p, embree::normalize(worldPosition - m_LocalToWorld.p));
    }

private:
    glm::mat4 m_ProjectionMatrix;
    glm::mat4 m_NDCToCamera;
    glm::mat4 m_RasterToCamera;

    float m_fLensRadius;
    float m_fFocalLength;
};

}

#endif
