#ifndef _BONEZ_COMMON_HPP_
#define _BONEZ_COMMON_HPP_

/**
 * Common stuff for all the engine.
 */

#define BONEZ_MULTITHREADING_ENABLED 1
#define override

#include "types.hpp"
#include "Params.hpp"
#include "itertools/itertools.hpp"
#include "DebugOutput.hpp"

template<typename TaskFunctor>
struct ParallelProcessHelper {
    const TaskFunctor& m_Task;
    uint32_t m_nTaskCount;
    uint32_t m_nBatchSize;
    embree::Atomic m_nBatchID;

    static void runRenderThread(const embree::TaskScheduler::ThreadInfo& thread,
                                ParallelProcessHelper* self, size_t threadID) {
        self->renderThread();
    }

    ParallelProcessHelper(const TaskFunctor& task, uint32_t taskCount):
        m_Task(task), m_nTaskCount(taskCount), m_nBatchID(0) {
        uint32_t threadCount = embree::scheduler->getNumThreads();
        m_nBatchSize = embree::max(1u, m_nTaskCount / threadCount);
    }

    void renderThread() {
        while (true) {
            uint32_t batchID = m_nBatchID++;
            uint32_t taskID = batchID * m_nBatchSize;
            if(taskID >= m_nTaskCount) {
                break;
            }

            uint32_t end = embree::min(taskID + m_nBatchSize, m_nTaskCount);

            while (taskID < end) {
                m_Task(taskID);
                ++taskID;
            }
        }
    }
};

template<typename TaskFunctor>
void parallelProcess(uint32_t taskCount, const TaskFunctor& task, bool debug = false) {
    typedef ParallelProcessHelper<TaskFunctor> Helper;
    Helper helper(task, taskCount);

    uint32_t nbThreads = debug ? 1 : embree::scheduler->getNumThreads();

    embree::scheduler->start();
    embree::scheduler->addTask(embree::TaskScheduler::ThreadInfo(), embree::TaskScheduler::GLOBAL_FRONT,
                               (embree::TaskScheduler::runFunction) &Helper::runRenderThread, &helper, nbThreads,
                               nullptr, nullptr, "parallelProcess");
    embree::scheduler->stop();
}

#endif
