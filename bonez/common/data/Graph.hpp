#ifndef _BONEZ_DATAS_GRAPH_
#define _BONEZ_DATAS_GRAPH_

#include <cassert>
#include <vector>
#include <iterator>
#include <list>
#include "bonez/common/common.hpp"

namespace BnZ {

static constexpr uint32_t UNDEFINED_NODE = std::numeric_limits<uint32_t>::max();

typedef uint32_t GraphNodeIndex;

typedef std::vector<GraphNodeIndex> AdjacencyList;
typedef std::vector<AdjacencyList> Graph;

}

#endif
