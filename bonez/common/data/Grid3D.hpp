#ifndef _BONEZ_DATAS_GRID3D_HPP_
#define _BONEZ_DATAS_GRID3D_HPP_

#include <cstdint>
#include <vector>

#include "bonez/common/common.hpp"

namespace BnZ {

template<typename T>
class Grid3D: std::vector<T> {
    typedef std::vector<T> Base;
public:
    typedef typename Base::value_type value_type;
    typedef typename Base::reference reference;
    typedef typename Base::iterator iterator;
    typedef typename Base::const_iterator const_iterator;
    using Base::data;
    using Base::size;
    using Base::empty;
    using Base::begin;
    using Base::end;
    using Base::operator [];

    Grid3D():
        m_nWidth(0),
        m_nHeight(0),
        m_nDepth(0),
        m_nSliceSize(0) {
    }

    Grid3D(size_t width, size_t height, size_t depth):
        Base(width * height * depth),
        m_nWidth(width),
        m_nHeight(height),
        m_nDepth(depth),
        m_nSliceSize(width * height) {
    }

    Grid3D(size_t width, size_t height, size_t depth, T value):
        Base(width * height * depth, value),
        m_nWidth(width),
        m_nHeight(height),
        m_nDepth(depth),
        m_nSliceSize(width * height) {
    }

    uint32_t offset(uint32_t x, uint32_t y, uint32_t z) const {
        return x + y * m_nWidth + z * m_nSliceSize;
    }

    uint32_t offset(const Vec3i& coords) const {
        return offset(coords.x, coords.y, coords.z);
    }

    Vec3i coords(uint32_t offset) const {
        Vec3i c;
        c.x = offset % m_nWidth;
        c.y = ((offset - c.x) / m_nWidth) % m_nHeight;
        c.z = ((offset - c.x - c.y * m_nWidth)) / m_nSliceSize;
        return c;
    }

    value_type operator ()(uint32_t x, uint32_t y, uint32_t z) const {
        return (*this)[offset(x, y, z)];
    }

    reference operator ()(uint32_t x, uint32_t y, uint32_t z) {
        return (*this)[offset(x, y, z)];
    }

    value_type operator ()(const Vec3i& coords) const {
        return (*this)(coords.x, coords.y, coords.z);
    }

    reference operator ()(const Vec3i& coords) {
        return (*this)(coords.x, coords.y, coords.z);
    }

    bool contains(int x, int y, int z) const {
        return x >= 0 &&
                y >= 0 &&
                z >= 0 &&
                x < (int)m_nWidth &&
                y < (int)m_nHeight &&
                z < (int)m_nDepth;
    }

    bool contains(const Vec3i& coords) const {
        return contains(coords.x, coords.y, coords.z);
    }

    size_t width() const {
        return m_nWidth;
    }

    size_t height() const {
        return m_nHeight;
    }

    size_t depth() const {
        return m_nDepth;
    }

private:
    size_t m_nWidth, m_nHeight, m_nDepth;
    size_t m_nSliceSize;
};

}

#endif
