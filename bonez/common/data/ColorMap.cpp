#include "ColorMap.hpp"

namespace BnZ {

const ColorMap HEAT_MAP = { Col3f(0, 0, 1), Col3f(0, 1, 0), Col3f(1, 0, 0) };

}
