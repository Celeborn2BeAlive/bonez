#ifndef _BONEZ_ITERTOOLS_HPP
#define _BONEZ_ITERTOOLS_HPP

#include <algorithm>
#include "Indexer.hpp"
#include "Range.hpp"

namespace BnZ {

template<typename Functor>
inline void repeat(size_t count, Functor f) {
    while(count--) {
        f();
    }
}

template<typename InputContainer, typename OutputContainer, typename UnaryOperator>
inline void transform(InputContainer&& in, OutputContainer&& out, UnaryOperator&& op) {
    std::transform(begin(in), end(in), begin(out), op);
}

}

#endif // _BONEZ_ITERTOOLS_HPP
