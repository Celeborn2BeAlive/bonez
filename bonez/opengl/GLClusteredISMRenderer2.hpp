#ifndef _BONEZ_GLCLUSTEREDISMRENDERER2_HPP_
#define _BONEZ_GLCLUSTEREDISMRENDERER2_HPP_

#include "bonez/scene/Scene.hpp"
#include "opengl/shaders/GLSLProgramBuilder.hpp"
#include "opengl/scene/GLScene.hpp"

#include "bonez/scene/lights/vpls.hpp"

#include <vector>
#include <glm/glm.hpp>

#include "bonez/opengl/utils/GLFramebuffer.hpp"
#include "bonez/opengl/utils/GLTextureBuffer.hpp"
#include "bonez/opengl/utils/GLBuffer.hpp"
#include "bonez/opengl/utils/GLQuery.hpp"

#include "analysis/GLVisualDataRenderer.hpp"

#include "GLShadowMapRenderer.hpp"
#include "GLISMRenderer.hpp"

#include "GLResultRenderer.hpp"

#include <atb.hpp>

namespace BnZ {

class GLClusteredISMRenderer2 {
public:
    void setResolution(size_t width, size_t height);

    void setProjectionMatrix(const glm::mat4& projectionMatrix) {
        m_ProjectionMatrix = projectionMatrix;
    }

    void setViewMatrix(const glm::mat4& viewMatrix) {
        m_ViewMatrix = viewMatrix;
    }

    void setUp();

    void sampleScene(const Scene& scene, const Camera& camera);

    void geometryPass(const GLScene& scene);

    void shadingPass(int x, int y, size_t width, size_t height,
        const VPLContainer& vpls, const Scene& scene);

    void drawBuffers(int x, int y, size_t width, size_t height);

    void drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay);

    void displayGClusters(GLVisualDataRenderer& renderer, const Scene& scene);

    void drawISMs(int x, int y, size_t width, size_t height);

    void computeTimings();

    void setPrimaryLightSource(const Vec3f& P, const Col3f& L, const GLScene& glScene);

    void drawPrimaryLightShadowMap(int x, int y, size_t width, size_t height);

    void exposeIO(TwBar* bar);

    bool doesRenderIrradiance() const {
        return m_bRenderIrradiance;
    }

    bool doesRenderDirectLight() const {
        return m_bDoDirectLightPass;
    }

    float getDistanceClamp() const {
        return m_fDistClamp;
    }

    const GLISMContainer& getVPLISMContainer() const {
        return m_VPLData.m_VPLISMContainer;
    }

    const GLISMContainer& getClusterISMContainer() const {
        return m_GeometryClustersData.m_ClusterISMContainer;
    }

    void processPixel(int pixelX, int pixelY);

private:
    static const uint32_t TILE_SIZE = 32;

    uint32_t m_nFrameIdx = 0;
    GLResultRenderer m_ResultRenderer;
    Framebuffer m_Framebuffer = Framebuffer(ISM_RESOLUTION, ISM_RESOLUTION);
    const Scene* m_pScene = nullptr;

    VPLContainer m_VPLContainer;

    glm::vec3 m_SelectedCacheP;
    glm::vec3 m_SelectedCacheN;
    void getClusterCache(uint32_t tileIdx, uint32_t clusterLocalIdx, glm::vec3& P, glm::vec3& N);

    void computeCPUIrradianceCaches();

    GLQuery m_ComputeVPLISMsTimeElapsedQuery;
    void computeVPLISMs(const VPLContainer& vpls);

    GLQuery m_ComputeClusterISMsTimeElapsedQuery;
    void computeClusterISMs();

    void sendVPLs(const VPLContainer& vpls, const Scene& scene);

    glm::mat4 m_ProjectionMatrix { 1.f };
    glm::mat4 m_ViewMatrix { 1.f };

    GLPoints m_SampledScene;

    glm::uvec2 m_TileCount;

    enum GBufferTextureType {
        GBUFFER_NORMAL_DEPTH,
        GBUFFER_DIFFUSE,
        GBUFFER_GLOSSY_SHININESS,
        GBUFFER_COUNT
    };
    GLFramebuffer<GBUFFER_COUNT> m_GBuffer;

    enum TextureUnits {
        TEXUNIT_MAT_DIFFUSE,
        TEXUNIT_MAT_GLOSSY,
        TEXUNIT_MAT_SHININESS,
        TEXUNIT_GBUFFER_NORMALDEPTH,
        TEXUNIT_GBUFFER_DIFFUSE,
        TEXUNIT_GBUFFER_GLOSSYSHININESS,
        //TEXUNIT_GBUFFER_GEOMETRYCLUSTERS,

        // Clustered shading textures
        TEXUNIT_TEX2D_GEOMETRYCLUSTERS,

        TEXUNIT_TEXCUBEMAP_SHADOWMAP,

        TEXUNIT_COUNT
    };

    enum ImageUnits {
        IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS
    };

    struct GeometryPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        GLMaterialUniforms m_MaterialUniforms;
        gloops::Uniform m_uMVPMatrix;
        gloops::Uniform m_uMVMatrix;
        gloops::Uniform m_uNormalMatrix;
        gloops::Uniform m_uSy; // Number of subdivision in the Y direction
        gloops::Uniform m_uHalfFOVY;
        gloops::Uniform m_uZNear;
        gloops::Uniform m_uZFar;

        GLQuery m_TimeElapsedQuery;

        GeometryPassData();
    };

    struct GBufferUniform {
        gloops::Uniform normalDepthSampler;
        gloops::Uniform diffuseSampler;
        gloops::Uniform glossyShininessSampler;

        GBufferUniform(const gloops::Program& program):
            normalDepthSampler(program, "uGBuffer.normalDepthSampler", true),
            diffuseSampler(program, "uGBuffer.diffuseSampler", true),
            glossyShininessSampler(program, "uGBuffer.glossyShininessSampler", true) {

            program.use();

            normalDepthSampler.set(TEXUNIT_GBUFFER_NORMALDEPTH);
            diffuseSampler.set(TEXUNIT_GBUFFER_DIFFUSE);
            glossyShininessSampler.set(TEXUNIT_GBUFFER_GLOSSYSHININESS);
        }
    };

    GeometryPassData m_GeometryPassData;

    struct FindUniqueGeometryClusterPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uClustersImage;
        gloops::Uniform m_uInvProjMatrix;
        gloops::Uniform m_uRcpViewMatrix;
        gloops::Uniform m_uScreenSize;
        gloops::Uniform m_uClusterCountsBuffer;
        gloops::Uniform m_uClusterBBoxLowerBuffer;
        gloops::Uniform m_uClusterBBoxUpperBuffer;
        gloops::Uniform m_uClusterNormalBuffer;

        gloops::Uniform m_uUseNormalClustering;

        gloops::Uniform m_uSy; // Number of subdivision in the Y direction
        gloops::Uniform m_uHalfFOVY;
        gloops::Uniform m_uZNear;
        gloops::Uniform m_uZFar;

        GBufferUniform m_uGBuffer;

        GLQuery m_TimeElapsedQuery;

        FindUniqueGeometryClusterPassData();
    };

    FindUniqueGeometryClusterPassData m_FindUniqueGeometryClusterPassData;

    void findUniqueGeometryClusters();

    static const uint32_t ISM_RESOLUTION = 64;

    struct GeometryClustersData {
        // This texture maps each pixel of the image to the local offset of its cluster.
        // The local offset is an index in the screen-space tile from 0 to the number of clusters in this tile
        // minus 1
        gloops::TextureObject m_GeometryClustersTexture;

        // This buffer contains for each tile (Tx, Ty) with index Tx + Ty * Sx the number of clusters
        // contains in this tile.
        // It is used by the Count Geometry Clusters Pass to count the total number of clusters
        // In this implementation, the total number of tiles must be less than the number of threads
        // in a work item (1024 on my computer)
        GLBuffer m_GClusterCountsBuffer;

        // This buffer contain for each tile a global offset for the clusters contained in this tile
        // After the CountGeometryClusterPass, let localID be the local cluster ID of a tile (Tx, Ty). Then it's
        // global cluster ID is globalID = m_GClusterTilesOffsetsBuffer[Tx + Ty * Sx] + localID
        GLBuffer m_GClusterTilesOffsetsBuffer;

        // A buffer that will contain the total number of clusters (buffer of size 1)
        GLBuffer m_GClusterTotalCountBuffer;
        // CPU version of the previous buffer
        GLuint m_nGClusterCount;

        GLBuffer m_GClusterToTileBuffer;


        // This buffers contain data computed for each cluster during the find unique geometr cluster pass
        // Let localID be the local index of the cluster (from 0 to 1023) in its tile (Tx, Ty)
        // Then its data are stored at address (Tx + Ty * Sy) * 1024 + localID
        // Indeed, the globalID (from 0 to the number of cluster - 1) is computed during the next pass (count
        // geometry cluster)
        GLBuffer m_ClusterBBoxLowerBuffer;
        GLBuffer m_ClusterBBoxUpperBuffer;
        GLBuffer m_ClusterNormalBuffer;

        // This buffers contain data computed for each cluster during the light assignment pass
        // The data for a cluster is stored at the address which is its global ID
        GLBuffer m_ClusterIrradianceBuffer;
        GLBuffer m_ClusterMeanIncidentDirectionBuffer;

        GLISMContainer m_ClusterISMContainer;

        GeometryClustersData(): m_ClusterISMContainer(ISM_RESOLUTION) {}
    };

    GeometryClustersData m_GeometryClustersData;

    struct CountGeometryClusterPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uTileCount;

        gloops::Uniform m_uClusterCountsBuffer;
        gloops::Uniform m_uClusterTilesOffsetsBuffer;
        gloops::Uniform m_uClusterCount;
        gloops::Uniform m_uClusterToTileBuffer;

        GLQuery m_TimeElapsedQuery;

        CountGeometryClusterPassData();
    };

    CountGeometryClusterPassData m_CountGeometryClusterPassData;

    struct LightAssignmentPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uVPLCount;
        gloops::Uniform m_uClusterCount;

        gloops::Uniform m_uNoShadow;

        gloops::Uniform m_uVPLBuffer;
        gloops::Uniform m_uVPLOffsetCountsBuffer;
        gloops::Uniform m_uVPLIndexBuffer;

        gloops::Uniform m_uClusterTilesOffsetsBuffer;
        gloops::Uniform m_uClusterToTileBuffer;
        gloops::Uniform m_uClusterBBoxLowerBuffer;
        gloops::Uniform m_uClusterBBoxUpperBuffer;
        gloops::Uniform m_uClusterNormalBuffer;
        gloops::Uniform m_uClusterIrradianceBuffer;
        gloops::Uniform m_uClusterMeanIncidentDirectionBuffer;

        GLISMRenderer::ISMUniforms m_uISMData;
        gloops::Uniform m_uClusterISMBuffer;
        gloops::Uniform m_uClusterViewMatrixBuffer;
        gloops::Uniform m_uZNear;
        gloops::Uniform m_uZFar;
        gloops::Uniform m_uISMOffset;

        gloops::Uniform m_uDistClamp;

        gloops::Uniform m_uUseIrradianceCache;

        GLBuffer m_ClusterISMBuffer;
        GLBuffer m_ClusterViewMatrixBuffer;

        GLQuery m_TimeElapsedQuery;

        LightAssignmentPassData();
    };

    LightAssignmentPassData m_LightAssignmentPassData;

    struct VPLShadingPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uInvProjMatrix;
        gloops::Uniform m_uTileCount;
        gloops::Uniform m_uVPLCount;
        gloops::Uniform m_uDisplayCostPerPixel;
        gloops::Uniform m_uVPLBuffer;
        gloops::Uniform m_uVPLIndexBuffer;
        gloops::Uniform m_uClusterTilesOffsetsBuffer;
        gloops::Uniform m_uVPLOffsetCountsBuffer;
        gloops::Uniform m_uRcpViewMatrix;
        gloops::Uniform m_uDistClamp;

        GBufferUniform m_uGBuffer;
        gloops::Uniform m_uGeometryClusterSampler;

        gloops::Uniform m_uRenderIrradiance;

        GLISMRenderer::ISMUniforms m_uISMData;
        gloops::Uniform m_uVPLISMBuffer;
        gloops::Uniform m_uVPLViewMatrixBuffer;

        gloops::Uniform m_uZNear;
        gloops::Uniform m_uZFar;

        gloops::Uniform m_uISMOffset;

        gloops::Uniform m_uNoShadow;

        gloops::Uniform m_uClusterCountsBuffer;
        gloops::Uniform m_uClusterTileOffsetBuffer;
        gloops::Uniform m_uClusterIrradianceBuffer;
        gloops::Uniform m_uClusterBBoxLowerBuffer;
        gloops::Uniform m_uClusterBBoxUpperBuffer;
        gloops::Uniform m_uClusterNormalBuffer;

        gloops::Uniform m_uTileWindow;
        gloops::Uniform m_uMaxDist;
        gloops::Uniform m_uUseIrradianceEstimate;

        GLBuffer m_VPLISMBuffer;
        GLBuffer m_VPLViewMatrixBuffer;

        GLQuery m_TimeElapsedQuery;

        VPLShadingPassData();
    };

    VPLShadingPassData m_VPLShadingPassData;

    struct ICSplatPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_GeometryShader;
        static const GLchar* s_FragmentShader;

        struct ICVertex {
            glm::vec4 positionRcpRadius;
            glm::vec3 normal;
            glm::vec3 irradiance;
        };

        gloops::Program m_Program;

        gloops::Uniform m_uViewMatrix;
        gloops::Uniform m_uProjMatrix;
        gloops::Uniform m_uViewport;
        gloops::Uniform m_uRcpProjMatrix;
        gloops::Uniform m_uRcpViewMatrix;
        GBufferUniform m_uGBuffer;

        GLQuery m_TimeElapsedQuery;

        // Inputs for the pass
        gloops::BufferObject m_ICVBO;
        gloops::VertexArrayObject m_ICVAO;

        // Output: R, G, B for irradiance estimation times weight, A for weight sum
        GLFramebuffer<1> m_IrradianceEstimationBuffer;
        GLuint64 m_IrradianceEstimationTextureHandle;

        ICSplatPassData();
    };

    ICSplatPassData m_ICSplatPassData;

    void splatIrradianceCache();

    struct ICDrawPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uRcpProjMatrix;
        GBufferUniform m_uGBuffer;
        gloops::Uniform m_uIrradianceSampler;
        gloops::Uniform m_uRenderIrradiance;

        ICDrawPassData();
    };

    ICDrawPassData m_ICDrawPassData;

    struct DirectLightingPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uInvProjMatrix;
        gloops::Uniform m_uShadowMapSampler;
        gloops::Uniform m_uLightPosition;
        gloops::Uniform m_uLightIntensity;
        gloops::Uniform m_uLightViewMatrix;
        gloops::Uniform m_uFaceProjectionMatrices;
        gloops::Uniform m_uRcpViewMatrix;

        gloops::Uniform m_uShadowMapOffset;
        gloops::Uniform m_uDistClamp;
        gloops::Uniform m_uRenderIrradiance;

        GBufferUniform m_uGBuffer;

        GLQuery m_TimeElapsedQuery;

        glm::vec3 m_LightPosition;
        glm::vec3 m_LightIntensity;
        glm::mat4 m_LightViewMatrix;
        bool m_DoDirectLightingPass;

        GLCubeShadowMapRenderer m_ShadowMapRenderer;
        GLCubeShadowMapContainer m_ShadowMapContainer;
        GLCubeShadowMapDrawer m_ShadowMapDrawer;

        DirectLightingPassData();
    };

    DirectLightingPassData m_DirectLightingPassData;

    struct DrawBuffersPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        GBufferUniform m_uGBuffer;
        gloops::Uniform m_uGeometryClusterSampler;
        gloops::Uniform m_uZFar;
        gloops::Uniform m_uDataToDisplay;
        gloops::Uniform m_uTileCount;
        gloops::Uniform m_uClusterTilesOffsetsBuffer;
        gloops::Uniform m_uClusterToTileBuffer;

        // Doesn't work ?
        //gloops::Uniform m_uClusterBBoxLowerBuffer;
        //gloops::Uniform m_uClusterBBoxUpperBuffer;

        GLQuery m_TimeElapsedQuery;

        DrawBuffersPassData();
    };

    DrawBuffersPassData m_DrawBuffersPassData;

    struct VPLData {
        GLBuffer m_VPLBuffer;

        // Lists of indices for each GCluster
        GLBuffer m_VPLIndexBuffer;
        // Offset counts in lists of indices
        GLBuffer m_VPLOffsetsCountsBuffer;

        GLISMContainer m_VPLISMContainer;

        VPLData(): m_VPLISMContainer(128) {
        }
    };

    VPLData m_VPLData;

    GLISMRenderer m_ISMRenderer;
    GLISMDrawer m_ISMDrawer;

    ScreenTriangle m_ScreenTriangle;

    // Input - Outputs

    // In milliseconds
    struct Timings {
        float geometryPass = 0;
        float findUniqueClustersPass = 0;
        float countUniqueClustersPass = 0;
        float lightAssignementPass = 0;
        float vplShadingPass = 0;
        float directLightShadingPass = 0;
        float drawBuffersPass = 0;
        float computeVPLISMPass = 0;
        float computeClusterISMPass = 0;
        float splatICPass = 0;

        float totalTime = 0;

        void updateTotalTime() {
            totalTime = geometryPass + findUniqueClustersPass +
                    countUniqueClustersPass + lightAssignementPass + computeClusterISMPass +
                    vplShadingPass + directLightShadingPass + drawBuffersPass + computeVPLISMPass + splatICPass;
        }
    };

    bool m_bDoVPLShadingPass = true;
    bool m_bNoShadow = true;
    bool m_bUseVPLISMs = false;
    bool m_bDoDirectLightPass = true;
    bool m_bRenderIrradiance = true;
    bool m_bDisplayCostPerPixel = false;
    float m_fGamma = 1.f;
    float m_fShadowMapOffset = 0.0001f;
    float m_fDistClamp = 100.f;
    bool m_bUseNormalClustering = true;
    bool m_bShowSpheres = false;
    bool m_bDoPull = true;
    bool m_bDoPullPush = true;
    float m_fPullTreshold = 0.05f;
    float m_fPushTreshold = 0.4f;
    float m_fISMOffset = 0.01f;
    float m_fISMPointMaxSize = 2.f;
    int m_nPullPushNbLevels = -1;
    float m_fISMNormalOffset = 2.f;
    int m_nSelectedISMIndex = -1;
    uint32_t m_nSelectedISMTextureIndex = 0;
    uint32_t m_nSelectedISMLevel = 0;
    uint32_t m_nSceneSampleCount = 10000;
    uint32_t m_nSceneSamplingMode = 0;
    int m_nTileWindow = 3;
    float m_fMaxDist = 100.f;
    float m_fSplatRadiusFactor = 1.f;
    bool m_bUseIrradianceEstimate = false;
    bool m_bUseCoherentIrradianceCache = false;
    bool m_bUseCPUIrradianceCache = false;
    Timings m_Timings;
};

}

#endif
