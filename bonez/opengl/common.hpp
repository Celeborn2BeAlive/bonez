#ifndef _BONEZ_GL_COMMON_HPP
#define _BONEZ_GL_COMMON_HPP

#include <iostream>
#include <GL/glew.h>

#define GLOOPS_USE_GLM
#include <gloops/gloops.hpp>

#include "utils/GLutils.hpp"

inline glm::vec3 uintToColor(uint32_t value) {
    return glm::fract(
                glm::sin(
                    glm::vec3(float(value) * 12.9898f,
                         float(value) * 78.233f,
                         float(value) * 56.128f)
                    )
                * 43758.5453f
                );
}

#endif // _BONEZ_GL_COMMON_HPP
