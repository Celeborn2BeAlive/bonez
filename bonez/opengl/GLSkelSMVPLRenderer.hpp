#ifndef _BONEZ_GLSKELSMVPLRENDERER_HPP_
#define _BONEZ_GLSKELSMVPLRENDERER_HPP_

#include "bonez/scene/Scene.hpp"
#include "opengl/shaders/GLSLProgramBuilder.hpp"
#include "opengl/scene/GLScene.hpp"

#include "bonez/scene/lights/vpls.hpp"

#include <vector>
#include <glm/glm.hpp>

#include "bonez/opengl/utils/GLFramebuffer.hpp"
#include "bonez/opengl/utils/GLTextureBuffer.hpp"
#include "bonez/opengl/utils/GLBuffer.hpp"
#include "bonez/opengl/utils/GLQuery.hpp"

#include "analysis/GLVisualDataRenderer.hpp"

#include "GLShadowMapRenderer.hpp"

namespace BnZ {

class GLSkelSMVPLRenderer {
public:
    GLSkelSMVPLRenderer();

    /**
     * @brief setResolution Allocate buffers for a given resolution.
     * @param width
     * @param height
     */
    void setResolution(size_t width, size_t height);

    void setProjectionMatrix(const glm::mat4& projectionMatrix) {
        m_ProjectionMatrix = projectionMatrix;
    }

    void setViewMatrix(const glm::mat4& viewMatrix) {
        m_ViewMatrix = viewMatrix;
    }

    /**
     * @brief setUp Initialize GL for future rendering with this renderer. Must be call before the geometry pass
     * and drawing methods.
     */
    void setUp();

    /**
     * @brief sendSkeletonData Send to the GPU the clustered skeleton of the scene
     * @param scene
     */
    void sendClusteredSkeletonData(const Scene& scene, const GLScene& glScene);

    /**
     * @brief geometryPass Do the geometry pass and fill GBuffer. Must be call before drawing methods.
     * @param scene
     */
    void geometryPass(const GLScene& scene);

    /**
     * @brief shadingPass Do the shading pass.
     * @param x
     * @param y
     * @param width
     * @param height
     * @param vpls
     * @param scene
     * @param vclustering
     */
    void shadingPass(int x, int y, size_t width, size_t height,
        const VPLContainer& vpls, const Scene& scene);

    void drawClusterNodeVisibilityPass(int x, int y, size_t width, size_t height,
                                       const Scene& scene, GraphNodeIndex clusterNode);

    void drawBuffers(int x, int y, size_t width, size_t height);

    void drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay);

    void drawClusterNodeShadowMap(int x, int y, size_t width, size_t height, GraphNodeIndex node);

    void displayGClusters(GLVisualDataRenderer& renderer, const Scene& scene);

    bool m_bDoVPLShadingPass;
    bool m_bNoShadow;
    bool m_bRenderIrradiance;
    bool m_bDisplayCostPerPixel;
    GraphNodeIndex m_nSelectedClusterNode;

    // In milliseconds
    struct Timings {
        float sendClusteredSkeletonDataPass;

        float geometryPass;
        float findUniqueClustersPass;
        float countUniqueClustersPass;
        float lightAssignementPass;
        float lightTransformPass;
        float shadingPass;
        float drawBuffersPass;

        float totalTime;

        Timings(): sendClusteredSkeletonDataPass(0), geometryPass(0), findUniqueClustersPass(0),
            countUniqueClustersPass(0), lightAssignementPass(0), lightTransformPass(0),
            shadingPass(0), drawBuffersPass(0), totalTime(0) {
        }

        void updateTotalTime() {
            totalTime = geometryPass + findUniqueClustersPass +
                    countUniqueClustersPass + lightAssignementPass + lightTransformPass +
                    shadingPass + drawBuffersPass;
        }
    };

    void getTimings(Timings& timings);

private:
    static const uint32_t TILE_SIZE = 32;

    void sendVPLs(const VPLContainer& vpls, const Scene& scene);

    glm::mat4 m_ProjectionMatrix;
    glm::mat4 m_ViewMatrix;
    glm::mat4 m_WorldToGridMatrix;

    glm::uvec2 m_TileCount;

    GLQuery m_SendClusteredSkeletonDataPassElapsedTime;

    enum GBufferTextureType {
        GBUFFER_NORMAL_DEPTH,
        GBUFFER_DIFFUSE,
        GBUFFER_GLOSSY_SHININESS,
        GBUFFER_CLUSTERNODE_INDICES,
        GBUFFER_GEOMETRYCLUSTERS,
        GBUFFER_COUNT
    };
    GLFramebuffer<GBUFFER_COUNT> m_GBuffer;

    enum TextureUnits {
        TEXUNIT_MAT_DIFFUSE,
        TEXUNIT_MAT_GLOSSY,
        TEXUNIT_MAT_SHININESS,
        TEXUNIT_GBUFFER_NORMALDEPTH,
        TEXUNIT_GBUFFER_DIFFUSE,
        TEXUNIT_GBUFFER_GLOSSYSHININESS,
        TEXUNIT_GBUFFER_CLUSTERNODE_INDICES,
        TEXUNIT_GBUFFER_GEOMETRYCLUSTERS,

        // Clustered shading textures
        TEXUNIT_TEX2D_GEOMETRYCLUSTERS,

        TEXUNIT_TEX3D_NODEGRID,

        TEXUNIT_CUBEMAP_CLUSTERNODESHADOWMAP,
        TEXUNIT_COUNT
    };

    enum ImageUnits {
        IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS
    };

    struct GeometryPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        GLMaterialUniforms m_MaterialUniforms;
        gloops::Uniform m_uMVPMatrix;
        gloops::Uniform m_uMVMatrix;
        gloops::Uniform m_uNormalMatrix;
        gloops::Uniform m_uSy; // Number of subdivision in the Y direction
        gloops::Uniform m_uHalfFOVY;
        gloops::Uniform m_uZNear;
        gloops::Uniform m_uZFar;
        gloops::Uniform m_uWorldToGrid;
        gloops::Uniform m_uVoxelSize;

        // CurvSkel clustering data
        gloops::Uniform m_uNodeGrid;

        GLQuery m_TimeElapsedQuery;

        GeometryPassData();
    };

    struct GLCurvSkelClusteringBuffers {
        GLBuffer m_NodeBuffer;
        GLBuffer m_NeighbourOffsetCountBuffer;
        GLBuffer m_NeighbourBuffer;

        gloops::TextureObject m_Grid;
    };

    GLCurvSkelClusteringBuffers m_GLCurvSkelClusteringBuffers;

    GeometryPassData m_GeometryPassData;

    struct FindUniqueGeometryClusterPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uClusterNodeSampler;
        gloops::Uniform m_uGeometryClusterSampler;
        gloops::Uniform m_uNormalDepthSampler;
        gloops::Uniform m_uClusterNodeCount;
        gloops::Uniform m_uClustersImage;
        gloops::Uniform m_uInvProjMatrix;
        gloops::Uniform m_uRcpViewMatrix;
        gloops::Uniform m_uScreenSize;

        GLQuery m_TimeElapsedQuery;

        FindUniqueGeometryClusterPassData();
    };

    // This texture maps each pixel of the image to the local offset of its cluster.
    // The local offset is an index in the screen-space tile from 0 to the number of clusters in this tile
    // minus 1
    gloops::TextureObject m_GeometryClustersTexture;

    // This texture maps each screen-space tile (Tx, Ty) to the number of clusters
    // contains in this tile
    //gloops::TextureObject m_GeometryClusterCountsTexture;

    GLBuffer m_ClusterBBoxLowerBuffer;
    GLBuffer m_ClusterBBoxUpperBuffer;
    GLBuffer m_ClusterNodeBuffer;

    // This buffer contains for each tile (Tx, Ty) with index Tx + Ty * Sy the number of clusters
    // contains in this tile.
    // It is used by the Count Geometry Clusters Pass to count the total number of clusters
    // In this implementation, the total number of tiles must be less than the number of threads
    // in a work item (1024 on my computer)
    GLBuffer m_GClusterCountsBuffer;

    FindUniqueGeometryClusterPassData m_FindUniqueGeometryClusterPassData;

    void findUniqueGeometryClusters();

    struct CountGeometryClusterPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uTileCount;

        GLQuery m_TimeElapsedQuery;

        CountGeometryClusterPassData();
    };

    // A buffer that will contain for each tile a global offset for the clusters contained in this tile
    // After the CountGeometryClusterPass, let localID be the local cluster ID of a tile (Tx, Ty). Then it's
    // global cluster ID is globalID = m_GClusterTilesOffsetsBuffer[Tx + Ty * Sy] + localID
    GLBuffer m_GClusterTilesOffsetsBuffer;
    // A buffer that will contain the total number of clusters (buffer of size 1)
    GLBuffer m_GClusterTotalCountBuffer;
    // CPU version of the previous buffer
    GLuint m_nGClusterCount;

    GLBuffer m_GClusterToTileBuffer;

    CountGeometryClusterPassData m_CountGeometryClusterPassData;

    struct LightAssignmentPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uWorldToGrid;
        gloops::Uniform m_uVoxelSize;
        gloops::Uniform m_uNodeGrid;

        gloops::Uniform m_uVPLCount;
        gloops::Uniform m_uClusterCount;

        gloops::Uniform m_uDoVPLShading;
        gloops::Uniform m_uNoShadow;

        GLBuffer m_NextOffsetBuffer;

        GLQuery m_TimeElapsedQuery;

        LightAssignmentPassData();
    };

    LightAssignmentPassData m_LightAssignmentPassData;

    struct TransformVPLToViewSpacePassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uVPLCount;
        gloops::Uniform m_uViewMatrix;
        gloops::Uniform m_uNormalMatrix;

        GLQuery m_TimeElapsedQuery;

        TransformVPLToViewSpacePassData();
    };

    TransformVPLToViewSpacePassData m_TransformVPLToViewSpacePassData;

    struct VPLShadingPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uInvProjMatrix;
        gloops::Uniform m_uTileCount;
        gloops::Uniform m_uVPLCount;
        gloops::Uniform m_uDisplayCostPerPixel;

        gloops::Uniform m_uSamplers[GBUFFER_COUNT];

        gloops::Uniform m_uWorldToGrid;
        gloops::Uniform m_uVoxelSize;

        // CurvSkel clustering data
        gloops::Uniform m_uNodeGrid;

        gloops::Uniform m_uRcpViewMatrix;

        gloops::Uniform m_uFaceProjectionMatrices;

        GLQuery m_TimeElapsedQuery;

        VPLShadingPassData();
    };

    VPLShadingPassData m_VPLShadingPassData;

    struct DrawBuffersPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uNormalDepthSampler;
        gloops::Uniform m_uClusterNodeSampler;
        gloops::Uniform m_uGeometryClusterSampler;
        gloops::Uniform m_uZFar;
        gloops::Uniform m_uDataToDisplay;
        gloops::Uniform m_uTileCount;

        GLQuery m_TimeElapsedQuery;

        DrawBuffersPassData();
    };

    DrawBuffersPassData m_DrawBuffersPassData;

    struct DrawClusterNodeVisibilityPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uInvProjMatrix;
        gloops::Uniform m_uRcpViewMatrix;

        gloops::Uniform m_uSamplers[GBUFFER_COUNT];

        gloops::Uniform m_uClusterPosition;
        gloops::Uniform m_uClusterNodeShadowMap;
        gloops::Uniform m_uClusterNodeMVPMatrices;

        DrawClusterNodeVisibilityPassData();
    };

    DrawClusterNodeVisibilityPassData m_DrawClusterNodeVisibilityPassData;

    GLuint m_nClusterCount;

    GLBuffer m_VPLBuffer;

    // Lists of indices for each GCluster
    GLBuffer m_VPLIndexBuffer;
    // Offset counts in lists of indices
    GLBuffer m_VPLOffsetsCountsBuffer;

    ScreenTriangle m_ScreenTriangle;

    static const uint32_t SHADOW_MAP_RESOLUTION = 256;
    GLCubeShadowMapContainer m_ClusterNodeShadowMaps;
    GLCubeShadowMapRenderer m_CubeShadowMapRenderer;
    GLCubeShadowMapDrawer m_CubeShadowMapDrawer;

    GLBuffer m_ClusterNodeShadowMapHandlesBuffer;

    std::vector<glm::mat4> m_ClusterNodeViewMatrices;
};

}

#endif
