#version 330

uniform vec4 uViewport;

uniform sampler2D uPositionSampler;
uniform sampler2D uNormalSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossySampler;
uniform sampler2D uShininessSampler;

out vec4 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;
    fFragColor = texture(uDiffuseSampler, texCoords);
}
