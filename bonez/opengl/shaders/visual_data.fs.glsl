#version 330

in vec3 vPosition; // View space fragment position
in vec3 vNormal; // View space fragment normal
in vec3 vColor;

uniform bool uDrawLines;

out vec3 fFragColor;

void main() {
    if(uDrawLines) {
        fFragColor = vColor;
    } else {
        vec3 eyeDirection = normalize(-vPosition);
        vec3 normalDirection = normalize(vNormal);

        float cosine = abs(dot(eyeDirection, normalDirection));

        fFragColor = cosine * vColor;
    }
}
