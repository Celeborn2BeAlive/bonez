#version 330

layout(location = 0) in vec3 aPosition; // World space vertex position

uniform mat4 uMVPMatrix;

void main() {
    gl_Position = uMVPMatrix * vec4(aPosition, 1);
}
