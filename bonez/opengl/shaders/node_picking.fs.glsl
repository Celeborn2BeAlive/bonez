#version 330

uniform uint uNodeIndex;

out uvec3 fNodeIndex;

void main() {
    fNodeIndex = uvec3(uNodeIndex + 1u, 0, 0);
}
