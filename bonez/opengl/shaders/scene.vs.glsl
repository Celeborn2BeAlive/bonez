#version 330

layout(location = 0) in vec3 aPosition; // World space vertex position
layout(location = 1) in vec3 aNormal; // World space vertex normal
layout(location = 2) in vec2 aTexCoords; // Texture coordinates of the vertex

layout(location = 3) in uint aNodeIndex; // Node index of the vertex

out vec3 vPosition; // View space fragment position
out vec3 vNormal; // View space fragment normal
out vec2 vTexCoords; // Texture coordinates of the frament

flat out uint vNodeIndex;

uniform mat4 uMVPMatrix; // Model View Projection matrix
uniform mat4 uMVMatrix; // Model View matrix
uniform mat3 uNormalMatrix; // Normal matrix

void main() {
    vec4 hPosition = vec4(aPosition, 1.f);
    vPosition = (uMVMatrix * hPosition).xyz;
    vNormal = uNormalMatrix * aNormal;
    vTexCoords = aTexCoords;
    vNodeIndex = aNodeIndex;
    gl_Position = uMVPMatrix * hPosition;
}
