#version 330

in vec3 vPosition; // View space fragment position
in vec3 vNormal; // View space fragment normal
in vec2 vTexCoords; // Texture coordinates of the frament

flat in uint vNodeIndex;

out vec3 fFragColor;

struct Material {
    vec3 Kd;
    vec3 Ks;
    float shininess;
    sampler2D KdSampler;
    sampler2D KsSampler;
    sampler2D shininessSampler;
};

uniform Material uMaterial;
uniform samplerBuffer uNodeColors;

void main() {
    vec3 eyeDirection = -vPosition;
    float dist = length(eyeDirection);
    eyeDirection /= dist;
    vec3 normalDirection = normalize(vNormal);

    float cosine = abs(dot(eyeDirection, normalDirection));

    fFragColor = cosine * uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb;
    //fFragColor = cosine * vec3(float(vNodeIndex) / 1002.0);
    //fFragColor = cosine * texelFetch(uNodeColors, int(vNodeIndex)).rgb;
}
