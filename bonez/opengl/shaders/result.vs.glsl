#version 330

layout(location = 0) in vec2 aPosition;

out vec2 vTexCoords;

void main() {
    vTexCoords = 0.5 * vec2(1 + aPosition.x, 1 - aPosition.y);
    gl_Position = vec4(aPosition, 0.f, 1.f);
}
