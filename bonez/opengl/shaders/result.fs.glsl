#version 330

in vec2 vTexCoords;

uniform sampler2D uImage;
uniform float uGamma;

out vec3 fFragColor;

void main() {
    fFragColor = pow(texture(uImage, vTexCoords).rgb, vec3(1.f / uGamma));
}
