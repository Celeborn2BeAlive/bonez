#version 330

layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in vec3 aVertexColor;

uniform vec3 uEyePosition;
uniform mat4 uVPMatrix;

uniform vec3 uNodeCenter;
uniform float uVoxelSize;
uniform bool uDrawNode; // If true, the shader is drawing a node and we use a uniform color
uniform vec3 uNodeColor;

out vec3 vColor;

void main() {
    if(uDrawNode) {
        vColor = uNodeColor;
        gl_Position = uVPMatrix * vec4(uNodeCenter + 0.1 * uVoxelSize * aVertexPosition, 1.f);
    } else {
        vColor = aVertexColor;
        gl_Position = uVPMatrix * vec4(aVertexPosition, 1.f);
    }
}
