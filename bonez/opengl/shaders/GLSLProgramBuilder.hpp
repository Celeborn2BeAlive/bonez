#ifndef _BONEZ_OPENGL_SHADERS_GLSLPROGRAMBUILDER_HPP_
#define _BONEZ_OPENGL_SHADERS_GLSLPROGRAMBUILDER_HPP_


#include <embree/common/sys/filename.h>
#include "opengl/common.hpp"

namespace BnZ {

class GLSLProgramBuilder {
public:
    GLSLProgramBuilder(const embree::FileName& basePath = ""):
        m_BasePath(basePath) {
    }

    gloops::Program buildProgram(const embree::FileName& vertexShader, const embree::FileName& fragmentShader) const;

private:
    embree::FileName m_BasePath;
};

template<typename VSrc, typename FSrc>
gloops::Program buildProgram(VSrc&& vsrc, FSrc&& fsrc) {
    gloops::Program program;
    if(!gloops::buildProgram(std::forward<VSrc>(vsrc), std::forward<FSrc>(fsrc), program)) {
        throw std::runtime_error("Program build error");
    }
    return program;
}

template<typename VSrc, typename GSrc, typename FSrc>
gloops::Program buildProgram(VSrc&& vsrc, GSrc&& gsrc, FSrc&& fsrc) {
    gloops::Program program;
    if(!gloops::buildProgram(std::forward<VSrc>(vsrc), std::forward<GSrc>(gsrc), std::forward<FSrc>(fsrc), program)) {
        throw std::runtime_error("Program build error");
    }
    return program;
}

template<typename CSrc>
gloops::Program buildComputeProgram(CSrc&& src) {
    gloops::Shader shader(GL_COMPUTE_SHADER);
    shader.setSource(std::forward<CSrc>(src));
    if(!shader.compile()) {
        std::cerr << shader.getInfoLog().get() << std::endl;
        throw std::runtime_error("Compute shader compilation error.");
    }

    gloops::Program program;
    program.attachShader(shader);

    if(!program.link()) {
        std::cerr << program.getInfoLog().get() << std::endl;
        throw std::runtime_error("Compute shader link error.");
    }

    return program;
}

}

#endif
