#version 330

uniform vec4 uViewport;

uniform sampler2D uPositionSampler;
uniform sampler2D uNormalSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossySampler;
uniform sampler2D uShininessSampler;

uniform vec3 uLightDirection; // Direction of the light in view space
uniform vec3 uLightIntensity; // Intensity of the light

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;
    
    vec3 wi = -uLightDirection;
    float G = max(0, dot(wi, vec3(texture(uNormalSampler, texCoords))));
    
    fFragColor = vec3(texture(uDiffuseSampler, texCoords)) * G * uLightIntensity;
}
