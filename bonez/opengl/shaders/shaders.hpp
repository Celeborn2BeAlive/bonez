#ifndef _BONEZ_OPENGL_SHADERS_SHADERS_HPP_
#define _BONEZ_OPENGL_SHADERS_SHADERS_HPP_

// Use this macro to stringify a shader source code embedded in C++ source code
#define BONEZ_STRINGIFY(s) #s

#include "GLSLProgramBuilder.hpp"

namespace BnZ {

struct GraphShader {
    static const GLuint aPositionLocation = 0;
    static const GLuint aColorLocation = 1;
    
    static constexpr const GLchar* uVoxelSize = "uVoxelSize";
    static constexpr const GLchar* uNodeCenter = "uNodeCenter";
    static constexpr const GLchar* uDrawNode = "uDrawNode";
    static constexpr const GLchar* uNodeColor = "uNodeColor";
};

struct ColorShader {
    static const GLuint aPositionLocation = 0;
    static const GLuint aColorLocation = 1;
};

}

#endif
