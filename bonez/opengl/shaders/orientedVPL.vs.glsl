#version 330

layout(location = 0) in vec3 aPosition;

// Attributes for instanced rendering
layout(location = 1) in vec3 aLightPosition;
layout(location = 2) in vec3 aLightNormal;
layout(location = 3) in vec3 aLightIntensity;

flat out vec3 vLightPosition;
flat out vec3 vLightNormal;
flat out vec3 vLightIntensity;

out vec3 vPositionInViewSpace;

uniform mat4 uInvProjMatrix;

uniform mat4 uMVPMatrix;

void main() {
    vLightPosition = aLightPosition;
    vLightNormal = aLightNormal;
    vLightIntensity = aLightIntensity;
    
    vec4 positionInViewSpace = uInvProjMatrix * vec4(aPosition.xy, -1.f, 1.f);
    vPositionInViewSpace = positionInViewSpace.xyz / positionInViewSpace.w;

    gl_Position = uMVPMatrix * vec4(aPosition, 1);
}
