#include <memory>
#include <fstream>
#include <stdexcept>

#include "GLSLProgramBuilder.hpp"

namespace BnZ {

gloops::Program GLSLProgramBuilder::buildProgram(const embree::FileName& vertexShader, const embree::FileName& fragmentShader) const {
    embree::FileName completeVertexShaderPath = m_BasePath + vertexShader;
    embree::FileName completeFragmentShaderPath = m_BasePath + fragmentShader;
    
    gloops::Program program;
    if(!gloops::loadAndBuildProgram(completeVertexShaderPath, completeFragmentShaderPath, program)) {
        throw std::runtime_error("GLSL program compilation failed");
    }
    return program;
}

}
