#version 330

layout(location = 0) in vec2 aPosition;
layout(location = 1) in vec2 aTexCoords;

uniform mat3 uTransform;

out vec2 vTexCoords;

void main() {
    vTexCoords = aTexCoords;
    gl_Position = vec4((uTransform * vec3(aPosition, 1.f)).xy, 0.f, 1.f);
}
