#version 330

uniform vec4 uViewport;

uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;

uniform vec3 uLightPosition; // Position of the light in view space
uniform vec3 uLightNormal; // Normal of the light in view space
uniform vec3 uLightIntensity; // Intensity of the light

// For instanced rendering
flat in vec3 vLightPosition;
flat in vec3 vLightNormal;
flat in vec3 vLightIntensity;

in vec3 vPositionInViewSpace;

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;
    
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vPositionInViewSpace;

    vec3 wi = uLightPosition - fragPosition;
    float dist = length(wi);
    wi /= dist;
    
    float cos_o = max(0, dot(-wi, uLightNormal));
 
    float G = max(0, dot(wi, normalDepth.xyz)) * cos_o / (2 + dist * dist);

    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    vec3 r = reflect(-wi, normalDepth.xyz);

    fFragColor = G * uLightIntensity *
            (Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a));
}
