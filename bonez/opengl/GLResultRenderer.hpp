#ifndef _BONEZ_GLRESULTRENDERER_HPP
#define _BONEZ_GLRESULTRENDERER_HPP

#include "shaders/GLSLProgramBuilder.hpp"
#include "bonez/renderer/Framebuffer.hpp"

namespace BnZ {

class GLResultRenderer {
public:
    GLResultRenderer();

    void drawResult(const gloops::Viewport& viewport, float gamma, const Framebuffer& framebuffer);

private:
    gloops::Program m_ResultProgram;
    gloops::Uniform m_uGamma;
    gloops::Uniform m_uImage;
    gloops::BufferObject m_QuadVBO;
    gloops::VertexArrayObject m_QuadVAO;
};

}

#endif // _BONEZ_GLRESULTRENDERER_HPP
