#ifndef _BONEZ_GLISMRENDERER_HPP
#define _BONEZ_GLISMRENDERER_HPP

#include "common.hpp"
#include "utils/GLBuffer.hpp"
#include "utils/GLUniform.hpp"
#include "shaders/GLSLProgramBuilder.hpp"
#include "bonez/opengl/scene/GLPoints.hpp"

namespace BnZ {

// Imperfect Shadow Maps container
class GLISMContainer {
    // Size of a texture
    uint32_t m_nTextureRes;

    // Resolution of single shadow map inside a texture (the size of the viewport)
    uint32_t m_nSMRes;

    // Number of shadow maps per texture in each dimension ( = m_nTextureSize / m_nRes)
    uint32_t m_nSMCountPerDimension;

    // Number of shadow maps per texture (m_nSMCount * m_nSMCount)
    uint32_t m_nSMCountPerTexture;

    // All the textures
    std::vector<gloops::TextureObject> m_Textures;

    // Texture handles for bindless texture access
    std::vector<GLuint64> m_TextureHandles;

    // Textures used to do the pull-push phase of ISM rendering
    std::vector<gloops::TextureObject> m_PullPushTextures;
public:
    GLISMContainer(uint32_t smRes);

    // Reserve enough textures to render smCount shadow maps
    void resize(uint32_t smCount);

    uint32_t getTextureResolution() const {
        return m_nTextureRes;
    }

    uint32_t getShadowMapResolution() const {
        return m_nSMRes;
    }

    uint32_t getShadowMapCountPerDimension() const {
        return m_nSMCountPerDimension;
    }

    uint32_t getShadowMapCountPerTexture() const {
        return m_nSMCountPerTexture;
    }

    const gloops::TextureObject& getTexture(uint32_t i) const {
        return m_Textures[i];
    }

    const gloops::TextureObject& getPullPushTexture(uint32_t i) const {
        return m_PullPushTextures[i];
    }

    uint32_t getPullPushTextureCount() const {
        return m_PullPushTextures.size();
    }

    uint32_t getTextureCount() const {
        return m_Textures.size();
    }

    const GLuint64* getTextureHandleBufferPtr() const {
        return m_TextureHandles.data();
    }

    GLuint64 getImageHandle(uint32_t i) const {
        return glGetImageHandleNV(m_Textures[i].glId(), 0, GL_FALSE, 0, GL_R32F);
    }

    GLuint64 getPullPushImageHandle(uint32_t i) const {
        return glGetImageHandleNV(m_PullPushTextures[i].glId(), 0, GL_FALSE, 0, GL_R32F);
    }
};

// Imperfect Shadow Maps renderer
class GLISMRenderer {
public:
    GLISMRenderer();

    struct ISMUniforms {
        gloops::Uniform smResolution;
        gloops::Uniform smCountPerTexture;
        gloops::Uniform smCountPerDimension;

        ISMUniforms(const gloops::Program& program):
            smResolution(program, "uISMData.smResolution", true),
            smCountPerTexture(program, "uISMData.smCountPerTexture", true),
            smCountPerDimension(program, "uISMData.smCountPerDimension", true) {
        }

        void set(const GLISMContainer& container) {
            smCountPerDimension.set(container.getShadowMapCountPerDimension());
            smCountPerTexture.set(container.getShadowMapCountPerTexture());
            smResolution.set(container.getShadowMapResolution());
        }
    };

    void render(const GLPoints& sampledScene, GLISMContainer& container,
                const glm::mat4* viewMatrices, uint32_t viewMatrixCount,
                float pointMaxSize,
                bool doPull, bool doPullPush,
                float pullTreshold, float pushTreshold,
                int nbLevels);

    void renderCoherent(const GLPoints& sampledScene, GLISMContainer& container,
                        const glm::mat4* viewMatrices, uint32_t viewMatrixCount,
                        float pointMaxSize,
                        bool doPull, bool doPullPush,
                        float pullTreshold, float pushTreshold,
                        int nbLevels);

    void renderPacks(const GLPoints& sampledScene, uint32_t pointCountPerISM, GLISMContainer& container,
                      const glm::mat4* viewMatrices, uint32_t viewMatrixCount,
                      float pointMaxSize,
                      bool doPull, bool doPullPush,
                      float pullTreshold, float pushTreshold,
                      int nbLevels);

    void pullPush(GLISMContainer& container, bool doPull, bool doPullPush,
                  float pullTreshold, float pushTreshold,
                  int nbLevels);

private:
    static const GLchar *s_RenderVertexShader, *s_RenderFragmentShader;
    static const GLchar *s_RenderCoherentVertexShader, *s_RenderCoherentFragmentShader;

    gloops::Program m_RenderProgram, m_RenderCoherentProgram;
    gloops::FramebufferObject m_FBO;
    gloops::TextureObject m_DepthBuffer;

    gloops::Uniform m_uZFar;
    gloops::Uniform m_uZNear;
    gloops::Uniform m_uPointMaxSize;
    gloops::Uniform m_uDoPacked;
    gloops::Uniform m_uPackSize;

    gloops::Uniform m_uZFarCoherent;
    gloops::Uniform m_uZNearCoherent;
    gloops::Uniform m_uPointMaxSizeCoherent;

    struct ViewMatrixUniforms {
        gloops::Uniform viewMatrixBuffer;
        gloops::Uniform viewMatrixCount;
        gloops::Uniform viewMatrixOffset;

        ViewMatrixUniforms(const gloops::Program& program):
            viewMatrixBuffer(program, "uViewMatrixData.viewMatrixBuffer", true),
            viewMatrixCount(program, "uViewMatrixData.viewMatrixCount", true),
            viewMatrixOffset(program, "uViewMatrixData.viewMatrixOffset", true) {
        }
    };

    ViewMatrixUniforms m_uViewMatrixData;

    ViewMatrixUniforms m_uViewMatrixDataCoherent;

    ISMUniforms m_uISMData;

    ISMUniforms m_uISMDataCoherent;

    GLBuffer m_ViewMatrixBuffer;

    struct PullPass {
        static const GLchar *s_ComputeShader;
        gloops::Program m_Program;

        gloops::Uniform m_uInputImage;
        gloops::Uniform m_uOutputImage;

        gloops::Uniform m_uTreshold;

        PullPass();
    };

    PullPass m_PullPass;

    void pullPass(uint32_t texIdx, GLISMContainer& container, float treshold, int nbLevels);

    struct PushPass {
        static const GLchar *s_ComputeShader;
        gloops::Program m_Program;

        gloops::Uniform m_uInputImage;
        gloops::Uniform m_uOutputImage;

        gloops::Uniform m_uTreshold;

        gloops::Uniform m_uInputSMResolution;

        PushPass();
    };

    PushPass m_PushPass;

    void pushPass(uint32_t texIdx, GLISMContainer& container, float treshold, int nbLevels);
};

class GLISMDrawer {
public:
    //GLISMDrawer();

    // Draw all ISMs from a texture
    void drawISMTexture(const GLISMContainer& container,
                        uint32_t texIdx, uint32_t texLvl, int textureUnit = 0);

    // Draw one ISM
    void drawISM(const GLISMContainer& container,
                 uint32_t ismIdx, int textureUnit = 0);

    // Draw one ISM
    void drawISM(const GLISMContainer& container,
                 uint32_t ismIdx, uint32_t texLvl, int textureUnit = 0);

private:
    static const GLchar *s_VertexShader, *s_FragmentShader;

    gloops::Program m_Program { buildProgram(s_VertexShader, s_FragmentShader) };

    BNZ_GLUNIFORM(m_Program, GLfloat, uSMX);
    BNZ_GLUNIFORM(m_Program, GLfloat, uSMY);
    BNZ_GLUNIFORM(m_Program, GLfloat, uRes);
    BNZ_GLUNIFORM(m_Program, GLint, uTexture);
    BNZ_GLUNIFORM(m_Program, GLuint, uTexIndex);

    GLISMRenderer::ISMUniforms m_uISMData { m_Program };

    ScreenTriangle m_ScreenTriangle;
};

}

#endif // _BONEZ_GLISMRENDERER_HPP
