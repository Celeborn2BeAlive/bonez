#ifndef _BONEZ_GLLIGHTGATERENDERER8_HPP_
#define _BONEZ_GLLIGHTGATERENDERER8_HPP_

#include "bonez/scene/Scene.hpp"
#include "opengl/shaders/GLSLProgramBuilder.hpp"
#include "opengl/scene/GLScene.hpp"

#include "bonez/scene/lights/vpls.hpp"
#include "bonez/scene/topology/VisibilityClustering.hpp"

#include <vector>
#include <glm/glm.hpp>

#include "bonez/opengl/utils/GLFramebuffer.hpp"
#include "bonez/opengl/utils/GLTextureBuffer.hpp"
#include "bonez/opengl/utils/GLBuffer.hpp"
#include "bonez/opengl/utils/GLQuery.hpp"

#include "bonez/opengl/analysis/GLVisualDataRenderer.hpp"

namespace BnZ {

class GLLightGateRenderer8 {
public:
    GLLightGateRenderer8();

    /**
     * @brief setResolution Allocate buffers for a given resolution.
     * @param width
     * @param height
     */
    void setResolution(size_t width, size_t height);

    void setProjectionMatrix(const glm::mat4& projectionMatrix) {
        m_ProjectionMatrix = projectionMatrix;
    }

    void setViewMatrix(const glm::mat4& viewMatrix) {
        m_ViewMatrix = viewMatrix;
    }

    /**
     * @brief setUp Initialize GL for future rendering with this renderer. Must be call before the geometry pass
     * and drawing methods.
     */
    void setUp();

    /**
     * @brief sendVisibilityClusteringGrid Send skeleton and visiblity clustering data to the GPU. Must be call
     * before the geometry pass.
     * @param scene
     * @param vclustering
     */
    void sendVisibilityClusteringGrid(const Scene& scene, const VisibilityClustering& vclustering);

    /**
     * @brief geometryPass Do the geometry pass and fill GBuffer. Must be call before drawing methods.
     * @param scene
     */
    void geometryPass(const GLScene& scene);

    /**
     * @brief shadingPass Do the shading pass.
     * @param x
     * @param y
     * @param width
     * @param height
     * @param vpls
     * @param scene
     * @param vclustering
     */
    void shadingPass(int x, int y, size_t width, size_t height,
        const VPLContainer& vpls, const Scene& scene, const VisibilityClustering& vclustering);

    void drawBuffers(int x, int y, size_t width, size_t height);

    void drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay);

    void displayData(GLVisualDataRenderer& renderer, const Scene& scene,
                     const VPLContainer& vpls, const VisibilityClustering& vclustering);

    bool m_bDoVPLShadingPass;
    bool m_bDoGateShadingPass;
    bool m_bNoShadow;
    bool m_bRenderIrradiance;
    uint32_t m_nSamplePerGateCount;
    bool m_bDisplayCostPerPixel;
    bool m_bUseGateCosFactor;
    bool m_bUseAttenuation;

    int m_nSelectedVisibilityCluster;

    // In milliseconds
    struct Timings {
        float geometryPass;
        float discontinuityPass;
        float findUniqueClustersPass;
        float countUniqueClustersPass;
        float lightAssignementPass;
        float lightTransformPass;
        float shadingPass;
        float finalPass;
        float drawBuffersPass;

        float totalTime;

        Timings(): geometryPass(0), discontinuityPass(0), findUniqueClustersPass(0),
            countUniqueClustersPass(0), lightAssignementPass(0), lightTransformPass(0),
            shadingPass(0), finalPass(0), drawBuffersPass(0), totalTime(0) {
        }

        void updateTotalTime() {
            totalTime = geometryPass + discontinuityPass + findUniqueClustersPass +
                    countUniqueClustersPass + lightAssignementPass + lightTransformPass +
                    shadingPass + finalPass + drawBuffersPass;
        }
    };

    void getTimings(Timings& timings);

private:
    static const uint32_t TILE_SIZE = 32;

    void sendVPLs(const VPLContainer& vpls, const Scene& scene, const VisibilityClustering& vclustering);

    glm::mat4 m_ProjectionMatrix;
    glm::mat4 m_ViewMatrix;
    glm::mat4 m_WorldToGridMatrix;

    glm::uvec2 m_TileCount;

    enum GBufferTextureType {
        GBUFFER_NORMAL_DEPTH,
        GBUFFER_DIFFUSE,
        GBUFFER_GLOSSY_SHININESS,
        GBUFFER_VCLUSTER_INDICES,
        GBUFFER_VCLUSTER_COHERENCIES,
        GBUFFER_GEOMETRYCLUSTERS,
        GBUFFER_COUNT
    };
    GLFramebuffer<GBUFFER_COUNT> m_GBuffer;

    enum TextureUnits {
        TEXUNIT_MAT_DIFFUSE,
        TEXUNIT_MAT_GLOSSY,
        TEXUNIT_MAT_SHININESS,
        TEXUNIT_GBUFFER_NORMALDEPTH,
        TEXUNIT_GBUFFER_DIFFUSE,
        TEXUNIT_GBUFFER_GLOSSYSHININESS,
        TEXUNIT_GBUFFER_VCLUSTERS,
        TEXUNIT_GBUFFER_VCLUSTERS_COHERENCIES,
        TEXUNIT_GBUFFER_GEOMETRYCLUSTERS,
        TEXUNIT_TEX2D_DISCONTINUITYBUFFER,
        TEXUNIT_TEX2D_IRRADIANCEBUFFER,

        // Clustered shading textures
        TEXUNIT_TEX2D_GEOMETRYCLUSTERS,

        TEXUNIT_TEX3D_NODEGRID,
        TEXUNIT_COUNT
    };

    enum ImageUnits {
        IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS
    };

    struct GeometryPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        GLMaterialUniforms m_MaterialUniforms;
        gloops::Uniform m_uMVPMatrix;
        gloops::Uniform m_uMVMatrix;
        gloops::Uniform m_uNormalMatrix;
        gloops::Uniform m_uSy; // Number of subdivision in the Y direction
        gloops::Uniform m_uHalfFOVY;
        gloops::Uniform m_uZNear;
        gloops::Uniform m_uZFar;
        gloops::Uniform m_uWorldToGrid;
        gloops::Uniform m_uVoxelSize;

        // CurvSkel clustering data
        gloops::Uniform m_uNodeGrid;

        gloops::Uniform m_uSelectedVCluster;

        GLQuery m_TimeElapsedQuery;

        GeometryPassData();
    };

    struct GLCurvSkelClusteringBuffers {
        GLBuffer m_NodeBuffer;
        GLBuffer m_NeighbourOffsetCountBuffer;
        GLBuffer m_NeighbourBuffer;

        gloops::TextureObject m_Grid;

        GLBuffer m_VisibilityClusterBuffer;

        GLBuffer m_VClusterGateCountBuffer;
    };

    GLCurvSkelClusteringBuffers m_GLCurvSkelClusteringBuffers;

    GeometryPassData m_GeometryPassData;

    struct FindUniqueGeometryClusterPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uVisibilityClusterSampler;
        gloops::Uniform m_uGeometryClusterSampler;
        gloops::Uniform m_uNormalDepthSampler;
        gloops::Uniform m_uVClusterCount;
        gloops::Uniform m_uClustersImage;
        gloops::Uniform m_uInvProjMatrix;
        gloops::Uniform m_uRcpViewMatrix;
        gloops::Uniform m_uScreenSize;

        GLQuery m_TimeElapsedQuery;

        FindUniqueGeometryClusterPassData();
    };

    // This texture maps each pixel of the image to the local offset of its cluster.
    // The local offset is an index in the screen-space tile from 0 to the number of clusters in this tile
    // minus 1
    gloops::TextureObject m_GeometryClustersTexture;

    // This texture maps each screen-space tile (Tx, Ty) to the number of clusters
    // contains in this tile
    //gloops::TextureObject m_GeometryClusterCountsTexture;

    GLBuffer m_ClusterBBoxLowerBuffer;
    GLBuffer m_ClusterBBoxUpperBuffer;
    GLBuffer m_ClusterVClusterBuffer;

    // This buffer contains for each tile (Tx, Ty) with index Tx + Ty * Sy the number of clusters
    // contains in this tile.
    // It is used by the Count Geometry Clusters Pass to count the total number of clusters
    // In this implementation, the total number of tiles must be less than the number of threads
    // in a work item (1024 on my computer)
    GLBuffer m_GClusterCountsBuffer;

    FindUniqueGeometryClusterPassData m_FindUniqueGeometryClusterPassData;

    void findUniqueGeometryClusters();

    struct CountGeometryClusterPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uTileCount;

        GLQuery m_TimeElapsedQuery;

        CountGeometryClusterPassData();
    };

    // A buffer that will contain for each tile a global offset for the clusters contained in this tile
    // After the CountGeometryClusterPass, let localID be the local cluster ID of a tile (Tx, Ty). Then it's
    // global cluster ID is globalID = m_GClusterTilesOffsetsBuffer[Tx + Ty * Sy] + localID
    GLBuffer m_GClusterTilesOffsetsBuffer;
    // A buffer that will contain the total number of clusters (buffer of size 1)
    GLBuffer m_GClusterTotalCountBuffer;
    // CPU version of the previous buffer
    GLuint m_nGClusterCount;

    GLBuffer m_GClusterToTileBuffer;

    CountGeometryClusterPassData m_CountGeometryClusterPassData;

    struct LightAssignmentPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uWorldToGrid;
        gloops::Uniform m_uVoxelSize;
        gloops::Uniform m_uNodeGrid;

        gloops::Uniform m_uVPLCount;
        gloops::Uniform m_uClusterCount;

        gloops::Uniform m_uDoVPLShading;
        gloops::Uniform m_uDoGateShading;
        gloops::Uniform m_uNoShadow;

        gloops::Uniform m_uUseGateCosFactor;

        GLBuffer m_NextOffsetBuffer;

        GLQuery m_TimeElapsedQuery;

        LightAssignmentPassData();
    };

    LightAssignmentPassData m_LightAssignmentPassData;

    struct TransformVPLToViewSpacePassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uVPLCount;
        gloops::Uniform m_uGateCount;
        gloops::Uniform m_uViewMatrix;
        gloops::Uniform m_uNormalMatrix;

        GLQuery m_TimeElapsedQuery;

        TransformVPLToViewSpacePassData();
    };

    TransformVPLToViewSpacePassData m_TransformVPLToViewSpacePassData;

    struct VPLShadingPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uInvProjMatrix;
        gloops::Uniform m_uTileCount;
        gloops::Uniform m_uVPLCount;
        gloops::Uniform m_uDisplayCostPerPixel;

        gloops::Uniform m_uUseGateCosFactor;
        gloops::Uniform m_uUseAttenuation;

        gloops::Uniform m_uSamplers[GBUFFER_COUNT];

        gloops::Uniform m_uWorldToGrid;
        gloops::Uniform m_uVoxelSize;

        // CurvSkel clustering data
        gloops::Uniform m_uNodeGrid;

        gloops::Uniform m_uRcpViewMatrix;
        gloops::Uniform m_uVClusterCount;

        GLQuery m_TimeElapsedQuery;

        VPLShadingPassData();
    };

    VPLShadingPassData m_VPLShadingPassData;

    GLFramebuffer<1> m_IrradianceBuffer;

    struct DrawBuffersPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uNormalDepthSampler;
        gloops::Uniform m_uVisibilityClusterSampler;
        gloops::Uniform m_uVisibilityClusterCoherenciesSampler;
        gloops::Uniform m_uDiscontinuitySampler;
        gloops::Uniform m_uGeometryClusterSampler;
        gloops::Uniform m_uZFar;
        gloops::Uniform m_uDataToDisplay;
        gloops::Uniform m_uTileCount;

        GLQuery m_TimeElapsedQuery;

        DrawBuffersPassData();
    };

    DrawBuffersPassData m_DrawBuffersPassData;

    struct DiscontinuityPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uSamplers[GBUFFER_COUNT];
        gloops::Uniform m_uViewport;
        gloops::Uniform m_uZFar;

        GLQuery m_TimeElapsedQuery;

        DiscontinuityPassData();
    };

    DiscontinuityPassData m_DiscontinuityPass;

    GLFramebuffer<1> m_DiscontinuityBuffer;
    void computeGeometryDiscontinuityBuffer();

    struct BlurPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uSamplers[GBUFFER_COUNT];
        gloops::Uniform m_uIrradianceBuffer;
        gloops::Uniform m_uDiscontinuityBuffer;
        gloops::Uniform m_uRenderIrradiance;

        GLQuery m_TimeElapsedQuery;

        BlurPassData();
    };

    BlurPassData m_BlurPassData;

    GLuint m_nClusterCount;

    GLBuffer m_VPLBuffer;

    // Lists of indices for each GCluster
    GLBuffer m_VPLIndexBuffer;
    // Offset counts in lists of indices
    GLBuffer m_VPLOffsetsCountsBuffer;

    GLBuffer m_VPLOffsetsCountsPerVClusterBuffer;

    GLBuffer m_GateBuffer;
    GLBuffer m_GateOffsetsCountsBuffer;
    GLBuffer m_TransfertFlagsBuffer;

    ScreenTriangle m_ScreenTriangle;
};

}

#endif
