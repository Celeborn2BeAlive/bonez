#ifndef _BONEZ_GLSKELDEFERREDRENDERER_HPP_
#define _BONEZ_GLSKELDEFERREDRENDERER_HPP_

#include "bonez/scene/Scene.hpp"
#include "opengl/shaders/GLSLProgramBuilder.hpp"
#include "opengl/scene/GLScene.hpp"

#include "bonez/scene/lights/vpls.hpp"
#include "bonez/scene/topology/VisibilityClustering.hpp"

#include <vector>
#include <glm/glm.hpp>

#include "bonez/opengl/utils/GLFramebuffer.hpp"
#include "bonez/opengl/utils/GLTextureBuffer.hpp"

namespace BnZ {

class GLSkelDeferredRenderer {
public:
    GLSkelDeferredRenderer();

    void setResolution(size_t width, size_t height);

    void setProjectionMatrix(const glm::mat4& projectionMatrix) {
        m_ProjectionMatrix = projectionMatrix;
    }

    void setViewMatrix(const glm::mat4& viewMatrix) {
        m_ViewMatrix = viewMatrix;
    }

    void setup();

    void sendSkelClustering(const Scene& scene);

    void geometryPass(const GLScene& scene);

    void shadingPass(int x, int y, size_t width, size_t height,
        const VPLContainer& vpls, const Scene& scene);

    void drawBuffers(int x, int y, size_t width, size_t height);

private:
    void sendVPLs(const VPLContainer& vpls, const Scene& scene);

    glm::mat4 m_ProjectionMatrix;
    glm::mat4 m_ViewMatrix;

    enum GBufferTextureType {
        GBUFFER_NORMAL_DEPTH,
        GBUFFER_DIFFUSE,
        GBUFFER_GLOSSY_SHININESS,
        GBUFFER_COUNT
    };
    GLFramebuffer<GBUFFER_COUNT> m_GBuffer;

    enum TextureUnits {
        MAT_DIFFUSE_TEXUNIT,
        MAT_GLOSSY_TEXUNIT,
        MAT_SHININESS_TEXUNIT,
        GBUFFER_NORMALDEPTH_TEXUNIT,
        GBUFFER_DIFFUSE_TEXUNIT,
        GBUFFER_GLOSSYSHININESS_TEXUNIT,
        TEX3D_SKELCLUSTERINGGRID_TEXUNIT,
        TEXBUFFER_VPLARRAY_TEXUNIT,
        TEXBUFFER_SKELCLUSTERING_TEXUNIT,
        TEXBUFFER_SKELCLUSTERINGNEIGHBOURS_TEXUNIT,
        TEXBUFFER_SKELCLUSTERINGOFFSETCOUNTS_TEXUNIT
    };

    struct GeometryPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        GLMaterialUniforms m_MaterialUniforms;
        gloops::Uniform m_uMVPMatrix;
        gloops::Uniform m_uMVMatrix;
        gloops::Uniform m_uNormalMatrix;
        gloops::Uniform m_uZFar;
        gloops::Uniform m_uCameraPosition;

        GeometryPassData();
    };

    GeometryPassData m_GeometryPassData;

    struct ShadingPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uInvProjMatrix;

        gloops::Uniform m_uSamplers[GBUFFER_COUNT];

        gloops::Uniform m_uVPLArray;

        gloops::Uniform m_uSkelClusteringGrid;
        gloops::Uniform m_uRcpViewMatrix;
        gloops::Uniform m_uRcpNormalMatrix;
        gloops::Uniform m_uWorldToGrid;
        gloops::Uniform m_uSkelClustering;
        gloops::Uniform m_uSkelClusteringNeighbours;
        gloops::Uniform m_uSkelClusteringOffsetCounts;
        gloops::Uniform m_uVoxelSize;

        ShadingPassData();
    };

    ShadingPassData m_ShadingPassData;

    struct DrawBuffersPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uNormalDepthSampler;
        gloops::Uniform m_uDataToDisplay;

        DrawBuffersPassData();
    };

    DrawBuffersPassData m_DrawBuffersPassData;

    gloops::TextureObject m_SkelClusteringGrid;

    GLTextureBuffer m_VPLBuffer;
    GLTextureBuffer m_SkelClusteringBuffer;
    GLTextureBuffer m_SkelClusteringNeighbours;
    GLTextureBuffer m_SkelClusteringOffsetCounts;



    ScreenTriangle m_ScreenTriangle;
};

}

#endif
