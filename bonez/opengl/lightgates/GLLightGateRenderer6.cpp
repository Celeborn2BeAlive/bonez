#include "GLLightGateRenderer6.hpp"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "bonez/opengl/utils/embree_opengl.hpp"

#include "bonez/common/data/ColorMap.hpp"
#include "bonez/common/Progress.hpp"

namespace BnZ {

using namespace gloops;

const GLchar* GLLightGateRenderer6::GeometryPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;

uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat3 uNormalMatrix;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoords;
out vec3 vWorldSpaceNormal;
out vec3 vWorldSpacePosition;

void main() {
    vWorldSpacePosition = aPosition;
    vWorldSpaceNormal = aNormal;
    vPosition = vec3(uMVMatrix * vec4(aPosition, 1));
    vNormal = uNormalMatrix * aNormal;
    vTexCoords = aTexCoords;

    gl_Position = uMVPMatrix * vec4(aPosition, 1);
}

);

const GLchar* GLLightGateRenderer6::GeometryPassData::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoords;
in vec3 vWorldSpacePosition;
in vec3 vWorldSpaceNormal;

struct Material {
    vec3 Kd;
    vec3 Ks;
    float shininess;
    sampler2D KdSampler;
    sampler2D KsSampler;
    sampler2D shininessSampler;
};

uniform Material uMaterial;

uniform float uSy;
uniform float uHalfFOVY;
uniform float uZNear;
uniform float uZFar;

struct VoxelSpace {
    // The length of an edge of a voxel in world space
    float voxelSize;
    // The transformation matrix that gives for a world space position
    // the associated voxel
    mat4 worldToGrid;
};

layout(std430, binding = 0) buffer NodeBuffer {
    vec4 bNodeBuffer[];
};

layout(std430, binding = 1) buffer NeighbourOffsetCountBuffer {
    ivec2 bNeighbourOffsetCountBuffer[];
};

layout(std430, binding = 2) buffer NeighbourBuffer {
    int bNeighbourBuffer[];
};

layout(std430, binding = 3) buffer VisibilityClusterBuffer {
    int bVisibilityClusterBuffer[];
};

uniform usampler3D uNodeGrid;

ivec3 gridSize;

uniform VoxelSpace uVoxelSpace;

layout(location = 0) out vec4 fNormalDepth;
layout(location = 1) out vec3 fDiffuse;
layout(location = 2) out vec4 fGlossyShininess;
layout(location = 3) out ivec4 fVClusterIndices;
layout(location = 4) out uint fGeometryCluster;

layout(binding = 0, offset = 0) uniform atomic_uint nextOffset;

const float PI = 3.14159265358979323846264;

float computeCoherency(unsigned int cluster, vec3 P, vec3 N) {
    vec4 node = bNodeBuffer[cluster];
    vec3 wi = node.xyz - P;
    float d = length(wi);
    wi /= d;
    return dot(wi, N) * sqrt(node.w) / (d * d);
}

unsigned int getNearestCluster(vec3 P, vec3 N) {
    vec3 lookAtPoint = P + N * uVoxelSpace.voxelSize;
    unsigned int cluster = unsigned int(-1);

    while(cluster == unsigned int(-1)) {
        vec4 gridSpacePosition = uVoxelSpace.worldToGrid * vec4(lookAtPoint, 1);
        ivec3 voxel = ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z);
        if(voxel.x < 0 || voxel.x >= gridSize.x || voxel.y < 0 || voxel.y >= gridSize.y ||
                voxel.z < 0 || voxel.z >= gridSize.z) {
            return unsigned int(-1);
        }

        cluster = texelFetch(uNodeGrid, voxel, 0).x;
        lookAtPoint += N * uVoxelSpace.voxelSize;
    }

    float coherency = computeCoherency(cluster, P, N);
    ivec2 neighbourOffsetCount = bNeighbourOffsetCountBuffer[cluster];

    int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

    for(int i = neighbourOffsetCount.x; i < end; ++i) {
        int neighbour = bNeighbourBuffer[i];
        float candidateCoherency = computeCoherency(unsigned int(neighbour), P, N);
        if(candidateCoherency > coherency) {
            coherency = candidateCoherency;
            cluster = unsigned int(neighbour);
        }
    }

    return cluster;
}

ivec4 computeVClusters(vec3 P, vec3 N) {
    unsigned int cluster = getNearestCluster(P, N);
    if(cluster == unsigned int(-1)) {
        return ivec4(-1);
    } else {
        //return ivec4(texelFetch(uCurvSkelClustering.visibilityClusterBuffer, int(cluster)).x, -1, -1, -1);
        return ivec4(bVisibilityClusterBuffer[cluster], -1, -1, -1);
    }
}

//ivec4 computeVClusters2(vec3 P, vec3 N) {
//    vec3 lookAtPoint = P + N * uVoxelSpace.voxelSize;
//    unsigned int cluster = unsigned int(-1);

//    while(cluster == unsigned int(-1)) {
//        vec4 gridSpacePosition = uVoxelSpace.worldToGrid * vec4(lookAtPoint, 1);
//        ivec3 voxel = ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z);
//        if(voxel.x < 0 || voxel.x >= gridSize.x || voxel.y < 0 || voxel.y >= gridSize.y ||
//                voxel.z < 0 || voxel.z >= gridSize.z) {
//            break;
//        }

//        cluster = texelFetch(uNodeGrid, voxel, 0).x;
//        lookAtPoint += N * uVoxelSpace.voxelSize;
//    }

//    if(cluster == unsigned int(-1)) {
//        return ivec4(-1, -1, -1, -1);
//    }

//    float coherency = computeCoherency(cluster, P, N);
//    int j = 0;
//    ivec4 result = ivec4(-1, -1, -1, -1);

//    if(coherency > 0) {
//        result[j++] = texelFetch(uCurvSkelClustering.visibilityClusterBuffer, int(cluster)).x;
//    }

//    ivec2 neighbourOffsetCount = texelFetch(uCurvSkelClustering.neighbourOffsetCountBuffer, int(cluster)).xy;
//    int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

//    for(int i = neighbourOffsetCount.x; i < end && j < 4; ++i) {
//        int neighbour = texelFetch(uCurvSkelClustering.neighbourBuffer, i).x;
//        coherency = computeCoherency(uint(neighbour), P, N);

//        if(coherency > 0) {
//            int vcluster = texelFetch(uCurvSkelClustering.visibilityClusterBuffer, neighbour).x;

//            bool exists = false;
//            for(int k = 0; k < j; ++k) {
//                if(result[k] == vcluster) {
//                    exists = true;
//                }
//            }

//            if(!exists) {
//                result[j++] = vcluster;
//            }
//        }
//    }

//    return result;
//}

void main() {
    gridSize = textureSize(uNodeGrid, 0);

    vec3 nNormal = normalize(vNormal);
    fNormalDepth.xyz = nNormal;
    fNormalDepth.w = -vPosition.z / uZFar; // Normalized depth value

    fDiffuse = uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb / PI;

    float shininess = uMaterial.shininess + texture(uMaterial.shininessSampler, vTexCoords).r;
    fGlossyShininess.rgb = uMaterial.Ks * texture(uMaterial.KsSampler, vTexCoords).rgb * (shininess + 2) / (2 * PI);
    fGlossyShininess.a = shininess;

    fVClusterIndices = computeVClusters(vWorldSpacePosition, normalize(vWorldSpaceNormal));

    fGeometryCluster = uint(floor(log(-vPosition.z / uZNear) / log(1 + 2 * tan(uHalfFOVY) / uSy)));
}

);

GLLightGateRenderer6::GeometryPassData::GeometryPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_MaterialUniforms(m_Program),
    m_uMVPMatrix(m_Program, "uMVPMatrix", true),
    m_uMVMatrix(m_Program, "uMVMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true),
    m_uSy(m_Program, "uSy", true),
    m_uHalfFOVY(m_Program, "uHalfFOVY", true),
    m_uZNear(m_Program, "uZNear", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uWorldToGrid(m_Program, "uVoxelSpace.worldToGrid", true),
    m_uVoxelSize(m_Program, "uVoxelSpace.voxelSize", true),
    m_uNodeGrid(m_Program, "uNodeGrid", true) {
}

// This shader sort the clusters of each tile (32x32 pixels) to compute the number of clusters
// in each tile, compute a new local index for each and link back each pixel to this index.
// At the end, we have 0 <= uClustersImage(x, y) <= uClusterCountsImage(tx, ty) - 1
const GLchar* GLLightGateRenderer6::FindUniqueGeometryClusterPassData::s_ComputeShader =
"#version 430\n"
GLOOPS_STRINGIFY(

// Input image computed in the geometry pass
uniform isampler2D uVisibilityClusterSampler;
uniform usampler2D uGeometryClusterSampler;
uniform sampler2D uNormalDepthSampler;

uniform uint uVClusterCount;

// Outputs of the shader:

// Link each pixel to a cluster index local to the tile
layout(r32ui) uniform writeonly uimage2D uClustersImage;

layout(std430, binding = 0) buffer ClusterCountsBuffer {
    uint data[];
} bClusterCountsBuffer;

layout(std430, binding = 3) buffer ClusterBBoxLowerBuffer {
    vec3 bClusterBBoxLowerBuffer[];
};

layout(std430, binding = 4) buffer ClusterBBoxUpperBuffer {
    vec3 bClusterBBoxUpperBuffer[];
};

layout(std430, binding = 5) buffer ClusterVClusterBuffer {
    int bClusterVClusterBuffer[];
};

uniform mat4 uInvProjMatrix;
uniform mat4 uRcpViewMatrix;
uniform vec2 uScreenSize;

// 32x32 is the size of a tile in screen space
layout(local_size_x = 32, local_size_y = 32) in;

const uint cNbThreads = gl_WorkGroupSize.x * gl_WorkGroupSize.y;

shared uint sSampleIndices[cNbThreads];
shared uint sClusterBuffer[cNbThreads];
shared uint sClusterOffsets[cNbThreads];

shared vec3 sViewSpaceBBoxLower[cNbThreads];
shared vec3 sViewSpaceBBoxUpper[cNbThreads];
//shared vec3 sViewSpaceNormals[cNbThreads];

void oddEvenSortClusters() {
    for(uint i = 0; i < cNbThreads; ++i) {
        uint j;
        if(i % 2 == 0) {
            j = 2 * gl_LocalInvocationIndex;
        } else {
            j = 2 * gl_LocalInvocationIndex + 1;
        }

        uint lhs;
        uint rhs;

        if(j < cNbThreads - 1) {
            lhs = sClusterBuffer[j];
            rhs = sClusterBuffer[j + 1];
        }

        barrier();

        if(j < cNbThreads - 1 && lhs > rhs) {
            sClusterBuffer[j + 1] = lhs;
            sClusterBuffer[j] = rhs;

            uint tmp = sSampleIndices[j + 1];
            sSampleIndices[j + 1] = sSampleIndices[j];
            sSampleIndices[j] = tmp;
        }

        barrier();
    }
}

void mergeSortClusters() {
    uint tid = gl_LocalInvocationIndex;

    for(uint i = 1; i < cNbThreads; i *= 2) {
        uint listSize = i;
        uint listIdx = tid / i;
        uint listOffset = listIdx * listSize;
        uint otherListOffset;
        uint newListOffset;

        if(listIdx % 2 == 0) {
            otherListOffset = listOffset + listSize;
            newListOffset = listOffset;
        } else {
            otherListOffset = listOffset - listSize;
            newListOffset = otherListOffset;
        }

        // On doit calculer la position finale de l'element courant
        uint indexInMyList = tid - listOffset;

        uint indexInOtherList = 0;

        uint value = sClusterBuffer[tid];
        uint sIdx = sSampleIndices[tid];

        uint a = 0;
        uint b = listSize;
        uint middle;

        for(uint j = 1; j < listSize; j *= 2) {
            middle = (a + b) / 2;
            uint otherValue = sClusterBuffer[otherListOffset + middle];
            if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
                b = middle;
            } else {
                a = middle;
            }
        }

        uint otherValue = sClusterBuffer[otherListOffset + a];
        if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
            indexInOtherList = a;
        } else {
            indexInOtherList = b;
        }

        uint finalIndex = indexInMyList + indexInOtherList;

        barrier();

        sClusterBuffer[newListOffset + finalIndex] = value;
        sSampleIndices[newListOffset + finalIndex] = sIdx;

        barrier();
    }
}

void blellochExclusiveAddScan() {
    // Up-sweep phase
    uint a = 2 * gl_LocalInvocationIndex;
    uint b = 2 * gl_LocalInvocationIndex + 1;

    for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
        if(gl_LocalInvocationIndex < s) {
            sClusterOffsets[b] += sClusterOffsets[a];

            a = a * 2 + 1;
            b = b * 2 + 1;
        }
        barrier();
    }

    // Down-sweep phase
    if(gl_LocalInvocationIndex == 0) {
        sClusterOffsets[cNbThreads - 1] = 0;
    }
    barrier();

    for(uint s = 1; s < cNbThreads; s *= 2) {
        if(gl_LocalInvocationIndex < s) {
            a = (a - 1) / 2;
            b = (b - 1) / 2;

            uint tmp = sClusterOffsets[b];
            sClusterOffsets[b] += sClusterOffsets[a];
            sClusterOffsets[a] = tmp;
        }
        barrier();
    }
}

void main() {
    // Local ID of the current thread
    uint tid = gl_LocalInvocationIndex;

    // This is the original cluster ID computed for this pixel
    uint cluster = texelFetch(uGeometryClusterSampler, ivec2(gl_GlobalInvocationID.xy), 0).x;
    int vcluster = texelFetch(uVisibilityClusterSampler, ivec2(gl_GlobalInvocationID.xy), 0).x;
    if(vcluster < 0) {
        vcluster = int(uVClusterCount);
    }

    // Initialize the shared memory
    sClusterBuffer[gl_LocalInvocationIndex] = cluster * (uVClusterCount + 1) + vcluster;
    sClusterOffsets[gl_LocalInvocationIndex] = 0;
    sSampleIndices[gl_LocalInvocationIndex] = gl_LocalInvocationIndex;

    barrier();

    // Local sorting
    //oddEvenSortClusters();
    mergeSortClusters();

    bool isUniqueCluster = gl_LocalInvocationIndex == 0 ||
            sClusterBuffer[gl_LocalInvocationIndex] != sClusterBuffer[gl_LocalInvocationIndex - 1];

    // Initialization for scan
    if(isUniqueCluster) {
        sClusterOffsets[gl_LocalInvocationIndex] = 1;
    }
    barrier();

    // Compaction step
    blellochExclusiveAddScan();

    // Compute offset
    if(!isUniqueCluster) {
        sClusterOffsets[gl_LocalInvocationIndex] -= 1;
    }
    barrier();

    // Write the number of clusters for this tile in the output image
    if(gl_LocalInvocationIndex == 0) {
        uint clusterCount = sClusterOffsets[cNbThreads - 1] + 1;
        bClusterCountsBuffer.data[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_WorkGroupSize.x] = clusterCount;
    }

    // Original view sample that have been moved to this cell by the sort step
    uint sampleIndex = sSampleIndices[gl_LocalInvocationIndex];

    // Offset associated to the cluster of this sample
    uint clusterOffset = sClusterOffsets[gl_LocalInvocationIndex];

    // Coordinates of this sample
    uint x = sampleIndex % gl_WorkGroupSize.x;
    uint y = (sampleIndex - x) / gl_WorkGroupSize.x;
    ivec2 globalCoords = ivec2(gl_WorkGroupID.x * gl_WorkGroupSize.x + x,
                         gl_WorkGroupID.y * gl_WorkGroupSize.y + y);

    // Write the offset for this cluster in the new cluster image
    imageStore(uClustersImage, globalCoords, uvec4(clusterOffset, 0, 0, 0));

    vcluster = texelFetch(uVisibilityClusterSampler, globalCoords, 0).x;

    // Read geometric information of the original sample
    vec4 normalDepth = texelFetch(uNormalDepthSampler, globalCoords, 0);
    vec2 pixelNDC = vec2(-1.f, -1.f) + 2.f * (vec2(globalCoords) + vec2(0.5f, 0.5f)) / uScreenSize;
    vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(pixelNDC, 1.f, 1.f);
    farPointViewSpaceCoords = farPointViewSpaceCoords / farPointViewSpaceCoords.w;
    vec4 samplePointViewSpaceCoords = vec4(normalDepth.w * farPointViewSpaceCoords.xyz, 1);

    sViewSpaceBBoxLower[gl_LocalInvocationIndex] = vec3(uRcpViewMatrix * samplePointViewSpaceCoords);
    sViewSpaceBBoxUpper[gl_LocalInvocationIndex] = sViewSpaceBBoxLower[gl_LocalInvocationIndex];

    barrier();

    // Reduce step to get bounds of the clusters
    for(uint s = 1; s < cNbThreads; s *= 2) {
        uint other = tid + s;
        if(other < cNbThreads && sClusterOffsets[other] == clusterOffset) {
            //sData[tid] = max(sData[tid], sData[other]);
            sViewSpaceBBoxLower[tid] = min(sViewSpaceBBoxLower[tid], sViewSpaceBBoxLower[other]);
            sViewSpaceBBoxUpper[tid] = max(sViewSpaceBBoxUpper[tid], sViewSpaceBBoxUpper[other]);
        }
        barrier();
    }

    // Write bounds to ouput images
    if(isUniqueCluster) {
        uint globalOffset = (gl_WorkGroupID.x + gl_NumWorkGroups.x * gl_WorkGroupID.y) * cNbThreads + clusterOffset;

        bClusterBBoxLowerBuffer[globalOffset] = sViewSpaceBBoxLower[tid];
        bClusterBBoxUpperBuffer[globalOffset] = sViewSpaceBBoxUpper[tid];
        bClusterVClusterBuffer[globalOffset] = vcluster;
    }
}

);

GLLightGateRenderer6::FindUniqueGeometryClusterPassData::FindUniqueGeometryClusterPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uVisibilityClusterSampler(m_Program, "uVisibilityClusterSampler", true),
    m_uGeometryClusterSampler(m_Program, "uGeometryClusterSampler", true),
    m_uNormalDepthSampler(m_Program, "uNormalDepthSampler", true),
    m_uVClusterCount(m_Program, "uVClusterCount", true),
    m_uClustersImage(m_Program, "uClustersImage", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uScreenSize(m_Program, "uScreenSize", true) {

    m_Program.use();
    m_uVisibilityClusterSampler.set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uGeometryClusterSampler.set(TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
    m_uNormalDepthSampler.set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uClustersImage.set(IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS);
}

const GLchar* GLLightGateRenderer6::CountGeometryClusterPassData::s_ComputeShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform uint uTileCount;

// Inputs

// For each tile (Tx, Ty), bClusterCountsBuffer[Tx + Ty * 32] is the number of cluster contained in the tile
layout(std430, binding = 0) buffer ClusterCountsBuffer {
    uint bClusterCountsBuffer[];
};

// Outputs

// For each tile (Tx, Ty), bClusterTilesOffsetsBuffer[Tx + Ty * 32] will be the global index of the first cluster
// contained in the tile.
layout(std430, binding = 1) buffer ClusterTilesOffsetsBuffer {
    uint bClusterTilesOffsetsBuffer[];
};

// Will be the total number of clusters
layout(std430, binding = 2) buffer ClusterCount {
    uint bClusterCount;
};

// Will contains for each cluster the index of the associated tile
layout(std430, binding = 3) buffer ClusterToTileBuffer {
    uint bClusterToTileBuffer[];
};

layout(local_size_x = 1024) in;

const uint cNbThreads = gl_WorkGroupSize.x;

shared uint sGroupData[cNbThreads];

void main() {
    uint tileClusterCount = 0;
    // Init
    if(gl_GlobalInvocationID.x >= uTileCount) {
        sGroupData[gl_LocalInvocationIndex] = 0;
    } else {
        tileClusterCount = sGroupData[gl_LocalInvocationIndex] = bClusterCountsBuffer[gl_GlobalInvocationID.x];
    }

    barrier();

    // Scan to compute offsets and cluster count

    // Up-sweep phase
    uint a = 2 * gl_LocalInvocationIndex;
    uint b = 2 * gl_LocalInvocationIndex + 1;

    for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
        if(gl_LocalInvocationIndex < s) {
            sGroupData[b] += sGroupData[a];

            a = a * 2 + 1;
            b = b * 2 + 1;
        }
        barrier();
    }

    // Down-sweep phase
    if(gl_LocalInvocationIndex == 0) {
        sGroupData[cNbThreads - 1] = 0;
    }
    barrier();

    for(uint s = 1; s < cNbThreads; s *= 2) {
        if(gl_LocalInvocationIndex < s) {
            a = (a - 1) / 2;
            b = (b - 1) / 2;

            uint tmp = sGroupData[b];
            sGroupData[b] += sGroupData[a];
            sGroupData[a] = tmp;
        }
        barrier();
    }

    if(gl_GlobalInvocationID.x < uTileCount) {
        uint tileOffset = bClusterTilesOffsetsBuffer[gl_GlobalInvocationID.x] = sGroupData[gl_LocalInvocationIndex];

        if(gl_GlobalInvocationID.x == uTileCount - 1) {
            bClusterCount = sGroupData[gl_LocalInvocationIndex] + bClusterCountsBuffer[gl_GlobalInvocationID.x];
        }

        // Write correspondance from cluster to tile
        for(uint i = 0; i < tileClusterCount; ++i) {
            bClusterToTileBuffer[tileOffset + i] = gl_LocalInvocationIndex;
        }

        barrier();
    }
}

);

GLLightGateRenderer6::CountGeometryClusterPassData::CountGeometryClusterPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uTileCount(m_Program, "uTileCount", true) {
}


const GLchar* GLLightGateRenderer6::VClusterLightAssignmentPassData::s_ComputeShader =
"#version 430\n"
GLOOPS_STRINGIFY(

struct VoxelSpace {
    // The length of an edge of a voxel in world space
    float voxelSize;
    // The transformation matrix that gives for a world space position
    // the associated voxel
    mat4 worldToGrid;
};

uniform VoxelSpace uVoxelSpace;

layout(std430, binding = 4) buffer NodeBuffer {
    vec4 bNodeBuffer[];
};

layout(std430, binding = 5) buffer NeighbourOffsetCountBuffer {
    ivec2 bNeighbourOffsetCountBuffer[];
};

layout(std430, binding = 6) buffer NeighbourBuffer {
    int bNeighbourBuffer[];
};

layout(std430, binding = 7) buffer VisibilityClusterBuffer {
    int bVisibilityClusterBuffer[];
};

uniform usampler3D uNodeGrid;

ivec3 gridSize;

float computeCoherency(unsigned int cluster, vec3 P, vec3 N) {
    vec4 node = bNodeBuffer[cluster];
    vec3 wi = node.xyz - P;
    float d = length(wi);
    wi /= d;
    return dot(wi, N) * sqrt(node.w) / (d * d);
    //return dot(wi, N) / d;
    //return dot(wi, N);
    //return node.w / d;
}

unsigned int getNearestCluster(vec3 P, vec3 N) {
    vec3 lookAtPoint = P + N * uVoxelSpace.voxelSize;
    unsigned int cluster = unsigned int(-1);

    while(cluster == unsigned int(-1)) {
        vec4 gridSpacePosition = uVoxelSpace.worldToGrid * vec4(lookAtPoint, 1);
        ivec3 voxel = ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z);
        if(voxel.x < 0 || voxel.x >= gridSize.x || voxel.y < 0 || voxel.y >= gridSize.y ||
                voxel.z < 0 || voxel.z >= gridSize.z) {
            return unsigned int(-1);
        }

        cluster = texelFetch(uNodeGrid, voxel, 0).x;
        lookAtPoint += N * uVoxelSpace.voxelSize;
    }

    float coherency = computeCoherency(cluster, P, N);
    ivec2 neighbourOffsetCount = bNeighbourOffsetCountBuffer[cluster];

    int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

    for(int i = neighbourOffsetCount.x; i < end; ++i) {
        int neighbour = bNeighbourBuffer[i];
        float candidateCoherency = computeCoherency(unsigned int(neighbour), P, N);
        if(candidateCoherency > coherency) {
            coherency = candidateCoherency;
            cluster = unsigned int(neighbour);
        }
    }

    return cluster;
}

struct VPL {
    vec3 P;
    vec3 N;
    vec3 L;
};

uniform uint uVPLCount;
uniform uint uClusterCount;

// Contains all the VPLs
layout(std430, binding = 0) buffer VPLBuffer {
    VPL bVPLBuffer[];
};

// Outputs
layout(std430, binding = 1) coherent buffer VPLOffsetsCountsBuffer {
    ivec2 bVPLOffsetCountsBuffer[];
};

layout(std430, binding = 2) coherent buffer VPLIndexBuffer {
    uint bVPLIndexBuffer[];
};

layout(std430, binding = 3) coherent buffer NextOffsetBuffer {
    uint bNextOffset;
    uint bVPLGateCount;
};

layout(std430, binding = 8) buffer VClusterGateCountBuffer {
    uint bVClusterGateCountBuffer[];
};

layout(local_size_x = 1024) in;

uint clusterIdx;

bool acceptVPL(VPL vpl) {
    uint cluster = getNearestCluster(vpl.P, vpl.N);
    if(cluster == uint(-1)) {
        return false;
    }

    return bVisibilityClusterBuffer[cluster] == int(clusterIdx);
}

void main() {
    clusterIdx = uint(gl_GlobalInvocationID.x);

    if(clusterIdx >= uClusterCount) {
        return;
    }

    gridSize = textureSize(uNodeGrid, 0);

    uint nbAcceptedLights = 0;
    for(uint i = 0; i < uVPLCount; ++i) {
        if(acceptVPL(bVPLBuffer[i])) {
            ++nbAcceptedLights;
        }
    }

    uint offset = atomicAdd(bNextOffset, nbAcceptedLights);

    bVPLOffsetCountsBuffer[clusterIdx] = ivec2(int(offset), int(nbAcceptedLights));

    uint vplGateCount = nbAcceptedLights * bVClusterGateCountBuffer[clusterIdx];
    atomicAdd(bVPLGateCount, vplGateCount);

    uint c = offset;
    for(uint i = 0; i < uVPLCount; ++i) {
        VPL vpl = bVPLBuffer[i];
        if(acceptVPL(vpl)) {
            bVPLIndexBuffer[c++] = i;
        }
    }

    memoryBarrier();
}

);

GLLightGateRenderer6::VClusterLightAssignmentPassData::VClusterLightAssignmentPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uWorldToGrid(m_Program, "uVoxelSpace.worldToGrid", true),
    m_uVoxelSize(m_Program, "uVoxelSpace.voxelSize", true),
    m_uNodeGrid(m_Program, "uNodeGrid", true),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uClusterCount(m_Program, "uClusterCount", true) {
}

const GLchar* GLLightGateRenderer6::LightAssignmentPassData::s_ComputeShader =
"#version 430\n"
GLOOPS_STRINGIFY(

struct VoxelSpace {
    // The length of an edge of a voxel in world space
    float voxelSize;
    // The transformation matrix that gives for a world space position
    // the associated voxel
    mat4 worldToGrid;
};

uniform VoxelSpace uVoxelSpace;
uniform usampler3D uNodeGrid;

ivec3 gridSize;

layout(std430, binding = 0) buffer NodeBuffer {
    vec4 bNodeBuffer[];
};

layout(std430, binding = 1) buffer NeighbourOffsetCountBuffer {
    ivec2 bNeighbourOffsetCountBuffer[];
};

layout(std430, binding = 2) buffer NeighbourBuffer {
    int bNeighbourBuffer[];
};

layout(std430, binding = 3) buffer VisibilityClusterBuffer {
    int bVisibilityClusterBuffer[];
};

float computeCoherency(unsigned int cluster, vec3 P, vec3 N) {
    vec4 node = bNodeBuffer[cluster];
    vec3 wi = node.xyz - P;
    float d = length(wi);
    wi /= d;
    return dot(wi, N) * sqrt(node.w) / (d * d);
}

unsigned int getNearestCluster(vec3 P, vec3 N) {
    vec3 lookAtPoint = P + N * uVoxelSpace.voxelSize;
    unsigned int cluster = unsigned int(-1);

    while(cluster == unsigned int(-1)) {
        vec4 gridSpacePosition = uVoxelSpace.worldToGrid * vec4(lookAtPoint, 1);
        ivec3 voxel = ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z);
        if(voxel.x < 0 || voxel.x >= gridSize.x || voxel.y < 0 || voxel.y >= gridSize.y ||
                voxel.z < 0 || voxel.z >= gridSize.z) {
            return unsigned int(-1);
        }

        cluster = texelFetch(uNodeGrid, voxel, 0).x;
        lookAtPoint += N * uVoxelSpace.voxelSize;
    }

    float coherency = computeCoherency(cluster, P, N);
    ivec2 neighbourOffsetCount = bNeighbourOffsetCountBuffer[cluster];

    int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

    for(int i = neighbourOffsetCount.x; i < end; ++i) {
        int neighbour = bNeighbourBuffer[i];
        float candidateCoherency = computeCoherency(unsigned int(neighbour), P, N);
        if(candidateCoherency > coherency) {
            coherency = candidateCoherency;
            cluster = unsigned int(neighbour);
        }
    }

    return cluster;
}

struct VPL {
    vec3 P;
    vec3 N;
    vec3 L;
};

uniform uint uVPLCount;

// Contains all the VPLs
layout(std430, binding = 4) buffer VPLBuffer {
    VPL bVPLBuffer[];
};

// Outputs

// For each geometry cluster
layout(std430, binding = 5) coherent buffer VPLOffsetsCountsBuffer {
    ivec2 bVPLOffsetCountsBuffer[];
};

layout(std430, binding = 6) coherent buffer VPLIndexBuffer {
    ivec2 bVPLIndexBuffer[];
};

layout(std430, binding = 7) coherent buffer NextOffsetBuffer {
    uint bNextOffset;
    uint bVPLGateCount;
};

uniform uint uClusterCount;

layout(std430, binding = 8) buffer ClusterTilesOffsetsBuffer {
    uint bClusterTilesOffsetsBuffer[];
};

layout(std430, binding = 9) buffer ClusterToTileBuffer {
    uint bClusterToTileBuffer[];
};

layout(std430, binding = 10) buffer ClusterBBoxLowerBuffer {
    vec3 bClusterBBoxLowerBuffer[];
};

layout(std430, binding = 11) buffer ClusterBBoxUpperBuffer {
    vec3 bClusterBBoxUpperBuffer[];
};

layout(std430, binding = 12) buffer ClusterVClusterBuffer {
    int bClusterVClusterBuffer[];
};

layout(local_size_x = 1024) in;

uniform bool uDoVPLShading;
uniform bool uDoGateShading;
uniform bool uNoShadow;

uint tileID;
uint clusterIdx;
uint clusterLocalID;
uint visibilityClusterIdx;
vec3 bboxLower;
vec3 bboxUpper;
vec3 bboxCenter;
float bboxRadius;

struct Gate {
    vec3 P;
    vec3 N;
    vec3 radius;
};

layout(std430, binding = 13) buffer GateBuffer {
    Gate bGateBuffer[];
};

layout(std430, binding = 14) buffer GateOffsetsCountsBuffer {
    ivec2 bGateOffsetCountsBuffer[];
};

bool testConeVsPoint(vec3 coneP, vec3 coneDir, float cosHalfAngle, vec3 P) {
    vec3 w = normalize(P - coneP);
    float c = dot(coneDir, w);

    return c > cosHalfAngle;
}

bool testConeVsSphere(vec3 coneVertex, vec3 coneAxis, float cosHalfAngle, float sinHalfAngle,
                      vec3 sphereCenter, float sphereRadius) {
    float rcpSin = 1.f / sinHalfAngle;
    vec3 U = coneVertex - (sphereRadius * rcpSin) * coneAxis;
    vec3 D = sphereCenter - U;

    if(dot(coneAxis, D) >= length(D) * cosHalfAngle) {
        D = sphereCenter - coneVertex;
        if(-dot(coneAxis, D) >= length(D) * sinHalfAngle) {
            return length(D) <= sphereRadius;
        } else {
            return true;
        }
    }
    return false;
}

int acceptGateVPL(VPL vpl, uint vSkelCluster) {
    ivec2 offsetCount = bGateOffsetCountsBuffer[vSkelCluster];

    uint end = offsetCount.x + offsetCount.y;

    for(uint i = offsetCount.x; i < end; ++i) {
        Gate gate = bGateBuffer[i];

        vec3 wo = gate.P - vpl.P;
        float d = length(wo);
        wo /= d;
        float coneR = gate.radius.x;
        float hLength = sqrt(d * d + coneR * coneR);
        float cosHalfAngle = d / hLength;
        float sinHalfAngle = coneR / hLength;

        if(testConeVsSphere(vpl.P, wo, cosHalfAngle, sinHalfAngle, bboxCenter, bboxRadius)) {
            return int(i);
        }

//        if(testConeVsPoint(vpl.P, wo, cosHalfAngle, bboxLower) ||
//                testConeVsPoint(vpl.P, wo, cosHalfAngle, bboxUpper) ||
//                testConeVsPoint(vpl.P, wo, cosHalfAngle, bboxCenter)) {
//            return int(i);
//        }
    }

    return -2;
}

int acceptVPL(VPL vpl) {
    if(uNoShadow) {
        return -1;
    }

    uint skelcluster = getNearestCluster(vpl.P, vpl.N);
    if(skelcluster == uint(-1)) {
        return -2;
    }

    uint vSkelCluster = bVisibilityClusterBuffer[skelcluster];

    if(vSkelCluster == int(visibilityClusterIdx)) {
        if(uDoVPLShading) {
            return -1;
        }
        return -2;
    }

    if(!uDoGateShading) {
        return -2;
    }

    return acceptGateVPL(vpl, vSkelCluster);
}

void main() {
    clusterIdx = uint(gl_GlobalInvocationID.x);

    if(clusterIdx >= uClusterCount) {
        return;
    }

    gridSize = textureSize(uNodeGrid, 0);

    tileID = bClusterToTileBuffer[clusterIdx];
    clusterLocalID = clusterIdx - bClusterTilesOffsetsBuffer[tileID];

    uint dataIdx = tileID * 1024 + clusterLocalID;

    visibilityClusterIdx = bClusterVClusterBuffer[dataIdx];
    bboxLower = bClusterBBoxLowerBuffer[dataIdx];
    bboxUpper = bClusterBBoxUpperBuffer[dataIdx];
    bboxCenter = (bboxLower + bboxUpper) * 0.5f;
    bboxRadius = length(bboxLower - bboxUpper) * 0.5f;

    uint nbAcceptedLights = 0;
    for(uint i = 0; i < uVPLCount; ++i) {
        if(acceptVPL(bVPLBuffer[i]) >= -1) {
            ++nbAcceptedLights;
        }
    }

    uint offset = atomicAdd(bNextOffset, nbAcceptedLights);

    bVPLOffsetCountsBuffer[clusterIdx] = ivec2(int(offset), int(nbAcceptedLights));

    uint c = offset;
    for(uint i = 0; i < uVPLCount; ++i) {
        VPL vpl = bVPLBuffer[i];
        int gateIdx = acceptVPL(vpl);
        if(gateIdx >= -1) {
            bVPLIndexBuffer[c++] = ivec2(i, gateIdx);
        }
    }

    memoryBarrier();
}

);

GLLightGateRenderer6::LightAssignmentPassData::LightAssignmentPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uWorldToGrid(m_Program, "uVoxelSpace.worldToGrid", true),
    m_uVoxelSize(m_Program, "uVoxelSpace.voxelSize", true),
    m_uNodeGrid(m_Program, "uNodeGrid", true),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uClusterCount(m_Program, "uClusterCount", true),
    m_uDoVPLShading(m_Program, "uDoVPLShading", true),
    m_uDoGateShading(m_Program, "uDoGateShading", true),
    m_uNoShadow(m_Program, "uNoShadow", true) {
}

const GLchar* GLLightGateRenderer6::TransformVPLToViewSpacePassData::s_ComputeShader =
"#version 430\n"
GLOOPS_STRINGIFY(

struct VPL {
    vec3 P;
    vec3 N;
    vec3 L;
};

uniform uint uVPLCount;

// Contains all the VPLs
layout(std430, binding = 0) coherent buffer VPLBuffer {
    VPL bVPLBuffer[];
};

struct Gate {
    vec3 P;
    vec3 N;
    vec3 radius;
};

uniform uint uGateCount;

layout(std430, binding = 1) coherent buffer GateBuffer {
    Gate bGateBuffer[];
};

uniform mat4 uViewMatrix;
uniform mat3 uNormalMatrix;

layout(local_size_x = 1024) in;

void main() {
    uint idx = uint(gl_GlobalInvocationID.x);

    // A thread represents a cluster
    if(idx < uVPLCount) {
        bVPLBuffer[idx].P = vec3(uViewMatrix * vec4(bVPLBuffer[idx].P, 1));
        bVPLBuffer[idx].N = normalize(uNormalMatrix * bVPLBuffer[idx].N);
    }

    if(idx < uGateCount) {
        bGateBuffer[idx].P = vec3(uViewMatrix * vec4(bGateBuffer[idx].P, 1));
        bGateBuffer[idx].N = normalize(uNormalMatrix * bGateBuffer[idx].N);
    }

    memoryBarrier();
}

);

GLLightGateRenderer6::TransformVPLToViewSpacePassData::TransformVPLToViewSpacePassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uGateCount(m_Program, "uGateCount", true),
    m_uViewMatrix(m_Program, "uViewMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true) {
}

const GLchar* GLLightGateRenderer6::VPLShadingPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vPositionInViewSpace;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 positionInViewSpace = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vPositionInViewSpace = positionInViewSpace.xyz / positionInViewSpace.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer6::VPLShadingPassData::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

// The GBuffer:
uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;

struct VPL {
    vec3 P;
    vec3 N;
    vec3 L;
};

// Contains all the VPLs
layout(std430, binding = 0) coherent buffer VPLBuffer {
    VPL bVPLBuffer[];
};

// For each cluster, records the number of VPLs affecting this cluster and an offset in the index
// buffer
layout(std430, binding = 1) coherent buffer VPLOffsetsCountsBuffer {
    ivec2 bVPLOffsetCountsBuffer[];
};

// Contains the indices of VPLs referenced by the clusters
layout(std430, binding = 2) coherent buffer VPLIndexBuffer {
    uint bVPLIndexBuffer[];
};

in vec3 vPositionInViewSpace;

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vPositionInViewSpace;
//    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    vec3 Kd = vec3(1.f / 3.14f);
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    ivec4 vclusters = texture(uVisibilityClusterSampler, texCoords);

    fFragColor = vec3(0, 0, 0);

    for(int i = 0; i < 1; ++i) {
        int visibilityCluster = vclusters[i];

        if(visibilityCluster < 0) {
            fFragColor = vec3(1, 0, 0);
            return;
        }

        ivec2 offsetCount = bVPLOffsetCountsBuffer[visibilityCluster];

        int end = offsetCount.x + offsetCount.y;

        for(int j = offsetCount.x; j < end; ++j) {
            VPL vpl = bVPLBuffer[bVPLIndexBuffer[j]];

            vec3 wi = vpl.P - fragPosition;
            float dist = length(wi);
            wi /= dist;

            float cos_o = max(0, dot(-wi, vpl.N));
            float cos_i = max(0, dot(wi, normalDepth.xyz));

            dist = max(dist, 0);

            float G = cos_i * cos_o / (dist * dist);

            vec3 r = reflect(-wi, normalDepth.xyz);

            vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
            fFragColor = fFragColor + G * vpl.L;
        }
    }
}

);

GLLightGateRenderer6::VPLShadingPassData::VPLShadingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);

    // Initialize constant uniforms
    m_Program.use();
    m_uSamplers[GBUFFER_NORMAL_DEPTH].set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uSamplers[GBUFFER_DIFFUSE].set(TEXUNIT_GBUFFER_DIFFUSE);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(TEXUNIT_GBUFFER_GLOSSYSHININESS);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES].set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS].set(TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
}

const GLchar* GLLightGateRenderer6::VPLShadingPassData2::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vFarPointViewSpaceCoords;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer6::VPLShadingPassData2::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

// The GBuffer:
uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;
uniform usampler2D uGeometryClusterSampler;

uniform uvec2 uTileCount;

struct VPL {
    vec3 P;
    vec3 N;
    vec3 L;
};

// Contains all the VPLs
layout(std430, binding = 0) coherent buffer VPLBuffer {
    VPL bVPLBuffer[];
};

// For each cluster, records the number of VPLs affecting this cluster and an offset in the index
// buffer
layout(std430, binding = 1) coherent buffer VPLOffsetsCountsBuffer {
    ivec2 bVPLOffsetCountsBuffer[];
};

// Contains the indices of VPLs referenced by the clusters
layout(std430, binding = 2) coherent buffer VPLIndexBuffer {
    ivec2 bVPLIndexBuffer[];
};

layout(std430, binding = 3) buffer ClusterTilesOffsetsBuffer {
    uint bClusterTilesOffsetsBuffer[];
};

struct Gate {
    vec3 P;
    vec3 N;
    vec3 radius;
};

layout(std430, binding = 4) buffer GateBuffer {
    Gate bGateBuffer[];
};

layout(std430, binding = 5) buffer GateOffsetsCountsBuffer {
    ivec2 bGateOffsetCountsBuffer[];
};


vec3 randomColor(uint value) {
    return fract(
                sin(
                    vec3(float(value) * 12.9898,
                         float(value) * 78.233,
                         float(value) * 56.128)
                    )
                * 43758.5453
                );
}

in vec3 vFarPointViewSpaceCoords;

out vec3 fFragColor;

bool testGate(int gateIdx, VPL vpl, vec3 fragP, out float attenuation) {
    if(gateIdx < 0) {
        attenuation = 1.f;
        return true;
    }

    Gate gate = bGateBuffer[gateIdx];

    vec3 wo = gate.P - vpl.P;
    float d = length(wo);
    wo /= d;
    float radius = gate.radius.x;
    float hLength = sqrt(d * d + radius * radius);
    float cosHalfAngle = d / hLength;

    vec3 w = normalize(fragP - vpl.P);
    float spotFactor = dot(wo, w);

    if(spotFactor > cosHalfAngle) {
        //attenuation = 1.0 - (1.0 - spotFactor) / (1.0 - cosHalfAngle);
        attenuation = 1.f;
        //attenuation = dot(normalize(fragP - gate.P), -gate.N);
        return true;
    }

    return false;
}

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    ivec2 tileCoords = ivec2(texCoords * vec2(textureSize(uGeometryClusterSampler, 0).xy));
    tileCoords /= 32;

    uint tileIdx = tileCoords.x + tileCoords.y * uTileCount.x;

    uint clusterLocalID = texture(uGeometryClusterSampler, texCoords).r;
    uint clusterGlobalID = bClusterTilesOffsetsBuffer[tileIdx] + clusterLocalID;

    // Read fragment attributes
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vFarPointViewSpaceCoords;
//    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    vec3 Kd = vec3(1.f / 3.14f);
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    fFragColor = vec3(0, 0, 0);

    int vSkelCluster = texture(uVisibilityClusterSampler, texCoords).r;

//    ivec2 oC = bGateOffsetCountsBuffer[vSkelCluster];
//    int gend = oC.x + oC.y;
//    for(int i = oC.x; i < gend; ++i) {
//        Gate g = bGateBuffer[i];

//        vec3 wi = g.P.xyz - fragPosition;
//        float dist = length(wi);
//        wi /= dist;

//        float cos_o = max(0, dot(-wi, g.N.xyz));
//        float cos_i = max(0, dot(wi, normalDepth.xyz));

//        dist = max(dist, 0);

//        float G = cos_i / (dist * dist);

//        fFragColor = fFragColor + G * vec3(g.r.x * g.r.x);
//    }

//    return;

    ivec2 offsetCount = bVPLOffsetCountsBuffer[clusterGlobalID];

    int end = offsetCount.x + offsetCount.y;

    for(int j = offsetCount.x; j < end; ++j) {
        ivec2 vplIdxGateIdx = bVPLIndexBuffer[j];

        VPL vpl = bVPLBuffer[vplIdxGateIdx.x];

        float attenuation;
        if(vplIdxGateIdx.y >= -1 && testGate(vplIdxGateIdx.y, vpl, fragPosition, attenuation)) {
            vec3 wi = vpl.P - fragPosition;
            float dist = length(wi);
            wi /= dist;

            float cos_o = max(0, dot(-wi, vpl.N));
            float cos_i = max(0, dot(wi, normalDepth.xyz));

            dist = max(dist, 0);

            float G = cos_i * cos_o / (dist * dist);

            vec3 r = reflect(-wi, normalDepth.xyz);

            vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
            fFragColor = fFragColor + G * vpl.L * attenuation;
        }
    }

//    ivec2 gateOffsetCount = bGateOffsetCountsBuffer[vSkelCluster];
//    int end = gateOffsetCount.x + gateOffsetCount.y;
//    for(int i = gateOffsetCount.x; i < end; ++i) {
//        Gate g = bGateBuffer[i];

//        vec3 wi = g.P - fragPosition;

//        float dist = length(wi);
//        wi /= dist;

//        float cos_o = max(0, dot(-wi, g.N));
//        float cos_i = max(0, dot(wi, normalDepth.xyz));

//        float G = cos_i * cos_o / (dist * dist);

//        fFragColor = fFragColor + G * vec3(5 * g.radius * g.radius);
//    }
}

);

GLLightGateRenderer6::VPLShadingPassData2::VPLShadingPassData2():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uTileCount(m_Program, "uTileCount", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS] = gloops::Uniform(m_Program, "uGeometryClusterSampler", true);

    // Initialize constant uniforms
    m_Program.use();
    m_uSamplers[GBUFFER_NORMAL_DEPTH].set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uSamplers[GBUFFER_DIFFUSE].set(TEXUNIT_GBUFFER_DIFFUSE);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(TEXUNIT_GBUFFER_GLOSSYSHININESS);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES].set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS].set(TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
}


const GLchar* GLLightGateRenderer6::GateShadingPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vPositionInViewSpace;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 positionInViewSpace = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vPositionInViewSpace = positionInViewSpace.xyz / positionInViewSpace.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer6::GateShadingPassData::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;

struct Gate {
    vec4 P;
    vec4 N;
    vec4 L;
    vec4 wo;
    vec4 cosHalfAngle;
};

uniform uint uClusterCount;

layout(std430, binding = 0) buffer GateBuffer {
    Gate bGateBuffer[];
};

layout(std430, binding = 1) buffer GateOffsetsCountsBuffer {
    ivec2 bGateOffsetCountsBuffer[];
};

uniform int uCurrentVisibilityCluster;

in vec3 vPositionInViewSpace;

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vPositionInViewSpace;
//    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    vec3 Kd = vec3(1.f / 3.14f);
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    int visibilityCluster = texture(uVisibilityClusterSampler, texCoords).r;

    fFragColor = vec3(0);

    for(int j = 0; j < uClusterCount; ++j) {
        if(j != visibilityCluster && j == 5) {
            ivec2 offsetCount = bGateOffsetCountsBuffer[j];
            int end = offsetCount.x + offsetCount.y;
            for(int i = offsetCount.x; i < end; ++i) {
                Gate gate = bGateBuffer[i];

                vec3 wi = gate.P.xyz - fragPosition;
                float dist = length(wi);
                wi /= dist;

                float spotFactor = dot(gate.wo.xyz, -wi);
                if(spotFactor < gate.cosHalfAngle.x) {
                    continue;
                }

                float cos_o = max(0, dot(-wi, gate.N.xyz));
                float cos_i = max(0, dot(wi, normalDepth.xyz));

                if(cos_o < 0) {
                    continue;
                }

                float G = cos_i * cos_o / (dist * dist);
                //float G = cos_i / (dist * dist);

                vec3 r = reflect(-wi, normalDepth.xyz);

                //float attenuation = 1.0 - (1.0 - spotFactor) / (1.0 - gate.cosHalfAngle.x);
                float attenuation = 1.f;

                //vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
                fFragColor = fFragColor + G * gate.L.rgb * attenuation;
            }
        }
    }
}

);

GLLightGateRenderer6::GateShadingPassData::GateShadingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uClusterCount(m_Program, "uClusterCount", true),
    m_uCurrentVisibilityCluster(m_Program, "uCurrentVisibilityCluster", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);

    m_Program.use();
    m_uSamplers[GBUFFER_NORMAL_DEPTH].set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uSamplers[GBUFFER_DIFFUSE].set(TEXUNIT_GBUFFER_DIFFUSE);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(TEXUNIT_GBUFFER_GLOSSYSHININESS);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES].set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS].set(TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
}

const GLchar* GLLightGateRenderer6::DrawBuffersPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer6::DrawBuffersPassData::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;
uniform sampler2D uNormalDepthSampler;
uniform isampler2D uVisibilityClusterSampler;
uniform isampler2D uDiscontinuitySampler;
uniform usampler2D uGeometryClusterSampler;

layout(std430, binding = 0) buffer ClusterTilesOffsetsBuffer {
    uint bClusterTilesOffsetsBuffer[];
};

layout(std430, binding = 3) buffer ClusterBBoxLowerBuffer {
    vec3 bClusterBBoxLowerBuffer[];
};

layout(std430, binding = 4) buffer ClusterBBoxUpperBuffer {
    vec3 bClusterBBoxUpperBuffer[];
};

layout(std430, binding = 5) buffer ClusterVClusterBuffer {
    int bClusterVClusterBuffer[];
};

layout(std430, binding = 6) buffer ClusterToTileBuffer {
    uint bClusterToTileBuffer[];
};

uniform float uZFar;
uniform uvec2 uTileCount;

uniform int uDataToDisplay;

out vec3 fFragColor;

vec3 randomColor(uint value) {
    return fract(
                sin(
                    vec3(float(value) * 12.9898,
                         float(value) * 78.233,
                         float(value) * 56.128)
                    )
                * 43758.5453
                );
}

vec3 randomColor(uvec3 value) {
    return fract(
                sin(
                    vec3(float(value.x * value.y) * 12.9898,
                         float(value.y * value.z) * 78.233,
                         float(value.z * value.x) * 56.128)
                    )
                * 43758.5453
                );
}

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    ivec2 tileCoords = ivec2(texCoords * vec2(textureSize(uGeometryClusterSampler, 0).xy));
    tileCoords /= 32;

    uint tileIdx = tileCoords.x + tileCoords.y * uTileCount.x;

    if(uDataToDisplay == 0) {
        fFragColor = texture(uNormalDepthSampler, texCoords).rgb;
    } else if(uDataToDisplay == 1) {
        fFragColor = vec3(texture(uNormalDepthSampler, texCoords).a);
    } else if(uDataToDisplay == 2) {
        //int visibilityCluster = texture(uVisibilityClusterSampler, texCoords).r;

        int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);

        uint globalID = bClusterTilesOffsetsBuffer[tileIdx] + uint(geometryCluster);
        tileIdx = bClusterToTileBuffer[globalID];

        int visibilityCluster = bClusterVClusterBuffer[tileIdx * 1024 + geometryCluster];

        if(visibilityCluster < 0 || tileIdx < 10) {
            fFragColor = vec3(0, 0, 0);
        } else {
            fFragColor = randomColor(uint(visibilityCluster));
        }

    } else if(uDataToDisplay == 3) {
        int discontinuity = texture(uDiscontinuitySampler, texCoords).r;
        if(discontinuity == 1) {
            fFragColor = vec3(1, 1, 1);
        } else {
            fFragColor = vec3(0, 0, 0);
        }
    } else if(uDataToDisplay == 4) {
        int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
        fFragColor = randomColor(bClusterTilesOffsetsBuffer[tileIdx] + geometryCluster);
    } else if (uDataToDisplay == 5) {
        int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
        vec3 bboxLower = bClusterBBoxLowerBuffer[tileIdx * 1024 + geometryCluster];
        fFragColor = vec3(-bboxLower.z / uZFar);
    } else if (uDataToDisplay == 6) {
        int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
        vec3 bboxUpper = bClusterBBoxUpperBuffer[tileIdx * 1024 + geometryCluster];
        fFragColor = vec3(-bboxUpper.z / uZFar);
    } else if (uDataToDisplay == 7) {
        int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
        uint globalID = bClusterTilesOffsetsBuffer[tileIdx] + uint(geometryCluster);
        fFragColor = randomColor(bClusterToTileBuffer[globalID]);
    }
}

);

GLLightGateRenderer6::DrawBuffersPassData::DrawBuffersPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uNormalDepthSampler(m_Program, "uNormalDepthSampler", true),
    m_uVisibilityClusterSampler(m_Program, "uVisibilityClusterSampler", true),
    m_uDiscontinuitySampler(m_Program, "uDiscontinuitySampler", true),
    m_uGeometryClusterSampler(m_Program, "uGeometryClusterSampler", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uDataToDisplay(m_Program, "uDataToDisplay", true),
    m_uTileCount(m_Program, "uTileCount", true) {

    m_Program.use();
    m_uNormalDepthSampler.set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uVisibilityClusterSampler.set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uDiscontinuitySampler.set(TEXUNIT_TEX2D_DISCONTINUITYBUFFER);
    m_uGeometryClusterSampler.set(TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
}

const GLchar* GLLightGateRenderer6::DiscontinuityPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer6::DiscontinuityPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;
uniform sampler2D uNormalDepthSampler;
uniform isampler2D uVisibilityClusterSampler;

uniform float uZFar;

out int fFragColor;

void main() {
    int x = int(gl_FragCoord.x);
    int y = int(gl_FragCoord.y);

    vec3 N = texelFetch(uNormalDepthSampler, ivec2(x, y), 0).rgb;
    vec3 Nr = texelFetch(uNormalDepthSampler, ivec2(x + 1, y), 0).rgb;
    vec3 Nb = texelFetch(uNormalDepthSampler, ivec2(x, y + 1), 0).rgb;
    vec3 Nrb = texelFetch(uNormalDepthSampler,ivec2(x + 1, y + 1), 0).rgb;

    float d1 = (dot(N, Nr) + dot(N, Nb) + dot(N, Nrb)) / 3.f;

    float d = texelFetch(uNormalDepthSampler, ivec2(x, y), 0).a;
    float dr = texelFetch(uNormalDepthSampler, ivec2(x + 1, y), 0).a;
    float db = texelFetch(uNormalDepthSampler, ivec2(x, y + 1), 0).a;
    float drb = texelFetch(uNormalDepthSampler,ivec2(x + 1, y + 1), 0).a;

    float meanDepth = (d + dr + db + drb) * 0.25f;
    float d2 = d / meanDepth;

    bool isGeometryDiscontinuous = d1 < 0.9 || d2 < 0.97 || d2 > 1.03;

    int C = texelFetch(uVisibilityClusterSampler, ivec2(x, y), 0).r;
    int Cr = texelFetch(uVisibilityClusterSampler, ivec2(x + 1, y), 0).r;
    int Cb = texelFetch(uVisibilityClusterSampler, ivec2(x, y + 1), 0).r;
    int Crb = texelFetch(uVisibilityClusterSampler, ivec2(x + 1, y + 1), 0).r;

    bool isClusteringDiscontinuous = C != Cr || C != Cb || C != Crb;

    fFragColor = int(isClusteringDiscontinuous && !isGeometryDiscontinuous);
}

);

GLLightGateRenderer6::DiscontinuityPassData::DiscontinuityPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uZFar(m_Program, "uZFar", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);

    m_Program.use();
    m_uSamplers[GBUFFER_NORMAL_DEPTH].set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uSamplers[GBUFFER_DIFFUSE].set(TEXUNIT_GBUFFER_DIFFUSE);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(TEXUNIT_GBUFFER_GLOSSYSHININESS);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES].set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS].set(TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
    m_uZFar.set(getZFar());
}


const GLchar* GLLightGateRenderer6::BlurPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer6::BlurPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;

uniform sampler2D uIrradianceBuffer;
uniform isampler2D uDiscontinuityBuffer;

uniform bool uRenderIrradiance;

out vec3 fFragColor;

void main() {
    int x = int(gl_FragCoord.x);
    int y = int(gl_FragCoord.y);

//    int half = 1;

//    bool isDiscontinuous = false;
//    for(int i = -half; i <= half; ++i) {
//        for(int j = -half; j <= half; ++j) {
//            if(1 == texelFetch(uDiscontinuityBuffer, ivec2(x + i, y + j), 0).r) {
//                isDiscontinuous = true;
//            }
//        }
//    }

//    int discontinuity = texelFetch(uDiscontinuityBuffer, ivec2(x, y), 0).r;
    vec3 irradiance = texelFetch(uIrradianceBuffer, ivec2(x, y), 0).rgb;

//    if(isDiscontinuous) {
//        for(int i = -half; i <= half; ++i) {
//            for(int j = -half; j <= half; ++j) {
//                if(i != 0 || j != 0) {
//                    irradiance += texelFetch(uIrradianceBuffer, ivec2(x + i, y + j), 0).rgb;
//                }
//            }
//        }
//        int nbSamples = (half * 2 + 1);
//        nbSamples *= nbSamples;
//        irradiance /= nbSamples;
//    }

    vec3 Kd;

    if(uRenderIrradiance) {
        Kd = vec3(1.f / 3.14f);
    } else {
        Kd = texelFetch(uDiffuseSampler, ivec2(x, y), 0).rgb;
    }

    fFragColor = Kd * irradiance;
}

);

GLLightGateRenderer6::BlurPassData::BlurPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uIrradianceBuffer(m_Program, "uIrradianceBuffer", true),
    m_uDiscontinuityBuffer(m_Program, "uDiscontinuityBuffer", true),
    m_uRenderIrradiance(m_Program, "uRenderIrradiance", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);

    m_Program.use();
    m_uSamplers[GBUFFER_NORMAL_DEPTH].set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uSamplers[GBUFFER_DIFFUSE].set(TEXUNIT_GBUFFER_DIFFUSE);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(TEXUNIT_GBUFFER_GLOSSYSHININESS);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES].set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS].set(TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
    m_uDiscontinuityBuffer.set(TEXUNIT_TEX2D_DISCONTINUITYBUFFER);
    m_uIrradianceBuffer.set(TEXUNIT_TEX2D_IRRADIANCEBUFFER);
}

GLLightGateRenderer6::GLLightGateRenderer6():
    m_ProjectionMatrix(1.f), m_ViewMatrix(1.f),
    m_bDoVPLShadingPass(true), m_bDoGateShadingPass(true), m_bNoShadow(false), m_bRenderIrradiance(true),
    m_nSamplePerGateCount(1), m_nSelectedVisibilityCluster(-1) {
}

void GLLightGateRenderer6::setResolution(size_t width, size_t height) {
    {
        GLTexFormat formats[GBUFFER_COUNT] = {
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32I, GL_RGBA_INTEGER, GL_INT },
            { GL_RGBA32UI, GL_RGBA_INTEGER, GL_UNSIGNED_INT }
        };
        m_GBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }

    {
        GLTexFormat formats[1] = {
            { GL_R8UI, GL_RED_INTEGER, GL_UNSIGNED_BYTE }
        };

        m_DiscontinuityBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }

    {
        m_IrradianceBuffer.init(width, height);
    }

    // Number of tiles in each dimension
    uint32_t Sx = (width / TILE_SIZE) + (width % TILE_SIZE != 0);
    uint32_t Sy = (height / TILE_SIZE) + (height % TILE_SIZE != 0);

    m_TileCount = glm::ivec2(Sx, Sy);

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GeometryClustersTexture.glId());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI, width, height, 0,
                 GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    GLuint size = Sx * Sy;

    m_GClusterCountsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterTilesOffsetsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterTotalCountBuffer.setSize(sizeof(GLuint), GL_STATIC_DRAW);

    size_t imageSize = width * height;

    m_GClusterToTileBuffer.setSize(imageSize * sizeof(GLuint), GL_STATIC_DRAW);
    m_ClusterBBoxLowerBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterBBoxUpperBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterVClusterBuffer.setSize(imageSize * sizeof(GLint), GL_STATIC_DRAW);
}

void GLLightGateRenderer6::setUp() {
    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_NORMALDEPTH);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_DIFFUSE);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_DIFFUSE).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_GLOSSYSHININESS);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_VCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_VCLUSTER_INDICES).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GEOMETRYCLUSTERS).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_DISCONTINUITYBUFFER);
    glBindTexture(GL_TEXTURE_2D, m_DiscontinuityBuffer.getColorBuffer(0).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_IRRADIANCEBUFFER);
    glBindTexture(GL_TEXTURE_2D, m_IrradianceBuffer.getColorBuffer(0).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GeometryClustersTexture.glId());
    glBindImageTexture(IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS, m_GeometryClustersTexture.glId(),
                       0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32UI);

    glActiveTexture(GL_TEXTURE0);
}

void GLLightGateRenderer6::computeGeometryDiscontinuityBuffer() {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DiscontinuityPass.m_Program.use();
    m_DiscontinuityBuffer.bindForDrawing();

    m_DiscontinuityPass.m_uViewport.set(glm::vec4(0, 0,
                                                  m_DiscontinuityBuffer.getWidth(),
                                                  m_DiscontinuityBuffer.getHeight()));

    m_ScreenTriangle.render();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLLightGateRenderer6::findUniqueGeometryClusters() {
    m_GClusterCountsBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
    m_GClusterTilesOffsetsBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);
    m_GClusterTotalCountBuffer.bindBase(2, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
    m_ClusterVClusterBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);

    m_FindUniqueGeometryClusterPassData.m_Program.use();

    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
    m_FindUniqueGeometryClusterPassData.m_uInvProjMatrix.set(rcpProjMatrix);
    m_FindUniqueGeometryClusterPassData.m_uRcpViewMatrix.set(glm::inverse(m_ViewMatrix));

    m_FindUniqueGeometryClusterPassData.m_uScreenSize.set(float(m_GBuffer.getWidth()),
                                        float(m_GBuffer.getHeight()));

    glDispatchCompute(m_TileCount.x, m_TileCount.y, 1);

    m_CountGeometryClusterPassData.m_Program.use();

    uint32_t tileCount = m_TileCount.x * m_TileCount.y;
    uint32_t workGroupSize = 1024;
    uint32_t workGroupCount = (tileCount / workGroupSize) + (tileCount % workGroupSize);

    m_GClusterToTileBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);

    // ONLY WORKS FOR IMAGE WITH LESS THAN 1024 TILES
    // Can be extended for more but require more compute shader code to handle the parallel
    // segmented scan
    assert(workGroupCount <= 1);

    m_CountGeometryClusterPassData.m_uTileCount.set(tileCount);

    glDispatchCompute(workGroupCount, 1, 1);

    GLuint* ptr = m_GClusterTotalCountBuffer.map<GLuint>(GL_READ_ONLY);
    m_nGClusterCount = *ptr;
    m_GClusterTotalCountBuffer.unmap();

/*
    ptr = m_GClusterToTileBuffer.map<GLuint>(GL_READ_ONLY);

    for(int i = 0; i < m_nGClusterCount; ++i) {
        std::cerr << ptr[i] << " ";
    }
    std::cerr << std::endl;

    m_GClusterToTileBuffer.unmap();

    // Check cluster counts
    std::cerr << "Total number of geometry clusters = " << m_nGClusterCount << std::endl;

    ptr = m_GClusterCountsBuffer.map<GLuint>(GL_READ_ONLY);

    for(int i = 0; i < tileCount; ++i) {
        std::cerr << ptr[i] << " ";
    }
    std::cerr << std::endl;

    m_GClusterCountsBuffer.unmap();

    ptr = m_GClusterTilesOffsetsBuffer.map<GLuint>(GL_READ_ONLY);

    for(int i = 0; i < tileCount; ++i) {
        std::cerr << ptr[i] << " ";
    }
    std::cerr << std::endl;

    m_GClusterTilesOffsetsBuffer.unmap();
*/
}

void GLLightGateRenderer6::geometryPass(const GLScene& scene) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    glViewport(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight());

    m_GeometryPassData.m_Program.use();

    m_GBuffer.bindForDrawing();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 MVPMatrix = m_ProjectionMatrix * m_ViewMatrix;
    glm::mat4 MVMatrix = m_ViewMatrix;
    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

    m_GeometryPassData.m_uMVPMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVPMatrix));
    m_GeometryPassData.m_uMVMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVMatrix));
    m_GeometryPassData.m_uNormalMatrix.setMatrix3(1, GL_FALSE, glm::value_ptr(NormalMatrix));

    m_GeometryPassData.m_uSy.set(float(m_TileCount.y));
    m_GeometryPassData.m_uHalfFOVY.set(glm::radians(0.5f * getFOVY()));
    m_GeometryPassData.m_uZNear.set(getZNear());
    m_GeometryPassData.m_uZFar.set(getZFar());

    GLMaterialManager::TextureUnits units(TEXUNIT_MAT_DIFFUSE, TEXUNIT_MAT_GLOSSY, TEXUNIT_MAT_SHININESS);

    m_GLCurvSkelClusteringBuffers.m_NodeBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
    m_GLCurvSkelClusteringBuffers.m_NeighbourOffsetCountBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);
    m_GLCurvSkelClusteringBuffers.m_NeighbourBuffer.bindBase(2, GL_SHADER_STORAGE_BUFFER);
    m_GLCurvSkelClusteringBuffers.m_VisibilityClusterBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);

    scene.render(m_GeometryPassData.m_MaterialUniforms, units);

    computeGeometryDiscontinuityBuffer();
    findUniqueGeometryClusters();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLLightGateRenderer6::sendVisibilityClusteringGrid(const Scene& scene,
                                                       const VisibilityClustering& vclustering) {
    const CurvSkelClustering& clustering = scene.voxelSpace->getClustering();
    const Grid3D<GraphNodeIndex>& grid = clustering.getGrid();

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX3D_NODEGRID);
    glBindTexture(GL_TEXTURE_3D, m_GLCurvSkelClusteringBuffers.m_Grid.glId());
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32UI, grid.width(), grid.height(), grid.depth(), 0,
                 GL_RED_INTEGER, GL_UNSIGNED_INT, grid.data());

    {
        std::vector<glm::vec4> nodeData;
        nodeData.reserve(clustering.size());
        for(auto nodeIdx: range(clustering.size())) {
            const auto& node = clustering.getNode(nodeIdx);
            nodeData.emplace_back(glm::vec4(node.P.x, node.P.y, node.P.z, node.maxball));
        }

        m_GLCurvSkelClusteringBuffers.m_NodeBuffer.setData(nodeData.size(), nodeData.data(), GL_STATIC_DRAW);
    }

    {
        std::vector<int32_t> neighbourLists;
        std::vector<glm::ivec2> neighbourOffsetsCounts;
        neighbourOffsetsCounts.reserve(clustering.size());
        std::vector<int32_t> visibilityClusters;
        visibilityClusters.reserve(clustering.size());

        int offset = 0;
        for(auto nodeIdx: range(clustering.size())) {
            visibilityClusters.emplace_back(vclustering.getClusterOf(nodeIdx));
            const auto& neighbours = clustering.neighbours(nodeIdx);
            for(auto neighbourIdx: neighbours) {
                neighbourLists.emplace_back(neighbourIdx);
            }
            auto count = neighbours.size();
            neighbourOffsetsCounts.emplace_back(glm::ivec2(offset, count));
            offset += count;
        }

        m_GLCurvSkelClusteringBuffers.m_NeighbourBuffer.setData(neighbourLists.size(), neighbourLists.data(), GL_STATIC_DRAW);
        m_GLCurvSkelClusteringBuffers.m_NeighbourOffsetCountBuffer.setData(neighbourOffsetsCounts.size(), neighbourOffsetsCounts.data(), GL_STATIC_DRAW);
        m_GLCurvSkelClusteringBuffers.m_VisibilityClusterBuffer.setData(visibilityClusters.size(), visibilityClusters.data(), GL_STATIC_DRAW);
    }

    std::vector<GLuint> vclusterGateCounts;
    for(uint32_t vcluster: range(vclustering.size())) {
        vclusterGateCounts.emplace_back(vclustering[vcluster].getNeighbourLinks().size());
    }

    m_GLCurvSkelClusteringBuffers.m_VClusterGateCountBuffer.setData(vclusterGateCounts.size(), vclusterGateCounts.data(), GL_STATIC_DRAW);

    m_GeometryPassData.m_Program.use();
    m_GeometryPassData.m_uWorldToGrid.set(convert(scene.voxelSpace->getWorldToLocalTransform()));
    m_GeometryPassData.m_uVoxelSize.set(scene.voxelSpace->getVoxelSize());
    m_GeometryPassData.m_uNodeGrid.set(TEXUNIT_TEX3D_NODEGRID);

    m_WorldToGridMatrix = convert(scene.voxelSpace->getWorldToLocalTransform());

    m_VClusterLightAssigmentPassData.m_Program.use();
    m_VClusterLightAssigmentPassData.m_uWorldToGrid.set(m_WorldToGridMatrix);
    m_VClusterLightAssigmentPassData.m_uVoxelSize.set(scene.voxelSpace->getVoxelSize());
    m_VClusterLightAssigmentPassData.m_uNodeGrid.set(TEXUNIT_TEX3D_NODEGRID);

    m_LightAssignmentPassData.m_Program.use();
    m_LightAssignmentPassData.m_uWorldToGrid.set(m_WorldToGridMatrix);
    m_LightAssignmentPassData.m_uVoxelSize.set(scene.voxelSpace->getVoxelSize());
    m_LightAssignmentPassData.m_uNodeGrid.set(TEXUNIT_TEX3D_NODEGRID);

    m_FindUniqueGeometryClusterPassData.m_Program.use();
    m_FindUniqueGeometryClusterPassData.m_uVClusterCount.set(GLuint(vclustering.size()));
}

struct GateSample {
    Vec3f P;
    Vec3f N;
    Col3f E; // Irradiance affecting the sample
};

struct GateVPLSpot {
    Vec3f P;
    Vec3f N;
    Col3f L;
    Vec3f wo; // direction from the VPL to the gate
    float cosHalfAngle; // Half angle of the spot
};

void GLLightGateRenderer6::sendVPLsOLD(const VPLContainer& vpls, const Scene& scene,
                                   const VisibilityClustering& vclustering) {
    // Init buffers' data
    std::vector<glm::vec4> vplBuffer;
    for(const auto& vpl: vpls.orientedVPLs) {
        glm::vec4 lightPosition = glm::vec4(convert(vpl.P), 1);
        glm::vec3 lightNormal = convert(vpl.N);

        vplBuffer.emplace_back(lightPosition);
        vplBuffer.emplace_back(glm::vec4(lightNormal, 0));
        vplBuffer.emplace_back(glm::vec4(vpl.L.r, vpl.L.g, vpl.L.b, 0));
    }

    m_VPLBuffer.setData(vplBuffer.size(), vplBuffer.data(), GL_DYNAMIC_DRAW);
    m_VPLOffsetsCountsBuffer.setSize(sizeof(glm::ivec2) * vclustering.size(), GL_DYNAMIC_DRAW);
    m_VPLIndexBuffer.setSize(vpls.orientedVPLs.size() * vclustering.size() * sizeof(GLuint), GL_DYNAMIC_DRAW);

    GLuint data[] = { 0, 0 };

    m_VClusterLightAssigmentPassData.m_NextOffsetBuffer.setData(2, data, GL_DYNAMIC_DRAW);

    m_VClusterLightAssigmentPassData.m_Program.use();

    // Set Uniforms
    m_VClusterLightAssigmentPassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
    m_VClusterLightAssigmentPassData.m_uClusterCount.set(GLuint(vclustering.size()));

    // Bind buffers
    m_VPLBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
    m_VPLOffsetsCountsBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);
    m_VPLIndexBuffer.bindBase(2, GL_SHADER_STORAGE_BUFFER);
    m_VClusterLightAssigmentPassData.m_NextOffsetBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_GLCurvSkelClusteringBuffers.m_NodeBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
    m_GLCurvSkelClusteringBuffers.m_NeighbourOffsetCountBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);
    m_GLCurvSkelClusteringBuffers.m_NeighbourBuffer.bindBase(6, GL_SHADER_STORAGE_BUFFER);
    m_GLCurvSkelClusteringBuffers.m_VisibilityClusterBuffer.bindBase(7, GL_SHADER_STORAGE_BUFFER);
    m_GLCurvSkelClusteringBuffers.m_VClusterGateCountBuffer.bindBase(8, GL_SHADER_STORAGE_BUFFER);

    // Launch compute shader
    uint32_t workGroupSize = 1024;
    uint32_t workItemCount = (vclustering.size() / workGroupSize) + (vclustering.size() % workGroupSize != 0);
    glDispatchCompute(workItemCount, 1, 1);

    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

    m_TransformVPLToViewSpacePassData.m_Program.use();

    m_TransformVPLToViewSpacePassData.m_uViewMatrix.set(m_ViewMatrix);
    m_TransformVPLToViewSpacePassData.m_uNormalMatrix.set(NormalMatrix);
    m_TransformVPLToViewSpacePassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));

    workItemCount = (vpls.orientedVPLs.size() / workGroupSize) + (vpls.orientedVPLs.size() % workGroupSize != 0);
    glDispatchCompute(workItemCount, 1, 1);

    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void GLLightGateRenderer6::sendVPLs(const VPLContainer& vpls, const Scene& scene,
                                   const VisibilityClustering& vclustering) {
    uint32_t workGroupSize = 1024;
    uint32_t workItemCount;

    // Init buffers' data

    {
        std::vector<glm::vec4> vplBuffer;
        for(const auto& vpl: vpls.orientedVPLs) {
            glm::vec4 lightPosition = glm::vec4(convert(vpl.P), 1);
            glm::vec3 lightNormal = convert(vpl.N);

            vplBuffer.emplace_back(lightPosition);
            vplBuffer.emplace_back(glm::vec4(lightNormal, 0));
            vplBuffer.emplace_back(glm::vec4(vpl.L.r, vpl.L.g, vpl.L.b, 0));
        }

        m_VPLBuffer.setData(vplBuffer.size(), vplBuffer.data(), GL_DYNAMIC_DRAW);
    }

    GLuint gateCount = 0;
    {
        std::vector<glm::vec4> gateBuffer;
        std::vector<glm::ivec2> gateOffsetCountBuffer;

        int offset = 0;
        for(auto vcluster: range(vclustering.size())) {
            for(const auto& neighbourLink: vclustering[vcluster].getNeighbourLinks()) {
                const auto& gate = vclustering.getGate(neighbourLink);
                gateBuffer.emplace_back(glm::vec4(gate.center.x, gate.center.y, gate.center.z, 1));
                gateBuffer.emplace_back(glm::vec4(gate.N.x, gate.N.y, gate.N.z, 0));
                gateBuffer.emplace_back(glm::vec4(gate.radius, 0, 0, 0));
            }
            int count = vclustering[vcluster].getNeighbourLinks().size();
            gateOffsetCountBuffer.emplace_back(glm::ivec2(offset, count));
            offset += count;
            gateCount += count;
        }

        m_GateBuffer.setData(gateBuffer, GL_STATIC_DRAW);
        m_GateOffsetsCountsBuffer.setData(gateOffsetCountBuffer, GL_STATIC_DRAW);
    }





    m_VPLOffsetsCountsBuffer.setSize(sizeof(glm::ivec2) * m_nGClusterCount, GL_DYNAMIC_DRAW);
    m_VPLIndexBuffer.setSize(vpls.orientedVPLs.size() * m_nGClusterCount * sizeof(glm::ivec2), GL_DYNAMIC_DRAW);

    GLuint data[] = { 0, 0 };

    m_LightAssignmentPassData.m_NextOffsetBuffer.setData(2, data, GL_DYNAMIC_DRAW);

    m_LightAssignmentPassData.m_Program.use();

    // Set Uniforms
    m_LightAssignmentPassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
    m_LightAssignmentPassData.m_uClusterCount.set(GLuint(m_nGClusterCount));
    m_LightAssignmentPassData.m_uDoVPLShading.set(m_bDoVPLShadingPass);
    m_LightAssignmentPassData.m_uDoGateShading.set(m_bDoGateShadingPass);
    m_LightAssignmentPassData.m_uNoShadow.set(m_bNoShadow);

//    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);
//    m_LightAssignmentPassData.m_uWorldToGrid.set(m_WorldToGridMatrix * rcpViewMatrix);

    // Bind buffers
    m_GLCurvSkelClusteringBuffers.m_NodeBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
    m_GLCurvSkelClusteringBuffers.m_NeighbourOffsetCountBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);
    m_GLCurvSkelClusteringBuffers.m_NeighbourBuffer.bindBase(2, GL_SHADER_STORAGE_BUFFER);
    m_GLCurvSkelClusteringBuffers.m_VisibilityClusterBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_VPLBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
    m_VPLOffsetsCountsBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);
    m_VPLIndexBuffer.bindBase(6, GL_SHADER_STORAGE_BUFFER);
    m_LightAssignmentPassData.m_NextOffsetBuffer.bindBase(7, GL_SHADER_STORAGE_BUFFER);

    m_GClusterTilesOffsetsBuffer.bindBase(8, GL_SHADER_STORAGE_BUFFER);
    m_GClusterToTileBuffer.bindBase(9, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxLowerBuffer.bindBase(10, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxUpperBuffer.bindBase(11, GL_SHADER_STORAGE_BUFFER);
    m_ClusterVClusterBuffer.bindBase(12, GL_SHADER_STORAGE_BUFFER);

    m_GateBuffer.bindBase(13, GL_SHADER_STORAGE_BUFFER);
    m_GateOffsetsCountsBuffer.bindBase(14, GL_SHADER_STORAGE_BUFFER);

    // Launch compute shader
    workItemCount = (m_nGClusterCount / workGroupSize) + (m_nGClusterCount % workGroupSize != 0);
    glDispatchCompute(workItemCount, 1, 1);

    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);



    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

    m_TransformVPLToViewSpacePassData.m_Program.use();

    m_VPLBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
    m_GateBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);

    m_TransformVPLToViewSpacePassData.m_uViewMatrix.set(m_ViewMatrix);
    m_TransformVPLToViewSpacePassData.m_uNormalMatrix.set(NormalMatrix);
    m_TransformVPLToViewSpacePassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
    m_TransformVPLToViewSpacePassData.m_uGateCount.set(gateCount);

    uint32_t taskCount = embree::max(uint32_t(vpls.orientedVPLs.size()), uint32_t(gateCount));
    workItemCount = (taskCount / workGroupSize) + (taskCount % workGroupSize != 0);
    glDispatchCompute(workItemCount, 1, 1);

    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void GLLightGateRenderer6::displayData(GLVisualDataRenderer& renderer, const Scene& scene,
                 const VPLContainer& vpls, const VisibilityClustering& vclustering) {
//    sendVPLs(vpls, scene, vclustering);
//    for(uint32_t j = 5; j < 6; ++j) {
//        bool stop = false;
//        for(const auto& neighbourLink:
//            index(vclustering[j].getNeighbourLinks())) {
//            if(stop) {
//                break;
//            }
//            stop = true;
//            auto gate = vclustering.getGate(neighbourLink.second);

//            AffineSpace3f frame = coordinateSystem(gate.center, gate.N);

//            Vec3f pointOnCircle = embree::xfmPoint(frame, Vec3f(gate.radius, 0, 0));

//            // Transforme chaque VPL en spot
//            for(uint32_t vplIdx: m_ClusterVPLs[j]) {
//                const OrientedVPL& vpl = vpls.orientedVPLs[vplIdx];

//                GateVPLSpot spot;
//                spot.P = vpl.P;
//                spot.N = vpl.N;
//                spot.L = vpl.L;
//                spot.wo = embree::normalize(gate.center - vpl.P);
//                spot.cosHalfAngle = embree::dot(spot.wo, embree::normalize(pointOnCircle - vpl.P));

//                renderer.renderLine(spot.P, gate.center, vpl.L, vpl.L, 1);
//                renderer.renderLine(spot.P, pointOnCircle, Col3f(1, 0, 0), Col3f(1, 0, 0), 1);
//            }
//        }
//    }

    size_t imageSize = m_GBuffer.getWidth() * m_GBuffer.getHeight();
    size_t tileCount = m_TileCount.x * m_TileCount.y;

    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<GLuint> clusterCount(tileCount);
    //std::vector<GLuint> clusterTilesOffsets(tileCount);

    //m_GClusterTilesOffsetsBuffer.getData(clusterTilesOffsets.size(), clusterTilesOffsets.data());
    m_GClusterCountsBuffer.getData(clusterCount.size(), clusterCount.data());
    m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());

    renderer.setUp();

    for(auto pair: index(clusterCount)) {
        GLuint offset = 1024 * pair.first;
        for(auto i: range(pair.second)) {
            glm::vec3 upper = glm::vec3(bboxUpper[offset + i]), lower = glm::vec3(bboxLower[offset + i]);
            float radius = glm::length(upper - lower) * 0.5f;
            renderer.renderSphere(0.5f * convert(upper + lower), radius, Col3f(1, 0, 0));
        }
    }
}

void GLLightGateRenderer6::sendGates(const VPLContainer& vpls, const Scene& scene,
                                    const VisibilityClustering& vclustering) {
    std::vector<GateVPLSpot> gateSamples;
    std::vector<glm::ivec2> gateOffsetsCounts;

    // Compute the VPLs contained in each cluster
    m_ClusterVPLs.clear();
    m_ClusterVPLs.resize(vclustering.size());

    // Regroupe les VPLs par cluster de visibilité
    for(const auto& vpl: index(vpls.orientedVPLs)) {
        GraphNodeIndex idx = scene.voxelSpace->getClustering().getNearestNode(vpl.second.P, vpl.second.N);
        if(idx == UNDEFINED_NODE) {
            continue;
        }
        int clusterIdx = vclustering.getClusterOf(idx);
        if(clusterIdx < 0) {
            continue;
        }
        m_ClusterVPLs[clusterIdx].push_back(vpl.first);
    }

    int offset = 0;
    for(uint32_t j = 0; j < vclustering.size(); ++j) {
        int count = 0;
        for(const auto& neighbourLink:
            index(vclustering[j].getNeighbourLinks())) {
            auto gate = vclustering.getGate(neighbourLink.second);

            AffineSpace3f frame = coordinateSystem(gate.center, gate.N);

            Vec3f pointOnCircle = embree::xfmPoint(frame, Vec3f(gate.radius, 0, 0));

            // Transforme chaque VPL en spot
            for(uint32_t vplIdx: m_ClusterVPLs[j]) {
                const OrientedVPL& vpl = vpls.orientedVPLs[vplIdx];

                GateVPLSpot spot;
                spot.P = vpl.P;
                spot.N = vpl.N;
                spot.L = vpl.L;
                spot.wo = gate.center - vpl.P;
                float d = embree::length(spot.wo);
                spot.wo /= d;
                //spot.cosHalfAngle = embree::dot(spot.wo, embree::normalize(pointOnCircle - vpl.P));
                //float r = 1.3 * gate.radius;

                spot.cosHalfAngle = d / embree::sqrt(d * d + gate.radius * gate.radius);
                //spot.cosHalfAngle = 0.01;

                gateSamples.emplace_back(spot);
            }
            count += m_ClusterVPLs[j].size();
        }
        gateOffsetsCounts.emplace_back(glm::ivec2(offset, count));
        offset += count;
    }

    //std::cerr << "NB VPL GATES = " << gateSamples.size() << std::endl;

    std::vector<glm::vec4> gateSamplesData;

    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

    for(const auto& gateSample: index(gateSamples)) {
        const GateVPLSpot& sample = gateSample.second;

        glm::vec4 samplePosition = m_ViewMatrix * glm::vec4(convert(sample.P), 1);
        glm::vec3 sampleNormal = NormalMatrix * convert(sample.N);
        glm::vec4 sampleWo = m_ViewMatrix * glm::vec4(convert(sample.wo), 0);

        gateSamplesData.push_back(samplePosition);
        gateSamplesData.push_back(glm::vec4(sampleNormal, 0));
        gateSamplesData.push_back(glm::vec4(sample.L.r, sample.L.g, sample.L.b, 0));
        gateSamplesData.push_back(sampleWo);
        gateSamplesData.push_back(glm::vec4(sample.cosHalfAngle, 0, 0, 0));
    }

    m_nClusterCount = vclustering.size();

    m_GateBuffer.setData(gateSamplesData.size(), gateSamplesData.data(), GL_DYNAMIC_DRAW);
    m_GateOffsetsCountsBuffer.setData(gateOffsetsCounts.size(), gateOffsetsCounts.data(), GL_DYNAMIC_DRAW);
}

void GLLightGateRenderer6::shadingPassOLD(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls, const Scene& scene, const VisibilityClustering& vclustering) {

    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::CapabilityRestorer blending(GL_BLEND, true);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLint bindedFramebuffer;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &bindedFramebuffer);

    m_IrradianceBuffer.bindForDrawing();

    glViewport(x, y, width, height);

    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);

    glClear(GL_COLOR_BUFFER_BIT);

    // VPL Shading pass

    sendVPLsOLD(vpls, scene, vclustering);

    if(m_bDoVPLShadingPass) {
        m_VPLShadingPassData.m_Program.use();
        m_VPLShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
        m_VPLShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);

        m_VPLBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
        m_VPLOffsetsCountsBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);
        m_VPLIndexBuffer.bindBase(2, GL_SHADER_STORAGE_BUFFER);

        m_ScreenTriangle.render();
    }

    // Gate Shading pass

    if(m_bDoGateShadingPass) {
        sendGates(vpls, scene, vclustering);

        m_GateShadingPassData.m_Program.use();
        m_GateShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
        m_GateShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);

        m_GateShadingPassData.m_uClusterCount.set(m_nClusterCount);

        m_GateShadingPassData.m_uCurrentVisibilityCluster.set(m_nSelectedVisibilityCluster);

        m_GateBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
        m_GateOffsetsCountsBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);

        m_ScreenTriangle.render();
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bindedFramebuffer);

    // Final pass

    glClear(GL_COLOR_BUFFER_BIT);

    m_BlurPassData.m_Program.use();

    m_ScreenTriangle.render();
}

void GLLightGateRenderer6::shadingPass(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls, const Scene& scene, const VisibilityClustering& vclustering) {

    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::CapabilityRestorer blending(GL_BLEND, true);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLint bindedFramebuffer;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &bindedFramebuffer);

    m_IrradianceBuffer.bindForDrawing();

    glViewport(x, y, width, height);

    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);

    glClear(GL_COLOR_BUFFER_BIT);

    // VPL Shading pass

    sendVPLs(vpls, scene, vclustering);

    //if(m_bDoVPLShadingPass) {
        m_VPLShadingPassData2.m_Program.use();
        m_VPLShadingPassData2.m_uViewport.set(float(x), float(y), float(width), float(height));
        m_VPLShadingPassData2.m_uInvProjMatrix.set(rcpProjMatrix);
        m_VPLShadingPassData2.m_uTileCount.set(m_TileCount);

        m_VPLBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
        m_VPLOffsetsCountsBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);
        m_VPLIndexBuffer.bindBase(2, GL_SHADER_STORAGE_BUFFER);
        m_GClusterTilesOffsetsBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
        m_GateBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
        m_GateOffsetsCountsBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);

        m_ScreenTriangle.render();
    //}

    // Gate Shading pass

//    if(m_bDoGateShadingPass) {
//        sendGates(vpls, scene, vclustering);

//        m_GateShadingPassData.m_Program.use();
//        m_GateShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
//        m_GateShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);

//        m_GateShadingPassData.m_uClusterCount.set(m_nClusterCount);

//        m_GateShadingPassData.m_uCurrentVisibilityCluster.set(m_nSelectedVisibilityCluster);

//        m_GateBuffer.bindBase(0);
//        m_GateOffsetsCountsBuffer.bindBase(1);

//        m_ScreenTriangle.render();
//    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bindedFramebuffer);

    // Final pass

    glClear(GL_COLOR_BUFFER_BIT);

    m_BlurPassData.m_Program.use();
    m_BlurPassData.m_uRenderIrradiance.set(m_bRenderIrradiance);

    m_ScreenTriangle.render();
}

void GLLightGateRenderer6::drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uTileCount.set(m_TileCount);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(dataToDisplay);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);

    m_GClusterTilesOffsetsBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
    m_ClusterVClusterBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);
    m_GClusterToTileBuffer.bindBase(6, GL_SHADER_STORAGE_BUFFER);

    m_ScreenTriangle.render();
}

void GLLightGateRenderer6::drawBuffers(int x, int y, size_t width, size_t height) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uTileCount.set(m_TileCount);

    m_GClusterTilesOffsetsBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
    m_ClusterVClusterBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);
    m_GClusterToTileBuffer.bindBase(6, GL_SHADER_STORAGE_BUFFER);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    // Draw normals
    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);
    m_DrawBuffersPassData.m_uDataToDisplay.set(0);

    m_ScreenTriangle.render();

    // Draw depth
    glViewport(x + width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(1);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw VCluster
    glViewport(x + 2 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(2);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw discontinuity buffer
    glViewport(x + 3 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(3);
    m_DrawBuffersPassData.m_uViewport.set(fX + 3 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster buffer
    glViewport(x + 4 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(4);
    m_DrawBuffersPassData.m_uViewport.set(fX + 4 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow lower point depth buffer
    glViewport(x, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(5);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow upper point depth buffer
    glViewport(x + width, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(6);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();
}

}
