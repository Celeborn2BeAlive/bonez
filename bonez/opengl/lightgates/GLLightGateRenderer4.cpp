#include "GLLightGateRenderer4.hpp"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "bonez/opengl/utils/embree_opengl.hpp"

#include "bonez/common/data/ColorMap.hpp"
#include "bonez/common/Progress.hpp"

namespace BnZ {

using namespace gloops;

const GLchar* GLLightGateRenderer4::GeometryPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;

uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat3 uNormalMatrix;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoords;
out vec3 vWorldSpaceNormal;
out vec3 vWorldSpacePosition;

void main() {
    vWorldSpacePosition = aPosition;
    vWorldSpaceNormal = aNormal;
    vPosition = vec3(uMVMatrix * vec4(aPosition, 1));
    vNormal = uNormalMatrix * aNormal;
    vTexCoords = aTexCoords;

    gl_Position = uMVPMatrix * vec4(aPosition, 1);
}

);

const GLchar* GLLightGateRenderer4::GeometryPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoords;
in vec3 vWorldSpacePosition;
in vec3 vWorldSpaceNormal;

struct Material {
    vec3 Kd;
    vec3 Ks;
    float shininess;
    sampler2D KdSampler;
    sampler2D KsSampler;
    sampler2D shininessSampler;
};

uniform Material uMaterial;

uniform float uZFar;

struct VoxelSpace {
    // The length of an edge of a voxel in world space
    float voxelSize;
    // The transformation matrix that gives for a world space position
    // the associated voxel
    mat4 worldToGrid;
};

struct CurvSkelClustering {
    // Contains world space position in xyz and maxball radius in w
    samplerBuffer nodeBuffer;
    // neighbourOffsetCountBuffer[i] contains the offset of the neighbour list
    // of node i in x and the size of the list in y
    isamplerBuffer neighbourOffsetCountBuffer;
    // All the neighbour lists
    isamplerBuffer neighbourBuffer;
    // Connect each voxel to a node
    usampler3D grid;
    // neighbourOffsetCountBuffer[i] is the visibility cluster of
    // node i
    isamplerBuffer visibilityClusterBuffer;
};

uniform CurvSkelClustering uCurvSkelClustering;

uniform isampler3D uVClusterGrid;

uniform vec3 uCameraPosition;
uniform VoxelSpace uVoxelSpace;

layout(location = 0) out vec4 fNormalDepth;
layout(location = 1) out vec3 fDiffuse;
layout(location = 2) out vec4 fGlossyShininess;
layout(location = 3) out int fVClusterIndex;

const float PI = 3.14159265358979323846264;

float computeCoherency(unsigned int cluster, vec3 P, vec3 N) {
    vec4 node = texelFetch(uCurvSkelClustering.nodeBuffer, int(cluster));
    vec3 wi = node.xyz - P;
    float d = length(wi);
    wi /= d;
    return dot(wi, N) * sqrt(node.w) / (d * d);
    //return dot(wi, N) / d;
    //return dot(wi, N);
    //return node.w / d;
}

unsigned int getNearestCluster(vec3 P, vec3 N) {
    vec3 lookAtPoint = P + N * uVoxelSpace.voxelSize;
    unsigned int cluster = unsigned int(-1);

    while(cluster == unsigned int(-1)) {
        vec4 gridSpacePosition = uVoxelSpace.worldToGrid * vec4(lookAtPoint, 1);
        ivec3 voxel = ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z);
        cluster = texelFetch(uCurvSkelClustering.grid, voxel, 0).x;
        lookAtPoint += N * uVoxelSpace.voxelSize;
    }

    float coherency = computeCoherency(cluster, P, N);
    ivec2 neighbourOffsetCount = texelFetch(uCurvSkelClustering.neighbourOffsetCountBuffer, int(cluster)).xy;
    int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

    for(int i = neighbourOffsetCount.x; i < end; ++i) {
        int neighbour = texelFetch(uCurvSkelClustering.neighbourBuffer, i).x;
        float candidateCoherency = computeCoherency(unsigned int(neighbour), P, N);
        if(candidateCoherency > coherency) {
            coherency = candidateCoherency;
            cluster = unsigned int(neighbour);
        }
    }

    return cluster;
}

void main() {
    vec3 nNormal = normalize(vNormal);
    fNormalDepth.xyz = nNormal;
    fNormalDepth.w = -vPosition.z / uZFar; // Normalized depth value

    fDiffuse = uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb / PI;

    float shininess = uMaterial.shininess + texture(uMaterial.shininessSampler, vTexCoords).r;
    fGlossyShininess.rgb = uMaterial.Ks * texture(uMaterial.KsSampler, vTexCoords).rgb * (shininess + 2) / (2 * PI);
    fGlossyShininess.a = shininess;

    unsigned int cluster = getNearestCluster(vWorldSpacePosition, normalize(vWorldSpaceNormal));
    fVClusterIndex = texelFetch(uCurvSkelClustering.visibilityClusterBuffer, int(cluster)).x;
}

);

GLLightGateRenderer4::GeometryPassData::GeometryPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_MaterialUniforms(m_Program),
    m_uMVPMatrix(m_Program, "uMVPMatrix", true),
    m_uMVMatrix(m_Program, "uMVMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uVClusterGrid(m_Program, "uVClusterGrid", true),
    m_uWorldToGrid(m_Program, "uVoxelSpace.worldToGrid", true),
    m_uVoxelSize(m_Program, "uVoxelSpace.voxelSize", true),
    m_uCameraPosition(m_Program, "uCameraPosition", true),
    m_uNodeBuffer(m_Program, "uCurvSkelClustering.nodeBuffer", true),
    m_uNeighbourOffsetCountBuffer(m_Program, "uCurvSkelClustering.neighbourOffsetCountBuffer", true),
    m_uNeighbourBuffer(m_Program, "uCurvSkelClustering.neighbourBuffer", true),
    m_uGrid(m_Program, "uCurvSkelClustering.grid", true),
    m_uVisibilityClusterBuffer(m_Program, "uCurvSkelClustering.visibilityClusterBuffer", true) {
}

const GLchar* GLLightGateRenderer4::VPLShadingPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vPositionInViewSpace;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 positionInViewSpace = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vPositionInViewSpace = positionInViewSpace.xyz / positionInViewSpace.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer4::VPLShadingPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

// The GBuffer:
uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;

// A buffer that contains the VPLs:
uniform samplerBuffer uVPLArray;
// A buffet that contains for each cluster an offset and a number of VPLs:
uniform isamplerBuffer uVPLOffsetsCounts;

in vec3 vPositionInViewSpace;

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vPositionInViewSpace;
//    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    vec3 Kd = vec3(1.f / 3.14f);
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    int visibilityCluster = texture(uVisibilityClusterSampler, texCoords).r;

    if(visibilityCluster < 0) {
        fFragColor = vec3(0, 0, 0);
        return;
    }

    ivec2 offsetCount = texelFetch(uVPLOffsetsCounts, visibilityCluster).xy;

    fFragColor = vec3(0, 0, 0);

    int end = offsetCount.x + offsetCount.y;
    //int end = textureSize(uVPLArray);

    for(int i = offsetCount.x; i < end; ++i) {
    //for(int i = 0; i < end; ++i) {
        vec3 lightPosition = texelFetch(uVPLArray, i * 3).xyz;
        vec3 lightNormal = texelFetch(uVPLArray, i * 3 + 1).xyz;
        vec3 lightIntensity = texelFetch(uVPLArray, i * 3 + 2).xyz;

        vec3 wi = lightPosition - fragPosition;
        float dist = length(wi);
        wi /= dist;

        float cos_o = max(0, dot(-wi, lightNormal));
        float cos_i = max(0, dot(wi, normalDepth.xyz));

        dist = max(dist, 0);

        float G = cos_i * cos_o / (dist * dist);

        vec3 r = reflect(-wi, normalDepth.xyz);

        vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
        fFragColor = fFragColor + G * lightIntensity/* * brdf*/;
    }
}

);

GLLightGateRenderer4::VPLShadingPassData::VPLShadingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uVPLArray(m_Program, "uVPLArray", true),
    m_uVPLOffsetsCounts(m_Program, "uVPLOffsetsCounts", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTERINDEX] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);
}



const GLchar* GLLightGateRenderer4::GateShadingPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vPositionInViewSpace;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 positionInViewSpace = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vPositionInViewSpace = positionInViewSpace.xyz / positionInViewSpace.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer4::GateShadingPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;

uniform samplerBuffer uGateArray;
uniform isamplerBuffer uGateOffsetsCounts;

uniform int uCurrentVisibilityCluster;

in vec3 vPositionInViewSpace;

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vPositionInViewSpace;
//    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    vec3 Kd = vec3(1.f / 3.14f);
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    int visibilityCluster = texture(uVisibilityClusterSampler, texCoords).r;

//    if(visibilityCluster == 5) {
//        fFragColor = vec3(1);
//        return;
//    }

    fFragColor = vec3(0);

    int clusterCount = textureSize(uGateOffsetsCounts);
    for(int j = 0; j < clusterCount; ++j) {
        //if(j != visibilityCluster) {
            ivec2 offsetCount = texelFetch(uGateOffsetsCounts, j).xy;
            int end = offsetCount.x + offsetCount.y;
            for(int i = offsetCount.x; i < end; ++i) {
                vec3 gatePosition = texelFetch(uGateArray, i * 3).xyz;
                vec3 gateNormal = texelFetch(uGateArray, i * 3 + 1).xyz;
                vec3 gateIntensity = texelFetch(uGateArray, i * 3 + 2).xyz;

                vec3 wi = gatePosition - fragPosition;
                float dist = length(wi);
                wi /= dist;

                float cos_o = max(0, dot(-wi, gateNormal));
                float cos_i = max(0, dot(wi, normalDepth.xyz));

                if(cos_o < 0) {
                    continue;
                }

                float G = cos_i * cos_o / (dist * dist);
                //float G = cos_i / (dist * dist);

                vec3 r = reflect(-wi, normalDepth.xyz);

                //vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
                fFragColor = fFragColor + G * gateIntensity;
            }
        //}
    }
}

);

GLLightGateRenderer4::GateShadingPassData::GateShadingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uGateArray(m_Program, "uGateArray", true),
    m_uGateOffsetsCounts(m_Program, "uGateOffsetsCounts", true),
    m_uCurrentVisibilityCluster(m_Program, "uCurrentVisibilityCluster", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTERINDEX] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);
}

const GLchar* GLLightGateRenderer4::DrawBuffersPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer4::DrawBuffersPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;
uniform sampler2D uNormalDepthSampler;
uniform isampler2D uVisibilityClusterSampler;
uniform isampler2D uDiscontinuitySampler;

uniform samplerBuffer uVClusterColors;

uniform int uDataToDisplay; // 0 for normals, 1 for depth, 2 for vclusters

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    if(uDataToDisplay == 0) {
        fFragColor = texture(uNormalDepthSampler, texCoords).rgb;
    } else if(uDataToDisplay == 1) {
        fFragColor = vec3(texture(uNormalDepthSampler, texCoords).a);
    } else if(uDataToDisplay == 2) {
        int visibilityCluster = texture(uVisibilityClusterSampler, texCoords).r;

        if(visibilityCluster < 0) {
            fFragColor = vec3(0, 0, 0);
        } else {
            fFragColor = texelFetch(uVClusterColors, visibilityCluster).rgb;
        }
        int discontinuity = texture(uDiscontinuitySampler, texCoords).r;
        if(discontinuity == 1) {
            fFragColor = vec3(0, 0, 0);
        }
    } else {
        int discontinuity = texture(uDiscontinuitySampler, texCoords).r;
        if(discontinuity == 1) {
            fFragColor = vec3(1, 1, 1);
        } else {
            fFragColor = vec3(0, 0, 0);
        }
    }
}

);

GLLightGateRenderer4::DrawBuffersPassData::DrawBuffersPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uNormalDepthSampler(m_Program, "uNormalDepthSampler", true),
    m_uVisibilityClusterSampler(m_Program, "uVisibilityClusterSampler", true),
    m_uDiscontinuitySampler(m_Program, "uDiscontinuitySampler", true),
    m_uDataToDisplay(m_Program, "uDataToDisplay", true),
    m_uVClusterColors(m_Program, "uVClusterColors", true) {
}

const GLchar* GLLightGateRenderer4::DiscontinuityPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer4::DiscontinuityPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;
uniform sampler2D uNormalDepthSampler;
uniform isampler2D uVisibilityClusterSampler;

uniform float uZFar;

out int fFragColor;

void main() {
    int x = int(gl_FragCoord.x);
    int y = int(gl_FragCoord.y);

    vec3 N = texelFetch(uNormalDepthSampler, ivec2(x, y), 0).rgb;
    vec3 Nr = texelFetch(uNormalDepthSampler, ivec2(x + 1, y), 0).rgb;
    vec3 Nb = texelFetch(uNormalDepthSampler, ivec2(x, y + 1), 0).rgb;
    vec3 Nrb = texelFetch(uNormalDepthSampler,ivec2(x + 1, y + 1), 0).rgb;

    float d1 = (dot(N, Nr) + dot(N, Nb) + dot(N, Nrb)) / 3.f;

    float d = texelFetch(uNormalDepthSampler, ivec2(x, y), 0).a;
    float dr = texelFetch(uNormalDepthSampler, ivec2(x + 1, y), 0).a;
    float db = texelFetch(uNormalDepthSampler, ivec2(x, y + 1), 0).a;
    float drb = texelFetch(uNormalDepthSampler,ivec2(x + 1, y + 1), 0).a;

    float meanDepth = (d + dr + db + drb) * 0.25f;
    float d2 = d / meanDepth;

    bool isGeometryDiscontinuous = d1 < 0.9 || d2 < 0.97 || d2 > 1.03;

    int C = texelFetch(uVisibilityClusterSampler, ivec2(x, y), 0).r;
    int Cr = texelFetch(uVisibilityClusterSampler, ivec2(x + 1, y), 0).r;
    int Cb = texelFetch(uVisibilityClusterSampler, ivec2(x, y + 1), 0).r;
    int Crb = texelFetch(uVisibilityClusterSampler, ivec2(x + 1, y + 1), 0).r;

    bool isClusteringDiscontinuous = C != Cr || C != Cb || C != Crb;

    fFragColor = int(isClusteringDiscontinuous && !isGeometryDiscontinuous);
}

);

GLLightGateRenderer4::DiscontinuityPassData::DiscontinuityPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uZFar(m_Program, "uZFar", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTERINDEX] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);
}


const GLchar* GLLightGateRenderer4::BlurPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer4::BlurPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;

uniform sampler2D uIrradianceBuffer;
uniform isampler2D uDiscontinuityBuffer;

out vec3 fFragColor;

void main() {
    int x = int(gl_FragCoord.x);
    int y = int(gl_FragCoord.y);

    int half = 1;

    bool isDiscontinuous = false;
    for(int i = -half; i <= half; ++i) {
        for(int j = -half; j <= half; ++j) {
            if(1 == texelFetch(uDiscontinuityBuffer, ivec2(x + i, y + j), 0).r) {
                isDiscontinuous = true;
            }
        }
    }

    int discontinuity = texelFetch(uDiscontinuityBuffer, ivec2(x, y), 0).r;
    vec3 irradiance = texelFetch(uIrradianceBuffer, ivec2(x, y), 0).rgb;

    if(isDiscontinuous) {
        for(int i = -half; i <= half; ++i) {
            for(int j = -half; j <= half; ++j) {
                if(i != 0 || j != 0) {
                    irradiance += texelFetch(uIrradianceBuffer, ivec2(x + i, y + j), 0).rgb;
                }
            }
        }
        int nbSamples = (half * 2 + 1);
        nbSamples *= nbSamples;
        irradiance /= nbSamples;
    }

    //vec3 Kd = texelFetch(uDiffuseSampler, ivec2(x, y), 0).rgb;
    vec3 Kd = vec3(1.f / 3.14f);
    fFragColor = Kd * irradiance;
}

);

//const GLchar* GLLightGateRenderer4::VClusterBufferCorrectionPassData::s_VertexShader =
//"#version 330\n"
//GLOOPS_STRINGIFY(

//layout(location = 0) in vec2 aPosition;

//void main() {
//    gl_Position = vec4(aPosition, 0, 1);
//}

//);

//const GLchar* GLLightGateRenderer4::VClusterBufferCorrectionPassData::s_FragmentShader =
//"#version 330\n"
//GLOOPS_STRINGIFY(

//uniform isampler2D uDiscontinuityBuffer;
//uniform isampler2D uVClusterBuffer;

//out int fNewVClusterIndex;

//void main() {
//    int x = int(gl_FragCoord.x);
//    int y = int(gl_FragCoord.y);

//    int clusters[7 * 7];
//    int counts[7 * 7];
//    int count = 0;

////    for(int i = 3; i <= 3; ++i) {
////        for(int j = 3; j <= 3; ++j) {
////            bool isDiscontinuous = (texelFetch(uDiscontinuityBuffer, ivec2(x + i, y + j)).x == 1);

////            int c = texelFetch(uVClusterBuffer)
////            for(int k = 0; k < count; ++k) {
////                if(clusters[k] ==)
////            }
////        }
////    }
//}

//);

//GLLightGateRenderer4::VClusterBufferCorrectionPassData::VClusterBufferCorrectionPassData():
//    m_Program(buildProgram(s_VertexShader,
//                           s_FragmentShader)),
//    m_uDiscontinuityBuffer(m_Program, "uDiscontinuityBuffer", true),
//    m_uVClusterBuffer(m_Program, "uVClusterBuffer", true) {
//}

GLLightGateRenderer4::BlurPassData::BlurPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uIrradianceBuffer(m_Program, "uIrradianceBuffer", true),
    m_uDiscontinuityBuffer(m_Program, "uDiscontinuityBuffer", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTERINDEX] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);
}

GLLightGateRenderer4::GLLightGateRenderer4():
    m_ProjectionMatrix(1.f), m_ViewMatrix(1.f),
    m_bDoVPLShadingPass(true), m_bDoGateShadingPass(true), m_nSamplePerGateCount(1),
    m_nSelectedVisibilityCluster(-1) {
}

void GLLightGateRenderer4::setResolution(size_t width, size_t height) {
    {
        GLTexFormat formats[GBUFFER_COUNT] = {
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32I, GL_RGBA_INTEGER, GL_INT }
        };
        m_GBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }

    {
        GLTexFormat formats[1] = {
            { GL_R8UI, GL_RED_INTEGER, GL_UNSIGNED_BYTE }
        };
//        GLTexFormat formats[1] = {
//            { GL_RGBA32F, GL_RGBA, GL_FLOAT }
//        };
        m_DiscontinuityBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }

    {
        m_IrradianceBuffer.init(width, height);
    }
}

void GLLightGateRenderer4::computeGeometryDiscontinuityBuffer() {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DiscontinuityPass.m_Program.use();
    m_DiscontinuityBuffer.bindForDrawing();

    m_DiscontinuityPass.m_uViewport.set(glm::vec4(0, 0,
                                                  m_DiscontinuityBuffer.getWidth(),
                                                  m_DiscontinuityBuffer.getHeight()));

    m_ScreenTriangle.render();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLLightGateRenderer4::geometryPass(const GLScene& scene) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);

    m_GeometryPassData.m_Program.use();

    m_GBuffer.bindForDrawing();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 MVPMatrix = m_ProjectionMatrix * m_ViewMatrix;
    glm::mat4 MVMatrix = m_ViewMatrix;
    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);
    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

    m_GeometryPassData.m_uMVPMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVPMatrix));
    m_GeometryPassData.m_uMVMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVMatrix));
    m_GeometryPassData.m_uNormalMatrix.setMatrix3(1, GL_FALSE, glm::value_ptr(NormalMatrix));
    m_GeometryPassData.m_uZFar.set(getZFar());
    m_GeometryPassData.m_uCameraPosition.set(rcpViewMatrix[3].x, rcpViewMatrix[3].y, rcpViewMatrix[3].z);

    GLMaterialManager::TextureUnits units(MAT_DIFFUSE_TEXUNIT, MAT_GLOSSY_TEXUNIT, MAT_SHININESS_TEXUNIT);

    scene.render(m_GeometryPassData.m_MaterialUniforms, units);

    computeGeometryDiscontinuityBuffer();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLLightGateRenderer4::sendVisibilityClusteringGrid(const Scene& scene,
                                                       const VisibilityClustering& vclustering) {
    const CurvSkelClustering& clustering = scene.voxelSpace->getClustering();
    const Grid3D<GraphNodeIndex>& grid = clustering.getGrid();

    glActiveTexture(GL_TEXTURE0 + TEX3D_GRID_TEXUNIT);
    glBindTexture(GL_TEXTURE_3D, m_CurvSkelClusteringTextures.m_Grid.glId());
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32UI, grid.width(), grid.height(), grid.depth(), 0,
                 GL_RED_INTEGER, GL_UNSIGNED_INT, grid.data());

    {
        std::vector<glm::vec4> nodeData;
        nodeData.reserve(clustering.size());
        for(auto nodeIdx: range(clustering.size())) {
            const auto& node = clustering.getNode(nodeIdx);
            nodeData.emplace_back(glm::vec4(node.P.x, node.P.y, node.P.z, node.maxball));
        }
        m_CurvSkelClusteringTextures.m_NodeBuffer.bind(TEXBUFFER_NODEBUFFER_TEXUNIT);
        m_CurvSkelClusteringTextures.m_NodeBuffer.setData(nodeData.size(), nodeData.data(), GL_RGBA32F);
    }

    {
        std::vector<int32_t> neighbourLists;
        std::vector<glm::ivec2> neighbourOffsetsCounts;
        neighbourOffsetsCounts.reserve(clustering.size());
        std::vector<int32_t> visibilityClusters;
        visibilityClusters.reserve(clustering.size());

        int offset = 0;
        for(auto nodeIdx: range(clustering.size())) {
            visibilityClusters.emplace_back(vclustering.getClusterOf(nodeIdx));
            const auto& neighbours = clustering.neighbours(nodeIdx);
            for(auto neighbourIdx: neighbours) {
                neighbourLists.emplace_back(neighbourIdx);
            }
            auto count = neighbours.size();
            neighbourOffsetsCounts.emplace_back(glm::ivec2(offset, count));
            offset += count;
        }

        m_CurvSkelClusteringTextures.m_NeighbourBuffer.bind(TEXBUFFER_NEIGHBOURBUFFER_TEXUNIT);
        m_CurvSkelClusteringTextures.m_NeighbourBuffer.setData(
                    neighbourLists.size(), neighbourLists.data(), GL_R32I);

        m_CurvSkelClusteringTextures.m_NeighbourOffsetCountBuffer.bind(TEXBUFFER_NEIGHBOUROFFSETCOUNTBUFFER_TEXUNIT);
        m_CurvSkelClusteringTextures.m_NeighbourOffsetCountBuffer.setData(
                    neighbourOffsetsCounts.size(), neighbourOffsetsCounts.data(), GL_RG32I);

        m_CurvSkelClusteringTextures.m_VisibilityClusterBuffer.bind(TEXBUFFER_VISIBILITYCLUSTERBUFFER_TEXUNIT);
        m_CurvSkelClusteringTextures.m_VisibilityClusterBuffer.setData(
                    visibilityClusters.size(), visibilityClusters.data(), GL_R32I);
    }

    Grid3D<int> clusterGrid(grid.width(), grid.height(), grid.depth());
    size_t totalSize = grid.width() * grid.height() * grid.depth();

    for(uint32_t i = 0; i < totalSize; ++i) {
        GraphNodeIndex node = grid.data()[i];
        if(UNDEFINED_NODE == node) {
            clusterGrid[i] = -1;
        } else {
            clusterGrid[i] = vclustering.getClusterOf(node);
        }
    }

    glActiveTexture(GL_TEXTURE0 + TEX3D_VCLUSTERGRID_TEXUNIT);
    glBindTexture(GL_TEXTURE_3D, m_VClusterGridTexture.glId());
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32I, grid.width(), grid.height(), grid.depth(), 0,
                 GL_RED_INTEGER, GL_INT, clusterGrid.data());

    glActiveTexture(GL_TEXTURE0);

    m_GeometryPassData.m_Program.use();
    m_GeometryPassData.m_uVClusterGrid.set(TEX3D_VCLUSTERGRID_TEXUNIT);
    m_GeometryPassData.m_uWorldToGrid.set(convert(scene.voxelSpace->getWorldToLocalTransform()));
    m_GeometryPassData.m_uVoxelSize.set(scene.voxelSpace->getVoxelSize());

    m_GeometryPassData.m_uNodeBuffer.set(TEXBUFFER_NODEBUFFER_TEXUNIT);
    m_GeometryPassData.m_uNeighbourBuffer.set(TEXBUFFER_NEIGHBOURBUFFER_TEXUNIT);
    m_GeometryPassData.m_uNeighbourOffsetCountBuffer.set(TEXBUFFER_NEIGHBOUROFFSETCOUNTBUFFER_TEXUNIT);
    m_GeometryPassData.m_uGrid.set(TEX3D_GRID_TEXUNIT);
    m_GeometryPassData.m_uVisibilityClusterBuffer.set(TEXBUFFER_VISIBILITYCLUSTERBUFFER_TEXUNIT);
}

void GLLightGateRenderer4::setup() {
    glActiveTexture(GL_TEXTURE0 + GBUFFER_NORMALDEPTH_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).glId());

    glActiveTexture(GL_TEXTURE0 + GBUFFER_DIFFUSE_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_DIFFUSE).glId());

    glActiveTexture(GL_TEXTURE0 + GBUFFER_GLOSSYSHININESS_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).glId());

    glActiveTexture(GL_TEXTURE0 + GBUFFER_VCLUSTER_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_VCLUSTERINDEX).glId());

    glActiveTexture(GL_TEXTURE0 + TEX3D_VCLUSTERGRID_TEXUNIT);
    glBindTexture(GL_TEXTURE_3D, m_VClusterGridTexture.glId());

    glActiveTexture(GL_TEXTURE0 + TEX2D_DISCONTINUITYBUFFER_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_DiscontinuityBuffer.getColorBuffer(0).glId());

    glActiveTexture(GL_TEXTURE0 + TEX2D_IRRADIANCEBUFFER_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_IrradianceBuffer.getColorBuffer(0).glId());

    GLOOPS_CHECK_ERROR;

    m_VPLShadingPassData.m_Program.use();
    m_VPLShadingPassData.m_uSamplers[GBUFFER_NORMAL_DEPTH].set(GBUFFER_NORMALDEPTH_TEXUNIT);
    m_VPLShadingPassData.m_uSamplers[GBUFFER_DIFFUSE].set(GBUFFER_DIFFUSE_TEXUNIT);
    m_VPLShadingPassData.m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(GBUFFER_GLOSSYSHININESS_TEXUNIT);
    m_VPLShadingPassData.m_uSamplers[GBUFFER_VCLUSTERINDEX].set(GBUFFER_VCLUSTER_TEXUNIT);
    m_VPLShadingPassData.m_uVPLArray.set(TEXBUFFER_VPLARRAY_TEXUNIT);
    m_VPLShadingPassData.m_uVPLOffsetsCounts.set(TEXBUFFER_VPLOFFSETSCOUNTS_TEXUNIT);

    m_GateShadingPassData.m_Program.use();
    m_GateShadingPassData.m_uSamplers[GBUFFER_NORMAL_DEPTH].set(GBUFFER_NORMALDEPTH_TEXUNIT);
    m_GateShadingPassData.m_uSamplers[GBUFFER_DIFFUSE].set(GBUFFER_DIFFUSE_TEXUNIT);
    m_GateShadingPassData.m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(GBUFFER_GLOSSYSHININESS_TEXUNIT);
    m_GateShadingPassData.m_uSamplers[GBUFFER_VCLUSTERINDEX].set(GBUFFER_VCLUSTER_TEXUNIT);
    m_GateShadingPassData.m_uGateArray.set(TEXBUFFER_GATEARRAY_TEXUNIT);
    m_GateShadingPassData.m_uGateOffsetsCounts.set(TEXBUFFER_GATEOFFSETSCOUNTS_TEXUNIT);

    m_DrawBuffersPassData.m_Program.use();
    m_DrawBuffersPassData.m_uNormalDepthSampler.set(GBUFFER_NORMALDEPTH_TEXUNIT);
    m_DrawBuffersPassData.m_uVisibilityClusterSampler.set(GBUFFER_VCLUSTER_TEXUNIT);
    m_DrawBuffersPassData.m_uDiscontinuitySampler.set(TEX2D_DISCONTINUITYBUFFER_TEXUNIT);
    m_DrawBuffersPassData.m_uVClusterColors.set(TEXBUFFER_VCLUSTERCOLORS_TEXUNIT);

    m_DiscontinuityPass.m_Program.use();
    m_DiscontinuityPass.m_uSamplers[GBUFFER_NORMAL_DEPTH].set(GBUFFER_NORMALDEPTH_TEXUNIT);
    m_DiscontinuityPass.m_uSamplers[GBUFFER_DIFFUSE].set(GBUFFER_DIFFUSE_TEXUNIT);
    m_DiscontinuityPass.m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(GBUFFER_GLOSSYSHININESS_TEXUNIT);
    m_DiscontinuityPass.m_uSamplers[GBUFFER_VCLUSTERINDEX].set(GBUFFER_VCLUSTER_TEXUNIT);
    m_DiscontinuityPass.m_uZFar.set(getZFar());

    m_BlurPassData.m_Program.use();
    m_BlurPassData.m_uSamplers[GBUFFER_NORMAL_DEPTH].set(GBUFFER_NORMALDEPTH_TEXUNIT);
    m_BlurPassData.m_uSamplers[GBUFFER_DIFFUSE].set(GBUFFER_DIFFUSE_TEXUNIT);
    m_BlurPassData.m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(GBUFFER_GLOSSYSHININESS_TEXUNIT);
    m_BlurPassData.m_uSamplers[GBUFFER_VCLUSTERINDEX].set(GBUFFER_VCLUSTER_TEXUNIT);
    m_BlurPassData.m_uDiscontinuityBuffer.set(TEX2D_DISCONTINUITYBUFFER_TEXUNIT);
    m_BlurPassData.m_uIrradianceBuffer.set(TEX2D_IRRADIANCEBUFFER_TEXUNIT);

    glActiveTexture(GL_TEXTURE0);
}

struct GateSample {
    Vec3f P;
    Vec3f N;
    Col3f E; // Irradiance affecting the sample
};

void GLLightGateRenderer4::sendVPLs(const VPLContainer& vpls, const Scene& scene,
                                   const VisibilityClustering& vclustering) {
    // Reset gate power array
    m_GatePower.resize(vclustering.size());
    for(const auto& cluster: index(vclustering)) {
        m_GatePower[cluster.first].resize(cluster.second.getNeighbourLinks().size(), Col3f(0.f));
    }

    // Compute the VPLs contained in each cluster
    m_ClusterVPLs.clear();
    m_ClusterVPLs.resize(vclustering.size());

    // Regroupe les VPLs par cluster de visibilité
    for(const auto& vpl: index(vpls.orientedVPLs)) {
        GraphNodeIndex idx = scene.voxelSpace->getClustering().getNearestNode(vpl.second.P, vpl.second.N);
        if(idx == UNDEFINED_NODE) {
            continue;
        }
        int clusterIdx = vclustering.getClusterOf(idx);
        if(clusterIdx < 0) {
            continue;
        }
        m_ClusterVPLs[clusterIdx].push_back(vpl.first);
    }

    // On stocke les VPLs dans un tableau, regroupés par cluster pour un traitement
    // plus facile par le GPU
    VPLContainer sorted;
    std::vector<glm::ivec2> vplOffsetsCounts;

    int offset = 0;
    for(uint32_t vcluster: range(vclustering.size())) {
        glm::ivec2 offsetCount(offset, 0);

        // On ajoute les VPLs du cluster
        offsetCount.y += m_ClusterVPLs[vcluster].size();
        for(uint32_t vplIdx: m_ClusterVPLs[vcluster]) {
            sorted.addVPL(vpls.orientedVPLs[vplIdx]);
        }

        if(vclustering[vcluster].isTransfertCluster()) {
            for(const auto& neighbourLink: vclustering[vcluster].getNeighbourLinks()) {
                offsetCount.y += m_ClusterVPLs[neighbourLink.neighbour].size();
                for(uint32_t vplIdx: m_ClusterVPLs[neighbourLink.neighbour]) {
                    sorted.addVPL(vpls.orientedVPLs[vplIdx]);
                }
            }
        } else {
            for(const auto& neighbourLink: vclustering[vcluster].getNeighbourLinks()) {
                if(vclustering[neighbourLink.neighbour].isTransfertCluster()) {
                    offsetCount.y += m_ClusterVPLs[neighbourLink.neighbour].size();
                    for(uint32_t vplIdx: m_ClusterVPLs[neighbourLink.neighbour]) {
                        sorted.addVPL(vpls.orientedVPLs[vplIdx]);
                    }
                }
            }
        }

        vplOffsetsCounts.emplace_back(offsetCount);
        offset += offsetCount.y;
    }

    // On Transforme les coordonnées des VPLS pour les avoir dans l'espace
    // caméra dans le shader
    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));
    std::vector<float> values(3 * 3 * sorted.orientedVPLs.size());
    for(const auto& vplIndex: index(sorted.orientedVPLs)) {
        const OrientedVPL& vpl = vplIndex.second;

        uint32_t offset = vplIndex.first * 9;

        glm::vec4 lightPosition = m_ViewMatrix * glm::vec4(convert(vpl.P), 1);
        glm::vec3 lightNormal = NormalMatrix * convert(vpl.N);

        values[offset + 0] = lightPosition.x;
        values[offset + 1] = lightPosition.y;
        values[offset + 2] = lightPosition.z;
        values[offset + 3] = lightNormal.x;
        values[offset + 4] = lightNormal.y;
        values[offset + 5] = lightNormal.z;
        values[offset + 6] = vpl.L.r;
        values[offset + 7] = vpl.L.g;
        values[offset + 8] = vpl.L.b;
    }

    // On envoit les VPLs au GPU
    m_VPLBuffer.bind(TEXBUFFER_VPLARRAY_TEXUNIT);
    m_VPLBuffer.setData(values.size(), values.data(), GL_RGB32F);

    m_VPLOffsetsCountsBuffer.bind(TEXBUFFER_VPLOFFSETSCOUNTS_TEXUNIT);
    m_VPLOffsetsCountsBuffer.setData(vplOffsetsCounts.size(), vplOffsetsCounts.data(), GL_RG32I);
}

void GLLightGateRenderer4::sendGates(const VPLContainer& vpls, const Scene& scene,
                                    const VisibilityClustering& vclustering) {
    Random rng(0);
    std::vector<GateSample> gateSamples;
    std::vector<glm::ivec2> gateOffsetsCounts;
    size_t Nsample = m_nSamplePerGateCount;

    int offset = 0;
    for(uint32_t j = 0; j < vclustering.size(); ++j) {
        for(const auto& neighbourLink:
            index(vclustering[j].getNeighbourLinks())) {
            auto gate = vclustering.getGate(neighbourLink.second);
            float gateArea = float(embree::pi) * gate.radius * gate.radius;

            for(uint32_t i: range(Nsample)) {
                /*Vec2f gateSample = embree::uniformSampleDisk(Vec2f(rng.getFloat(), rng.getFloat()),
                                                             gate.radius);*/
                Vec2f gateSample(0.f, 0.f);
                AffineSpace3f frame = coordinateSystem(gate.center, gate.N);

                GateSample sample;

                sample.P = embree::xfmPoint(frame, Vec3f(gateSample.x, 0, gateSample.y));
                sample.N = -gate.N;
                sample.E = Col3f(0.f);

                // Estimation de l'intensité incidente sur le VPLs depuis ce cluster
                for(uint32_t vplIdx: m_ClusterVPLs[j]) {
                    const OrientedVPL& vpl = vpls.orientedVPLs[vplIdx];
                    Vec3f wi = sample.P - vpl.P;
                    float dot_i = embree::dot(wi, vpl.N);
                    if(dot_i < 0.f) {
                        continue;
                    }
                    float distSquared = lengthSquared(wi);
                    float dot_o = embree::abs(embree::dot(-wi, gate.N));

                    float G = dot_i * dot_o / (distSquared * distSquared);

                    sample.E += vpl.L * G;
                }

                // Monte carlo estimation
                sample.E = gateArea * sample.E / Nsample;

                // Add the sample for the neighbour
                gateSamples.emplace_back(sample);
            }
        }

        int nb = Nsample * vclustering[j].getNeighbourLinks().size();
        gateOffsetsCounts.emplace_back(glm::ivec2(offset, nb));
        offset += nb;
    }

    std::vector<float> gateSamplesData;

    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

    for(const auto& gateSample: index(gateSamples)) {
        const GateSample& sample = gateSample.second;

        glm::vec4 samplePosition = m_ViewMatrix * glm::vec4(convert(sample.P), 1);
        glm::vec3 sampleNormal = NormalMatrix * convert(sample.N);

        gateSamplesData.push_back(samplePosition.x);
        gateSamplesData.push_back(samplePosition.y);
        gateSamplesData.push_back(samplePosition.z);
        gateSamplesData.push_back(sampleNormal.x);
        gateSamplesData.push_back(sampleNormal.y);
        gateSamplesData.push_back(sampleNormal.z);
        gateSamplesData.push_back(sample.E.r);
        gateSamplesData.push_back(sample.E.g);
        gateSamplesData.push_back(sample.E.b);
    }

    m_GateBuffer.bind(TEXBUFFER_GATEARRAY_TEXUNIT);
    m_GateBuffer.setData(gateSamplesData.size(),
                         gateSamplesData.data(), GL_RGB32F);

    m_GateOffsetsCountsBuffer.bind(TEXBUFFER_GATEOFFSETSCOUNTS_TEXUNIT);
    m_GateOffsetsCountsBuffer.setData(gateOffsetsCounts.size(),
                                      gateOffsetsCounts.data(), GL_RG32I);
}

void GLLightGateRenderer4::shadingPass(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls, const Scene& scene, const VisibilityClustering& vclustering) {

    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::CapabilityRestorer blending(GL_BLEND, true);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLint bindedFramebuffer;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &bindedFramebuffer);

    m_IrradianceBuffer.bindForDrawing();

    glViewport(x, y, width, height);

    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);

    glClear(GL_COLOR_BUFFER_BIT);

    // VPL Shading pass

    sendVPLs(vpls, scene, vclustering);

    if(m_bDoVPLShadingPass) {
        m_VPLShadingPassData.m_Program.use();
        m_VPLShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
        m_VPLShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);

        m_ScreenTriangle.render();
    }

    // Gate Shading pass

    if(m_bDoGateShadingPass) {
        sendGates(vpls, scene, vclustering);

        m_GateShadingPassData.m_Program.use();
        m_GateShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
        m_GateShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);

        m_GateShadingPassData.m_uCurrentVisibilityCluster.set(m_nSelectedVisibilityCluster);

        m_ScreenTriangle.render();
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bindedFramebuffer);

    // Final pass

    glClear(GL_COLOR_BUFFER_BIT);

    m_BlurPassData.m_Program.use();

    m_ScreenTriangle.render();
}

void GLLightGateRenderer4::drawDiscontinuityBuffer(int x, int y, size_t width, size_t height) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(3);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);

    m_ScreenTriangle.render();
}

void GLLightGateRenderer4::drawBuffers(int x, int y, size_t width, size_t height,
                                      const VisibilityClustering& vclustering) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    // Draw normals
    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);
    m_DrawBuffersPassData.m_uDataToDisplay.set(0);

    m_ScreenTriangle.render();

    // Draw depth
    glViewport(x + width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(1);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw VCluster
    glViewport(x + 2 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(2);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY, fW, fH);

    ColorMap colors = buildRandomColorMap(vclustering.size());

    GLTextureBuffer colorBuffer;
    colorBuffer.bind(TEXBUFFER_VCLUSTERCOLORS_TEXUNIT);
    colorBuffer.setData(colors.size(), colors.data(), getEmbreeColorFormat());

    m_ScreenTriangle.render();

    // Draw discontinuity buffer
    glViewport(x + 3 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(3);
    m_DrawBuffersPassData.m_uViewport.set(fX + 3 * fW, fY, fW, fH);

    m_ScreenTriangle.render();
}

}
