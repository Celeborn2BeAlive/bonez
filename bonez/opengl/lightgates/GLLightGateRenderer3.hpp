#ifndef _BONEZ_GLLIGHTGATERENDERER3_HPP_
#define _BONEZ_GLLIGHTGATERENDERER3_HPP_

#include "bonez/scene/Scene.hpp"
#include "opengl/shaders/GLSLProgramBuilder.hpp"
#include "opengl/scene/GLScene.hpp"

#include "bonez/scene/lights/vpls.hpp"
#include "bonez/scene/topology/VisibilityClustering.hpp"

#include <vector>
#include <glm/glm.hpp>

#include "bonez/opengl/utils/GLFramebuffer.hpp"
#include "bonez/opengl/utils/GLTextureBuffer.hpp"

namespace BnZ {

class GLLightGateRenderer3 {
public:
    GLLightGateRenderer3();

    void setResolution(size_t width, size_t height);

    void setProjectionMatrix(const glm::mat4& projectionMatrix) {
        m_ProjectionMatrix = projectionMatrix;
    }

    void setViewMatrix(const glm::mat4& viewMatrix) {
        m_ViewMatrix = viewMatrix;
    }

    void setup();

    void sendVisibilityClusteringGrid(const Scene& scene, const VisibilityClustering& vclustering);

    void geometryPass(const GLScene& scene);

    void shadingPass(int x, int y, size_t width, size_t height,
        const VPLContainer& vpls, const Scene& scene, const VisibilityClustering& vclustering);

    void drawBuffers(int x, int y, size_t width, size_t height, const VisibilityClustering& vclustering);

    void drawDiscontinuityBuffer(int x, int y, size_t width, size_t height);

    bool m_bDoVPLShadingPass;
    bool m_bDoGateShadingPass;
    size_t m_nSamplePerGateCount;

private:
    void sendVPLs(const VPLContainer& vpls, const Scene& scene, const VisibilityClustering& vclustering);

    void sendGates(const VPLContainer& vpls, const Scene& scene,
                                        const VisibilityClustering& vclustering);

    glm::mat4 m_ProjectionMatrix;
    glm::mat4 m_ViewMatrix;

    enum GBufferTextureType {
        GBUFFER_NORMAL_DEPTH,
        GBUFFER_DIFFUSE,
        GBUFFER_GLOSSY_SHININESS,
        GBUFFER_VCLUSTERINDEX,
        GBUFFER_COUNT
    };
    GLFramebuffer<GBUFFER_COUNT> m_GBuffer;

    enum TextureUnits {
        MAT_DIFFUSE_TEXUNIT,
        MAT_GLOSSY_TEXUNIT,
        MAT_SHININESS_TEXUNIT,
        GBUFFER_NORMALDEPTH_TEXUNIT,
        GBUFFER_DIFFUSE_TEXUNIT,
        GBUFFER_GLOSSYSHININESS_TEXUNIT,
        GBUFFER_VCLUSTER_TEXUNIT,
        TEXBUFFER_VPLARRAY_TEXUNIT,
        TEXBUFFER_VPLOFFSETSCOUNTS_TEXUNIT,
        TEXBUFFER_GATEARRAY_TEXUNIT,
        TEXBUFFER_GATEOFFSETSCOUNTS_TEXUNIT,
        TEXBUFFER_VCLUSTERCOLORS_TEXUNIT,
        TEX3D_VCLUSTERGRID_TEXUNIT,
        TEX2D_DISCONTINUITYBUFFER_TEXUNIT,
        TEX2D_IRRADIANCEBUFFER_TEXUNIT
    };

    struct GeometryPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        GLMaterialUniforms m_MaterialUniforms;
        gloops::Uniform m_uMVPMatrix;
        gloops::Uniform m_uMVMatrix;
        gloops::Uniform m_uNormalMatrix;
        gloops::Uniform m_uZFar;
        gloops::Uniform m_uVClusterGrid;
        gloops::Uniform m_uWorldToGrid;
        gloops::Uniform m_uVoxelSize;
        gloops::Uniform m_uCameraPosition;

        GeometryPassData();
    };

    GeometryPassData m_GeometryPassData;

    struct ShadingPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uInvProjMatrix;

        gloops::Uniform m_uSamplers[GBUFFER_COUNT];

        gloops::Uniform m_uVPLArray;
        gloops::Uniform m_uVPLOffsetsCounts;

        gloops::Uniform m_uGateArray;
        gloops::Uniform m_uGateOffsetsCounts;

        ShadingPassData();
    };

    ShadingPassData m_ShadingPassData;

    GLFramebuffer<1> m_IrradianceBuffer;

    struct DrawBuffersPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uNormalDepthSampler;
        gloops::Uniform m_uVisibilityClusterSampler;
        gloops::Uniform m_uDiscontinuitySampler;
        gloops::Uniform m_uDataToDisplay;
        gloops::Uniform m_uVClusterColors;

        DrawBuffersPassData();
    };

    DrawBuffersPassData m_DrawBuffersPassData;

    struct DiscontinuityPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uSamplers[GBUFFER_COUNT];
        gloops::Uniform m_uViewport;
        gloops::Uniform m_uZFar;

        DiscontinuityPassData();
    };

    DiscontinuityPassData m_DiscontinuityPass;

    GLFramebuffer<1> m_DiscontinuityBuffer;
    void computeGeometryDiscontinuityBuffer();

    struct BlurPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uSamplers[GBUFFER_COUNT];
        gloops::Uniform m_uIrradianceBuffer;
        gloops::Uniform m_uDiscontinuityBuffer;

        BlurPassData();
    };

    BlurPassData m_BlurPassData;

    std::vector<std::vector<uint32_t>> m_ClusterVPLs;

    GLTextureBuffer m_VPLBuffer;
    GLTextureBuffer m_VPLOffsetsCountsBuffer;

    GLTextureBuffer m_GateBuffer;
    GLTextureBuffer m_GateOffsetsCountsBuffer;

    gloops::TextureObject m_VClusterTexture;
    gloops::TextureObject m_VClusterGridTexture;

    ScreenTriangle m_ScreenTriangle;
};

}

#endif
