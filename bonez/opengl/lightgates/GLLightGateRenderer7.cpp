#include "GLLightGateRenderer7.hpp"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "bonez/opengl/utils/embree_opengl.hpp"

#include "bonez/common/data/ColorMap.hpp"
#include "bonez/common/Progress.hpp"

namespace BnZ {

using namespace gloops;

const GLchar* GLLightGateRenderer7::GeometryPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;

uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat3 uNormalMatrix;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoords;
out vec3 vWorldSpaceNormal;
out vec3 vWorldSpacePosition;

void main() {
    vWorldSpacePosition = aPosition;
    vWorldSpaceNormal = aNormal;
    vPosition = vec3(uMVMatrix * vec4(aPosition, 1));
    vNormal = uNormalMatrix * aNormal;
    vTexCoords = aTexCoords;

    gl_Position = uMVPMatrix * vec4(aPosition, 1);
}

);

const GLchar* GLLightGateRenderer7::GeometryPassData::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoords;
in vec3 vWorldSpacePosition;
in vec3 vWorldSpaceNormal;

struct Material {
    vec3 Kd;
    vec3 Ks;
    float shininess;
    sampler2D KdSampler;
    sampler2D KsSampler;
    sampler2D shininessSampler;
};

uniform Material uMaterial;

uniform float uSy;
uniform float uHalfFOVY;
uniform float uZNear;
uniform float uZFar;

struct VoxelSpace {
    // The length of an edge of a voxel in world space
    float voxelSize;
    // The transformation matrix that gives for a world space position
    // the associated voxel
    mat4 worldToGrid;
};

layout(std430, binding = 0) buffer NodeBuffer {
    vec4 bNodeBuffer[];
};

layout(std430, binding = 1) buffer NeighbourOffsetCountBuffer {
    ivec2 bNeighbourOffsetCountBuffer[];
};

layout(std430, binding = 2) buffer NeighbourBuffer {
    int bNeighbourBuffer[];
};

layout(std430, binding = 3) buffer VisibilityClusterBuffer {
    int bVisibilityClusterBuffer[];
};

uniform usampler3D uNodeGrid;

ivec3 gridSize;

uniform VoxelSpace uVoxelSpace;

layout(location = 0) out vec4 fNormalDepth;
layout(location = 1) out vec3 fDiffuse;
layout(location = 2) out vec4 fGlossyShininess;
layout(location = 3) out ivec4 fVClusterIndices;
layout(location = 4) out vec3 fVClusterCoherencies;
layout(location = 5) out uint fGeometryCluster;


const float PI = 3.14159265358979323846264;

float computeCoherency(unsigned int cluster, vec3 P, vec3 N, float dskel) {
    vec4 node = bNodeBuffer[cluster];
    vec3 wi = node.xyz - P;
    float d = length(wi);
    wi /= d;

    float orientationFactor = dot(wi, N);
    if(orientationFactor > 0 && orientationFactor < 0.7f) {
        orientationFactor *= orientationFactor;
    }

    if(orientationFactor < 0) {
        return 0.f;
    }

    return /*orientationFactor * */node.w / (d * d);
}

float computeCoherency2(unsigned int cluster, vec3 P, vec3 N, float dskel) {
    vec4 node = bNodeBuffer[cluster];
    vec3 wi = node.xyz - P;
    float d = length(wi);
    wi /= d;

    float orientationFactor = dot(wi, N);

    return orientationFactor * node.w / (d * d);
}

uint getNearestCluster(vec3 P, vec3 N) {
    vec3 lookAtPoint = P + N * uVoxelSpace.voxelSize;
    unsigned int cluster = unsigned int(-1);

    while(cluster == unsigned int(-1)) {
        vec4 gridSpacePosition = uVoxelSpace.worldToGrid * vec4(lookAtPoint, 1);
        ivec3 voxel = ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z);
        if(voxel.x < 0 || voxel.x >= gridSize.x || voxel.y < 0 || voxel.y >= gridSize.y ||
                voxel.z < 0 || voxel.z >= gridSize.z) {
            return unsigned int(-1);
        }

        cluster = texelFetch(uNodeGrid, voxel, 0).x;
        lookAtPoint += N * uVoxelSpace.voxelSize;
    }

    vec3 clustP = bNodeBuffer[cluster].xyz;
    float dskel = length(P - clustP);

    float coherency = computeCoherency(cluster, P, N, dskel);
    ivec2 neighbourOffsetCount = bNeighbourOffsetCountBuffer[cluster];

    int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

    for(int i = neighbourOffsetCount.x; i < end; ++i) {
        int neighbour = bNeighbourBuffer[i];
        float candidateCoherency = computeCoherency(unsigned int(neighbour), P, N, dskel + length(clustP - bNodeBuffer[neighbour].xyz));
        if(candidateCoherency > coherency) {
            coherency = candidateCoherency;
            cluster = unsigned int(neighbour);
        }
    }

    return cluster;
}

ivec4 computeVClusters(vec3 P, vec3 N) {
    uint cluster = getNearestCluster(P, N);
    if(cluster == unsigned int(-1)) {
        return ivec4(-1);
    }

    return ivec4(bVisibilityClusterBuffer[cluster], -1, -1, -1);
}

uint getNearestClusterWrtToGrid(vec3 P, vec3 N) {
    vec3 lookAtPoint = P + N * uVoxelSpace.voxelSize;
    uint cluster = uint(-1);

    while(cluster == uint(-1)) {
        vec4 gridSpacePosition = uVoxelSpace.worldToGrid * vec4(lookAtPoint, 1);
        ivec3 voxel = ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z);
        if(voxel.x < 0 || voxel.x >= gridSize.x || voxel.y < 0 || voxel.y >= gridSize.y ||
                voxel.z < 0 || voxel.z >= gridSize.z) {
            return uint(-1);
        }

        cluster = texelFetch(uNodeGrid, voxel, 0).x;
        lookAtPoint += N * uVoxelSpace.voxelSize;
    }

    return cluster;
}

uniform int uSelectedVCluster;

ivec4 computeMultiVClusters(vec3 P, vec3 N, out vec2 coherencies) {
    uint cluster = getNearestClusterWrtToGrid(P, N);
    if(cluster == uint(-1)) {
        return ivec4(-1);
    }

    int vc = bVisibilityClusterBuffer[cluster];

    vec3 clustP = bNodeBuffer[cluster].xyz;
    float dskel = length(P - clustP);

    float myCoherency = computeCoherency(uint(cluster), P, N, dskel);

    uint bestNeighbour;
    float neighbourCoherency = 0;
    ivec2 neighbourOffsetCount = bNeighbourOffsetCountBuffer[cluster];

    int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

    for(int i = neighbourOffsetCount.x; i < end; ++i) {
        int neighbour = bNeighbourBuffer[i];
        if(bVisibilityClusterBuffer[neighbour] != vc) {
            float candidateCoherency = computeCoherency(uint(neighbour), P, N, dskel + length(clustP - bNodeBuffer[neighbour].xyz));
            if(candidateCoherency > neighbourCoherency) {
                neighbourCoherency = candidateCoherency;
                bestNeighbour = uint(neighbour);
            }
        }
    }

    if(neighbourCoherency > myCoherency) {
        uint tmp = bestNeighbour;
        bestNeighbour = cluster;
        cluster = tmp;

        float tmp2 = neighbourCoherency;
        neighbourCoherency = myCoherency;
        myCoherency = tmp2;
    }

    float disimilarity = abs(1 - neighbourCoherency / myCoherency);

    ivec4 vclusters;
//    if(disimilarity < 0.4) {
//        vclusters = ivec4(bVisibilityClusterBuffer[cluster], bVisibilityClusterBuffer[bestNeighbour], -1, -1);
//        coherencies = vec2(myCoherency, neighbourCoherency);
//    } else {
        vclusters = ivec4(bVisibilityClusterBuffer[cluster], -1, -1, -1);
        coherencies = vec2(myCoherency, 0);
//    }

    if(vclusters.x == vclusters.y) {
        coherencies = vec2(myCoherency, 0);
        vclusters.y = -1;
    }

    return vclusters;
}

float computeVCluster5Coherency(vec3 P, vec3 N) {
    vec3 lookAtPoint = P + N * uVoxelSpace.voxelSize;

    vec4 gridSpacePosition = uVoxelSpace.worldToGrid * vec4(lookAtPoint, 1);
    vec3 gridSpaceNormal = normalize((uVoxelSpace.worldToGrid * vec4(N, 0)).xyz);
    ivec3 voxel = ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z);

    float sum = 0;
    float sumTotal = 0;

    int windowHalfSize = 1;

    for(int k = -windowHalfSize; k <= windowHalfSize; ++k) {
        for(int j = -windowHalfSize; j <= windowHalfSize; ++j) {
            for(int i = -windowHalfSize; i <= windowHalfSize; ++i) {
                if(dot(gridSpaceNormal, vec3(i, j, k)) > 0) {
                    ivec3 v = voxel + ivec3(i, j, k);
                    bool isOutside = v.x < 0 || v.x >= gridSize.x || v.y < 0 || v.y >= gridSize.y ||
                                        v.z < 0 || v.z >= gridSize.z;
                    if(!isOutside) {
                        uint cluster = texelFetch(uNodeGrid, v, 0).x;

                        vec3 clustP = bNodeBuffer[cluster].xyz;
                        float dskel = length(P - clustP);

                        float myCoherency = max(0, computeCoherency2(uint(cluster), P, N, dskel));

                        if(cluster != uint(-1) && bVisibilityClusterBuffer[cluster] == uSelectedVCluster) {
                            sum += myCoherency;
                        }

                        ivec2 neighbourOffsetCount = bNeighbourOffsetCountBuffer[cluster];

                        int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

                        for(int i = neighbourOffsetCount.x; i < end; ++i) {
                            int neighbour = bNeighbourBuffer[i];
                            float candidateCoherency = max(0, computeCoherency2(uint(neighbour), P, N, dskel + length(clustP - bNodeBuffer[neighbour].xyz)));

                            if(bVisibilityClusterBuffer[neighbour] == uSelectedVCluster) {
                                sum += candidateCoherency;
                            }
                            sumTotal += candidateCoherency;
                        }

                        sumTotal += myCoherency;
                    }
                }
            }
        }
    }

    return sum / sumTotal;

//    uint cluster = getNearestClusterWrtToGrid(P, N);
//    if(cluster == uint(-1)) {
//        return 0;
//    }

//    vec3 clustP = bNodeBuffer[cluster].xyz;
//    float dskel = length(P - clustP);

//    float myCoherency = max(0, computeCoherency2(uint(cluster), P, N, dskel));

//    float sum5 = 0.f;
//    float sumTotal = 0.f;

//    if(bVisibilityClusterBuffer[cluster] == uSelectedVCluster) {
//        sum5 += myCoherency;
//    }
//    sumTotal += myCoherency;

//    ivec2 neighbourOffsetCount = bNeighbourOffsetCountBuffer[cluster];

//    int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

//    for(int i = neighbourOffsetCount.x; i < end; ++i) {
//        int neighbour = bNeighbourBuffer[i];
//        float candidateCoherency = max(0, computeCoherency2(uint(neighbour), P, N, dskel + length(clustP - bNodeBuffer[neighbour].xyz)));

//        if(bVisibilityClusterBuffer[neighbour] == uSelectedVCluster) {
//            sum5 += candidateCoherency;
//        }
//        sumTotal += candidateCoherency;
//    }

//    return sum5 / sumTotal;
}

void main() {
    gridSize = textureSize(uNodeGrid, 0);

    vec3 nNormal = normalize(vNormal);
    fNormalDepth.xyz = nNormal;
    fNormalDepth.w = -vPosition.z / uZFar; // Normalized depth value

    fDiffuse = uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb / PI;

    float shininess = uMaterial.shininess + texture(uMaterial.shininessSampler, vTexCoords).r;
    fGlossyShininess.rgb = uMaterial.Ks * texture(uMaterial.KsSampler, vTexCoords).rgb * (shininess + 2) / (2 * PI);
    fGlossyShininess.a = shininess;

    fVClusterIndices = computeVClusters(vWorldSpacePosition, normalize(vWorldSpaceNormal));

    //vec2 coherencies;
    //fVClusterIndices = computeMultiVClusters(vWorldSpacePosition, normalize(vWorldSpaceNormal), coherencies);

    //fVClusterCoherencies.xy = coherencies;
    fVClusterCoherencies.xy = vec2(1, 0);
    fVClusterCoherencies.z = computeVCluster5Coherency(vWorldSpacePosition, normalize(vWorldSpaceNormal));

    fGeometryCluster = uint(floor(log(-vPosition.z / uZNear) / log(1 + 2 * tan(uHalfFOVY) / uSy)));
}

);

GLLightGateRenderer7::GeometryPassData::GeometryPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_MaterialUniforms(m_Program),
    m_uMVPMatrix(m_Program, "uMVPMatrix", true),
    m_uMVMatrix(m_Program, "uMVMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true),
    m_uSy(m_Program, "uSy", true),
    m_uHalfFOVY(m_Program, "uHalfFOVY", true),
    m_uZNear(m_Program, "uZNear", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uWorldToGrid(m_Program, "uVoxelSpace.worldToGrid", true),
    m_uVoxelSize(m_Program, "uVoxelSpace.voxelSize", true),
    m_uNodeGrid(m_Program, "uNodeGrid", true),
    m_uSelectedVCluster(m_Program, "uSelectedVCluster", true) {
}

// This shader sort the clusters of each tile (32x32 pixels) to compute the number of clusters
// in each tile, compute a new local index for each and link back each pixel to this index.
// At the end, we have 0 <= uClustersImage(x, y) <= uClusterCountsImage(tx, ty) - 1
const GLchar* GLLightGateRenderer7::FindUniqueGeometryClusterPassData::s_ComputeShader =
"#version 430\n"
GLOOPS_STRINGIFY(

// Input image computed in the geometry pass
uniform isampler2D uVisibilityClusterSampler;
uniform usampler2D uGeometryClusterSampler;
uniform sampler2D uNormalDepthSampler;

uniform uint uVClusterCount;

// Outputs of the shader:

// Link each pixel to a cluster index local to the tile
layout(r32ui) uniform writeonly uimage2D uClustersImage;

layout(std430, binding = 0) buffer ClusterCountsBuffer {
    uint data[];
} bClusterCountsBuffer;

layout(std430, binding = 3) buffer ClusterBBoxLowerBuffer {
    vec3 bClusterBBoxLowerBuffer[];
};

layout(std430, binding = 4) buffer ClusterBBoxUpperBuffer {
    vec3 bClusterBBoxUpperBuffer[];
};

layout(std430, binding = 5) buffer ClusterVClusterBuffer {
    ivec2 bClusterVClusterBuffer[];
};

uniform mat4 uInvProjMatrix;
uniform mat4 uRcpViewMatrix;
uniform vec2 uScreenSize;

// 32x32 is the size of a tile in screen space
layout(local_size_x = 32, local_size_y = 32) in;

const uint cNbThreads = gl_WorkGroupSize.x * gl_WorkGroupSize.y;

shared uint sSampleIndices[cNbThreads];
shared uint sClusterBuffer[cNbThreads];
shared uint sClusterOffsets[cNbThreads];

shared vec3 sViewSpaceBBoxLower[cNbThreads];
shared vec3 sViewSpaceBBoxUpper[cNbThreads];
//shared vec3 sViewSpaceNormals[cNbThreads];

void oddEvenSortClusters() {
    for(uint i = 0; i < cNbThreads; ++i) {
        uint j;
        if(i % 2 == 0) {
            j = 2 * gl_LocalInvocationIndex;
        } else {
            j = 2 * gl_LocalInvocationIndex + 1;
        }

        uint lhs;
        uint rhs;

        if(j < cNbThreads - 1) {
            lhs = sClusterBuffer[j];
            rhs = sClusterBuffer[j + 1];
        }

        barrier();

        if(j < cNbThreads - 1 && lhs > rhs) {
            sClusterBuffer[j + 1] = lhs;
            sClusterBuffer[j] = rhs;

            uint tmp = sSampleIndices[j + 1];
            sSampleIndices[j + 1] = sSampleIndices[j];
            sSampleIndices[j] = tmp;
        }

        barrier();
    }
}

void mergeSortClusters() {
    uint tid = gl_LocalInvocationIndex;

    for(uint i = 1; i < cNbThreads; i *= 2) {
        uint listSize = i;
        uint listIdx = tid / i;
        uint listOffset = listIdx * listSize;
        uint otherListOffset;
        uint newListOffset;

        if(listIdx % 2 == 0) {
            otherListOffset = listOffset + listSize;
            newListOffset = listOffset;
        } else {
            otherListOffset = listOffset - listSize;
            newListOffset = otherListOffset;
        }

        // On doit calculer la position finale de l'element courant
        uint indexInMyList = tid - listOffset;

        uint indexInOtherList = 0;

        uint value = sClusterBuffer[tid];
        uint sIdx = sSampleIndices[tid];

        uint a = 0;
        uint b = listSize;
        uint middle;

        for(uint j = 1; j < listSize; j *= 2) {
            middle = (a + b) / 2;
            uint otherValue = sClusterBuffer[otherListOffset + middle];
            if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
                b = middle;
            } else {
                a = middle;
            }
        }

        uint otherValue = sClusterBuffer[otherListOffset + a];
        if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
            indexInOtherList = a;
        } else {
            indexInOtherList = b;
        }

        uint finalIndex = indexInMyList + indexInOtherList;

        barrier();

        sClusterBuffer[newListOffset + finalIndex] = value;
        sSampleIndices[newListOffset + finalIndex] = sIdx;

        barrier();
    }
}

void blellochExclusiveAddScan() {
    // Up-sweep phase
    uint a = 2 * gl_LocalInvocationIndex;
    uint b = 2 * gl_LocalInvocationIndex + 1;

    for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
        if(gl_LocalInvocationIndex < s) {
            sClusterOffsets[b] += sClusterOffsets[a];

            a = a * 2 + 1;
            b = b * 2 + 1;
        }
        barrier();
    }

    // Down-sweep phase
    if(gl_LocalInvocationIndex == 0) {
        sClusterOffsets[cNbThreads - 1] = 0;
    }
    barrier();

    for(uint s = 1; s < cNbThreads; s *= 2) {
        if(gl_LocalInvocationIndex < s) {
            a = (a - 1) / 2;
            b = (b - 1) / 2;

            uint tmp = sClusterOffsets[b];
            sClusterOffsets[b] += sClusterOffsets[a];
            sClusterOffsets[a] = tmp;
        }
        barrier();
    }
}

void main() {
    // Local ID of the current thread
    uint tid = gl_LocalInvocationIndex;

    // This is the original cluster ID computed for this pixel
    uint cluster = texelFetch(uGeometryClusterSampler, ivec2(gl_GlobalInvocationID.xy), 0).x;
    ivec2 vclusters = texelFetch(uVisibilityClusterSampler, ivec2(gl_GlobalInvocationID.xy), 0).xy;
    if(vclusters.x < 0) {
        vclusters.x = int(uVClusterCount);
    }

    if(vclusters.y < 0) {
        vclusters.y = int(uVClusterCount);
    }

    // Initialize the shared memory
    sClusterBuffer[gl_LocalInvocationIndex] = (cluster * (uVClusterCount + 1) + vclusters.x) * (uVClusterCount + 1) + vclusters.y;
    sClusterOffsets[gl_LocalInvocationIndex] = 0;
    sSampleIndices[gl_LocalInvocationIndex] = gl_LocalInvocationIndex;

    barrier();

    // Local sorting
    //oddEvenSortClusters();
    mergeSortClusters();

    bool isUniqueCluster = gl_LocalInvocationIndex == 0 ||
            sClusterBuffer[gl_LocalInvocationIndex] != sClusterBuffer[gl_LocalInvocationIndex - 1];

    // Initialization for scan
    if(isUniqueCluster) {
        sClusterOffsets[gl_LocalInvocationIndex] = 1;
    }
    barrier();

    // Compaction step
    blellochExclusiveAddScan();

    // Compute offset
    if(!isUniqueCluster) {
        sClusterOffsets[gl_LocalInvocationIndex] -= 1;
    }
    barrier();

    // Write the number of clusters for this tile in the output image
    if(gl_LocalInvocationIndex == 0) {
        uint clusterCount = sClusterOffsets[cNbThreads - 1] + 1;
        bClusterCountsBuffer.data[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_WorkGroupSize.x] = clusterCount;
    }

    // Original view sample that have been moved to this cell by the sort step
    uint sampleIndex = sSampleIndices[gl_LocalInvocationIndex];

    // Offset associated to the cluster of this sample
    uint clusterOffset = sClusterOffsets[gl_LocalInvocationIndex];

    // Coordinates of this sample
    uint x = sampleIndex % gl_WorkGroupSize.x;
    uint y = (sampleIndex - x) / gl_WorkGroupSize.x;
    ivec2 globalCoords = ivec2(gl_WorkGroupID.x * gl_WorkGroupSize.x + x,
                         gl_WorkGroupID.y * gl_WorkGroupSize.y + y);

    // Write the offset for this cluster in the new cluster image
    imageStore(uClustersImage, globalCoords, uvec4(clusterOffset, 0, 0, 0));

    vclusters = texelFetch(uVisibilityClusterSampler, globalCoords, 0).xy;

    // Read geometric information of the original sample
    vec4 normalDepth = texelFetch(uNormalDepthSampler, globalCoords, 0);
    vec2 pixelNDC = vec2(-1.f, -1.f) + 2.f * (vec2(globalCoords) + vec2(0.5f, 0.5f)) / uScreenSize;
    vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(pixelNDC, 1.f, 1.f);
    farPointViewSpaceCoords = farPointViewSpaceCoords / farPointViewSpaceCoords.w;
    vec4 samplePointViewSpaceCoords = vec4(normalDepth.w * farPointViewSpaceCoords.xyz, 1);

    sViewSpaceBBoxLower[gl_LocalInvocationIndex] = vec3(uRcpViewMatrix * samplePointViewSpaceCoords);
    sViewSpaceBBoxUpper[gl_LocalInvocationIndex] = sViewSpaceBBoxLower[gl_LocalInvocationIndex];

    barrier();

    // Reduce step to get bounds of the clusters
    for(uint s = 1; s < cNbThreads; s *= 2) {
        uint other = tid + s;
        if(other < cNbThreads && sClusterOffsets[other] == clusterOffset) {
            //sData[tid] = max(sData[tid], sData[other]);
            sViewSpaceBBoxLower[tid] = min(sViewSpaceBBoxLower[tid], sViewSpaceBBoxLower[other]);
            sViewSpaceBBoxUpper[tid] = max(sViewSpaceBBoxUpper[tid], sViewSpaceBBoxUpper[other]);
        }
        barrier();
    }

    // Write bounds to ouput images
    if(isUniqueCluster) {
        uint globalOffset = (gl_WorkGroupID.x + gl_NumWorkGroups.x * gl_WorkGroupID.y) * cNbThreads + clusterOffset;

        bClusterBBoxLowerBuffer[globalOffset] = sViewSpaceBBoxLower[tid];
        bClusterBBoxUpperBuffer[globalOffset] = sViewSpaceBBoxUpper[tid];
        bClusterVClusterBuffer[globalOffset] = vclusters;
    }
}

);

GLLightGateRenderer7::FindUniqueGeometryClusterPassData::FindUniqueGeometryClusterPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uVisibilityClusterSampler(m_Program, "uVisibilityClusterSampler", true),
    m_uGeometryClusterSampler(m_Program, "uGeometryClusterSampler", true),
    m_uNormalDepthSampler(m_Program, "uNormalDepthSampler", true),
    m_uVClusterCount(m_Program, "uVClusterCount", true),
    m_uClustersImage(m_Program, "uClustersImage", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uScreenSize(m_Program, "uScreenSize", true) {

    m_Program.use();
    m_uVisibilityClusterSampler.set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uGeometryClusterSampler.set(TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
    m_uNormalDepthSampler.set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uClustersImage.set(IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS);
}

const GLchar* GLLightGateRenderer7::CountGeometryClusterPassData::s_ComputeShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform uint uTileCount;

// Inputs

// For each tile (Tx, Ty), bClusterCountsBuffer[Tx + Ty * 32] is the number of cluster contained in the tile
layout(std430, binding = 0) buffer ClusterCountsBuffer {
    uint bClusterCountsBuffer[];
};

// Outputs

// For each tile (Tx, Ty), bClusterTilesOffsetsBuffer[Tx + Ty * 32] will be the global index of the first cluster
// contained in the tile.
layout(std430, binding = 1) buffer ClusterTilesOffsetsBuffer {
    uint bClusterTilesOffsetsBuffer[];
};

// Will be the total number of clusters
layout(std430, binding = 2) buffer ClusterCount {
    uint bClusterCount;
};

// Will contains for each cluster the index of the associated tile
layout(std430, binding = 3) buffer ClusterToTileBuffer {
    uint bClusterToTileBuffer[];
};

layout(local_size_x = 1024) in;

const uint cNbThreads = gl_WorkGroupSize.x;

shared uint sGroupData[cNbThreads];

void main() {
    uint tileClusterCount = 0;
    // Init
    if(gl_GlobalInvocationID.x >= uTileCount) {
        sGroupData[gl_LocalInvocationIndex] = 0;
    } else {
        tileClusterCount = sGroupData[gl_LocalInvocationIndex] = bClusterCountsBuffer[gl_GlobalInvocationID.x];
    }

    barrier();

    // Scan to compute offsets and cluster count

    // Up-sweep phase
    uint a = 2 * gl_LocalInvocationIndex;
    uint b = 2 * gl_LocalInvocationIndex + 1;

    for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
        if(gl_LocalInvocationIndex < s) {
            sGroupData[b] += sGroupData[a];

            a = a * 2 + 1;
            b = b * 2 + 1;
        }
        barrier();
    }

    // Down-sweep phase
    if(gl_LocalInvocationIndex == 0) {
        sGroupData[cNbThreads - 1] = 0;
    }
    barrier();

    for(uint s = 1; s < cNbThreads; s *= 2) {
        if(gl_LocalInvocationIndex < s) {
            a = (a - 1) / 2;
            b = (b - 1) / 2;

            uint tmp = sGroupData[b];
            sGroupData[b] += sGroupData[a];
            sGroupData[a] = tmp;
        }
        barrier();
    }

    if(gl_GlobalInvocationID.x < uTileCount) {
        uint tileOffset = bClusterTilesOffsetsBuffer[gl_GlobalInvocationID.x] = sGroupData[gl_LocalInvocationIndex];

        if(gl_GlobalInvocationID.x == uTileCount - 1) {
            bClusterCount = sGroupData[gl_LocalInvocationIndex] + bClusterCountsBuffer[gl_GlobalInvocationID.x];
        }

        // Write correspondance from cluster to tile
        for(uint i = 0; i < tileClusterCount; ++i) {
            bClusterToTileBuffer[tileOffset + i] = gl_LocalInvocationIndex;
        }

        barrier();
    }
}

);

GLLightGateRenderer7::CountGeometryClusterPassData::CountGeometryClusterPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uTileCount(m_Program, "uTileCount", true) {
}

const GLchar* GLLightGateRenderer7::LightAssignmentPassData::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

struct VoxelSpace {
    // The length of an edge of a voxel in world space
    float voxelSize;
    // The transformation matrix that gives for a world space position
    // the associated voxel
    mat4 worldToGrid;
};

uniform VoxelSpace uVoxelSpace;
uniform usampler3D uNodeGrid;

ivec3 gridSize;

layout(std430, binding = 0) buffer NodeBuffer {
    vec4 bNodeBuffer[];
};

layout(std430, binding = 1) buffer NeighbourOffsetCountBuffer {
    ivec2 bNeighbourOffsetCountBuffer[];
};

layout(std430, binding = 2) buffer NeighbourBuffer {
    int bNeighbourBuffer[];
};

layout(std430, binding = 3) buffer VisibilityClusterBuffer {
    int bVisibilityClusterBuffer[];
};

float computeCoherency(unsigned int cluster, vec3 P, vec3 N) {
    vec4 node = bNodeBuffer[cluster];
    vec3 wi = node.xyz - P;
    float d = length(wi);
    wi /= d;
    return dot(wi, N) * node.w / (d * d);
}

unsigned int getNearestCluster(vec3 P, vec3 N) {
    vec3 lookAtPoint = P + N * uVoxelSpace.voxelSize;
    unsigned int cluster = unsigned int(-1);

    while(cluster == unsigned int(-1)) {
        vec4 gridSpacePosition = uVoxelSpace.worldToGrid * vec4(lookAtPoint, 1);
        ivec3 voxel = ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z);
        if(voxel.x < 0 || voxel.x >= gridSize.x || voxel.y < 0 || voxel.y >= gridSize.y ||
                voxel.z < 0 || voxel.z >= gridSize.z) {
            return unsigned int(-1);
        }

        cluster = texelFetch(uNodeGrid, voxel, 0).x;
        lookAtPoint += N * uVoxelSpace.voxelSize;
    }

    float coherency = computeCoherency(cluster, P, N);
    ivec2 neighbourOffsetCount = bNeighbourOffsetCountBuffer[cluster];

    int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

    for(int i = neighbourOffsetCount.x; i < end; ++i) {
        int neighbour = bNeighbourBuffer[i];
        float candidateCoherency = computeCoherency(unsigned int(neighbour), P, N);
        if(candidateCoherency > coherency) {
            coherency = candidateCoherency;
            cluster = unsigned int(neighbour);
        }
    }

    return cluster;
}

struct VPL {
    vec3 P;
    vec3 N;
    vec3 L;
};

uniform uint uVPLCount;

// Contains all the VPLs
layout(std430, binding = 4) buffer VPLBuffer {
    VPL bVPLBuffer[];
};

// Outputs

// For each geometry cluster
layout(std430, binding = 5) coherent buffer VPLOffsetsCountsBuffer {
    ivec2 bVPLOffsetCountsBuffer[];
};

layout(std430, binding = 6) coherent buffer VPLIndexBuffer {
    ivec3 bVPLIndexBuffer[];
};

//uniform ivec3** uVPLIndexBuffer;

layout(std430, binding = 7) coherent buffer NextOffsetBuffer {
    uint bNextOffset;
    uint bVPLGateCount;
};

uniform uint uClusterCount;

layout(std430, binding = 8) buffer ClusterTilesOffsetsBuffer {
    uint bClusterTilesOffsetsBuffer[];
};

layout(std430, binding = 9) buffer ClusterToTileBuffer {
    uint bClusterToTileBuffer[];
};

layout(std430, binding = 10) buffer ClusterBBoxLowerBuffer {
    vec3 bClusterBBoxLowerBuffer[];
};

layout(std430, binding = 11) buffer ClusterBBoxUpperBuffer {
    vec3 bClusterBBoxUpperBuffer[];
};

layout(std430, binding = 12) buffer ClusterVClusterBuffer {
    ivec2 bClusterVClusterBuffer[];
};

layout(local_size_x = 1024) in;

uniform bool uDoVPLShading;
uniform bool uDoGateShading;
uniform bool uNoShadow;
uniform bool uUseGateCosFactor;

uint tileID;
uint clusterIdx;
uint clusterLocalID;
vec3 bboxLower;
vec3 bboxUpper;
vec3 bboxCenter;
float bboxRadius;
int vclusterIdx;

struct Gate {
    vec4 P;
    vec4 N;
    vec4 radius;
    ivec4 neighbourVCluster;
};

// Visibility Clusters graph data:

layout(std430, binding = 13) buffer GateBuffer {
    Gate bGateBuffer[];
};

layout(std430, binding = 14) buffer GateOffsetsCountsBuffer {
    ivec2 bGateOffsetCountsBuffer[];
};

layout(std430, binding = 15) buffer TransfertFlagsBuffer {
    bool bTransfertFlagsBuffer[]; // An entry for each visibility cluster
};

bool testConeVsPoint(vec3 coneP, vec3 coneDir, float cosHalfAngle, vec3 P) {
    vec3 w = normalize(P - coneP);
    float c = dot(coneDir, w);

    return c > cosHalfAngle;
}

bool testConeVsSphere(vec3 coneVertex, vec3 coneAxis, float cosHalfAngle, float sinHalfAngle,
                      vec3 sphereCenter, float sphereRadius) {
    float rcpSin = 1.f / sinHalfAngle;
    vec3 U = coneVertex - (sphereRadius * rcpSin) * coneAxis;
    vec3 D = sphereCenter - U;

    if(dot(coneAxis, D) >= length(D) * cosHalfAngle) {
        D = sphereCenter - coneVertex;
        if(-dot(coneAxis, D) >= length(D) * sinHalfAngle) {
            return length(D) <= sphereRadius;
        } else {
            return true;
        }
    }
    return false;
}

bool testGateVPL(VPL vpl, Gate gate) {
    if(vclusterIdx != gate.neighbourVCluster.x) {
        return false;
    }

    vec3 wo = gate.P.xyz - vpl.P;
    float d = length(wo);
    wo /= d;
    float coneR = gate.radius.x;

    float cosine = dot(-wo, gate.N.xyz);
    if(cosine < 0) {
        return false;
    }

    if(uUseGateCosFactor) {
        coneR *= cosine;
    }

    float hLength = sqrt(d * d + coneR * coneR);
    float cosHalfAngle = d / hLength;
    float sinHalfAngle = coneR / hLength;

    return testConeVsSphere(vpl.P, wo, cosHalfAngle, sinHalfAngle, bboxCenter, bboxRadius);
}

int acceptGateVPL(VPL vpl, int vSkelCluster) {
    ivec2 offsetCount = bGateOffsetCountsBuffer[vSkelCluster];
    uint end = offsetCount.x + offsetCount.y;
    for(uint i = offsetCount.x; i < end; ++i) {
        if(testGateVPL(vpl, bGateBuffer[i])) {
            return int(i);
        }
    }
    return -2;
}

int acceptVPL(VPL vpl, int clusterVCluster) {
    if(clusterVCluster < 0) {
        return -2;
    }

    if(uNoShadow) {
        return -1;
    }

    uint skelcluster = getNearestCluster(vpl.P, vpl.N);
    if(skelcluster == uint(-1)) {
        return -2;
    }

    int vplVisibilityCluster = bVisibilityClusterBuffer[skelcluster];

    // Si cluster de transfert, pas d'utilisation de gate: on regarde si
    // le vpl appartient à un voisin;

    if(vplVisibilityCluster == clusterVCluster) {
        if(uDoVPLShading) {
            return -1;
        }
        return -2;
    }

    if(bTransfertFlagsBuffer[clusterVCluster]) {
        ivec2 offsetCount = bGateOffsetCountsBuffer[clusterVCluster];

        uint end = offsetCount.x + offsetCount.y;

        for(uint i = offsetCount.x; i < end; ++i) {
            Gate gate = bGateBuffer[i];
            if(gate.neighbourVCluster.x == vplVisibilityCluster) {
                if(uDoVPLShading) {
                    return -1;
                }
                return -2;
            }
        }
    } else {
        ivec2 offsetCount = bGateOffsetCountsBuffer[clusterVCluster];

        uint end = offsetCount.x + offsetCount.y;

        for(uint i = offsetCount.x; i < end; ++i) {
            Gate gate = bGateBuffer[i];
            if(bTransfertFlagsBuffer[gate.neighbourVCluster.x] && gate.neighbourVCluster.x == vplVisibilityCluster) {
                vec3 wi = vpl.P - bboxCenter;
                float d = length(wi);
                float ratio = gate.radius.x / d;
                if(ratio < 0.7) {
                    return -2;
                }

                if(uDoVPLShading) {
                    return -1;
                }
                return -2;
            }
        }
    }

    if(!uDoGateShading) {
        return -2;
    }

    return acceptGateVPL(vpl, vplVisibilityCluster);
}

void main() {
    clusterIdx = uint(gl_GlobalInvocationID.x);

    if(clusterIdx >= uClusterCount) {
        return;
    }

    gridSize = textureSize(uNodeGrid, 0);

    tileID = bClusterToTileBuffer[clusterIdx];
    clusterLocalID = clusterIdx - bClusterTilesOffsetsBuffer[tileID];

    uint dataIdx = tileID * 1024 + clusterLocalID;

    ivec2 visibilityClusterIdx = bClusterVClusterBuffer[dataIdx];
    vclusterIdx = visibilityClusterIdx.x;
    bboxLower = bClusterBBoxLowerBuffer[dataIdx];
    bboxUpper = bClusterBBoxUpperBuffer[dataIdx];
    bboxCenter = (bboxLower + bboxUpper) * 0.5f;
    bboxRadius = length(bboxLower - bboxUpper) * 0.5f;

    uint nbAcceptedLights = 0;
    uint offset = clusterIdx * uVPLCount;
    uint c = offset;
    for(uint i = 0; i < uVPLCount; ++i) {
        VPL vpl = bVPLBuffer[i];
        int gateIdx = acceptVPL(vpl, visibilityClusterIdx.x);
        if(gateIdx >= -1) {
            bVPLIndexBuffer[c++] = ivec3(i, gateIdx, 0);
            ++nbAcceptedLights;
        } else {
            gateIdx = acceptVPL(vpl, visibilityClusterIdx.y);
            if(acceptVPL(bVPLBuffer[i], visibilityClusterIdx.y) >= -1) {
                bVPLIndexBuffer[c++] = ivec3(i, gateIdx, 1);
                ++nbAcceptedLights;
            }
        }
    }

    bVPLOffsetCountsBuffer[clusterIdx] = ivec2(int(offset), int(nbAcceptedLights));


//    uint nbAcceptedLights = 0;
//    for(uint i = 0; i < uVPLCount; ++i) {
//        VPL vpl = bVPLBuffer[i];
//        int gateIdx = acceptVPL(vpl, visibilityClusterIdx.x);
//        if(gateIdx >= -1) {
//            //uVPLIndexBuffer[clusterIdx][nbAcceptedLights] = ivec3(i, gateIdx, 0);
//            ++nbAcceptedLights;
//        } else {
//            gateIdx = acceptVPL(vpl, visibilityClusterIdx.y);
//            if(acceptVPL(bVPLBuffer[i], visibilityClusterIdx.y) >= -1) {
//                //uVPLIndexBuffer[clusterIdx][nbAcceptedLights] = ivec3(i, gateIdx, 1);
//                ++nbAcceptedLights;
//            }
//        }
//    }

//    uint offset = atomicAdd(bNextOffset, nbAcceptedLights);

//    bVPLOffsetCountsBuffer[clusterIdx] = ivec2(int(offset), int(nbAcceptedLights));

//    uint c = offset;
//    for(uint i = 0; i < uVPLCount; ++i) {
//        VPL vpl = bVPLBuffer[i];
//        int gateIdx = acceptVPL(vpl, visibilityClusterIdx.x);
//        if(gateIdx >= -1) {
//            bVPLIndexBuffer[c++] = ivec3(i, gateIdx, 0);
//        } else {
//            gateIdx = acceptVPL(vpl, visibilityClusterIdx.y);
//            if(gateIdx >= -1) {
//                bVPLIndexBuffer[c++] = ivec3(i, gateIdx, 1);
//            }
//        }
//    }

    memoryBarrier();
}

);

GLLightGateRenderer7::LightAssignmentPassData::LightAssignmentPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uWorldToGrid(m_Program, "uVoxelSpace.worldToGrid", true),
    m_uVoxelSize(m_Program, "uVoxelSpace.voxelSize", true),
    m_uNodeGrid(m_Program, "uNodeGrid", true),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uClusterCount(m_Program, "uClusterCount", true),
    m_uDoVPLShading(m_Program, "uDoVPLShading", true),
    m_uDoGateShading(m_Program, "uDoGateShading", true),
    m_uNoShadow(m_Program, "uNoShadow", true),
    m_uUseGateCosFactor(m_Program, "uUseGateCosFactor", true)/*,
    m_uVPLIndexBuffer(m_Program, "uVPLIndexBuffer", true) */{
}

const GLchar* GLLightGateRenderer7::TransformVPLToViewSpacePassData::s_ComputeShader =
"#version 430\n"
GLOOPS_STRINGIFY(

struct VPL {
    vec3 P;
    vec3 N;
    vec3 L;
};

uniform uint uVPLCount;

// Contains all the VPLs
layout(std430, binding = 0) coherent buffer VPLBuffer {
    VPL bVPLBuffer[];
};

struct Gate {
    vec4 P;
    vec4 N;
    vec4 radius;
    ivec4 neighbourVCluster;
};

uniform uint uGateCount;

layout(std430, binding = 1) coherent buffer GateBuffer {
    Gate bGateBuffer[];
};

uniform mat4 uViewMatrix;
uniform mat3 uNormalMatrix;

layout(local_size_x = 1024) in;

void main() {
    uint idx = uint(gl_GlobalInvocationID.x);

    // A thread represents a cluster
    if(idx < uVPLCount) {
        bVPLBuffer[idx].P = vec3(uViewMatrix * vec4(bVPLBuffer[idx].P, 1));
        bVPLBuffer[idx].N = normalize(uNormalMatrix * bVPLBuffer[idx].N);
    }

    if(idx < uGateCount) {
        bGateBuffer[idx].P = uViewMatrix * bGateBuffer[idx].P;
        bGateBuffer[idx].N = vec4(normalize(uNormalMatrix * bGateBuffer[idx].N.xyz), 0);
    }

    memoryBarrier();
}

);

GLLightGateRenderer7::TransformVPLToViewSpacePassData::TransformVPLToViewSpacePassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uGateCount(m_Program, "uGateCount", true),
    m_uViewMatrix(m_Program, "uViewMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true) {
}

const GLchar* GLLightGateRenderer7::VPLShadingPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vFarPointViewSpaceCoords;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer7::VPLShadingPassData::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

// The GBuffer:
uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;
uniform sampler2D uVisibilityClusterCoherenciesSampler;
uniform usampler2D uGeometryClusterSampler;

uniform uvec2 uTileCount;

struct VPL {
    vec3 P;
    vec3 N;
    vec3 L;
};

uniform uint uVPLCount;
uniform bool uDisplayCostPerPixel;

// Contains all the VPLs
layout(std430, binding = 0) coherent buffer VPLBuffer {
    VPL bVPLBuffer[];
};

// For each cluster, records the number of VPLs affecting this cluster and an offset in the index
// buffer
layout(std430, binding = 1) coherent buffer VPLOffsetsCountsBuffer {
    ivec2 bVPLOffsetCountsBuffer[];
};

// Contains the indices of VPLs referenced by the clusters
layout(std430, binding = 2) coherent buffer VPLIndexBuffer {
    ivec3 bVPLIndexBuffer[];
};

layout(std430, binding = 3) buffer ClusterTilesOffsetsBuffer {
    uint bClusterTilesOffsetsBuffer[];
};

struct Gate {
    vec4 P;
    vec4 N;
    vec4 radius;
    ivec4 neighbourVCluster;
};

layout(std430, binding = 4) buffer GateBuffer {
    Gate bGateBuffer[];
};

layout(std430, binding = 5) buffer GateOffsetsCountsBuffer {
    ivec2 bGateOffsetCountsBuffer[];
};

layout(std430, binding = 6) buffer TransfertFlagsBuffer {
    bool bTransfertFlagsBuffer[]; // An entry for each visibility cluster
};


vec3 randomColor(uint value) {
    return fract(
                sin(
                    vec3(float(value) * 12.9898,
                         float(value) * 78.233,
                         float(value) * 56.128)
                    )
                * 43758.5453
                );
}

vec3 heatColor(float t) {
    vec3 map[] = vec3[](vec3(0, 0, 1), vec3(0, 1, 0), vec3(1, 0, 0));
    t *= 3;
    int i = clamp(int(t), 0, 2);
    int j = clamp(i + 1, 0, 2);
    float r = t - i;
    return (1 - r) * map[i] + r * map[j];
}

in vec3 vFarPointViewSpaceCoords;

out vec3 fFragColor;

uniform bool uUseGateCosFactor;
uniform bool uUseAttenuation;

bool testGate(Gate gate, VPL vpl, vec3 fragP, out float attenuation) {
    vec3 wo = gate.P.xyz - vpl.P;
    float d = length(wo);
    wo /= d;
    float radius = gate.radius.x;

    if(uUseGateCosFactor) {
        radius *= dot(-wo, gate.N.xyz);
    }

    float hLength = sqrt(d * d + radius * radius);
    float cosHalfAngle = d / hLength;

    vec3 w = normalize(fragP - vpl.P);
    float spotFactor = dot(wo, w);

    if(spotFactor > cosHalfAngle) {
        if(uUseAttenuation) {
            float x = (1.0 - spotFactor) / (1.0 - cosHalfAngle);
            attenuation = 1.0 - x * x;
        } else {
            attenuation = 1.f;
        }
        return true;
    }

    return false;
}

bool testGate(int gateIdx, VPL vpl, vec3 fragP, out float attenuation) {
    if(gateIdx < 0) {
        attenuation = 1.f;
        return true;
    }

    return testGate(bGateBuffer[gateIdx], vpl, fragP, attenuation);
}

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    ivec2 tileCoords = ivec2(texCoords * vec2(textureSize(uGeometryClusterSampler, 0).xy));
    tileCoords /= 32;

    uint tileIdx = tileCoords.x + tileCoords.y * uTileCount.x;

    uint clusterLocalID = texture(uGeometryClusterSampler, texCoords).r;
    uint clusterGlobalID = bClusterTilesOffsetsBuffer[tileIdx] + clusterLocalID;

    // Read fragment attributes
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vFarPointViewSpaceCoords;
    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    //vec3 Kd = vec3(1.f / 3.14f);
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

//    uint visibilityCluster = texture(uVisibilityClusterSampler, texCoords, 0);

    vec3 L[2] = vec3[](vec3(0), vec3(0));

    //fFragColor = vec3(0, 0, 0);

    ivec2 offsetCount = bVPLOffsetCountsBuffer[clusterGlobalID];

    if(uDisplayCostPerPixel) {
        fFragColor = heatColor(float(offsetCount.y) / uVPLCount);
        return;
    }

    int end = offsetCount.x + offsetCount.y;

    for(int j = offsetCount.x; j < end; ++j) {
        ivec3 vplIdxGateIdx = bVPLIndexBuffer[j];
        VPL vpl = bVPLBuffer[vplIdxGateIdx.x];

        float attenuation;
        if(testGate(vplIdxGateIdx.y, vpl, fragPosition, attenuation)) {
            vec3 wi = vpl.P - fragPosition;
            float dist = length(wi);
            wi /= dist;

            float cos_o = max(0, dot(-wi, vpl.N));
            float cos_i = max(0, dot(wi, normalDepth.xyz));

            dist = max(dist, 100);

            float G = cos_i * cos_o / (dist * dist);

            vec3 r = reflect(-wi, normalDepth.xyz);

            vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
            L[vplIdxGateIdx.z] = L[vplIdxGateIdx.z] + brdf * G * vpl.L * attenuation;
        }
    }

    vec2 coherencies = texture(uVisibilityClusterCoherenciesSampler, texCoords).xy;
    fFragColor = (coherencies.x * L[0] + coherencies.y * L[1]) / (coherencies.x + coherencies.y);
}

);

GLLightGateRenderer7::VPLShadingPassData::VPLShadingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uTileCount(m_Program, "uTileCount", true),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uDisplayCostPerPixel(m_Program, "uDisplayCostPerPixel", true),
    m_uUseGateCosFactor(m_Program, "uUseGateCosFactor", true),
    m_uUseAttenuation(m_Program, "uUseAttenuation", true)  {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);
    m_uSamplers[GBUFFER_VCLUSTER_COHERENCIES] = gloops::Uniform(m_Program, "uVisibilityClusterCoherenciesSampler", true);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS] = gloops::Uniform(m_Program, "uGeometryClusterSampler", true);

    // Initialize constant uniforms
    m_Program.use();
    m_uSamplers[GBUFFER_NORMAL_DEPTH].set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uSamplers[GBUFFER_DIFFUSE].set(TEXUNIT_GBUFFER_DIFFUSE);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(TEXUNIT_GBUFFER_GLOSSYSHININESS);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES].set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uSamplers[GBUFFER_VCLUSTER_COHERENCIES].set(TEXUNIT_GBUFFER_VCLUSTERS_COHERENCIES);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS].set(TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
}



const GLchar* GLLightGateRenderer7::DirectLightingPassData::s_VertexShader =
"#version 430\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vFarPointViewSpaceCoords;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer7::DirectLightingPassData::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

// The GBuffer:
uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;
uniform sampler2D uVisibilityClusterCoherenciesSampler;
uniform usampler2D uGeometryClusterSampler;

uniform mat4 uFaceProjectionMatrices[6];
uniform samplerCubeShadow uShadowMapSampler;

uniform vec3 uLightPosition;
uniform vec3 uLightIntensity;
uniform mat4 uLightViewMatrix;

uniform mat4 uRcpViewMatrix;

uniform float uDepthBias;

in vec3 vFarPointViewSpaceCoords;

out vec3 fFragColor;

int getFaceIdx(vec3 wi) {
    vec3 absWi = abs(wi);

    float maxComponent = max(absWi.x, max(absWi.y, absWi.z));

    if(maxComponent == absWi.x) {
        if(wi.x > 0) {
            return 0;
        }
        return 1;
    }

    if(maxComponent == absWi.y) {
        if(wi.y > 0) {
            return 2;
        }
        return 3;
    }

    if(maxComponent == absWi.z) {
        if(wi.z > 0) {
            return 4;
        }
        return 5;
    }

    return -1;
}

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vFarPointViewSpaceCoords;
    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    //vec3 Kd = vec3(1.f / 3.14f);
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    vec3 wi = uLightPosition - fragPosition;
    float dist = length(wi);
    wi /= dist;

    float cos_i = max(0, dot(wi, normalDepth.xyz));

    //dist = max(dist, 100);

    float G = cos_i / (dist * dist);

    vec3 fragPos_ws = vec3(uRcpViewMatrix * vec4(fragPosition, 1));
    vec3 lightPos_ws = vec3(uRcpViewMatrix * vec4(uLightPosition, 1));

    vec3 fragPos_ls = vec3(uLightViewMatrix * vec4(fragPos_ws, 1));
    vec3 shadowRay = normalize(fragPos_ws - lightPos_ws);
    int face = getFaceIdx(shadowRay);
    vec4 faceClipSpacePosition = uFaceProjectionMatrices[face] * vec4(fragPos_ls, 1);
    float depthRef = faceClipSpacePosition.z / faceClipSpacePosition.w;
    depthRef = 0.5 * (depthRef + 1) - uDepthBias;

    float shadowFactor = texture(uShadowMapSampler, vec4(shadowRay, depthRef));

    vec3 r = reflect(-wi, normalDepth.xyz);

    vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
    fFragColor = brdf * shadowFactor * G * uLightIntensity;
}

);

GLLightGateRenderer7::DirectLightingPassData::DirectLightingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uShadowMapSampler(m_Program, "uShadowMapSampler", true),
    m_uLightPosition(m_Program, "uLightPosition", true),
    m_uLightIntensity(m_Program, "uLightIntensity", true),
    m_uLightViewMatrix(m_Program, "uLightViewMatrix", true),
    m_uFaceProjectionMatrices(m_Program, "uFaceProjectionMatrices", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uDepthBias(m_Program, "uDepthBias", true),
    m_DoDirectLightingPass(false) {

    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);
    m_uSamplers[GBUFFER_VCLUSTER_COHERENCIES] = gloops::Uniform(m_Program, "uVisibilityClusterCoherenciesSampler", true);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS] = gloops::Uniform(m_Program, "uGeometryClusterSampler", true);

    // Initialize constant uniforms
    m_Program.use();
    m_uSamplers[GBUFFER_NORMAL_DEPTH].set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uSamplers[GBUFFER_DIFFUSE].set(TEXUNIT_GBUFFER_DIFFUSE);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(TEXUNIT_GBUFFER_GLOSSYSHININESS);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES].set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uSamplers[GBUFFER_VCLUSTER_COHERENCIES].set(TEXUNIT_GBUFFER_VCLUSTERS_COHERENCIES);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS].set(TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
}




const GLchar* GLLightGateRenderer7::DrawBuffersPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer7::DrawBuffersPassData::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;
uniform sampler2D uNormalDepthSampler;
uniform isampler2D uVisibilityClusterSampler;
uniform sampler2D uVisibilityClusterCoherenciesSampler;
uniform isampler2D uDiscontinuitySampler;
uniform usampler2D uGeometryClusterSampler;

layout(std430, binding = 0) buffer ClusterTilesOffsetsBuffer {
    uint bClusterTilesOffsetsBuffer[];
};

layout(std430, binding = 3) buffer ClusterBBoxLowerBuffer {
    vec3 bClusterBBoxLowerBuffer[];
};

layout(std430, binding = 4) buffer ClusterBBoxUpperBuffer {
    vec3 bClusterBBoxUpperBuffer[];
};

layout(std430, binding = 5) buffer ClusterVClusterBuffer {
    ivec2 bClusterVClusterBuffer[];
};

layout(std430, binding = 6) buffer ClusterToTileBuffer {
    uint bClusterToTileBuffer[];
};

uniform float uZFar;
uniform uvec2 uTileCount;

uniform int uDataToDisplay;

out vec3 fFragColor;

vec3 randomColor(uint value) {
    return fract(
                sin(
                    vec3(float(value) * 12.9898,
                         float(value) * 78.233,
                         float(value) * 56.128)
                    )
                * 43758.5453
                );
}

vec3 randomColor(uvec3 value) {
    return fract(
                sin(
                    vec3(float(value.x * value.y) * 12.9898,
                         float(value.y * value.z) * 78.233,
                         float(value.z * value.x) * 56.128)
                    )
                * 43758.5453
                );
}

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    ivec2 tileCoords = ivec2(texCoords * vec2(textureSize(uGeometryClusterSampler, 0).xy));
    tileCoords /= 32;

    uint tileIdx = tileCoords.x + tileCoords.y * uTileCount.x;

    vec3 coherencies = texture(uVisibilityClusterCoherenciesSampler, texCoords).xyz;

    if(uDataToDisplay == 0) {
        fFragColor = texture(uNormalDepthSampler, texCoords).rgb;
    } else if(uDataToDisplay == 1) {
        fFragColor = vec3(texture(uNormalDepthSampler, texCoords).a);
    } else if(uDataToDisplay == 2) {
        //int visibilityCluster = texture(uVisibilityClusterSampler, texCoords).r;

        int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);

        uint globalID = bClusterTilesOffsetsBuffer[tileIdx] + uint(geometryCluster);
        tileIdx = bClusterToTileBuffer[globalID];

        int visibilityCluster = bClusterVClusterBuffer[tileIdx * 1024 + geometryCluster].x;

        if(visibilityCluster < 0) {
            fFragColor = vec3(0, 0, 0);
        } else {
            fFragColor = coherencies.x * randomColor(visibilityCluster + 1) / (coherencies.x + coherencies.y);
        }

    } else if(uDataToDisplay == 3) {
        int discontinuity = texture(uDiscontinuitySampler, texCoords).r;
        if(discontinuity == 1) {
            fFragColor = vec3(1, 1, 1);
        } else {
            fFragColor = vec3(0, 0, 0);
        }
    } else if(uDataToDisplay == 4) {
        int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
        fFragColor = randomColor(bClusterTilesOffsetsBuffer[tileIdx] + geometryCluster);
    } else if (uDataToDisplay == 5) {
        int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
        vec3 bboxLower = bClusterBBoxLowerBuffer[tileIdx * 1024 + geometryCluster];
        fFragColor = vec3(-bboxLower.z / uZFar);
    } else if (uDataToDisplay == 6) {
        int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
        vec3 bboxUpper = bClusterBBoxUpperBuffer[tileIdx * 1024 + geometryCluster];
        fFragColor = vec3(-bboxUpper.z / uZFar);
    } else if (uDataToDisplay == 7) {
        int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
        uint globalID = bClusterTilesOffsetsBuffer[tileIdx] + uint(geometryCluster);
        fFragColor = randomColor(bClusterToTileBuffer[globalID]);
    } else if(uDataToDisplay == 8) {
        //int visibilityCluster = texture(uVisibilityClusterSampler, texCoords).r;

        int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);

        uint globalID = bClusterTilesOffsetsBuffer[tileIdx] + uint(geometryCluster);
        tileIdx = bClusterToTileBuffer[globalID];

        int visibilityCluster = bClusterVClusterBuffer[tileIdx * 1024 + geometryCluster].y;

        if(visibilityCluster < 0) {
            fFragColor = vec3(0, 0, 0);
        } else {
            fFragColor = coherencies.y * randomColor(uint(visibilityCluster)) / (coherencies.x + coherencies.y);
        }
    } else if(uDataToDisplay == 9) {
        fFragColor = vec3(coherencies.z);
    }
}

);

GLLightGateRenderer7::DrawBuffersPassData::DrawBuffersPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uNormalDepthSampler(m_Program, "uNormalDepthSampler", true),
    m_uVisibilityClusterSampler(m_Program, "uVisibilityClusterSampler", true),
    m_uVisibilityClusterCoherenciesSampler(m_Program, "uVisibilityClusterCoherenciesSampler", true),
    m_uDiscontinuitySampler(m_Program, "uDiscontinuitySampler", true),
    m_uGeometryClusterSampler(m_Program, "uGeometryClusterSampler", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uDataToDisplay(m_Program, "uDataToDisplay", true),
    m_uTileCount(m_Program, "uTileCount", true) {

    m_Program.use();
    m_uNormalDepthSampler.set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uVisibilityClusterSampler.set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uVisibilityClusterCoherenciesSampler.set(TEXUNIT_GBUFFER_VCLUSTERS_COHERENCIES);
    m_uDiscontinuitySampler.set(TEXUNIT_TEX2D_DISCONTINUITYBUFFER);
    m_uGeometryClusterSampler.set(TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
}


const GLchar* GLLightGateRenderer7::DiscontinuityPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer7::DiscontinuityPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;
uniform sampler2D uNormalDepthSampler;
uniform isampler2D uVisibilityClusterSampler;

uniform float uZFar;

out int fFragColor;

void main() {
    int x = int(gl_FragCoord.x);
    int y = int(gl_FragCoord.y);

    vec3 N = texelFetch(uNormalDepthSampler, ivec2(x, y), 0).rgb;
    vec3 Nr = texelFetch(uNormalDepthSampler, ivec2(x + 1, y), 0).rgb;
    vec3 Nb = texelFetch(uNormalDepthSampler, ivec2(x, y + 1), 0).rgb;
    vec3 Nrb = texelFetch(uNormalDepthSampler,ivec2(x + 1, y + 1), 0).rgb;

    float d1 = (dot(N, Nr) + dot(N, Nb) + dot(N, Nrb)) / 3.f;

    float d = texelFetch(uNormalDepthSampler, ivec2(x, y), 0).a;
    float dr = texelFetch(uNormalDepthSampler, ivec2(x + 1, y), 0).a;
    float db = texelFetch(uNormalDepthSampler, ivec2(x, y + 1), 0).a;
    float drb = texelFetch(uNormalDepthSampler,ivec2(x + 1, y + 1), 0).a;

    float meanDepth = (d + dr + db + drb) * 0.25f;
    float d2 = d / meanDepth;

    bool isGeometryDiscontinuous = d1 < 0.9 || d2 < 0.97 || d2 > 1.03;

    int C = texelFetch(uVisibilityClusterSampler, ivec2(x, y), 0).r;
    int Cr = texelFetch(uVisibilityClusterSampler, ivec2(x + 1, y), 0).r;
    int Cb = texelFetch(uVisibilityClusterSampler, ivec2(x, y + 1), 0).r;
    int Crb = texelFetch(uVisibilityClusterSampler, ivec2(x + 1, y + 1), 0).r;

    bool isClusteringDiscontinuous = C != Cr || C != Cb || C != Crb;

    fFragColor = int(isClusteringDiscontinuous && !isGeometryDiscontinuous);
}

);

GLLightGateRenderer7::DiscontinuityPassData::DiscontinuityPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uZFar(m_Program, "uZFar", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);

    m_Program.use();
    m_uSamplers[GBUFFER_NORMAL_DEPTH].set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uSamplers[GBUFFER_DIFFUSE].set(TEXUNIT_GBUFFER_DIFFUSE);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(TEXUNIT_GBUFFER_GLOSSYSHININESS);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES].set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS].set(TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
    m_uZFar.set(getZFar());
}


const GLchar* GLLightGateRenderer7::BlurPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer7::BlurPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;

uniform sampler2D uIrradianceBuffer;
uniform isampler2D uDiscontinuityBuffer;

uniform bool uRenderIrradiance;

uniform float uGammaExponent;

out vec3 fFragColor;

void main() {
    int x = int(gl_FragCoord.x);
    int y = int(gl_FragCoord.y);



//    bool isDiscontinuous = false;
//    for(int i = -half; i <= half; ++i) {
//        for(int j = -half; j <= half; ++j) {
//            if(1 == texelFetch(uDiscontinuityBuffer, ivec2(x + i, y + j), 0).r) {
//                isDiscontinuous = true;
//            }
//        }
//    }

//    int discontinuity = texelFetch(uDiscontinuityBuffer, ivec2(x, y), 0).r;
    vec3 irradiance = texelFetch(uIrradianceBuffer, ivec2(x, y), 0).rgb;

//    int half = 1;
//    ivec2 vclusters = texelFetch(uVisibilityClusterSampler, ivec2(x, y), 0).xy;
//    if(vclusters.y != -1) {
//        int nbSamples = 1;
//        for(int i = -half; i <= half; ++i) {
//            for(int j = -half; j <= half; ++j) {
//                if(i != 0 || j != 0) {
//                    ivec2 vc = texelFetch(uVisibilityClusterSampler, ivec2(x + i, y + i), 0).xy;
//                    if(vc.x == vclusters.y) {
//                        irradiance += texelFetch(uIrradianceBuffer, ivec2(x + i, y + j), 0).rgb;
//                        ++nbSamples;
//                    }
//                }
//            }
//        }
//        irradiance /= float(nbSamples);
//    }

//    if(isDiscontinuous) {
//        for(int i = -half; i <= half; ++i) {
//            for(int j = -half; j <= half; ++j) {
//                if(i != 0 || j != 0) {
//                    irradiance += texelFetch(uIrradianceBuffer, ivec2(x + i, y + j), 0).rgb;
//                }
//            }
//        }
//        int nbSamples = (half * 2 + 1);
//        nbSamples *= nbSamples;
//        irradiance /= nbSamples;
//    }

//    vec3 Kd;

//    if(uRenderIrradiance) {
//        Kd = vec3(1.f / 3.14f);
//    } else {
//        Kd = texelFetch(uDiffuseSampler, ivec2(x, y), 0).rgb;
//    }

    fFragColor = pow(irradiance, vec3(uGammaExponent));
}

);

GLLightGateRenderer7::BlurPassData::BlurPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uIrradianceBuffer(m_Program, "uIrradianceBuffer", true),
    m_uDiscontinuityBuffer(m_Program, "uDiscontinuityBuffer", true),
    m_uRenderIrradiance(m_Program, "uRenderIrradiance", true),
    m_uGammaExponent(m_Program, "uGammaExponent", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);

    m_Program.use();
    m_uSamplers[GBUFFER_NORMAL_DEPTH].set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uSamplers[GBUFFER_DIFFUSE].set(TEXUNIT_GBUFFER_DIFFUSE);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(TEXUNIT_GBUFFER_GLOSSYSHININESS);
    m_uSamplers[GBUFFER_VCLUSTER_INDICES].set(TEXUNIT_GBUFFER_VCLUSTERS);
    m_uSamplers[GBUFFER_GEOMETRYCLUSTERS].set(TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
    m_uDiscontinuityBuffer.set(TEXUNIT_TEX2D_DISCONTINUITYBUFFER);
    m_uIrradianceBuffer.set(TEXUNIT_TEX2D_IRRADIANCEBUFFER);
}

GLLightGateRenderer7::GLLightGateRenderer7():
    m_ProjectionMatrix(1.f), m_ViewMatrix(1.f),
    m_bDoVPLShadingPass(true), m_bDoGateShadingPass(true), m_bNoShadow(false),
    m_bDoDirectLightPass(true), m_bDoIndirectLightPass(true), m_bRenderIrradiance(false),
    m_bDisplayCostPerPixel(false), m_bUseGateCosFactor(false),
    m_bUseAttenuation(true), m_nSelectedVisibilityCluster(-1),
    m_fGamma(2.2f) {
}

void GLLightGateRenderer7::setResolution(size_t width, size_t height) {
    {
        GLTexFormat formats[GBUFFER_COUNT] = {
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32I, GL_RGBA_INTEGER, GL_INT },
            { GL_RGB32F, GL_RGB, GL_FLOAT },
            { GL_RGBA32UI, GL_RGBA_INTEGER, GL_UNSIGNED_INT }
        };
        m_GBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }

    {
        GLTexFormat formats[1] = {
            { GL_R8UI, GL_RED_INTEGER, GL_UNSIGNED_BYTE }
        };

        m_DiscontinuityBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }

    {
        m_IrradianceBuffer.init(width, height);
    }

    // Number of tiles in each dimension
    uint32_t Sx = (width / TILE_SIZE) + (width % TILE_SIZE != 0);
    uint32_t Sy = (height / TILE_SIZE) + (height % TILE_SIZE != 0);

    m_TileCount = glm::ivec2(Sx, Sy);

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GeometryClustersTexture.glId());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI, width, height, 0,
                 GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    GLuint size = Sx * Sy;

    m_GClusterCountsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterTilesOffsetsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterTotalCountBuffer.setSize(sizeof(GLuint), GL_STATIC_DRAW);

    size_t imageSize = width * height;

    m_GClusterToTileBuffer.setSize(imageSize * sizeof(GLuint), GL_STATIC_DRAW);
    m_ClusterBBoxLowerBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterBBoxUpperBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterVClusterBuffer.setSize(imageSize * sizeof(glm::ivec2), GL_STATIC_DRAW);
}

void GLLightGateRenderer7::setUp() {
    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_NORMALDEPTH);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_DIFFUSE);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_DIFFUSE).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_GLOSSYSHININESS);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_VCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_VCLUSTER_INDICES).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_VCLUSTERS_COHERENCIES);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_VCLUSTER_COHERENCIES).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GEOMETRYCLUSTERS).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_DISCONTINUITYBUFFER);
    glBindTexture(GL_TEXTURE_2D, m_DiscontinuityBuffer.getColorBuffer(0).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_IRRADIANCEBUFFER);
    glBindTexture(GL_TEXTURE_2D, m_IrradianceBuffer.getColorBuffer(0).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GeometryClustersTexture.glId());
    glBindImageTexture(IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS, m_GeometryClustersTexture.glId(),
                       0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32UI);

    glActiveTexture(GL_TEXTURE0);
}

void GLLightGateRenderer7::computeGeometryDiscontinuityBuffer() {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLTimer timer(m_DiscontinuityPass.m_TimeElapsedQuery);

    m_DiscontinuityPass.m_Program.use();
    m_DiscontinuityBuffer.bindForDrawing();

    m_DiscontinuityPass.m_uViewport.set(glm::vec4(0, 0,
                                                  m_DiscontinuityBuffer.getWidth(),
                                                  m_DiscontinuityBuffer.getHeight()));

    m_ScreenTriangle.render();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLLightGateRenderer7::findUniqueGeometryClusters() {
    {
        GLTimer timer(m_FindUniqueGeometryClusterPassData.m_TimeElapsedQuery);

        m_GClusterCountsBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
        m_GClusterTilesOffsetsBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);
        m_GClusterTotalCountBuffer.bindBase(2, GL_SHADER_STORAGE_BUFFER);
        m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
        m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
        m_ClusterVClusterBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);

        m_FindUniqueGeometryClusterPassData.m_Program.use();

        glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
        m_FindUniqueGeometryClusterPassData.m_uInvProjMatrix.set(rcpProjMatrix);
        m_FindUniqueGeometryClusterPassData.m_uRcpViewMatrix.set(glm::inverse(m_ViewMatrix));

        m_FindUniqueGeometryClusterPassData.m_uScreenSize.set(float(m_GBuffer.getWidth()),
                                            float(m_GBuffer.getHeight()));

        glDispatchCompute(m_TileCount.x, m_TileCount.y, 1);
    }

    {
        GLTimer timer(m_CountGeometryClusterPassData.m_TimeElapsedQuery);

        m_CountGeometryClusterPassData.m_Program.use();

        uint32_t tileCount = m_TileCount.x * m_TileCount.y;
        uint32_t workGroupSize = 1024;
        uint32_t workGroupCount = (tileCount / workGroupSize) + (tileCount % workGroupSize);

        m_GClusterToTileBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);

        // ONLY WORKS FOR IMAGE WITH LESS THAN 1024 TILES
        // Can be extended for more but require more compute shader code to handle the parallel
        // segmented scan
        assert(workGroupCount <= 1);

        m_CountGeometryClusterPassData.m_uTileCount.set(tileCount);

        glDispatchCompute(workGroupCount, 1, 1);

        GLuint* ptr = m_GClusterTotalCountBuffer.map<GLuint>(GL_READ_ONLY);
        m_nGClusterCount = *ptr;
        m_GClusterTotalCountBuffer.unmap();

        /*
            ptr = m_GClusterToTileBuffer.map<GLuint>(GL_READ_ONLY);

            for(int i = 0; i < m_nGClusterCount; ++i) {
                std::cerr << ptr[i] << " ";
            }
            std::cerr << std::endl;

            m_GClusterToTileBuffer.unmap();

            // Check cluster counts
            std::cerr << "Total number of geometry clusters = " << m_nGClusterCount << std::endl;

            ptr = m_GClusterCountsBuffer.map<GLuint>(GL_READ_ONLY);

            for(int i = 0; i < tileCount; ++i) {
                std::cerr << ptr[i] << " ";
            }
            std::cerr << std::endl;

            m_GClusterCountsBuffer.unmap();

            ptr = m_GClusterTilesOffsetsBuffer.map<GLuint>(GL_READ_ONLY);

            for(int i = 0; i < tileCount; ++i) {
                std::cerr << ptr[i] << " ";
            }
            std::cerr << std::endl;

            m_GClusterTilesOffsetsBuffer.unmap();
        */
    }
}

void GLLightGateRenderer7::geometryPass(const GLScene& scene) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    {
        GLTimer timer(m_GeometryPassData.m_TimeElapsedQuery);

        glViewport(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight());

        m_GeometryPassData.m_Program.use();

        m_GBuffer.bindForDrawing();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 MVPMatrix = m_ProjectionMatrix * m_ViewMatrix;
        glm::mat4 MVMatrix = m_ViewMatrix;
        glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

        m_GeometryPassData.m_uMVPMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVPMatrix));
        m_GeometryPassData.m_uMVMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVMatrix));
        m_GeometryPassData.m_uNormalMatrix.setMatrix3(1, GL_FALSE, glm::value_ptr(NormalMatrix));

        m_GeometryPassData.m_uSy.set(float(m_TileCount.y));
        m_GeometryPassData.m_uHalfFOVY.set(glm::radians(0.5f * getFOVY()));
        m_GeometryPassData.m_uZNear.set(getZNear());
        m_GeometryPassData.m_uZFar.set(getZFar());

        m_GeometryPassData.m_uSelectedVCluster.set(m_nSelectedVisibilityCluster);

        GLMaterialManager::TextureUnits units(TEXUNIT_MAT_DIFFUSE, TEXUNIT_MAT_GLOSSY, TEXUNIT_MAT_SHININESS);

        m_GLCurvSkelClusteringBuffers.m_NodeBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
        m_GLCurvSkelClusteringBuffers.m_NeighbourOffsetCountBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);
        m_GLCurvSkelClusteringBuffers.m_NeighbourBuffer.bindBase(2, GL_SHADER_STORAGE_BUFFER);
        m_GLCurvSkelClusteringBuffers.m_VisibilityClusterBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);

        scene.render(m_GeometryPassData.m_MaterialUniforms, units);
    }

    computeGeometryDiscontinuityBuffer();
    findUniqueGeometryClusters();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLLightGateRenderer7::sendVisibilityClusteringGrid(const Scene& scene,
                                                       const VisibilityClustering& vclustering) {
    const CurvSkelClustering& clustering = scene.voxelSpace->getClustering();
    const Grid3D<GraphNodeIndex>& grid = clustering.getGrid();

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX3D_NODEGRID);
    glBindTexture(GL_TEXTURE_3D, m_GLCurvSkelClusteringBuffers.m_Grid.glId());
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32UI, grid.width(), grid.height(), grid.depth(), 0,
                 GL_RED_INTEGER, GL_UNSIGNED_INT, grid.data());

    {
        std::vector<glm::vec4> nodeData;
        nodeData.reserve(clustering.size());
        for(auto nodeIdx: range(clustering.size())) {
            const auto& node = clustering.getNode(nodeIdx);
            nodeData.emplace_back(glm::vec4(node.P.x, node.P.y, node.P.z, node.maxball));
        }

        m_GLCurvSkelClusteringBuffers.m_NodeBuffer.setData(nodeData.size(), nodeData.data(), GL_STATIC_DRAW);
    }

    {
        std::vector<int32_t> neighbourLists;
        std::vector<glm::ivec2> neighbourOffsetsCounts;
        neighbourOffsetsCounts.reserve(clustering.size());
        std::vector<int32_t> visibilityClusters;
        visibilityClusters.reserve(clustering.size());

        int offset = 0;
        for(auto nodeIdx: range(clustering.size())) {
            visibilityClusters.emplace_back(vclustering.getClusterOf(nodeIdx));
            const auto& neighbours = clustering.neighbours(nodeIdx);
            for(auto neighbourIdx: neighbours) {
                neighbourLists.emplace_back(neighbourIdx);
            }
            auto count = neighbours.size();
            neighbourOffsetsCounts.emplace_back(glm::ivec2(offset, count));
            offset += count;
        }

        m_GLCurvSkelClusteringBuffers.m_NeighbourBuffer.setData(neighbourLists.size(), neighbourLists.data(), GL_STATIC_DRAW);
        m_GLCurvSkelClusteringBuffers.m_NeighbourOffsetCountBuffer.setData(neighbourOffsetsCounts.size(), neighbourOffsetsCounts.data(), GL_STATIC_DRAW);
        m_GLCurvSkelClusteringBuffers.m_VisibilityClusterBuffer.setData(visibilityClusters.size(), visibilityClusters.data(), GL_STATIC_DRAW);
    }

    std::vector<GLuint> vclusterGateCounts;
    for(uint32_t vcluster: range(vclustering.size())) {
        vclusterGateCounts.emplace_back(vclustering[vcluster].getNeighbourLinks().size());
    }

    m_GLCurvSkelClusteringBuffers.m_VClusterGateCountBuffer.setData(vclusterGateCounts.size(), vclusterGateCounts.data(), GL_STATIC_DRAW);

    m_GeometryPassData.m_Program.use();
    m_GeometryPassData.m_uWorldToGrid.set(convert(scene.voxelSpace->getWorldToLocalTransform()));
    m_GeometryPassData.m_uVoxelSize.set(scene.voxelSpace->getVoxelSize());
    m_GeometryPassData.m_uNodeGrid.set(TEXUNIT_TEX3D_NODEGRID);

    m_WorldToGridMatrix = convert(scene.voxelSpace->getWorldToLocalTransform());

    m_LightAssignmentPassData.m_Program.use();
    m_LightAssignmentPassData.m_uWorldToGrid.set(m_WorldToGridMatrix);
    m_LightAssignmentPassData.m_uVoxelSize.set(scene.voxelSpace->getVoxelSize());
    m_LightAssignmentPassData.m_uNodeGrid.set(TEXUNIT_TEX3D_NODEGRID);

    m_FindUniqueGeometryClusterPassData.m_Program.use();
    m_FindUniqueGeometryClusterPassData.m_uVClusterCount.set(GLuint(vclustering.size()));
}

struct GLSLGate {
    glm::vec4 P;
    glm::vec4 N;
    glm::vec4 radius;
    glm::ivec4 neighbourVCluster;

    GLSLGate(const VisibilityGate& G):
        P(G.center.x, G.center.y, G.center.z, 1), N(G.N.x, G.N.y, G.N.z, 0),
        radius(G.radius, 0, 0, 0), neighbourVCluster(G.destinationCluster, 0, 0, 0) {
    }
};

void GLLightGateRenderer7::sendVPLs(const VPLContainer& vpls, const Scene& scene,
                                   const VisibilityClustering& vclustering) {
    uint32_t workGroupSize = 1024;
    uint32_t workItemCount;

    // Init buffers' data

    {
        std::vector<glm::vec4> vplBuffer;
        for(const auto& vpl: vpls.orientedVPLs) {
            glm::vec4 lightPosition = glm::vec4(convert(vpl.P), 1);
            glm::vec3 lightNormal = convert(vpl.N);

            vplBuffer.emplace_back(lightPosition);
            vplBuffer.emplace_back(glm::vec4(lightNormal, 0));
            vplBuffer.emplace_back(glm::vec4(vpl.L.r, vpl.L.g, vpl.L.b, 0));
        }

        m_VPLBuffer.setData(vplBuffer.size(), vplBuffer.data(), GL_DYNAMIC_DRAW);
    }

    uint32_t maxGateCount = 0;
    GLuint gateCount = 0;
    uint i = 0;
    {
        std::vector<GLSLGate> gateBuffer;
        std::vector<glm::ivec2> gateOffsetCountBuffer;
        std::vector<int> transfertFlagBuffer;

        int offset = 0;
        for(auto vcluster: range(vclustering.size())) {
            for(const auto& neighbourLink: vclustering[vcluster].getNeighbourLinks()) {
                gateBuffer.emplace_back(vclustering.getGate(neighbourLink));

                if(vclustering[gateBuffer.back().neighbourVCluster.x].isTransfertCluster()) {
                    for(const auto& nL: vclustering[gateBuffer.back().neighbourVCluster.x].getNeighbourLinks()) {
                        if(vclustering.getGate(nL).destinationCluster != vcluster) {
                            gateBuffer.back().neighbourVCluster.x = vclustering.getGate(nL).destinationCluster;
                            break;
                        }
                    }
                }

            }
            uint32_t count = vclustering[vcluster].getNeighbourLinks().size();
            maxGateCount = embree::max(count, maxGateCount);
            gateOffsetCountBuffer.emplace_back(glm::ivec2(offset, count));
            offset += count;
            gateCount += count;
            transfertFlagBuffer.emplace_back(vclustering[vcluster].isTransfertCluster());
        }

        m_GateBuffer.setData(gateBuffer, GL_STATIC_DRAW);
        m_GateOffsetsCountsBuffer.setData(gateOffsetCountBuffer, GL_STATIC_DRAW);
        m_TransfertFlagsBuffer.setData(transfertFlagBuffer, GL_STATIC_DRAW);
    }

    m_VPLOffsetsCountsBuffer.setSize(sizeof(glm::ivec2) * m_nGClusterCount, GL_DYNAMIC_DRAW);
    m_VPLIndexBuffer.setSize(vpls.orientedVPLs.size() * m_nGClusterCount * sizeof(glm::ivec4), GL_DYNAMIC_DRAW);

    GLuint data[] = { 0, 0 };

    m_LightAssignmentPassData.m_NextOffsetBuffer.setData(2, data, GL_DYNAMIC_DRAW);

    {
        GLTimer timer(m_LightAssignmentPassData.m_TimeElapsedQuery);

        // Seems too long:
//        m_LightAssignmentPassData.m_VPLIndexBuffers.resize(m_nGClusterCount);
//        std::vector<GLuint64> indexBufferPtrs;
//        for(int i = 0; i < m_nGClusterCount; ++i) {
//            m_LightAssignmentPassData.m_VPLIndexBuffers[i].makeNonResident();
//            m_LightAssignmentPassData.m_VPLIndexBuffers[i].setSize(vpls.orientedVPLs.size() * sizeof(glm::ivec4), GL_DYNAMIC_DRAW);
//            m_LightAssignmentPassData.m_VPLIndexBuffers[i].makeResident(GL_READ_WRITE);
//            indexBufferPtrs.emplace_back(m_LightAssignmentPassData.m_VPLIndexBuffers[i].getGPUAddress());
//        }

//        m_LightAssignmentPassData.m_VPLIndexBufferPtrs.makeNonResident();
//        m_LightAssignmentPassData.m_VPLIndexBufferPtrs.setData(indexBufferPtrs, GL_DYNAMIC_DRAW);
//        m_LightAssignmentPassData.m_VPLIndexBufferPtrs.makeResident(GL_READ_WRITE);

        m_LightAssignmentPassData.m_Program.use();

        // Set Uniforms
        m_LightAssignmentPassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
        m_LightAssignmentPassData.m_uClusterCount.set(GLuint(m_nGClusterCount));
        m_LightAssignmentPassData.m_uDoVPLShading.set(m_bDoVPLShadingPass);
        m_LightAssignmentPassData.m_uDoGateShading.set(m_bDoGateShadingPass);
        m_LightAssignmentPassData.m_uNoShadow.set(m_bNoShadow);
        m_LightAssignmentPassData.m_uUseGateCosFactor.set(m_bUseGateCosFactor);
        //m_LightAssignmentPassData.m_uVPLIndexBuffer.set(m_LightAssignmentPassData.m_VPLIndexBufferPtrs.getGPUAddress());

    //    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);
    //    m_LightAssignmentPassData.m_uWorldToGrid.set(m_WorldToGridMatrix * rcpViewMatrix);

        // Bind buffers
        m_GLCurvSkelClusteringBuffers.m_NodeBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
        m_GLCurvSkelClusteringBuffers.m_NeighbourOffsetCountBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);
        m_GLCurvSkelClusteringBuffers.m_NeighbourBuffer.bindBase(2, GL_SHADER_STORAGE_BUFFER);
        m_GLCurvSkelClusteringBuffers.m_VisibilityClusterBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
        m_VPLBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
        m_VPLOffsetsCountsBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);
        m_VPLIndexBuffer.bindBase(6, GL_SHADER_STORAGE_BUFFER);
        m_LightAssignmentPassData.m_NextOffsetBuffer.bindBase(7, GL_SHADER_STORAGE_BUFFER);

        m_GClusterTilesOffsetsBuffer.bindBase(8, GL_SHADER_STORAGE_BUFFER);
        m_GClusterToTileBuffer.bindBase(9, GL_SHADER_STORAGE_BUFFER);
        m_ClusterBBoxLowerBuffer.bindBase(10, GL_SHADER_STORAGE_BUFFER);
        m_ClusterBBoxUpperBuffer.bindBase(11, GL_SHADER_STORAGE_BUFFER);
        m_ClusterVClusterBuffer.bindBase(12, GL_SHADER_STORAGE_BUFFER);

        m_GateBuffer.bindBase(13, GL_SHADER_STORAGE_BUFFER);
        m_GateOffsetsCountsBuffer.bindBase(14, GL_SHADER_STORAGE_BUFFER);
        m_TransfertFlagsBuffer.bindBase(15, GL_SHADER_STORAGE_BUFFER);

        // Launch compute shader
        workItemCount = (m_nGClusterCount / workGroupSize) + (m_nGClusterCount % workGroupSize != 0);
        glDispatchCompute(workItemCount, 1, 1);

        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }

    {
        GLTimer timer(m_TransformVPLToViewSpacePassData.m_TimeElapsedQuery);

        glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

        m_TransformVPLToViewSpacePassData.m_Program.use();

        m_VPLBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
        m_GateBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);

        m_TransformVPLToViewSpacePassData.m_uViewMatrix.set(m_ViewMatrix);
        m_TransformVPLToViewSpacePassData.m_uNormalMatrix.set(NormalMatrix);
        m_TransformVPLToViewSpacePassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
        m_TransformVPLToViewSpacePassData.m_uGateCount.set(gateCount);

        uint32_t taskCount = embree::max(uint32_t(vpls.orientedVPLs.size()), uint32_t(gateCount));
        workItemCount = (taskCount / workGroupSize) + (taskCount % workGroupSize != 0);
        glDispatchCompute(workItemCount, 1, 1);

        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }
}

void GLLightGateRenderer7::displayData(GLVisualDataRenderer& renderer, const Scene& scene,
                 const VPLContainer& vpls, const VisibilityClustering& vclustering) {
//    sendVPLs(vpls, scene, vclustering);
//    for(uint32_t j = 5; j < 6; ++j) {
//        bool stop = false;
//        for(const auto& neighbourLink:
//            index(vclustering[j].getNeighbourLinks())) {
//            if(stop) {
//                break;
//            }
//            stop = true;
//            auto gate = vclustering.getGate(neighbourLink.second);

//            AffineSpace3f frame = coordinateSystem(gate.center, gate.N);

//            Vec3f pointOnCircle = embree::xfmPoint(frame, Vec3f(gate.radius, 0, 0));

//            // Transforme chaque VPL en spot
//            for(uint32_t vplIdx: m_ClusterVPLs[j]) {
//                const OrientedVPL& vpl = vpls.orientedVPLs[vplIdx];

//                GateVPLSpot spot;
//                spot.P = vpl.P;
//                spot.N = vpl.N;
//                spot.L = vpl.L;
//                spot.wo = embree::normalize(gate.center - vpl.P);
//                spot.cosHalfAngle = embree::dot(spot.wo, embree::normalize(pointOnCircle - vpl.P));

//                renderer.renderLine(spot.P, gate.center, vpl.L, vpl.L, 1);
//                renderer.renderLine(spot.P, pointOnCircle, Col3f(1, 0, 0), Col3f(1, 0, 0), 1);
//            }
//        }
//    }

    size_t imageSize = m_GBuffer.getWidth() * m_GBuffer.getHeight();
    size_t tileCount = m_TileCount.x * m_TileCount.y;

    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<GLuint> clusterCount(tileCount);
    //std::vector<GLuint> clusterTilesOffsets(tileCount);

    //m_GClusterTilesOffsetsBuffer.getData(clusterTilesOffsets.size(), clusterTilesOffsets.data());
    m_GClusterCountsBuffer.getData(clusterCount.size(), clusterCount.data());
    m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());

    renderer.setUp();

    for(auto pair: index(clusterCount)) {
        GLuint offset = 1024 * pair.first;
        for(auto i: range(pair.second)) {
            glm::vec3 upper = glm::vec3(bboxUpper[offset + i]), lower = glm::vec3(bboxLower[offset + i]);
            float radius = glm::length(upper - lower) * 0.5f;
            renderer.renderSphere(0.5f * convert(upper + lower), radius, Col3f(1, 0, 0));
        }
    }
}

void GLLightGateRenderer7::setPrimaryLightSource(const Vec3f& P, const Col3f& L, const GLScene& glScene) {
    m_DirectLightingPassData.m_DoDirectLightingPass = true;
    m_DirectLightingPassData.m_LightPosition = convert(P);
    m_DirectLightingPassData.m_LightIntensity = convert(L);
    m_DirectLightingPassData.m_LightViewMatrix =
            glm::translate(glm::mat4(1), -m_DirectLightingPassData.m_LightPosition);

    m_DirectLightingPassData.m_ShadowMapContainer.init(2048, 1);
    m_DirectLightingPassData.m_ShadowMapRenderer.render(
                glScene, m_DirectLightingPassData.m_ShadowMapContainer, &m_DirectLightingPassData.m_LightViewMatrix, 1);

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEXCUBEMAP_SHADOWMAP);
    m_DirectLightingPassData.m_ShadowMapContainer.bindForShadowTest(0);
}

void GLLightGateRenderer7::shadingPass(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls, const Scene& scene, const VisibilityClustering& vclustering) {

    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::CapabilityRestorer blending(GL_BLEND, true);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    sendVPLs(vpls, scene, vclustering);

    {
        GLTimer timer(m_VPLShadingPassData.m_TimeElapsedQuery);

        GLint bindedFramebuffer;
        glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &bindedFramebuffer);

        m_IrradianceBuffer.bindForDrawing();

        glViewport(x, y, width, height);

        glBlendEquation(GL_FUNC_ADD);
        glBlendFunc(GL_ONE, GL_ONE);

        glClear(GL_COLOR_BUFFER_BIT);

        if(m_bDoIndirectLightPass) {
            m_VPLShadingPassData.m_Program.use();
            m_VPLShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
            m_VPLShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);
            m_VPLShadingPassData.m_uTileCount.set(m_TileCount);
            m_VPLShadingPassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
            m_VPLShadingPassData.m_uDisplayCostPerPixel.set(m_bDisplayCostPerPixel);
            m_VPLShadingPassData.m_uUseGateCosFactor.set(m_bUseGateCosFactor);
            m_VPLShadingPassData.m_uUseAttenuation.set(m_bUseAttenuation);

            m_VPLBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
            m_VPLOffsetsCountsBuffer.bindBase(1, GL_SHADER_STORAGE_BUFFER);
            m_VPLIndexBuffer.bindBase(2, GL_SHADER_STORAGE_BUFFER);
            m_GClusterTilesOffsetsBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
            m_GateBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
            m_GateOffsetsCountsBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);
            m_TransfertFlagsBuffer.bindBase(6, GL_SHADER_STORAGE_BUFFER);

            m_ScreenTriangle.render();
        }

        if(m_bDoDirectLightPass && m_DirectLightingPassData.m_DoDirectLightingPass) {
            m_DirectLightingPassData.m_Program.use();
            m_DirectLightingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
            m_DirectLightingPassData.m_uInvProjMatrix.set(rcpProjMatrix);

            m_DirectLightingPassData.m_uLightPosition.set(
                        glm::vec3(m_ViewMatrix * glm::vec4(m_DirectLightingPassData.m_LightPosition, 1.f))
                        );
            m_DirectLightingPassData.m_uLightIntensity.set(m_DirectLightingPassData.m_LightIntensity);
            m_DirectLightingPassData.m_uLightViewMatrix.set(m_DirectLightingPassData.m_LightViewMatrix);
            m_DirectLightingPassData.m_uShadowMapSampler.set(TEXUNIT_TEXCUBEMAP_SHADOWMAP);
            m_DirectLightingPassData.m_uFaceProjectionMatrices.setMatrix4(
                        6, GL_FALSE, glm::value_ptr(m_DirectLightingPassData.m_ShadowMapRenderer.getFaceProjectionMatrices()[0]));
            m_DirectLightingPassData.m_uRcpViewMatrix.set(rcpViewMatrix);
            m_DirectLightingPassData.m_uDepthBias.set(m_DirectLightingPassData.m_fDepthBias);

            m_ScreenTriangle.render();
        }

        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bindedFramebuffer);
    }

    // Final pass

    {
        GLTimer timer(m_BlurPassData.m_TimeElapsedQuery);

        glClear(GL_COLOR_BUFFER_BIT);

        m_BlurPassData.m_Program.use();
        m_BlurPassData.m_uRenderIrradiance.set(m_bRenderIrradiance);
        m_BlurPassData.m_uGammaExponent.set(1.f / m_fGamma);

        m_ScreenTriangle.render();
    }
}

void GLLightGateRenderer7::drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uTileCount.set(m_TileCount);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(dataToDisplay);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);

    m_GClusterTilesOffsetsBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
    m_ClusterVClusterBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);
    m_GClusterToTileBuffer.bindBase(6, GL_SHADER_STORAGE_BUFFER);

    m_ScreenTriangle.render();
}

void GLLightGateRenderer7::drawBuffers(int x, int y, size_t width, size_t height) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLTimer timer(m_DrawBuffersPassData.m_TimeElapsedQuery);

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uTileCount.set(m_TileCount);

    m_GClusterTilesOffsetsBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
    m_ClusterVClusterBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);
    m_GClusterToTileBuffer.bindBase(6, GL_SHADER_STORAGE_BUFFER);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    // Draw normals
    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);
    m_DrawBuffersPassData.m_uDataToDisplay.set(0);

    m_ScreenTriangle.render();

    // Draw depth
    glViewport(x + width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(1);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw VCluster
    glViewport(x + 2 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(2);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw discontinuity buffer
    glViewport(x + 3 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(3);
    m_DrawBuffersPassData.m_uViewport.set(fX + 3 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster buffer
    glViewport(x + 4 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(4);
    m_DrawBuffersPassData.m_uViewport.set(fX + 4 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow lower point depth buffer
    glViewport(x, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(5);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow upper point depth buffer
    glViewport(x + width, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(6);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    glViewport(x + 2 * width, y - height, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(7);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    glViewport(x + 3 * width, y - height, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(8);
    m_DrawBuffersPassData.m_uViewport.set(fX + 3 * fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    glViewport(x + 4 * width, y - height, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(9);
    m_DrawBuffersPassData.m_uViewport.set(fX + 4 * fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();
}

void GLLightGateRenderer7::computeTimings() {
    float scale = 1.f / 1000000;

    m_Timings.geometryPass = m_GeometryPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.discontinuityPass = m_DiscontinuityPass.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.findUniqueClustersPass = m_FindUniqueGeometryClusterPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.countUniqueClustersPass = m_CountGeometryClusterPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.lightAssignementPass = m_LightAssignmentPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.lightTransformPass = m_TransformVPLToViewSpacePassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.shadingPass = m_VPLShadingPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.finalPass = m_BlurPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.drawBuffersPass = m_DrawBuffersPassData.m_TimeElapsedQuery.waitResult() * scale;

    m_Timings.updateTotalTime();
}

void GLLightGateRenderer7::drawPrimaryLightShadowMap(int x, int y, size_t width, size_t height) {
    glViewport(x, y, width, height);
    m_DirectLightingPassData.m_ShadowMapDrawer.drawShadowMap(m_DirectLightingPassData.m_ShadowMapContainer, 0, TEXUNIT_TEXCUBEMAP_SHADOWMAP);
}

void GLLightGateRenderer7::exposeIO(TwBar* bar) {
    TwAddSeparator(bar, "GLLightGateRenderer7::Separator0", "");

    atb::addVarRW(bar, "Do VPL Shading Pass", m_bDoVPLShadingPass, "");
    atb::addVarRW(bar, "Do Gate Shading Pass", m_bDoGateShadingPass, "");
    atb::addVarRW(bar, "Do Direct Light Pass", m_bDoDirectLightPass, "");
    atb::addVarRW(bar, "Depth bias", m_DirectLightingPassData.m_fDepthBias, "");
    atb::addVarRW(bar, "Do Indirect Light Pass", m_bDoIndirectLightPass, "");
    atb::addVarRW(bar, "No Shadow", m_bNoShadow, "");
    atb::addVarRW(bar, "Render irradiance", m_bRenderIrradiance, "");
    atb::addVarRW(bar, "Display cost per pixel", m_bDisplayCostPerPixel, "");
    atb::addVarRW(bar, "Use cos factor", m_bUseGateCosFactor, "");
    atb::addVarRW(bar, "Use attenuation", m_bUseAttenuation, "");
    atb::addVarRO(bar, "Visibility cluster", m_nSelectedVisibilityCluster, "");
    atb::addVarRW(bar, "Gamma", m_fGamma, "");

    TwAddSeparator(bar, "GLLightGateRenderer7::Separator1", "");

    atb::addVarRO(bar, "Geometry Pass", m_Timings.geometryPass, "");
    atb::addVarRO(bar, "Discontinuity Pass", m_Timings.discontinuityPass, "");
    atb::addVarRO(bar, "Find Unique Clusters Pass", m_Timings.findUniqueClustersPass, "");
    atb::addVarRO(bar, "Count Unique Clusters Pass", m_Timings.countUniqueClustersPass, "");
    atb::addVarRO(bar, "Light Assignement Pass", m_Timings.lightAssignementPass, "");
    atb::addVarRO(bar, "Light Transform Pass", m_Timings.lightTransformPass, "");
    atb::addVarRO(bar, "Shading Pass", m_Timings.shadingPass, "");
    atb::addVarRO(bar, "Final Pass", m_Timings.finalPass, "");
    atb::addVarRO(bar, "Draw Buffers Pass", m_Timings.drawBuffersPass, "");
    atb::addVarRO(bar, "Total time", m_Timings.totalTime, "");

    TwAddSeparator(bar, "GLLightGateRenderer7::Separator2", "");
}

}
