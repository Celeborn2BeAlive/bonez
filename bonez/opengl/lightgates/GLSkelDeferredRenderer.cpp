#include "GLSkelDeferredRenderer.hpp"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "bonez/opengl/utils/embree_opengl.hpp"

#include "bonez/common/data/ColorMap.hpp"
#include "bonez/common/Progress.hpp"

namespace BnZ {

using namespace gloops;

const GLchar* GLSkelDeferredRenderer::GeometryPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;

uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat3 uNormalMatrix;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoords;

void main() {
    vPosition = vec3(uMVMatrix * vec4(aPosition, 1));
    vNormal = uNormalMatrix * aNormal;
    vTexCoords = aTexCoords;

    gl_Position = uMVPMatrix * vec4(aPosition, 1);
}

);

const GLchar* GLSkelDeferredRenderer::GeometryPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoords;

struct Material {
    vec3 Kd;
    vec3 Ks;
    float shininess;
    sampler2D KdSampler;
    sampler2D KsSampler;
    sampler2D shininessSampler;
};

uniform Material uMaterial;

uniform float uZFar;

uniform vec3 uCameraPosition;

layout(location = 0) out vec4 fNormalDepth;
layout(location = 1) out vec3 fDiffuse;
layout(location = 2) out vec4 fGlossyShininess;

const float PI = 3.14159265358979323846264;

void main() {
    vec3 nNormal = normalize(vNormal);
    fNormalDepth.xyz = nNormal;
    fNormalDepth.w = -vPosition.z / uZFar; // Normalized depth value

    fDiffuse = uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb / PI;

    float shininess = uMaterial.shininess + texture(uMaterial.shininessSampler, vTexCoords).r;
    fGlossyShininess.rgb = uMaterial.Ks * texture(uMaterial.KsSampler, vTexCoords).rgb * (shininess + 2) / (2 * PI);
    fGlossyShininess.a = shininess;
}

);

GLSkelDeferredRenderer::GeometryPassData::GeometryPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_MaterialUniforms(m_Program),
    m_uMVPMatrix(m_Program, "uMVPMatrix", true),
    m_uMVMatrix(m_Program, "uMVMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uCameraPosition(m_Program, "uCameraPosition", true) {
}

const GLchar* GLSkelDeferredRenderer::ShadingPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vPositionInViewSpace;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 positionInViewSpace = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vPositionInViewSpace = positionInViewSpace.xyz / positionInViewSpace.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLSkelDeferredRenderer::ShadingPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

// The GBuffer:
uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;

// A buffer that contains the VPLs:
uniform samplerBuffer uVPLArray;

uniform isampler3D uSkelClusteringGrid;
uniform mat4 uRcpViewMatrix;
uniform mat3 uRcpNormalMatrix;
uniform mat4 uWorldToGrid;
uniform samplerBuffer uSkelClustering;
uniform isamplerBuffer uSkelClusteringNeighbours;
uniform isamplerBuffer uSkelClusteringOffsetCounts;
uniform float uVoxelSize;

in vec3 vPositionInViewSpace;

out vec3 fFragColor;

int getSkelCluster(vec3 P, vec3 N) {
    vec3 wsp = P + N * uVoxelSize;
    int cluster = -1;
    while(cluster == -1) {
        vec4 gridSpacePosition = uWorldToGrid * vec4(wsp, 1);
        cluster = texelFetch(uSkelClusteringGrid,
                             ivec3(gridSpacePosition.x,
                                   gridSpacePosition.y,
                                   gridSpacePosition.z), 0).r;
        wsp += N * uVoxelSize;
    }
    return cluster;
}

int getSkelCluster(vec3 P) {
    vec4 gridSpacePosition = uWorldToGrid * vec4(P, 1);
    return texelFetch(uSkelClusteringGrid,
                         ivec3(gridSpacePosition.x,
                               gridSpacePosition.y,
                               gridSpacePosition.z), 0).r;
}

bool isSkelVisible(int cluster, vec3 P, vec3 N, vec3 lightPosition, vec3 lightNormal) {
    vec3 lP = (uRcpViewMatrix * vec4(lightPosition, 1)).xyz;
    vec3 lN = normalize(uRcpNormalMatrix * lightNormal);

    int lCluster = getSkelCluster(lP, lN);

    return true;
}

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vPositionInViewSpace;
//    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    vec3 Kd = vec3(1.f / 3.14f);
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);


    vec3 P = (uRcpViewMatrix * vec4(fragPosition, 1)).xyz;
    vec3 N = normalize(uRcpNormalMatrix * normalDepth.xyz);
    int cluster = getSkelCluster(P, N);

    int nbVPLs = textureSize(uVPLArray);

    fFragColor = vec3(0);

    for(int j = 0; j < nbVPLs; ++j) {
        vec3 lightPosition = texelFetch(uVPLArray, j * 3).xyz;
        vec3 lightNormal = texelFetch(uVPLArray, j * 3 + 1).xyz;

        if(isSkelVisible(cluster, P, N, lightPosition, lightNormal)) {
            vec3 lightIntensity = texelFetch(uVPLArray, j * 3 + 2).xyz;

            vec3 wi = lightPosition - fragPosition;
            float dist = length(wi);
            wi /= dist;

            float cos_o = max(0, dot(-wi, lightNormal));

            float G = max(0, dot(wi, normalDepth.xyz)) * cos_o / (dist * dist);

            vec3 r = reflect(-wi, normalDepth.xyz);

            vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
            fFragColor = fFragColor + G * lightIntensity * brdf;
        }
    }
}

);

GLSkelDeferredRenderer::ShadingPassData::ShadingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uVPLArray(m_Program, "uVPLArray", true),
    m_uSkelClusteringGrid(m_Program, "uSkelClusteringGrid", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uRcpNormalMatrix(m_Program, "uRcpNormalMatrix", true),
    m_uWorldToGrid(m_Program, "uWorldToGrid", true),
    m_uSkelClustering(m_Program, "uSkelClustering", true),
    m_uSkelClusteringNeighbours(m_Program, "uSkelClusteringNeighbours", true),
    m_uSkelClusteringOffsetCounts(m_Program, "uSkelClusteringOffsetCounts", true),
    m_uVoxelSize(m_Program, "uVoxelSize", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
}

const GLchar* GLSkelDeferredRenderer::DrawBuffersPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLSkelDeferredRenderer::DrawBuffersPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;
uniform sampler2D uNormalDepthSampler;

uniform int uDataToDisplay; // 0 for normals, 1 for depth, 2 for vclusters

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    if(uDataToDisplay == 0) {
        fFragColor = texture(uNormalDepthSampler, texCoords).rgb;
    } else if(uDataToDisplay == 1) {
        fFragColor = vec3(texture(uNormalDepthSampler, texCoords).a);
    }
}

);

GLSkelDeferredRenderer::DrawBuffersPassData::DrawBuffersPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uNormalDepthSampler(m_Program, "uNormalDepthSampler", true),
    m_uDataToDisplay(m_Program, "uDataToDisplay", true) {
}

GLSkelDeferredRenderer::GLSkelDeferredRenderer():
    m_ProjectionMatrix(1.f), m_ViewMatrix(1.f) {
}

void GLSkelDeferredRenderer::setResolution(size_t width, size_t height) {
    {
        GLTexFormat formats[GBUFFER_COUNT] = {
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT }
        };
        m_GBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }
}

void GLSkelDeferredRenderer::geometryPass(const GLScene& scene) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);

    m_GeometryPassData.m_Program.use();

    m_GBuffer.bindForDrawing();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 MVPMatrix = m_ProjectionMatrix * m_ViewMatrix;
    glm::mat4 MVMatrix = m_ViewMatrix;
    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);
    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

    m_GeometryPassData.m_uMVPMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVPMatrix));
    m_GeometryPassData.m_uMVMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVMatrix));
    m_GeometryPassData.m_uNormalMatrix.setMatrix3(1, GL_FALSE, glm::value_ptr(NormalMatrix));
    m_GeometryPassData.m_uZFar.set(getZFar());
    m_GeometryPassData.m_uCameraPosition.set(rcpViewMatrix[3].x, rcpViewMatrix[3].y, rcpViewMatrix[3].z);

    GLMaterialManager::TextureUnits units(MAT_DIFFUSE_TEXUNIT, MAT_GLOSSY_TEXUNIT, MAT_SHININESS_TEXUNIT);

    scene.render(m_GeometryPassData.m_MaterialUniforms, units);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLSkelDeferredRenderer::sendSkelClustering(const Scene& scene) {
    const CurvilinearSkeleton::GridType& grid = scene.voxelSpace->getClustering().getGrid();

    Vec3ui size(grid.width(), grid.height(), grid.depth());

    Grid3D<int> clusterGrid(grid.width(), grid.height(), grid.depth());
    size_t totalSize = grid.width() * grid.height() * grid.depth();

    for(uint32_t i = 0; i < totalSize; ++i) {
        GraphNodeIndex node = grid.data()[i];
        if(UNDEFINED_NODE == node) {
            clusterGrid[i] = -1;
        } else {
            clusterGrid[i] = node;
        }
    }

    glActiveTexture(GL_TEXTURE0 + TEX3D_SKELCLUSTERINGGRID_TEXUNIT);
    glBindTexture(GL_TEXTURE_3D, m_SkelClusteringGrid.glId());
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32I, grid.width(), grid.height(), grid.depth(), 0,
                 GL_RED_INTEGER, GL_INT, clusterGrid.data());

    std::vector<glm::vec4> skelClusteringData;
    std::vector<int> skelClusteringNeighboursData;
    std::vector<glm::ivec2> skelClusteringOffsetCountsData;

    int offset = 0;
    for(auto i: range(scene.voxelSpace->getClustering().size())) {
        const auto& node = scene.voxelSpace->getClustering().getNode(i);
        glm::vec4 data(node.P.x, node.P.y, node.P.z, node.maxball);
        skelClusteringData.emplace_back(data);

        for(auto j: scene.voxelSpace->getClustering().neighbours(i)) {
            skelClusteringNeighboursData.emplace_back(j);
        }
        int count = scene.voxelSpace->getClustering().neighbours(i).size();
        skelClusteringOffsetCountsData.emplace_back(glm::ivec2(offset, count));
        offset += count;
    }

    glActiveTexture(GL_TEXTURE0 + TEXBUFFER_SKELCLUSTERING_TEXUNIT);
    m_SkelClusteringBuffer.setData(skelClusteringData.size(),
                                   skelClusteringData.data(),
                                   GL_RGBA32F);

    glActiveTexture(GL_TEXTURE0 + TEXBUFFER_SKELCLUSTERINGNEIGHBOURS_TEXUNIT);
    m_SkelClusteringNeighbours.setData(skelClusteringNeighboursData.size(),
                                       skelClusteringNeighboursData.data(),
                                       GL_R32I);

    glActiveTexture(GL_TEXTURE0 + TEXBUFFER_SKELCLUSTERINGOFFSETCOUNTS_TEXUNIT);
    m_SkelClusteringOffsetCounts.setData(skelClusteringOffsetCountsData.size(),
                                         skelClusteringOffsetCountsData.data(),
                                         GL_RG32I);

    glActiveTexture(GL_TEXTURE0);

    m_ShadingPassData.m_Program.use();
    m_ShadingPassData.m_uSkelClusteringGrid.set(TEX3D_SKELCLUSTERINGGRID_TEXUNIT);
    m_ShadingPassData.m_uWorldToGrid.set(convert(scene.voxelSpace->getWorldToLocalTransform()));
    m_ShadingPassData.m_uVoxelSize.set(scene.voxelSpace->getVoxelSize());
    m_ShadingPassData.m_uSkelClustering.set(TEXBUFFER_SKELCLUSTERING_TEXUNIT);
    m_ShadingPassData.m_uSkelClusteringNeighbours.set(TEXBUFFER_SKELCLUSTERINGNEIGHBOURS_TEXUNIT);
    m_ShadingPassData.m_uSkelClusteringOffsetCounts.set(TEXBUFFER_SKELCLUSTERINGOFFSETCOUNTS_TEXUNIT);
}

void GLSkelDeferredRenderer::setup() {
    glActiveTexture(GL_TEXTURE0 + GBUFFER_NORMALDEPTH_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).glId());

    glActiveTexture(GL_TEXTURE0 + GBUFFER_DIFFUSE_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_DIFFUSE).glId());

    glActiveTexture(GL_TEXTURE0 + GBUFFER_GLOSSYSHININESS_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).glId());

    glActiveTexture(GL_TEXTURE0 + TEX3D_SKELCLUSTERINGGRID_TEXUNIT);
    glBindTexture(GL_TEXTURE_3D, m_SkelClusteringGrid.glId());

    m_VPLBuffer.bind(TEXBUFFER_VPLARRAY_TEXUNIT);
    m_SkelClusteringBuffer.bind(TEXBUFFER_SKELCLUSTERING_TEXUNIT);
    m_SkelClusteringNeighbours.bind(TEXBUFFER_SKELCLUSTERINGNEIGHBOURS_TEXUNIT);
    m_SkelClusteringOffsetCounts.bind(TEXBUFFER_SKELCLUSTERINGOFFSETCOUNTS_TEXUNIT);

    GLOOPS_CHECK_ERROR;

    m_ShadingPassData.m_Program.use();
    m_ShadingPassData.m_uSamplers[GBUFFER_NORMAL_DEPTH].set(GBUFFER_NORMALDEPTH_TEXUNIT);
    m_ShadingPassData.m_uSamplers[GBUFFER_DIFFUSE].set(GBUFFER_DIFFUSE_TEXUNIT);
    m_ShadingPassData.m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(GBUFFER_GLOSSYSHININESS_TEXUNIT);
    m_ShadingPassData.m_uVPLArray.set(TEXBUFFER_VPLARRAY_TEXUNIT);

    m_DrawBuffersPassData.m_Program.use();
    m_DrawBuffersPassData.m_uNormalDepthSampler.set(GBUFFER_NORMALDEPTH_TEXUNIT);

    glActiveTexture(GL_TEXTURE0);
}

void GLSkelDeferredRenderer::sendVPLs(const VPLContainer& vpls, const Scene& scene) {
    // On Transforme les coordonnées des VPLS pour les avoir dans l'espace
    // caméra dans le shader
    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));
    std::vector<float> values(3 * 3 * vpls.orientedVPLs.size());
    uint32_t offset = 0;
    for(const auto& vpl: vpls.orientedVPLs) {
        glm::vec4 lightPosition = m_ViewMatrix * glm::vec4(convert(vpl.P), 1);
        glm::vec3 lightNormal = NormalMatrix * convert(vpl.N);

        values[offset + 0] = lightPosition.x;
        values[offset + 1] = lightPosition.y;
        values[offset + 2] = lightPosition.z;
        values[offset + 3] = lightNormal.x;
        values[offset + 4] = lightNormal.y;
        values[offset + 5] = lightNormal.z;
        values[offset + 6] = vpl.L.r;
        values[offset + 7] = vpl.L.g;
        values[offset + 8] = vpl.L.b;

        offset += 9;
    }

    // On envoit les VPLs au GPU
    m_VPLBuffer.bind(TEXBUFFER_VPLARRAY_TEXUNIT);
    m_VPLBuffer.setData(values.size(), values.data(), GL_RGB32F);
}

void GLSkelDeferredRenderer::shadingPass(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls, const Scene& scene) {

    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);
    glm::mat3 rcpNormalMatrix = glm::transpose(glm::mat3(m_ViewMatrix));

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::CapabilityRestorer blending(GL_BLEND, true);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    glViewport(x, y, width, height);

    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);

    glClear(GL_COLOR_BUFFER_BIT);

    // VPL Shading pass

    sendVPLs(vpls, scene);

    m_ShadingPassData.m_Program.use();
    m_ShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
    m_ShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);
    m_ShadingPassData.m_uRcpViewMatrix.set(rcpViewMatrix);
    m_ShadingPassData.m_uRcpNormalMatrix.set(rcpNormalMatrix);

    m_ScreenTriangle.render();
}

void GLSkelDeferredRenderer::drawBuffers(int x, int y, size_t width, size_t height) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    // Draw normals
    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);
    m_DrawBuffersPassData.m_uDataToDisplay.set(0);

    m_ScreenTriangle.render();

    // Draw depth
    glViewport(x + width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(1);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY, fW, fH);

    m_ScreenTriangle.render();
}

}
