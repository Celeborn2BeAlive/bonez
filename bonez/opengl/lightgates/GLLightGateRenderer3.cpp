#include "GLLightGateRenderer3.hpp"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "bonez/opengl/utils/embree_opengl.hpp"

#include "bonez/common/data/ColorMap.hpp"
#include "bonez/common/Progress.hpp"

namespace BnZ {

using namespace gloops;

const GLchar* GLLightGateRenderer3::GeometryPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;

uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat3 uNormalMatrix;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoords;
out vec3 vWorldSpaceNormal;
out vec3 vWorldSpacePosition;

void main() {
    vWorldSpacePosition = aPosition;
    vWorldSpaceNormal = aNormal;
    vPosition = vec3(uMVMatrix * vec4(aPosition, 1));
    vNormal = uNormalMatrix * aNormal;
    vTexCoords = aTexCoords;

    gl_Position = uMVPMatrix * vec4(aPosition, 1);
}

);

const GLchar* GLLightGateRenderer3::GeometryPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoords;
in vec3 vWorldSpacePosition;
in vec3 vWorldSpaceNormal;

struct Material {
    vec3 Kd;
    vec3 Ks;
    float shininess;
    sampler2D KdSampler;
    sampler2D KsSampler;
    sampler2D shininessSampler;
};

uniform Material uMaterial;

uniform float uZFar;

uniform isampler3D uVClusterGrid;
uniform float uVoxelSize;
uniform mat4 uWorldToGrid;

uniform vec3 uCameraPosition;

layout(location = 0) out vec4 fNormalDepth;
layout(location = 1) out vec3 fDiffuse;
layout(location = 2) out vec4 fGlossyShininess;
layout(location = 3) out int fVClusterIndex;

const float PI = 3.14159265358979323846264;

void main() {
    vec3 nNormal = normalize(vNormal);
    fNormalDepth.xyz = nNormal;
    fNormalDepth.w = -vPosition.z / uZFar; // Normalized depth value

    fDiffuse = uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb / PI;

    float shininess = uMaterial.shininess + texture(uMaterial.shininessSampler, vTexCoords).r;
    fGlossyShininess.rgb = uMaterial.Ks * texture(uMaterial.KsSampler, vTexCoords).rgb * (shininess + 2) / (2 * PI);
    fGlossyShininess.a = shininess;

    //vec3 wi = normalize(uCameraPosition - vWorldSpacePosition);
    vec3 wi = normalize(vWorldSpaceNormal);

    vec3 wsp = vWorldSpacePosition + wi * uVoxelSize;
    fVClusterIndex = -1;
    while(fVClusterIndex == -1) {
        vec4 gridSpacePosition = uWorldToGrid * vec4(wsp, 1);
        fVClusterIndex = texelFetch(uVClusterGrid, ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z), 0).r;
        wsp += wi * uVoxelSize;
    }
}

);

GLLightGateRenderer3::GeometryPassData::GeometryPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_MaterialUniforms(m_Program),
    m_uMVPMatrix(m_Program, "uMVPMatrix", true),
    m_uMVMatrix(m_Program, "uMVMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uVClusterGrid(m_Program, "uVClusterGrid", true),
    m_uWorldToGrid(m_Program, "uWorldToGrid", true),
    m_uVoxelSize(m_Program, "uVoxelSize", true),
    m_uCameraPosition(m_Program, "uCameraPosition", true) {
}

const GLchar* GLLightGateRenderer3::ShadingPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vPositionInViewSpace;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 positionInViewSpace = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vPositionInViewSpace = positionInViewSpace.xyz / positionInViewSpace.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer3::ShadingPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

// The GBuffer:
uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;

// A buffer that contains the VPLs:
uniform samplerBuffer uVPLArray;
// A buffer that contains for each cluster the offset and the number of VPLs in that cluster:
uniform isamplerBuffer uVPLOffsetsCounts;

// A buffer that contains all the gates
uniform samplerBuffer uGateArray;
// A buffer that contains in position i * nbCluster + k the offset and the number of
// gates that go from cluster i to cluster k
uniform isamplerBuffer uGateOffsetsCounts;

in vec3 vPositionInViewSpace;

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vPositionInViewSpace;
//    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    vec3 Kd = vec3(1.f / 3.14f);
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    int visibilityCluster = texture(uVisibilityClusterSampler, texCoords).r;

    if(visibilityCluster < 0) {
        fFragColor = vec3(0, 0, 0);
        return;
    }

    int nbClusters = textureSize(uVPLOffsetsCounts);

    fFragColor = vec3(0, 0, 0);

    // Process the VPLs of each cluster
    for(int i = 0; i < nbClusters; ++i) {
        int index = i * nbClusters + visibilityCluster;

        int nbGatesFromI = texelFetch(uGateOffsetsCounts, index).y;
        int offsetGatesFromI = texelFetch(uGateOffsetsCounts, index).x;

        float gateAttenuationFactor = 0;

        if(i != visibilityCluster) {
            // Compute the attenuation factor from i to visibilityCluster for this pixel
            int end = offsetGatesFromI + nbGatesFromI;
            for(int j = offsetGatesFromI; j < end; ++j) {
                vec3 gatePosition = texelFetch(uGateArray, j * 3).xyz;
                vec3 gateNormal = texelFetch(uGateArray, j * 3 + 1).xyz;
                float gateIntensity = length(texelFetch(uGateArray, j * 3 + 2).xyz);

                vec3 wi = gatePosition - fragPosition;
                float dist = length(wi);
                wi /= dist;
                float cos_o = abs(dot(-wi, gateNormal));
                float G = max(0, dot(wi, normalDepth.xyz)) * cos_o * cos_o * cos_o / (dist * dist);
                gateAttenuationFactor = gateAttenuationFactor + gateIntensity * G;
            }
        } else {
            gateAttenuationFactor = 1;
        }

        if(gateAttenuationFactor > 0.f) {
            vec3 color = vec3(0.f);
            ivec2 offsetCount = texelFetch(uVPLOffsetsCounts, i).xy;
            int end = offsetCount.x + offsetCount.y;

            for(int j = offsetCount.x; j < end; ++j) {
                vec3 lightPosition = texelFetch(uVPLArray, j * 3).xyz;
                vec3 lightNormal = texelFetch(uVPLArray, j * 3 + 1).xyz;
                vec3 lightIntensity = texelFetch(uVPLArray, j * 3 + 2).xyz;

                vec3 wi = lightPosition - fragPosition;
                float dist = length(wi);
                wi /= dist;

                float cos_o = max(0, dot(-wi, lightNormal));

                float G = max(0, dot(wi, normalDepth.xyz)) * cos_o / (dist * dist);

                vec3 r = reflect(-wi, normalDepth.xyz);

                vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
                color = color + G * lightIntensity/* * brdf*/;
            }

            fFragColor = fFragColor + gateAttenuationFactor * color;
        }
    }
}

);

GLLightGateRenderer3::ShadingPassData::ShadingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uVPLArray(m_Program, "uVPLArray", true),
    m_uVPLOffsetsCounts(m_Program, "uVPLOffsetsCounts", true),
    m_uGateArray(m_Program, "uGateArray", true),
    m_uGateOffsetsCounts(m_Program, "uGateOffsetsCounts", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTERINDEX] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);
}

const GLchar* GLLightGateRenderer3::DrawBuffersPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer3::DrawBuffersPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;
uniform sampler2D uNormalDepthSampler;
uniform isampler2D uVisibilityClusterSampler;
uniform isampler2D uDiscontinuitySampler;

uniform samplerBuffer uVClusterColors;

uniform int uDataToDisplay; // 0 for normals, 1 for depth, 2 for vclusters

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    if(uDataToDisplay == 0) {
        fFragColor = texture(uNormalDepthSampler, texCoords).rgb;
    } else if(uDataToDisplay == 1) {
        fFragColor = vec3(texture(uNormalDepthSampler, texCoords).a);
    } else if(uDataToDisplay == 2) {
        int visibilityCluster = texture(uVisibilityClusterSampler, texCoords).r;

        if(visibilityCluster < 0) {
            fFragColor = vec3(0, 0, 0);
        } else {
            fFragColor = texelFetch(uVClusterColors, visibilityCluster).rgb;
        }
        int discontinuity = texture(uDiscontinuitySampler, texCoords).r;
        if(discontinuity == 1) {
            fFragColor = vec3(0, 0, 0);
        }
    } else {
        int discontinuity = texture(uDiscontinuitySampler, texCoords).r;
        if(discontinuity == 1) {
            fFragColor = vec3(1, 1, 1);
        } else {
            fFragColor = vec3(0, 0, 0);
        }
    }
}

);

GLLightGateRenderer3::DrawBuffersPassData::DrawBuffersPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uNormalDepthSampler(m_Program, "uNormalDepthSampler", true),
    m_uVisibilityClusterSampler(m_Program, "uVisibilityClusterSampler", true),
    m_uDiscontinuitySampler(m_Program, "uDiscontinuitySampler", true),
    m_uDataToDisplay(m_Program, "uDataToDisplay", true),
    m_uVClusterColors(m_Program, "uVClusterColors", true) {
}

const GLchar* GLLightGateRenderer3::DiscontinuityPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer3::DiscontinuityPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;
uniform sampler2D uNormalDepthSampler;
uniform isampler2D uVisibilityClusterSampler;

uniform float uZFar;

out int fFragColor;

void main() {
    int x = int(gl_FragCoord.x);
    int y = int(gl_FragCoord.y);

    vec3 N = texelFetch(uNormalDepthSampler, ivec2(x, y), 0).rgb;
    vec3 Nr = texelFetch(uNormalDepthSampler, ivec2(x + 1, y), 0).rgb;
    vec3 Nb = texelFetch(uNormalDepthSampler, ivec2(x, y + 1), 0).rgb;
    vec3 Nrb = texelFetch(uNormalDepthSampler,ivec2(x + 1, y + 1), 0).rgb;

    float d1 = (dot(N, Nr) + dot(N, Nb) + dot(N, Nrb)) / 3.f;

    float d = texelFetch(uNormalDepthSampler, ivec2(x, y), 0).a;
    float dr = texelFetch(uNormalDepthSampler, ivec2(x + 1, y), 0).a;
    float db = texelFetch(uNormalDepthSampler, ivec2(x, y + 1), 0).a;
    float drb = texelFetch(uNormalDepthSampler,ivec2(x + 1, y + 1), 0).a;

    float meanDepth = (d + dr + db + drb) * 0.25f;
    float d2 = d / meanDepth;

    bool isGeometryDiscontinuous = d1 < 0.9 || d2 < 0.97 || d2 > 1.03;

    int C = texelFetch(uVisibilityClusterSampler, ivec2(x, y), 0).r;
    int Cr = texelFetch(uVisibilityClusterSampler, ivec2(x + 1, y), 0).r;
    int Cb = texelFetch(uVisibilityClusterSampler, ivec2(x, y + 1), 0).r;
    int Crb = texelFetch(uVisibilityClusterSampler, ivec2(x + 1, y + 1), 0).r;

    bool isClusteringDiscontinuous = C != Cr || C != Cb || C != Crb;

    fFragColor = int(/*isClusteringDiscontinuous && !*/isGeometryDiscontinuous);
}

);

GLLightGateRenderer3::DiscontinuityPassData::DiscontinuityPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uZFar(m_Program, "uZFar", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTERINDEX] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);
}


const GLchar* GLLightGateRenderer3::BlurPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLLightGateRenderer3::BlurPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;
uniform isampler2D uVisibilityClusterSampler;

uniform sampler2D uIrradianceBuffer;
uniform isampler2D uDiscontinuityBuffer;

out vec3 fFragColor;

void main() {
    int x = int(gl_FragCoord.x);
    int y = int(gl_FragCoord.y);

    bool isDiscontinuous = false;
    for(int i = -1; i <= 1; ++i) {
        for(int j = -1; j <= 1; ++j) {
            if(1 == texelFetch(uDiscontinuityBuffer, ivec2(x + i, y + j), 0).r) {
                isDiscontinuous = true;
            }
        }
    }

    int discontinuity = texelFetch(uDiscontinuityBuffer, ivec2(x, y), 0).r;
    vec3 irradiance = texelFetch(uIrradianceBuffer, ivec2(x, y), 0).rgb;
/*
    if(isDiscontinuous) {
        for(int i = -1; i <= 1; ++i) {
            for(int j = -1; j <= 1; ++j) {
                if(i != 0 || j != 0) {
                    irradiance += texelFetch(uIrradianceBuffer, ivec2(x + i, y + j), 0).rgb;
                }
            }
        }
        irradiance /= 9;
    }*/

//    vec3 Kd = texelFetch(uDiffuseSampler, ivec2(x, y), 0).rgb;
    vec3 Kd = vec3(1.f / 3.14f);
    fFragColor = Kd * irradiance;
}

);

GLLightGateRenderer3::BlurPassData::BlurPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uIrradianceBuffer(m_Program, "uIrradianceBuffer", true),
    m_uDiscontinuityBuffer(m_Program, "uDiscontinuityBuffer", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
    m_uSamplers[GBUFFER_VCLUSTERINDEX] = gloops::Uniform(m_Program, "uVisibilityClusterSampler", true);
}

GLLightGateRenderer3::GLLightGateRenderer3():
    m_ProjectionMatrix(1.f), m_ViewMatrix(1.f),
    m_bDoVPLShadingPass(true), m_bDoGateShadingPass(true), m_nSamplePerGateCount(4) {
}

void GLLightGateRenderer3::setResolution(size_t width, size_t height) {
    {
        GLTexFormat formats[GBUFFER_COUNT] = {
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32I, GL_RGBA_INTEGER, GL_INT }
        };
        m_GBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }

    {
        GLTexFormat formats[1] = {
            { GL_R8UI, GL_RED_INTEGER, GL_UNSIGNED_BYTE }
        };
//        GLTexFormat formats[1] = {
//            { GL_RGBA32F, GL_RGBA, GL_FLOAT }
//        };
        m_DiscontinuityBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }

    {
        m_IrradianceBuffer.init(width, height);
    }
}

void GLLightGateRenderer3::computeGeometryDiscontinuityBuffer() {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DiscontinuityPass.m_Program.use();
    m_DiscontinuityBuffer.bindForDrawing();

    m_DiscontinuityPass.m_uViewport.set(glm::vec4(0, 0,
                                                  m_DiscontinuityBuffer.getWidth(),
                                                  m_DiscontinuityBuffer.getHeight()));

    m_ScreenTriangle.render();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLLightGateRenderer3::geometryPass(const GLScene& scene) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);

    m_GeometryPassData.m_Program.use();

    m_GBuffer.bindForDrawing();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 MVPMatrix = m_ProjectionMatrix * m_ViewMatrix;
    glm::mat4 MVMatrix = m_ViewMatrix;
    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);
    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

    m_GeometryPassData.m_uMVPMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVPMatrix));
    m_GeometryPassData.m_uMVMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVMatrix));
    m_GeometryPassData.m_uNormalMatrix.setMatrix3(1, GL_FALSE, glm::value_ptr(NormalMatrix));
    m_GeometryPassData.m_uZFar.set(getZFar());
    m_GeometryPassData.m_uCameraPosition.set(rcpViewMatrix[3].x, rcpViewMatrix[3].y, rcpViewMatrix[3].z);

    GLMaterialManager::TextureUnits units(MAT_DIFFUSE_TEXUNIT, MAT_GLOSSY_TEXUNIT, MAT_SHININESS_TEXUNIT);

    scene.render(m_GeometryPassData.m_MaterialUniforms, units);

    computeGeometryDiscontinuityBuffer();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLLightGateRenderer3::sendVisibilityClusteringGrid(const Scene& scene,
                                                       const VisibilityClustering& vclustering) {
    const CurvilinearSkeleton::GridType& grid = scene.voxelSpace->getClustering().getGrid();

    Vec3ui size(grid.width(), grid.height(), grid.depth());

    Grid3D<int> clusterGrid(grid.width(), grid.height(), grid.depth());
    size_t totalSize = grid.width() * grid.height() * grid.depth();

    for(uint32_t i = 0; i < totalSize; ++i) {
        GraphNodeIndex node = grid.data()[i];
        if(UNDEFINED_NODE == node) {
            clusterGrid[i] = -1;
        } else {
            clusterGrid[i] = vclustering.getClusterOf(node);
        }
    }

    glActiveTexture(GL_TEXTURE0 + TEX3D_VCLUSTERGRID_TEXUNIT);
    glBindTexture(GL_TEXTURE_3D, m_VClusterGridTexture.glId());
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32I, grid.width(), grid.height(), grid.depth(), 0,
                 GL_RED_INTEGER, GL_INT, clusterGrid.data());

    glActiveTexture(GL_TEXTURE0);

    m_GeometryPassData.m_Program.use();
    m_GeometryPassData.m_uVClusterGrid.set(int(TEX3D_VCLUSTERGRID_TEXUNIT));
    m_GeometryPassData.m_uWorldToGrid.set(convert(scene.voxelSpace->getWorldToLocalTransform()));
    m_GeometryPassData.m_uVoxelSize.set(scene.voxelSpace->getVoxelSize());
}

void GLLightGateRenderer3::setup() {
    glActiveTexture(GL_TEXTURE0 + GBUFFER_NORMALDEPTH_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).glId());

    glActiveTexture(GL_TEXTURE0 + GBUFFER_DIFFUSE_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_DIFFUSE).glId());

    glActiveTexture(GL_TEXTURE0 + GBUFFER_GLOSSYSHININESS_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).glId());

    glActiveTexture(GL_TEXTURE0 + GBUFFER_VCLUSTER_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_VCLUSTERINDEX).glId());

    glActiveTexture(GL_TEXTURE0 + TEX3D_VCLUSTERGRID_TEXUNIT);
    glBindTexture(GL_TEXTURE_3D, m_VClusterGridTexture.glId());

    glActiveTexture(GL_TEXTURE0 + TEX2D_DISCONTINUITYBUFFER_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_DiscontinuityBuffer.getColorBuffer(0).glId());

    glActiveTexture(GL_TEXTURE0 + TEX2D_IRRADIANCEBUFFER_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_IrradianceBuffer.getColorBuffer(0).glId());

    GLOOPS_CHECK_ERROR;

    m_ShadingPassData.m_Program.use();
    m_ShadingPassData.m_uSamplers[GBUFFER_NORMAL_DEPTH].set(GBUFFER_NORMALDEPTH_TEXUNIT);
    m_ShadingPassData.m_uSamplers[GBUFFER_DIFFUSE].set(GBUFFER_DIFFUSE_TEXUNIT);
    m_ShadingPassData.m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(GBUFFER_GLOSSYSHININESS_TEXUNIT);
    m_ShadingPassData.m_uSamplers[GBUFFER_VCLUSTERINDEX].set(GBUFFER_VCLUSTER_TEXUNIT);
    m_ShadingPassData.m_uVPLArray.set(TEXBUFFER_VPLARRAY_TEXUNIT);
    m_ShadingPassData.m_uVPLOffsetsCounts.set(TEXBUFFER_VPLOFFSETSCOUNTS_TEXUNIT);
    m_ShadingPassData.m_uGateArray.set(TEXBUFFER_GATEARRAY_TEXUNIT);
    m_ShadingPassData.m_uGateOffsetsCounts.set(TEXBUFFER_GATEOFFSETSCOUNTS_TEXUNIT);

    m_DrawBuffersPassData.m_Program.use();
    m_DrawBuffersPassData.m_uNormalDepthSampler.set(GBUFFER_NORMALDEPTH_TEXUNIT);
    m_DrawBuffersPassData.m_uVisibilityClusterSampler.set(GBUFFER_VCLUSTER_TEXUNIT);
    m_DrawBuffersPassData.m_uDiscontinuitySampler.set(TEX2D_DISCONTINUITYBUFFER_TEXUNIT);
    m_DrawBuffersPassData.m_uVClusterColors.set(TEXBUFFER_VCLUSTERCOLORS_TEXUNIT);

    m_DiscontinuityPass.m_Program.use();
    m_DiscontinuityPass.m_uSamplers[GBUFFER_NORMAL_DEPTH].set(GBUFFER_NORMALDEPTH_TEXUNIT);
    m_DiscontinuityPass.m_uSamplers[GBUFFER_DIFFUSE].set(GBUFFER_DIFFUSE_TEXUNIT);
    m_DiscontinuityPass.m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(GBUFFER_GLOSSYSHININESS_TEXUNIT);
    m_DiscontinuityPass.m_uSamplers[GBUFFER_VCLUSTERINDEX].set(GBUFFER_VCLUSTER_TEXUNIT);
    m_DiscontinuityPass.m_uZFar.set(getZFar());

    m_BlurPassData.m_Program.use();
    m_BlurPassData.m_uSamplers[GBUFFER_NORMAL_DEPTH].set(GBUFFER_NORMALDEPTH_TEXUNIT);
    m_BlurPassData.m_uSamplers[GBUFFER_DIFFUSE].set(GBUFFER_DIFFUSE_TEXUNIT);
    m_BlurPassData.m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(GBUFFER_GLOSSYSHININESS_TEXUNIT);
    m_BlurPassData.m_uSamplers[GBUFFER_VCLUSTERINDEX].set(GBUFFER_VCLUSTER_TEXUNIT);
    m_BlurPassData.m_uDiscontinuityBuffer.set(TEX2D_DISCONTINUITYBUFFER_TEXUNIT);
    m_BlurPassData.m_uIrradianceBuffer.set(TEX2D_IRRADIANCEBUFFER_TEXUNIT);

    glActiveTexture(GL_TEXTURE0);
}

struct GateSample {
    Vec3f P;
    Vec3f N;
    Col3f E; // Irradiance affecting the sample
};

void GLLightGateRenderer3::sendVPLs(const VPLContainer& vpls, const Scene& scene,
                                   const VisibilityClustering& vclustering) {
    m_ClusterVPLs.clear();
    m_ClusterVPLs.resize(vclustering.size());

    // Ce tableau indique pour chaque cluster si c'est un cluster de transfert
    // Un cluster de transfert n'a que des noeuds de degré 2
    std::vector<bool> isTransfertCluster(vclustering.size(), true);

    // Regroupe les VPLs par cluster de visibilité
    for(const auto& vpl: index(vpls.orientedVPLs)) {
        GraphNodeIndex idx = scene.voxelSpace->getClustering().getNearestNode(vpl.second.P, vpl.second.N);
        if(idx == UNDEFINED_NODE) {
            continue;
        }
        int clusterIdx = vclustering.getClusterOf(idx);
        if(clusterIdx < 0) {
            continue;
        }
        m_ClusterVPLs[clusterIdx].push_back(vpl.first);
    }

    // On stocke les VPLs dans un tableau, regroupés par cluster pour un traitement
    // plus facile par le GPU
    VPLContainer sorted;
    /*
    std::vector<int> vclusterVPLOffsets; // Pour chaque cluster, l'offset de son premier VPL
    std::vector<int> vclusterVPLCounts; // Pour chaque cluster le nombre de VPLs associés*/

    std::vector<glm::ivec2> vplOffsetsCounts;

    int offset = 0;
    for(uint32_t vcluster: range(vclustering.size())) {
        glm::ivec2 offsetCount(offset, 0);
        //vclusterVPLOffsets.push_back(offset);

        // On vérifie si le cluster est un cluster de transfert
        for(auto node: vclustering[vcluster]) {
            if(scene.voxelSpace->getClustering().neighbours(node).size() != 2) {
                isTransfertCluster[vcluster] = false;
                break;
            }
        }

        //size_t count = 0;

//        // Tous les VPLs
//        offsetCount.y += vpls.orientedVPLs.size();
//        for(const auto& vpl: vpls.orientedVPLs) {
//            sorted.addVPL(vpl);
//        }

        // On ajoute les VPLs du cluster
        offsetCount.y += m_ClusterVPLs[vcluster].size();
        for(uint32_t vplIdx: m_ClusterVPLs[vcluster]) {
            sorted.addVPL(vpls.orientedVPLs[vplIdx]);
        }

        /*
        if(!isTransfertCluster[vcluster]) {
            // Si le cluster n'est pas un cluster de transfert, on ajoute les VPLs
            // des clusters de transfert voisins
            for(const auto& neighbourLink: vclustering[vcluster].getNeighbourLinks()) {
                if(isTransfertCluster[neighbourLink.neighbour]) {
                    count += m_ClusterVPLs[neighbourLink.neighbour].size();
                    for(uint32_t vplIdx: m_ClusterVPLs[neighbourLink.neighbour]) {
                        sorted.addVPL(vpls.orientedVPLs[vplIdx]);
                    }
                }
            }
        } else {*/
            // Si le cluster est un cluster de transfert on ajoute les VPLs de TOUS
            // les clusters voisins
/*
            for(const auto& neighbourLink: vclustering[vcluster].getNeighbourLinks()) {
                offsetCount.y += m_ClusterVPLs[neighbourLink.neighbour].size();
                for(uint32_t vplIdx: m_ClusterVPLs[neighbourLink.neighbour]) {
                    sorted.addVPL(vpls.orientedVPLs[vplIdx]);
                }
            }
*/
        //}

        //vclusterVPLCounts.push_back(count);
        vplOffsetsCounts.emplace_back(offsetCount);
        offset += offsetCount.y;
    }

    // On Transforme les coordonnées des VPLS pour les avoir dans l'espace
    // caméra dans le shader
    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));
    std::vector<float> values(3 * 3 * sorted.orientedVPLs.size());
    for(const auto& vplIndex: index(sorted.orientedVPLs)) {
        const OrientedVPL& vpl = vplIndex.second;

        uint32_t offset = vplIndex.first * 9;

        glm::vec4 lightPosition = m_ViewMatrix * glm::vec4(convert(vpl.P), 1);
        glm::vec3 lightNormal = NormalMatrix * convert(vpl.N);

        values[offset + 0] = lightPosition.x;
        values[offset + 1] = lightPosition.y;
        values[offset + 2] = lightPosition.z;
        values[offset + 3] = lightNormal.x;
        values[offset + 4] = lightNormal.y;
        values[offset + 5] = lightNormal.z;
        values[offset + 6] = vpl.L.r;
        values[offset + 7] = vpl.L.g;
        values[offset + 8] = vpl.L.b;
    }

    // On envoit les VPLs au GPU
    m_VPLBuffer.bind(TEXBUFFER_VPLARRAY_TEXUNIT);
    m_VPLBuffer.setData(values.size(), values.data(), GL_RGB32F);

    m_VPLOffsetsCountsBuffer.bind(TEXBUFFER_VPLOFFSETSCOUNTS_TEXUNIT);
    m_VPLOffsetsCountsBuffer.setData(vplOffsetsCounts.size(), vplOffsetsCounts.data(), GL_RG32I);
}

void GLLightGateRenderer3::sendGates(const VPLContainer& vpls, const Scene& scene,
                                    const VisibilityClustering& vclustering) {
    // Calcul des samples sur les gates
    //std::vector<std::vector<GateSample>> gateSamples(vclustering.size());
    Random rng(0);
    size_t Nsample = m_nSamplePerGateCount; // 4 sample par gate

    std::vector<bool> isTransfertCluster(vclustering.size(), true);
    for(uint32_t vcluster: range(vclustering.size())) {
        // On vérifie si le cluster est un cluster de transfert
        for(auto node: vclustering[vcluster]) {
            if(scene.voxelSpace->getClustering().neighbours(node).size() != 2) {
                isTransfertCluster[vcluster] = false;
                break;
            }
        }
    }

    std::vector<GateSample> sorted;
    std::vector<glm::ivec2> gateOffsetsCounts;

    uint32_t offset = 0;

    for(const auto& cluster: index(vclustering)) {

        //gateSamples[k] sera la liste des gates passant the cluster à k
        std::vector<std::vector<GateSample>> gateSamples(vclustering.size());

        // On parcourt chacune des gates
        for(const auto& neighbourLink: index(vclustering[cluster.first].getNeighbourLinks())) {

            auto gate = vclustering.getGate(neighbourLink.second);
            float gateArea = float(embree::pi) * gate.radius * gate.radius;

            if(!isTransfertCluster[cluster.first] && !isTransfertCluster[neighbourLink.second.neighbour]) {
                // Dans le cas ou je ne suis pas de transfert et le voisin n'est pas un de transfert, on échantillonne
                // sur sa gate pour avoir une estimation du transfert d'energie entre les deux
                // clusters

                for(uint32_t i: range(Nsample)) {
                    Vec2f gateSample = embree::uniformSampleDisk(Vec2f(rng.getFloat(), rng.getFloat()),
                                                                 gate.radius);
                    AffineSpace3f frame = coordinateSystem(gate.center, gate.N);

                    GateSample sample;

                    sample.P = embree::xfmPoint(frame, Vec3f(gateSample.x, 0, gateSample.y));
                    sample.N = -gate.N; // Go to the other cluster
                    sample.E = Col3f(0.f);

                    // Estimation de l'intensité incidente sur le VPLs depuis ce cluster
                    for(uint32_t vplIdx: m_ClusterVPLs[cluster.first]) {
                        const OrientedVPL& vpl = vpls.orientedVPLs[vplIdx];
                        Vec3f wi = sample.P - vpl.P;
                        float dot_i = embree::dot(wi, vpl.N);
                        if(dot_i < 0.f) {
                            continue;
                        }
                        float distSquared = lengthSquared(wi);
                        float dot_o = embree::abs(embree::dot(-wi, gate.N));

                        float G = dot_i * dot_o / (distSquared * distSquared);

                        sample.E += vpl.L * G;
                    }

                    // Monte carlo estimation
                    sample.E = gateArea * sample.E / Nsample;

                    // Add the sample for the neighbour
                    gateSamples[neighbourLink.second.neighbour].emplace_back(sample);
                }

            } else if(isTransfertCluster[neighbourLink.second.neighbour]) {

                // Dans le cas ou c'est un cluster de transfert, il est déjà affecté par les VPLs
                // de ses clusters voisins, on échantillone donc pas sur sa gate. Par contre il
                // faut calculer une estimation de l'échange d'énergie entre les clusters séparés
                // par le transfert cluster
                VisibilityCluster::NeighbourLink next(-1, -1);
                for(const auto& neighbourLink2: vclustering[neighbourLink.second.neighbour].getNeighbourLinks()) {
                    // Recherche du suivant
                    if(neighbourLink2.neighbour != cluster.first) {
                        next = neighbourLink2;
                        break;
                    }
                }

                // Echantillonage sur sa gate et estimation du transfert d'energie
                if(next.neighbour != uint32_t(-1)) {
                    auto gate = vclustering.getGate(next);
                    float gateArea = float(embree::pi) * gate.radius * gate.radius;

                    for(uint32_t i: range(Nsample)) {
                        Vec2f gateSample = embree::uniformSampleDisk(Vec2f(rng.getFloat(), rng.getFloat()), gate.radius);
                        AffineSpace3f frame = coordinateSystem(gate.center, gate.N);

                        GateSample sample;

                        sample.P = embree::xfmPoint(frame, Vec3f(gateSample.x, 0, gateSample.y));
                        sample.N = -gate.N;
                        sample.E = Col3f(0.f);

                        for(uint32_t vplIdx: m_ClusterVPLs[cluster.first]) {
                            const OrientedVPL& vpl = vpls.orientedVPLs[vplIdx];
                            Vec3f wi = sample.P - vpl.P;
                            float dot_i = embree::dot(wi, vpl.N);
                            if(dot_i < 0.f) {
                                continue;
                            }
                            float distSquared = lengthSquared(wi);
                            float dot_o = embree::abs(embree::dot(-wi, gate.N));

                            float G = dot_i * dot_o / (distSquared * distSquared);

                            sample.E += vpl.L * G;
                        }

                        sample.E = gateArea * sample.E / Nsample;

                        gateSamples[next.neighbour].emplace_back(sample);
                    }
                }
            }
        }

        for(auto i: range(vclustering.size())) {
            gateOffsetsCounts.emplace_back(glm::ivec2(offset, gateSamples[i].size()));
            for(const auto& sample: gateSamples[i]) {
                sorted.emplace_back(sample);
            }
            offset += gateSamples[i].size();
        }


    }

    std::vector<float> gateSamplesData;

    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

    for(const auto& sample: sorted) {
        glm::vec4 samplePosition = m_ViewMatrix * glm::vec4(convert(sample.P), 1);
        glm::vec3 sampleNormal = NormalMatrix * convert(sample.N);

        gateSamplesData.push_back(samplePosition.x);
        gateSamplesData.push_back(samplePosition.y);
        gateSamplesData.push_back(samplePosition.z);
        gateSamplesData.push_back(sampleNormal.x);
        gateSamplesData.push_back(sampleNormal.y);
        gateSamplesData.push_back(sampleNormal.z);
        gateSamplesData.push_back(sample.E.r);
        gateSamplesData.push_back(sample.E.g);
        gateSamplesData.push_back(sample.E.b);
    }

    m_GateBuffer.bind(TEXBUFFER_GATEARRAY_TEXUNIT);
    m_GateBuffer.setData(gateSamplesData.size(), gateSamplesData.data(), GL_RGB32F);

    m_GateOffsetsCountsBuffer.bind(TEXBUFFER_GATEOFFSETSCOUNTS_TEXUNIT);
    m_GateOffsetsCountsBuffer.setData(gateOffsetsCounts.size(), gateOffsetsCounts.data(), GL_RG32I);
}

void GLLightGateRenderer3::shadingPass(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls, const Scene& scene, const VisibilityClustering& vclustering) {

    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::CapabilityRestorer blending(GL_BLEND, true);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLint bindedFramebuffer;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &bindedFramebuffer);

    m_IrradianceBuffer.bindForDrawing();

    glViewport(x, y, width, height);

    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);

    glClear(GL_COLOR_BUFFER_BIT);

    // VPL Shading pass

    sendVPLs(vpls, scene, vclustering);
    sendGates(vpls, scene, vclustering);

    m_ShadingPassData.m_Program.use();
    m_ShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
    m_ShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);

    m_ScreenTriangle.render();

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bindedFramebuffer);

    // Final pass

    glClear(GL_COLOR_BUFFER_BIT);

    m_BlurPassData.m_Program.use();

    m_ScreenTriangle.render();
}

void GLLightGateRenderer3::drawDiscontinuityBuffer(int x, int y, size_t width, size_t height) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(3);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);

    m_ScreenTriangle.render();
}

void GLLightGateRenderer3::drawBuffers(int x, int y, size_t width, size_t height,
                                      const VisibilityClustering& vclustering) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    // Draw normals
    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);
    m_DrawBuffersPassData.m_uDataToDisplay.set(0);

    m_ScreenTriangle.render();

    // Draw depth
    glViewport(x + width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(1);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw VCluster
    glViewport(x + 2 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(2);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY, fW, fH);

    ColorMap colors = buildRandomColorMap(vclustering.size());

    GLTextureBuffer colorBuffer;
    colorBuffer.bind(TEXBUFFER_VCLUSTERCOLORS_TEXUNIT);
    colorBuffer.setData(colors.size(), colors.data(), getEmbreeColorFormat());

    m_ScreenTriangle.render();

    // Draw discontinuity buffer
    glViewport(x + 3 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(3);
    m_DrawBuffersPassData.m_uViewport.set(fX + 3 * fW, fY, fW, fH);

    m_ScreenTriangle.render();
}

}
