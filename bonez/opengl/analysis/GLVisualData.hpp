#ifndef _BONEZ_GLVISUALDATA_HPP
#define _BONEZ_GLVISUALDATA_HPP

#include <string>
#include <memory>
#include <functional>
#include <glm/glm.hpp>

namespace BnZ {

class GLVisualDataRenderer;

class GLVisualData
{
private:
    void render(GLVisualDataRenderer& renderer);

    friend class GLVisualDataRenderer;

protected:
    virtual void doRender(GLVisualDataRenderer& renderer) = 0;

public:
    GLVisualData(const std::string& name, bool enable = true,
                 const glm::mat4& modelMatrix = glm::mat4(1.f)):
        m_Name(name), m_bEnabled(enable), m_ModelMatrix(modelMatrix) {
    }

    virtual ~GLVisualData() {
    }

    const std::string& getName() const {
        return m_Name;
    }

    bool isEnabled() const {
        return m_bEnabled;
    }

    void setEnable(bool value = true) {
        m_bEnabled = true;
    }

    void setModelMatrix(const glm::mat4& matrix) {
        m_ModelMatrix = matrix;
    }

    const glm::mat4& getModelMatrix() const {
        return m_ModelMatrix;
    }

private:
    std::string m_Name;
    bool m_bEnabled;
    glm::mat4 m_ModelMatrix;
};

typedef std::shared_ptr<GLVisualData> GLVisualDataPtr;

}

#endif // _BONEZ_GLVISUALDATA_HPP
