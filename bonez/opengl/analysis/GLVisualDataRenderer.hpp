#ifndef _BONEZ_GLVISUALDATARENDERER_HPP
#define _BONEZ_GLVISUALDATARENDERER_HPP

#include "GLVisualDataGroup.hpp"
#include "GLModel.hpp"
#include "GLNodePickingTexture.hpp"
#include "bonez/opengl/shaders/shaders.hpp"
#include "bonez/opengl/scene/GLScene.hpp"
#include "bonez/opengl/scene/GLGrid.hpp"
#include "bonez/opengl/GLISMRenderer.hpp"

#include <stack>

namespace BnZ {

class GLVisualDataRenderer
{
public:
    enum ProgramId {
        SCENE_PROGRAM,
        VISUAL_DATA_PROGRAM,
        NODE_PICKING_PROGRAM,
        SURFACE_POINT_PROGRAM,
        PROGRAM_COUNT
    };

    enum ShapeId {
        SPHERE,
        DISK,
        CONE,
        CYLINDER,
        CIRCLE,
        SHAPE_COUNT
    };

    enum DisplayMode {
        POINT = GL_POINT,
        LINE = GL_LINE,
        FILL = GL_FILL
    };

    enum TextureUnits {
        MAT_DIFFUSE_TEXUNIT,
        MAT_GLOSSY_TEXUNIT,
        MAT_SHININESS_TEXUNIT,
        TEXBUFFER_NODECOLORS_TEXUNIT
    };

    GLVisualDataRenderer(const GLSLProgramBuilder& programBuilder);

    void setUp();

    void setProjectionMatrix(const glm::mat4& matrix);

    void setViewMatrix(const glm::mat4& matrix);

    void setDisplayMode(DisplayMode mode);

    void setCulling(bool value = true);

    void pushModelMatrix(const glm::mat4& matrix);

    void popModelMatrix();

    void sendMatrices();

    struct FrameStatesRestorer {
        gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
        gloops::CapabilityRestorer depthTest;
        gloops::Viewport viewport;

        FrameStatesRestorer(): depthTest(GL_DEPTH_TEST, true), viewport(gloops::getViewport()) {
        }

        ~FrameStatesRestorer() {
            gloops::setViewport(viewport);
        }
    };

    FrameStatesRestorer startFrame(int x, int y, size_t w, size_t h);

    void startNodePicking();

    void endNodePicking();

    void renderScene(const GLScene& glScene, const Scene& scene);

    void renderGrid(const GLGrid& grid);

    void renderPoints(const GLPoints& points);

    void render(ShapeId shape, const glm::vec3& color);

    void render(const GLModel& model, const glm::vec3& color);

    void render(GLVisualData& data);

    void render(const GLVisualDataPtr& data);

    void renderLine(const Vec3f& p, const Vec3f& q, const Col3f& c1, const Col3f& c2, float lineWidth);

    void renderArrow(const Vec3f& p, const Vec3f& N, const Col3f& color, float scale);

    void renderSphere(const Vec3f& center, float radius, const Col3f& color);

    void renderSkeleton(const Skeleton& skeleton, float nodeScale, const Col3f* colors, bool showNodes);

    void renderGridArround(const Vec3f& P, int halfWidth, const Scene& scene, const Skeleton* pSkeleton);

    void renderFrame(const AffineSpace3f& frameSpace, float scale);

    void renderDisk(const Vec3f& center, const Vec3f& N, float radius, const Col3f& color);

    void renderCircle(const Vec3f& center, float radius, const Col3f& color, float lineWidth);

    const GLNodePickingTexture& getNodePickingTexture() const {
        return m_PickingTexture;
    }

private:
    void useProgram(ProgramId id);

    GLModel m_Shapes[SHAPE_COUNT];

    glm::mat4 m_ProjectionMatrix, m_ViewMatrix;
    std::stack<glm::mat4> m_ModelMatrixStack;
    bool m_bResendMatrices;

    struct Uniforms {
        gloops::Uniform m_uMVPMatrix;
        gloops::Uniform m_uMVMatrix;
        gloops::Uniform m_uNormalMatrix;
        gloops::Uniform m_uDrawLines;
        gloops::Uniform m_uNodeIndex;
        gloops::Uniform m_uNodeColors;
        gloops::Uniform m_uNormalLength;

        Uniforms() {
        }

        Uniforms(const gloops::Program& program):
            m_uMVPMatrix(program, "uMVPMatrix", true),
            m_uMVMatrix(program, "uMVMatrix"),
            m_uNormalMatrix(program, "uNormalMatrix"),
            m_uDrawLines(program, "uDrawLines"),
            m_uNodeIndex(program, "uNodeIndex"),
            m_uNodeColors(program, "uNodeColors"),
            m_uNormalLength(program, "uNormalLength") {
        }
    };

    Uniforms m_Uniforms;

    gloops::Program m_Programs[PROGRAM_COUNT];
    ProgramId m_CurrentProgram;

    gloops::Viewport m_Viewport;

    bool m_bNodePicking;
    GLNodePickingTexture m_PickingTexture;
};

class GLShapeVisualData: public GLVisualData {
public:
    GLShapeVisualData(GLVisualDataRenderer::ShapeId shape,
                      const glm::vec3& color,
                      const std::string& name,
                      bool enable = true,
                      const glm::mat4& modelMatrix = glm::mat4(1.f)):
        GLVisualData(name, enable, modelMatrix), m_Shape(shape), m_Color(color) {
    }

protected:
    virtual void doRender(GLVisualDataRenderer& renderer) {
        renderer.render(m_Shape, m_Color);
    }

private:
    GLVisualDataRenderer::ShapeId m_Shape;
    glm::vec3 m_Color;
};

class GLModelVisualData: public GLVisualData {
public:
    GLModelVisualData(GLenum primitiveType,
                      GLModelVertex* vertices,
                      size_t vertexCount,
                      GLuint* indices,
                      size_t indexCount,
                      const glm::vec3& color,
                      const std::string& name,
                      bool enable = true,
                      const glm::mat4& modelMatrix = glm::mat4(1.f)):
        GLVisualData(name, enable, modelMatrix), m_Model(primitiveType), m_Color(color) {
        m_Model.fill(vertices, vertexCount, indices, indexCount);
    }

protected:
    virtual void doRender(GLVisualDataRenderer& renderer) {
        renderer.render(m_Model, m_Color);
    }

private:
    GLModel m_Model;
    const glm::vec3& m_Color;
};

}

#endif // _BONEZ_GLVISUALDATARENDERER_HPP
