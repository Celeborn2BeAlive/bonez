#include "GLVisualDataGroup.hpp"
#include "GLVisualDataRenderer.hpp"

namespace BnZ {

void GLVisualDataGroup::doRender(GLVisualDataRenderer& renderer) {
   for(auto child: m_Children) {
       if(child) {
           renderer.render(child);
       }
   }
}

}
