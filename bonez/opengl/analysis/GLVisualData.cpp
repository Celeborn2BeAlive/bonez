#include "GLVisualData.hpp"
#include "GLVisualDataRenderer.hpp"

namespace BnZ {

void GLVisualData::render(GLVisualDataRenderer& renderer) {
    if(m_bEnabled) {
        renderer.pushModelMatrix(m_ModelMatrix);
            doRender(renderer);
        renderer.popModelMatrix();
    }
}

}
