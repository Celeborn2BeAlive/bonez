#include "GLModel.hpp"
#include <vector>

namespace BnZ {

void buildSphere(GLsizei discLat, GLsizei discLong, GLModel& model) {
    GLfloat rcpLat = 1.f / discLat, rcpLong = 1.f / discLong;
    GLfloat dPhi = float(embree::two_pi) * rcpLat, dTheta = float(embree::pi) * rcpLong;

    std::vector<GLModelVertex> vertices;

    float pi_over_two = float(embree::pi) / 2.f;

    // Construit l'ensemble des vertex
    for(GLsizei j = 0; j <= discLong; ++j) {
        GLfloat cosTheta = cos(-pi_over_two + j * dTheta);
        GLfloat sinTheta = sin(-pi_over_two + j * dTheta);

        for(GLsizei i = 0; i <= discLat; ++i) {
            glm::vec3 coords;

            coords.x = sin(i * dPhi) * cosTheta;
            coords.y = sinTheta;
            coords.z = cos(i * dPhi) * cosTheta;

            vertices.push_back(GLModelVertex(coords, coords));
        }
    }

    std::vector<GLuint> indices;

    for(GLsizei j = 0; j < discLong; ++j) {
        GLsizei offset = j * (discLat + 1);
        for(GLsizei i = 0; i < discLat; ++i) {
            indices.push_back(offset + i);
            indices.push_back(offset + (i + 1));
            indices.push_back(offset + discLat + 1 + (i + 1));

            indices.push_back(offset + i);
            indices.push_back(offset + discLat + 1 + (i + 1));
            indices.push_back(offset + i + discLat + 1);
        }
    }

    model.fill(vertices.data(), vertices.size(), indices.data(), indices.size());
    model.primitiveType = GL_TRIANGLES;
}

void buildDisk(GLsizei discBase, GLModel& model) {
    GLfloat rcpBase = 1.f / discBase;
    GLfloat dPhi = float(embree::two_pi) * rcpBase;

    std::vector<GLModelVertex> vertices;

    // Construit l'ensemble des vertex

    glm::vec3 normal(0, 1, 0);

    // Origin
    vertices.push_back(GLModelVertex(glm::vec3(0, 0, 0), normal));

    for(GLsizei i = 0; i <= discBase; ++i) {
        vertices.push_back(GLModelVertex(glm::vec3(sin(i * dPhi), 0, cos(i * dPhi)),
                                         normal));
    }

    std::vector<GLuint> indices;

    for(GLsizei i = 0; i < discBase; ++i) {
        indices.push_back(0);
        indices.push_back(i + 1);
        indices.push_back(i + 2);
    }

    model.fill(vertices.data(), vertices.size(), indices.data(), indices.size());
    model.primitiveType = GL_TRIANGLES;
}

void buildCircle(GLsizei discBase, GLModel& model) {
    GLfloat rcpBase = 1.f / discBase;
    GLfloat dPhi = float(embree::two_pi) * rcpBase;

    std::vector<GLModelVertex> vertices;

    // Construit l'ensemble des vertex

    glm::vec3 normal(0, 1, 0);

    for(GLsizei i = 0; i <= discBase; ++i) {
        vertices.push_back(GLModelVertex(glm::vec3(sin(i * dPhi), 0, cos(i * dPhi)),
                                         normal));
    }

    std::vector<GLuint> indices;

    for(GLsizei i = 0; i < discBase; ++i) {
        indices.push_back(i);
        indices.push_back(i + 1);
    }

    model.fill(vertices.data(), vertices.size(), indices.data(), indices.size());
    model.primitiveType = GL_LINES;
}

void buildCone(GLsizei discLat, GLsizei discHeight, GLModel& model) {
    GLfloat rcpLat = 1.f / discLat, rcpH = 1.f / discHeight;
    GLfloat dPhi = float(embree::two_pi) * rcpLat, dH = rcpH;

    std::vector<GLModelVertex> vertices;

    // Construit l'ensemble des vertex
    for(GLsizei j = 0; j <= discHeight; ++j) {
        for(GLsizei i = 0; i < discLat; ++i) {
            glm::vec3 position, normal;

            position.x = (1.f - j * dH) * sin(i * dPhi);
            position.y = j * dH;
            position.z = (1.f - j * dH) * cos(i * dPhi);

            normal.x = sin(i * dPhi);
            normal.y = 1.f;
            normal.z = cos(i * dPhi);

            vertices.push_back(GLModelVertex(position, glm::normalize(normal)));
        }
    }

    std::vector<GLuint> indices;

    // Construit les vertex finaux en regroupant les données en triangles:
    // Pour une longitude donnée, les deux triangles formant une face sont de la forme:
    // (i, i + 1, i + discLat + 1), (i, i + discLat + 1, i + discLat)
    // avec i sur la bande correspondant à la longitude
    for(GLsizei j = 0; j < discHeight; ++j) {
        GLsizei offset = j * discLat;
        for(GLsizei i = 0; i < discLat; ++i) {
            indices.push_back(offset + i);
            indices.push_back(offset + (i + 1) % discLat);
            indices.push_back(offset + discLat + (i + 1) % discLat);

            indices.push_back(offset + i);
            indices.push_back(offset + discLat + (i + 1) % discLat);
            indices.push_back(offset + i + discLat);
        }
    }

    model.fill(vertices.data(), vertices.size(), indices.data(), indices.size());
    model.primitiveType = GL_TRIANGLES;
}

void buildCylinder(GLsizei discLat, GLsizei discHeight, GLModel& model) {
    GLfloat rcpLat = 1.f / discLat, rcpH = 1.f / discHeight;
    GLfloat dPhi = float(embree::two_pi) * rcpLat, dH = rcpH;

    std::vector<GLModelVertex> vertices;

    // Construit l'ensemble des vertex
    for(GLsizei j = 0; j <= discHeight; ++j) {
        for(GLsizei i = 0; i < discLat; ++i) {
            glm::vec3 position, normal;

            normal.x = sin(i * dPhi);
            normal.y = 0;
            normal.z = cos(i * dPhi);

            position = normal;
            position.y = j * dH;

            vertices.push_back(GLModelVertex(position, normal));
        }
    }

    std::vector<GLuint> indices;

    for(GLsizei j = 0; j < discHeight; ++j) {
        GLsizei offset = j * discLat;
        for(GLsizei i = 0; i < discLat; ++i) {
            indices.push_back(offset + i);
            indices.push_back(offset + (i + 1) % discLat);
            indices.push_back(offset + discLat + (i + 1) % discLat);

            indices.push_back(offset + i);
            indices.push_back(offset + discLat + (i + 1) % discLat);
            indices.push_back(offset + i + discLat);
        }
    }

    model.fill(vertices.data(), vertices.size(), indices.data(), indices.size());
    model.primitiveType = GL_TRIANGLES;
}

}
