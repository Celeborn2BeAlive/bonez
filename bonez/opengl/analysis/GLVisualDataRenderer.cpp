#include "GLVisualDataRenderer.hpp"
#include "visual_data.hpp"

namespace BnZ {

static const GLchar* s_GLPointsVertexShader =
"#version 430 core\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec3 aPosition; // World space vertex position
layout(location = 1) in vec3 aNormal; // World space vertex normal
layout(location = 2) in vec3 aColor;

out vec3 vPosition;
out vec3 vNormal;
out vec3 vColor;

void main() {
    vec4 hPosition = vec4(aPosition, 1.f);

    vPosition = aPosition;
    vNormal = aNormal;
    vColor = aColor;

    gl_Position = hPosition;
}

);

static const GLchar* s_GLPointsGeometryShader =
"#version 430 core\n"
GLOOPS_STRINGIFY(

layout(points) in;
layout(line_strip, max_vertices = 2) out;

in vec3 vPosition[];
in vec3 vNormal[];
in vec3 vColor[];

out vec3 gColor;

uniform mat4 uMVPMatrix; // Model View Projection matrix
uniform float uNormalLength;

void main() {
    gColor = vColor[0];

    gl_Position = uMVPMatrix * vec4(vPosition[0], 1);

    EmitVertex();

    gl_Position = uMVPMatrix * vec4(vPosition[0] + uNormalLength * vNormal[0], 1);

    EmitVertex();

    EndPrimitive();
}

);

static const GLchar* s_GLPointsFragmentShader =
"#version 430 core\n"
GLOOPS_STRINGIFY(

in vec3 gColor;

out vec3 fFragColor;

void main() {
    fFragColor = gColor;
}

);

GLVisualDataRenderer::GLVisualDataRenderer(const GLSLProgramBuilder& programBuilder):
    m_ProjectionMatrix(1.f),
    m_ViewMatrix(1.f),
    m_CurrentProgram(PROGRAM_COUNT),
    m_bNodePicking(false) {

    m_Programs[SCENE_PROGRAM] = programBuilder.buildProgram("scene.vs.glsl", "scene.fs.glsl");
    m_Programs[VISUAL_DATA_PROGRAM] = programBuilder.buildProgram("visual_data.vs.glsl", "visual_data.fs.glsl");
    m_Programs[NODE_PICKING_PROGRAM] = programBuilder.buildProgram("node_picking.vs.glsl", "node_picking.fs.glsl");
    m_Programs[SURFACE_POINT_PROGRAM] = buildProgram(s_GLPointsVertexShader, s_GLPointsGeometryShader,
                                                     s_GLPointsFragmentShader);

    m_ModelMatrixStack.push(glm::mat4(1.f));

    buildSphere(16, 8, m_Shapes[SPHERE]);
    buildDisk(8, m_Shapes[DISK]);
    buildCone(8, 1, m_Shapes[CONE]);
    buildCylinder(8, 1, m_Shapes[CYLINDER]);
    buildCircle(32, m_Shapes[CIRCLE]);
}

void GLVisualDataRenderer::setUp() {
    m_CurrentProgram = PROGRAM_COUNT;
}

void GLVisualDataRenderer::setProjectionMatrix(const glm::mat4& matrix) {
    m_ProjectionMatrix = matrix;
    m_bResendMatrices = true;
}

void GLVisualDataRenderer::setViewMatrix(const glm::mat4& matrix) {
    m_ViewMatrix = matrix;
    m_bResendMatrices = true;
}

void GLVisualDataRenderer::setDisplayMode(DisplayMode mode) {
    glPolygonMode(GL_FRONT_AND_BACK, mode);
}

void GLVisualDataRenderer::setCulling(bool value) {
    if(value) {
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
    } else {
        glDisable(GL_CULL_FACE);
    }
}

void GLVisualDataRenderer::pushModelMatrix(const glm::mat4& matrix) {
    m_ModelMatrixStack.push(m_ModelMatrixStack.top() * matrix);
    m_bResendMatrices = true;
}

void GLVisualDataRenderer::popModelMatrix() {
    if(m_ModelMatrixStack.size() == 1) {
        return;
    }
    m_ModelMatrixStack.pop();
    m_bResendMatrices = true;
}

void GLVisualDataRenderer::useProgram(ProgramId id) {
    if(m_bNodePicking) {
        if(m_CurrentProgram != NODE_PICKING_PROGRAM) {
            m_Programs[NODE_PICKING_PROGRAM].use();
            m_Uniforms = Uniforms(m_Programs[NODE_PICKING_PROGRAM]);
            m_CurrentProgram = NODE_PICKING_PROGRAM;
            m_bResendMatrices = true;
        }
    } else if(m_CurrentProgram != id) {
        m_Programs[id].use();
        m_Uniforms = Uniforms(m_Programs[id]);
        m_CurrentProgram = id;
        m_bResendMatrices = true;
    }
}

void GLVisualDataRenderer::sendMatrices() {
    if(m_bResendMatrices) {
        glm::mat4 MVMatrix = m_ViewMatrix * m_ModelMatrixStack.top();
        m_Uniforms.m_uMVMatrix.set(MVMatrix);
        m_Uniforms.m_uNormalMatrix.set(glm::mat3(glm::inverse(glm::transpose(MVMatrix))));
        m_Uniforms.m_uMVPMatrix.set(m_ProjectionMatrix * MVMatrix);
        m_bResendMatrices = true;
    }
}

GLVisualDataRenderer::FrameStatesRestorer GLVisualDataRenderer::startFrame(int x, int y, size_t w, size_t h) {
    FrameStatesRestorer restorer;

    m_Viewport = gloops::Viewport(x, y, w, h);
    gloops::setViewport(m_Viewport);

    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    m_CurrentProgram = PROGRAM_COUNT;
    m_bResendMatrices = true;

    return restorer;
}

void GLVisualDataRenderer::renderScene(const GLScene& glScene, const Scene& scene) {
    GLMaterialManager::TextureUnits units { MAT_DIFFUSE_TEXUNIT, MAT_GLOSSY_TEXUNIT, MAT_SHININESS_TEXUNIT };

    if(m_bNodePicking) {
        useProgram(NODE_PICKING_PROGRAM);
        m_Uniforms.m_uNodeIndex.set(UNDEFINED_NODE);

        sendMatrices();

        GLMaterialUniforms materialUniforms(m_Programs[NODE_PICKING_PROGRAM]);
        glScene.render(materialUniforms, units);
    }
    else {
        useProgram(SCENE_PROGRAM);

        m_Uniforms.m_uNodeColors.set(TEXBUFFER_NODECOLORS_TEXUNIT);

        if(scene.voxelSpace) {
            gloops::BufferObject nodeColorsBO;

            NodeColors colors = scene.voxelSpace->getClusteringNodeColors();
            {
                gloops::BufferBind textureBuffer(GL_TEXTURE_BUFFER, nodeColorsBO);
                textureBuffer.bufferData(colors.size(), colors.data(), GL_STATIC_DRAW);
            }

            gloops::TextureObject texture;
            gloops::TextureBind textureBuffer(GL_TEXTURE_BUFFER, TEXBUFFER_NODECOLORS_TEXUNIT, texture);

            GLenum format = GL_RGB32F;
            if(sizeof(Col3f) == 4 * sizeof(float)) {
                format = GL_RGBA32F;
            }

            glTexBuffer(GL_TEXTURE_BUFFER, format, nodeColorsBO.glId());
            GLOOPS_CHECK_ERROR;
        }

        sendMatrices();
        GLMaterialUniforms materialUniforms(m_Programs[SCENE_PROGRAM]);
        glScene.render(materialUniforms, units);
    }
}

void GLVisualDataRenderer::renderPoints(const GLPoints& points) {
    useProgram(SURFACE_POINT_PROGRAM);

    m_Uniforms.m_uNormalLength.set(2.f);

    glVertexAttrib3f(2, 1, 1, 1);

    sendMatrices();

    points.render();
}

void GLVisualDataRenderer::renderGrid(const GLGrid& grid) {
    useProgram(VISUAL_DATA_PROGRAM);

    m_Uniforms.m_uNodeIndex.set(UNDEFINED_NODE);

    glVertexAttrib3f(2, 1, 1, 1);

    m_Uniforms.m_uDrawLines.set(false);

    sendMatrices();

    glEnable(GL_CULL_FACE);

    glCullFace(GL_BACK);
    grid.render();

    /*
    glVertexAttrib3f(2, 0, 0, 0);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    gloops::StateRestorer<GL_LINE_WIDTH> lineWidth;

    glLineWidth(2.f);
    glEnable(GL_POLYGON_OFFSET_LINE);
    glPolygonOffset(5.f, 5.f);
    glCullFace(GL_FRONT);
    grid.render();

    glDisable(GL_CULL_FACE);
    glDisable(GL_POLYGON_OFFSET_LINE);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);*/
}

void GLVisualDataRenderer::render(ShapeId shape, const glm::vec3& color) {
    useProgram(VISUAL_DATA_PROGRAM);
    sendMatrices();

    render(m_Shapes[shape], color);
}

void GLVisualDataRenderer::render(const GLModel& model, const glm::vec3& color) {
    useProgram(VISUAL_DATA_PROGRAM);
    sendMatrices();

    if(model.primitiveType == GL_LINES || model.primitiveType == GL_POINTS) {
        m_Uniforms.m_uDrawLines.set(true);
    } else {
        glVertexAttrib3fv(2, glm::value_ptr(color));
        m_Uniforms.m_uDrawLines.set(false);
    }
    model.render();
}

void GLVisualDataRenderer::render(GLVisualData& data) {
    data.render(*this);
}

void GLVisualDataRenderer::render(const GLVisualDataPtr& data) {
    if(data) {
        render(*data);
    }
}

void GLVisualDataRenderer::renderLine(const Vec3f &p, const Vec3f &q, const Col3f &c1, const Col3f &c2, float lineWidth) {
    GLModelVertex vertices[] = {
        GLModelVertex(convert(p), convert(c1)),
        GLModelVertex(convert(q), convert(c2))
    };

    GLuint indices[] = { 0, 1 };

    GLModel model(GL_LINES);
    model.fill(vertices, 2, indices, 2);

    gloops::StateRestorer<GL_LINE_WIDTH> lineWidthState;

    glLineWidth(lineWidth);

    render(model, glm::vec3());
}

void GLVisualDataRenderer::renderGridArround(const Vec3f& P, int halfWidth,
                                             const Scene& scene, const Skeleton* pSkeleton) {
    Vec3i centerVoxel = scene.voxelSpace->getVoxel(P);

    for(int i = -halfWidth; i <= halfWidth; ++i) {
        for(int j = -halfWidth; j <= halfWidth; ++j) {
            for(int k = -halfWidth; k <= halfWidth; ++k) {
                Vec3f voxel = Vec3f(int(centerVoxel.x) + i, int(centerVoxel.y) + j, int(centerVoxel.z) + k);
                Vec3f vertices[] = {
                    Vec3f(voxel.x, voxel.y, voxel.z),
                    Vec3f(voxel.x + 1, voxel.y, voxel.z),
                    Vec3f(voxel.x, voxel.y + 1, voxel.z),
                    Vec3f(voxel.x, voxel.y, voxel.z + 1),
                    Vec3f(voxel.x + 1, voxel.y + 1, voxel.z),
                    Vec3f(voxel.x + 1, voxel.y, voxel.z + 1),
                    Vec3f(voxel.x, voxel.y + 1, voxel.z + 1),
                    Vec3f(voxel.x + 1, voxel.y + 1, voxel.z + 1)
                };

                for(auto& v: vertices) {
                    v = embree::xfmPoint(scene.voxelSpace->getLocalToWorldTransform(), v);
                }

                Col3f colors[8];
                for(size_t i = 0; i < 8; ++i) {
                    GraphNodeIndex node = UNDEFINED_NODE;

                    if(pSkeleton == &scene.voxelSpace->getSkeleton()) {
                        node = scene.voxelSpace->getSkeleton().getNearestNode(vertices[i]);
                    } else {
                        node = scene.voxelSpace->getClustering().getNearestNode(vertices[i]);
                    }

                    if(node == UNDEFINED_NODE) {
                        colors[i] = Col3f(0.f);
                    } else {
                        if(pSkeleton == &scene.voxelSpace->getSkeleton()) {
                            colors[i] = scene.voxelSpace->getSkeletonNodeColor(node);
                        } else {
                            colors[i] = scene.voxelSpace->getClusteringNodeColor(node);
                        }
                    }
                }

                renderLine(vertices[0], vertices[1], colors[0], colors[1], 1);
                renderLine(vertices[0], vertices[2], colors[0], colors[2], 1);
                renderLine(vertices[0], vertices[3], colors[0], colors[3], 1);
                renderLine(vertices[1], vertices[4], colors[1], colors[4], 1);
                renderLine(vertices[1], vertices[5], colors[1], colors[5], 1);
                renderLine(vertices[2], vertices[4], colors[2], colors[4], 1);
                renderLine(vertices[2], vertices[6], colors[2], colors[6], 1);
                renderLine(vertices[3], vertices[5], colors[3], colors[5], 1);
                renderLine(vertices[3], vertices[6], colors[3], colors[6], 1);
                renderLine(vertices[7], vertices[4], colors[7], colors[4], 1);
                renderLine(vertices[7], vertices[5], colors[7], colors[5], 1);
                renderLine(vertices[7], vertices[6], colors[7], colors[6], 1);
            }
        }
    }
}

void GLVisualDataRenderer::renderArrow(const Vec3f& P, const Vec3f& N, const Col3f& color, float scale) {
    glm::mat4 modelMatrix = glm::scale(convert(coordinateSystem(P, N)),
                                       glm::vec3(scale));

    pushModelMatrix(modelMatrix);

    pushModelMatrix(glm::scale(glm::mat4(1.f), glm::vec3(0.05, 0.7, 0.05)));
    render(CYLINDER, convert(color));
    popModelMatrix();

    pushModelMatrix(glm::scale(glm::translate(glm::mat4(1.f), glm::vec3(0, 0.7, 0)), glm::vec3(0.2, 0.3, 0.2)));
    render(CONE, convert(color));
    popModelMatrix();

    popModelMatrix();
}

void GLVisualDataRenderer::renderFrame(const AffineSpace3f& frameSpace, float scale) {
    renderArrow(frameSpace.p, frameSpace.l.vx, Col3f(1, 0, 0), scale);
    renderArrow(frameSpace.p, frameSpace.l.vy, Col3f(0, 1, 0), scale);
    renderArrow(frameSpace.p, frameSpace.l.vz, Col3f(0, 0, 1), scale);
}

void GLVisualDataRenderer::renderSphere(const Vec3f& center, float radius, const Col3f& color) {
    glm::mat4 modelMatrix = glm::scale(glm::translate(glm::mat4(1), convert(center)),
                                       glm::vec3(radius));
    pushModelMatrix(modelMatrix);

    render(SPHERE, convert(color));

    popModelMatrix();
}

void GLVisualDataRenderer::renderDisk(const Vec3f& center, const Vec3f& N, float radius, const Col3f& color) {
    AffineSpace3f frame = coordinateSystem(center, N);

    glm::mat4 modelMatrix = glm::scale(convert(frame), glm::vec3(radius));

    pushModelMatrix(modelMatrix);

    render(DISK, convert(color));

    popModelMatrix();
}

void GLVisualDataRenderer::renderSkeleton(const Skeleton& skeleton, float nodeScale, const Col3f* colors,
                                          bool showNodes) {
    useProgram(VISUAL_DATA_PROGRAM);

    std::vector<GLModelVertex> edgeVertices;
    std::vector<GLuint> edgeIndices;

    size_t count = skeleton.size();

    float maxMaxball = 0.f;
    for(GraphNodeIndex i = 0; i < count; ++i) {
        maxMaxball = std::max(skeleton.getNode(i).maxball, maxMaxball);
    }

    for(GraphNodeIndex i = 0; i < count; ++i) {
        Skeleton::Node node = skeleton.getNode(i);

        if(showNodes) {
            if(m_bNodePicking) {
                m_Uniforms.m_uNodeIndex.set(i);
            }

            renderSphere(node.P, (node.maxball / maxMaxball) * nodeScale, colors[i]);
        }

        if(!m_bNodePicking) {
            edgeVertices.push_back(GLModelVertex(convert(node.P), convert(colors[i])));

            for(GraphNodeIndex j: skeleton.neighbours(i)) {
                edgeIndices.push_back(i);
                edgeIndices.push_back(j);
            }
        }
    }

    if(!m_bNodePicking) {
        GLModel edges(GL_LINES);

        edges.fill(edgeVertices.data(), edgeVertices.size(), edgeIndices.data(), edgeIndices.size());

        if(m_bNodePicking) {
            m_Uniforms.m_uNodeIndex.set(UNDEFINED_NODE);
        }

        render(edges, glm::vec3());
   }
}

void GLVisualDataRenderer::renderCircle(const Vec3f& center, float radius, const Col3f& color, float lineWidth) {
    gloops::StateRestorer<GL_LINE_WIDTH> lineWidthState;

    glLineWidth(lineWidth);

    glm::mat4 modelMatrix = glm::scale(glm::translate(glm::mat4(1), convert(center)), glm::vec3(radius));

    pushModelMatrix(modelMatrix);

    useProgram(VISUAL_DATA_PROGRAM);

    glm::mat4 MVMatrix = m_ViewMatrix * m_ModelMatrixStack.top();
    MVMatrix = glm::mat4(glm::vec4(radius, 0, 0, 0), glm::vec4(0, radius, 0, 0), glm::vec4(0, 0, radius, 0),
                         MVMatrix[3]);
    MVMatrix *= convert(coordinateSystem(Vec3f(0.f), Vec3f(0, 0, 1)));
    m_Uniforms.m_uMVMatrix.set(MVMatrix);
    m_Uniforms.m_uNormalMatrix.set(glm::mat3(glm::inverse(glm::transpose(MVMatrix))));
    m_Uniforms.m_uMVPMatrix.set(m_ProjectionMatrix * MVMatrix);
    m_bResendMatrices = true;

    glVertexAttrib3fv(2, glm::value_ptr(convert(color)));

    m_Uniforms.m_uDrawLines.set(false);

    m_Shapes[CIRCLE].render();

    popModelMatrix();
}

void GLVisualDataRenderer::startNodePicking() {
    m_bNodePicking = true;
    m_PickingTexture.init(m_Viewport.width, m_Viewport.height);
    m_PickingTexture.bindForWriting();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, m_Viewport.width, m_Viewport.height);
}

void GLVisualDataRenderer::endNodePicking() {
    m_PickingTexture.unbind();
    m_bNodePicking = false;
}

}
