#include "GLNodePickingTexture.hpp"

namespace BnZ {

GLNodePickingTexture::GLNodePickingTexture():
    m_nWidth(0),
    m_nHeight(0) {
}

bool GLNodePickingTexture::init(size_t width, size_t height) {
    if(width != m_nWidth && height != m_nHeight) {
        m_nWidth = width;
        m_nHeight = height;

        gloops::FramebufferBind framebuffer(GL_FRAMEBUFFER, m_FBO);

        {
            gloops::TextureBind texture2D(GL_TEXTURE_2D, 0, m_DepthTexture);
            texture2D.texImage2D(0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
            framebuffer.framebufferTexture2D(GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_DepthTexture, 0);
        }


        {
            gloops::TextureBind texture2D(GL_TEXTURE_2D, 0, m_NodeIndexTexture);
            texture2D.texImage2D(0, GL_RGB32UI, width, height, 0, GL_RGB_INTEGER, GL_UNSIGNED_INT, nullptr);
            framebuffer.framebufferTexture2D(GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_NodeIndexTexture, 0);
        }

        GLenum status = framebuffer.checkFramebufferStatus();
        if(status != GL_FRAMEBUFFER_COMPLETE) {
            std::cerr << "GL_FRAMEBUFFER_COMPLETE" << std::endl;
            return false;
        }
    }

    return true;
}

void GLNodePickingTexture::bindForWriting() {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());
    GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, drawBuffers);
}

void GLNodePickingTexture::unbind() {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

GraphNodeIndex GLNodePickingTexture::operator ()(uint32_t x, uint32_t y) const {
    gloops::FramebufferBind framebuffer(GL_READ_FRAMEBUFFER, m_FBO);
    glReadBuffer(GL_COLOR_ATTACHMENT0);

    uint32_t values[3];
    glReadPixels(x, m_nHeight - y - 1, 1, 1, GL_RGB_INTEGER, GL_UNSIGNED_INT, values);

    glReadBuffer(GL_NONE);

    if(values[0] == 0) {
        return UNDEFINED_NODE;
    }

    return values[0] - 1;
}

}
