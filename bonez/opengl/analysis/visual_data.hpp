#ifndef _BONEZ_VISUAL_DATA_HPP
#define _BONEZ_VISUAL_DATA_HPP

#include "GLVisualData.hpp"
#include "GLVisualDataGroup.hpp"
#include "GLModel.hpp"

#include "bonez/common/data/Graph.hpp"
#include "bonez/scene/Scene.hpp"
#include "bonez/scene/lights/vpls.hpp"

#include <functional>

#include "bonez/opengl/common.hpp"

namespace BnZ {

/*
class GLGraphVisualData: public GLVisualData {
protected:
    virtual void doRender(GLVisualDataRenderer& renderer);

public:
    GLGraphVisualData(const std::string& name):
        GLVisualData(name, true, glm::mat4(1.f)),
        m_NodeGroup(std::make_shared<GLVisualDataGroup>(name + "_nodes")) {
    }

    GLVisualDataGroupPtr m_NodeGroup;
    GLModel m_Edges;
};

typedef std::shared_ptr<GLGraphVisualData> GLGraphVisualDataPtr;

GLGraphVisualDataPtr createVisualData(const Graph& graph,
                                      std::function<Vec3f (GraphNodeIndex)> getPosition,
                                      std::function<Col3f (GraphNodeIndex)> getColor,
                                      float nodeScale);
*/
GLVisualDataPtr createVisualData(const SurfacePoint& point, const Col3f& color, const Scene& scene);

GLVisualDataPtr createVisualData(const VPLContainer& vpls, const Scene& scene);

template<typename Functor>
class GLVisualDataFunctor: public GLVisualData {
    Functor m_Functor;

    virtual void doRender(GLVisualDataRenderer& renderer) {
        m_Functor(renderer);
    }

public:
    GLVisualDataFunctor(Functor functor, const std::string& name, bool enable = true,
                 const glm::mat4& modelMatrix = glm::mat4(1.f)):
        GLVisualData(name, enable, modelMatrix), m_Functor(functor) {
    }
};

template<typename Functor>
GLVisualDataPtr createVisualData(Functor functor, const std::string& name, bool enable = true,
                                 const glm::mat4& modelMatrix = glm::mat4(1.f)) {
    return std::make_shared<GLVisualDataFunctor<Functor>>(functor, name, enable, modelMatrix);
}

GLVisualDataGroupPtr buildArrow(const std::string& name, const glm::vec3& color);

}

#endif // _BONEZ_VISUAL_DATA_HPP
