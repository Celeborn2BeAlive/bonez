#ifndef _BONEZ_GLNODEPICKINGTEXTURE_HPP
#define _BONEZ_GLNODEPICKINGTEXTURE_HPP

#include "bonez/opengl/common.hpp"
#include "bonez/common/data/Graph.hpp"
#include "../deferred/GBuffer.hpp"

namespace BnZ {

class GLNodePickingTexture {
public:
    GLNodePickingTexture();

    bool init(size_t width, size_t height);

    void bindForWriting();

    void unbind();

    GraphNodeIndex operator ()(uint32_t x, uint32_t y) const;

private:
    size_t m_nWidth, m_nHeight;

    gloops::FramebufferObject m_FBO;
    gloops::TextureObject m_DepthTexture;
    gloops::TextureObject m_NodeIndexTexture;
};

}

#endif // _BONEZ_GLNODEPICKINGTEXTURE_HPP
