#include "visual_data.hpp"

#include "GLVisualDataGroup.hpp"
#include "GLVisualDataRenderer.hpp"

#include <sstream>

namespace BnZ {

/*
void GLGraphVisualData::doRender(GLVisualDataRenderer &renderer) {
    renderer.render(m_NodeGroup);
    gloops::StateRestorer<GL_LINE_WIDTH> lineWidth;
    glLineWidth(2.f);
    renderer.render(m_Edges, glm::vec3(0, 0, 0));
}

GLGraphVisualDataPtr createVisualData(const Graph& graph,
                                      std::function<Vec3f (GraphNodeIndex)> getPosition,
                                      std::function<Col3f (GraphNodeIndex)> getColor,
                                      float nodeScale) {
    GLGraphVisualDataPtr glGraph = std::make_shared<GLGraphVisualData>("graph");

    std::vector<GLModelVertex> vertices;
    std::vector<GLuint> indices;

    for(GraphNodeIndex i = 0; i < graph.size(); ++i) {
        std::stringstream ss;
        ss << "node_" << i;

        glm::vec3 position = convert(getPosition(i));

        glm::mat4 modelMatrix = glm::translate(glm::mat4(1.f), position);

        glm::vec3 color = convert(getColor(i));
        glGraph->m_NodeGroup->add(std::make_shared<GLShapeVisualData>(
                   GLVisualDataRenderer::SPHERE,
                   color,
                   ss.str(),
                   true,
                   glm::scale(modelMatrix, glm::vec3(nodeScale * 0.2f))));

        vertices.push_back(GLModelVertex(position, color));

        for(GraphNodeIndex j = 0; j < graph[i].size(); ++j) {
            indices.push_back(i);
            indices.push_back(graph[i][j]);
        }
    }

    glGraph->m_Edges.fill(vertices.data(), vertices.size(), indices.data(), indices.size());
    glGraph->m_Edges.primitiveType = GL_LINES;

    return glGraph;
}
*/
GLVisualDataGroupPtr buildArrow(const std::string& name, const glm::vec3& color) {
    GLVisualDataGroupPtr group = std::make_shared<GLVisualDataGroup>(name);

    group->add(std::make_shared<GLShapeVisualData>(
                   GLVisualDataRenderer::CYLINDER,
                   color,
                   name + "_base",
                   true,
                   glm::scale(glm::mat4(1.f), glm::vec3(0.1, 1, 0.1))
                   )
               );

    group->add(std::make_shared<GLShapeVisualData>(
                   GLVisualDataRenderer::CONE,
                   color,
                   name + "_arrow",
                   true,
                   glm::scale(glm::translate(glm::mat4(1.f), glm::vec3(0, 1, 0)), glm::vec3(0.5, 1, 0.5))
                   )
               );

    return group;
}

GLVisualDataPtr createVisualData(const SurfacePoint& point, const Col3f& color, const Scene& scene) {
    float scaleFactor = embree::length(scene.geometry.boundingBox.size()) * 0.006;

    return createVisualData([point, color, scaleFactor](GLVisualDataRenderer& renderer) {
        renderer.renderArrow(point.P, point.Ns, color, scaleFactor);
    }, "point");
}

GLVisualDataPtr createVisualData(const VPLContainer& vpls, const Scene& scene) {
    float scaleFactor = embree::length(scene.geometry.boundingBox.size()) * 0.006;

    return createVisualData([vpls, scaleFactor](GLVisualDataRenderer& renderer) {
        for(const OrientedVPL& vpl: vpls.orientedVPLs) {
            renderer.renderArrow(vpl.P, vpl.N, vpl.L, scaleFactor);
        }
    }, "vpls");
}

}
