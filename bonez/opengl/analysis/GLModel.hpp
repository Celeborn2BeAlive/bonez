#ifndef _BONEZ_OPENGL_MODELS_COMMON_HPP
#define _BONEZ_OPENGL_MODELS_COMMON_HPP

#include "opengl/common.hpp"

namespace BnZ {

struct GLModelVertex {
    static const GLuint POSITION_LOCATION = 0;
    static const GLuint NORMAL_LOCATION = 1;
    static const GLuint COLOR_LOCATION = 2;

    glm::vec3 position;
    glm::vec3 normalOrColor;

    GLModelVertex() {
    }

    GLModelVertex(glm::vec3 position, glm::vec3 normalOrColor):
        position(position), normalOrColor(normalOrColor) {
    }
};

struct GLModel {
    gloops::BufferObject vbo, ibo;
    gloops::VertexArrayObject vao;
    size_t indexCount;
    GLenum primitiveType;

    GLModel(GLenum primitiveType = GL_TRIANGLES): indexCount(0), primitiveType(primitiveType) {
        gloops::BufferBind vboBind(GL_ARRAY_BUFFER, vbo);
        gloops::VertexArrayBind vaoBind(vao);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo.glId());

        vaoBind.enableVertexAttribArray(GLModelVertex::POSITION_LOCATION);
        vaoBind.vertexAttribPointer(GLModelVertex::POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE,
                                    sizeof(GLModelVertex), GLOOPS_OFFSETOF(GLModelVertex, position));

        if(primitiveType == GL_TRIANGLES) {
            vaoBind.enableVertexAttribArray(GLModelVertex::NORMAL_LOCATION);
            vaoBind.vertexAttribPointer(GLModelVertex::NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE,
                                        sizeof(GLModelVertex), GLOOPS_OFFSETOF(GLModelVertex, normalOrColor));
        } else {
            vaoBind.enableVertexAttribArray(GLModelVertex::COLOR_LOCATION);
            vaoBind.vertexAttribPointer(GLModelVertex::COLOR_LOCATION, 3, GL_FLOAT, GL_FALSE,
                                        sizeof(GLModelVertex), GLOOPS_OFFSETOF(GLModelVertex, normalOrColor));
        }
    }

    void fill(GLModelVertex* vertices, size_t vertexCount, GLuint* indices, size_t indexCount) {
        this->indexCount = indexCount;

        gloops::BufferBind vboBind(GL_ARRAY_BUFFER, vbo);
        vboBind.bufferData(vertexCount, vertices, GL_STATIC_DRAW);

        gloops::BufferBind iboBind(GL_ELEMENT_ARRAY_BUFFER, ibo);
        iboBind.bufferData(indexCount, indices, GL_STATIC_DRAW);
    }

    void render() const {
        gloops::VertexArrayBind(vao).drawElements(primitiveType, indexCount, GL_UNSIGNED_INT, 0);
    }
};

void buildSphere(GLsizei discLat, GLsizei discLong, GLModel& model);

void buildDisk(GLsizei discBase, GLModel& model);

void buildCircle(GLsizei discBase, GLModel& model);

void buildCone(GLsizei discBase, GLsizei discHeight, GLModel& model);

void buildCylinder(GLsizei discBase, GLsizei discHeight, GLModel& model);

}

#endif // _BONEZ_OPENGL_MODELS_COMMON_HPP
