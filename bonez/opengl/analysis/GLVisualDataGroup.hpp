#ifndef _BONEZ_GLVISUALDATAGROUP_HPP
#define _BONEZ_GLVISUALDATAGROUP_HPP

#include <vector>
#include "GLVisualData.hpp"

namespace BnZ {

class GLVisualDataRenderer;

class GLVisualDataGroup: public GLVisualData {
public:
    typedef std::vector<GLVisualDataPtr> Container;
    typedef Container::iterator iterator;
    typedef Container::const_iterator const_iterator;

    struct Element {
        GLVisualDataPtr data;
        size_t index;

        Element() {
        }

        Element(const GLVisualDataPtr& data, size_t idx):
            data(data), index(idx) {
        }

        operator bool() const {
            return data.get();
        }
    };

    GLVisualDataGroup(const std::string& name,
                      bool enable = true,
                      const glm::mat4& modelMatrix = glm::mat4(1.f)):
        GLVisualData(name, enable, modelMatrix) {
    }

    Element add(const GLVisualDataPtr& data) {
        Element e(data, m_Children.size());
        m_Children.push_back(e.data);
        return e;
    }

    void set(Element& elt, const GLVisualDataPtr& data) {
        if(elt) {
            m_Children[elt.index] = data;
            elt.data = data;
        } else {
            elt = add(data);
        }
    }

    void remove(Element& elt) {
        if(elt) {
            std::swap(m_Children[elt.index], m_Children.back());
            m_Children.pop_back();
            elt.data = nullptr;
        }
    }

    void clear() {
        m_Children.clear();
    }

    const GLVisualDataPtr& operator [](size_t idx) const {
        return m_Children[idx];
    }

    iterator begin() {
        return std::begin(m_Children);
    }

    const_iterator begin() const {
        return std::begin(m_Children);
    }

    iterator end() {
        return std::end(m_Children);
    }

    const_iterator end() const {
        return std::end(m_Children);
    }

protected:
    virtual void doRender(GLVisualDataRenderer& renderer);

private:
    Container m_Children;
};

typedef std::shared_ptr<GLVisualDataGroup> GLVisualDataGroupPtr;

}

#endif // _BONEZ_GLVISUALDATAGROUP_HPP
