#ifndef _BONEZ_GLMESH_HPP_
#define _BONEZ_GLMESH_HPP_

#include <gloops/gloops.hpp>
#include <glm/glm.hpp>

#include "bonez/scene/geometry/TriangleMesh.hpp"
#include "GLMaterial.hpp"

namespace BnZ {

class GLMesh {
public:
    struct Vertex {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec2 texCoords;
        uint32_t nodeIndex;
    };

    GLMesh(const TriangleMesh& mesh, const Scene& scene);
    
    void render() const;
    
    size_t getMaterialIndex() const {
        return m_nMaterialIndex;
    }
    
private:
    gloops::BufferObject m_VBO, m_IBO;
    gloops::VertexArrayObject m_VAO;
    size_t m_nIndexCount;
    size_t m_nMaterialIndex;
};

}

#endif
