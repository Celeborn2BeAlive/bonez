#include "GLGrid.hpp"

namespace BnZ {

#define V1 Vec3f(0, 0, 0)
#define V2 Vec3f(1, 0, 0)
#define V3 Vec3f(1, 1, 0)
#define V4 Vec3f(0, 1, 0)
#define V5 Vec3f(0, 0, 1)
#define V6 Vec3f(0, 1, 1)
#define V7 Vec3f(1, 1, 1)
#define V8 Vec3f(1, 0, 1)

#define N0 Vec3f(0, 0, -1)
#define N1 Vec3f(0, 1, 0)
#define N2 Vec3f(-1, 0, 0)
#define N3 Vec3f(0, -1, 0)
#define N4 Vec3f(1, 0, 0)
#define N5 Vec3f(0, 0, 1)

/*
void GLGrid::init(const Scene &scene) {
    if(!scene.topology) {
        return;
    }

    const CurvilinearSkeleton::GridType& grid = scene.voxelSpace->getSkeleton().getGrid();
    Vec3ui size = grid.getSize();

    AffineSpace3f transform = grid.getGridToWorldTransform();

    int size_x = size.x;
    int size_y = size.y;
    int size_z = size.z;

    std::vector<Vertex> vertices;

    const Vec3f P[] = {
        V1, V2, V3, V4, V5, V6, V7, V8
    };

    const Vec3f N[] = {
        xfmNormal(transform, N0),
        xfmNormal(transform, N1),
        xfmNormal(transform, N2),
        xfmNormal(transform, N3),
        xfmNormal(transform, N4),
        xfmNormal(transform, N5)
    };

    const uint32_t F[6][6] = {
        { 0, 3, 2, 0, 2, 1 },
        { 5, 6, 2, 5, 2, 3 },
        { 0, 4, 5, 0, 5, 3 },
        { 0, 7, 4, 0, 1, 7 },
        { 7, 1, 2, 7, 2, 6 },
        { 4, 7, 6, 4, 6, 5}
    };

#define ADD_FACE(voxel, face) \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][0]]), N[face]); \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][1]]), N[face]); \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][2]]), N[face]); \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][3]]), N[face]); \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][4]]), N[face]); \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][5]]), N[face]); \

    for(int z = 0; z < size_z; ++z) {
        for(int y = 0; y < size_y; ++y) {
            for(int x = 0; x < size_x; ++x) {
                if(grid(x, y, z) == UNDEFINED_NODE) {
                    Vec3f voxel(x, y, z);
                    if(x == 0 || grid(x - 1, y, z) != UNDEFINED_NODE) {
                        ADD_FACE(voxel, 2);
                    }

                    if(y == 0 || grid(x, y - 1, z) != UNDEFINED_NODE) {
                        ADD_FACE(voxel, 3);
                    }

                    if(z == 0 || grid(x, y, z - 1) != UNDEFINED_NODE) {
                        ADD_FACE(voxel, 0);
                    }

                    if(x == size_x - 1 || grid(x + 1, y, z) != UNDEFINED_NODE) {
                        ADD_FACE(voxel, 4);
                    }

                    if(y == size_y - 1 || grid(x, y + 1, z) != UNDEFINED_NODE) {
                        ADD_FACE(voxel, 1);
                    }

                    if(z == size_z - 1 || grid(x, y, z + 1) != UNDEFINED_NODE) {
                        ADD_FACE(voxel, 5);
                    }
                }
            }
        }
    }

    m_nVertexCount = vertices.size();

    gloops::BufferBind arrayBuffer(GL_ARRAY_BUFFER, m_VBO);
    arrayBuffer.bufferData(vertices.size(), vertices.data(), GL_STATIC_DRAW);

    gloops::VertexArrayBind vao(m_VAO);
    vao.enableVertexAttribArray(0);
    vao.enableVertexAttribArray(1);
    vao.vertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), GLOOPS_OFFSETOF(Vertex, position));
    vao.vertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), GLOOPS_OFFSETOF(Vertex, normal));

#undef ADD_FACE
}
*/

void GLGrid::init(const Scene& scene, bool useClusteringColorGrid) {
    if(!scene.voxelSpace || scene.voxelSpace->getVoxelGrid().empty()) {
        return;
    }

    const VoxelGrid& grid = scene.voxelSpace->getVoxelGrid();

    AffineSpace3f transform = scene.voxelSpace->getLocalToWorldTransform();

    int size_x = grid.width();
    int size_y = grid.height();
    int size_z = grid.depth();

    std::vector<Vertex> vertices;

    const Vec3f P[] = {
        V1, V2, V3, V4, V5, V6, V7, V8
    };

    const Vec3f N[] = {
        xfmNormal(transform, N0),
        xfmNormal(transform, N1),
        xfmNormal(transform, N2),
        xfmNormal(transform, N3),
        xfmNormal(transform, N4),
        xfmNormal(transform, N5)
    };

    const uint32_t F[6][6] = {
        { 0, 3, 2, 0, 2, 1 },
        { 5, 6, 2, 5, 2, 3 },
        { 0, 4, 5, 0, 5, 3 },
        { 0, 7, 4, 0, 1, 7 },
        { 7, 1, 2, 7, 2, 6 },
        { 4, 7, 6, 4, 6, 5}
    };

#define ADD_FACE(voxel, face, color) \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][0]]), N[face], color); \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][1]]), N[face], color); \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][2]]), N[face], color); \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][3]]), N[face], color); \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][4]]), N[face], color); \
    vertices.emplace_back(xfmPoint(transform, voxel + P[F[face][5]]), N[face], color); \

    auto getColor = [&](int x, int y, int z) {
        if(useClusteringColorGrid) {
            return scene.voxelSpace->getClusteringNodeColor(
                        scene.voxelSpace->getClustering().getGrid()(x, y, z));
        }
        return scene.voxelSpace->getSkeletonNodeColor(
            scene.voxelSpace->getSkeleton().getGrid()(x, y, z));
    };

    for(int z = 0; z < size_z; ++z) {
        for(int y = 0; y < size_y; ++y) {
            for(int x = 0; x < size_x; ++x) {
                if(grid(x, y, z)) {
                    Vec3f voxel(x, y, z);
                    if(x == 0 || !grid(x - 1, y, z)) {
                        Col3f color(0, 0, 0);
                        if(x) {
                            color = getColor(x - 1, y, z);
                        }
                        ADD_FACE(voxel, 2, color);
                    }

                    if(y == 0 || !grid(x, y - 1, z)) {
                        Col3f color(0, 0, 0);
                        if(y) {
                            color = getColor(x, y - 1, z);
                        }
                        ADD_FACE(voxel, 3, color);
                    }

                    if(z == 0 || !grid(x, y, z - 1)) {
                        Col3f color(0, 0, 0);
                        if(z) {
                            color = getColor(x, y, z - 1);
                        }
                        ADD_FACE(voxel, 0, color);
                    }

                    if(x == size_x - 1 || !grid(x + 1, y, z)) {
                        Col3f color(0, 0, 0);
                        if(x < size_x - 1) {
                            color = getColor(x + 1, y, z);
                        }
                        ADD_FACE(voxel, 4, color);
                    }

                    if(y == size_y - 1 || !grid(x, y + 1, z)) {
                        Col3f color(0, 0, 0);
                        if(y < size_y - 1) {
                            color = getColor(x, y + 1, z);
                        }
                        ADD_FACE(voxel, 1, color);
                    }

                    if(z == size_z - 1 || !grid(x, y, z + 1)) {
                        Col3f color(0, 0, 0);
                        if(z < size_z - 1) {
                            color = getColor(x, y, z + 1);
                        }
                        ADD_FACE(voxel, 5, color);
                    }
                }
            }
        }
    }

    m_nVertexCount = vertices.size();

    gloops::BufferBind arrayBuffer(GL_ARRAY_BUFFER, m_VBO);
    arrayBuffer.bufferData(vertices.size(), vertices.data(), GL_STATIC_DRAW);

    gloops::VertexArrayBind vao(m_VAO);
    vao.enableVertexAttribArray(0);
    vao.enableVertexAttribArray(1);
    vao.enableVertexAttribArray(2);
    vao.vertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), GLOOPS_OFFSETOF(Vertex, position));
    vao.vertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), GLOOPS_OFFSETOF(Vertex, normal));
    vao.vertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), GLOOPS_OFFSETOF(Vertex, color));

#undef ADD_FACE
}

void GLGrid::render() const {
    gloops::VertexArrayBind(m_VAO).drawArrays(GL_TRIANGLES, 0, m_nVertexCount);
}

}
