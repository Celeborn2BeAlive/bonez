#ifndef _BONEZ_GLMATERIAL_HPP_
#define _BONEZ_GLMATERIAL_HPP_

#include <unordered_map>

#include "bonez/common/common.hpp"
#include "bonez/opengl/common.hpp"

#include "bonez/scene/shading/Material.hpp"

namespace BnZ {

struct GLMaterial {
    Col3f Kd; // Diffuse component
    Col3f Ks; // Glossy component
    float shininess; // Glossy exponent

    // Texture ids for components
    GLint KdTexture = 0;
    GLint KsTexture = 0;
    GLint shininessTexture = 0;

    // For bindless texturing
    GLuint64 KdTextureHandle = 0;
    GLuint64 KsTextureHandle = 0;
    GLuint64 shininessTextureHandle = 0;

    GLMaterial() {
    }
    
    GLMaterial(Col3f Kd, Col3f Ks, float shininess, GLint KdTexture, GLint KsTexture,
        GLint shininessTexture):
        Kd(Kd), Ks(Ks), shininess(shininess), KdTexture(KdTexture), KsTexture(KsTexture), 
        shininessTexture(shininessTexture) {
        if(KdTexture) {
            KdTextureHandle = glGetTextureHandleNV(KdTexture);
        }
        if(KsTexture) {
            KsTextureHandle = glGetTextureHandleNV(KsTexture);
        }
        if(shininessTexture) {
            shininessTextureHandle = glGetTextureHandleNV(shininessTexture);
        }
    }
};

struct GLMaterialUniforms {
    gloops::Uniform m_uKd;
    gloops::Uniform m_uKs;
    gloops::Uniform m_uShininess;
    gloops::Uniform m_uKdSampler;
    gloops::Uniform m_uKsSampler;
    gloops::Uniform m_uShininessSampler;

    GLMaterialUniforms(const gloops::Program& program);
};

class GLMaterialManager {
public:
    struct TextureUnits {
        GLint m_nDiffuseTexUnit;
        GLint m_nGlossyTexUnit;
        GLint m_nShininessTexUnit;

        TextureUnits(GLint diffuse, GLint glossy, GLint shininess):
            m_nDiffuseTexUnit(diffuse), m_nGlossyTexUnit(glossy), m_nShininessTexUnit(shininess) {
        }
    };

    void init(const Material* begin, const Material* end);

    const GLMaterial& getMaterial(size_t idx) const {
        if(idx >= m_Materials.size()) {
            return m_DefaultMaterial;
        }
        return m_Materials[idx];
    }
    
    void bindMaterial(size_t idx, GLMaterialUniforms& uniforms, const TextureUnits& units) const;

    // Set uniforms with texture handles
    void setMaterialUniforms(size_t idx, GLMaterialUniforms& uniforms) const;

private:
    typedef std::unordered_map<TextureImage, GLuint> TextureObjectCache;

    GLuint createTextureObject(const TextureImage& textureImage, TextureObjectCache& cache);

    void createWhiteTexture();

    std::vector<GLMaterial> m_Materials;
    std::vector<gloops::TextureObject> m_Textures;
    
    GLMaterial m_DefaultMaterial;
};


}

#endif
