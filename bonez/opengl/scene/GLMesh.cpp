#include "GLMesh.hpp"
#include "bonez/opengl/utils/embree_opengl.hpp"
#include <memory>

#include "bonez/scene/Scene.hpp"

namespace BnZ {

static const GLuint POSITION_LOCATION = 0;
static const GLuint NORMAL_LOCATION = 1;
static const GLuint TEXCOORDS_LOCATION = 2;
static const GLuint NODEINDEX_LOCATION = 3;

GLMesh::GLMesh(const TriangleMesh& mesh, const Scene& scene):
    m_nIndexCount(3 * mesh.triangleCount()), m_nMaterialIndex(mesh.getMaterialIndex()) {
    typedef std::unique_ptr<Vertex[]> VertexBuffer;
    typedef std::unique_ptr<GLuint[]> GLuintBuffer;
    
    VertexBuffer vb(new Vertex[mesh.vertexCount()]);
    Vertex* vbptr = vb.get();

    for (const TriangleMesh::Vertex & vertex: mesh.getVertices()) {
        vbptr->position = convert(vertex.position);
        vbptr->normal = convert(vertex.normal);
        vbptr->texCoords = convert(vertex.texCoords);
        if(scene.voxelSpace) {
            vbptr->nodeIndex = scene.voxelSpace->getClustering().getNearestNode(vertex.position, vertex.normal);
        } else {
            vbptr->nodeIndex = UNDEFINED_NODE;
        }
        ++vbptr;
    }
    
    GLuintBuffer ib(new GLuint[m_nIndexCount]);
    GLuint* ibptr = ib.get();

    for (const TriangleMesh::Triangle & triangle: mesh.getTriangles()) {
        ibptr[0] = triangle.first;
        ibptr[1] = triangle.second;
        ibptr[2] = triangle.third;
        ibptr += 3;
    }
    
    gloops::VertexArrayBind vaoBind(m_VAO);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO.glId());
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * m_nIndexCount, ib.get(), GL_STATIC_DRAW);
    
    gloops::BufferBind vboBind(GL_ARRAY_BUFFER, m_VBO);
    vboBind.bufferData(mesh.vertexCount(), vb.get(), GL_STATIC_DRAW);
    
    vaoBind.enableVertexAttribArray(POSITION_LOCATION);
    vaoBind.enableVertexAttribArray(NORMAL_LOCATION);
    vaoBind.enableVertexAttribArray(TEXCOORDS_LOCATION);
    vaoBind.enableVertexAttribArray(NODEINDEX_LOCATION);
    vaoBind.vertexAttribPointer(
        POSITION_LOCATION,
        3,
        GL_FLOAT,
        GL_FALSE,
        sizeof(Vertex),
        GLOOPS_OFFSETOF(Vertex, position)
    );
    vaoBind.vertexAttribPointer(
        NORMAL_LOCATION,
        3,
        GL_FLOAT,
        GL_FALSE,
        sizeof(Vertex),
        GLOOPS_OFFSETOF(Vertex, normal)
    );
    vaoBind.vertexAttribPointer(
        TEXCOORDS_LOCATION,
        2,
        GL_FLOAT,
        GL_FALSE,
        sizeof(Vertex),
        GLOOPS_OFFSETOF(Vertex, texCoords)
    );
    vaoBind.vertexAttribIPointer(
        NODEINDEX_LOCATION,
        1,
        GL_UNSIGNED_INT,
        sizeof(Vertex),
        GLOOPS_OFFSETOF(Vertex, nodeIndex)
    );
}

void GLMesh::render() const {
    gloops::VertexArrayBind(m_VAO).drawElements(GL_TRIANGLES, m_nIndexCount, GL_UNSIGNED_INT, 0);
}

}
