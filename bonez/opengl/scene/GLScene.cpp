#include "GLScene.hpp"
#include <gloops/gloops.hpp>

namespace BnZ {

void GLScene::init(const Scene& scene) {
    m_MaterialManager.init(scene.materials.data(), scene.materials.data() + scene.materials.size());
    m_Meshs.clear();
    for(const TriangleMesh& mesh: scene.geometry) {
        m_Meshs.emplace_back(GLMesh(mesh, scene));
    }
}


void GLScene::render(GLMaterialUniforms& materialUniforms, const GLMaterialManager::TextureUnits& units) const {
    // Render each mesh
    for(const GLMesh& mesh: m_Meshs) {
        m_MaterialManager.bindMaterial(mesh.getMaterialIndex(), materialUniforms, units);
        mesh.render();
    }
}

void GLScene::render() const {
    for(const GLMesh& mesh: m_Meshs) {
        mesh.render();
    }
}

void GLScene::render(GLMaterialUniforms& materialUniforms) const {
    // Render each mesh
    for(const GLMesh& mesh: m_Meshs) {
        m_MaterialManager.setMaterialUniforms(mesh.getMaterialIndex(), materialUniforms);
        mesh.render();
    }
}

}
