#ifndef _BONEZ_GLGRID_HPP
#define _BONEZ_GLGRID_HPP

#include "bonez/scene/Scene.hpp"
#include "bonez/opengl/common.hpp"

namespace BnZ {

class GLGrid {
public:
    struct Vertex {
        Vec3f position;
        Vec3f normal;
        Col3f color;

        Vertex(Vec3f position, Vec3f normal, Col3f color):
            position(position), normal(normal), color(color) {
        }
    };

    GLGrid(): m_nVertexCount(0) {
    }

    void init(const Scene& scene, bool useClusterGridColor);

    void render() const;

private:
    gloops::BufferObject m_VBO;
    gloops::VertexArrayObject m_VAO;
    size_t m_nVertexCount;
};

}

#endif // _BONEZ_GLGRID_HPP
