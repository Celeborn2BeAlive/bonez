#pragma once

#include "bonez/opengl/utils/GLutils.hpp"

namespace BnZ {

struct GLPoint {
    glm::vec3 position;
    glm::vec3 normal;
    float sqrRadius;

    GLPoint() {}
    GLPoint(glm::vec3 P, glm::vec3 N): position(P), normal(N), sqrRadius(0.f) {}
};

class GLPoints {
public:
    GLPoints();

    void fill(uint32_t count, const GLPoint* points);

    void setColors(const glm::vec3* colors);

    void render() const;

    void render(uint32_t numInstances) const;

    uint32_t getSize() const {
        return m_nCount;
    }
private:
    uint32_t m_nCount;
    gloops::BufferObject m_PositionNormalBO;
    gloops::BufferObject m_ColorBO;
    gloops::VertexArrayObject m_VAO;
};

}
