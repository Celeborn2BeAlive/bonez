#ifndef _BONEZ_GLSCENE_HPP_
#define _BONEZ_GLSCENE_HPP_

#include "GLMaterial.hpp"
#include "GLMesh.hpp"

#include "bonez/scene/Scene.hpp"

namespace BnZ {

class GLScene {
public:
    void init(const Scene& scene);

    void render(GLMaterialUniforms& materialUniforms, const GLMaterialManager::TextureUnits& units) const;

    // Render with no material
    void render() const;

    // Render using bindless texture access
    void render(GLMaterialUniforms& materialUniforms) const;

private:
    GLMaterialManager m_MaterialManager;
    std::vector<GLMesh> m_Meshs;
};

}

#endif
