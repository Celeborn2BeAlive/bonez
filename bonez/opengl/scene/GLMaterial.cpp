#include "GLMaterial.hpp"
#include "opengl/common.hpp"

#include <cassert>

using namespace gloops;

namespace BnZ {

GLMaterialUniforms::GLMaterialUniforms(const Program &program):
    m_uKd(program, "uMaterial.Kd"),
    m_uKs(program, "uMaterial.Ks"),
    m_uShininess(program, "uMaterial.shininess"),
    m_uKdSampler(program, "uMaterial.KdSampler"),
    m_uKsSampler(program, "uMaterial.KsSampler"),
    m_uShininessSampler(program, "uMaterial.shininessSampler") {
}

static void fillTextureObject(const TextureObject& textureObject, const TextureImage& textureImage) {
    GLenum format = GL_RGB;
    GLenum type;
    const void* data = nullptr;
    
    if(embree::Ref<embree::Image3c> image = textureImage.getImage().dynamicCast<embree::Image3c>()) {
        if(sizeof(embree::Col3c) == 4 * sizeof(unsigned char)) {
            format = GL_RGBA;
        }
        type = GL_UNSIGNED_BYTE;
        data = image->ptr();
    } else if(embree::Ref<embree::Image3f> image = textureImage.getImage().dynamicCast<embree::Image3f>()) {
        if(sizeof(embree::Col3f) == 4 * sizeof(float)) {
            format = GL_RGBA;
        }
        type = GL_FLOAT;
        data = image->ptr();
    }
    
    assert(data);

    glTextureParameteriEXT(textureObject.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MIN_FILTER,
                           GL_NEAREST_MIPMAP_NEAREST);
    glTextureParameteriEXT(textureObject.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MAG_FILTER,
                           GL_LINEAR);
    glTextureParameteriEXT(textureObject.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_WRAP_S,
                           GL_REPEAT);
    glTextureParameteriEXT(textureObject.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_WRAP_T,
                           GL_REPEAT);

    glTextureImage2DEXT(textureObject.glId(),
                        GL_TEXTURE_2D,
                        0,
                        GL_RGB,
                        textureImage.width(),
                        textureImage.height(),
                        0,
                        format,
                        type,
                        data);

    glGenerateTextureMipmapEXT(textureObject.glId(), GL_TEXTURE_2D);

    glMakeTextureHandleResidentNV(glGetTextureHandleNV(textureObject.glId()));
}

GLuint GLMaterialManager::createTextureObject(const TextureImage& textureImage, TextureObjectCache& cache) {
    if(!textureImage) {
        return m_Textures[0].glId();
    }

    auto cacheIt = cache.find(textureImage);
    if(cacheIt != std::end(cache)) {
        // if the texture object is in the cache, return its GL id
        return (*cacheIt).second;
    }
    
    // If not, create a new texture object, fill it and add its GL id to the cache
    m_Textures.emplace_back(TextureObject());
    fillTextureObject(m_Textures.back(), textureImage);

    return (cache[textureImage] = m_Textures.back().glId());
}

void GLMaterialManager::createWhiteTexture() {
    m_Textures.emplace_back(TextureObject());
    
    glTextureParameteriEXT(m_Textures.back().glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MIN_FILTER,
                           GL_LINEAR);
    glTextureParameteriEXT(m_Textures.back().glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MAG_FILTER,
                           GL_LINEAR);

    float white[] = { 1.f, 1.f, 1.f };
    glTextureImage2DEXT(m_Textures.back().glId(),
                        GL_TEXTURE_2D,
                        0,
                        GL_RGB,
                        1,
                        1,
                        0,
                        GL_RGB,
                        GL_FLOAT,
                        white);

    glMakeTextureHandleResidentNV(glGetTextureHandleNV(m_Textures.back().glId()));
}

void GLMaterialManager::init(const Material* begin, const Material* end) {
    m_Textures.clear();
    m_Materials.clear();
    
    createWhiteTexture();
    
    // A cache is used to avoid creating multiple texture object for the same image
    TextureObjectCache cache;
    
    for(auto it = begin; it != end; ++it) {
        m_Materials.emplace_back(
            GLMaterial(
                it->getKd(), 
                it->getKs(), 
                it->getShininess(),
                createTextureObject(it->getKdTexture(), cache), 
                createTextureObject(it->getKsTexture(), cache), 
                createTextureObject(it->getShininessTexture(), cache)
            )
        );
    }
    
    // The default material:
    m_DefaultMaterial = GLMaterial(
        Col3f(0.f), 
        Col3f(0.f), 
        1.f,
        0,
        0,
        0
    );
}

void GLMaterialManager::bindMaterial(size_t idx, GLMaterialUniforms& uniforms, const TextureUnits& units) const {
    const GLMaterial& material = getMaterial(idx);
    uniforms.m_uKd.set(convert(material.Kd));
    uniforms.m_uKs.set(convert(material.Ks));
    uniforms.m_uShininess.set(material.shininess);

    glActiveTexture(GL_TEXTURE0 + units.m_nDiffuseTexUnit);
    glBindTexture(GL_TEXTURE_2D, material.KdTexture);
    uniforms.m_uKdSampler.set(units.m_nDiffuseTexUnit);
    
    glActiveTexture(GL_TEXTURE0 + units.m_nGlossyTexUnit);
    glBindTexture(GL_TEXTURE_2D, material.KsTexture);
    uniforms.m_uKsSampler.set(units.m_nGlossyTexUnit);
    
    glActiveTexture(GL_TEXTURE0 + units.m_nShininessTexUnit);
    glBindTexture(GL_TEXTURE_2D, material.shininessTexture);
    uniforms.m_uShininessSampler.set(units.m_nShininessTexUnit);
}

void GLMaterialManager::setMaterialUniforms(size_t idx, GLMaterialUniforms& uniforms) const {
    const GLMaterial& material = getMaterial(idx);
    uniforms.m_uKd.set(convert(material.Kd));
    uniforms.m_uKs.set(convert(material.Ks));
    uniforms.m_uShininess.set(material.shininess);
    uniforms.m_uKdSampler.set(material.KdTextureHandle);
    uniforms.m_uKsSampler.set(material.KsTextureHandle);
    uniforms.m_uShininessSampler.set(material.shininessTextureHandle);
}

}
