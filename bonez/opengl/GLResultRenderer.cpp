#include "GLResultRenderer.hpp"

namespace BnZ {

static const GLchar* VS_SOURCE =
"#version 330\n" \
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    out vec2 vTexCoords;

    void main() {
        vTexCoords = 0.5 * vec2(1 + aPosition.x, 1 - aPosition.y);
        gl_Position = vec4(aPosition, 0.f, 1.f);
    }
);

static const GLchar* FS_SOURCE =
"#version 430\n"
"#extension GL_NV_bindless_texture : enable\n"
GLOOPS_STRINGIFY(
    in vec2 vTexCoords;

    uniform sampler2D uImage;
    uniform float uGamma;

    out vec3 fFragColor;

    void main() {
        fFragColor = pow(texture(uImage, vTexCoords).rgb, vec3(1.f / uGamma));
    }
);

GLResultRenderer::GLResultRenderer() {
    if(!gloops::buildProgram(VS_SOURCE, FS_SOURCE, m_ResultProgram)) {
        exit(-1);
    }

    m_uGamma = gloops::Uniform(m_ResultProgram, "uGamma", true);
    m_uImage = gloops::Uniform(m_ResultProgram, "uImage", true);

    GLfloat quad[] = {
        -1, -1,
         1, -1,
         1,  1,
        -1,  1
    };

    gloops::BufferBind arrayBuffer(GL_ARRAY_BUFFER, m_QuadVBO);
    arrayBuffer.bufferData(8, quad, GL_STATIC_DRAW);

    gloops::VertexArrayBind bindedVAO(m_QuadVAO);
    bindedVAO.enableVertexAttribArray(0);
    bindedVAO.vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
}

void GLResultRenderer::drawResult(const gloops::Viewport& viewport,
                                  float gamma,
                                  const Framebuffer& framebuffer) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    m_ResultProgram.use();

    gloops::setViewport(viewport);

    gloops::TextureObject m_ResultTexture;

    glTextureParameteriEXT(m_ResultTexture.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MIN_FILTER,
                           GL_LINEAR);
    glTextureParameteriEXT(m_ResultTexture.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MAG_FILTER,
                           GL_LINEAR);

    glTextureImage2DEXT(m_ResultTexture.glId(),
                        GL_TEXTURE_2D,
                        0,
                        GL_RGB,
                        framebuffer.getWidth(),
                        framebuffer.getHeight(),
                        0,
                        GL_RGBA,
                        GL_FLOAT,
                        framebuffer.getPixels());

    m_uGamma.set(gamma);

    GLuint64 m_nTextureHandle = glGetTextureHandleNV(m_ResultTexture.glId());
    glMakeTextureHandleResidentNV(m_nTextureHandle);

    glUniformHandleui64NV(m_uImage.location(), m_nTextureHandle);

    gloops::VertexArrayBind(m_QuadVAO).drawArrays(GL_TRIANGLE_FAN, 0, 4);
}

}
