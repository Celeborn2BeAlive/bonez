#pragma once

#include "embree_opengl.hpp"
#include "GLFramebuffer.hpp"
#include "GLQuery.hpp"
#include "GLBuffer.hpp"
#include "GLState.hpp"
#include "GLTextureBuffer.hpp"
#include "GLUniform.hpp"
#include "GLVertexArray.hpp"

namespace BnZ {

// A full screen triangle
struct ScreenTriangle {
    gloops::BufferObject vbo;
    gloops::VertexArrayObject vao;

    ScreenTriangle() {
        float vertices[] = {
            -1, -1,
             3, -1,
            -1,  3
        };

        gloops::BufferBind vboBind(GL_ARRAY_BUFFER, vbo);
        vboBind.bufferData(3 * 2, vertices, GL_STATIC_DRAW);

        gloops::VertexArrayBind vaoBind(vao);

        vaoBind.enableVertexAttribArray(0);
        vaoBind.vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    }

    void render() const {
        glBindVertexArray(vao.glId());
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }
};

}
