#pragma once

#include <GL/glew.h>

namespace BnZ {

template<typename Factory>
class GLObject {
    GLuint m_GLId;
public:
    GLObject() {
        Factory::genObjects(1, &m_GLId);
    }

    ~GLObject() {
        Factory::deleteObjects(1, &m_GLId);
    }

    GLObject(const GLObject&) = delete;

    GLObject& operator =(const GLObject&) = delete;

    GLObject(GLObject&& rvalue): m_GLId(rvalue.m_GLId) {
        rvalue.m_GLId = 0;
    }

    GLObject& operator =(GLObject&& rvalue) {
        this->~GLObject();
        m_GLId = rvalue.m_GLId;
        rvalue.m_GLId = 0;
        return *this;
    }

    GLuint glId() const {
        return m_GLId;
    }
};

struct GLTextureFactory {
    static void genObjects(GLsizei n, GLuint* textures) {
        glGenTextures(n, textures);
    }

    static void deleteObjects(GLsizei n, GLuint* textures) {
        glDeleteTextures(n, textures);
    }
};

using GLTextureObject = GLObject<GLTextureFactory>;

struct GLFramebufferFactory {
    static void genObjects(GLsizei n, GLuint* fb) {
        glGenFramebuffers(n, fb);
    }

    static void deleteObjects(GLsizei n, GLuint* fb) {
        glDeleteFramebuffers(n, fb);
    }
};

using GLFramebufferObject = GLObject<GLFramebufferFactory>;

struct GLTransformFeedbackFactory {
    static void genObjects(GLsizei n, GLuint* tf) {
        glGenTransformFeedbacks(n, tf);
    }

    static void deleteObjects(GLsizei n, GLuint* tf) {
        glDeleteTransformFeedbacks(n, tf);
    }
};

using GLTransformFeedbackObject = GLObject<GLTransformFeedbackFactory>;

struct GLVertexArrayFactory {
    static void genObjects(GLsizei n, GLuint* o) {
        glGenVertexArrays(n, o);
    }

    static void deleteObjects(GLsizei n, GLuint* o) {
        glDeleteVertexArrays(n, o);
    }
};

using GLVertexArrayObject = GLObject<GLVertexArrayFactory>;

}
