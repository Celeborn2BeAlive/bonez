#pragma once

#include "bonez/opengl/common.hpp"

namespace BnZ {

class GLBuffer {
public:
    gloops::BufferObject m_BufferObject;
    bool m_bIsResident;
    GLenum m_Access;

    GLBuffer(): m_bIsResident(false) {
    }

    GLuint glId() const {
        return m_BufferObject.glId();
    }

    // size in bytes
    void setSize(size_t size, GLenum usage) {
        bool wasResident = m_bIsResident;
        if(m_bIsResident) {
            makeNonResident();
        }

        glNamedBufferDataEXT(m_BufferObject.glId(), size, nullptr, usage);

        if(wasResident) {
            makeResident(m_Access);
        }
    }

    template<typename T>
    void setData(size_t count, const T* data, GLenum usage) {
        bool wasResident = m_bIsResident;
        if(m_bIsResident) {
            makeNonResident();
        }

        glNamedBufferDataEXT(m_BufferObject.glId(), count * sizeof(T), data, usage);

        if(wasResident) {
            makeResident(m_Access);
        }
    }

    template<typename T>
    void setData(const std::vector<T>& data, GLenum usage) {
        setData(data.size(), data.data(), usage);
    }

    template<typename T>
    void setSubData(uint32_t offset, size_t count, const T* data) {
        glNamedBufferSubDataEXT(m_BufferObject.glId(), offset * sizeof(T), count * sizeof(T), data);
    }

    template<typename T>
    void setSubData(const std::vector<T>& data) {
        setSubData(0, data.size(), data.data());
    }

    void bindBase(GLuint index, GLenum target) const {
        glBindBufferBase(target, index, m_BufferObject.glId());
    }

    void bind(GLenum target) const {
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_BufferObject.glId());
    }

    GLuint64 getGPUAddress() const {
        GLuint64 address;
        glGetNamedBufferParameterui64vNV(m_BufferObject.glId(), GL_BUFFER_GPU_ADDRESS_NV, &address);
        return address;
    }

    void makeResident(GLenum access) {
        if(!m_bIsResident) {
            glMakeNamedBufferResidentNV(m_BufferObject.glId(), access);
            m_bIsResident = true;
            m_Access = access;
        }
    }

    void makeNonResident() {
        if(m_bIsResident) {
            glMakeNamedBufferNonResidentNV(m_BufferObject.glId());
            m_bIsResident = false;
        }
    }

    template<typename T>
    T* map(GLenum access) {
        return (T*) glMapNamedBufferEXT(m_BufferObject.glId(), access);
    }

    void unmap() {
        glUnmapNamedBufferEXT(m_BufferObject.glId());
    }

    template<typename T>
    void getData(size_t count, T* data) {
        glGetNamedBufferSubDataEXT(m_BufferObject.glId(), 0, count * sizeof(T), data);
    }
};

}
