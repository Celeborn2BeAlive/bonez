#ifndef _BONEZ_GLTEXTUREBUFFER_HPP
#define _BONEZ_GLTEXTUREBUFFER_HPP

#include <gloops/gloops.hpp>

namespace BnZ {

class GLTextureBuffer {
public:
    /**
     * @brief bind Bind the texture buffer to the target GL_TEXTURE_BUFFER of the
     * current texture unit.
     */
    void bind() {
        glBindTexture(GL_TEXTURE_BUFFER, m_TextureObject.glId());
        GLOOPS_CHECK_ERROR;
    }

    /**
     * @brief bind Active the texture unit textureUnit and bind the texture buffer to the target
     * GL_TEXTURE_BUFFER.
     * @param textureUnit
     */
    void bind(int textureUnit) {
        glActiveTexture(GL_TEXTURE0 + textureUnit);
        bind();
    }

    /**
     * @brief setData Set the data of the texture buffer.
     * @param count Number of elements pointed by data
     * @param data Pointer to the first element
     * @param format Specifies the internal format of the data
     *
     * Prerequiste: The buffer must have been bound to the current texture unit.
     * Side effect: The buffer object bound to the target GL_TEXTURE_BUFFER become m_BufferObject
     */
    template<typename T>
    void setData(size_t count, const T* data, GLenum internalFormat) {
        glBindBuffer(GL_TEXTURE_BUFFER, m_BufferObject.glId());
        glBufferData(GL_TEXTURE_BUFFER, count * sizeof(T), data, GL_STATIC_DRAW);
        glTexBuffer(GL_TEXTURE_BUFFER, internalFormat, m_BufferObject.glId());
        GLOOPS_CHECK_ERROR;
    }

    const gloops::BufferObject& getBufferObject() const {
        return m_BufferObject;
    }

    const gloops::TextureObject& getTextureObject() const {
        return m_TextureObject;
    }

private:
    gloops::BufferObject m_BufferObject;
    gloops::TextureObject m_TextureObject;
};

}

#endif // _BONEZ_GLTEXTUREBUFFER_HPP
