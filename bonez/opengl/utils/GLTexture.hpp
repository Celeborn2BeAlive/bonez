#pragma once

#include <GL/glew.h>
#include "GLObject.hpp"

namespace BnZ {

template<GLenum textureType>
class GLTexture: GLTextureObject {
    GLuint64 m_nTextureHandle = 0;
    GLuint64 m_nImageHandle = 0;
public:
    using GLTextureObject::glId;

    static const GLenum TEXTURE_TYPE = textureType;

    void getImage(GLint level, GLenum format, GLenum type, GLvoid *pixels) const {
        glGetTextureImageEXT(glId(), TEXTURE_TYPE, level, format, type, pixels);
    }

    void setParameter(GLenum pname, GLfloat param) {
        glTextureParameterfEXT(glId(), TEXTURE_TYPE, pname, param);
    }

    void setParameter(GLenum pname, GLint param) {
        glTextureParameteriEXT(glId(), TEXTURE_TYPE, pname, param);
    }

    void setMinFilter(GLint param) {
        setParameter(GL_TEXTURE_MIN_FILTER, param);
    }

    void setMagFilter(GLint param) {
        setParameter(GL_TEXTURE_MAG_FILTER, param);
    }

    void setWrapS(GLint param) {
        setParameter(GL_TEXTURE_WRAP_S, param);
    }

    void setWrapT(GLint param) {
        setParameter(GL_TEXTURE_WRAP_T, param);
    }

    void setCompareMode(GLint param) {
        setParameter(GL_TEXTURE_COMPARE_MODE, param);
    }

    void setCompareFunc(GLint param) {
        setParameter(GL_TEXTURE_COMPARE_FUNC, param);
    }

    GLuint64 makeTextureHandleResident() {
        m_nTextureHandle = glGetTextureHandleNV(glId());
        glMakeTextureHandleResidentNV(m_nTextureHandle);
        return m_nTextureHandle;
    }

    void makeTextureHandleNonResident() {
        glMakeTextureHandleNonResidentNV(m_nTextureHandle);
        m_nTextureHandle = 0;
    }

    GLuint64 getTextureHandle() const {
        return m_nTextureHandle;
    }

    bool isTextureHandleResident() const {
        return m_nTextureHandle != 0;
    }

    GLuint64 makeImageHandleResident(GLint level, GLboolean layered, GLint layer, GLenum format, GLenum access) {
        m_nImageHandle = glGetImageHandleNV(glId(), level, layered, layer, format);
        glMakeImageHandleResidentNV(m_nImageHandle, access);
        return m_nImageHandle;
    }

    void makeImageHandleNonResident() {
        glMakeImageHandleNonResidentNV(m_nImageHandle);
        m_nImageHandle = 0;
    }

    GLuint64 getImageHandle() const {
        return m_nImageHandle;
    }

    bool isImageHandleResident() const {
        return m_nImageHandle != 0;
    }
};

class GLTexture2D: public GLTexture<GL_TEXTURE_2D> {
public:
    void setImage(GLint level, GLint internalFormat, GLsizei width, GLsizei height,
                  GLint border, GLenum format, GLenum type, const GLvoid* data) {
        glTextureImage2DEXT(glId(), TEXTURE_TYPE, level, internalFormat, width, height,
                            border, format, type, data);
    }
};

class GLTextureCubeMap: public GLTexture<GL_TEXTURE_CUBE_MAP> {
public:
    void setImage(GLint face, GLint level, GLint internalFormat, GLsizei width, GLsizei height,
                  GLint border, GLenum format, GLenum type, const GLvoid* data) {
        glTextureImage2DEXT(glId(), GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, level, internalFormat, width, height,
                            border, format, type, data);
    }
};

}
