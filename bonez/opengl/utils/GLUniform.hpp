#ifndef BONEZ_GLUNIFORM_HPP
#define BONEZ_GLUNIFORM_HPP

#include <gloops/gloops.hpp>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace BnZ {

template<typename T>
struct GLUniformHelper;

#define GEN_UNIFORM_HELPER(TYPE, FUNCTION) \
    template<> \
    struct GLUniformHelper<TYPE> { \
        static void set(GLuint programID, GLint location, TYPE v0) { \
            glProgram##FUNCTION(programID, location, v0); \
        } \
        static void set(GLint location, TYPE v0) { \
            gl##FUNCTION(location, v0); \
        } \
    }

GEN_UNIFORM_HELPER(float, Uniform1f);
GEN_UNIFORM_HELPER(int, Uniform1i);
GEN_UNIFORM_HELPER(unsigned int, Uniform1ui);
GEN_UNIFORM_HELPER(bool, Uniform1i);
GEN_UNIFORM_HELPER(GLuint64, Uniform1ui64NV);

#undef GEN_UNIFORM_HELPER

// For glm vector types:
#define GEN_UNIFORM_HELPER(TYPE, FUNCTION) \
    template<> \
    struct GLUniformHelper<TYPE> { \
        static void set(GLuint programID, GLint location, TYPE value) { \
            glProgram##FUNCTION(programID, location, 1, glm::value_ptr(value)); \
        } \
        static void set(GLint location, TYPE value) { \
            gl##FUNCTION(location, 1, glm::value_ptr(value)); \
        } \
    }

GEN_UNIFORM_HELPER(glm::vec2, Uniform2fv);
GEN_UNIFORM_HELPER(glm::vec3, Uniform3fv);
GEN_UNIFORM_HELPER(glm::vec4, Uniform4fv);
GEN_UNIFORM_HELPER(glm::ivec2, Uniform2iv);
GEN_UNIFORM_HELPER(glm::ivec3, Uniform3iv);
GEN_UNIFORM_HELPER(glm::ivec4, Uniform4iv);
GEN_UNIFORM_HELPER(glm::uvec2, Uniform2uiv);
GEN_UNIFORM_HELPER(glm::uvec3, Uniform3uiv);
GEN_UNIFORM_HELPER(glm::uvec4, Uniform4uiv);

#undef GEN_UNIFORM_HELPER

// For glm matrix types:
#define GEN_UNIFORM_HELPER(TYPE, FUNCTION) \
    template<> \
    struct GLUniformHelper<TYPE> { \
        static void set(GLuint programID, GLint location, TYPE value) { \
            glProgram##FUNCTION(programID, location, 1, GL_FALSE, glm::value_ptr(value)); \
        } \
        static void set(GLint location, TYPE value) { \
            gl##FUNCTION(location, 1, GL_FALSE, glm::value_ptr(value)); \
        } \
    }

GEN_UNIFORM_HELPER(glm::mat2, UniformMatrix2fv);
GEN_UNIFORM_HELPER(glm::mat3, UniformMatrix3fv);
GEN_UNIFORM_HELPER(glm::mat4, UniformMatrix4fv);
GEN_UNIFORM_HELPER(glm::mat2x3, UniformMatrix2x3fv);
GEN_UNIFORM_HELPER(glm::mat3x2, UniformMatrix3x2fv);
GEN_UNIFORM_HELPER(glm::mat2x4, UniformMatrix2x4fv);
GEN_UNIFORM_HELPER(glm::mat4x2, UniformMatrix4x2fv);
GEN_UNIFORM_HELPER(glm::mat3x4, UniformMatrix3x4fv);
GEN_UNIFORM_HELPER(glm::mat4x3, UniformMatrix4x3fv);

#undef GEN_UNIFORM_HELPER

// For simple array types
#define GEN_UNIFORM_HELPER(TYPE, FUNCTION) \
    template<> \
    struct GLUniformHelper<TYPE[]> { \
        static void set(GLuint programID, GLint location, GLsizei count, const TYPE* value) { \
            glProgram##FUNCTION(programID, location, count, value); \
        } \
        static void set(GLint location, GLsizei count, const TYPE* value) { \
            gl##FUNCTION(location, count, value); \
        } \
    }

GEN_UNIFORM_HELPER(float, Uniform1fv);
GEN_UNIFORM_HELPER(int, Uniform1iv);
GEN_UNIFORM_HELPER(unsigned int, Uniform1uiv);
GEN_UNIFORM_HELPER(GLuint64, Uniform1ui64vNV);

#undef GEN_UNIFORM_HELPER

// For glm array types:
#define GEN_UNIFORM_HELPER(TYPE, FUNCTION) \
    template<> \
    struct GLUniformHelper<TYPE[]> { \
        static void set(GLuint programID, GLint location, GLsizei count, const TYPE* value) { \
            glProgram##FUNCTION(programID, location, count, glm::value_ptr(value[0])); \
        } \
        static void set(GLint location, GLsizei count, const TYPE* value) { \
            gl##FUNCTION(location, count, glm::value_ptr(value[0])); \
        } \
    }

GEN_UNIFORM_HELPER(glm::vec2, Uniform2fv);
GEN_UNIFORM_HELPER(glm::vec3, Uniform3fv);
GEN_UNIFORM_HELPER(glm::vec4, Uniform4fv);
GEN_UNIFORM_HELPER(glm::ivec2, Uniform2iv);
GEN_UNIFORM_HELPER(glm::ivec3, Uniform3iv);
GEN_UNIFORM_HELPER(glm::ivec4, Uniform4iv);
GEN_UNIFORM_HELPER(glm::uvec2, Uniform2uiv);
GEN_UNIFORM_HELPER(glm::uvec3, Uniform3uiv);
GEN_UNIFORM_HELPER(glm::uvec4, Uniform4uiv);

#undef GEN_UNIFORM_HELPER

// For glm array matrix types:
#define GEN_UNIFORM_HELPER(TYPE, FUNCTION) \
    template<> \
    struct GLUniformHelper<TYPE[]> { \
        static void set(GLuint programID, GLint location, GLsizei count, const TYPE* value) { \
            glProgram##FUNCTION(programID, location, count, GL_FALSE, glm::value_ptr(value[0])); \
        } \
        static void set(GLint location, GLsizei count, const TYPE* value) { \
            gl##FUNCTION(location, count, GL_FALSE, glm::value_ptr(value[0])); \
        } \
    }

GEN_UNIFORM_HELPER(glm::mat2, UniformMatrix2fv);
GEN_UNIFORM_HELPER(glm::mat3, UniformMatrix3fv);
GEN_UNIFORM_HELPER(glm::mat4, UniformMatrix4fv);
GEN_UNIFORM_HELPER(glm::mat2x3, UniformMatrix2x3fv);
GEN_UNIFORM_HELPER(glm::mat3x2, UniformMatrix3x2fv);
GEN_UNIFORM_HELPER(glm::mat2x4, UniformMatrix2x4fv);
GEN_UNIFORM_HELPER(glm::mat4x2, UniformMatrix4x2fv);
GEN_UNIFORM_HELPER(glm::mat3x4, UniformMatrix3x4fv);
GEN_UNIFORM_HELPER(glm::mat4x3, UniformMatrix4x3fv);

#undef GEN_UNIFORM_HELPER

template<typename T>
class GLUniform {
public:
    GLUniform(const gloops::Program& program, const GLchar* name):
        m_nLocation(program.getUniformLocation(name)) {
        if(m_nLocation < 0) {
            std::clog << "WARNING: uniform '" << name << "' not found in the program "
                << program.glId() << std::endl;
        }
    }

    GLint location() const {
        return m_nLocation;
    }

    void set(T value) {
        GLUniformHelper<T>::set(m_nLocation, value);
    }

    void set(const gloops::Program& program, T value) {
        GLUniformHelper<T>::set(program.glId(), m_nLocation, value);
    }

private:
    GLint m_nLocation;
};

template<typename T>
class GLUniform<T[]> {
public:
    GLUniform(const gloops::Program& program, const GLchar* name):
        m_nLocation(program.getUniformLocation(name)) {
        if(m_nLocation < 0) {
            std::clog << "WARNING: uniform '" << name << "' not found in the program "
                << program.glId() << std::endl;
        }
    }

    GLint location() const {
        return m_nLocation;
    }

    void set(GLsizei count, const T* value) {
        GLUniformHelper<T[]>::set(m_nLocation, count, value);
    }

    void set(const gloops::Program& program, GLsizei count, const T* value) {
        GLUniformHelper<T[]>::set(program.glId(), m_nLocation, count, value);
    }

private:
    GLint m_nLocation;
};

#define BNZ_GLUNIFORM(PROGRAM, TYPE, NAME) GLUniform<TYPE> NAME { PROGRAM, #NAME }

}

#endif // BONEZ_GLUNIFORM_HPP
