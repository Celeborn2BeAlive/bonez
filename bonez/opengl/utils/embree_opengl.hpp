#ifndef _BONEZ_OPENGL_UTILS_EMBREE_OPENGL_HPP_
#define _BONEZ_OPENGL_UTILS_EMBREE_OPENGL_HPP_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "bonez/renderer/Framebuffer.hpp"
#include "bonez/common/cameras/ProjectiveCamera.hpp"
#include "bonez/common/common.hpp"

namespace BnZ {

inline GLenum getEmbreeColorFormat() {
    if(sizeof(Col3f) == 4 * sizeof(GLfloat)) {
        return GL_RGBA32F;
    }
    return GL_RGB32F;
}

inline glm::vec3 convert(const Vec3f& v) {
    return glm::vec3(v.x, v.y, v.z);
}

inline Vec3f convert(const glm::vec3& v) {
    return Vec3f(v.x, v.y, v.z);
}

inline Vec3f convert(const glm::vec4& v) {
    return Vec3f(v.x, v.y, v.z);
}

inline glm::vec2 convert(const Vec2f& v) {
    return glm::vec2(v.x, v.y);
}

inline glm::vec3 convert(const Col3f& c) {
    return glm::vec3(c.r, c.g, c.b);
}

inline float getZNear() {
    return 3.f;
}

inline float getZFar() {
    return 3000.f;
}

inline float getFOVY() {
    return 45.f;
}

inline glm::mat4 getProjectionMatrix(const Framebuffer& fb) {
    return glm::perspective(getFOVY(), fb.getAspectRatio(), getZNear(), getZFar());
}

inline glm::mat4 convert(const AffineSpace3f& transform) {
    return glm::mat4(
               glm::vec4(transform.l.vx.x, transform.l.vx.y, transform.l.vx.z, 0.f),
               glm::vec4(transform.l.vy.x, transform.l.vy.y, transform.l.vy.z, 0.f),
               glm::vec4(transform.l.vz.x, transform.l.vz.y, transform.l.vz.z, 0.f),
               glm::vec4(transform.p.x, transform.p.y, transform.p.z, 1.f));
}

inline AffineSpace3f convert(const glm::mat4& M) {
    return AffineSpace3f(LinearSpace3f(convert(glm::vec3(M[0])),
                         convert(glm::vec3(M[1])),
            convert(glm::vec3(M[2]))), convert(glm::vec3(M[3])));
}

inline glm::mat4 computeGLProjectionMatrix(const ProjectiveCamera& camera) {
    return camera.getProjectionMatrix();
}

inline glm::mat4 computeGLViewMatrix(const ProjectiveCamera& camera) {
    //return glm::lookAt(convert(camera.getPosition()), convert(camera.getPoint()), convert(camera.getUp()));

    glm::mat4 cameraLocalMatrix = convert(camera.getLocalToWorldTransform());
    // Conversion from embree lookAt matrix to opengl lookAt matrix (must be rotated and inversed)
    return glm::inverse(glm::rotate(cameraLocalMatrix, 180.f, glm::vec3(0.f, 1.f, 0.f)));
}

inline AffineSpace3f computeEmbreeLocalToWorld(const glm::mat4& viewMatrix) {
    glm::mat4 localToWorld = glm::rotate(glm::inverse(viewMatrix), -180.f, glm::vec3(0.f, 1.f, 0.f));
    return convert(localToWorld);
}

}

#endif
