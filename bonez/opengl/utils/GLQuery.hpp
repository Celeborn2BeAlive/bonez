#ifndef _BONEZ_GLQUERY_HPP
#define _BONEZ_GLQUERY_HPP

#include "../common.hpp"

namespace BnZ {

class GLQuery {
public:
    GLQuery() {
        glGenQueries(1, &m_nId);
        glBeginQuery(GL_TIME_ELAPSED, m_nId);
        glEndQuery(GL_TIME_ELAPSED);
    }

    ~GLQuery() {
        glDeleteQueries(1, &m_nId);
    }

    GLQuery(const GLQuery&) = delete;

    GLQuery& operator =(const GLQuery&) = delete;

    GLQuery(GLQuery&& query): m_nId(query.m_nId) {
        query.m_nId = 0;
    }

    GLQuery& operator =(GLQuery&& query) {
        m_nId = query.m_nId;
        query.m_nId = 0;
        return *this;
    }

    GLuint glId() const {
        return m_nId;
    }

    bool isResultAvailable() const {
        GLint done;
        glGetQueryObjectiv(m_nId, GL_QUERY_RESULT_AVAILABLE, &done);
        return done;
    }

    GLuint64 getResult() const {
        GLuint64 result;
        glGetQueryObjectui64v(m_nId, GL_QUERY_RESULT, &result);
        return result;
    }

    GLuint64 waitResult() const {
        GLint done = false;
        while(!done) {
            glGetQueryObjectiv(m_nId, GL_QUERY_RESULT_AVAILABLE, &done);
        }
        return getResult();
    }

    void queryCounter() const {
        glQueryCounter(m_nId, GL_TIMESTAMP);
    }

    void begin(GLenum type) {
        glBeginQuery(type, m_nId);
    }

private:
    GLuint m_nId;
};

class GLTimer {
public:
    GLTimer(const GLQuery& query) {
        glBeginQuery(GL_TIME_ELAPSED, query.glId());
    }

    ~GLTimer() {
        glEndQuery(GL_TIME_ELAPSED);
    }
};

}

#endif // _BONEZ_GLQUERY_HPP
