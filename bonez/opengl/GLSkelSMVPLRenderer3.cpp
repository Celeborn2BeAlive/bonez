#include "GLSkelSMVPLRenderer3.hpp"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "bonez/opengl/utils/embree_opengl.hpp"

#include "bonez/common/Progress.hpp"

namespace BnZ {

using namespace gloops;

const GLchar* GLSkelSMVPLRenderer3::GeometryPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec3 aPosition;
    layout(location = 1) in vec3 aNormal;
    layout(location = 2) in vec2 aTexCoords;

    uniform mat4 uMVPMatrix;
    uniform mat4 uMVMatrix;
    uniform mat3 uNormalMatrix;

    out vec3 vPosition;
    out vec3 vNormal;
    out vec2 vTexCoords;
    out vec3 vWorldSpaceNormal;
    out vec3 vWorldSpacePosition;

    void main() {
        vWorldSpacePosition = aPosition;
        vWorldSpaceNormal = aNormal;
        vPosition = vec3(uMVMatrix * vec4(aPosition, 1));
        vNormal = uNormalMatrix * aNormal;
        vTexCoords = aTexCoords;

        gl_Position = uMVPMatrix * vec4(aPosition, 1);
    }
);

const GLchar* GLSkelSMVPLRenderer3::GeometryPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    in vec3 vPosition;
    in vec3 vNormal;
    in vec2 vTexCoords;
    in vec3 vWorldSpacePosition;
    in vec3 vWorldSpaceNormal;

    struct Material {
        vec3 Kd;
        vec3 Ks;
        float shininess;
        sampler2D KdSampler;
        sampler2D KsSampler;
        sampler2D shininessSampler;
    };

    uniform Material uMaterial;

    uniform float uSy;
    uniform float uHalfFOVY;
    uniform float uZNear;
    uniform float uZFar;

    struct VoxelSpace {
        // The length of an edge of a voxel in world space
        float voxelSize;
        // The transformation matrix that gives for a world space position
        // the associated voxel
        mat4 worldToGrid;

        vec4* nodeBuffer;
        ivec2* neighbourOffsetCountBuffer;
        int* neighbourBuffer;

        usampler3D nodeGrid;
    };

    ivec3 gridSize;

    uniform VoxelSpace uVoxelSpace;

    layout(location = 0) out vec4 fNormalDepth;
    layout(location = 1) out vec3 fDiffuse;
    layout(location = 2) out vec4 fGlossyShininess;
    layout(location = 3) out uint fClusterNodeIndex;
    layout(location = 4) out uint fGeometryCluster;

    const float PI = 3.14159265358979323846264;

    float computeCoherency(uint cluster, vec3 P, vec3 N, float dskel) {
        vec4 node = uVoxelSpace.nodeBuffer[cluster];
        vec3 wi = node.xyz - P;
        float d = length(wi);
        wi /= d;

        float orientationFactor = dot(wi, N);
        if(orientationFactor > 0 && orientationFactor < 0.7f) {
            orientationFactor *= orientationFactor;
        }

        return orientationFactor * node.w / (d * d);
    }

    uint getNearestCluster(vec3 P, vec3 N) {
        vec3 lookAtPoint = P + N * uVoxelSpace.voxelSize;
        uint cluster = uint(-1);

        while(cluster == uint(-1)) {
            vec4 gridSpacePosition = uVoxelSpace.worldToGrid * vec4(lookAtPoint, 1);
            ivec3 voxel = ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z);
            if(voxel.x < 0 || voxel.x >= gridSize.x || voxel.y < 0 || voxel.y >= gridSize.y ||
                    voxel.z < 0 || voxel.z >= gridSize.z) {
                return uint(-1);
            }

            cluster = texelFetch(uVoxelSpace.nodeGrid, voxel, 0).x;
            lookAtPoint += N * uVoxelSpace.voxelSize;
        }

        vec3 clustP = uVoxelSpace.nodeBuffer[cluster].xyz;
        float dskel = length(P - clustP);

        float coherency = computeCoherency(cluster, P, N, dskel);
        ivec2 neighbourOffsetCount = uVoxelSpace.neighbourOffsetCountBuffer[cluster];

        int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

        for(int i = neighbourOffsetCount.x; i < end; ++i) {
            int neighbour = uVoxelSpace.neighbourBuffer[i];
            float candidateCoherency = computeCoherency(uint(neighbour), P, N,
                                                        dskel + length(clustP - uVoxelSpace.nodeBuffer[neighbour].xyz));
            if(candidateCoherency > coherency) {
                coherency = candidateCoherency;
                cluster = uint(neighbour);
            }
        }

        return cluster;
    }

    void main() {
        gridSize = textureSize(uVoxelSpace.nodeGrid, 0);

        vec3 nNormal = normalize(vNormal);
        fNormalDepth.xyz = nNormal;
        fNormalDepth.w = -vPosition.z / uZFar; // Normalized depth value

        fDiffuse = uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb / PI;

        float shininess = uMaterial.shininess + texture(uMaterial.shininessSampler, vTexCoords).r;
        fGlossyShininess.rgb = uMaterial.Ks * texture(uMaterial.KsSampler, vTexCoords).rgb * (shininess + 2) / (2 * PI);
        fGlossyShininess.a = shininess;

        fClusterNodeIndex = getNearestCluster(vWorldSpacePosition, normalize(vWorldSpaceNormal));

        fGeometryCluster = uint(floor(log(-vPosition.z / uZNear) / log(1 + 2 * tan(uHalfFOVY) / uSy)));
    }
);

GLSkelSMVPLRenderer3::GeometryPassData::GeometryPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_MaterialUniforms(m_Program),
    m_uMVPMatrix(m_Program, "uMVPMatrix", true),
    m_uMVMatrix(m_Program, "uMVMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true),
    m_uSy(m_Program, "uSy", true),
    m_uHalfFOVY(m_Program, "uHalfFOVY", true),
    m_uZNear(m_Program, "uZNear", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uVoxelSpace(m_Program) {
}

// This shader sort the clusters of each tile (32x32 pixels) to compute the number of clusters
// in each tile, compute a new local index for each and link back each pixel to this index.
// At the end, we have 0 <= uClustersImage(x, y) <= uClusterCountsImage(tx, ty) - 1
const GLchar* GLSkelSMVPLRenderer3::FindUniqueGeometryClusterPassData::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    // Input image computed in the geometry pass
    uniform usampler2D uClusterNodeSampler;
    uniform usampler2D uGeometryClusterSampler;
    uniform sampler2D uNormalDepthSampler;

    uniform uint uClusterNodeCount;

    // Outputs of the shader:

    // Link each pixel to a cluster index local to the tile
    layout(r32ui) uniform writeonly uimage2D uClustersImage;

    uniform uint* uClusterCountsBuffer;
    uniform vec3* uClusterBBoxLowerBuffer;
    uniform vec3* uClusterBBoxUpperBuffer;
    uniform uint* uClusterNodeBuffer;

    uniform mat4 uInvProjMatrix;
    uniform mat4 uRcpViewMatrix;
    uniform vec2 uScreenSize;

    // 32x32 is the size of a tile in screen space
    layout(local_size_x = 32, local_size_y = 32) in;

    const uint cNbThreads = gl_WorkGroupSize.x * gl_WorkGroupSize.y;

    shared uint sSampleIndices[cNbThreads];
    shared uint sClusterBuffer[cNbThreads];
    shared uint sClusterOffsets[cNbThreads];

    shared vec3 sViewSpaceBBoxLower[cNbThreads];
    shared vec3 sViewSpaceBBoxUpper[cNbThreads];
    //shared vec3 sViewSpaceNormals[cNbThreads];

    void oddEvenSortClusters() {
        for(uint i = 0; i < cNbThreads; ++i) {
            uint j;
            if(i % 2 == 0) {
                j = 2 * gl_LocalInvocationIndex;
            } else {
                j = 2 * gl_LocalInvocationIndex + 1;
            }

            uint lhs;
            uint rhs;

            if(j < cNbThreads - 1) {
                lhs = sClusterBuffer[j];
                rhs = sClusterBuffer[j + 1];
            }

            barrier();

            if(j < cNbThreads - 1 && lhs > rhs) {
                sClusterBuffer[j + 1] = lhs;
                sClusterBuffer[j] = rhs;

                uint tmp = sSampleIndices[j + 1];
                sSampleIndices[j + 1] = sSampleIndices[j];
                sSampleIndices[j] = tmp;
            }

            barrier();
        }
    }

    void mergeSortClusters() {
        uint tid = gl_LocalInvocationIndex;

        for(uint i = 1; i < cNbThreads; i *= 2) {
            uint listSize = i;
            uint listIdx = tid / i;
            uint listOffset = listIdx * listSize;
            uint otherListOffset;
            uint newListOffset;

            if(listIdx % 2 == 0) {
                otherListOffset = listOffset + listSize;
                newListOffset = listOffset;
            } else {
                otherListOffset = listOffset - listSize;
                newListOffset = otherListOffset;
            }

            // On doit calculer la position finale de l'element courant
            uint indexInMyList = tid - listOffset;

            uint indexInOtherList = 0;

            uint value = sClusterBuffer[tid];
            uint sIdx = sSampleIndices[tid];

            uint a = 0;
            uint b = listSize;
            uint middle;

            for(uint j = 1; j < listSize; j *= 2) {
                middle = (a + b) / 2;
                uint otherValue = sClusterBuffer[otherListOffset + middle];
                if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
                    b = middle;
                } else {
                    a = middle;
                }
            }

            uint otherValue = sClusterBuffer[otherListOffset + a];
            if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
                indexInOtherList = a;
            } else {
                indexInOtherList = b;
            }

            uint finalIndex = indexInMyList + indexInOtherList;

            barrier();

            sClusterBuffer[newListOffset + finalIndex] = value;
            sSampleIndices[newListOffset + finalIndex] = sIdx;

            barrier();
        }
    }

    void blellochExclusiveAddScan() {
        // Up-sweep phase
        uint a = 2 * gl_LocalInvocationIndex;
        uint b = 2 * gl_LocalInvocationIndex + 1;

        for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
            if(gl_LocalInvocationIndex < s) {
                sClusterOffsets[b] += sClusterOffsets[a];

                a = a * 2 + 1;
                b = b * 2 + 1;
            }
            barrier();
        }

        // Down-sweep phase
        if(gl_LocalInvocationIndex == 0) {
            sClusterOffsets[cNbThreads - 1] = 0;
        }
        barrier();

        for(uint s = 1; s < cNbThreads; s *= 2) {
            if(gl_LocalInvocationIndex < s) {
                a = (a - 1) / 2;
                b = (b - 1) / 2;

                uint tmp = sClusterOffsets[b];
                sClusterOffsets[b] += sClusterOffsets[a];
                sClusterOffsets[a] = tmp;
            }
            barrier();
        }
    }

    void main() {
        // Local ID of the current thread
        uint tid = gl_LocalInvocationIndex;

        // This is the original cluster ID computed for this pixel
        uint cluster = texelFetch(uGeometryClusterSampler, ivec2(gl_GlobalInvocationID.xy), 0).x;
        uint clusterNode = texelFetch(uClusterNodeSampler, ivec2(gl_GlobalInvocationID.xy), 0).x;

        // Initialize the shared memory
        sClusterBuffer[gl_LocalInvocationIndex] = cluster * uClusterNodeCount + clusterNode;
        sClusterOffsets[gl_LocalInvocationIndex] = 0;
        sSampleIndices[gl_LocalInvocationIndex] = gl_LocalInvocationIndex;

        barrier();

        // Local sorting
        //oddEvenSortClusters();
        mergeSortClusters();

        bool isUniqueCluster = gl_LocalInvocationIndex == 0 ||
                sClusterBuffer[gl_LocalInvocationIndex] != sClusterBuffer[gl_LocalInvocationIndex - 1];

        // Initialization for scan
        if(isUniqueCluster) {
            sClusterOffsets[gl_LocalInvocationIndex] = 1;
        }
        barrier();

        // Compaction step
        blellochExclusiveAddScan();

        // Compute offset
        if(!isUniqueCluster) {
            sClusterOffsets[gl_LocalInvocationIndex] -= 1;
        }
        barrier();

        // Write the number of clusters for this tile in the output image
        if(gl_LocalInvocationIndex == 0) {
            uint clusterCount = sClusterOffsets[cNbThreads - 1] + 1;
            //bClusterCountsBuffer.data[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_WorkGroupSize.x] = clusterCount;
            uClusterCountsBuffer[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_WorkGroupSize.x] = clusterCount;
        }

        // Original view sample that have been moved to this cell by the sort step
        uint sampleIndex = sSampleIndices[gl_LocalInvocationIndex];

        // Offset associated to the cluster of this sample
        uint clusterOffset = sClusterOffsets[gl_LocalInvocationIndex];

        // Coordinates of this sample
        uint x = sampleIndex % gl_WorkGroupSize.x;
        uint y = (sampleIndex - x) / gl_WorkGroupSize.x;
        ivec2 globalCoords = ivec2(gl_WorkGroupID.x * gl_WorkGroupSize.x + x,
                             gl_WorkGroupID.y * gl_WorkGroupSize.y + y);

        // Write the offset for this cluster in the new cluster image
        imageStore(uClustersImage, globalCoords, uvec4(clusterOffset, 0, 0, 0));

        clusterNode = texelFetch(uClusterNodeSampler, globalCoords, 0).x;

        // Read geometric information of the original sample
        vec4 normalDepth = texelFetch(uNormalDepthSampler, globalCoords, 0);
        vec2 pixelNDC = vec2(-1.f, -1.f) + 2.f * (vec2(globalCoords) + vec2(0.5f, 0.5f)) / uScreenSize;
        vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(pixelNDC, 1.f, 1.f);
        farPointViewSpaceCoords = farPointViewSpaceCoords / farPointViewSpaceCoords.w;
        vec4 samplePointViewSpaceCoords = vec4(normalDepth.w * farPointViewSpaceCoords.xyz, 1);

        sViewSpaceBBoxLower[gl_LocalInvocationIndex] = vec3(uRcpViewMatrix * samplePointViewSpaceCoords);
        sViewSpaceBBoxUpper[gl_LocalInvocationIndex] = sViewSpaceBBoxLower[gl_LocalInvocationIndex];

        barrier();

        // Reduce step to get bounds of the clusters
        for(uint s = 1; s < cNbThreads; s *= 2) {
            uint other = tid + s;
            if(other < cNbThreads && sClusterOffsets[other] == clusterOffset) {
                //sData[tid] = max(sData[tid], sData[other]);
                sViewSpaceBBoxLower[tid] = min(sViewSpaceBBoxLower[tid], sViewSpaceBBoxLower[other]);
                sViewSpaceBBoxUpper[tid] = max(sViewSpaceBBoxUpper[tid], sViewSpaceBBoxUpper[other]);
            }
            barrier();
        }

        // Write bounds to ouput images
        if(isUniqueCluster) {
            uint globalOffset = (gl_WorkGroupID.x + gl_NumWorkGroups.x * gl_WorkGroupID.y) * cNbThreads + clusterOffset;

            uClusterBBoxLowerBuffer[globalOffset] = sViewSpaceBBoxLower[tid];
            uClusterBBoxUpperBuffer[globalOffset] = sViewSpaceBBoxUpper[tid];
            uClusterNodeBuffer[globalOffset] = clusterNode;
        }
    }
);

GLSkelSMVPLRenderer3::FindUniqueGeometryClusterPassData::FindUniqueGeometryClusterPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uClusterNodeSampler(m_Program, "uClusterNodeSampler", true),
    m_uGeometryClusterSampler(m_Program, "uGeometryClusterSampler", true),
    m_uNormalDepthSampler(m_Program, "uNormalDepthSampler", true),
    m_uClusterNodeCount(m_Program, "uClusterNodeCount", true),
    m_uClustersImage(m_Program, "uClustersImage", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uScreenSize(m_Program, "uScreenSize", true),
    m_uClusterCountsBuffer(m_Program, "uClusterCountsBuffer", true),
    m_uClusterBBoxLowerBuffer(m_Program, "uClusterBBoxLowerBuffer", true),
    m_uClusterBBoxUpperBuffer(m_Program, "uClusterBBoxUpperBuffer", true),
    m_uClusterNodeBuffer(m_Program, "uClusterNodeBuffer", true) {

    m_Program.use();
    m_uClusterNodeSampler.set(TEXUNIT_GBUFFER_CLUSTERNODE_INDICES);
    m_uGeometryClusterSampler.set(TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
    m_uNormalDepthSampler.set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uClustersImage.set(IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS);
}

const GLchar* GLSkelSMVPLRenderer3::CountGeometryClusterPassData::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    // Inputs
    // Number of screen space tiles
    uniform uint uTileCount;

    // For each tile (Tx, Ty), uClusterCountsBuffer[Tx + Ty * 32] is the number of cluster contained in the tile
    uniform uint* uClusterCountsBuffer;

    // Outputs
    // For each tile (Tx, Ty), uClusterTilesOffsetsBuffer[Tx + Ty * 32] will be the global index of the first cluster
    // contained in the tile.
    uniform uint* uClusterTilesOffsetsBuffer;
    // Will be the total number of clusters
    uniform uint* uClusterCount;
    // Will contains for each cluster the index of the associated tile
    uniform uint* uClusterToTileBuffer;

    layout(local_size_x = 1024) in;

    const uint cNbThreads = gl_WorkGroupSize.x;

    shared uint sGroupData[cNbThreads];

    void main() {
        uint tileClusterCount = 0;
        // Init
        if(gl_GlobalInvocationID.x >= uTileCount) {
            sGroupData[gl_LocalInvocationIndex] = 0;
        } else {
            tileClusterCount = sGroupData[gl_LocalInvocationIndex] = uClusterCountsBuffer[gl_GlobalInvocationID.x];
        }

        barrier();

        // Scan to compute offsets and cluster count

        // Up-sweep phase
        uint a = 2 * gl_LocalInvocationIndex;
        uint b = 2 * gl_LocalInvocationIndex + 1;

        for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
            if(gl_LocalInvocationIndex < s) {
                sGroupData[b] += sGroupData[a];

                a = a * 2 + 1;
                b = b * 2 + 1;
            }
            barrier();
        }

        // Down-sweep phase
        if(gl_LocalInvocationIndex == 0) {
            sGroupData[cNbThreads - 1] = 0;
        }
        barrier();

        for(uint s = 1; s < cNbThreads; s *= 2) {
            if(gl_LocalInvocationIndex < s) {
                a = (a - 1) / 2;
                b = (b - 1) / 2;

                uint tmp = sGroupData[b];
                sGroupData[b] += sGroupData[a];
                sGroupData[a] = tmp;
            }
            barrier();
        }

        if(gl_GlobalInvocationID.x < uTileCount) {
            uint tileOffset = uClusterTilesOffsetsBuffer[gl_GlobalInvocationID.x] = sGroupData[gl_LocalInvocationIndex];

            if(gl_GlobalInvocationID.x == uTileCount - 1) {
                *uClusterCount = sGroupData[gl_LocalInvocationIndex] + uClusterCountsBuffer[gl_GlobalInvocationID.x];
            }

            // Write correspondance from cluster to tile
            for(uint i = 0; i < tileClusterCount; ++i) {
                uClusterToTileBuffer[tileOffset + i] = gl_LocalInvocationIndex;
            }

            barrier();
        }
    }
);

GLSkelSMVPLRenderer3::CountGeometryClusterPassData::CountGeometryClusterPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uTileCount(m_Program, "uTileCount", true),
    m_uClusterCountsBuffer(m_Program, "uClusterCountsBuffer", true),
    m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
    m_uClusterCount(m_Program, "uClusterCount", true),
    m_uClusterToTileBuffer(m_Program, "uClusterToTileBuffer", true) {
}

const GLchar* GLSkelSMVPLRenderer3::LightAssignmentPassData::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_bindless_texture : enable\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    struct VoxelSpace {
        // The length of an edge of a voxel in world space
        float voxelSize;
        // The transformation matrix that gives for a world space position
        // the associated voxel
        mat4 worldToGrid;

        vec4* nodeBuffer;
        ivec2* neighbourOffsetCountBuffer;
        int* neighbourBuffer;

        usampler3D nodeGrid;
    };

    uniform VoxelSpace uVoxelSpace;

    ivec3 gridSize;

    float computeCoherency(uint cluster, vec3 P, vec3 N) {
        vec4 node = uVoxelSpace.nodeBuffer[cluster];
        vec3 wi = node.xyz - P;
        float d = length(wi);
        wi /= d;

        float orientationFactor = dot(wi, N);
        return orientationFactor * node.w / (d * d);
    }

    uint getNearestCluster(vec3 P, vec3 N) {
        vec3 lookAtPoint = P + N * uVoxelSpace.voxelSize;
        uint cluster = uint(-1);

        while(cluster == uint(-1)) {
            vec4 gridSpacePosition = uVoxelSpace.worldToGrid * vec4(lookAtPoint, 1);
            ivec3 voxel = ivec3(gridSpacePosition.x, gridSpacePosition.y, gridSpacePosition.z);
            if(voxel.x < 0 || voxel.x >= gridSize.x || voxel.y < 0 || voxel.y >= gridSize.y ||
                    voxel.z < 0 || voxel.z >= gridSize.z) {
                return uint(-1);
            }

            cluster = texelFetch(uVoxelSpace.nodeGrid, voxel, 0).x;
            lookAtPoint += N * uVoxelSpace.voxelSize;
        }

        float coherency = computeCoherency(cluster, P, N);
        ivec2 neighbourOffsetCount = uVoxelSpace.neighbourOffsetCountBuffer[cluster];

        int end = neighbourOffsetCount.x + neighbourOffsetCount.y;

        for(int i = neighbourOffsetCount.x; i < end; ++i) {
            int neighbour = uVoxelSpace.neighbourBuffer[i];
            float candidateCoherency = computeCoherency(uint(neighbour), P, N);
            if(candidateCoherency > coherency) {
                coherency = candidateCoherency;
                cluster = uint(neighbour);
            }
        }

        return cluster;
    }

    struct VPL {
        vec3 P;
        vec3 N;
        vec3 L;
    };

    uniform uint uVPLCount;
    uniform VPL* uVPLBuffer;

    // Outputs

    uniform ivec2* uVPLOffsetCountsBuffer;
    uniform uint* uVPLIndexBuffer;
    uniform coherent uint* uNextOffset;

    uniform uint uClusterCount;

    uniform uint* uClusterTilesOffsetsBuffer;
    uniform uint* uClusterToTileBuffer;
    uniform vec3* uClusterBBoxLowerBuffer;
    uniform vec3* uClusterBBoxUpperBuffer;
    uniform uint* uNodeClusterBuffer;

    uniform mat4 uFaceProjectionMatrices[6];
    uniform samplerCubeShadow* uClusterNodeShadowMapBuffer;

    layout(local_size_x = 1024) in;

    uniform bool uDoVPLShading;
    uniform bool uNoShadow;

    uint tileID;
    uint clusterIdx;
    uint clusterLocalID;
    vec3 bboxLower;
    vec3 bboxUpper;
    vec3 bboxCenter;
    float bboxRadius;

    int getFaceIdx(vec3 wi) {
        vec3 absWi = abs(wi);

        float maxComponent = max(absWi.x, max(absWi.y, absWi.z));

        if(maxComponent == absWi.x) {
            if(wi.x > 0) {
                return 0;
            }
            return 1;
        }

        if(maxComponent == absWi.y) {
            if(wi.y > 0) {
                return 2;
            }
            return 3;
        }

        if(maxComponent == absWi.z) {
            if(wi.z > 0) {
                return 4;
            }
            return 5;
        }

        return -1;
    }

    bool acceptVPL(VPL vpl, vec3 nodePosition, uint nodeCluster) {
        mat4 nodeViewMatrix = mat4(1);
        nodeViewMatrix[3] = vec4(-nodePosition, 1);
        vec3 vplWorldSpacePosition = vpl.P;
        vec3 shadowRay = vplWorldSpacePosition - nodePosition;
        int face = getFaceIdx(shadowRay);
        vec4 vplNodeClipSpacePosition = uFaceProjectionMatrices[face] * nodeViewMatrix * vec4(vplWorldSpacePosition, 1);
        float depthRef = vplNodeClipSpacePosition.z / vplNodeClipSpacePosition.w;
        depthRef = 0.5 * (depthRef + 1) - 0.001;

        if(texture(uClusterNodeShadowMapBuffer[nodeCluster], vec4(shadowRay, depthRef)) > 0.f) {
            return true;
        }

//        ivec2 offsetCount = uVoxelSpace.neighbourOffsetCountBuffer[nodeCluster];
//        int end = offsetCount.x + offsetCount.y;
//        for(int i = offsetCount.x; i < end; ++i) {
//            int neighbour = uVoxelSpace.neighbourBuffer[i];

//            vec3 nodePosition = uVoxelSpace.nodeBuffer[neighbour].xyz;

//            mat4 nodeViewMatrix = mat4(1);
//            nodeViewMatrix[3] = vec4(-nodePosition, 1);

//            vec3 shadowRay = vplWorldSpacePosition - nodePosition;
//            int face = getFaceIdx(shadowRay);
//            vec4 vplNodeClipSpacePosition = uFaceProjectionMatrices[face] * nodeViewMatrix * vec4(vplWorldSpacePosition, 1);
//            float depthRef = vplNodeClipSpacePosition.z / vplNodeClipSpacePosition.w;
//            depthRef = 0.5 * (depthRef + 1) - 0.001;

//            if(texture(uClusterNodeShadowMapBuffer[neighbour], vec4(shadowRay, depthRef)) > 0.f) {
//                return true;
//            }
//        }

        return false;
    }

    void main() {
        clusterIdx = uint(gl_GlobalInvocationID.x);

        if(clusterIdx >= uClusterCount) {
            return;
        }

        gridSize = textureSize(uVoxelSpace.nodeGrid, 0);

        tileID = uClusterToTileBuffer[clusterIdx];
        clusterLocalID = clusterIdx - uClusterTilesOffsetsBuffer[tileID];

        uint dataIdx = tileID * 1024 + clusterLocalID;

        uint nodeCluster = uNodeClusterBuffer[dataIdx];
        vec3 nodePosition = uVoxelSpace.nodeBuffer[nodeCluster].xyz;
        bboxLower = uClusterBBoxLowerBuffer[dataIdx];
        bboxUpper = uClusterBBoxUpperBuffer[dataIdx];
        bboxCenter = (bboxLower + bboxUpper) * 0.5f;
        bboxRadius = length(bboxLower - bboxUpper) * 0.5f;

        uint nbAcceptedLights = 0;
        for(uint i = 0; i < uVPLCount; ++i) {
            if(acceptVPL(uVPLBuffer[i], nodePosition, nodeCluster)) {
                ++nbAcceptedLights;
            }
        }

        uint offset = atomicAdd(uNextOffset, nbAcceptedLights);

        uVPLOffsetCountsBuffer[clusterIdx] = ivec2(int(offset), int(nbAcceptedLights));

        uint c = offset;
        for(uint i = 0; i < uVPLCount; ++i) {
            VPL vpl = uVPLBuffer[i];
            if(acceptVPL(vpl, nodePosition, nodeCluster)) {
                uVPLIndexBuffer[c++] = i;
            }
        }

        memoryBarrier();
    }

    );

    GLSkelSMVPLRenderer3::LightAssignmentPassData::LightAssignmentPassData():
        m_Program(buildComputeProgram(s_ComputeShader)),
        m_uVoxelSpace(m_Program),
        m_uVPLCount(m_Program, "uVPLCount", true),
        m_uClusterCount(m_Program, "uClusterCount", true),
        m_uDoVPLShading(m_Program, "uDoVPLShading", true),
        m_uNoShadow(m_Program, "uNoShadow", true),
        m_uVPLBuffer(m_Program, "uVPLBuffer", true),
        m_uVPLOffsetCountsBuffer(m_Program, "uVPLOffsetCountsBuffer", true),
        m_uVPLIndexBuffer(m_Program, "uVPLIndexBuffer", true),
        m_uNextOffset(m_Program, "uNextOffset", true),
        m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
        m_uClusterToTileBuffer(m_Program, "uClusterToTileBuffer", true),
        m_uClusterBBoxLowerBuffer(m_Program, "uClusterBBoxLowerBuffer", true),
        m_uClusterBBoxUpperBuffer(m_Program, "uClusterBBoxUpperBuffer", true),
        m_uNodeClusterBuffer(m_Program, "uNodeClusterBuffer", true),
        m_uFaceProjectionMatrices(m_Program, "uFaceProjectionMatrices", true),
        m_uClusterNodeShadowMapBuffer(m_Program, "uClusterNodeShadowMapBuffer", true) {
    }

    const GLchar* GLSkelSMVPLRenderer3::TransformVPLToViewSpacePassData::s_ComputeShader =
    "#version 430\n"
    GLOOPS_STRINGIFY(

    struct VPL {
        vec3 P;
        vec3 N;
        vec3 L;
    };

    uniform uint uVPLCount;

    // Contains all the VPLs
    layout(std430, binding = 0) coherent buffer VPLBuffer {
        VPL bVPLBuffer[];
    };

    uniform mat4 uViewMatrix;
    uniform mat3 uNormalMatrix;

    layout(local_size_x = 1024) in;

    void main() {
        uint idx = uint(gl_GlobalInvocationID.x);

        // A thread represents a cluster
        if(idx < uVPLCount) {
            bVPLBuffer[idx].P = vec3(uViewMatrix * vec4(bVPLBuffer[idx].P, 1));
            bVPLBuffer[idx].N = normalize(uNormalMatrix * bVPLBuffer[idx].N);
        }

        memoryBarrier();
    }
);

GLSkelSMVPLRenderer3::TransformVPLToViewSpacePassData::TransformVPLToViewSpacePassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uViewMatrix(m_Program, "uViewMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true) {
}

const GLchar* GLSkelSMVPLRenderer3::VPLShadingPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    out vec3 vFarPointViewSpaceCoords;

    uniform mat4 uInvProjMatrix;

    void main() {
        vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
        vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

        gl_Position = vec4(aPosition, 0, 1);
    }

    );

const GLchar* GLSkelSMVPLRenderer3::VPLShadingPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_bindless_texture : enable\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

    uniform vec4 uViewport;

    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
        usampler2D clusterNodeSampler;
        usampler2D geometryClusterSampler;
    };

    uniform GBuffer uGBuffer;

    uniform uvec2 uTileCount;

    struct VPL {
        vec3 P;
        vec3 N;
        vec3 L;
    };

    uniform uint uVPLCount;
    uniform bool uDisplayCostPerPixel;
    uniform bool uRenderIrradiance;

    uniform VPL* uVPLBuffer;
    uniform uint* uVPLIndexBuffer;
    uniform uint* uClusterTilesOffsetsBuffer;

    vec3 randomColor(uint value) {
        return fract(
                    sin(
                        vec3(float(value) * 12.9898,
                             float(value) * 78.233,
                             float(value) * 56.128)
                        )
                    * 43758.5453
                    );
    }

    vec3 heatColor(float t) {
        vec3 map[] = vec3[](vec3(0, 0, 1), vec3(0, 1, 0), vec3(1, 0, 0));
        t *= 3;
        int i = clamp(int(t), 0, 2);
        int j = clamp(i + 1, 0, 2);
        float r = t - i;
        return (1 - r) * map[i] + r * map[j];
    }

    in vec3 vFarPointViewSpaceCoords;

    out vec3 fFragColor;

    struct VoxelSpace {
        // The length of an edge of a voxel in world space
        float voxelSize;
        // The transformation matrix that gives for a world space position
        // the associated voxel
        mat4 worldToGrid;

        vec4* nodeBuffer;
        ivec2* neighbourOffsetCountBuffer;
        int* neighbourBuffer;

        usampler3D nodeGrid;
    };

    ivec3 gridSize;

    uniform VoxelSpace uVoxelSpace;
    uniform ivec2* uVPLOffsetCountsBuffer;
    uniform mat4 uFaceProjectionMatrices[6];
    uniform samplerCubeShadow* uClusterNodeShadowMapBuffer;
    uniform mat4 uRcpViewMatrix;

    int getFaceIdx(vec3 wi) {
        vec3 absWi = abs(wi);

        float maxComponent = max(absWi.x, max(absWi.y, absWi.z));

        if(maxComponent == absWi.x) {
            if(wi.x > 0) {
                return 0;
            }
            return 1;
        }

        if(maxComponent == absWi.y) {
            if(wi.y > 0) {
                return 2;
            }
            return 3;
        }

        if(maxComponent == absWi.z) {
            if(wi.z > 0) {
                return 4;
            }
            return 5;
        }

        return -1;
    }

    float computeCoherency(uint cluster, vec3 P, vec3 N) {
        vec4 node = uVoxelSpace.nodeBuffer[cluster];
        vec3 wi = node.xyz - P;
        float d = length(wi);
        wi /= d;

        float orientationFactor = dot(wi, N);
        return orientationFactor * node.w / (d * d);
    }

    void main() {
        gridSize = textureSize(uVoxelSpace.nodeGrid, 0);

        vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

        ivec2 tileCoords = ivec2(texCoords * vec2(textureSize(uGBuffer.geometryClusterSampler, 0)));
        tileCoords /= 32;

        uint tileIdx = tileCoords.x + tileCoords.y * uTileCount.x;

        uint clusterLocalID = texture(uGBuffer.geometryClusterSampler, texCoords).r;
        uint clusterGlobalID = uClusterTilesOffsetsBuffer[tileIdx] + clusterLocalID;

        // Read fragment attributes
        vec4 normalDepth = texture(uGBuffer.normalDepthSampler, texCoords);
        vec3 fragPosition = normalDepth.w * vFarPointViewSpaceCoords;
        vec3 Kd;
        if(uRenderIrradiance) {
            Kd = vec3(1.f / 3.14f);
        } else {
            Kd = texture(uGBuffer.diffuseSampler, texCoords).rgb;
        }
        vec4 KsShininess = texture(uGBuffer.glossyShininessSampler, texCoords);

        uint clusterNode = texture(uGBuffer.clusterNodeSampler, texCoords, 0);
        if(clusterNode == uint(-1)) {
            discard;
        }

        vec3 nodePosition = uVoxelSpace.nodeBuffer[clusterNode].xyz;

        mat4 nodeViewMatrix = mat4(1);
        nodeViewMatrix[3] = vec4(-nodePosition, 1);

        vec3 P = vec3(uRcpViewMatrix * vec4(fragPosition, 1));
        vec3 N = vec3(uRcpViewMatrix * vec4(normalDepth.xyz, 0));

        fFragColor = vec3(0, 0, 0);

        ivec2 offsetCount = uVPLOffsetCountsBuffer[clusterGlobalID];

        if(uDisplayCostPerPixel) {
            fFragColor = heatColor(float(offsetCount.y) / uVPLCount);
            return;
        }

        int end = offsetCount.x + offsetCount.y;

        for(int j = offsetCount.x; j < end; ++j) {
            uint vplIdx = uVPLIndexBuffer[j];
            VPL vpl = uVPLBuffer[vplIdx];

            vec3 wi = vpl.P - fragPosition;
            float dist = length(wi);
            wi /= dist;

            float cos_o = max(0, dot(-wi, vpl.N));
            float cos_i = max(0, dot(wi, normalDepth.xyz));

            dist = max(dist, 0);

            float G = cos_i * cos_o / (dist * dist);

            vec3 r = reflect(-wi, normalDepth.xyz);

            vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);

            float shadowFactor = 0;
            float sumCoherency = 0;

            vec3 vplWorldSpacePosition = vec3(uRcpViewMatrix * vec4(vpl.P, 1));
            vec3 shadowRay = vplWorldSpacePosition - nodePosition;
            int face = getFaceIdx(shadowRay);
            vec4 vplNodeClipSpacePosition = uFaceProjectionMatrices[face] * nodeViewMatrix * vec4(vplWorldSpacePosition, 1);
            float depthRef = vplNodeClipSpacePosition.z / vplNodeClipSpacePosition.w;
            depthRef = 0.5 * (depthRef + 1) - 0.001;

            float coherency = computeCoherency(clusterNode, P, N);
            shadowFactor += coherency * texture(uClusterNodeShadowMapBuffer[clusterNode], vec4(shadowRay, depthRef));
            sumCoherency += coherency;

//            if(shadowFactor == 0.f) {
//                ivec2 offsetCount = uVoxelSpace.neighbourOffsetCountBuffer[clusterNode];
//                int end = offsetCount.x + offsetCount.y;
//                for(int i = offsetCount.x; i < end; ++i) {
//                    int neighbour = uVoxelSpace.neighbourBuffer[i];

//                    vec3 nodePosition = uVoxelSpace.nodeBuffer[neighbour].xyz;

//                    mat4 nodeViewMatrix = mat4(1);
//                    nodeViewMatrix[3] = vec4(-nodePosition, 1);

//                    vec3 vplWorldSpacePosition = vec3(uRcpViewMatrix * vec4(vpl.P, 1));
//                    vec3 shadowRay = vplWorldSpacePosition - nodePosition;
//                    int face = getFaceIdx(shadowRay);
//                    vec4 vplNodeClipSpacePosition = uFaceProjectionMatrices[face] * nodeViewMatrix * vec4(vplWorldSpacePosition, 1);
//                    float depthRef = vplNodeClipSpacePosition.z / vplNodeClipSpacePosition.w;
//                    depthRef = 0.5 * (depthRef + 1) - 0.001;

//                    float coherency = computeCoherency(neighbour, P, N);

//                    shadowFactor += coherency * texture(uClusterNodeShadowMapBuffer[neighbour], vec4(shadowRay, depthRef));
//                    sumCoherency += coherency;
//                }
//            }

            shadowFactor /= sumCoherency;

            fFragColor = fFragColor + shadowFactor * brdf * G * vpl.L;
        }
    }
);

GLSkelSMVPLRenderer3::VPLShadingPassData::VPLShadingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uTileCount(m_Program, "uTileCount", true),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uDisplayCostPerPixel(m_Program, "uDisplayCostPerPixel", true),
    m_uVPLBuffer(m_Program, "uVPLBuffer", true),
    m_uVPLIndexBuffer(m_Program, "uVPLIndexBuffer", true),
    m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
    m_uVPLOffsetCountsBuffer(m_Program, "uVPLOffsetCountsBuffer", true),
    m_uVoxelSpace(m_Program),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uFaceProjectionMatrices(m_Program, "uFaceProjectionMatrices", true),
    m_uClusterNodeShadowMapBuffer(m_Program, "uClusterNodeShadowMapBuffer", true),
    m_uGBuffer(m_Program),
    m_uRenderIrradiance(m_Program, "uRenderIrradiance", true) {
}

const GLchar* GLSkelSMVPLRenderer3::DrawBuffersPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    void main() {
        gl_Position = vec4(aPosition, 0, 1);
    }
);

const GLchar* GLSkelSMVPLRenderer3::DrawBuffersPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    uniform vec4 uViewport;
    uniform sampler2D uNormalDepthSampler;
    uniform usampler2D uClusterNodeSampler;
    uniform usampler2D uGeometryClusterSampler;

    uniform uint* uClusterTilesOffsetsBuffer;
    //uniform vec3* bClusterBBoxLowerBuffer;
    //uniform vec3* bClusterBBoxUpperBuffer;
    uniform uint* uClusterNodeBuffer;
    uniform uint* uClusterToTileBuffer;

    layout(std430, binding = 3) buffer ClusterBBoxLowerBuffer {
        vec3 bClusterBBoxLowerBuffer[];
    };

    layout(std430, binding = 4) buffer ClusterBBoxUpperBuffer {
        vec3 bClusterBBoxUpperBuffer[];
    };

    uniform float uZFar;
    uniform uvec2 uTileCount;

    uniform int uDataToDisplay;

    out vec3 fFragColor;

    vec3 randomColor(uint value) {
        return fract(
                    sin(
                        vec3(float(value) * 12.9898,
                             float(value) * 78.233,
                             float(value) * 56.128)
                        )
                    * 43758.5453
                    );
    }

    vec3 randomColor(uvec3 value) {
        return fract(
                    sin(
                        vec3(float(value.x * value.y) * 12.9898,
                             float(value.y * value.z) * 78.233,
                             float(value.z * value.x) * 56.128)
                        )
                    * 43758.5453
                    );
    }

    void main() {
        vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

        ivec2 tileCoords = ivec2(texCoords * vec2(textureSize(uGeometryClusterSampler, 0).xy));
        tileCoords /= 32;

        uint tileIdx = tileCoords.x + tileCoords.y * uTileCount.x;

        if(uDataToDisplay == 0) {
            fFragColor = texture(uNormalDepthSampler, texCoords).rgb;
        } else if(uDataToDisplay == 1) {
            fFragColor = vec3(texture(uNormalDepthSampler, texCoords).a);
        } else if(uDataToDisplay == 2) {
            //uint clusterNode = texture(uClusterNodeSampler, texCoords).r;

            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);

            uint globalID = uClusterTilesOffsetsBuffer[tileIdx] + uint(geometryCluster);
            tileIdx = uClusterToTileBuffer[globalID];

            uint clusterNode = uClusterNodeBuffer[tileIdx * 1024 + geometryCluster];

            if(clusterNode == uint(-1)) {
                fFragColor = vec3(0, 0, 0);
            } else {
                fFragColor = randomColor(clusterNode);
            }

        } else if(uDataToDisplay == 4) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            fFragColor = randomColor(uClusterTilesOffsetsBuffer[tileIdx] + geometryCluster);
        } else if (uDataToDisplay == 5) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            vec3 bboxLower = bClusterBBoxLowerBuffer[tileIdx * 1024 + geometryCluster];
            fFragColor = vec3(-bboxLower.z / uZFar);
        } else if (uDataToDisplay == 6) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            vec3 bboxUpper = bClusterBBoxUpperBuffer[tileIdx * 1024 + geometryCluster];
            fFragColor = vec3(-bboxUpper.z / uZFar);
        } else if (uDataToDisplay == 7) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            uint globalID = uClusterTilesOffsetsBuffer[tileIdx] + uint(geometryCluster);
            fFragColor = randomColor(uClusterToTileBuffer[globalID]);
        }
    }
);

GLSkelSMVPLRenderer3::DrawBuffersPassData::DrawBuffersPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uNormalDepthSampler(m_Program, "uNormalDepthSampler", true),
    m_uClusterNodeSampler(m_Program, "uClusterNodeSampler", true),
    m_uGeometryClusterSampler(m_Program, "uGeometryClusterSampler", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uDataToDisplay(m_Program, "uDataToDisplay", true),
    m_uTileCount(m_Program, "uTileCount", true),
    m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
    //m_uClusterBBoxLowerBuffer(m_Program, "bClusterBBoxLowerBuffer", true),
    //m_uClusterBBoxUpperBuffer(m_Program, "bClusterBBoxUpperBuffer", true),
    m_uClusterNodeBuffer(m_Program, "uClusterNodeBuffer", true),
    m_uClusterToTileBuffer(m_Program, "uClusterToTileBuffer", true) {

    m_Program.use();
    m_uNormalDepthSampler.set(TEXUNIT_GBUFFER_NORMALDEPTH);
    m_uClusterNodeSampler.set(TEXUNIT_GBUFFER_CLUSTERNODE_INDICES);
    m_uGeometryClusterSampler.set(TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
}

const GLchar* GLSkelSMVPLRenderer3::DrawClusterNodeVisibilityPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    out vec2 vTexCoords;
    out vec3 vFarPointViewSpaceCoords;

    uniform mat4 uInvProjMatrix;

    void main() {
        vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
        vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

        vTexCoords = aPosition;

        gl_Position = vec4(aPosition, 0, 1);
    }
);

const GLchar* GLSkelSMVPLRenderer3::DrawClusterNodeVisibilityPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_bindless_texture : enable\n"
GLOOPS_STRINGIFY(
    uniform vec4 uViewport;

    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
        usampler2D clusterNodeSampler;
        usampler2D geometryClusterSampler;
    };

    uniform GBuffer uGBuffer;

    uniform vec3 uClusterPosition;
    //uniform samplerCubeShadow* uClusterNodeShadowMap;
    uniform uint uClusterIndex;

    layout(binding = 0) buffer ClusterNodeShadowMapBuffer {
        samplerCubeShadow bClusterNodeShadowMap[];
    };

    uniform mat4 uClusterNodeMVPMatrices[6];

    uniform mat4 uRcpViewMatrix;

    in vec3 vFarPointViewSpaceCoords;
    in vec2 vTexCoords;

    out vec3 fFragColor;

    int getFaceIdx(vec3 wi) {
        vec3 absWi = abs(wi);

        float maxComponent = max(absWi.x, max(absWi.y, absWi.z));

        if(maxComponent == absWi.x) {
            if(wi.x > 0) {
                return 0;
            }
            return 1;
        }

        if(maxComponent == absWi.y) {
            if(wi.y > 0) {
                return 2;
            }
            return 3;
        }

        if(maxComponent == absWi.z) {
            if(wi.z > 0) {
                return 4;
            }
            return 5;
        }

        return -1;
    }

    vec3 getFaceColor(int face) {
        switch(face) {
        case 0:
            return vec3(1, 0, 0);
        case 1:
            return vec3(0, 1, 0);
        case 2:
            return vec3(0, 0, 1);
        case 3:
            return vec3(1, 1, 0);
        case 4:
            return vec3(1, 0, 1);
        case 5:
            return vec3(0, 1, 1);
        }
        return vec3(0, 0, 0);
    }

    void main() {
        vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

        // Read fragment attributes
        vec4 normalDepth = texture(uGBuffer.normalDepthSampler, texCoords);
        vec3 fragPosition = normalDepth.w * vFarPointViewSpaceCoords;
        vec3 Kd = texture(uGBuffer.diffuseSampler, texCoords).rgb;
    //    vec3 Kd = vec3(1.f / 3.14f);

        vec3 P = vec3(uRcpViewMatrix * vec4(fragPosition, 1));
        vec3 N = vec3(uRcpViewMatrix * vec4(normalDepth.xyz, 0));
        vec3 wi = P - uClusterPosition;

        int faceIdx = getFaceIdx(wi);
        vec4 Pclip = uClusterNodeMVPMatrices[faceIdx] * vec4(P, 1);

        float depth = Pclip.z / Pclip.w;
        depth = 0.5 * (depth + 1);

        //fFragColor = getFaceColor(faceIdx);

        //float depth = texture(uClusterNodeShadowMap, wi).r;
        //fFragColor = vec3((2.f) / (5000 + 1 - depth * (5000 - 1)));

        float l = length(wi);
        wi /= l;

        vec3 L = vec3(1000000) * Kd * dot(N, -wi) / (l * l);

        float shadow = texture(bClusterNodeShadowMap[uClusterIndex], vec4(wi, depth - 0.0001));

        fFragColor = shadow * L;
    }
);

GLSkelSMVPLRenderer3::DrawClusterNodeVisibilityPassData::DrawClusterNodeVisibilityPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uClusterPosition(m_Program, "uClusterPosition", true),
    m_uClusterNodeShadowMap(m_Program, "uClusterNodeShadowMap", true),
    m_uClusterNodeMVPMatrices(m_Program, "uClusterNodeMVPMatrices", true),
    m_uGBuffer(m_Program) {
}



GLSkelSMVPLRenderer3::GLSkelSMVPLRenderer3():
    m_bDoVPLShadingPass(true), m_bNoShadow(false), m_bRenderIrradiance(true),
    m_bDisplayCostPerPixel(false), m_nSelectedClusterNode(UNDEFINED_NODE),
    m_ProjectionMatrix(1.f), m_ViewMatrix(1.f) {
}

void GLSkelSMVPLRenderer3::setResolution(size_t width, size_t height) {
    {
        GLTexFormat formats[GBUFFER_COUNT] = {
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT },
            { GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT }
        };
        m_GBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }

    // Number of tiles in each dimension
    uint32_t Sx = (width / TILE_SIZE) + (width % TILE_SIZE != 0);
    uint32_t Sy = (height / TILE_SIZE) + (height % TILE_SIZE != 0);

    m_TileCount = glm::ivec2(Sx, Sy);

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GeometryClustersTexture.glId());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI, width, height, 0,
                 GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    GLuint size = Sx * Sy;

    m_GClusterCountsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterCountsBuffer.makeResident(GL_READ_WRITE);

    m_GClusterTilesOffsetsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterTilesOffsetsBuffer.makeResident(GL_READ_WRITE);

    m_GClusterTotalCountBuffer.setSize(sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterTotalCountBuffer.makeResident(GL_READ_WRITE);

    size_t imageSize = width * height;

    m_GClusterToTileBuffer.setSize(imageSize * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterToTileBuffer.makeResident(GL_READ_WRITE);

    m_ClusterBBoxLowerBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterBBoxLowerBuffer.makeResident(GL_READ_WRITE);

    m_ClusterBBoxUpperBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterBBoxUpperBuffer.makeResident(GL_READ_WRITE);

    m_ClusterNodeBuffer.setSize(imageSize * sizeof(GLuint), GL_STATIC_DRAW);
    m_ClusterNodeBuffer.makeResident(GL_READ_WRITE);
}

void GLSkelSMVPLRenderer3::setUp() {
    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_NORMALDEPTH);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_DIFFUSE);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_DIFFUSE).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_GLOSSYSHININESS);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_CLUSTERNODE_INDICES);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_CLUSTERNODE_INDICES).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GEOMETRYCLUSTERS).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GeometryClustersTexture.glId());
    glBindImageTexture(IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS, m_GeometryClustersTexture.glId(),
                       0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32UI);

    glActiveTexture(GL_TEXTURE0);
}

void GLSkelSMVPLRenderer3::findUniqueGeometryClusters() {
    {
        GLTimer timer(m_FindUniqueGeometryClusterPassData.m_TimeElapsedQuery);

        m_FindUniqueGeometryClusterPassData.m_Program.use();

        m_FindUniqueGeometryClusterPassData.m_uClusterCountsBuffer.set(m_GClusterCountsBuffer.getGPUAddress());
        m_FindUniqueGeometryClusterPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
        m_FindUniqueGeometryClusterPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());
        m_FindUniqueGeometryClusterPassData.m_uClusterNodeBuffer.set(m_ClusterNodeBuffer.getGPUAddress());

        glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
        m_FindUniqueGeometryClusterPassData.m_uInvProjMatrix.set(rcpProjMatrix);
        m_FindUniqueGeometryClusterPassData.m_uRcpViewMatrix.set(glm::inverse(m_ViewMatrix));

        m_FindUniqueGeometryClusterPassData.m_uScreenSize.set(float(m_GBuffer.getWidth()),
                                            float(m_GBuffer.getHeight()));

        glDispatchCompute(m_TileCount.x, m_TileCount.y, 1);
    }

    {
        GLTimer timer(m_CountGeometryClusterPassData.m_TimeElapsedQuery);

        m_CountGeometryClusterPassData.m_Program.use();

        uint32_t tileCount = m_TileCount.x * m_TileCount.y;
        uint32_t workGroupSize = 1024;
        uint32_t workGroupCount = (tileCount / workGroupSize) + (tileCount % workGroupSize);

        m_CountGeometryClusterPassData.m_uClusterTilesOffsetsBuffer.set(m_GClusterTilesOffsetsBuffer.getGPUAddress());
        m_CountGeometryClusterPassData.m_uClusterCountsBuffer.set(m_GClusterCountsBuffer.getGPUAddress());
        m_CountGeometryClusterPassData.m_uClusterToTileBuffer.set(m_GClusterToTileBuffer.getGPUAddress());
        m_CountGeometryClusterPassData.m_uClusterCount.set(m_GClusterTotalCountBuffer.getGPUAddress());

        // ONLY WORKS FOR IMAGE WITH LESS THAN 1024 TILES
        // Can be extended for more but require more compute shader code to handle the parallel
        // segmented scan
        assert(workGroupCount <= 1);

        m_CountGeometryClusterPassData.m_uTileCount.set(tileCount);

        glDispatchCompute(workGroupCount, 1, 1);

        m_GClusterTotalCountBuffer.makeNonResident();
        m_GClusterTotalCountBuffer.getData(1, &m_nGClusterCount);
        m_GClusterTotalCountBuffer.makeResident(GL_READ_WRITE);
    }
}

void GLSkelSMVPLRenderer3::geometryPass(const GLScene& scene) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    {
        GLTimer timer(m_GeometryPassData.m_TimeElapsedQuery);

        glViewport(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight());

        m_GeometryPassData.m_Program.use();

        m_GBuffer.bindForDrawing();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 MVPMatrix = m_ProjectionMatrix * m_ViewMatrix;
        glm::mat4 MVMatrix = m_ViewMatrix;
        glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

        m_GeometryPassData.m_uMVPMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVPMatrix));
        m_GeometryPassData.m_uMVMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVMatrix));
        m_GeometryPassData.m_uNormalMatrix.setMatrix3(1, GL_FALSE, glm::value_ptr(NormalMatrix));

        m_GeometryPassData.m_uSy.set(float(m_TileCount.y));
        m_GeometryPassData.m_uHalfFOVY.set(glm::radians(0.5f * getFOVY()));
        m_GeometryPassData.m_uZNear.set(getZNear());
        m_GeometryPassData.m_uZFar.set(getZFar());

        GLMaterialManager::TextureUnits units(TEXUNIT_MAT_DIFFUSE, TEXUNIT_MAT_GLOSSY, TEXUNIT_MAT_SHININESS);

        scene.render(m_GeometryPassData.m_MaterialUniforms, units);
    }

    findUniqueGeometryClusters();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLSkelSMVPLRenderer3::sendClusteredSkeletonData(const Scene& scene, const GLScene& glScene) {
    GLTimer timer(m_SendClusteredSkeletonDataPassElapsedTime);

    const CurvSkelClustering& clustering = scene.voxelSpace->getClustering();

    // Send 3D association texture:

    const Grid3D<GraphNodeIndex>& grid = clustering.getGrid();

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX3D_NODEGRID);
    glBindTexture(GL_TEXTURE_3D, m_GLCurvSkelClusteringBuffers.m_Grid.glId());
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32UI, grid.width(), grid.height(), grid.depth(), 0,
                 GL_RED_INTEGER, GL_UNSIGNED_INT, grid.data());


    // Fill GPU buffers with clustering data:

    m_ClusterNodeViewMatrices.clear();

    {
        std::vector<glm::vec4> nodeData;
        nodeData.reserve(clustering.size());
        for(auto nodeIdx: range(clustering.size())) {
            const auto& node = clustering.getNode(nodeIdx);
            nodeData.emplace_back(glm::vec4(node.P.x, node.P.y, node.P.z, node.maxball));
            m_ClusterNodeViewMatrices.emplace_back(glm::translate(glm::mat4(1), convert(-node.P)));
        }

        m_GLCurvSkelClusteringBuffers.m_NodeBuffer.setData(nodeData.size(), nodeData.data(), GL_STATIC_DRAW);
    }

    {
        std::vector<int32_t> neighbourLists;
        std::vector<glm::ivec2> neighbourOffsetsCounts;
        neighbourOffsetsCounts.reserve(clustering.size());

        int offset = 0;
        for(auto nodeIdx: range(clustering.size())) {
            const auto& neighbours = clustering.neighbours(nodeIdx);
            for(auto neighbourIdx: neighbours) {
                neighbourLists.emplace_back(neighbourIdx);
            }
            auto count = neighbours.size();
            neighbourOffsetsCounts.emplace_back(glm::ivec2(offset, count));
            offset += count;
        }

        m_GLCurvSkelClusteringBuffers.m_NeighbourBuffer.setData(neighbourLists.size(), neighbourLists.data(), GL_STATIC_DRAW);
        m_GLCurvSkelClusteringBuffers.m_NeighbourOffsetCountBuffer.setData(neighbourOffsetsCounts.size(), neighbourOffsetsCounts.data(), GL_STATIC_DRAW);
    }

    // Make buffers resident and get GPU addresses:

    GLuint64 nodeBufferAddr = m_GLCurvSkelClusteringBuffers.m_NodeBuffer.getGPUAddress();
    m_GLCurvSkelClusteringBuffers.m_NodeBuffer.makeResident(GL_READ_ONLY);

    GLuint64 neighbourBufferAddr = m_GLCurvSkelClusteringBuffers.m_NeighbourBuffer.getGPUAddress();
    m_GLCurvSkelClusteringBuffers.m_NeighbourBuffer.makeResident(GL_READ_ONLY);

    GLuint64 neighbourOffsetCountBufferAddr = m_GLCurvSkelClusteringBuffers.m_NeighbourOffsetCountBuffer.getGPUAddress();
    m_GLCurvSkelClusteringBuffers.m_NeighbourOffsetCountBuffer.makeResident(GL_READ_ONLY);


    // Initialize uniforms:

    m_WorldToGridMatrix = convert(scene.voxelSpace->getWorldToLocalTransform());

    auto initVoxelSpaceUniform = [&](const gloops::Program& program, VoxelSpaceUniform& uniform) {
        program.use();
        uniform.m_uWorldToGrid.set(m_WorldToGridMatrix);
        uniform.m_uVoxelSize.set(scene.voxelSpace->getVoxelSize());
        uniform.m_uNodeGrid.set(TEXUNIT_TEX3D_NODEGRID);

        uniform.m_uNodeBuffer.set(nodeBufferAddr);
        uniform.m_uNeighbourBuffer.set(neighbourBufferAddr);
        uniform.m_uNeighbourOffsetCountBuffer.set(neighbourOffsetCountBufferAddr);
    };

    initVoxelSpaceUniform(m_GeometryPassData.m_Program, m_GeometryPassData.m_uVoxelSpace);

    initVoxelSpaceUniform(m_LightAssignmentPassData.m_Program, m_LightAssignmentPassData.m_uVoxelSpace);

    initVoxelSpaceUniform(m_VPLShadingPassData.m_Program, m_VPLShadingPassData.m_uVoxelSpace);

    m_FindUniqueGeometryClusterPassData.m_Program.use();
    m_FindUniqueGeometryClusterPassData.m_uClusterNodeCount.set(GLuint(clustering.size()));


    // Render shadow maps of each node cluster:

    m_ClusterNodeShadowMaps.init(SHADOW_MAP_RESOLUTION, clustering.size());
    m_CubeShadowMapRenderer.render(glScene, m_ClusterNodeShadowMaps, m_ClusterNodeViewMatrices.data(),
                                   m_ClusterNodeViewMatrices.size());

    std::vector<GLuint64> texHandles;

    for(uint32_t i = 0; i < clustering.size(); ++i) {
        texHandles.emplace_back(glGetTextureHandleNV(m_ClusterNodeShadowMaps.m_Textures[i].glId()));
        glMakeTextureHandleResidentNV(texHandles.back());
    }

    m_ClusterNodeShadowMapHandlesBuffer.setData(texHandles, GL_STATIC_DRAW);
    m_ClusterNodeShadowMapHandlesBuffer.makeResident(GL_READ_ONLY);
}

void GLSkelSMVPLRenderer3::sendVPLs(const VPLContainer& vpls, const Scene& scene) {
    uint32_t workGroupSize = 1024;
    uint32_t workItemCount;

    {
        GLTimer timer(m_LightAssignmentPassData.m_TimeElapsedQuery);

        std::vector<glm::vec4> vplBuffer;

        for(auto idx: range(vpls.orientedVPLs.size())) {
            auto vpl = vpls.orientedVPLs[idx];

            glm::vec4 lightPosition = glm::vec4(convert(vpl.P), 1);
            glm::vec3 lightNormal = convert(vpl.N);

            vplBuffer.emplace_back(lightPosition);
            vplBuffer.emplace_back(glm::vec4(lightNormal, 0));
            vplBuffer.emplace_back(glm::vec4(vpl.L.r, vpl.L.g, vpl.L.b, 0));
        }

        m_VPLBuffer.setData(vplBuffer, GL_DYNAMIC_DRAW);
        m_VPLBuffer.makeResident(GL_READ_ONLY);

        m_VPLOffsetsCountsBuffer.setSize(sizeof(glm::ivec2) * m_nGClusterCount, GL_STATIC_DRAW);
        m_VPLOffsetsCountsBuffer.makeResident(GL_READ_WRITE);

        m_VPLIndexBuffer.setSize(vpls.orientedVPLs.size() * m_nGClusterCount * sizeof(GLuint), GL_STATIC_DRAW);
        m_VPLIndexBuffer.makeResident(GL_READ_WRITE);

        GLuint nextOffset = 0;
        m_LightAssignmentPassData.m_NextOffsetBuffer.setData(1, &nextOffset, GL_DYNAMIC_DRAW);
        m_LightAssignmentPassData.m_NextOffsetBuffer.makeResident(GL_READ_WRITE);

        m_LightAssignmentPassData.m_Program.use();

        // Set Uniforms
        m_LightAssignmentPassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
        m_LightAssignmentPassData.m_uClusterCount.set(GLuint(m_nGClusterCount));
        m_LightAssignmentPassData.m_uDoVPLShading.set(m_bDoVPLShadingPass);
        m_LightAssignmentPassData.m_uNoShadow.set(m_bNoShadow);
        m_LightAssignmentPassData.m_uVPLBuffer.set(m_VPLBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uVPLOffsetCountsBuffer.set(m_VPLOffsetsCountsBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uVPLIndexBuffer.set(m_VPLIndexBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uNextOffset.set(m_LightAssignmentPassData.m_NextOffsetBuffer.getGPUAddress());

        m_LightAssignmentPassData.m_uClusterTilesOffsetsBuffer.set(m_GClusterTilesOffsetsBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterToTileBuffer.set(m_GClusterToTileBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uNodeClusterBuffer.set(m_ClusterNodeBuffer.getGPUAddress());

        m_LightAssignmentPassData.m_uFaceProjectionMatrices.setMatrix4(6, GL_FALSE, glm::value_ptr(m_CubeShadowMapRenderer.getFaceProjectionMatrices()[0]));
        m_LightAssignmentPassData.m_uClusterNodeShadowMapBuffer.set(m_ClusterNodeShadowMapHandlesBuffer.getGPUAddress());

        // Launch compute shader
        workItemCount = (m_nGClusterCount / workGroupSize) + (m_nGClusterCount % workGroupSize != 0);
        glDispatchCompute(workItemCount, 1, 1);

        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }

    {
        GLTimer timer(m_TransformVPLToViewSpacePassData.m_TimeElapsedQuery);

        glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

        m_TransformVPLToViewSpacePassData.m_Program.use();

        m_VPLBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);

        m_TransformVPLToViewSpacePassData.m_uViewMatrix.set(m_ViewMatrix);
        m_TransformVPLToViewSpacePassData.m_uNormalMatrix.set(NormalMatrix);
        m_TransformVPLToViewSpacePassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));

        uint32_t taskCount = uint32_t(vpls.orientedVPLs.size());
        workItemCount = (taskCount / workGroupSize) + (taskCount % workGroupSize != 0);
        glDispatchCompute(workItemCount, 1, 1);

        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }
}

void GLSkelSMVPLRenderer3::displayGClusters(GLVisualDataRenderer& renderer, const Scene& scene) {
    size_t imageSize = m_GBuffer.getWidth() * m_GBuffer.getHeight();
    size_t tileCount = m_TileCount.x * m_TileCount.y;

    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<GLuint> clusterCount(tileCount);

    m_GClusterCountsBuffer.makeNonResident();
    m_GClusterCountsBuffer.getData(clusterCount.size(), clusterCount.data());
    m_GClusterCountsBuffer.makeResident(GL_READ_WRITE);

    m_ClusterBBoxLowerBuffer.makeNonResident();
    m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_ClusterBBoxLowerBuffer.makeResident(GL_READ_WRITE);

    m_ClusterBBoxUpperBuffer.makeNonResident();
    m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());
    m_ClusterBBoxUpperBuffer.makeResident(GL_READ_WRITE);

    renderer.setUp();

    for(auto pair: index(clusterCount)) {
        GLuint offset = 1024 * pair.first;
        for(auto i: range(pair.second)) {
            glm::vec3 upper = glm::vec3(bboxUpper[offset + i]), lower = glm::vec3(bboxLower[offset + i]);
            float radius = glm::length(upper - lower) * 0.5f;
            renderer.renderSphere(0.5f * convert(upper + lower), radius, Col3f(1, 0, 0));
        }
    }
}

void GLSkelSMVPLRenderer3::shadingPass(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls, const Scene& scene) {

    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_VIEWPORT> viewport;

    sendVPLs(vpls, scene);

    {
        GLTimer timer(m_VPLShadingPassData.m_TimeElapsedQuery);

        glViewport(x, y, width, height);

        glClear(GL_COLOR_BUFFER_BIT);

        m_VPLShadingPassData.m_Program.use();

        // Send matrices
        m_VPLShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
        m_VPLShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);
        m_VPLShadingPassData.m_uTileCount.set(m_TileCount);
        m_VPLShadingPassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
        m_VPLShadingPassData.m_uDisplayCostPerPixel.set(m_bDisplayCostPerPixel);
        m_VPLShadingPassData.m_uVPLBuffer.set(m_VPLBuffer.getGPUAddress());
        m_VPLShadingPassData.m_uVPLIndexBuffer.set(m_VPLIndexBuffer.getGPUAddress());
        m_VPLShadingPassData.m_uClusterTilesOffsetsBuffer.set(m_GClusterTilesOffsetsBuffer.getGPUAddress());
        m_VPLShadingPassData.m_uVPLOffsetCountsBuffer.set(m_VPLOffsetsCountsBuffer.getGPUAddress());
        m_VPLShadingPassData.m_uRcpViewMatrix.set(rcpViewMatrix);
        m_VPLShadingPassData.m_uFaceProjectionMatrices.setMatrix4(6, GL_FALSE, glm::value_ptr(m_CubeShadowMapRenderer.getFaceProjectionMatrices()[0]));
        m_VPLShadingPassData.m_uClusterNodeShadowMapBuffer.set(m_ClusterNodeShadowMapHandlesBuffer.getGPUAddress());
        m_VPLShadingPassData.m_uRenderIrradiance.set(m_bRenderIrradiance);

        m_ScreenTriangle.render();
    }
}

void GLSkelSMVPLRenderer3::drawClusterNodeVisibilityPass(int x, int y, size_t width, size_t height,
                                   const Scene& scene, GraphNodeIndex clusterNode) {
    if(clusterNode == UNDEFINED_NODE) {
        std::cerr << "Select a cluster node plz." << std::endl;
        return;
    }

    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_VIEWPORT> viewport;
    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;

    glViewport(x, y, width, height);

    glClear(GL_COLOR_BUFFER_BIT);

    m_DrawClusterNodeVisibilityPassData.m_Program.use();
    m_DrawClusterNodeVisibilityPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
    m_DrawClusterNodeVisibilityPassData.m_uInvProjMatrix.set(rcpProjMatrix);
    m_DrawClusterNodeVisibilityPassData.m_uRcpViewMatrix.set(rcpViewMatrix);

    Vec3f P = scene.voxelSpace->getClustering().getNode(clusterNode).P;
    m_DrawClusterNodeVisibilityPassData.m_uClusterPosition.set(P.x, P.y, P.z);

    glm::mat4 faceMVPMatrices[6];
    for(uint32_t i = 0; i < 6; ++i) {
        faceMVPMatrices[i] = m_CubeShadowMapRenderer.getFaceProjectionMatrix(i) * m_ClusterNodeViewMatrices[clusterNode];
    }
    m_DrawClusterNodeVisibilityPassData.m_uClusterNodeMVPMatrices.setMatrix4(6, GL_FALSE, glm::value_ptr(faceMVPMatrices[0]));

    m_ClusterNodeShadowMapHandlesBuffer.bindBase(0, GL_SHADER_STORAGE_BUFFER);

    gloops::Uniform uClusterIndex(m_DrawClusterNodeVisibilityPassData.m_Program, "uClusterIndex", true);
    uClusterIndex.set(GLuint(clusterNode));

    m_ScreenTriangle.render();
}

void GLSkelSMVPLRenderer3::drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uTileCount.set(m_TileCount);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(dataToDisplay);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);

    //m_DrawBuffersPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
    //m_DrawBuffersPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterNodeBuffer.set(m_ClusterNodeBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterTilesOffsetsBuffer.set(m_GClusterTilesOffsetsBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterToTileBuffer.set(m_GClusterToTileBuffer.getGPUAddress());

    m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);

    m_ScreenTriangle.render();
}

void GLSkelSMVPLRenderer3::drawBuffers(int x, int y, size_t width, size_t height) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLTimer timer(m_DrawBuffersPassData.m_TimeElapsedQuery);

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uTileCount.set(m_TileCount);

    //m_DrawBuffersPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
    //m_DrawBuffersPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterNodeBuffer.set(m_ClusterNodeBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterTilesOffsetsBuffer.set(m_GClusterTilesOffsetsBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterToTileBuffer.set(m_GClusterToTileBuffer.getGPUAddress());

    m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    // Draw normals
    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);
    m_DrawBuffersPassData.m_uDataToDisplay.set(0);

    m_ScreenTriangle.render();

    // Draw depth
    glViewport(x + width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(1);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw Cluster node
    glViewport(x + 2 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(2);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster buffer
    glViewport(x + 4 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(4);
    m_DrawBuffersPassData.m_uViewport.set(fX + 4 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow lower point depth buffer
    glViewport(x, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(5);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow upper point depth buffer
    glViewport(x + width, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(6);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    glViewport(x + 2 * width, y - height, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(7);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    glViewport(x + 3 * width, y - height, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(8);
    m_DrawBuffersPassData.m_uViewport.set(fX + 3 * fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();
}

void GLSkelSMVPLRenderer3::getTimings(Timings& timings) {
    float scale = 1.f / 1000000;
    timings.sendClusteredSkeletonDataPass = m_SendClusteredSkeletonDataPassElapsedTime.waitResult() * scale;
    timings.geometryPass = m_GeometryPassData.m_TimeElapsedQuery.waitResult() * scale;
    timings.findUniqueClustersPass = m_FindUniqueGeometryClusterPassData.m_TimeElapsedQuery.waitResult() * scale;
    timings.countUniqueClustersPass = m_CountGeometryClusterPassData.m_TimeElapsedQuery.waitResult() * scale;
    timings.lightAssignementPass = m_LightAssignmentPassData.m_TimeElapsedQuery.waitResult() * scale;
    timings.lightTransformPass = m_TransformVPLToViewSpacePassData.m_TimeElapsedQuery.waitResult() * scale;
    timings.shadingPass = m_VPLShadingPassData.m_TimeElapsedQuery.waitResult() * scale;
    timings.drawBuffersPass = m_DrawBuffersPassData.m_TimeElapsedQuery.waitResult() * scale;
}

void GLSkelSMVPLRenderer3::drawClusterNodeShadowMap(int x, int y, size_t width, size_t height, GraphNodeIndex node) {
    glViewport(x, y, width, height);
    m_CubeShadowMapDrawer.drawShadowMap(m_ClusterNodeShadowMaps, node, TEXUNIT_CUBEMAP_CLUSTERNODESHADOWMAP);
}

}

