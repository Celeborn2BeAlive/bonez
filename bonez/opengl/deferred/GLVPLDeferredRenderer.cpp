#include "GLVPLDeferredRenderer.hpp"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "bonez/opengl/utils/embree_opengl.hpp"

#include "bonez/common/data/ColorMap.hpp"
#include "bonez/common/Progress.hpp"

namespace BnZ {

using namespace gloops;

const GLchar* GLVPLDeferredRenderer::GeometryPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;

uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat3 uNormalMatrix;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoords;
out vec3 vWorldSpaceNormal;
out vec3 vWorldSpacePosition;

void main() {
    vWorldSpacePosition = aPosition;
    vWorldSpaceNormal = aNormal;
    vPosition = vec3(uMVMatrix * vec4(aPosition, 1));
    vNormal = uNormalMatrix * aNormal;
    vTexCoords = aTexCoords;

    gl_Position = uMVPMatrix * vec4(aPosition, 1);
}

);

const GLchar* GLVPLDeferredRenderer::GeometryPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoords;
in vec3 vWorldSpacePosition;
in vec3 vWorldSpaceNormal;

struct Material {
    vec3 Kd;
    vec3 Ks;
    float shininess;
    sampler2D KdSampler;
    sampler2D KsSampler;
    sampler2D shininessSampler;
};

uniform Material uMaterial;

uniform float uZFar;

layout(location = 0) out vec4 fNormalDepth;
layout(location = 1) out vec3 fDiffuse;
layout(location = 2) out vec4 fGlossyShininess;

const float PI = 3.14159265358979323846264;

void main() {
    vec3 nNormal = normalize(vNormal);
    fNormalDepth.xyz = nNormal;
    fNormalDepth.w = -vPosition.z / uZFar; // Normalized depth value

    fDiffuse = uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb / PI;

    float shininess = uMaterial.shininess + texture(uMaterial.shininessSampler, vTexCoords).r;
    fGlossyShininess.rgb = uMaterial.Ks * texture(uMaterial.KsSampler, vTexCoords).rgb * (shininess + 2) / (2 * PI);
    fGlossyShininess.a = shininess;
}

);

GLVPLDeferredRenderer::GeometryPassData::GeometryPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_MaterialUniforms(m_Program),
    m_uMVPMatrix(m_Program, "uMVPMatrix", true),
    m_uMVMatrix(m_Program, "uMVMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true),
    m_uZFar(m_Program, "uZFar", true) {
}

const GLchar* GLVPLDeferredRenderer::VPLShadingPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vPositionInViewSpace;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 positionInViewSpace = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vPositionInViewSpace = positionInViewSpace.xyz / positionInViewSpace.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLVPLDeferredRenderer::VPLShadingPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

// The GBuffer:
uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;

// A buffer that contains the VPLs:
uniform samplerBuffer uVPLArray;

uniform bool uRenderIrradiance;

in vec3 vPositionInViewSpace;

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vPositionInViewSpace;

    vec3 Kd;
    if(uRenderIrradiance) {
        Kd = vec3(1.f / 3.14f);
    } else {
        Kd = texture(uDiffuseSampler, texCoords).rgb;
    }

    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    fFragColor = vec3(0, 0, 0);

    int end = textureSize(uVPLArray);

    for(int i = 0; i < end; ++i) {
        vec3 lightPosition = texelFetch(uVPLArray, i * 3).xyz;
        vec3 lightNormal = texelFetch(uVPLArray, i * 3 + 1).xyz;
        vec3 lightIntensity = texelFetch(uVPLArray, i * 3 + 2).xyz;

        vec3 wi = lightPosition - fragPosition;
        float dist = length(wi);
        wi /= dist;

        float cos_o = max(0, dot(-wi, lightNormal));
        float cos_i = max(0, dot(wi, normalDepth.xyz));

        float G = cos_i * cos_o / (dist * dist);

        vec3 r = reflect(-wi, normalDepth.xyz);
        float cos_s = max(0, dot(-normalize(fragPosition), r));

        vec3 brdf = Kd + KsShininess.rgb * pow(cos_s, KsShininess.a);
        fFragColor = fFragColor + G * lightIntensity * brdf;
    }
}

);

GLVPLDeferredRenderer::VPLShadingPassData::VPLShadingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uVPLArray(m_Program, "uVPLArray", true),
    m_uRenderIrradiance(m_Program, "uRenderIrradiance", true) {
    m_uSamplers[GBUFFER_NORMAL_DEPTH] = gloops::Uniform(m_Program, "uNormalDepthSampler", true);
    m_uSamplers[GBUFFER_DIFFUSE] = gloops::Uniform(m_Program, "uDiffuseSampler", true);
    m_uSamplers[GBUFFER_GLOSSY_SHININESS] = gloops::Uniform(m_Program, "uGlossyShininessSampler", true);
}

const GLchar* GLVPLDeferredRenderer::DrawBuffersPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLVPLDeferredRenderer::DrawBuffersPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;
uniform sampler2D uNormalDepthSampler;

uniform int uDataToDisplay; // 0 for normals, 1 for depth

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    if(uDataToDisplay == 0) {
        fFragColor = texture(uNormalDepthSampler, texCoords).rgb;
    } else if(uDataToDisplay == 1) {
        fFragColor = vec3(texture(uNormalDepthSampler, texCoords).a);
    }
}

);

GLVPLDeferredRenderer::DrawBuffersPassData::DrawBuffersPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uNormalDepthSampler(m_Program, "uNormalDepthSampler", true),
    m_uDataToDisplay(m_Program, "uDataToDisplay", true) {
}

GLVPLDeferredRenderer::GLVPLDeferredRenderer():
    m_bRenderIrradiance(true),
    m_ProjectionMatrix(1.f), m_ViewMatrix(1.f) {
}

void GLVPLDeferredRenderer::getTimings(Timings& timings) {
    float scale = 1.f / 1000000;
    timings.geometryPass = m_GeometryPassData.m_TimeElapsedQuery.waitResult() * scale;
    timings.sendVPLsPass = m_SendVPLsTimeElapsedQuery.waitResult() * scale;
    timings.shadingPass = m_VPLShadingPassData.m_TimeElapsedQuery.waitResult() * scale;
}

void GLVPLDeferredRenderer::setResolution(size_t width, size_t height) {
    {
        GLTexFormat formats[GBUFFER_COUNT] = {
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT }
        };
        m_GBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }
}

void GLVPLDeferredRenderer::geometryPass(const GLScene& scene) {
    GLTimer timer(m_GeometryPassData.m_TimeElapsedQuery);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    glViewport(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight());

    m_GeometryPassData.m_Program.use();

    m_GBuffer.bindForDrawing();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 MVPMatrix = m_ProjectionMatrix * m_ViewMatrix;
    glm::mat4 MVMatrix = m_ViewMatrix;
    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

    m_GeometryPassData.m_uMVPMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVPMatrix));
    m_GeometryPassData.m_uMVMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVMatrix));
    m_GeometryPassData.m_uNormalMatrix.setMatrix3(1, GL_FALSE, glm::value_ptr(NormalMatrix));
    m_GeometryPassData.m_uZFar.set(getZFar());

    GLMaterialManager::TextureUnits units(MAT_DIFFUSE_TEXUNIT, MAT_GLOSSY_TEXUNIT, MAT_SHININESS_TEXUNIT);

    scene.render(m_GeometryPassData.m_MaterialUniforms, units);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLVPLDeferredRenderer::setUp() {
    glActiveTexture(GL_TEXTURE0 + GBUFFER_NORMALDEPTH_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).glId());

    glActiveTexture(GL_TEXTURE0 + GBUFFER_DIFFUSE_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_DIFFUSE).glId());

    glActiveTexture(GL_TEXTURE0 + GBUFFER_GLOSSYSHININESS_TEXUNIT);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).glId());

    GLOOPS_CHECK_ERROR;

    m_VPLShadingPassData.m_Program.use();
    m_VPLShadingPassData.m_uSamplers[GBUFFER_NORMAL_DEPTH].set(GBUFFER_NORMALDEPTH_TEXUNIT);
    m_VPLShadingPassData.m_uSamplers[GBUFFER_DIFFUSE].set(GBUFFER_DIFFUSE_TEXUNIT);
    m_VPLShadingPassData.m_uSamplers[GBUFFER_GLOSSY_SHININESS].set(GBUFFER_GLOSSYSHININESS_TEXUNIT);
    m_VPLShadingPassData.m_uVPLArray.set(TEXBUFFER_VPLARRAY_TEXUNIT);

    m_DrawBuffersPassData.m_Program.use();
    m_DrawBuffersPassData.m_uNormalDepthSampler.set(GBUFFER_NORMALDEPTH_TEXUNIT);

    glActiveTexture(GL_TEXTURE0);
}

void GLVPLDeferredRenderer::sendVPLs(const VPLContainer& vpls, const Scene& scene) {
    GLTimer timer(m_SendVPLsTimeElapsedQuery);

    // On Transforme les coordonnées des VPLS pour les avoir dans l'espace
    // caméra dans le shader
    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));
    std::vector<float> values(3 * 3 * vpls.orientedVPLs.size());
    for(const auto& vplIndex: index(vpls.orientedVPLs)) {
        const OrientedVPL& vpl = vplIndex.second;

        uint32_t offset = vplIndex.first * 9;

        glm::vec4 lightPosition = m_ViewMatrix * glm::vec4(convert(vpl.P), 1);
        glm::vec3 lightNormal = NormalMatrix * convert(vpl.N);

        values[offset + 0] = lightPosition.x;
        values[offset + 1] = lightPosition.y;
        values[offset + 2] = lightPosition.z;
        values[offset + 3] = lightNormal.x;
        values[offset + 4] = lightNormal.y;
        values[offset + 5] = lightNormal.z;
        values[offset + 6] = vpl.L.r;
        values[offset + 7] = vpl.L.g;
        values[offset + 8] = vpl.L.b;
    }

    // On envoit les VPLs au GPU
    m_VPLBuffer.bind(TEXBUFFER_VPLARRAY_TEXUNIT);
    m_VPLBuffer.setData(values.size(), values.data(), GL_RGB32F);
}

void GLVPLDeferredRenderer::shadingPass(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls, const Scene& scene) {

    sendVPLs(vpls, scene);

    GLTimer timer(m_VPLShadingPassData.m_TimeElapsedQuery);

    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::CapabilityRestorer blending(GL_BLEND, true);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    glViewport(x, y, width, height);

    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);

    glClear(GL_COLOR_BUFFER_BIT);

    // VPL Shading pass

    m_VPLShadingPassData.m_Program.use();
    m_VPLShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
    m_VPLShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);
    m_VPLShadingPassData.m_uRenderIrradiance.set(m_bRenderIrradiance);

    m_ScreenTriangle.render();
}

void GLVPLDeferredRenderer::drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    glViewport(x, y, width, height);
    //m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(dataToDisplay);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);

    m_ScreenTriangle.render();
}

void GLVPLDeferredRenderer::drawBuffers(int x, int y, size_t width, size_t height) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    // Draw normals
    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);
    m_DrawBuffersPassData.m_uDataToDisplay.set(0);

    m_ScreenTriangle.render();

    // Draw depth
    glViewport(x + width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(1);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY, fW, fH);

    m_ScreenTriangle.render();
}

}
