#include "GLDeferredRenderer.hpp"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "bonez/opengl/utils/embree_opengl.hpp"

namespace BnZ {

namespace shaders {

static const GLchar* GEOMETRYPASS_VERTEX_SHADER =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;

uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat3 uNormalMatrix;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoords;

void main() {
    vPosition = vec3(uMVMatrix * vec4(aPosition, 1));
    vNormal = uNormalMatrix * aNormal;
    vTexCoords = aTexCoords;

    gl_Position = uMVPMatrix * vec4(aPosition, 1);
}

);

static const GLchar* GEOMETRYPASS_FRAGMENT_SHADER =
"#version 330\n"
GLOOPS_STRINGIFY(

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoords;

struct Material {
    vec3 Kd;
    vec3 Ks;
    float shininess;
    sampler2D KdSampler;
    sampler2D KsSampler;
    sampler2D shininessSampler;
};

uniform Material uMaterial;

uniform float uZNear;

layout(location = 0) out vec4 fNormalDepth;
layout(location = 1) out vec3 fDiffuse;
layout(location = 2) out vec4 fGlossyShininess;

const float PI = 3.14159265358979323846264;

void main() {
    fNormalDepth.xyz = normalize(vNormal);
    fNormalDepth.w = -vPosition.z / uZNear; // Normalized depth value

    fDiffuse = uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb / PI;

    float shininess = uMaterial.shininess + texture(uMaterial.shininessSampler, vTexCoords).r;
    fGlossyShininess.rgb = uMaterial.Ks * texture(uMaterial.KsSampler, vTexCoords).rgb * (shininess + 2) / (2 * PI);
    fGlossyShininess.a = shininess;
}

);

}

namespace shaders {

static const GLchar* SHADINGPASS_VERTEX_SHADER =
"#version 330\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec3 aPosition;

out vec3 vPositionInViewSpace;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 positionInViewSpace = uInvProjMatrix * vec4(aPosition.xy, -1.f, 1.f);
    vPositionInViewSpace = positionInViewSpace.xyz / positionInViewSpace.w;

    gl_Position = vec4(aPosition, 1);
}

);

static const GLchar* ORIENTEDVPL_FRAGMENT_SHADER_OLD =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;

uniform vec3 uLightPosition; // Position of the light in view space
uniform vec3 uLightNormal; // Normal of the light in view space
uniform vec3 uLightIntensity; // Intensity of the light

in vec3 vPositionInViewSpace;

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vPositionInViewSpace;

    vec3 wi = uLightPosition - fragPosition;
    float dist = length(wi);
    wi /= dist;

    float cos_o = max(0, dot(-wi, uLightNormal));

    float G = max(0, dot(wi, normalDepth.xyz)) * cos_o / (2 + dist * dist);

    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    vec3 r = reflect(-wi, normalDepth.xyz);

    fFragColor = G * uLightIntensity *
            (Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a));
}

);

static const GLchar* ORIENTEDVPL_FRAGMENT_SHADER =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;

uniform samplerBuffer uLightArray;

in vec3 vPositionInViewSpace;

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vPositionInViewSpace;
    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    int numLights = textureSize(uLightArray) / 3;

    for(int i = 0; i < numLights; ++i) {
        vec3 lightPosition = texelFetch(uLightArray, i * 3).xyz;
        vec3 lightNormal = texelFetch(uLightArray, i * 3 + 1).xyz;
        vec3 lightIntensity = texelFetch(uLightArray, i * 3 + 2).xyz;

        vec3 wi = lightPosition - fragPosition;
        float dist = length(wi);
        wi /= dist;

        float cos_o = max(0, dot(-wi, lightNormal));

        float G = max(0, dot(wi, normalDepth.xyz)) * cos_o / (2 + dist * dist);

        vec3 r = reflect(-wi, normalDepth.xyz);

        fFragColor = fFragColor + G * lightIntensity *
                (Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a));
    }
}

);

static const GLchar* DIRECTIONALVPL_FRAGMENT_SHADER =
"#version 330\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

uniform sampler2D uNormalDepthSampler;
uniform sampler2D uDiffuseSampler;
uniform sampler2D uGlossyShininessSampler;

uniform vec3 uLightDirection; // Normalized direction of the light in view space
uniform vec3 uLightIntensity; // Intensity of the light

in vec3 vPositionInViewSpace;

out vec3 fFragColor;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    vec4 normalDepth = texture(uNormalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vPositionInViewSpace;

    vec3 wi = -uLightDirection;

    float G = max(0, dot(wi, normalDepth.xyz));

    vec3 Kd = texture(uDiffuseSampler, texCoords).rgb;
    vec4 KsShininess = texture(uGlossyShininessSampler, texCoords);

    vec3 r = reflect(-wi, normalDepth.xyz);

    fFragColor = G * uLightIntensity *
            (Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a));
}

);



}

GLDeferredRenderer::GLDeferredRenderer(const GLSLProgramBuilder& programBuilder):
    m_GeometryPassProgram(buildProgram(shaders::GEOMETRYPASS_VERTEX_SHADER, shaders::GEOMETRYPASS_FRAGMENT_SHADER)),
    m_OrientedVPLShadingProgram(buildProgram(shaders::SHADINGPASS_VERTEX_SHADER, shaders::ORIENTEDVPL_FRAGMENT_SHADER)),
    m_DirectionalVPLShadingProgram(buildProgram(shaders::SHADINGPASS_VERTEX_SHADER, shaders::DIRECTIONALVPL_FRAGMENT_SHADER)),
    m_MaterialUniforms(m_GeometryPassProgram),
    m_ProjectionMatrix(1.f), m_ViewMatrix(1.f),
    m_GeometryPassUniforms(m_GeometryPassProgram),
    m_OrientedVPLUniforms(m_OrientedVPLShadingProgram),
    m_DirectionalVPLUniforms(m_DirectionalVPLShadingProgram) {
}

void GLDeferredRenderer::geometryPass(const GLScene& scene) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    
    m_GeometryPassProgram.use();
    
    m_GBuffer.bindForDrawing();
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 MVPMatrix = m_ProjectionMatrix * m_ViewMatrix;
    glm::mat4 MVMatrix = m_ViewMatrix;
    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));
    
    m_GeometryPassUniforms.uMVPMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVPMatrix));
    m_GeometryPassUniforms.uMVMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVMatrix));
    m_GeometryPassUniforms.uNormalMatrix.setMatrix3(1, GL_FALSE, glm::value_ptr(NormalMatrix));
    m_GeometryPassUniforms.uZNear.set(getZNear());

    GLMaterialManager::TextureUnits units(MAT_DIFFUSE_TEXUNIT, MAT_GLOSSY_TEXUNIT, MAT_SHININESS_TEXUNIT);

    scene.render(m_MaterialUniforms, units);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

struct Quad {
    gloops::BufferObject vbo;
    gloops::VertexArrayObject vao;
    size_t vertexCount;
    
    Quad() {
        float vertices[] = {
            -1, -1, 0,
             1, -1, 0,
             1,  1, 0,
            -1,  1, 0
        };
        
        vertexCount = 4;

        gloops::BufferBind vboBind(GL_ARRAY_BUFFER, vbo);
        vboBind.bufferData(vertexCount * 3, vertices, GL_STATIC_DRAW);
        
        gloops::VertexArrayBind vaoBind(vao);
        
        vaoBind.enableVertexAttribArray(0);
        vaoBind.vertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }

    void render() {
        glDrawArrays(GL_TRIANGLE_FAN, 0, vertexCount);
    }
};

struct OrientedVPLQuad {
    gloops::BufferObject vbo;

    // VBO for instanced rendering
    gloops::BufferObject lightPositionsVBO;
    gloops::BufferObject lightNormalsVBO;
    gloops::BufferObject lightIntensitiesVBO;

    gloops::VertexArrayObject vao;
    size_t vertexCount;

    size_t instanceCount;

    OrientedVPLQuad(): instanceCount(0) {
        float vertices[] = {
            -1, -1, 0,
             1, -1, 0,
             1,  1, 0,
            -1,  1, 0
        };

        vertexCount = 4;

        gloops::VertexArrayBind vaoBind(vao);

        {
            gloops::BufferBind vboBind(GL_ARRAY_BUFFER, vbo);
            vboBind.bufferData(vertexCount * 3, vertices, GL_STATIC_DRAW);

            GLuint location = 0;

            vaoBind.enableVertexAttribArray(location);
            vaoBind.vertexAttribPointer(location, 3, GL_FLOAT, GL_FALSE, 0, 0);
        }

        {
            gloops::BufferBind vboBind(GL_ARRAY_BUFFER, lightPositionsVBO);

            GLuint location = 1;

            vaoBind.enableVertexAttribArray(location);
            vaoBind.vertexAttribPointer(location, 3, GL_FLOAT, GL_FALSE, 0, 0);
            glVertexAttribDivisor(location, 1);
        }

        {
            gloops::BufferBind vboBind(GL_ARRAY_BUFFER, lightNormalsVBO);

            GLuint location = 2;

            vaoBind.enableVertexAttribArray(location);
            vaoBind.vertexAttribPointer(location, 3, GL_FLOAT, GL_FALSE, 0, 0);
            glVertexAttribDivisor(location, 1);
        }

        {
            gloops::BufferBind vboBind(GL_ARRAY_BUFFER, lightIntensitiesVBO);

            GLuint location = 3;

            vaoBind.enableVertexAttribArray(location);
            vaoBind.vertexAttribPointer(location, 3, GL_FLOAT, GL_FALSE, 0, 0);
            glVertexAttribDivisor(location, 1);
        }
    }

    void updateVPLs(const VPLContainer::OrientedVPLContainer& vpls, const glm::mat4& viewMatrix,
                    const glm::mat3& normalMatrix) {
        std::vector<glm::vec3> positions;
        positions.reserve(vpls.size());
        std::vector<glm::vec3> normals;
        normals.reserve(vpls.size());
        std::vector<glm::vec3> intensities;
        intensities.reserve(vpls.size());

        for(const OrientedVPL& vpl: vpls) {
            positions.push_back(glm::vec3(viewMatrix * glm::vec4(convert(vpl.P), 1)));
            normals.push_back(normalMatrix * convert(vpl.N));
            intensities.push_back(convert(vpl.L));
        }

        gloops::BufferBind bind(GL_ARRAY_BUFFER, lightPositionsVBO);
        bind.bufferData(positions.size(), positions.data(), GL_STATIC_DRAW);

        bind.bind(lightNormalsVBO);
        bind.bufferData(normals.size(), normals.data(), GL_STATIC_DRAW);

        bind.bind(lightIntensitiesVBO);
        bind.bufferData(intensities.size(), intensities.data(), GL_STATIC_DRAW);

        instanceCount = vpls.size();
    }

    void render() {
        glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, vertexCount, instanceCount);
        GLOOPS_CHECK_ERROR;
    }
};

void GLDeferredRenderer::shadingPass(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::CapabilityRestorer blending(GL_BLEND, true);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    glViewport(x, y, width, height);
    
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);
    
    glClear(GL_COLOR_BUFFER_BIT);

    glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

    {
        Quad quad;
        gloops::VertexArrayBind vaoBind(quad.vao);

        glActiveTexture(GL_TEXTURE0 + GBUFFER_NORMALDEPTH_TEXUNIT);
        glBindTexture(GL_TEXTURE_2D, m_GBuffer.getTexture(GBuffer::TEX_NORMAL_DEPTH).glId());

        glActiveTexture(GL_TEXTURE0 + GBUFFER_DIFFUSE_TEXUNIT);
        glBindTexture(GL_TEXTURE_2D, m_GBuffer.getTexture(GBuffer::TEX_DIFFUSE).glId());

        glActiveTexture(GL_TEXTURE0 + GBUFFER_GLOSSYSHININESS_TEXUNIT);
        glBindTexture(GL_TEXTURE_2D, m_GBuffer.getTexture(GBuffer::TEX_GLOSSY_SHININESS).glId());

        m_OrientedVPLShadingProgram.use();
        m_OrientedVPLUniforms.uViewport.set(float(x), float(y), float(width), float(height));
        m_OrientedVPLUniforms.uInvProjMatrix.set(glm::inverse(m_ProjectionMatrix));
        m_OrientedVPLUniforms.uSamplers[GBuffer::TEX_NORMAL_DEPTH].set(GLint(GBUFFER_NORMALDEPTH_TEXUNIT));
        m_OrientedVPLUniforms.uSamplers[GBuffer::TEX_DIFFUSE].set(GLint(GBUFFER_DIFFUSE_TEXUNIT));
        m_OrientedVPLUniforms.uSamplers[GBuffer::TEX_GLOSSY_SHININESS].set(GLint(GBUFFER_GLOSSYSHININESS_TEXUNIT));

        gloops::BufferObject lightBuffer;
        {
            gloops::BufferBind textureBuffer(GL_TEXTURE_BUFFER, lightBuffer);
            std::vector<float> values(3 * 3 * vpls.orientedVPLs.size());
            for(const auto& vpl: index(vpls.orientedVPLs)) {
                uint32_t offset = vpl.first * 9;

                glm::vec4 lightPosition = m_ViewMatrix * glm::vec4(convert(vpl.second.P), 1);
                glm::vec3 lightNormal = NormalMatrix * convert(vpl.second.N);

                values[offset + 0] = lightPosition.x;
                values[offset + 1] = lightPosition.y;
                values[offset + 2] = lightPosition.z;
                values[offset + 3] = lightNormal.x;
                values[offset + 4] = lightNormal.y;
                values[offset + 5] = lightNormal.z;
                values[offset + 6] = vpl.second.L.r;
                values[offset + 7] = vpl.second.L.g;
                values[offset + 8] = vpl.second.L.b;
            }
            textureBuffer.bufferData(values.size(), values.data(), GL_STATIC_DRAW);
        }

        gloops::TextureObject lightTexture;
        gloops::TextureBind textureBuffer(GL_TEXTURE_BUFFER, GBuffer::TEX_COUNT, lightTexture);

        glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F, lightBuffer.glId());
        GLOOPS_CHECK_ERROR;

        m_OrientedVPLUniforms.uLightArray.set(int(GBuffer::TEX_COUNT));

        quad.render();

        /*
        for(const OrientedVPL& vpl: vpls.orientedVPLs) {
            glm::vec4 lightPosition = m_ViewMatrix * glm::vec4(convert(vpl.P), 1);
            glm::vec3 lightNormal = NormalMatrix * convert(vpl.N);

            m_OrientedVPLUniforms.uLightPosition.set(lightPosition.x, lightPosition.y, lightPosition.z);
            m_OrientedVPLUniforms.uLightNormal.set(lightNormal.x, lightNormal.y, lightNormal.z);
            m_OrientedVPLUniforms.uLightIntensity.set(vpl.L.r, vpl.L.g, vpl.L.b);

            quad.render();
        }*/

        m_DirectionalVPLShadingProgram.use();

        m_DirectionalVPLShadingProgram.use();
        m_DirectionalVPLUniforms.uViewport.set(float(x), float(y), float(width), float(height));
        m_DirectionalVPLUniforms.uInvProjMatrix.set(glm::inverse(m_ProjectionMatrix));
        m_DirectionalVPLUniforms.uSamplers[GBuffer::TEX_NORMAL_DEPTH].set(GLint(GBUFFER_NORMALDEPTH_TEXUNIT));
        m_DirectionalVPLUniforms.uSamplers[GBuffer::TEX_DIFFUSE].set(GLint(GBUFFER_DIFFUSE_TEXUNIT));
        m_DirectionalVPLUniforms.uSamplers[GBuffer::TEX_GLOSSY_SHININESS].set(GLint(GBUFFER_GLOSSYSHININESS_TEXUNIT));

        for(const DirectionalVPL& vpl: vpls.directionalVPLs) {
            glm::vec4 lightDirection = m_ViewMatrix * glm::vec4(convert(vpl.D), 0);

            m_DirectionalVPLUniforms.uLightDirection.set(lightDirection.x, lightDirection.y, lightDirection.z);
            m_DirectionalVPLUniforms.uLightIntensity.set(vpl.L.r, vpl.L.g, vpl.L.b);

            quad.render();
        }
    }
}

}
