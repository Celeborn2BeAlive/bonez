#include <iostream>
#include "GBuffer.hpp"

#include <vector>

using namespace gloops;

namespace BnZ {

bool GBuffer::init(size_t width, size_t height) {
    static const GLint TEX_UNIT = 0;
    
    FramebufferBind fboBind(GL_DRAW_FRAMEBUFFER, m_Fbo);
    
    // Allocation of the textures for each attribute and attachment to the FBO
    for(size_t i = 0; i < TEX_COUNT; ++i) {
        TextureBind texBind(GL_TEXTURE_2D, TEX_UNIT, m_Textures[i]);
        texBind.texImage2D(0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);
        texBind.setMinFilter(GL_NEAREST);
        texBind.setMagFilter(GL_NEAREST);
        fboBind.framebufferTexture2D(GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, m_Textures[i], 0);
        
        // Add corresponding draw buffer GL constant
        m_DrawBuffers[i] = GL_COLOR_ATTACHMENT0 + i;
    }
    
    // Allocation and attachment of depth texture
    TextureBind texBind(GL_TEXTURE_2D, TEX_UNIT, m_DepthTexture);
    texBind.texImage2D(0, GL_DEPTH_COMPONENT32F, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    texBind.setMinFilter(GL_NEAREST);
    texBind.setMagFilter(GL_NEAREST);
    fboBind.framebufferTexture2D(GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_DepthTexture, 0);

    // check that the FBO is complete
    GLenum status = fboBind.checkFramebufferStatus();
    if(GL_FRAMEBUFFER_COMPLETE != status) {
        std::cerr << framebufferErrorString(status) << std::endl;
        return false;
    }
    
    return true;
}

}
