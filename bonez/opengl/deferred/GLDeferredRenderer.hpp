#ifndef _BONEZ_DEFERREDRENDERER_HPP_
#define _BONEZ_DEFERREDRENDERER_HPP_

#include "bonez/scene/Scene.hpp"
#include "opengl/shaders/GLSLProgramBuilder.hpp"
#include "opengl/scene/GLScene.hpp"
#include "GBuffer.hpp"

#include "bonez/scene/lights/vpls.hpp"


#include <vector>
#include <glm/glm.hpp>

namespace BnZ {

class GLDeferredRenderer {
public:
    GLDeferredRenderer(const GLSLProgramBuilder& programBuilder);

    void setResolution(size_t width, size_t height) {
        m_GBuffer.init(width, height);
    }

    void setProjectionMatrix(const glm::mat4& projectionMatrix) {
        m_ProjectionMatrix = projectionMatrix;
    }
    
    void setViewMatrix(const glm::mat4& viewMatrix) {
        m_ViewMatrix = viewMatrix;
    }
    
    const GBuffer& getGBuffer() const {
        return m_GBuffer;
    }

    void geometryPass(const GLScene& scene);
    
    void shadingPass(int x, int y, size_t width, size_t height,
        const VPLContainer& vpls);

private:
    enum TextureUnits {
        MAT_DIFFUSE_TEXUNIT,
        MAT_GLOSSY_TEXUNIT,
        MAT_SHININESS_TEXUNIT,
        GBUFFER_NORMALDEPTH_TEXUNIT,
        GBUFFER_DIFFUSE_TEXUNIT,
        GBUFFER_GLOSSYSHININESS_TEXUNIT
    };

    gloops::Program m_GeometryPassProgram;
    gloops::Program m_OrientedVPLShadingProgram;
    gloops::Program m_DirectionalVPLShadingProgram;

    GLMaterialUniforms m_MaterialUniforms;

    glm::mat4 m_ProjectionMatrix;
    glm::mat4 m_ViewMatrix;

    GBuffer m_GBuffer;
    
    struct GeometryPassUniforms {
        gloops::Uniform uMVPMatrix;
        gloops::Uniform uMVMatrix;
        gloops::Uniform uNormalMatrix;
        gloops::Uniform uZNear;
        
        GeometryPassUniforms(const gloops::Program& program):
            uMVPMatrix(program, "uMVPMatrix", true),
            uMVMatrix(program, "uMVMatrix", true),
            uNormalMatrix(program, "uNormalMatrix", true),
            uZNear(program, "uZNear", true) {
        }
    };

    struct ShadingPassUniforms {
        gloops::Uniform uViewport;
        gloops::Uniform uInvProjMatrix;

        gloops::Uniform uSamplers[GBuffer::TEX_COUNT];

        ShadingPassUniforms(const gloops::Program& program):
            uViewport(program, "uViewport", true),
            uInvProjMatrix(program, "uInvProjMatrix", true) {

            uSamplers[GBuffer::TEX_NORMAL_DEPTH] = gloops::Uniform(program, "uNormalDepthSampler", true);
            uSamplers[GBuffer::TEX_DIFFUSE] = gloops::Uniform(program, "uDiffuseSampler", true);
            uSamplers[GBuffer::TEX_GLOSSY_SHININESS] = gloops::Uniform(program, "uGlossyShininessSampler", true);
        }
    };

    struct OrientedVPLUniforms: public ShadingPassUniforms {
        /*gloops::Uniform uLightPosition;
        gloops::Uniform uLightNormal;
        gloops::Uniform uLightIntensity;
        
        OrientedVPLUniforms(const gloops::Program& program):
            ShadingPassUniforms(program),
            uLightPosition(program, "uLightPosition", true),
            uLightNormal(program, "uLightNormal", true),
            uLightIntensity(program, "uLightIntensity", true) {
        }*/

        gloops::Uniform uLightArray;

        OrientedVPLUniforms(const gloops::Program& program):
            ShadingPassUniforms(program),
            uLightArray(program, "uLightArray", true) {
        }
    };
    
    struct DirectionalVPLUniforms: public ShadingPassUniforms {
        gloops::Uniform uLightDirection;
        gloops::Uniform uLightIntensity;

        DirectionalVPLUniforms(const gloops::Program& program):
            ShadingPassUniforms(program),
            uLightDirection(program, "uLightDirection", true),
            uLightIntensity(program, "uLightIntensity", true) {
        }
    };
    
    GeometryPassUniforms m_GeometryPassUniforms;
    OrientedVPLUniforms m_OrientedVPLUniforms;
    DirectionalVPLUniforms m_DirectionalVPLUniforms;
};

}

#endif
