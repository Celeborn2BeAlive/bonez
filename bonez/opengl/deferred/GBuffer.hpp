#ifndef _BONEZ_GBUFFER_HPP_
#define _BONEZ_GBUFFER_HPP_

#include <gloops/gloops.hpp>

namespace BnZ {

class GBuffer {
public:
    enum TextureType {
        TEX_NORMAL_DEPTH,
        TEX_DIFFUSE,
        TEX_GLOSSY_SHININESS,
        TEX_COUNT
    };

    bool init(size_t width, size_t height);
    
    void bindForDrawing() {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_Fbo.glId());
        
        // Specify the outputs of the fragment shader
        glDrawBuffers(TEX_COUNT, m_DrawBuffers);
    }
    
    void bindForReading() const {
        glBindFramebuffer(GL_READ_FRAMEBUFFER, m_Fbo.glId());
    }
    
    void setReadBuffer(TextureType type) const {
        glReadBuffer(GL_COLOR_ATTACHMENT0 + type);
    }
    
    const gloops::TextureObject& getTexture(TextureType type) const {
        return m_Textures[type];
    }
    
    const gloops::TextureObject& getDepthTexture() const {
        return m_DepthTexture;
    }
    
private:
    gloops::FramebufferObject m_Fbo;
    gloops::TextureObject m_Textures[TEX_COUNT];
    gloops::TextureObject m_DepthTexture;
    
    GLenum m_DrawBuffers[TEX_COUNT];
};

}

#endif
