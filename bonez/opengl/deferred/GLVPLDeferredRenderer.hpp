#ifndef _BONEZ_GLVPLDEFERREDRENDERER_HPP_
#define _BONEZ_GLVPLDEFERREDRENDERER_HPP_

#include "bonez/scene/Scene.hpp"
#include "opengl/shaders/GLSLProgramBuilder.hpp"
#include "opengl/scene/GLScene.hpp"

#include "bonez/scene/lights/vpls.hpp"

#include <vector>
#include <glm/glm.hpp>

#include "bonez/opengl/utils/GLFramebuffer.hpp"
#include "bonez/opengl/utils/GLTextureBuffer.hpp"
#include "bonez/opengl/utils/GLQuery.hpp"

namespace BnZ {

class GLVPLDeferredRenderer {
public:
    GLVPLDeferredRenderer();

    void setResolution(size_t width, size_t height);

    void setProjectionMatrix(const glm::mat4& projectionMatrix) {
        m_ProjectionMatrix = projectionMatrix;
    }

    void setViewMatrix(const glm::mat4& viewMatrix) {
        m_ViewMatrix = viewMatrix;
    }

    void setUp();

    void geometryPass(const GLScene& scene);

    void shadingPass(int x, int y, size_t width, size_t height,
        const VPLContainer& vpls, const Scene& scene);

    void drawBuffers(int x, int y, size_t width, size_t height);

    void drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay);

    bool m_bRenderIrradiance;

    // In milliseconds
    struct Timings {
        float geometryPass;
        float sendVPLsPass;
        float shadingPass;

        float totalTime;

        Timings(): geometryPass(0), sendVPLsPass(0), shadingPass(0), totalTime(0) {
        }

        void updateTotalTime() {
            totalTime = geometryPass + sendVPLsPass + shadingPass;
        }
    };

    void getTimings(Timings& timings);

private:
    void sendVPLs(const VPLContainer& vpls, const Scene& scene);

    glm::mat4 m_ProjectionMatrix;
    glm::mat4 m_ViewMatrix;

    enum GBufferTextureType {
        GBUFFER_NORMAL_DEPTH,
        GBUFFER_DIFFUSE,
        GBUFFER_GLOSSY_SHININESS,
        GBUFFER_COUNT
    };
    GLFramebuffer<GBUFFER_COUNT> m_GBuffer;

    enum TextureUnits {
        MAT_DIFFUSE_TEXUNIT,
        MAT_GLOSSY_TEXUNIT,
        MAT_SHININESS_TEXUNIT,
        GBUFFER_NORMALDEPTH_TEXUNIT,
        GBUFFER_DIFFUSE_TEXUNIT,
        GBUFFER_GLOSSYSHININESS_TEXUNIT,
        TEXBUFFER_VPLARRAY_TEXUNIT
    };

    struct GeometryPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        GLMaterialUniforms m_MaterialUniforms;
        gloops::Uniform m_uMVPMatrix;
        gloops::Uniform m_uMVMatrix;
        gloops::Uniform m_uNormalMatrix;
        gloops::Uniform m_uZFar;

        GLQuery m_TimeElapsedQuery;

        GeometryPassData();
    };

    GeometryPassData m_GeometryPassData;

    GLQuery m_SendVPLsTimeElapsedQuery;

    struct VPLShadingPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uInvProjMatrix;

        gloops::Uniform m_uSamplers[GBUFFER_COUNT];

        gloops::Uniform m_uVPLArray;
        gloops::Uniform m_uRenderIrradiance;

        GLQuery m_TimeElapsedQuery;

        VPLShadingPassData();
    };

    VPLShadingPassData m_VPLShadingPassData;

    struct DrawBuffersPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uNormalDepthSampler;
        gloops::Uniform m_uDataToDisplay;

        DrawBuffersPassData();
    };

    DrawBuffersPassData m_DrawBuffersPassData;

    GLTextureBuffer m_VPLBuffer;

    ScreenTriangle m_ScreenTriangle;
};

}

#endif
