#ifndef _BONEZ_GLCLUSTEREDISMRENDERER_HPP_
#define _BONEZ_GLCLUSTEREDISMRENDERER_HPP_

#include "bonez/scene/Scene.hpp"
#include "opengl/shaders/GLSLProgramBuilder.hpp"
#include "opengl/scene/GLScene.hpp"

#include "bonez/scene/lights/vpls.hpp"

#include <vector>
#include <glm/glm.hpp>

#include "bonez/opengl/utils/GLFramebuffer.hpp"
#include "bonez/opengl/utils/GLTextureBuffer.hpp"
#include "bonez/opengl/utils/GLBuffer.hpp"
#include "bonez/opengl/utils/GLQuery.hpp"

#include "analysis/GLVisualDataRenderer.hpp"

#include "GLShadowMapRenderer.hpp"
#include "GLISMRenderer.hpp"

#include <atb.hpp>

namespace BnZ {

class GLClusteredISMRenderer {
public:
    GLClusteredISMRenderer();

    void setResolution(size_t width, size_t height);

    void setProjectionMatrix(const glm::mat4& projectionMatrix) {
        m_ProjectionMatrix = projectionMatrix;
    }

    void setViewMatrix(const glm::mat4& viewMatrix) {
        m_ViewMatrix = viewMatrix;
    }

    void setUp();

    void sampleScene(const Scene& scene);

    void geometryPass(const GLScene& scene);

    void shadingPass(int x, int y, size_t width, size_t height,
        const VPLContainer& vpls, const Scene& scene);

    void drawBuffers(int x, int y, size_t width, size_t height);

    void drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay);

    void displayGClusters(GLVisualDataRenderer& renderer, const Scene& scene);

    void drawISMs(int x, int y, size_t width, size_t height);

    void computeTimings();

    void setPrimaryLightSource(const Vec3f& P, const Col3f& L, const GLScene& glScene);

    void drawPrimaryLightShadowMap(int x, int y, size_t width, size_t height);

    void exposeIO(TwBar* bar);

    bool doesRenderIrradiance() const {
        return m_bRenderIrradiance;
    }

    bool doesRenderDirectLight() const {
        return m_bDoDirectLightPass;
    }

    float getDistanceClamp() const {
        return m_fDistClamp;
    }

    const GLISMContainer& getVPLISMContainer() const {
        return m_VPLISMContainer;
    }

    const GLISMContainer& getClusterISMContainer() const {
        return m_ClusterISMContainer;
    }



private:
    static const uint32_t TILE_SIZE = 32;

    GLQuery m_ComputeVPLISMsTimeElapsedQuery;
    void computeVPLISMs(const VPLContainer& vpls);

    GLQuery m_ComputeClusterISMsTimeElapsedQuery;
    void computeClusterISMs();

    void sendVPLs(const VPLContainer& vpls, const Scene& scene);

    glm::mat4 m_ProjectionMatrix;
    glm::mat4 m_ViewMatrix;

    GLPoints m_SampledScene;

    glm::uvec2 m_TileCount;

    enum GBufferTextureType {
        GBUFFER_NORMAL_DEPTH,
        GBUFFER_DIFFUSE,
        GBUFFER_GLOSSY_SHININESS,
        GBUFFER_COUNT
    };
    GLFramebuffer<GBUFFER_COUNT> m_GBuffer;

    enum TextureUnits {
        TEXUNIT_MAT_DIFFUSE,
        TEXUNIT_MAT_GLOSSY,
        TEXUNIT_MAT_SHININESS,
        TEXUNIT_GBUFFER_NORMALDEPTH,
        TEXUNIT_GBUFFER_DIFFUSE,
        TEXUNIT_GBUFFER_GLOSSYSHININESS,
        //TEXUNIT_GBUFFER_GEOMETRYCLUSTERS,

        // Clustered shading textures
        TEXUNIT_TEX2D_GEOMETRYCLUSTERS,

        TEXUNIT_TEXCUBEMAP_SHADOWMAP,

        TEXUNIT_COUNT
    };

    enum ImageUnits {
        IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS
    };

    struct GeometryPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        GLMaterialUniforms m_MaterialUniforms;
        gloops::Uniform m_uMVPMatrix;
        gloops::Uniform m_uMVMatrix;
        gloops::Uniform m_uNormalMatrix;
        gloops::Uniform m_uSy; // Number of subdivision in the Y direction
        gloops::Uniform m_uHalfFOVY;
        gloops::Uniform m_uZNear;
        gloops::Uniform m_uZFar;

        GLQuery m_TimeElapsedQuery;

        GeometryPassData();
    };

    struct GBufferUniform {
        gloops::Uniform normalDepthSampler;
        gloops::Uniform diffuseSampler;
        gloops::Uniform glossyShininessSampler;
        //gloops::Uniform geometryClusterSampler;

        GBufferUniform(const gloops::Program& program):
            normalDepthSampler(program, "uGBuffer.normalDepthSampler", true),
            diffuseSampler(program, "uGBuffer.diffuseSampler", true),
            glossyShininessSampler(program, "uGBuffer.glossyShininessSampler", true)
            /*geometryClusterSampler(program, "uGBuffer.geometryClusterSampler", true) */{

            program.use();

            normalDepthSampler.set(TEXUNIT_GBUFFER_NORMALDEPTH);
            diffuseSampler.set(TEXUNIT_GBUFFER_DIFFUSE);
            glossyShininessSampler.set(TEXUNIT_GBUFFER_GLOSSYSHININESS);
            //geometryClusterSampler.set(TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
        }
    };

    GeometryPassData m_GeometryPassData;

    struct FindUniqueGeometryClusterPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uClustersImage;
        gloops::Uniform m_uInvProjMatrix;
        gloops::Uniform m_uRcpViewMatrix;
        gloops::Uniform m_uScreenSize;
        gloops::Uniform m_uClusterCountsBuffer;
        gloops::Uniform m_uClusterBBoxLowerBuffer;
        gloops::Uniform m_uClusterBBoxUpperBuffer;
        gloops::Uniform m_uClusterNormalBuffer;

        gloops::Uniform m_uUseNormalClustering;

        gloops::Uniform m_uSy; // Number of subdivision in the Y direction
        gloops::Uniform m_uHalfFOVY;
        gloops::Uniform m_uZNear;
        gloops::Uniform m_uZFar;

        GBufferUniform m_uGBuffer;

        GLQuery m_TimeElapsedQuery;

        FindUniqueGeometryClusterPassData();
    };

    // This texture maps each pixel of the image to the local offset of its cluster.
    // The local offset is an index in the screen-space tile from 0 to the number of clusters in this tile
    // minus 1
    gloops::TextureObject m_GeometryClustersTexture;

    // This texture maps each screen-space tile (Tx, Ty) to the number of clusters
    // contains in this tile
    //gloops::TextureObject m_GeometryClusterCountsTexture;

    GLBuffer m_ClusterBBoxLowerBuffer;
    GLBuffer m_ClusterBBoxUpperBuffer;
    GLBuffer m_ClusterNormalBuffer;

    GLBuffer m_ClusterIrradianceBuffer;
    GLBuffer m_ClusterMeanIncidentDirectionBuffer;

    // This buffer contains for each tile (Tx, Ty) with index Tx + Ty * Sy the number of clusters
    // contains in this tile.
    // It is used by the Count Geometry Clusters Pass to count the total number of clusters
    // In this implementation, the total number of tiles must be less than the number of threads
    // in a work item (1024 on my computer)
    GLBuffer m_GClusterCountsBuffer;

    FindUniqueGeometryClusterPassData m_FindUniqueGeometryClusterPassData;

    void findUniqueGeometryClusters();

    struct CountGeometryClusterPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uTileCount;

        gloops::Uniform m_uClusterCountsBuffer;
        gloops::Uniform m_uClusterTilesOffsetsBuffer;
        gloops::Uniform m_uClusterCount;
        gloops::Uniform m_uClusterToTileBuffer;

        GLQuery m_TimeElapsedQuery;

        CountGeometryClusterPassData();
    };

    // A buffer that will contain for each tile a global offset for the clusters contained in this tile
    // After the CountGeometryClusterPass, let localID be the local cluster ID of a tile (Tx, Ty). Then it's
    // global cluster ID is globalID = m_GClusterTilesOffsetsBuffer[Tx + Ty * Sy] + localID
    GLBuffer m_GClusterTilesOffsetsBuffer;
    // A buffer that will contain the total number of clusters (buffer of size 1)
    GLBuffer m_GClusterTotalCountBuffer;
    // CPU version of the previous buffer
    GLuint m_nGClusterCount;

    GLBuffer m_GClusterToTileBuffer;

    CountGeometryClusterPassData m_CountGeometryClusterPassData;

    struct LightAssignmentPassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uVPLCount;
        gloops::Uniform m_uClusterCount;

        gloops::Uniform m_uNoShadow;

        gloops::Uniform m_uVPLBuffer;
        gloops::Uniform m_uVPLOffsetCountsBuffer;
        gloops::Uniform m_uVPLIndexBuffer;

        gloops::Uniform m_uClusterTilesOffsetsBuffer;
        gloops::Uniform m_uClusterToTileBuffer;
        gloops::Uniform m_uClusterBBoxLowerBuffer;
        gloops::Uniform m_uClusterBBoxUpperBuffer;
        gloops::Uniform m_uClusterNormalBuffer;
        gloops::Uniform m_uClusterIrradianceBuffer;
        gloops::Uniform m_uClusterMeanIncidentDirectionBuffer;

        GLISMRenderer::ISMUniforms m_uISMData;
        gloops::Uniform m_uClusterISMBuffer;
        gloops::Uniform m_uClusterViewMatrixBuffer;
        gloops::Uniform m_uZNear;
        gloops::Uniform m_uZFar;
        gloops::Uniform m_uISMOffset;

        gloops::Uniform m_uDistClamp;

        GLBuffer m_ClusterISMBuffer;
        GLBuffer m_ClusterViewMatrixBuffer;

        GLQuery m_TimeElapsedQuery;

        LightAssignmentPassData();
    };

    LightAssignmentPassData m_LightAssignmentPassData;

    struct TransformVPLToViewSpacePassData {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program;

        gloops::Uniform m_uVPLCount;
        gloops::Uniform m_uViewMatrix;
        gloops::Uniform m_uNormalMatrix;

        GLQuery m_TimeElapsedQuery;

        TransformVPLToViewSpacePassData();
    };

    TransformVPLToViewSpacePassData m_TransformVPLToViewSpacePassData;

    struct VPLShadingPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uInvProjMatrix;
        gloops::Uniform m_uTileCount;
        gloops::Uniform m_uVPLCount;
        gloops::Uniform m_uDisplayCostPerPixel;
        gloops::Uniform m_uVPLBuffer;
        gloops::Uniform m_uVPLIndexBuffer;
        gloops::Uniform m_uClusterTilesOffsetsBuffer;
        gloops::Uniform m_uVPLOffsetCountsBuffer;
        gloops::Uniform m_uRcpViewMatrix;
        gloops::Uniform m_uDistClamp;

        GBufferUniform m_uGBuffer;
        gloops::Uniform m_uGeometryClusterSampler;

        gloops::Uniform m_uRenderIrradiance;

        GLISMRenderer::ISMUniforms m_uISMData;
        gloops::Uniform m_uVPLISMBuffer;
        gloops::Uniform m_uVPLViewMatrixBuffer;

        gloops::Uniform m_uZNear;
        gloops::Uniform m_uZFar;

        gloops::Uniform m_uISMOffset;

        gloops::Uniform m_uNoShadow;

        gloops::Uniform m_uClusterCountsBuffer;
        gloops::Uniform m_uClusterTileOffsetBuffer;
        gloops::Uniform m_uClusterIrradianceBuffer;
        gloops::Uniform m_uClusterBBoxLowerBuffer;
        gloops::Uniform m_uClusterBBoxUpperBuffer;
        gloops::Uniform m_uClusterNormalBuffer;

        gloops::Uniform m_uTileWindow;
        gloops::Uniform m_uMaxDist;
        gloops::Uniform m_uUseIrradianceEstimate;

        GLBuffer m_VPLISMBuffer;
        GLBuffer m_VPLViewMatrixBuffer;

        GLQuery m_TimeElapsedQuery;

        VPLShadingPassData();
    };

    VPLShadingPassData m_VPLShadingPassData;

    struct DirectLightingPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        gloops::Uniform m_uInvProjMatrix;
        gloops::Uniform m_uShadowMapSampler;
        gloops::Uniform m_uLightPosition;
        gloops::Uniform m_uLightIntensity;
        gloops::Uniform m_uLightViewMatrix;
        gloops::Uniform m_uFaceProjectionMatrices;
        gloops::Uniform m_uRcpViewMatrix;

        gloops::Uniform m_uShadowMapOffset;
        gloops::Uniform m_uDistClamp;
        gloops::Uniform m_uRenderIrradiance;

        GBufferUniform m_uGBuffer;

        GLQuery m_TimeElapsedQuery;

        glm::vec3 m_LightPosition;
        glm::vec3 m_LightIntensity;
        glm::mat4 m_LightViewMatrix;
        bool m_DoDirectLightingPass;

        GLCubeShadowMapRenderer m_ShadowMapRenderer;
        GLCubeShadowMapContainer m_ShadowMapContainer;
        GLCubeShadowMapDrawer m_ShadowMapDrawer;

        DirectLightingPassData();
    };

    DirectLightingPassData m_DirectLightingPassData;

    struct DrawBuffersPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        GBufferUniform m_uGBuffer;
        gloops::Uniform m_uGeometryClusterSampler;
        gloops::Uniform m_uZFar;
        gloops::Uniform m_uDataToDisplay;
        gloops::Uniform m_uTileCount;
        gloops::Uniform m_uClusterTilesOffsetsBuffer;
        gloops::Uniform m_uClusterToTileBuffer;

        // Doesn't work ?
        //gloops::Uniform m_uClusterBBoxLowerBuffer;
        //gloops::Uniform m_uClusterBBoxUpperBuffer;

        GLQuery m_TimeElapsedQuery;

        DrawBuffersPassData();
    };

    DrawBuffersPassData m_DrawBuffersPassData;

    GLBuffer m_VPLBuffer;

    // Lists of indices for each GCluster
    GLBuffer m_VPLIndexBuffer;
    // Offset counts in lists of indices
    GLBuffer m_VPLOffsetsCountsBuffer;

    GLISMContainer m_VPLISMContainer;

    GLISMContainer m_ClusterISMContainer;

    GLISMRenderer m_ISMRenderer;
    GLISMDrawer m_ISMDrawer;

    ScreenTriangle m_ScreenTriangle;

    // Input - Outputs

    // In milliseconds
    struct Timings {
        float geometryPass;
        float findUniqueClustersPass;
        float countUniqueClustersPass;
        float lightAssignementPass;
        float lightTransformPass;
        float vplShadingPass;
        float directLightShadingPass;
        float drawBuffersPass;
        float computeVPLISMPass;

        float totalTime;

        Timings(): geometryPass(0), findUniqueClustersPass(0), countUniqueClustersPass(0),
            lightAssignementPass(0), lightTransformPass(0),
            vplShadingPass(0), directLightShadingPass(0), drawBuffersPass(0), computeVPLISMPass(0), totalTime(0) {
        }

        void updateTotalTime() {
            totalTime = geometryPass + findUniqueClustersPass +
                    countUniqueClustersPass + lightAssignementPass + lightTransformPass +
                    vplShadingPass + directLightShadingPass + drawBuffersPass + computeVPLISMPass;
        }
    };

    bool m_bDoVPLShadingPass;
    bool m_bNoShadow;
    bool m_bUseVPLISMs;
    bool m_bDoDirectLightPass;
    bool m_bRenderIrradiance;
    bool m_bDisplayCostPerPixel;
    float m_fGamma;
    float m_fShadowMapOffset;
    float m_fDistClamp;
    bool m_bUseNormalClustering;
    bool m_bShowSpheres;
    bool m_bDoPull;
    bool m_bDoPullPush;
    float m_fPullTreshold;
    float m_fPushTreshold;
    float m_fISMOffset;
    float m_fISMPointMaxSize;
    int m_nPullPushNbLevels;
    float m_fISMNormalOffset;
    int m_nSelectedISMIndex;
    uint32_t m_nSelectedISMTextureIndex;
    uint32_t m_nSelectedISMLevel;
    uint32_t m_nSceneSampleCount;
    int m_nTileWindow;
    float m_fMaxDist;
    bool m_bUseIrradianceEstimate;
    Timings m_Timings;
};

}

#endif
