#ifndef _BONEZ_GLSHADOWMAPRENDERER_HPP
#define _BONEZ_GLSHADOWMAPRENDERER_HPP

#include "common.hpp"
#include "scene/GLScene.hpp"
#include "bonez/opengl/utils/GLutils.hpp"

#include "GLISMRenderer.hpp"

namespace BnZ {

// A container for a list of shadow maps of the same dimensions
class GLShadowMapContainer {
    static const uint32_t TEXTURE_SIZE = 4096;
public:
    // Resolution of a shadow map
    uint32_t m_nResX, m_nResY;
    // Dimension of a texture
    uint32_t m_nTexWidth, m_nTexHeight;

    // Number of shadow maps per texture in each dimension
    uint32_t m_nSMCountX, m_nSMCountY;

    // Number of shadow maps per texture (m_nSMCountX * m_nSMCountY) and total number of shadow maps
    uint32_t m_nSMCountPerTexture, m_nSMCount;

    // All the textures
    std::vector<gloops::TextureObject> m_Textures;

    GLShadowMapContainer() {
    }

    GLShadowMapContainer(uint32_t resX, uint32_t resY, uint32_t shadowMapCount) {
        init(resX, resY, shadowMapCount);
    }

    void init(uint32_t resX, uint32_t resY, uint32_t shadowMapCount) {
        m_nResX = resX;
        m_nResY = resY;
        m_nTexWidth = TEXTURE_SIZE;
        m_nTexHeight = TEXTURE_SIZE;
        m_nSMCountX = m_nTexWidth / m_nResX;
        m_nSMCountY = m_nTexHeight / m_nResY;
        m_nSMCountPerTexture = m_nSMCountX * m_nSMCountY;
        m_nSMCount = shadowMapCount;
        m_Textures.resize(m_nSMCount / m_nSMCountPerTexture + (m_nSMCount % m_nSMCountPerTexture != 0));
        for(const auto& texObject: m_Textures) {
            glBindTexture(GL_TEXTURE_2D, texObject.glId());
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, m_nTexWidth, m_nTexHeight, 0, GL_RED, GL_FLOAT, nullptr);
        }
    }
};

class GLShadowMapDrawer {
public:
    GLShadowMapDrawer();

    void drawShadowMap(const GLShadowMapContainer& container, uint32_t idx, int textureUnit = 0);

private:
    static const GLchar *s_VertexShader, *s_FragmentShader;

    gloops::Program m_Program;

    gloops::Uniform m_uSMX;
    gloops::Uniform m_uSMY;
    gloops::Uniform m_uResX;
    gloops::Uniform m_uResY;
    gloops::Uniform m_uTexture;

    ScreenTriangle m_ScreenTriangle;
};

class GLCubeShadowMapContainer {
public:
    uint32_t m_nRes;
    std::vector<GLTextureCubeMap> m_Textures;

    GLCubeShadowMapContainer() {
    }

    GLCubeShadowMapContainer(uint32_t res, uint32_t smCount) {
        init(res, smCount);
    }

    void bindForShadowTest(uint32_t idx) const {
        glBindTexture(GL_TEXTURE_CUBE_MAP, m_Textures[idx].glId());
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
    }

    void bindForDrawing(uint32_t idx) const {
        glBindTexture(GL_TEXTURE_CUBE_MAP, m_Textures[idx].glId());
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    }

    void init(uint32_t res, uint32_t smCount) {
        m_nRes = res;
        m_Textures.clear();
        m_Textures.resize(smCount);

        for(auto& texture: m_Textures) {
            texture.setMinFilter(GL_LINEAR);
            texture.setMagFilter(GL_LINEAR);
            texture.setWrapS(GL_CLAMP_TO_EDGE);
            texture.setWrapT(GL_CLAMP_TO_EDGE);

            texture.setCompareMode(GL_COMPARE_REF_TO_TEXTURE);
            texture.setCompareFunc(GL_LESS);

            for(uint face = 0; face < 6; ++face) {
                texture.setImage(face, 0, GL_DEPTH_COMPONENT32F, m_nRes, m_nRes, 0,
                             GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
            }
        }
    }
};

class GLOmniShadowMapRenderer {
public:
    GLOmniShadowMapRenderer();

    // Render "count" shadow maps with view matrices given in parameter
    void render(const GLScene& scene, GLShadowMapContainer& container, const glm::mat4* viewMatrices, uint32_t count);

    void render(const GLPoints& sampledScene, GLShadowMapContainer& container,
                const glm::mat4* viewMatrices, uint32_t count);

    void render(const GLScene& scene, GLCubeShadowMapContainer& container, const glm::mat4* viewMatrices, uint32_t count);

    float getElapsedTime() const {
        return m_ElapsedTimeQuery.waitResult() / 1000000.f;
    }

private:
    static const GLchar *s_VertexShader, *s_FragmentShader;

    gloops::Program m_Program;
    gloops::FramebufferObject m_FBO;
    gloops::TextureObject m_DepthBuffer;

    gloops::Uniform m_uMVMatrix;
    gloops::Uniform m_uZFar;
    gloops::Uniform m_uZNear;

    GLQuery m_ElapsedTimeQuery;
};

class GLCubeShadowMapRenderer {
public:
    GLCubeShadowMapRenderer();

    // Render 6 * "count" shadow maps
    void render(const GLScene& scene, GLCubeShadowMapContainer& container, const glm::mat4* viewMatrices,
                uint32_t count);

    const glm::mat4* getFaceProjectionMatrices() const {
        return m_FaceProjectionMatrices;
    }

    const glm::mat4& getFaceProjectionMatrix(uint32_t face) const {
        return m_FaceProjectionMatrices[face];
    }


private:
    static const GLchar *s_VertexShader, *s_FragmentShader, *s_GeometryShader;

    // Contains the projection matrix of each face
    glm::mat4 m_FaceProjectionMatrices[6];

    gloops::Program m_Program;
    gloops::FramebufferObject m_FBO;

    gloops::Uniform m_uFaceMVPMatrices;
};

class GLCubeShadowMapDrawer {
public:
    GLCubeShadowMapDrawer();

    void drawShadowMap(const GLCubeShadowMapContainer& container, uint32_t idx, int textureUnit = 0);

private:
    static const GLchar *s_VertexShader, *s_FragmentShader;

    gloops::Program m_Program;

    gloops::Uniform m_uTexture;
    gloops::Uniform m_uZNear;
    gloops::Uniform m_uZFar;

    ScreenTriangle m_ScreenTriangle;
};

}

#endif // _BONEZ_GLSHADOWMAPRENDERER_HPP
