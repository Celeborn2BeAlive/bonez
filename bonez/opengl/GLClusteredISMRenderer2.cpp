#include "GLClusteredISMRenderer2.hpp"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "bonez/opengl/utils/embree_opengl.hpp"

#include "bonez/common/Progress.hpp"
#include "bonez/common/sampling/Sampler.hpp"

#include "bonez/common/sampling/StratifiedSampler.hpp"
#include "bonez/common/cameras/ParaboloidCamera.hpp"
#include "bonez/common/sampling/patterns.hpp"

namespace BnZ {

using namespace gloops;

const GLchar* GLClusteredISMRenderer2::GeometryPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec3 aPosition;
    layout(location = 1) in vec3 aNormal;
    layout(location = 2) in vec2 aTexCoords;

    uniform mat4 uMVPMatrix;
    uniform mat4 uMVMatrix;
    uniform mat3 uNormalMatrix;

    out vec3 vPosition;
    out vec3 vNormal;
    out vec2 vTexCoords;
    out vec3 vWorldSpaceNormal;
    out vec3 vWorldSpacePosition;

    void main() {
        vWorldSpacePosition = aPosition;
        vWorldSpaceNormal = aNormal;
        vPosition = vec3(uMVMatrix * vec4(aPosition, 1));
        vNormal = aNormal;
        vTexCoords = aTexCoords;

        gl_Position = uMVPMatrix * vec4(aPosition, 1);
    }
);

const GLchar* GLClusteredISMRenderer2::GeometryPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    in vec3 vPosition;
    in vec3 vNormal;
    in vec2 vTexCoords;
    in vec3 vWorldSpacePosition;
    in vec3 vWorldSpaceNormal;

    struct Material {
        vec3 Kd;
        vec3 Ks;
        float shininess;
        sampler2D KdSampler;
        sampler2D KsSampler;
        sampler2D shininessSampler;
    };

    uniform Material uMaterial;

    uniform float uSy;
    uniform float uHalfFOVY;
    uniform float uZNear;
    uniform float uZFar;

    layout(location = 0) out vec4 fNormalDepth;
    layout(location = 1) out vec3 fDiffuse;
    layout(location = 2) out vec4 fGlossyShininess;

    const float PI = 3.14159265358979323846264;

    void main() {
        vec3 nNormal = normalize(vNormal);
        fNormalDepth.xyz = nNormal;
        fNormalDepth.w = -vPosition.z / uZFar; // Normalized depth value

        fDiffuse = uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb / PI;

        float shininess = uMaterial.shininess + texture(uMaterial.shininessSampler, vTexCoords).r;
        fGlossyShininess.rgb = uMaterial.Ks * texture(uMaterial.KsSampler, vTexCoords).rgb * (shininess + 2) / (2 * PI);
        fGlossyShininess.a = shininess;
    }
);

GLClusteredISMRenderer2::GeometryPassData::GeometryPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_MaterialUniforms(m_Program),
    m_uMVPMatrix(m_Program, "uMVPMatrix", true),
    m_uMVMatrix(m_Program, "uMVMatrix", true),
    m_uNormalMatrix(m_Program, "uNormalMatrix", true),
    m_uSy(m_Program, "uSy", true),
    m_uHalfFOVY(m_Program, "uHalfFOVY", true),
    m_uZNear(m_Program, "uZNear", true),
    m_uZFar(m_Program, "uZFar", true) {
}

// This shader sort the clusters of each tile (32x32 pixels) to compute the number of clusters
// in each tile, compute a new local index for each and link back each pixel to this index.
// At the end, we have 0 <= uClustersImage(x, y) <= uClusterCountsImage(tx, ty) - 1
const GLchar* GLClusteredISMRenderer2::FindUniqueGeometryClusterPassData::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
    };

    uniform GBuffer uGBuffer;

    // Outputs of the shader:

    // Link each pixel to a cluster index local to the tile
    layout(r32ui) uniform coherent writeonly uimage2D uClustersImage;

    uniform coherent uint* uClusterCountsBuffer;
    uniform coherent vec3* uClusterBBoxLowerBuffer;
    uniform coherent vec3* uClusterBBoxUpperBuffer;
    uniform coherent vec3* uClusterNormalBuffer;

    uniform mat4 uInvProjMatrix;
    uniform mat4 uRcpViewMatrix;
    uniform vec2 uScreenSize;

    uniform bool uUseNormalClustering;

    uniform uint uSy;
    uniform float uZNear;
    uniform float uZFar;
    uniform float uHalfFOVY;

    // 32x32 is the size of a tile in screen space
    layout(local_size_x = 32, local_size_y = 32) in;

    const uint cNbThreads = gl_WorkGroupSize.x * gl_WorkGroupSize.y;

    shared uint sSampleIndices[cNbThreads];
    shared uint sClusterBuffer[cNbThreads];
    shared uint sClusterOffsets[cNbThreads];

    shared vec3 sViewSpaceBBoxLower[cNbThreads];
    shared vec4 sViewSpaceBBoxUpper[cNbThreads];

    void oddEvenSortClusters() {
        for(uint i = 0; i < cNbThreads; ++i) {
            uint j;
            if(i % 2 == 0) {
                j = 2 * gl_LocalInvocationIndex;
            } else {
                j = 2 * gl_LocalInvocationIndex + 1;
            }

            uint lhs;
            uint rhs;

            if(j < cNbThreads - 1) {
                lhs = sClusterBuffer[j];
                rhs = sClusterBuffer[j + 1];
            }

            barrier();

            if(j < cNbThreads - 1 && lhs > rhs) {
                sClusterBuffer[j + 1] = lhs;
                sClusterBuffer[j] = rhs;

                uint tmp = sSampleIndices[j + 1];
                sSampleIndices[j + 1] = sSampleIndices[j];
                sSampleIndices[j] = tmp;
            }

            barrier();
        }
    }

    void mergeSortClusters() {
        uint tid = gl_LocalInvocationIndex;

        for(uint i = 1; i < cNbThreads; i *= 2) {
            uint listSize = i;
            uint listIdx = tid / i;
            uint listOffset = listIdx * listSize;
            uint otherListOffset;
            uint newListOffset;

            if(listIdx % 2 == 0) {
                otherListOffset = listOffset + listSize;
                newListOffset = listOffset;
            } else {
                otherListOffset = listOffset - listSize;
                newListOffset = otherListOffset;
            }

            // On doit calculer la position finale de l'element courant
            uint indexInMyList = tid - listOffset;

            uint indexInOtherList = 0;

            uint value = sClusterBuffer[tid];
            uint sIdx = sSampleIndices[tid];

            uint a = 0;
            uint b = listSize;
            uint middle;

            for(uint j = 1; j < listSize; j *= 2) {
                middle = (a + b) / 2;
                uint otherValue = sClusterBuffer[otherListOffset + middle];
                if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
                    b = middle;
                } else {
                    a = middle;
                }
            }

            uint otherValue = sClusterBuffer[otherListOffset + a];
            if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
                indexInOtherList = a;
            } else {
                indexInOtherList = b;
            }

            uint finalIndex = indexInMyList + indexInOtherList;

            barrier();

            sClusterBuffer[newListOffset + finalIndex] = value;
            sSampleIndices[newListOffset + finalIndex] = sIdx;

            barrier();
        }
    }

    void blellochExclusiveAddScan() {
        // Up-sweep phase
        uint a = 2 * gl_LocalInvocationIndex;
        uint b = 2 * gl_LocalInvocationIndex + 1;

        for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
            if(gl_LocalInvocationIndex < s) {
                sClusterOffsets[b] += sClusterOffsets[a];

                a = a * 2 + 1;
                b = b * 2 + 1;
            }
            barrier();
        }

        // Down-sweep phase
        if(gl_LocalInvocationIndex == 0) {
            sClusterOffsets[cNbThreads - 1] = 0;
        }
        barrier();

        for(uint s = 1; s < cNbThreads; s *= 2) {
            if(gl_LocalInvocationIndex < s) {
                a = (a - 1) / 2;
                b = (b - 1) / 2;

                uint tmp = sClusterOffsets[b];
                sClusterOffsets[b] += sClusterOffsets[a];
                sClusterOffsets[a] = tmp;
            }
            barrier();
        }
    }

    int getFaceIdx(vec3 wi) {
        vec3 absWi = abs(wi);

        float maxComponent = max(absWi.x, max(absWi.y, absWi.z));

        if(maxComponent == absWi.x) {
            if(wi.x > 0) {
                return 0;
            }
            return 1;
        }

        if(maxComponent == absWi.y) {
            if(wi.y > 0) {
                return 2;
            }
            return 3;
        }

        if(maxComponent == absWi.z) {
            if(wi.z > 0) {
                return 4;
            }
            return 5;
        }

        return -1;
    }

    vec3 getConeAxis(vec3 normal) {
        int faceIdx = getFaceIdx(normal);

        vec3 axis[] = vec3[](
                vec3(1, 0, 0),
                vec3(-1, 0, 0),
                vec3(0, 1, 0),
                vec3(0, -1, 0),
                vec3(0, 0, 1),
                vec3(0, 0, -1)
        );

        return axis[faceIdx];
    }

    void main() {
        // Local ID of the current thread
        uint tid = gl_LocalInvocationIndex;

        vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, ivec2(gl_GlobalInvocationID.xy), 0);
        vec3 N = normalDepth.xyz;
        uint cluster = uint(floor(log(normalDepth.w * uZFar / uZNear) / log(1 + 2 * tan(uHalfFOVY) / uSy)));
        if(uUseNormalClustering) {
            int faceIdx = getFaceIdx(N);
            cluster = 6 * cluster + faceIdx;
        }

        // Initialize the shared memory
        sClusterBuffer[gl_LocalInvocationIndex] = cluster;
        sClusterOffsets[gl_LocalInvocationIndex] = 0;
        sSampleIndices[gl_LocalInvocationIndex] = gl_LocalInvocationIndex;

        barrier();

        // Local sorting
        //oddEvenSortClusters();
        mergeSortClusters();

        bool isUniqueCluster = gl_LocalInvocationIndex == 0 ||
                sClusterBuffer[gl_LocalInvocationIndex] != sClusterBuffer[gl_LocalInvocationIndex - 1];

        // Initialization for scan
        if(isUniqueCluster) {
            sClusterOffsets[gl_LocalInvocationIndex] = 1;
        }
        barrier();

        // Compaction step
        blellochExclusiveAddScan();

        // Compute offset
        if(!isUniqueCluster) {
            sClusterOffsets[gl_LocalInvocationIndex] -= 1;
        }
        barrier();

        // Write the number of clusters for this tile in the output image
        if(gl_LocalInvocationIndex == 0) {
            uint clusterCount = sClusterOffsets[cNbThreads - 1] + 1;

            // FAUX ??! pas plutot gl_WorkGroupID.x + gl_WorkGroupID.y * NB TILES EN X ???
            // FONCTIONNE UNIQUEMENT SI NB TILES EN X = 32
            uClusterCountsBuffer[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_WorkGroupSize.x] = clusterCount;
        }

        // Original view sample that have been moved to this cell by the sort step
        uint sampleIndex = sSampleIndices[gl_LocalInvocationIndex];

        // Offset associated to the cluster of this sample
        uint clusterOffset = sClusterOffsets[gl_LocalInvocationIndex];

        // Coordinates of this sample
        uint x = sampleIndex % gl_WorkGroupSize.x;
        uint y = (sampleIndex - x) / gl_WorkGroupSize.x;
        ivec2 globalCoords = ivec2(gl_WorkGroupID.x * gl_WorkGroupSize.x + x,
                             gl_WorkGroupID.y * gl_WorkGroupSize.y + y);

        // Write the offset for this cluster in the new cluster image
        imageStore(uClustersImage, globalCoords, uvec4(clusterOffset, 0, 0, 0));

        // Read geometric information of the original sample
        normalDepth = texelFetch(uGBuffer.normalDepthSampler, globalCoords, 0);
        N = normalDepth.xyz;
        vec2 pixelNDC = vec2(-1.f, -1.f) + 2.f * (vec2(globalCoords) + vec2(0.5f, 0.5f)) / uScreenSize;
        vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(pixelNDC, 1.f, 1.f);
        farPointViewSpaceCoords = farPointViewSpaceCoords / farPointViewSpaceCoords.w;
        vec4 samplePointViewSpaceCoords = vec4(normalDepth.w * farPointViewSpaceCoords.xyz, 1);

        sViewSpaceBBoxLower[gl_LocalInvocationIndex] = vec3(uRcpViewMatrix * samplePointViewSpaceCoords);
        sViewSpaceBBoxUpper[gl_LocalInvocationIndex] = vec4(sViewSpaceBBoxLower[gl_LocalInvocationIndex], 0);

        barrier();

        // Reduce step to get bounds of the clusters
        for(uint s = 1; s < cNbThreads; s *= 2) {
            uint other = tid + s;
            if(other < cNbThreads && sClusterOffsets[other] == clusterOffset) {
                //sData[tid] = max(sData[tid], sData[other]);
                sViewSpaceBBoxLower[tid] = min(sViewSpaceBBoxLower[tid], sViewSpaceBBoxLower[other]);
                sViewSpaceBBoxUpper[tid] = max(sViewSpaceBBoxUpper[tid], sViewSpaceBBoxUpper[other]);
            }
            barrier();
        }

        // Write bounds to ouput images
        if(isUniqueCluster) {
            uint globalOffset = (gl_WorkGroupID.x + gl_NumWorkGroups.x * gl_WorkGroupID.y) * cNbThreads + clusterOffset;

            uClusterBBoxLowerBuffer[globalOffset] = sViewSpaceBBoxLower[tid];
            uClusterBBoxUpperBuffer[globalOffset] = sViewSpaceBBoxUpper[tid].xyz;

            if(uUseNormalClustering) {
                uClusterNormalBuffer[globalOffset] = getConeAxis(N);
            }
        }

        if(!uUseNormalClustering) {
            barrier();

            sViewSpaceBBoxUpper[gl_LocalInvocationIndex] = vec4(N, 1);

            barrier();

            // Reduce step to get normal of the clusters
            for(uint s = 1; s < cNbThreads; s *= 2) {
                uint other = tid + s;
                if(other < cNbThreads && sClusterOffsets[other] == clusterOffset) {
                    sViewSpaceBBoxUpper[tid] = sViewSpaceBBoxUpper[tid] + sViewSpaceBBoxUpper[other];
                }
                barrier();
            }

            // Write normal to ouput image
            if(isUniqueCluster) {
                uint globalOffset = (gl_WorkGroupID.x + gl_NumWorkGroups.x * gl_WorkGroupID.y) * cNbThreads + clusterOffset;
                uClusterNormalBuffer[globalOffset] = sViewSpaceBBoxUpper[tid].xyz / sViewSpaceBBoxUpper[tid].w;
            }
        }

        memoryBarrier();
    }
);

GLClusteredISMRenderer2::FindUniqueGeometryClusterPassData::FindUniqueGeometryClusterPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uGBuffer(m_Program),
    m_uClustersImage(m_Program, "uClustersImage", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uScreenSize(m_Program, "uScreenSize", true),
    m_uClusterCountsBuffer(m_Program, "uClusterCountsBuffer", true),
    m_uClusterBBoxLowerBuffer(m_Program, "uClusterBBoxLowerBuffer", true),
    m_uClusterBBoxUpperBuffer(m_Program, "uClusterBBoxUpperBuffer", true),
    m_uClusterNormalBuffer(m_Program, "uClusterNormalBuffer", true),
    m_uUseNormalClustering(m_Program, "uUseNormalClustering", true),
    m_uSy(m_Program, "uSy", true),
    m_uHalfFOVY(m_Program, "uHalfFOVY", true),
    m_uZNear(m_Program, "uZNear", true),
    m_uZFar(m_Program, "uZFar", true) {

    m_Program.use();
    m_uClustersImage.set(IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS);
}

const GLchar* GLClusteredISMRenderer2::CountGeometryClusterPassData::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    // Inputs
    // Number of screen space tiles
    uniform uint uTileCount;

    // For each tile (Tx, Ty), uClusterCountsBuffer[Tx + Ty * Sx] is the number of cluster contained in the tile
    uniform coherent uint* uClusterCountsBuffer;

    // Outputs
    // For each tile (Tx, Ty), uClusterTilesOffsetsBuffer[Tx + Ty * Sx] will be the global index of the first cluster
    // contained in the tile.
    uniform coherent uint* uClusterTilesOffsetsBuffer;
    // Will be the total number of clusters
    uniform coherent uint* uClusterCount;
    // Will contains for each cluster the index of the associated tile
    uniform coherent uint* uClusterToTileBuffer;

    layout(local_size_x = 1024) in;

    const uint cNbThreads = gl_WorkGroupSize.x;

    shared uint sGroupData[cNbThreads];

    void main() {
        uint tileClusterCount = 0;
        // Init
        if(gl_GlobalInvocationID.x >= uTileCount) {
            sGroupData[gl_LocalInvocationIndex] = 0;
        } else {
            tileClusterCount = sGroupData[gl_LocalInvocationIndex] = uClusterCountsBuffer[gl_GlobalInvocationID.x];
        }

        barrier();

        // Scan to compute offsets and cluster count

        // Up-sweep phase
        uint a = 2 * gl_LocalInvocationIndex;
        uint b = 2 * gl_LocalInvocationIndex + 1;

        for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
            if(gl_LocalInvocationIndex < s) {
                sGroupData[b] += sGroupData[a];

                a = a * 2 + 1;
                b = b * 2 + 1;
            }
            barrier();
        }

        // Down-sweep phase
        if(gl_LocalInvocationIndex == 0) {
            sGroupData[cNbThreads - 1] = 0;
        }
        barrier();

        for(uint s = 1; s < cNbThreads; s *= 2) {
            if(gl_LocalInvocationIndex < s) {
                a = (a - 1) / 2;
                b = (b - 1) / 2;

                uint tmp = sGroupData[b];
                sGroupData[b] += sGroupData[a];
                sGroupData[a] = tmp;
            }
            barrier();
        }

        if(gl_GlobalInvocationID.x < uTileCount) {
            uint tileOffset = uClusterTilesOffsetsBuffer[gl_GlobalInvocationID.x] = sGroupData[gl_LocalInvocationIndex];

            if(gl_GlobalInvocationID.x == uTileCount - 1) {
                *uClusterCount = sGroupData[gl_LocalInvocationIndex] + uClusterCountsBuffer[gl_GlobalInvocationID.x];
            }

            // Write correspondance from cluster to tile
            for(uint i = 0; i < tileClusterCount; ++i) {
                uClusterToTileBuffer[tileOffset + i] = gl_LocalInvocationIndex;
            }

            barrier();
        }

        memoryBarrier();
    }
);

GLClusteredISMRenderer2::CountGeometryClusterPassData::CountGeometryClusterPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uTileCount(m_Program, "uTileCount", true),
    m_uClusterCountsBuffer(m_Program, "uClusterCountsBuffer", true),
    m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
    m_uClusterCount(m_Program, "uClusterCount", true),
    m_uClusterToTileBuffer(m_Program, "uClusterToTileBuffer", true) {
}

const GLchar* GLClusteredISMRenderer2::LightAssignmentPassData::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_bindless_texture : enable\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    struct VPL {
        vec3 P;
        vec3 N;
        vec3 L;
    };

    uniform uint uVPLCount;
    uniform VPL* uVPLBuffer;

    // Outputs

    uniform coherent ivec2* uVPLOffsetCountsBuffer;
    uniform coherent uint* uVPLIndexBuffer;

    uniform uint uClusterCount;

    uniform coherent uint* uClusterTilesOffsetsBuffer;
    uniform coherent uint* uClusterToTileBuffer;
    uniform coherent vec3* uClusterBBoxLowerBuffer;
    uniform coherent vec3* uClusterBBoxUpperBuffer;
    uniform coherent vec3* uClusterNormalBuffer;

    // Output an estimation of the irradiance at the cluster
    uniform coherent vec3* uClusterIrradianceBuffer;
    uniform coherent vec3* uClusterMeanIncidentDirectionBuffer;

    struct ISMData {
        uint smResolution;
        uint smCountPerTexture;
        uint smCountPerDimension;
    };

    uniform ISMData uISMData;
    uniform coherent sampler2D* uClusterISMBuffer;
    uniform coherent mat4* uClusterViewMatrixBuffer;

    uniform float uZNear;
    uniform float uZFar;
    uniform float uISMOffset;
    uniform float uDistClamp;
    uniform bool uUseIrradianceCache;

    layout(local_size_x = 1024) in;

    uniform bool uNoShadow;

    uint tileID;
    uint clusterIdx;
    uint clusterLocalID;
    vec3 bboxLower;
    vec3 bboxUpper;
    vec3 bboxCenter;
    vec3 normal;
    float bboxRadius;

    bool ismLookup(vec3 Pws) {
        mat4 viewMatrix = uClusterViewMatrixBuffer[clusterIdx];

        vec4 Pvs = viewMatrix * vec4(Pws, 1); // Position in view space
        Pvs.z = -Pvs.z; // View the negative hemisphere

        // Paraboloid projection
        Pvs /= Pvs.w;

        vec3 v = Pvs.xyz;

        vec3 projectedPosition;
        projectedPosition.z = Pvs.z;

        float l = length(v);
        v /= l;

        vec3 h = v + vec3(0, 0, 1); // h = k * (x, y, 1)  => x = h.x / h.z   y = h.y / h.z

        float refDepth = (l - uZNear) / (uZFar - uZNear);

        if(refDepth < 0.f) {
            // The point is behind: coarse approximation
            return false;
        }

        // vProjectedPosition is in [-1, 1] x [-1, 1]
        projectedPosition.xy = h.xy / h.z;

        uint texIdx = clusterIdx / uISMData.smCountPerTexture;

        uint viewIdx = clusterIdx % uISMData.smCountPerTexture;

        // Dimension of a view in texture space
        float size = 1.f / uISMData.smCountPerDimension;

        // 2D coordinates of the viewport attached to the view
        uint i = viewIdx % uISMData.smCountPerDimension;
        uint j = (viewIdx - i) / uISMData.smCountPerDimension;

        // Coordinates between 0 and 1 in the viewport of the view
        vec2 xy = 0.5 * (projectedPosition.xy + vec2(1, 1));

        // Origin of the viewport of the view in clip space
        vec2 origin = vec2(i, j) * size;

        vec2 texCoords = origin + xy * size;

        float depth = texture(uClusterISMBuffer[texIdx], texCoords).r;

        return refDepth < depth + uISMOffset;
    }

    bool acceptVPL(VPL vpl) {
        if(uNoShadow) {
            return true;
        }
        return ismLookup(vpl.P);
    }

    vec3 computeIrradiance(VPL vpl, out vec3 wi) {
        wi = vpl.P - bboxCenter;
        float dist = length(wi);
        wi /= dist;

        float cos_o = max(0, dot(-wi, vpl.N));
        float cos_i = max(0, dot(wi, normal));

        dist = max(dist, uDistClamp);

        float G = cos_i * cos_o / (dist * dist);
        return G * vpl.L;
    }

    void main() {
        clusterIdx = uint(gl_GlobalInvocationID.x);

        if(clusterIdx >= uClusterCount) {
            return;
        }

        vec3 irradiance = vec3(0);
        vec3 meanIncidentDirection = vec3(0);

        tileID = uClusterToTileBuffer[clusterIdx];
        clusterLocalID = clusterIdx - uClusterTilesOffsetsBuffer[tileID];

        uint dataIdx = tileID * 1024 + clusterLocalID;

        bboxLower = uClusterBBoxLowerBuffer[dataIdx];
        bboxUpper = uClusterBBoxUpperBuffer[dataIdx];
        bboxCenter = (bboxLower + bboxUpper) * 0.5f;
        bboxRadius = length(bboxLower - bboxUpper) * 0.5f;
        normal = uClusterNormalBuffer[dataIdx];

        uint offset = uVPLCount * clusterIdx;
        uint c = offset;
        uint nbAcceptedLights = 0;
        for(uint i = 0; i < uVPLCount; ++i) {
            VPL vpl = uVPLBuffer[i];
            if(acceptVPL(vpl)) {
                if(!uUseIrradianceCache) {
                    uVPLIndexBuffer[c++] = i;
                } else {
                    vec3 wi;
                    irradiance += computeIrradiance(vpl, wi);
                    meanIncidentDirection += wi;
                }
                ++nbAcceptedLights;
            }
        }

        uClusterMeanIncidentDirectionBuffer[clusterIdx] = meanIncidentDirection / float(nbAcceptedLights);
        uClusterIrradianceBuffer[clusterIdx] = irradiance;
        uVPLOffsetCountsBuffer[clusterIdx] = ivec2(int(offset), int(nbAcceptedLights));

        memoryBarrier();
    }

);

GLClusteredISMRenderer2::LightAssignmentPassData::LightAssignmentPassData():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uClusterCount(m_Program, "uClusterCount", true),
    m_uNoShadow(m_Program, "uNoShadow", true),
    m_uVPLBuffer(m_Program, "uVPLBuffer", true),
    m_uVPLOffsetCountsBuffer(m_Program, "uVPLOffsetCountsBuffer", true),
    m_uVPLIndexBuffer(m_Program, "uVPLIndexBuffer", true),
    m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
    m_uClusterToTileBuffer(m_Program, "uClusterToTileBuffer", true),
    m_uClusterBBoxLowerBuffer(m_Program, "uClusterBBoxLowerBuffer", true),
    m_uClusterBBoxUpperBuffer(m_Program, "uClusterBBoxUpperBuffer", true),
    m_uClusterNormalBuffer(m_Program, "uClusterNormalBuffer", true),
    m_uClusterIrradianceBuffer(m_Program, "uClusterIrradianceBuffer", true),
    m_uClusterMeanIncidentDirectionBuffer(m_Program, "uClusterMeanIncidentDirectionBuffer", true),
    m_uISMData(m_Program),
    m_uClusterISMBuffer(m_Program, "uClusterISMBuffer", true),
    m_uClusterViewMatrixBuffer(m_Program, "uClusterViewMatrixBuffer", true),
    m_uZNear(m_Program, "uZNear", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uISMOffset(m_Program, "uISMOffset", true),
    m_uDistClamp(m_Program, "uDistClamp", true),
    m_uUseIrradianceCache(m_Program, "uUseIrradianceCache", true) {
    m_Program.use();
    m_uZNear.set(getZNear());
    m_uZFar.set(getZFar());
}

const GLchar* GLClusteredISMRenderer2::VPLShadingPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    out vec3 vFarPointViewSpaceCoords;

    uniform mat4 uInvProjMatrix;

    void main() {
        vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
        vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

        gl_Position = vec4(aPosition, 0, 1);
    }
);

const GLchar* GLClusteredISMRenderer2::VPLShadingPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_bindless_texture : enable\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

    uniform vec4 uViewport;

    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
    };

    uniform GBuffer uGBuffer;

    uniform usampler2D uGeometryClusterSampler;

    uniform uvec2 uTileCount;

    struct VPL {
        vec3 P;
        vec3 N;
        vec3 L;
    };

    uniform uint uVPLCount;
    uniform bool uDisplayCostPerPixel;
    uniform bool uRenderIrradiance;

    uniform VPL* uVPLBuffer;
    uniform uint* uVPLIndexBuffer;
    uniform uint* uClusterTilesOffsetsBuffer;

    struct ISMData {
        uint smResolution;
        uint smCountPerTexture;
        uint smCountPerDimension;
    };

    uniform ISMData uISMData;
    uniform sampler2D* uVPLISMBuffer;
    uniform mat4* uVPLViewMatrixBuffer;

    uniform float uDistClamp;

    uniform float uZNear;
    uniform float uZFar;
    uniform float uISMOffset;

    uniform bool uNoShadow;

    vec3 randomColor(uint value) {
        return fract(
                    sin(
                        vec3(float(value) * 12.9898,
                             float(value) * 78.233,
                             float(value) * 56.128)
                        )
                    * 43758.5453
                    );
    }

    vec3 heatColor(float t) {
        vec3 map[] = vec3[](vec3(0, 0, 1), vec3(0, 1, 0), vec3(1, 0, 0));
        t *= 3;
        int i = clamp(int(t), 0, 2);
        int j = clamp(i + 1, 0, 2);
        float r = t - i;
        return (1 - r) * map[i] + r * map[j];
    }

    in vec3 vFarPointViewSpaceCoords;

    out vec3 fFragColor;

    uniform ivec2* uVPLOffsetCountsBuffer;
    uniform mat4 uRcpViewMatrix;

    float ismLookup(uint vplIdx, vec3 Pws) {
        if(uNoShadow) {
            return 1.f;
        }

        mat4 viewMatrix = uVPLViewMatrixBuffer[vplIdx];

        vec4 Pvs = viewMatrix * vec4(Pws, 1); // Position in view space
        Pvs.z = -Pvs.z; // View the negative hemisphere

        // Paraboloid projection
        Pvs /= Pvs.w;

        vec3 v = Pvs.xyz;

        vec3 projectedPosition;
        projectedPosition.z = Pvs.z;

        float l = length(v);
        v /= l;

        vec3 h = v + vec3(0, 0, 1); // h = k * (x, y, 1)  => x = h.x / h.z   y = h.y / h.z

        float refDepth = (l - uZNear) / (uZFar - uZNear);

        if(projectedPosition.z < 0.f) {
            return 0.f;
        }

        // vProjectedPosition is in [-1, 1] x [-1, 1]
        projectedPosition.xy = h.xy / h.z;

        uint texIdx = vplIdx / uISMData.smCountPerTexture;

        uint viewIdx = vplIdx % uISMData.smCountPerTexture;

        // Dimension of a view in texture space
        float size = 1.f / uISMData.smCountPerDimension;

        // 2D coordinates of the viewport attached to the view
        uint i = viewIdx % uISMData.smCountPerDimension;
        uint j = (viewIdx - i) / uISMData.smCountPerDimension;

        // Coordinates between 0 and 1 in the viewport of the view
        vec2 xy = 0.5 * (projectedPosition.xy + vec2(1, 1));

        // Origin of the viewport of the view in clip space
        vec2 origin = vec2(i, j) * size;

        vec2 texCoords = origin + xy * size;

        float depth = texture(uVPLISMBuffer[texIdx], texCoords).r;

        return refDepth < depth + uISMOffset ? 1.f : 0.f;
    }

    uniform coherent uint* uClusterCountsBuffer; // A value for each tile
    uniform coherent vec3* uClusterIrradianceBuffer; // A value for each cluster

    uniform coherent vec3* uClusterBBoxLowerBuffer;
    uniform coherent vec3* uClusterBBoxUpperBuffer;
    uniform coherent vec3* uClusterNormalBuffer;

    uniform int uTileWindow;
    uniform float uMaxDist;
    uniform bool uUseIrradianceEstimate;

    uint clusterLocalID;

    vec3 estimateIrradiance(ivec2 tileCoords, vec3 Pws, vec3 Nws) {
        vec3 irradiance = vec3(0);
        float sumWeights = 0.f;

        float maxDist = 0.f;

        for(int j = -uTileWindow; j <= uTileWindow; ++j) {
            for(int i = -uTileWindow; i <= uTileWindow; ++i) {
                ivec2 tile = tileCoords + ivec2(i, j);
                int tileIdx = tile.x + tile.y * int(uTileCount.x);

                if(tileIdx >= 0 && tileIdx < int(uTileCount.x * uTileCount.y)) {
                    uint clusterCount = uClusterCountsBuffer[tileIdx];
                    uint clusterOffset = uClusterTilesOffsetsBuffer[tileIdx];
                    uint lastCluster = clusterOffset + clusterCount;
                    uint clusterLocalID = 0;
                    for(uint cluster = clusterOffset; cluster <= lastCluster; ++cluster) {
                        uint dataIdx = tileIdx * 1024 + clusterLocalID;

                        vec3 bboxLower = uClusterBBoxLowerBuffer[dataIdx];
                        vec3 bboxUpper = uClusterBBoxUpperBuffer[dataIdx];
                        vec3 normal = uClusterNormalBuffer[dataIdx];

                        vec3 center = 0.5f * (bboxLower + bboxUpper);

                        vec3 wi = normalize(center - Pws);

                        //if(dot(normal, Nws) > 0.f && dot(normal, wi) > 0.f) {
                            float d = distance(center, Pws);

                            if(d <= uMaxDist || (uMaxDist < 0 && d < distance(bboxUpper, bboxLower))) {
                                maxDist = max(maxDist, d);
                            }
                        //}
                        ++clusterLocalID;
                    }
                }
            }
        }

        for(int j = -uTileWindow; j <= uTileWindow; ++j) {
            for(int i = -uTileWindow; i <= uTileWindow; ++i) {
                ivec2 tile = tileCoords + ivec2(i, j);
                int tileIdx = tile.x + tile.y * int(uTileCount.x);

                if(tileIdx >= 0 && tileIdx < int(uTileCount.x * uTileCount.y)) {
                    uint clusterCount = uClusterCountsBuffer[tileIdx];
                    uint clusterOffset = uClusterTilesOffsetsBuffer[tileIdx];
                    uint lastCluster = clusterOffset + clusterCount;
                    uint cLID = 0;
                    for(uint cluster = clusterOffset; cluster <= lastCluster; ++cluster) {
                        uint dataIdx = tileIdx * 1024 + cLID;

                        vec3 bboxLower = uClusterBBoxLowerBuffer[dataIdx];
                        vec3 bboxUpper = uClusterBBoxUpperBuffer[dataIdx];
                        vec3 normal = uClusterNormalBuffer[dataIdx];

                        vec3 center = 0.5f * (bboxLower + bboxUpper);

                        float d = distance(center, Pws);
                        vec3 wi = normalize(center - Pws);

                        //if(dot(normal, Nws) > 0.f && dot(normal, wi) > 0.f) {
                            if(d <= uMaxDist || (uMaxDist < 0 && d < distance(bboxUpper, bboxLower))) {
                                float weight = maxDist - distance(center, Pws);
                                //float weight = 1.f / (distance(center, Pws) + sqrt(1 + dot(normal, Nws)));
                                sumWeights += weight;

                                irradiance += weight * uClusterIrradianceBuffer[cluster];
                            }
                        //}
                        ++cLID;
                    }
                }
            }
        }

        return irradiance / sumWeights;
    }

    void main() {
        vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

        ivec2 tileCoords = ivec2(texCoords * vec2(textureSize(uGeometryClusterSampler, 0)));
        tileCoords /= 32;

        uint tileIdx = tileCoords.x + tileCoords.y * uTileCount.x;

        clusterLocalID = texture(uGeometryClusterSampler, texCoords).r;
        uint clusterGlobalID = uClusterTilesOffsetsBuffer[tileIdx] + clusterLocalID;

        // Read fragment attributes
        vec4 normalDepth = texture(uGBuffer.normalDepthSampler, texCoords);
        vec3 fragPosition = normalDepth.w * vFarPointViewSpaceCoords;
        vec3 Kd;
        vec4 KsShininess;

        if(uRenderIrradiance) {
            Kd = vec3(1.f / 3.14f);
            KsShininess = vec4(0.f, 0.f, 0.f, 1.f);
        } else {
            Kd = texture(uGBuffer.diffuseSampler, texCoords).rgb;
            KsShininess = texture(uGBuffer.glossyShininessSampler, texCoords);
        }

        fFragColor = vec3(0, 0, 0);

        ivec2 offsetCount = uVPLOffsetCountsBuffer[clusterGlobalID];

        if(uDisplayCostPerPixel) {
            fFragColor = heatColor(float(offsetCount.y) / uVPLCount);
            return;
        }

        int end = offsetCount.x + offsetCount.y;

        // Position of the fragment in world space
        vec3 Pws = vec3(uRcpViewMatrix * vec4(fragPosition, 1));

        if(uUseIrradianceEstimate) {
            vec3 Nws = normalDepth.xyz;
            fFragColor = estimateIrradiance(tileCoords, Pws, Nws) * Kd;
            return;
        }

        for(int j = offsetCount.x; j < end; ++j) {
            uint vplIdx = uVPLIndexBuffer[j];
            VPL vpl = uVPLBuffer[vplIdx];

            vec3 wi = vpl.P - Pws;
            float dist = length(wi);
            wi /= dist;

            float cos_o = max(0, dot(-wi, vpl.N));
            float cos_i = max(0, dot(wi, normalDepth.xyz));

            dist = max(dist, uDistClamp);

            float G = cos_i * cos_o / (dist * dist);

            vec3 r = reflect(-wi, normalDepth.xyz);

            vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);

            float shadowFactor = ismLookup(vplIdx, Pws);

            fFragColor = fFragColor + shadowFactor * brdf * G * vpl.L;
        }
    }
);

GLClusteredISMRenderer2::VPLShadingPassData::VPLShadingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uTileCount(m_Program, "uTileCount", true),
    m_uVPLCount(m_Program, "uVPLCount", true),
    m_uDisplayCostPerPixel(m_Program, "uDisplayCostPerPixel", true),
    m_uVPLBuffer(m_Program, "uVPLBuffer", true),
    m_uVPLIndexBuffer(m_Program, "uVPLIndexBuffer", true),
    m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
    m_uVPLOffsetCountsBuffer(m_Program, "uVPLOffsetCountsBuffer", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uDistClamp(m_Program, "uDistClamp", true),
    m_uGBuffer(m_Program),
    m_uGeometryClusterSampler(m_Program, "uGeometryClusterSampler", true),
    m_uRenderIrradiance(m_Program, "uRenderIrradiance", true),
    m_uISMData(m_Program),
    m_uVPLISMBuffer(m_Program, "uVPLISMBuffer", true),
    m_uVPLViewMatrixBuffer(m_Program, "uVPLViewMatrixBuffer", true),
    m_uZNear(m_Program, "uZNear", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uISMOffset(m_Program, "uISMOffset", true),
    m_uNoShadow(m_Program, "uNoShadow", true),
    m_uClusterCountsBuffer(m_Program, "uClusterCountsBuffer", true),
    m_uClusterIrradianceBuffer(m_Program, "uClusterIrradianceBuffer", true),
    m_uClusterBBoxLowerBuffer(m_Program, "uClusterBBoxLowerBuffer", true),
    m_uClusterBBoxUpperBuffer(m_Program, "uClusterBBoxUpperBuffer", true),
    m_uClusterNormalBuffer(m_Program, "uClusterNormalBuffer", true),
    m_uTileWindow(m_Program, "uTileWindow", true),
    m_uMaxDist(m_Program, "uMaxDist", true),
    m_uUseIrradianceEstimate(m_Program, "uUseIrradianceEstimate", true) {
    m_Program.use();
    m_uGeometryClusterSampler.set(TEXUNIT_TEX2D_GEOMETRYCLUSTERS);

    m_uZNear.set(getZNear());
    m_uZFar.set(getZFar());
}


const GLchar* GLClusteredISMRenderer2::ICSplatPassData::s_VertexShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

// The input is an irradiance cache
layout(location = 0) in vec4 aICPositionRadius;
layout(location = 1) in vec3 aICNormal;
layout(location = 2) in vec3 aICIrradiance;

out vec4 vICPositionRadius;
out vec3 vICNormal;
out vec3 vICIrradiance;

uniform mat4 uMVPMatrix;

void main() {
    vICPositionRadius = aICPositionRadius;
    vICNormal = aICNormal;
    vICIrradiance = aICIrradiance;

    gl_Position = vec4(aICPositionRadius.xyz, 1);
}

);

const GLchar* GLClusteredISMRenderer2::ICSplatPassData::s_GeometryShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in vec4 vICPositionRadius[];
in vec3 vICNormal[];
in vec3 vICIrradiance[];

flat out vec4 gICPositionRadius;
flat out vec3 gICNormal;
flat out vec3 gICIrradiance;

uniform mat4 uViewMatrix;
uniform mat4 uProjMatrix;

void main() {
    vec3 P_vs = vec3(uViewMatrix * vec4(vICPositionRadius[0].xyz, 1));

    float w = vICPositionRadius[0].w;

    gICPositionRadius = vICPositionRadius[0];
    gICNormal = vICNormal[0];
    gICIrradiance = vICIrradiance[0];

    gl_Position = uProjMatrix * vec4(P_vs + vec3(-w, -w, 0), 1);

    EmitVertex();

    gICPositionRadius = vICPositionRadius[0];
    gICNormal = vICNormal[0];
    gICIrradiance = vICIrradiance[0];

    gl_Position = uProjMatrix * vec4(P_vs + vec3(w, -w, 0), 1);

    EmitVertex();

    gICPositionRadius = vICPositionRadius[0];
    gICNormal = vICNormal[0];
    gICIrradiance = vICIrradiance[0];

    gl_Position = uProjMatrix * vec4(P_vs + vec3(-w, w, 0), 1);

    EmitVertex();

    gICPositionRadius = vICPositionRadius[0];
    gICNormal = vICNormal[0];
    gICIrradiance = vICIrradiance[0];

    gl_Position = uProjMatrix * vec4(P_vs + vec3(w, w, 0), 1);

    EmitVertex();

    EndPrimitive();
}

);

const GLchar* GLClusteredISMRenderer2::ICSplatPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

uniform GBuffer uGBuffer;

flat in vec4 gICPositionRadius;
flat in vec3 gICNormal;
flat in vec3 gICIrradiance;

uniform mat4 uRcpViewMatrix;
uniform mat4 uRcpProjMatrix;

out vec4 fFragIrradianceWeight;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // The far point for that pixel in view space
    vec4 farPoint_vs = uRcpProjMatrix * vec4(2 * texCoords - vec2(1, 1), 1.f, 1.f);
    farPoint_vs /= farPoint_vs.w;

    // Read fragment attributes
    vec4 normalDepth = texture(uGBuffer.normalDepthSampler, texCoords);
    vec3 P_ws = vec3(uRcpViewMatrix * vec4(normalDepth.w * farPoint_vs.xyz, 1)); // Position of the fragment in world space

    float dist = distance(gICPositionRadius.xyz, P_ws);

    if(dist > gICPositionRadius.w) {
        discard;
    }

    float c = max(0, dot(gICNormal, normalDepth.xyz));

//    if(c < 0) {
//        discard;
//    }

    // Ward weights
    //float weight = 1.f / (dist / gICPositionRadius.w + sqrt(1 - c));
    float weight = max(0, c * (gICPositionRadius.w - dist));
    //float weight = max(0, c * exp(-dist / gICPositionRadius.w));

    //float x = (gICPositionRadius.w - dist) / gICPositionRadius.w;
    //float weight = max(0, c * x * x);

    fFragIrradianceWeight = weight * vec4(gICIrradiance, 1);
}

);

GLClusteredISMRenderer2::ICSplatPassData::ICSplatPassData():
    m_Program(buildProgram(s_VertexShader, s_GeometryShader, s_FragmentShader)),
    m_uViewMatrix(m_Program, "uViewMatrix", true),
    m_uProjMatrix(m_Program, "uProjMatrix", true),
    m_uViewport(m_Program, "uViewport", true),
    m_uRcpProjMatrix(m_Program, "uRcpProjMatrix", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uGBuffer(m_Program) {


    glBindVertexArray(m_ICVAO.glId());

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, m_ICVBO.glId());
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(ICVertex), GLOOPS_OFFSETOF(ICVertex, positionRcpRadius));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(ICVertex), GLOOPS_OFFSETOF(ICVertex, normal));
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(ICVertex), GLOOPS_OFFSETOF(ICVertex, irradiance));
}

const GLchar* GLClusteredISMRenderer2::ICDrawPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    out vec3 vFarPointViewSpaceCoords;

    uniform mat4 uRcpProjMatrix;

    void main() {
        vec4 farPointViewSpaceCoords = uRcpProjMatrix * vec4(aPosition, 1.f, 1.f);
        vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

        gl_Position = vec4(aPosition, 0, 1);
    }
);

const GLchar* GLClusteredISMRenderer2::ICDrawPassData::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    uniform vec4 uViewport;

    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
    };

    uniform GBuffer uGBuffer;
    uniform sampler2D uIrradianceSampler;

    uniform bool uRenderIrradiance;

    in vec3 vFarPointViewSpaceCoords;

    out vec3 fFragColor;

    void main() {
        vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

        // Read fragment attributes
        vec3 Kd;

        if(uRenderIrradiance) {
            Kd = vec3(1.f / 3.14f);
        } else {
            Kd = texture(uGBuffer.diffuseSampler, texCoords).rgb;
        }

        vec4 irradianceWeight = texture(uIrradianceSampler, texCoords);

        if(irradianceWeight.a == 0.f) {
            fFragColor = vec3(1, 0, 1);
            return;
        }

        fFragColor = Kd * irradianceWeight.rgb / irradianceWeight.a;
    }
);

GLClusteredISMRenderer2::ICDrawPassData::ICDrawPassData():
    m_Program(buildProgram(s_VertexShader, s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uRcpProjMatrix(m_Program, "uRcpProjMatrix", true),
    m_uGBuffer(m_Program),
    m_uIrradianceSampler(m_Program, "uIrradianceSampler", true),
    m_uRenderIrradiance(m_Program, "uRenderIrradiance", true) {
}

const GLchar* GLClusteredISMRenderer2::DirectLightingPassData::s_VertexShader =
"#version 430\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vFarPointViewSpaceCoords;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLClusteredISMRenderer2::DirectLightingPassData::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

uniform GBuffer uGBuffer;

uniform mat4 uFaceProjectionMatrices[6];
uniform samplerCubeShadow uShadowMapSampler;
uniform float uShadowMapOffset;
uniform float uDistClamp;

uniform vec3 uLightPosition;
uniform vec3 uLightIntensity;
uniform mat4 uLightViewMatrix;

uniform mat4 uRcpViewMatrix;

uniform bool uRenderIrradiance;

in vec3 vFarPointViewSpaceCoords;

out vec3 fFragColor;

int getFaceIdx(vec3 wi) {
    vec3 absWi = abs(wi);

    float maxComponent = max(absWi.x, max(absWi.y, absWi.z));

    if(maxComponent == absWi.x) {
        if(wi.x > 0) {
            return 0;
        }
        return 1;
    }

    if(maxComponent == absWi.y) {
        if(wi.y > 0) {
            return 2;
        }
        return 3;
    }

    if(maxComponent == absWi.z) {
        if(wi.z > 0) {
            return 4;
        }
        return 5;
    }

    return -1;
}

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uGBuffer.normalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vFarPointViewSpaceCoords;

    vec3 Kd;
    vec4 KsShininess;

    if(uRenderIrradiance) {
        Kd = vec3(1.f / 3.14f);
        KsShininess = vec4(0.f, 0.f, 0.f, 1.f);
    } else {
        Kd = texture(uGBuffer.diffuseSampler, texCoords).rgb;
        KsShininess = texture(uGBuffer.glossyShininessSampler, texCoords);
    }

    vec3 fragPos_ws = vec3(uRcpViewMatrix * vec4(fragPosition, 1));

    vec3 wi = uLightPosition - fragPos_ws;
    float dist = length(wi);
    wi /= dist;

    float cos_i = max(0, dot(wi, normalDepth.xyz));

    dist = max(dist, uDistClamp);

    float G = cos_i / (dist * dist);

    vec3 fragPos_ls = vec3(uLightViewMatrix * vec4(fragPos_ws, 1));
    vec3 shadowRay = normalize(fragPos_ws - uLightPosition);
    int face = getFaceIdx(shadowRay);
    vec4 faceClipSpacePosition = uFaceProjectionMatrices[face] * vec4(fragPos_ls, 1);
    float depthRef = faceClipSpacePosition.z / faceClipSpacePosition.w;
    depthRef = 0.5 * (depthRef + 1) - uShadowMapOffset;

    float shadowFactor = texture(uShadowMapSampler, vec4(shadowRay, depthRef));

    vec3 r = reflect(-wi, normalDepth.xyz);

    vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
    fFragColor = brdf * shadowFactor * G * uLightIntensity;
}

);

GLClusteredISMRenderer2::DirectLightingPassData::DirectLightingPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uInvProjMatrix(m_Program, "uInvProjMatrix", true),
    m_uShadowMapSampler(m_Program, "uShadowMapSampler", true),
    m_uLightPosition(m_Program, "uLightPosition", true),
    m_uLightIntensity(m_Program, "uLightIntensity", true),
    m_uLightViewMatrix(m_Program, "uLightViewMatrix", true),
    m_uFaceProjectionMatrices(m_Program, "uFaceProjectionMatrices", true),
    m_uRcpViewMatrix(m_Program, "uRcpViewMatrix", true),
    m_uShadowMapOffset(m_Program, "uShadowMapOffset", true),
    m_uDistClamp(m_Program, "uDistClamp", true),
    m_uRenderIrradiance(m_Program, "uRenderIrradiance", true),
    m_uGBuffer(m_Program),
    m_DoDirectLightingPass(false) {

    // Initialize constant uniforms
    m_Program.use();
}


const GLchar* GLClusteredISMRenderer2::DrawBuffersPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    void main() {
        gl_Position = vec4(aPosition, 0, 1);
    }
);

const GLchar* GLClusteredISMRenderer2::DrawBuffersPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    uniform vec4 uViewport;

    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
    };

    uniform GBuffer uGBuffer;

    uniform usampler2D uGeometryClusterSampler;

    uniform uint* uClusterTilesOffsetsBuffer;
    //uniform vec3* bClusterBBoxLowerBuffer;
    //uniform vec3* bClusterBBoxUpperBuffer;
    uniform uint* uClusterToTileBuffer;

    layout(std430, binding = 3) buffer ClusterBBoxLowerBuffer {
        vec3 bClusterBBoxLowerBuffer[];
    };

    layout(std430, binding = 4) buffer ClusterBBoxUpperBuffer {
        vec3 bClusterBBoxUpperBuffer[];
    };

    layout(std430, binding = 5) buffer ClusterNormalBuffer {
        vec3 bClusterNormalBuffer[];
    };

    uniform float uZFar;
    uniform uvec2 uTileCount;

    uniform int uDataToDisplay;

    out vec3 fFragColor;

    vec3 randomColor(uint value) {
        return fract(
                    sin(
                        vec3(float(value) * 12.9898,
                             float(value) * 78.233,
                             float(value) * 56.128)
                        )
                    * 43758.5453
                    );
    }

    vec3 randomColor(uvec3 value) {
        return fract(
                    sin(
                        vec3(float(value.x * value.y) * 12.9898,
                             float(value.y * value.z) * 78.233,
                             float(value.z * value.x) * 56.128)
                        )
                    * 43758.5453
                    );
    }

    void main() {
        vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

        ivec2 tileCoords = ivec2(texCoords * vec2(textureSize(uGeometryClusterSampler, 0).xy));
        tileCoords /= 32;

        uint tileIdx = tileCoords.x + tileCoords.y * uTileCount.x;

        if(uDataToDisplay == 0) {
            fFragColor = texture(uGBuffer.normalDepthSampler, texCoords).rgb;
        } else if(uDataToDisplay == 1) {
            fFragColor = vec3(texture(uGBuffer.normalDepthSampler, texCoords).a);
        } else if(uDataToDisplay == 2) {
            fFragColor = texture(uGBuffer.diffuseSampler, texCoords).rgb;
        } else if(uDataToDisplay == 3) {
            fFragColor = texture(uGBuffer.glossyShininessSampler, texCoords).rgb;
        } else if(uDataToDisplay == 4) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            fFragColor = randomColor(uClusterTilesOffsetsBuffer[tileIdx] + geometryCluster);
        } else if (uDataToDisplay == 5) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            vec3 bboxLower = bClusterBBoxLowerBuffer[tileIdx * 1024 + geometryCluster];
            fFragColor = vec3(-bboxLower.z / uZFar);
        } else if (uDataToDisplay == 6) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            vec3 bboxUpper = bClusterBBoxUpperBuffer[tileIdx * 1024 + geometryCluster];
            fFragColor = vec3(-bboxUpper.z / uZFar);
        } else if (uDataToDisplay == 7) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            vec3 normal = bClusterNormalBuffer[tileIdx * 1024 + geometryCluster];
            fFragColor = normal;

//            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
//            uint globalID = uClusterTilesOffsetsBuffer[tileIdx] + uint(geometryCluster);
//            fFragColor = randomColor(uClusterToTileBuffer[globalID]);
        }
    }
);

GLClusteredISMRenderer2::DrawBuffersPassData::DrawBuffersPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    m_uGBuffer(m_Program),
    m_uGeometryClusterSampler(m_Program, "uGeometryClusterSampler", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uDataToDisplay(m_Program, "uDataToDisplay", true),
    m_uTileCount(m_Program, "uTileCount", true),
    m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
    //m_uClusterBBoxLowerBuffer(m_Program, "bClusterBBoxLowerBuffer", true),
    //m_uClusterBBoxUpperBuffer(m_Program, "bClusterBBoxUpperBuffer", true),
    m_uClusterToTileBuffer(m_Program, "uClusterToTileBuffer", true) {
    m_Program.use();
    m_uGeometryClusterSampler.set(TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
}

void GLClusteredISMRenderer2::setResolution(size_t width, size_t height) {
    {
        GLTexFormat formats[GBUFFER_COUNT] = {
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
           // { GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT }
        };
        m_GBuffer.init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });
    }

    {
        GLTexFormat format = { GL_RGBA32F, GL_RGBA, GL_FLOAT };
        m_ICSplatPassData.m_IrradianceEstimationBuffer.init(
                    width, height, &format, { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });

        m_ICSplatPassData.m_IrradianceEstimationTextureHandle =
                glGetTextureHandleNV(m_ICSplatPassData.m_IrradianceEstimationBuffer.getColorBuffer(0).glId());
        glMakeTextureHandleResidentNV(m_ICSplatPassData.m_IrradianceEstimationTextureHandle);
    }

    // Number of tiles in each dimension
    uint32_t Sx = (width / TILE_SIZE) + (width % TILE_SIZE != 0);
    uint32_t Sy = (height / TILE_SIZE) + (height % TILE_SIZE != 0);

    m_TileCount = glm::ivec2(Sx, Sy);

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GeometryClustersData.m_GeometryClustersTexture.glId());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI, width, height, 0,
                 GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    GLuint size = Sx * Sy;

    m_GeometryClustersData.m_GClusterCountsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GeometryClustersData.m_GClusterCountsBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_GClusterTotalCountBuffer.setSize(sizeof(GLuint), GL_STATIC_DRAW);
    m_GeometryClustersData.m_GClusterTotalCountBuffer.makeResident(GL_READ_WRITE);

    size_t imageSize = width * height;

    m_GeometryClustersData.m_GClusterToTileBuffer.setSize(imageSize * sizeof(GLuint), GL_STATIC_DRAW);
    m_GeometryClustersData.m_GClusterToTileBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterNormalBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_GeometryClustersData.m_ClusterNormalBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterIrradianceBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_GeometryClustersData.m_ClusterIrradianceBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterMeanIncidentDirectionBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_GeometryClustersData.m_ClusterMeanIncidentDirectionBuffer.makeResident(GL_READ_WRITE);
}

void GLClusteredISMRenderer2::setUp() {
    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_NORMALDEPTH);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_DIFFUSE);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_DIFFUSE).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_GBUFFER_GLOSSYSHININESS);
    glBindTexture(GL_TEXTURE_2D, m_GBuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).glId());

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_GEOMETRYCLUSTERS);
    glBindTexture(GL_TEXTURE_2D, m_GeometryClustersData.m_GeometryClustersTexture.glId());

    glActiveTexture(GL_TEXTURE0);
}

void GLClusteredISMRenderer2::sampleScene(const Scene& scene, const Camera& camera) {
    Progress progress;
    progress.init("Scene sampling", 1);

    std::vector<GLPoint> samples;

    uint32_t count = m_nSceneSampleCount;

    if(!m_bUseCoherentIrradianceCache) {
        count = m_nSceneSampleCount * m_TileCount.x * m_TileCount.y;
    }

    if(m_nSceneSamplingMode == 0) {
        Sampler sampler;
        for(auto i = 0u; i < count; ++i) {
            SurfacePointRandom rdm(sampler.get1DSample(), sampler.get1DSample(),
                                   sampler.get1DSample(), sampler.get2DSample());
            SurfacePointSample s = scene.geometry.sampleSurfacePoint(rdm);
            samples.emplace_back(convert(s.value.P), convert(s.value.Ns));
        }
        progress.end();
    } else {
        float ratio = m_GBuffer.getWidth() / float(m_GBuffer.getHeight());
        uint32_t sppY = embree::ceil(embree::sqrt(count / ratio));
        embree::Random rng;
        StratifiedSampler sampler(sppY * ratio, sppY);
        sampler.init(0, 0, 0, 1, 1);
        IntegratorSample sample;
        while(sampler.getNextSample(sample)) {
            Ray ray = camera.ray(sample.getRasterPosition());
            Intersection I = intersect(ray, scene);
            if(I) {
                Vec2f hemisphereSample;
                createJittered2DPattern(&hemisphereSample, 1, 1, rng);
                Sample3f wo = embree::uniformSampleHemisphere(hemisphereSample.x, hemisphereSample.y, I.Ns);
                Intersection I2 = intersect(secondary_ray(I, wo), scene);
                if(I2) {
                    samples.emplace_back(convert(I2.P), convert(I2.Ns));
                }
            }
        }
    }

    std::cerr << samples.size() << std::endl;
    m_SampledScene.fill(samples.size(), &samples[0]);

    m_pScene = &scene;
}

void GLClusteredISMRenderer2::findUniqueGeometryClusters() {
    {
        GLTimer timer(m_FindUniqueGeometryClusterPassData.m_TimeElapsedQuery);

        m_FindUniqueGeometryClusterPassData.m_Program.use();

        glBindImageTexture(IMAGEUNIT_TEX2D_GEOMETRYCLUSTERS, m_GeometryClustersData.m_GeometryClustersTexture.glId(),
                           0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32UI);

        m_FindUniqueGeometryClusterPassData.m_uClusterCountsBuffer.set(m_GeometryClustersData.m_GClusterCountsBuffer.getGPUAddress());
        m_FindUniqueGeometryClusterPassData.m_uClusterBBoxLowerBuffer.set(m_GeometryClustersData.m_ClusterBBoxLowerBuffer.getGPUAddress());
        m_FindUniqueGeometryClusterPassData.m_uClusterBBoxUpperBuffer.set(m_GeometryClustersData.m_ClusterBBoxUpperBuffer.getGPUAddress());
        m_FindUniqueGeometryClusterPassData.m_uClusterNormalBuffer.set(m_GeometryClustersData.m_ClusterNormalBuffer.getGPUAddress());

        glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
        m_FindUniqueGeometryClusterPassData.m_uInvProjMatrix.set(rcpProjMatrix);
        m_FindUniqueGeometryClusterPassData.m_uRcpViewMatrix.set(glm::inverse(m_ViewMatrix));

        m_FindUniqueGeometryClusterPassData.m_uScreenSize.set(float(m_GBuffer.getWidth()),
                                            float(m_GBuffer.getHeight()));

        m_FindUniqueGeometryClusterPassData.m_uUseNormalClustering.set(m_bUseNormalClustering);

        m_FindUniqueGeometryClusterPassData.m_uSy.set(m_TileCount.y);
        m_FindUniqueGeometryClusterPassData.m_uHalfFOVY.set(glm::radians(0.5f * getFOVY()));
        m_FindUniqueGeometryClusterPassData.m_uZNear.set(getZNear());
        m_FindUniqueGeometryClusterPassData.m_uZFar.set(getZFar());

        glDispatchCompute(m_TileCount.x, m_TileCount.y, 1);

        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }

    {
        GLTimer timer(m_CountGeometryClusterPassData.m_TimeElapsedQuery);

        m_CountGeometryClusterPassData.m_Program.use();

        uint32_t tileCount = m_TileCount.x * m_TileCount.y;
        uint32_t workGroupSize = 1024;
        uint32_t workGroupCount = (tileCount / workGroupSize) + (tileCount % workGroupSize != 0);

        m_CountGeometryClusterPassData.m_uClusterTilesOffsetsBuffer.set(m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.getGPUAddress());
        m_CountGeometryClusterPassData.m_uClusterCountsBuffer.set(m_GeometryClustersData.m_GClusterCountsBuffer.getGPUAddress());
        m_CountGeometryClusterPassData.m_uClusterToTileBuffer.set(m_GeometryClustersData.m_GClusterToTileBuffer.getGPUAddress());
        m_CountGeometryClusterPassData.m_uClusterCount.set(m_GeometryClustersData.m_GClusterTotalCountBuffer.getGPUAddress());

        // ONLY WORKS FOR IMAGE WITH LESS THAN 1024 TILES
        // Can be extended for more but require more compute shader code to handle the parallel
        // segmented scan
        assert(workGroupCount <= 1);

        m_CountGeometryClusterPassData.m_uTileCount.set(tileCount);

        glDispatchCompute(workGroupCount, 1, 1);

        glMemoryBarrier(GL_ALL_BARRIER_BITS);

        m_GeometryClustersData.m_GClusterTotalCountBuffer.makeNonResident();
        m_GeometryClustersData.m_GClusterTotalCountBuffer.getData(1, &m_GeometryClustersData.m_nGClusterCount);
        m_GeometryClustersData.m_GClusterTotalCountBuffer.makeResident(GL_READ_WRITE);
    }
}

void GLClusteredISMRenderer2::geometryPass(const GLScene& scene) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    {
        GLTimer timer(m_GeometryPassData.m_TimeElapsedQuery);

        glViewport(0, 0, m_GBuffer.getWidth(), m_GBuffer.getHeight());

        m_GeometryPassData.m_Program.use();

        m_GBuffer.bindForDrawing();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 MVPMatrix = m_ProjectionMatrix * m_ViewMatrix;
        glm::mat4 MVMatrix = m_ViewMatrix;
        glm::mat3 NormalMatrix = glm::inverse(glm::transpose(glm::mat3(m_ViewMatrix)));

        m_GeometryPassData.m_uMVPMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVPMatrix));
        m_GeometryPassData.m_uMVMatrix.setMatrix4(1, GL_FALSE, glm::value_ptr(MVMatrix));
        m_GeometryPassData.m_uNormalMatrix.setMatrix3(1, GL_FALSE, glm::value_ptr(NormalMatrix));

        m_GeometryPassData.m_uSy.set(float(m_TileCount.y));
        m_GeometryPassData.m_uHalfFOVY.set(glm::radians(0.5f * getFOVY()));
        m_GeometryPassData.m_uZNear.set(getZNear());
        m_GeometryPassData.m_uZFar.set(getZFar());

        GLMaterialManager::TextureUnits units(TEXUNIT_MAT_DIFFUSE, TEXUNIT_MAT_GLOSSY, TEXUNIT_MAT_SHININESS);

        scene.render(m_GeometryPassData.m_MaterialUniforms, units);
    }
    findUniqueGeometryClusters();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLClusteredISMRenderer2::computeVPLISMs(const VPLContainer& vpls) {
    GLTimer timer(m_ComputeVPLISMsTimeElapsedQuery);

    if(m_bNoShadow || !m_bUseVPLISMs) {
        return;
    }

    std::vector<glm::mat4> viewMatrices;

    for(const auto& vpl: vpls.orientedVPLs) {
        glm::vec3 eye = convert(vpl.P + vpl.N * m_fISMNormalOffset);
        glm::vec3 point = eye + convert(vpl.N);
        glm::vec3 up;
        if(vpl.N.x != 0) {
            up = glm::vec3(-(vpl.N.y + vpl.N.z) / vpl.N.x, 1, 1);
        } else if(vpl.N.y != 0) {
            up = glm::vec3(1, -(vpl.N.x + vpl.N.z) / vpl.N.y, 1);
        } else {
            up = glm::vec3(1, 1, -(vpl.N.x + vpl.N.y) / vpl.N.z);
        }

        viewMatrices.emplace_back(glm::lookAt(eye, point, up));
    }

    m_ISMRenderer.render(m_SampledScene, m_VPLData.m_VPLISMContainer, viewMatrices.data(),
                         viewMatrices.size(), m_fISMPointMaxSize, m_bDoPull, m_bDoPullPush,
                         m_fPullTreshold, m_fPushTreshold, m_nPullPushNbLevels);

    m_VPLShadingPassData.m_VPLViewMatrixBuffer.makeNonResident();
    m_VPLShadingPassData.m_VPLViewMatrixBuffer.setData(viewMatrices, GL_DYNAMIC_DRAW);
    m_VPLShadingPassData.m_VPLISMBuffer.makeNonResident();
    m_VPLShadingPassData.m_VPLISMBuffer.setData(m_VPLData.m_VPLISMContainer.getTextureCount(),
                                                m_VPLData.m_VPLISMContainer.getTextureHandleBufferPtr(),
                                                GL_DYNAMIC_DRAW);
}

void GLClusteredISMRenderer2::computeClusterISMs() {
    GLTimer timer(m_ComputeClusterISMsTimeElapsedQuery);

    if(m_bNoShadow || m_bUseVPLISMs || m_bUseCPUIrradianceCache) {
        return;
    }

    size_t imageSize = m_GBuffer.getWidth() * m_GBuffer.getHeight();
    size_t tileCount = m_TileCount.x * m_TileCount.y;

    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<glm::vec4> normals(imageSize);
    std::vector<GLuint> clusterCount(tileCount);

    m_GeometryClustersData.m_GClusterCountsBuffer.getData(clusterCount.size(), clusterCount.data());
    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());
    m_GeometryClustersData.m_ClusterNormalBuffer.getData(normals.size(), normals.data());

    std::vector<glm::mat4> viewMatrices;

    for(auto pair: index(clusterCount)) {
        GLuint offset = 1024 * pair.first;
        for(auto i: range(pair.second)) {
            glm::vec3 upper = glm::vec3(bboxUpper[offset + i]), lower = glm::vec3(bboxLower[offset + i]);
            float radius = glm::length(upper - lower) * 0.5f;
            glm::vec3 normal = glm::vec3(normals[offset + i]);
            glm::vec3 center = 0.5f * (upper + lower);

            glm::vec3 eye = center + m_fISMNormalOffset * normal;
            glm::vec3 point = eye + normal;
            //glm::vec3 dir = point - eye;
            //glm::vec3 up = glm::normalize(glm::cross(dir, glm::vec3(dir.x + 1, dir.y, dir.z)));
            glm::vec3 up;
            if(normal.x != 0) {
                up = glm::vec3(-(normal.y + normal.z) / normal.x, 1, 1);
            } else if(normal.y != 0) {
                up = glm::vec3(1, -(normal.x + normal.z) / normal.y, 1);
            } else {
                up = glm::vec3(1, 1, -(normal.x + normal.y) / normal.z);
            }

            viewMatrices.emplace_back(glm::lookAt(eye, point, up));
        }
    }

    if(m_bUseCoherentIrradianceCache) {
        m_ISMRenderer.renderCoherent(m_SampledScene, m_GeometryClustersData.m_ClusterISMContainer, viewMatrices.data(),
                             viewMatrices.size(), m_fISMPointMaxSize, m_bDoPull, m_bDoPullPush,
                             m_fPullTreshold, m_fPushTreshold, m_nPullPushNbLevels);
    } else {
        m_ISMRenderer.render(m_SampledScene, m_GeometryClustersData.m_ClusterISMContainer, viewMatrices.data(),
                             viewMatrices.size(), m_fISMPointMaxSize, m_bDoPull, m_bDoPullPush,
                             m_fPullTreshold, m_fPushTreshold, m_nPullPushNbLevels);
    }

    m_LightAssignmentPassData.m_ClusterViewMatrixBuffer.makeNonResident();
    m_LightAssignmentPassData.m_ClusterViewMatrixBuffer.setData(viewMatrices, GL_DYNAMIC_DRAW);
    m_LightAssignmentPassData.m_ClusterISMBuffer.makeNonResident();
    m_LightAssignmentPassData.m_ClusterISMBuffer.setData(m_GeometryClustersData.m_ClusterISMContainer.getTextureCount(),
                                                         m_GeometryClustersData.m_ClusterISMContainer.getTextureHandleBufferPtr(),
                                                         GL_DYNAMIC_DRAW);
}

void GLClusteredISMRenderer2::processPixel(int pixelX, int pixelY) {
    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEX2D_GEOMETRYCLUSTERS);

    auto w = m_GBuffer.getWidth();
    auto h = m_GBuffer.getHeight();

    std::vector<uint32_t> img(m_GBuffer.getWidth() * m_GBuffer.getHeight());
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, img.data());

    std::vector<GLuint> clusterOffset(m_TileCount.x * m_TileCount.y);
    m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.makeNonResident();
    m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.getData(clusterOffset.size(), clusterOffset.data());
    m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.makeResident(GL_READ_WRITE);

    auto tile = glm::uvec2(pixelX, pixelY) / TILE_SIZE;
    auto localClusterIdx = img[pixelX + pixelY * w];
    auto globalClusterIdx = clusterOffset[tile.x + tile.y * m_TileCount.x] + localClusterIdx;

    m_nSelectedISMIndex = globalClusterIdx;

    getClusterCache(tile.x + tile.y * m_TileCount.x, localClusterIdx, m_SelectedCacheP, m_SelectedCacheN);
    m_nFrameIdx = 0;

    glActiveTexture(GL_TEXTURE0);
}

void GLClusteredISMRenderer2::sendVPLs(const VPLContainer& vpls, const Scene& scene) {
    uint32_t workGroupSize = 1024;
    uint32_t workItemCount;

    computeVPLISMs(vpls);

    computeClusterISMs();

    if(m_bUseVPLISMs || m_bNoShadow || !m_bUseCPUIrradianceCache)
    {
        GLTimer timer(m_LightAssignmentPassData.m_TimeElapsedQuery);

        std::vector<glm::vec4> vplBuffer;

        for(auto idx: range(vpls.orientedVPLs.size())) {
            auto vpl = vpls.orientedVPLs[idx];

            glm::vec4 lightPosition = glm::vec4(convert(vpl.P), 1);
            glm::vec3 lightNormal = convert(vpl.N);

            vplBuffer.emplace_back(lightPosition);
            vplBuffer.emplace_back(glm::vec4(lightNormal, 0));
            vplBuffer.emplace_back(glm::vec4(vpl.L.r, vpl.L.g, vpl.L.b, 0));
        }

        m_VPLData.m_VPLBuffer.setData(vplBuffer, GL_DYNAMIC_DRAW);
        m_VPLData.m_VPLBuffer.makeResident(GL_READ_ONLY);

        m_VPLData.m_VPLOffsetsCountsBuffer.setSize(sizeof(glm::ivec2) * m_GeometryClustersData.m_nGClusterCount, GL_STATIC_DRAW);
        m_VPLData.m_VPLOffsetsCountsBuffer.makeResident(GL_READ_WRITE);

        m_VPLData.m_VPLIndexBuffer.setSize(vpls.orientedVPLs.size() * m_GeometryClustersData.m_nGClusterCount * sizeof(GLuint), GL_STATIC_DRAW);
        m_VPLData.m_VPLIndexBuffer.makeResident(GL_READ_WRITE);

        m_LightAssignmentPassData.m_Program.use();

        // Set Uniforms
        m_LightAssignmentPassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
        m_LightAssignmentPassData.m_uClusterCount.set(GLuint(m_GeometryClustersData.m_nGClusterCount));
        m_LightAssignmentPassData.m_uVPLBuffer.set(m_VPLData.m_VPLBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uVPLOffsetCountsBuffer.set(m_VPLData.m_VPLOffsetsCountsBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uVPLIndexBuffer.set(m_VPLData.m_VPLIndexBuffer.getGPUAddress());

        m_LightAssignmentPassData.m_uClusterTilesOffsetsBuffer.set(m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterToTileBuffer.set(m_GeometryClustersData.m_GClusterToTileBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterBBoxLowerBuffer.set(m_GeometryClustersData.m_ClusterBBoxLowerBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterBBoxUpperBuffer.set(m_GeometryClustersData.m_ClusterBBoxUpperBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterNormalBuffer.set(m_GeometryClustersData.m_ClusterNormalBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterIrradianceBuffer.set(m_GeometryClustersData.m_ClusterIrradianceBuffer.getGPUAddress());
        m_LightAssignmentPassData.m_uClusterMeanIncidentDirectionBuffer.set(m_GeometryClustersData.m_ClusterMeanIncidentDirectionBuffer.getGPUAddress());

        m_LightAssignmentPassData.m_uDistClamp.set(m_fDistClamp);

        m_LightAssignmentPassData.m_uUseIrradianceCache.set(m_bUseIrradianceEstimate);

        if(!m_bNoShadow && !m_bUseVPLISMs) {
            // ISMs
            m_LightAssignmentPassData.m_uISMData.smCountPerDimension.set(m_GeometryClustersData.m_ClusterISMContainer.getShadowMapCountPerDimension());
            m_LightAssignmentPassData.m_uISMData.smCountPerTexture.set(m_GeometryClustersData.m_ClusterISMContainer.getShadowMapCountPerTexture());
            m_LightAssignmentPassData.m_uISMData.smResolution.set(m_GeometryClustersData.m_ClusterISMContainer.getShadowMapResolution());

            m_LightAssignmentPassData.m_ClusterISMBuffer.makeResident(GL_READ_ONLY);
            m_LightAssignmentPassData.m_ClusterViewMatrixBuffer.makeResident(GL_READ_ONLY);

            m_LightAssignmentPassData.m_uClusterISMBuffer.set(m_LightAssignmentPassData.m_ClusterISMBuffer.getGPUAddress());
            m_LightAssignmentPassData.m_uClusterViewMatrixBuffer.set(m_LightAssignmentPassData.m_ClusterViewMatrixBuffer.getGPUAddress());

            m_LightAssignmentPassData.m_uISMOffset.set(m_fISMOffset);

            m_LightAssignmentPassData.m_uNoShadow.set(false);
        } else {
            m_LightAssignmentPassData.m_uNoShadow.set(true);
        }

        // Launch compute shader
        workItemCount = (m_GeometryClustersData.m_nGClusterCount / workGroupSize) + (m_GeometryClustersData.m_nGClusterCount % workGroupSize != 0);
        glDispatchCompute(workItemCount, 1, 1);

        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
}

void GLClusteredISMRenderer2::displayGClusters(GLVisualDataRenderer& renderer, const Scene& scene) {
    /*if(m_nSelectedISMIndex >= 0) {
        renderer.setUp();

        renderer.renderArrow(
                    convert(m_SelectedCacheP),
                    convert(m_SelectedCacheN),
                    Col3f(1, 0, 0), 10.f);
        return;
    }*/


    /*
    size_t imageSize = m_GBuffer.getWidth() * m_GBuffer.getHeight();
    size_t tileCount = m_TileCount.x * m_TileCount.y;

    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<glm::vec4> normals(imageSize);
    std::vector<GLuint> clusterCount(tileCount);

    m_GeometryClustersData.m_GClusterCountsBuffer.makeNonResident();
    m_GeometryClustersData.m_GClusterCountsBuffer.getData(clusterCount.size(), clusterCount.data());
    m_GeometryClustersData.m_GClusterCountsBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.makeNonResident();
    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.makeNonResident();
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterNormalBuffer.makeNonResident();
    m_GeometryClustersData.m_ClusterNormalBuffer.getData(normals.size(), normals.data());
    m_GeometryClustersData.m_ClusterNormalBuffer.makeResident(GL_READ_WRITE);

    renderer.setUp();

    for(auto pair: index(clusterCount)) {
        GLuint offset = 1024 * pair.first;
        for(auto i: range(pair.second)) {
            glm::vec3 upper = glm::vec3(bboxUpper[offset + i]), lower = glm::vec3(bboxLower[offset + i]);
            float radius = glm::length(upper - lower) * 0.5f;
            glm::vec3 normal = glm::vec3(normals[offset + i]);
            Vec3f center = 0.5f * convert(upper + lower);
            Col3f color(convert(glm::abs(normal)));
            if(m_bShowSpheres) {
                renderer.renderSphere(center, radius, color);
            }
            renderer.renderArrow(center, convert(normal), color, radius);
        }
    }*/


    renderer.setUp();
    renderer.renderPoints(m_SampledScene);
}

void GLClusteredISMRenderer2::shadingPass(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls, const Scene& scene) {

    m_VPLContainer = vpls;

    sendVPLs(vpls, scene);
    splatIrradianceCache();

    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::CapabilityRestorer blend(GL_BLEND, true);

    gloops::StateRestorer<GL_VIEWPORT> viewport;

    glViewport(x, y, width, height);

    glClear(GL_COLOR_BUFFER_BIT);

    glBlendFunc(GL_ONE, GL_ONE);
    glBlendEquation(GL_FUNC_ADD);

    {
        GLTimer timer(m_VPLShadingPassData.m_TimeElapsedQuery);
        if(m_bDoVPLShadingPass) {
            if(!m_bUseIrradianceEstimate) {
                m_VPLShadingPassData.m_Program.use();

                // Send matrices
                m_VPLShadingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
                m_VPLShadingPassData.m_uInvProjMatrix.set(rcpProjMatrix);
                m_VPLShadingPassData.m_uTileCount.set(m_TileCount);
                m_VPLShadingPassData.m_uVPLCount.set(GLuint(vpls.orientedVPLs.size()));
                m_VPLShadingPassData.m_uDisplayCostPerPixel.set(m_bDisplayCostPerPixel);
                m_VPLShadingPassData.m_uVPLBuffer.set(m_VPLData.m_VPLBuffer.getGPUAddress());
                m_VPLShadingPassData.m_uVPLIndexBuffer.set(m_VPLData.m_VPLIndexBuffer.getGPUAddress());
                m_VPLShadingPassData.m_uClusterTilesOffsetsBuffer.set(m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.getGPUAddress());
                m_VPLShadingPassData.m_uVPLOffsetCountsBuffer.set(m_VPLData.m_VPLOffsetsCountsBuffer.getGPUAddress());
                m_VPLShadingPassData.m_uRcpViewMatrix.set(rcpViewMatrix);
                m_VPLShadingPassData.m_uRenderIrradiance.set(m_bRenderIrradiance);
                m_VPLShadingPassData.m_uDistClamp.set(m_fDistClamp);
                m_VPLShadingPassData.m_uClusterCountsBuffer.set(m_GeometryClustersData.m_GClusterCountsBuffer.getGPUAddress());
                m_VPLShadingPassData.m_uClusterIrradianceBuffer.set(m_GeometryClustersData.m_ClusterIrradianceBuffer.getGPUAddress());
                m_VPLShadingPassData.m_uClusterBBoxLowerBuffer.set(m_GeometryClustersData.m_ClusterBBoxLowerBuffer.getGPUAddress());
                m_VPLShadingPassData.m_uClusterBBoxUpperBuffer.set(m_GeometryClustersData.m_ClusterBBoxUpperBuffer.getGPUAddress());
                m_VPLShadingPassData.m_uClusterNormalBuffer.set(m_GeometryClustersData.m_ClusterNormalBuffer.getGPUAddress());

                m_VPLShadingPassData.m_uTileWindow.set(m_nTileWindow);
                m_VPLShadingPassData.m_uMaxDist.set(m_fMaxDist);
                m_VPLShadingPassData.m_uUseIrradianceEstimate.set(false);

                if(!m_bNoShadow && m_bUseVPLISMs) {
                    m_VPLShadingPassData.m_uISMData.smCountPerDimension.set(m_VPLData.m_VPLISMContainer.getShadowMapCountPerDimension());
                    m_VPLShadingPassData.m_uISMData.smCountPerTexture.set(m_VPLData.m_VPLISMContainer.getShadowMapCountPerTexture());
                    m_VPLShadingPassData.m_uISMData.smResolution.set(m_VPLData.m_VPLISMContainer.getShadowMapResolution());

                    m_VPLShadingPassData.m_VPLISMBuffer.makeResident(GL_READ_ONLY);
                    m_VPLShadingPassData.m_VPLViewMatrixBuffer.makeResident(GL_READ_ONLY);

                    m_VPLShadingPassData.m_uVPLISMBuffer.set(m_VPLShadingPassData.m_VPLISMBuffer.getGPUAddress());
                    m_VPLShadingPassData.m_uVPLViewMatrixBuffer.set(m_VPLShadingPassData.m_VPLViewMatrixBuffer.getGPUAddress());

                    m_VPLShadingPassData.m_uISMOffset.set(m_fISMOffset);

                    m_VPLShadingPassData.m_uNoShadow.set(false);
                } else {
                    m_VPLShadingPassData.m_uNoShadow.set(true);
                }
            } else if(!m_bNoShadow) {
                m_ICDrawPassData.m_Program.use();

                m_ICDrawPassData.m_uRcpProjMatrix.set(rcpProjMatrix);
                m_ICDrawPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
                m_ICDrawPassData.m_uIrradianceSampler.set(m_ICSplatPassData.m_IrradianceEstimationTextureHandle);
                m_ICDrawPassData.m_uRenderIrradiance.set(m_bRenderIrradiance);
            }

            m_ScreenTriangle.render();
        }
    }

    {
        GLTimer timer(m_DirectLightingPassData.m_TimeElapsedQuery);

        if(m_bDoDirectLightPass && m_DirectLightingPassData.m_DoDirectLightingPass) {
            m_DirectLightingPassData.m_Program.use();

            m_DirectLightingPassData.m_uViewport.set(float(x), float(y), float(width), float(height));
            m_DirectLightingPassData.m_uInvProjMatrix.set(rcpProjMatrix);

            m_DirectLightingPassData.m_uLightPosition.set(m_DirectLightingPassData.m_LightPosition);
            m_DirectLightingPassData.m_uLightIntensity.set(m_DirectLightingPassData.m_LightIntensity);
            m_DirectLightingPassData.m_uLightViewMatrix.set(m_DirectLightingPassData.m_LightViewMatrix);
            m_DirectLightingPassData.m_uShadowMapSampler.set(TEXUNIT_TEXCUBEMAP_SHADOWMAP);
            m_DirectLightingPassData.m_uFaceProjectionMatrices.setMatrix4(
                        6, GL_FALSE, glm::value_ptr(m_DirectLightingPassData.m_ShadowMapRenderer.getFaceProjectionMatrices()[0]));
            m_DirectLightingPassData.m_uRcpViewMatrix.set(rcpViewMatrix);
            m_DirectLightingPassData.m_uDistClamp.set(m_fDistClamp);
            m_DirectLightingPassData.m_uShadowMapOffset.set(m_fShadowMapOffset);
            m_DirectLightingPassData.m_uRenderIrradiance.set(m_bRenderIrradiance);

            m_ScreenTriangle.render();
        }
    }
}

void GLClusteredISMRenderer2::getClusterCache(uint32_t tileIdx, uint32_t clusterLocalIdx, glm::vec3& P, glm::vec3& N) {
    auto width = m_GBuffer.getWidth();
    auto height = m_GBuffer.getHeight();
    auto imageSize = width * height;

    // Compute input buffers
    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<glm::vec4> normals(imageSize);

    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.makeNonResident();
    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.makeNonResident();
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterNormalBuffer.makeNonResident();
    m_GeometryClustersData.m_ClusterNormalBuffer.getData(normals.size(), normals.data());
    m_GeometryClustersData.m_ClusterNormalBuffer.makeResident(GL_READ_WRITE);


    uint32_t offset = 1024 * tileIdx;
    uint32_t clusterGlobalIdx = offset + clusterLocalIdx;

    glm::vec3 upper = glm::vec3(bboxUpper[clusterGlobalIdx]), lower = glm::vec3(bboxLower[clusterGlobalIdx]);
    N = glm::vec3(normals[clusterGlobalIdx]);
    P = 0.5f * (upper + lower);
}

void GLClusteredISMRenderer2::computeCPUIrradianceCaches() {
    Progress progress;
    progress.init("Compute CPU Irradiance Caches", 1);

    if(m_bNoShadow || !m_bUseIrradianceEstimate) {
        return;
    }

    auto width = m_GBuffer.getWidth();
    auto height = m_GBuffer.getHeight();
    auto imageSize = width * height;
    auto tileCount = m_TileCount.x * m_TileCount.y;

    // Compute input buffers
    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<glm::vec4> normals(imageSize);
    std::vector<GLuint> clusterCount(tileCount);

    m_GeometryClustersData.m_GClusterCountsBuffer.getData(clusterCount.size(), clusterCount.data());
    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());
    m_GeometryClustersData.m_ClusterNormalBuffer.getData(normals.size(), normals.data());

    std::vector<glm::vec4> irradiances(m_GeometryClustersData.m_nGClusterCount);

    uint32_t count = m_GeometryClustersData.m_nGClusterCount;
    std::vector<ICSplatPassData::ICVertex> vertexBuffer;

    uint32_t clusterGlobalIdx = 0;
    for(auto pair: index(clusterCount)) {
        GLuint offset = 1024 * pair.first;
        for(auto i: range(pair.second)) {
            glm::vec3 upper = glm::vec3(bboxUpper[offset + i]), lower = glm::vec3(bboxLower[offset + i]);
            glm::vec3 normal = glm::vec3(normals[offset + i]);
            glm::vec3 center = 0.5f * (upper + lower);

            glm::vec3 eye = center + m_fISMNormalOffset * normal;

            Col3f irradiance(0.f);

            RayCounter rc;
            for(const auto& vpl: m_VPLContainer.orientedVPLs) {
                Vec3f wi = vpl.P - convert(eye);
                float dist = length(wi);
                wi /= dist;

                if(!occluded(shadow_ray(SurfacePoint(convert(eye), convert(normal)), wi, dist), *m_pScene, rc)) {
                    float cos_o = embree::max(0.f, dot(-wi, vpl.N));
                    float cos_i = embree::max(0.f, dot(wi, convert(normal)));

                    dist = embree::max(dist, m_fDistClamp);

                    float G = cos_i * cos_o / (dist * dist);
                    irradiance += G * vpl.L;
                }
            }

            irradiances[clusterGlobalIdx] = glm::vec4(irradiance.r, irradiance.g, irradiance.b, 0.f);
            ++clusterGlobalIdx;
        }
    }

    m_GeometryClustersData.m_ClusterIrradianceBuffer.setData(irradiances, GL_DYNAMIC_DRAW);

    progress.end();
}

void GLClusteredISMRenderer2::splatIrradianceCache() {
    GLTimer timer(m_ICSplatPassData.m_TimeElapsedQuery);

    if(m_bNoShadow || !m_bUseIrradianceEstimate) {
        return;
    }

    auto width = m_GBuffer.getWidth();
    auto height = m_GBuffer.getHeight();
    auto imageSize = width * height;
    auto tileCount = m_TileCount.x * m_TileCount.y;

    // Compute input buffers
    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<glm::vec4> normals(imageSize);
    std::vector<GLuint> clusterCount(tileCount);

    m_GeometryClustersData.m_GClusterCountsBuffer.makeNonResident();
    m_GeometryClustersData.m_GClusterCountsBuffer.getData(clusterCount.size(), clusterCount.data());
    m_GeometryClustersData.m_GClusterCountsBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.makeNonResident();
    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.makeNonResident();
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.makeResident(GL_READ_WRITE);

    m_GeometryClustersData.m_ClusterNormalBuffer.makeNonResident();
    m_GeometryClustersData.m_ClusterNormalBuffer.getData(normals.size(), normals.data());
    m_GeometryClustersData.m_ClusterNormalBuffer.makeResident(GL_READ_WRITE);

    std::vector<glm::vec4> irradiances(m_GeometryClustersData.m_nGClusterCount);
    m_GeometryClustersData.m_ClusterIrradianceBuffer.makeNonResident();
    m_GeometryClustersData.m_ClusterIrradianceBuffer.getData(irradiances.size(), irradiances.data());
    m_GeometryClustersData.m_ClusterIrradianceBuffer.makeResident(GL_READ_WRITE);

    uint32_t count = m_GeometryClustersData.m_nGClusterCount;
    std::vector<ICSplatPassData::ICVertex> vertexBuffer;

    uint32_t clusterGlobalIdx = 0;
    for(auto pair: index(clusterCount)) {
        GLuint offset = 1024 * pair.first;
        for(auto i: range(pair.second)) {
            glm::vec3 upper = glm::vec3(bboxUpper[offset + i]), lower = glm::vec3(bboxLower[offset + i]);
            float radius = m_fSplatRadiusFactor * glm::length(upper - lower) * 0.5f;
            glm::vec3 normal = glm::vec3(normals[offset + i]);
            glm::vec3 center = 0.5f * (upper + lower);

            ICSplatPassData::ICVertex v{ glm::vec4(center, radius), normal, glm::vec3(irradiances[clusterGlobalIdx]) };

            vertexBuffer.emplace_back(v);
            ++clusterGlobalIdx;
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, m_ICSplatPassData.m_ICVBO.glId());
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBuffer[0]) * vertexBuffer.size(), vertexBuffer.data(), GL_DYNAMIC_DRAW);

    glm::mat4 MVPMatrix = m_ProjectionMatrix * m_ViewMatrix;
    glm::mat4 rcpProjMatrix = glm::inverse(m_ProjectionMatrix);
    glm::mat4 rcpViewMatrix = glm::inverse(m_ViewMatrix);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    gloops::CapabilityRestorer blend(GL_BLEND, true);

    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLint currentFBO;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentFBO);

    m_ICSplatPassData.m_IrradianceEstimationBuffer.bindForDrawing();

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT);

    glBlendFunc(GL_ONE, GL_ONE);
    glBlendEquation(GL_FUNC_ADD);

    m_ICSplatPassData.m_Program.use();

    m_ICSplatPassData.m_uProjMatrix.set(m_ProjectionMatrix);
    m_ICSplatPassData.m_uViewMatrix.set(m_ViewMatrix);
    m_ICSplatPassData.m_uViewport.set(0.f, 0.f, float(width), float(height));
    m_ICSplatPassData.m_uRcpProjMatrix.set(rcpProjMatrix);
    m_ICSplatPassData.m_uRcpViewMatrix.set(rcpViewMatrix);

    glBindVertexArray(m_ICSplatPassData.m_ICVAO.glId());
    glDrawArrays(GL_POINTS, 0, count);

    glBindFramebuffer(GL_FRAMEBUFFER, currentFBO);
}

void GLClusteredISMRenderer2::drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uTileCount.set(m_TileCount);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(dataToDisplay);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);

    //m_DrawBuffersPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
    //m_DrawBuffersPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());

    m_DrawBuffersPassData.m_uClusterTilesOffsetsBuffer.set(m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterToTileBuffer.set(m_GeometryClustersData.m_GClusterToTileBuffer.getGPUAddress());

    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
    m_GeometryClustersData.m_ClusterNormalBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);

    m_ScreenTriangle.render();
}

void GLClusteredISMRenderer2::drawBuffers(int x, int y, size_t width, size_t height) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLTimer timer(m_DrawBuffersPassData.m_TimeElapsedQuery);

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uTileCount.set(m_TileCount);

    //m_DrawBuffersPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
    //m_DrawBuffersPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());

    m_DrawBuffersPassData.m_uClusterTilesOffsetsBuffer.set(m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterToTileBuffer.set(m_GeometryClustersData.m_GClusterToTileBuffer.getGPUAddress());

    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
    m_GeometryClustersData.m_ClusterNormalBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    // Draw normals
    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);
    m_DrawBuffersPassData.m_uDataToDisplay.set(0);

    m_ScreenTriangle.render();

    // Draw depth
    glViewport(x + width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(1);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw Cluster node
    glViewport(x + 2 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(2);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster buffer
    glViewport(x + 4 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(4);
    m_DrawBuffersPassData.m_uViewport.set(fX + 4 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow lower point depth buffer
    glViewport(x, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(5);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow upper point depth buffer
    glViewport(x + width, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(6);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    glViewport(x + 2 * width, y - height, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(7);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    glViewport(x + 3 * width, y - height, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(8);
    m_DrawBuffersPassData.m_uViewport.set(fX + 3 * fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();
}

void GLClusteredISMRenderer2::drawISMs(int x, int y, size_t width, size_t height) {
    GLISMContainer* pContainer = m_bUseVPLISMs ? &m_VPLData.m_VPLISMContainer : &m_GeometryClustersData.m_ClusterISMContainer;

    if(pContainer->getTextureCount() != 0) {
        glViewport(x, y, width, height);

        if(m_nSelectedISMIndex < 0) {
            m_ISMDrawer.drawISMTexture(*pContainer, m_nSelectedISMTextureIndex, m_nSelectedISMLevel);
        } else {
            m_ISMDrawer.drawISM(*pContainer, m_nSelectedISMIndex, m_nSelectedISMLevel);

            if(m_nFrameIdx == 0) {
                m_Framebuffer.clear();
            }

            StratifiedSampler sampler(1, 1);
            sampler.init(m_nFrameIdx, 0, 0, pContainer->getShadowMapResolution(), pContainer->getShadowMapResolution());
            IntegratorSample sample;

            glm::vec3 eye = m_SelectedCacheP + m_SelectedCacheN;
            glm::vec3 point = eye + m_SelectedCacheN;
            glm::vec3 up;
            if(m_SelectedCacheN.x != 0) {
                up = glm::vec3(-(m_SelectedCacheN.y + m_SelectedCacheN.z) / m_SelectedCacheN.x, 1, 1);
            } else if(m_SelectedCacheN.y != 0) {
                up = glm::vec3(1, -(m_SelectedCacheN.x + m_SelectedCacheN.z) / m_SelectedCacheN.y, 1);
            } else {
                up = glm::vec3(1, 1, -(m_SelectedCacheN.x + m_SelectedCacheN.y) / m_SelectedCacheN.z);
            }

            glm::mat4 viewMatrix = glm::lookAt(eye, point, up);

            ParaboloidCamera camera(computeEmbreeLocalToWorld(viewMatrix),
                                            pContainer->getShadowMapResolution(), pContainer->getShadowMapResolution());



            while(sampler.getNextSample(sample)) {
                Ray ray = camera.ray(sample);
                Intersection I = intersect(ray, *m_pScene);
                if(I) {
                    float d = embree::distance(I.P, convert(m_SelectedCacheP));
                    m_Framebuffer.accumulate(embree::Vec2i(sample.getPixel()),
                                           Col3f((d - getZNear()) / (getZFar() - getZNear())));
                } else {
                    m_Framebuffer.accumulate(embree::Vec2i(sample.getPixel()), Col3f(1));
                }
            }

            m_ResultRenderer.drawResult(gloops::Viewport(x + width, y, width, height), 1.f, normalize(m_Framebuffer));
            ++m_nFrameIdx;
        }
    } else {
        std::cerr << "No ISM in the container." << std::endl;
    }
}

void GLClusteredISMRenderer2::computeTimings() {
    float scale = 1.f / 1000000;

    m_Timings.geometryPass = m_GeometryPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.findUniqueClustersPass = m_FindUniqueGeometryClusterPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.countUniqueClustersPass = m_CountGeometryClusterPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.lightAssignementPass = m_LightAssignmentPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.vplShadingPass = m_VPLShadingPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.directLightShadingPass = m_DirectLightingPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.drawBuffersPass = m_DrawBuffersPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.computeVPLISMPass = m_ComputeVPLISMsTimeElapsedQuery.waitResult() * scale;
    m_Timings.computeClusterISMPass = m_ComputeClusterISMsTimeElapsedQuery.waitResult() * scale;
    m_Timings.splatICPass = m_ICSplatPassData.m_TimeElapsedQuery.waitResult() * scale;

    m_Timings.updateTotalTime();
}

void GLClusteredISMRenderer2::setPrimaryLightSource(const Vec3f& P, const Col3f& L, const GLScene& glScene) {
    m_DirectLightingPassData.m_DoDirectLightingPass = true;
    m_DirectLightingPassData.m_LightPosition = convert(P);
    m_DirectLightingPassData.m_LightIntensity = convert(L);
    m_DirectLightingPassData.m_LightViewMatrix =
            glm::translate(glm::mat4(1), -m_DirectLightingPassData.m_LightPosition);

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEXCUBEMAP_SHADOWMAP);

    m_DirectLightingPassData.m_ShadowMapContainer.init(2048, 1);
    m_DirectLightingPassData.m_ShadowMapRenderer.render(
                glScene, m_DirectLightingPassData.m_ShadowMapContainer, &m_DirectLightingPassData.m_LightViewMatrix, 1);

    m_DirectLightingPassData.m_ShadowMapContainer.bindForShadowTest(0);
}

void GLClusteredISMRenderer2::drawPrimaryLightShadowMap(int x, int y, size_t width, size_t height) {
    glViewport(x, y, width, height);
    m_DirectLightingPassData.m_ShadowMapDrawer.drawShadowMap(m_DirectLightingPassData.m_ShadowMapContainer, 0, TEXUNIT_TEXCUBEMAP_SHADOWMAP);

    glActiveTexture(GL_TEXTURE0 + TEXUNIT_TEXCUBEMAP_SHADOWMAP);
    m_DirectLightingPassData.m_ShadowMapContainer.bindForShadowTest(0);
}

void GLClusteredISMRenderer2::exposeIO(TwBar* bar) {
    TwAddSeparator(bar, "GLClusteredISMRenderer2::Separator0", "");

    atb::addVarRW(bar, "Do VPL Shading Pass", m_bDoVPLShadingPass, "");
    atb::addVarRW(bar, "Do Direct Light Pass", m_bDoDirectLightPass, "");
    atb::addVarRW(bar, "No Shadow", m_bNoShadow, "");
    atb::addVarRW(bar, "Use VPL ISMs", m_bUseVPLISMs, "");
    atb::addVarRW(bar, "Render irradiance", m_bRenderIrradiance, "");
    atb::addVarRW(bar, "Display cost per pixel", m_bDisplayCostPerPixel, "");
    atb::addVarRW(bar, "Gamma", m_fGamma, "");
    atb::addVarRW(bar, "Shadow Map Offset", m_fShadowMapOffset, "");
    atb::addVarRW(bar, "Distance clamp", m_fDistClamp, "");
    atb::addVarRW(bar, "Use normal clustering", m_bUseNormalClustering, "");
    atb::addVarRW(bar, "Show GSpheres", m_bShowSpheres, "");
    atb::addVarRO(bar, "Geometry cluster count", m_GeometryClustersData.m_nGClusterCount, "");
    atb::addVarRW(bar, "Do Pull", m_bDoPull, "");
    atb::addVarRW(bar, "Do Pull Push", m_bDoPullPush, "");
    atb::addVarRW(bar, "Pull treshold", m_fPullTreshold, "");
    atb::addVarRW(bar, "Push treshold", m_fPushTreshold, "");
    atb::addVarRW(bar, "Push Push nb levels", m_nPullPushNbLevels, "");
    atb::addVarRW(bar, "ISM Point Max Size", m_fISMPointMaxSize, "");
    atb::addVarRW(bar, "ISM offset", m_fISMOffset, "");
    atb::addVarRW(bar, "ISM normal offset", m_fISMNormalOffset, "");
    atb::addVarRW(bar, "ISM view index", m_nSelectedISMIndex, "");
    atb::addVarRW(bar, "ISM texture index", m_nSelectedISMTextureIndex, "");
    atb::addVarRW(bar, "ISM texture level", m_nSelectedISMLevel, "");
    atb::addVarRW(bar, "Scene Sample Count", m_nSceneSampleCount, "");
    atb::addVarRW(bar, "Scene Sampling Mode", m_nSceneSamplingMode, "");
    atb::addVarRW(bar, "Tile Window", m_nTileWindow, "");
    atb::addVarRW(bar, "Max distance lookup", m_fMaxDist, "");
    atb::addVarRW(bar, "SplatRadiusFactor", m_fSplatRadiusFactor, "");
    atb::addVarRW(bar, "Use irradiance caching", m_bUseIrradianceEstimate, "");
    atb::addVarRW(bar, "Use coherent irradiance cache", m_bUseCoherentIrradianceCache, "");
    atb::addVarRW(bar, "Use CPU Irradiance Caches", m_bUseCPUIrradianceCache, "");
    atb::addButton(bar, "Compute CPU Irradiance Caches",
                   [this]() { computeCPUIrradianceCaches(); m_bUseCPUIrradianceCache = true; }, "");

    TwAddSeparator(bar, "GLClusteredISMRenderer2::Separator1", "");

    atb::addVarRO(bar, "Geometry Pass", m_Timings.geometryPass, "");
    atb::addVarRO(bar, "Find Unique Clusters Pass", m_Timings.findUniqueClustersPass, "");
    atb::addVarRO(bar, "Count Unique Clusters Pass", m_Timings.countUniqueClustersPass, "");
    atb::addVarRO(bar, "Light Assignement Pass", m_Timings.lightAssignementPass, "");
    atb::addVarRO(bar, "VPL Shading Pass", m_Timings.vplShadingPass, "");
    atb::addVarRO(bar, "Direct Light Shading Pass", m_Timings.directLightShadingPass, "");
    atb::addVarRO(bar, "Draw Buffers Pass", m_Timings.drawBuffersPass, "");
    atb::addVarRO(bar, "Compute VPL ISMs Pass", m_Timings.computeVPLISMPass, "");
    atb::addVarRO(bar, "Compute Cluster ISMs Pass", m_Timings.computeClusterISMPass, "");
    atb::addVarRO(bar, "Splat IC Pass", m_Timings.splatICPass, "");
    atb::addVarRO(bar, "Total time", m_Timings.totalTime, "");

    TwAddSeparator(bar, "GLClusteredISMRenderer2::Separator2", "");
}

}


