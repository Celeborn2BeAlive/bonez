#pragma once

#include "bonez/common/common.hpp"
#include "bonez/opengl/common.hpp"
#include "bonez/opengl/scene/GLPoints.hpp"

#include "bonez/common/data/KdTree.hpp"

#include <stack>

namespace BnZ {

class BSphere3f {
    template<typename GetPositionFunctor>
    static BSphere3f recurseBuild(uint32_t* indices, unsigned int count, GetPositionFunctor getPosition,
                                  unsigned int b = 0) {
        BSphere3f MB;

        switch(b)
        {
        case 1:
            MB = BSphere3f(getPosition(indices[-1]));
            break;
        case 2:
            MB = BSphere3f(
                    getPosition(indices[-1]),
                    getPosition(indices[-2])
                    );
            break;
        case 3:
            MB = BSphere3f(
                        getPosition(indices[-1]),
                        getPosition(indices[-2]),
                        getPosition(indices[-3])
                    );
            break;
        case 4:
            return BSphere3f(
                        getPosition(indices[-1]),
                        getPosition(indices[-2]),
                        getPosition(indices[-3]),
                        getPosition(indices[-4])
                    );
        }

        for(auto i = 0u; i < count; i++) {
            if(!MB.contains(getPosition(indices[i])))   // Signed square distance to sphere
            {
                for(auto j = i; j > 0u; j--)
                {
                    std::swap(indices[j], indices[j - 1]);
                }

                MB = recurseBuild(indices + 1, i, getPosition, b + 1);
            }
        }

        return MB;
    }

public:
    glm::vec3 center;
    float sqrRadius;

    static float det(float m11, float m12, float m13,
                     float m21, float m22, float m23,
                     float m31, float m32, float m33) {
        return m11 * (m22 * m33 - m32 * m23) -
               m21 * (m12 * m33 - m32 * m13) +
               m31 * (m12 * m23 - m22 * m13);
    }

    BSphere3f(): sqrRadius(-1.f) {
    }

    BSphere3f(glm::vec3 center, float sqrRadius):
        center(center), sqrRadius(sqrRadius) {
    }

    BSphere3f(glm::vec3 point): BSphere3f(point, 0.f) {
    }

    BSphere3f(glm::vec3 O, glm::vec3 A) {
        auto a = A - O;
        auto o = 0.5f * a;

        sqrRadius = glm::dot(o, o);
        center = O + o;
    }

    BSphere3f(glm::vec3 O, glm::vec3 A, glm::vec3 B) {
        auto a = A - O;
        auto b = B - O;

        auto ab = glm::cross(a, b);
        auto denominator = 2.0f * glm::dot(ab, ab);

        auto o = (glm::dot(b, b) * glm::cross(ab, a) +
                    glm::dot(a, a) * glm::cross(b, ab)) / denominator;

        sqrRadius = glm::dot(o, o);
        center = O + o;
    }

    BSphere3f(glm::vec3 O, glm::vec3 A, glm::vec3 B, glm::vec3 C) {
        auto a = A - O;
        auto b = B - O;
        auto c = C - O;

        auto denominator = 2.0f * det(a.x, a.y, a.z,
                                       b.x, b.y, b.z,
                                       c.x, c.y, c.z);

        auto o = (glm::dot(c, c) * glm::cross(a, b) +
                    glm::dot(b, b) * glm::cross(c, a) +
                    glm::dot(a, a) * glm::cross(b, c)) / denominator;

        sqrRadius = glm::dot(o, o);
        center = O + o;
    }

    template<typename GetPositionFunctor>
    BSphere3f(uint32_t count, GetPositionFunctor getPosition) {
        std::vector<uint32_t> indices;
        indices.reserve(count);
        for(auto i = 0u; i < count; ++i) {
            indices.emplace_back(i);
        }

        auto ptr = indices.data();
        *this = recurseBuild(ptr, count, getPosition);
    }

    BSphere3f(const BBox3f& bbox) {
        center = convert(embree::center(bbox));
        auto v = center - convert(bbox.upper);
        sqrRadius = glm::dot(v, v);
    }

    bool contains(const glm::vec3& P) const {
        auto v = P - center;
        float d2 = glm::dot(v, v);
        return d2 < sqrRadius;
    }

    float distanceSquared(const glm::vec3& P) const {
        auto v = P - center;
        float d2 = glm::dot(v, v);
        if(d2 < sqrRadius) {
            return 0.f;
        }
        return d2 - 2 * glm::sqrt(d2 * sqrRadius) + sqrRadius;
    }

    float distance(const glm::vec3& P) const {
        auto v = P - center;
        float d2 = glm::dot(v, v);
        if(d2 < sqrRadius) {
            return 0.f;
        }
        return glm::sqrt(d2) - glm::sqrt(sqrRadius);
    }
};

class GLManyLODBSH {
public:
    struct Node {
        BSphere3f sphere;
        glm::vec3 normal;
        float normalConeDot;
        uint32_t rightChild;

        Node(BSphere3f s, glm::vec3 n, float nCD, uint32_t rC):
            sphere(s), normal(n), normalConeDot(nCD), rightChild(rC) {
        }

        bool isLeaf() const {
            return rightChild == 0;
        }
    };

private:
    std::vector<Node> m_Nodes;

    typedef std::vector<const GLPoint*> GLPointPtrVector;

    uint32_t addLeaf(const GLPoint* point) {
        m_Nodes.emplace_back(BSphere3f(point->position, point->sqrRadius), point->normal, 1.f, 0);
        return m_Nodes.size() - 1;
    }

    uint32_t recursiveBuild(GLPoint const** P, uint32_t start, uint32_t end) {
        if(end - start == 1) {
            return addLeaf(P[start]);
        }

        // Compute the bounding box of the data
        BBox3f bound(convert(P[start]->position));
        for(auto i = start + 1; i != end; ++i) {
            bound += convert(P[i]->position);
        }
        // The split axis is the one with maximal extent for the data
        uint32_t splitAxis = maxDim(abs(embree::size(bound)));
        uint32_t splitIndex = (start + end) / 2;
        // Reorganize the pointers such that the middle element is the middle element on the split axis
        std::nth_element(P + start, P + splitIndex, P + end,
                         [splitAxis](const GLPoint* lhs, const GLPoint* rhs) -> bool {
                            auto v1 = lhs->position[splitAxis];
                            auto v2 = rhs->position[splitAxis];
                            return v1 == v2 ? lhs < rhs : v1 < v2;
                         });

        uint32_t idx = m_Nodes.size();
        //BSphere3f sphere(bound);
        BSphere3f sphere(end - start, [&](uint32_t idx) -> glm::vec3 { return P[start + idx]->position; });
        m_Nodes.emplace_back(sphere, glm::vec3(0), 1.f, 0);

        recursiveBuild(P, start, splitIndex);

        // does'nt work with m_Nodes[idx].rightChild = recursiveBuild(P, splitIndex, end);
        // why ??? (some rightChild become equal to 0)
        auto rC = recursiveBuild(P, splitIndex, end);
        m_Nodes[idx].rightChild = rC;

        /*
        if(m_Nodes[idx + 1].isLeaf()) {
            m_Nodes[idx + 1].sphere.sqrRadius = m_Nodes[idx].sphere.sqrRadius;
        }

        if(m_Nodes[rC].isLeaf()) {
            m_Nodes[rC].sphere.sqrRadius = m_Nodes[idx].sphere.sqrRadius;
        }*/

        return idx;
    }

    uint32_t m_nSize = 0;

public:
    GLManyLODBSH() = default;

    bool empty() const {
        return m_Nodes.empty();
    }

    uint32_t size() const {
        return m_nSize;
    }

    uint32_t nodeCount() const {
        return m_Nodes.size();
    }

    template<typename CallbackFunctor>
    void depthFirstTraversal(CallbackFunctor callback) const {
        if(empty()) {
            return;
        }

        std::stack<std::pair<uint32_t, uint32_t>> stack;

        stack.push(std::make_pair(0, 0));
        while(!stack.empty()) {
            auto node = stack.top();
            auto idx = node.first;
            auto level = node.second;
            stack.pop();

            auto dontStop = callback(idx, m_Nodes[idx], level);

            if(dontStop && m_Nodes[idx].rightChild != 0) {
                stack.push(std::make_pair(idx + 1, level + 1)); // left child
                stack.push(std::make_pair(m_Nodes[idx].rightChild, level + 1));
            }
        }
    }

    void build(const GLPoint* begin, const GLPoint* end) {
        m_Nodes.clear();

        if(begin == end) {
            return;
        }

        m_nSize = end - begin;

        GLPointPtrVector ptrs;
        ptrs.reserve(m_nSize);
        for(auto ptr = begin; ptr != end; ++ptr) {
            ptrs.emplace_back(ptr);
        }

        m_Nodes.reserve(2 * m_nSize - 1);
        recursiveBuild(ptrs.data(), 0, m_nSize);
    }

    const std::vector<Node> getNodes() const {
        return m_Nodes;
    }
};

}
