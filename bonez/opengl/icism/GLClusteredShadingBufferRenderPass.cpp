#include "GLClusteredShadingBufferRenderPass.hpp"

namespace BnZ {

void GLClusteredShadingBuffer::init(uint32_t width, uint32_t height) {
    // Number of tiles in each dimension
    uint32_t Sx = (width / TILE_SIZE) + (width % TILE_SIZE != 0);
    uint32_t Sy = (height / TILE_SIZE) + (height % TILE_SIZE != 0);

    m_TileCount = glm::uvec2(Sx, Sy);

    // Create a new texture 2D because the previous one was resident
    m_GeometryClustersTexture = GLTexture2D();

    m_GeometryClustersTexture.setImage(0, GL_R32UI,
                                      width, height, 0,
                                      GL_RED_INTEGER, GL_UNSIGNED_INT,
                                      nullptr);
    m_GeometryClustersTexture.setMinFilter(GL_NEAREST);
    m_GeometryClustersTexture.setMagFilter(GL_NEAREST);

    m_GeometryClustersTexture.makeTextureHandleResident();
    m_GeometryClustersTexture.makeImageHandleResident(0, GL_FALSE, 0, GL_R32UI, GL_READ_WRITE);

    GLuint size = Sx * Sy;

    m_GClusterCountsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterCountsBuffer.makeResident(GL_READ_WRITE);

    m_GClusterTilesOffsetsBuffer.setSize(size * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterTilesOffsetsBuffer.makeResident(GL_READ_WRITE);

    m_GClusterTotalCountBuffer.setSize(sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterTotalCountBuffer.makeResident(GL_READ_WRITE);

    size_t imageSize = width * height;

    m_GClusterToTileBuffer.setSize(imageSize * sizeof(GLuint), GL_STATIC_DRAW);
    m_GClusterToTileBuffer.makeResident(GL_READ_WRITE);

    m_ClusterBBoxLowerBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterBBoxLowerBuffer.makeResident(GL_READ_WRITE);

    m_ClusterBBoxUpperBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterBBoxUpperBuffer.makeResident(GL_READ_WRITE);

    m_ClusterNormalBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterNormalBuffer.makeResident(GL_READ_WRITE);

    m_ClusterRepPositionBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterRepPositionBuffer.makeResident(GL_READ_WRITE);

    m_ClusterRepNormalBuffer.setSize(imageSize * sizeof(glm::vec4), GL_STATIC_DRAW);
    m_ClusterRepNormalBuffer.makeResident(GL_READ_WRITE);
}

// This shader sort the clusters of each tile (32x32 pixels) to compute the number of clusters
// in each tile, compute a new local index for each and link back each pixel to this index.
// At the end, we have 0 <= uClustersImage(x, y) <= uClusterCountsImage(tx, ty) - 1
const GLchar* GLClusteredShadingBufferRenderer::FindUniqueClustersPass::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
    };

    uniform GBuffer uGBuffer;

    // Outputs of the shader:

    // Link each pixel to a cluster index local to the tile
    layout(r32ui) uniform coherent writeonly uimage2D uClustersImage;

    uniform coherent uint* uClusterCountsBuffer;
    uniform coherent vec3* uClusterBBoxLowerBuffer;
    uniform coherent vec3* uClusterBBoxUpperBuffer;
    uniform coherent vec3* uClusterNormalBuffer;

    uniform coherent vec3* uClusterRepPositionBuffer;
    uniform coherent vec3* uClusterRepNormalBuffer;

    uniform mat4 uInvProjMatrix;
    uniform mat4 uRcpViewMatrix;
    uniform vec2 uScreenSize;

    uniform bool uUseNormalClustering;

    uniform uint uSy;
    uniform float uZNear;
    uniform float uZFar;
    uniform float uHalfFOVY;

    // 32x32 is the size of a tile in screen space
    layout(local_size_x = 32, local_size_y = 32) in;

    const uint cNbThreads = gl_WorkGroupSize.x * gl_WorkGroupSize.y;

    shared uint sSampleIndices[cNbThreads];
    shared uint sClusterBuffer[cNbThreads];
    shared uint sClusterOffsets[cNbThreads];

    shared vec3 sViewSpaceBBoxLower[cNbThreads];
    shared vec4 sViewSpaceBBoxUpper[cNbThreads];

    shared vec3 sBBoxCenter;

    void oddEvenSortClusters() {
        for(uint i = 0; i < cNbThreads; ++i) {
            uint j;
            if(i % 2 == 0) {
                j = 2 * gl_LocalInvocationIndex;
            } else {
                j = 2 * gl_LocalInvocationIndex + 1;
            }

            uint lhs;
            uint rhs;

            if(j < cNbThreads - 1) {
                lhs = sClusterBuffer[j];
                rhs = sClusterBuffer[j + 1];
            }

            barrier();

            if(j < cNbThreads - 1 && lhs > rhs) {
                sClusterBuffer[j + 1] = lhs;
                sClusterBuffer[j] = rhs;

                uint tmp = sSampleIndices[j + 1];
                sSampleIndices[j + 1] = sSampleIndices[j];
                sSampleIndices[j] = tmp;
            }

            barrier();
        }
    }

    void mergeSortClusters() {
        uint tid = gl_LocalInvocationIndex;

        for(uint i = 1; i < cNbThreads; i *= 2) {
            uint listSize = i;
            uint listIdx = tid / i;
            uint listOffset = listIdx * listSize;
            uint otherListOffset;
            uint newListOffset;

            if(listIdx % 2 == 0) {
                otherListOffset = listOffset + listSize;
                newListOffset = listOffset;
            } else {
                otherListOffset = listOffset - listSize;
                newListOffset = otherListOffset;
            }

            // On doit calculer la position finale de l'element courant
            uint indexInMyList = tid - listOffset;

            uint indexInOtherList = 0;

            uint value = sClusterBuffer[tid];
            uint sIdx = sSampleIndices[tid];

            uint a = 0;
            uint b = listSize;
            uint middle;

            for(uint j = 1; j < listSize; j *= 2) {
                middle = (a + b) / 2;
                uint otherValue = sClusterBuffer[otherListOffset + middle];
                if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
                    b = middle;
                } else {
                    a = middle;
                }
            }

            uint otherValue = sClusterBuffer[otherListOffset + a];
            if(value < otherValue || (value == otherValue && (listIdx % 2 == 0))) {
                indexInOtherList = a;
            } else {
                indexInOtherList = b;
            }

            uint finalIndex = indexInMyList + indexInOtherList;

            barrier();

            sClusterBuffer[newListOffset + finalIndex] = value;
            sSampleIndices[newListOffset + finalIndex] = sIdx;

            barrier();
        }
    }

    void blellochExclusiveAddScan() {
        // Up-sweep phase
        uint a = 2 * gl_LocalInvocationIndex;
        uint b = 2 * gl_LocalInvocationIndex + 1;

        for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
            if(gl_LocalInvocationIndex < s) {
                sClusterOffsets[b] += sClusterOffsets[a];

                a = a * 2 + 1;
                b = b * 2 + 1;
            }
            barrier();
        }

        // Down-sweep phase
        if(gl_LocalInvocationIndex == 0) {
            sClusterOffsets[cNbThreads - 1] = 0;
        }
        barrier();

        for(uint s = 1; s < cNbThreads; s *= 2) {
            if(gl_LocalInvocationIndex < s) {
                a = (a - 1) / 2;
                b = (b - 1) / 2;

                uint tmp = sClusterOffsets[b];
                sClusterOffsets[b] += sClusterOffsets[a];
                sClusterOffsets[a] = tmp;
            }
            barrier();
        }
    }

    int getFaceIdx(vec3 wi) {
        vec3 absWi = abs(wi);

        float maxComponent = max(absWi.x, max(absWi.y, absWi.z));

        if(maxComponent == absWi.x) {
            if(wi.x > 0) {
                return 0;
            }
            return 1;
        }

        if(maxComponent == absWi.y) {
            if(wi.y > 0) {
                return 2;
            }
            return 3;
        }

        if(maxComponent == absWi.z) {
            if(wi.z > 0) {
                return 4;
            }
            return 5;
        }

        return -1;
    }

    vec3 getConeAxis(vec3 normal) {
        int faceIdx = getFaceIdx(normal);

        vec3 axis[] = vec3[](
                vec3(1, 0, 0),
                vec3(-1, 0, 0),
                vec3(0, 1, 0),
                vec3(0, -1, 0),
                vec3(0, 0, 1),
                vec3(0, 0, -1)
        );

        return axis[faceIdx];
    }

    bool lessThan(vec3 a, vec3 b) {
        return a.x < b.x || (a.x == b.x && a.y < b.y) || (a.xy == b.xy && a.z < b.z);
    }

    void main() {
        // Local ID of the current thread
        uint tid = gl_LocalInvocationIndex;

        vec4 normalDepth = texelFetch(uGBuffer.normalDepthSampler, ivec2(gl_GlobalInvocationID.xy), 0);
        vec3 N = normalDepth.xyz;
        uint cluster = uint(floor(log(normalDepth.w * uZFar / uZNear) / log(1 + 2 * tan(uHalfFOVY) / uSy)));
        if(uUseNormalClustering) {
            int faceIdx = getFaceIdx(N);
            cluster = 6 * cluster + faceIdx;
        }

        // Initialize the shared memory
        sClusterBuffer[gl_LocalInvocationIndex] = cluster;
        sClusterOffsets[gl_LocalInvocationIndex] = 0;
        sSampleIndices[gl_LocalInvocationIndex] = gl_LocalInvocationIndex;

        barrier();

        // Local sorting
        //oddEvenSortClusters();
        mergeSortClusters();

        bool isUniqueCluster = gl_LocalInvocationIndex == 0 ||
                sClusterBuffer[gl_LocalInvocationIndex] != sClusterBuffer[gl_LocalInvocationIndex - 1];

        // Initialization for scan
        if(isUniqueCluster) {
            sClusterOffsets[gl_LocalInvocationIndex] = 1;
        }
        barrier();

        // Compaction step
        blellochExclusiveAddScan();

        // Compute offset
        if(!isUniqueCluster) {
            sClusterOffsets[gl_LocalInvocationIndex] -= 1;
        }
        barrier();

        // Write the number of clusters for this tile in the output image
        if(gl_LocalInvocationIndex == 0) {
            uint clusterCount = sClusterOffsets[cNbThreads - 1] + 1;

            // FAUX ??! pas plutot gl_WorkGroupID.x + gl_WorkGroupID.y * NB TILES EN X ???
            // FONCTIONNE UNIQUEMENT SI NB TILES EN X = 32
            uClusterCountsBuffer[gl_WorkGroupID.x + gl_WorkGroupID.y * gl_WorkGroupSize.x] = clusterCount;
        }

        // Original view sample that have been moved to this cell by the sort step
        uint sampleIndex = sSampleIndices[gl_LocalInvocationIndex];

        // Offset associated to the cluster of this sample
        uint clusterOffset = sClusterOffsets[gl_LocalInvocationIndex];

        // Coordinates of this sample
        uint x = sampleIndex % gl_WorkGroupSize.x;
        uint y = (sampleIndex - x) / gl_WorkGroupSize.x;
        ivec2 globalCoords = ivec2(gl_WorkGroupID.x * gl_WorkGroupSize.x + x,
                             gl_WorkGroupID.y * gl_WorkGroupSize.y + y);

        // Write the offset for this cluster in the new cluster image
        imageStore(uClustersImage, globalCoords, uvec4(clusterOffset, 0, 0, 0));

        // Read geometric information of the original sample
        normalDepth = texelFetch(uGBuffer.normalDepthSampler, globalCoords, 0);
        N = normalDepth.xyz;
        vec2 pixelNDC = vec2(-1.f, -1.f) + 2.f * (vec2(globalCoords) + vec2(0.5f, 0.5f)) / uScreenSize;
        vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(pixelNDC, 1.f, 1.f);
        farPointViewSpaceCoords = farPointViewSpaceCoords / farPointViewSpaceCoords.w;
        vec4 samplePointViewSpaceCoords = vec4(normalDepth.w * farPointViewSpaceCoords.xyz, 1);

        sViewSpaceBBoxLower[gl_LocalInvocationIndex] = vec3(uRcpViewMatrix * samplePointViewSpaceCoords);
        sViewSpaceBBoxUpper[gl_LocalInvocationIndex] = vec4(sViewSpaceBBoxLower[gl_LocalInvocationIndex], 0);

        barrier();

        // Reduce step to get bounds of the clusters
        for(uint s = 1; s < cNbThreads; s *= 2) {
            uint other = tid + s;
            if(other < cNbThreads && sClusterOffsets[other] == clusterOffset) {
                //sData[tid] = max(sData[tid], sData[other]);
                sViewSpaceBBoxLower[tid] = min(sViewSpaceBBoxLower[tid], sViewSpaceBBoxLower[other]);
                sViewSpaceBBoxUpper[tid] = max(sViewSpaceBBoxUpper[tid], sViewSpaceBBoxUpper[other]);
            }
            barrier();
        }

        // Write bounds to ouput images
        if(isUniqueCluster) {
            uint globalOffset = (gl_WorkGroupID.x + gl_NumWorkGroups.x * gl_WorkGroupID.y) * cNbThreads + clusterOffset;

            uClusterBBoxLowerBuffer[globalOffset] = sViewSpaceBBoxLower[tid];
            uClusterBBoxUpperBuffer[globalOffset] = sViewSpaceBBoxUpper[tid].xyz;

            sBBoxCenter = (sViewSpaceBBoxLower[tid] + sViewSpaceBBoxUpper[tid].xyz) * 0.5f;

            if(uUseNormalClustering) {
                uClusterNormalBuffer[globalOffset] = getConeAxis(N);
            }
        }

        if(!uUseNormalClustering) {
            barrier();

            sViewSpaceBBoxUpper[gl_LocalInvocationIndex] = vec4(N, 1);

            barrier();

            // Reduce step to get normal of the clusters
            for(uint s = 1; s < cNbThreads; s *= 2) {
                uint other = tid + s;
                if(other < cNbThreads && sClusterOffsets[other] == clusterOffset) {
                    sViewSpaceBBoxUpper[tid] = sViewSpaceBBoxUpper[tid] + sViewSpaceBBoxUpper[other];
                }
                barrier();
            }

            // Write normal to ouput image
            if(isUniqueCluster) {
                uint globalOffset = (gl_WorkGroupID.x + gl_NumWorkGroups.x * gl_WorkGroupID.y) * cNbThreads + clusterOffset;
                uClusterNormalBuffer[globalOffset] = sViewSpaceBBoxUpper[tid].xyz / sViewSpaceBBoxUpper[tid].w;
            }
        }


        barrier();

        sViewSpaceBBoxLower[gl_LocalInvocationIndex] = vec3(uRcpViewMatrix * samplePointViewSpaceCoords);
        sViewSpaceBBoxUpper[gl_LocalInvocationIndex] = vec4(N, 1);

        barrier();

        // Reduce step to get representative of the clusters
        for(uint s = 1; s < cNbThreads; s *= 2) {
            uint other = tid + s;
            if(other < cNbThreads && sClusterOffsets[other] == clusterOffset) {
                if(lessThan(sViewSpaceBBoxLower[other], sViewSpaceBBoxLower[tid])) {
                    sViewSpaceBBoxLower[tid] = sViewSpaceBBoxLower[other];
                    sViewSpaceBBoxUpper[tid] = sViewSpaceBBoxUpper[other];
                }

//                float d1 = distance(sBBoxCenter, sViewSpaceBBoxLower[other]);
//                float d2 = distance(sBBoxCenter, sViewSpaceBBoxLower[tid]);

//                if(d1 < d2) {
//                    sViewSpaceBBoxLower[tid] = sViewSpaceBBoxLower[other];
//                    sViewSpaceBBoxUpper[tid] = sViewSpaceBBoxUpper[other];
//                } else if(d1 == d2 && lessThan(sViewSpaceBBoxLower[other], sViewSpaceBBoxLower[tid])) {
//                    sViewSpaceBBoxLower[tid] = sViewSpaceBBoxLower[other];
//                    sViewSpaceBBoxUpper[tid] = sViewSpaceBBoxUpper[other];
//                }
            }
            barrier();
        }

        // Write bounds to ouput images
        if(isUniqueCluster) {
            uint globalOffset = (gl_WorkGroupID.x + gl_NumWorkGroups.x * gl_WorkGroupID.y) * cNbThreads + clusterOffset;

            uClusterRepPositionBuffer[globalOffset] = sViewSpaceBBoxLower[tid];
            uClusterRepNormalBuffer[globalOffset] = sViewSpaceBBoxUpper[tid].xyz;
        }

        if(!uUseNormalClustering) {
            barrier();

            sViewSpaceBBoxUpper[gl_LocalInvocationIndex] = vec4(N, 1);

            barrier();

            // Reduce step to get normal of the clusters
            for(uint s = 1; s < cNbThreads; s *= 2) {
                uint other = tid + s;
                if(other < cNbThreads && sClusterOffsets[other] == clusterOffset) {
                    sViewSpaceBBoxUpper[tid] = sViewSpaceBBoxUpper[tid] + sViewSpaceBBoxUpper[other];
                }
                barrier();
            }

            // Write normal to ouput image
            if(isUniqueCluster) {
                uint globalOffset = (gl_WorkGroupID.x + gl_NumWorkGroups.x * gl_WorkGroupID.y) * cNbThreads + clusterOffset;
                uClusterNormalBuffer[globalOffset] = sViewSpaceBBoxUpper[tid].xyz / sViewSpaceBBoxUpper[tid].w;
            }
        }


        memoryBarrier();
    }
);

const GLchar* GLClusteredShadingBufferRenderer::CountClustersPass::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    // Inputs
    // Number of screen space tiles
    uniform uint uTileCount;

    // For each tile (Tx, Ty), uClusterCountsBuffer[Tx + Ty * Sx] is the number of cluster contained in the tile
    uniform coherent uint* uClusterCountsBuffer;

    // Outputs
    // For each tile (Tx, Ty), uClusterTilesOffsetsBuffer[Tx + Ty * Sx] will be the global index of the first cluster
    // contained in the tile.
    uniform coherent uint* uClusterTilesOffsetsBuffer;
    // Will be the total number of clusters
    uniform coherent uint* uClusterCount;
    // Will contains for each cluster the index of the associated tile
    uniform coherent uint* uClusterToTileBuffer;

    layout(local_size_x = 1024) in;

    const uint cNbThreads = gl_WorkGroupSize.x;

    shared uint sGroupData[cNbThreads];

    void main() {
        uint tileClusterCount = 0;
        // Init
        if(gl_GlobalInvocationID.x >= uTileCount) {
            sGroupData[gl_LocalInvocationIndex] = 0;
        } else {
            tileClusterCount = sGroupData[gl_LocalInvocationIndex] = uClusterCountsBuffer[gl_GlobalInvocationID.x];
        }

        barrier();

        // Scan to compute offsets and cluster count

        // Up-sweep phase
        uint a = 2 * gl_LocalInvocationIndex;
        uint b = 2 * gl_LocalInvocationIndex + 1;

        for(uint s = cNbThreads >> 1; s > 0; s >>= 1) {
            if(gl_LocalInvocationIndex < s) {
                sGroupData[b] += sGroupData[a];

                a = a * 2 + 1;
                b = b * 2 + 1;
            }
            barrier();
        }

        // Down-sweep phase
        if(gl_LocalInvocationIndex == 0) {
            sGroupData[cNbThreads - 1] = 0;
        }
        barrier();

        for(uint s = 1; s < cNbThreads; s *= 2) {
            if(gl_LocalInvocationIndex < s) {
                a = (a - 1) / 2;
                b = (b - 1) / 2;

                uint tmp = sGroupData[b];
                sGroupData[b] += sGroupData[a];
                sGroupData[a] = tmp;
            }
            barrier();
        }

        if(gl_GlobalInvocationID.x < uTileCount) {
            uint tileOffset = uClusterTilesOffsetsBuffer[gl_GlobalInvocationID.x] = sGroupData[gl_LocalInvocationIndex];

            if(gl_GlobalInvocationID.x == uTileCount - 1) {
                *uClusterCount = sGroupData[gl_LocalInvocationIndex] + uClusterCountsBuffer[gl_GlobalInvocationID.x];
            }

            // Write correspondance from cluster to tile
            for(uint i = 0; i < tileClusterCount; ++i) {
                uClusterToTileBuffer[tileOffset + i] = gl_LocalInvocationIndex;
            }

            barrier();
        }

        memoryBarrier();
    }
);

void GLClusteredShadingBufferRenderer::render(const glm::mat4& rcpProjMatrix, const glm::mat4& rcpViewMatrix,
                                              const GLGBuffer& gbuffer, GLClusteredShadingBuffer& buffer, bool useNormalClustering) {
    m_FindUniqueClustersPass.m_Program.use();

    m_FindUniqueClustersPass.uGBuffer.set(gbuffer);

    m_FindUniqueClustersPass.uClustersImage.set(buffer.m_GeometryClustersTexture.getImageHandle());

    m_FindUniqueClustersPass.uClusterCountsBuffer.set(buffer.m_GClusterCountsBuffer.getGPUAddress());
    m_FindUniqueClustersPass.uClusterBBoxLowerBuffer.set(buffer.m_ClusterBBoxLowerBuffer.getGPUAddress());
    m_FindUniqueClustersPass.uClusterBBoxUpperBuffer.set(buffer.m_ClusterBBoxUpperBuffer.getGPUAddress());
    m_FindUniqueClustersPass.uClusterNormalBuffer.set(buffer.m_ClusterNormalBuffer.getGPUAddress());

    m_FindUniqueClustersPass.uClusterRepPositionBuffer.set(buffer.m_ClusterRepPositionBuffer.getGPUAddress());
    m_FindUniqueClustersPass.uClusterRepNormalBuffer.set(buffer.m_ClusterRepNormalBuffer.getGPUAddress());

    m_FindUniqueClustersPass.uInvProjMatrix.set(rcpProjMatrix);
    m_FindUniqueClustersPass.uRcpViewMatrix.set(rcpViewMatrix);

    m_FindUniqueClustersPass.uScreenSize.set(glm::vec2(gbuffer.getWidth(), gbuffer.getHeight()));

    m_FindUniqueClustersPass.uUseNormalClustering.set(useNormalClustering);

    m_FindUniqueClustersPass.uSy.set(buffer.m_TileCount.y);
    m_FindUniqueClustersPass.uHalfFOVY.set(glm::radians(0.5f * getFOVY()));
    m_FindUniqueClustersPass.uZNear.set(getZNear());
    m_FindUniqueClustersPass.uZFar.set(getZFar());

    glDispatchCompute(buffer.m_TileCount.x, buffer.m_TileCount.y, 1);

    glMemoryBarrier(GL_ALL_BARRIER_BITS);

    m_CountClustersPass.m_Program.use();

    uint32_t tileCount = buffer.m_TileCount.x * buffer.m_TileCount.y;
    uint32_t workGroupSize = 1024;
    uint32_t workGroupCount = (tileCount / workGroupSize) + (tileCount % workGroupSize != 0);

    m_CountClustersPass.uClusterTilesOffsetsBuffer.set(buffer.m_GClusterTilesOffsetsBuffer.getGPUAddress());
    m_CountClustersPass.uClusterCountsBuffer.set(buffer.m_GClusterCountsBuffer.getGPUAddress());
    m_CountClustersPass.uClusterToTileBuffer.set(buffer.m_GClusterToTileBuffer.getGPUAddress());
    m_CountClustersPass.uClusterCount.set(buffer.m_GClusterTotalCountBuffer.getGPUAddress());

    // ONLY WORKS FOR IMAGE WITH LESS THAN 1024 TILES
    // Can be extended for more but require more compute shader code to handle the parallel
    // segmented scan
    assert(workGroupCount <= 1);

    m_CountClustersPass.uTileCount.set(tileCount);

    glDispatchCompute(workGroupCount, 1, 1);

    glMemoryBarrier(GL_ALL_BARRIER_BITS);

    buffer.m_GClusterTotalCountBuffer.getData(1, &buffer.m_nGClusterCount);
}

}
