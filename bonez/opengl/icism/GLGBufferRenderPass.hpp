#pragma once

#include "bonez/opengl/shaders/GLSLProgramBuilder.hpp"
#include "bonez/opengl/scene/GLScene.hpp"

#include "bonez/opengl/utils/GLutils.hpp"
#include "GLGBuffer.hpp"

namespace BnZ {

class GLGBufferRenderPass {
    static const GLchar* s_VertexShader;
    static const GLchar* s_FragmentShader;

    gloops::Program m_Program { buildProgram(s_VertexShader, s_FragmentShader) };

    GLMaterialUniforms m_MaterialUniforms { m_Program };
    BNZ_GLUNIFORM(m_Program, glm::mat4, uMVPMatrix);
    BNZ_GLUNIFORM(m_Program, glm::mat4, uMVMatrix);
    BNZ_GLUNIFORM(m_Program, float, uZFar);

public:
    void render(const glm::mat4& projMatrix, const glm::mat4& viewMatrix,
                const GLScene& scene, GLGBuffer& gbuffer);
};

}
