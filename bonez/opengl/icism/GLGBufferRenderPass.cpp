#include "GLGBufferRenderPass.hpp"

namespace BnZ {

const GLchar* GLGBufferRenderPass::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec3 aPosition;
    layout(location = 1) in vec3 aNormal;
    layout(location = 2) in vec2 aTexCoords;

    uniform mat4 uMVPMatrix;
    uniform mat4 uMVMatrix;

    out vec3 vPosition;
    out vec3 vNormal;
    out vec2 vTexCoords;
    out vec3 vWorldSpaceNormal;
    out vec3 vWorldSpacePosition;

    void main() {
        vWorldSpacePosition = aPosition;
        vWorldSpaceNormal = aNormal;
        vPosition = vec3(uMVMatrix * vec4(aPosition, 1));
        vNormal = aNormal;
        vTexCoords = aTexCoords;

        gl_Position = uMVPMatrix * vec4(aPosition, 1);
    }
);

const GLchar* GLGBufferRenderPass::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    in vec3 vPosition;
    in vec3 vNormal;
    in vec2 vTexCoords;
    in vec3 vWorldSpacePosition;
    in vec3 vWorldSpaceNormal;

    struct Material {
        vec3 Kd;
        vec3 Ks;
        float shininess;
        sampler2D KdSampler;
        sampler2D KsSampler;
        sampler2D shininessSampler;
    };

    uniform Material uMaterial;

    uniform float uZFar;

    layout(location = 0) out vec4 fNormalDepth;
    layout(location = 1) out vec3 fDiffuse;
    layout(location = 2) out vec4 fGlossyShininess;

    const float PI = 3.14159265358979323846264;

    void main() {
        vec3 nNormal = normalize(vNormal);
        fNormalDepth.xyz = nNormal;
        fNormalDepth.w = -vPosition.z / uZFar; // Normalized depth value

        fDiffuse = uMaterial.Kd * texture(uMaterial.KdSampler, vTexCoords).rgb / PI;

        float shininess = uMaterial.shininess + texture(uMaterial.shininessSampler, vTexCoords).r;
        fGlossyShininess.rgb = uMaterial.Ks * texture(uMaterial.KsSampler, vTexCoords).rgb * (shininess + 2) / (2 * PI);
        fGlossyShininess.a = shininess;
    }
);

void GLGBufferRenderPass::render(const glm::mat4& projMatrix, const glm::mat4& viewMatrix,
            const GLScene& scene, GLGBuffer& gbuffer) {
    auto depthTest = pushGLState<GL_DEPTH_TEST>();
    depthTest.set(true);

    auto viewport = pushGLState<GL_VIEWPORT>();
    viewport.set(glm::vec4(0, 0, gbuffer.getWidth(), gbuffer.getHeight()));

    auto fbo = pushGLState<GL_DRAW_FRAMEBUFFER_BINDING>();
    gbuffer.bindForDrawing();

    m_Program.use();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 MVPMatrix = projMatrix * viewMatrix;
    glm::mat4 MVMatrix = viewMatrix;

    uMVPMatrix.set(MVPMatrix);
    uMVMatrix.set(MVMatrix);

    uZFar.set(getZFar());

    scene.render(m_MaterialUniforms);
}

}
