#include "GLVPLLightingRenderPass.hpp"

namespace BnZ {

const GLchar* GLVPLLightingRenderPass::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    out vec3 vFarPointViewSpaceCoords;

    uniform mat4 uInvProjMatrix;

    void main() {
        vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
        vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

        gl_Position = vec4(aPosition, 0, 1);
    }
);

const GLchar* GLVPLLightingRenderPass::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_bindless_texture : enable\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

    uniform vec4 uViewport;

    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
    };

    uniform GBuffer uGBuffer;

    uniform uvec2 uTileCount;

    struct VPL {
        vec3 P;
        vec3 N;
        vec3 L;
    };

    uniform uint uVPLCount;
    uniform bool uRenderIrradiance;

    uniform VPL* uVPLBuffer;

    struct ISMData {
        uint smResolution;
        uint smCountPerTexture;
        uint smCountPerDimension;
    };

    uniform ISMData uISMData;
    uniform sampler2D* uVPLISMBuffer;
    uniform mat4* uVPLViewMatrixBuffer;

    uniform float uDistClamp;

    uniform float uZNear;
    uniform float uZFar;
    uniform float uISMOffset;

    uniform bool uNoShadow;

    in vec3 vFarPointViewSpaceCoords;

    out vec3 fFragColor;

    uniform mat4 uRcpViewMatrix;

    float ismLookup(uint vplIdx, vec3 Pws) {
        if(uNoShadow) {
            return 1.f;
        }

        mat4 viewMatrix = uVPLViewMatrixBuffer[vplIdx];

        vec4 Pvs = viewMatrix * vec4(Pws, 1); // Position in view space
        Pvs.z = -Pvs.z; // View the negative hemisphere

        // Paraboloid projection
        Pvs /= Pvs.w;

        vec3 v = Pvs.xyz;

        vec3 projectedPosition;
        projectedPosition.z = Pvs.z;

        float l = length(v);
        v /= l;

        vec3 h = v + vec3(0, 0, 1); // h = k * (x, y, 1)  => x = h.x / h.z   y = h.y / h.z

        float refDepth = (l - uZNear) / (uZFar - uZNear);

        if(projectedPosition.z < 0.f) {
            return 0.f;
        }

        // vProjectedPosition is in [-1, 1] x [-1, 1]
        projectedPosition.xy = h.xy / h.z;

        uint texIdx = vplIdx / uISMData.smCountPerTexture;

        uint viewIdx = vplIdx % uISMData.smCountPerTexture;

        // Dimension of a view in texture space
        float size = 1.f / uISMData.smCountPerDimension;

        // 2D coordinates of the viewport attached to the view
        uint i = viewIdx % uISMData.smCountPerDimension;
        uint j = (viewIdx - i) / uISMData.smCountPerDimension;

        // Coordinates between 0 and 1 in the viewport of the view
        vec2 xy = 0.5 * (projectedPosition.xy + vec2(1, 1));

        // Origin of the viewport of the view in clip space
        vec2 origin = vec2(i, j) * size;

        vec2 texCoords = origin + xy * size;

        float depth = texture(uVPLISMBuffer[texIdx], texCoords).r;

        return refDepth < depth + uISMOffset ? 1.f : 0.f;
    }

    void main() {
        vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

        // Read fragment attributes
        vec4 normalDepth = texture(uGBuffer.normalDepthSampler, texCoords);
        vec3 fragPosition = normalDepth.w * vFarPointViewSpaceCoords;
        vec3 Kd;
        vec4 KsShininess;

        if(uRenderIrradiance) {
            Kd = vec3(1.f / 3.14f);
            KsShininess = vec4(0.f, 0.f, 0.f, 1.f);
        } else {
            Kd = texture(uGBuffer.diffuseSampler, texCoords).rgb;
            KsShininess = texture(uGBuffer.glossyShininessSampler, texCoords);
        }

        fFragColor = vec3(0, 0, 0);

        // Position of the fragment in world space
        vec3 Pws = vec3(uRcpViewMatrix * vec4(fragPosition, 1));

        for(int vplIdx = 0; vplIdx < uVPLCount; ++vplIdx) {
            VPL vpl = uVPLBuffer[vplIdx];

            vec3 wi = vpl.P - Pws;
            float dist = length(wi);
            wi /= dist;

            float cos_o = max(0, dot(-wi, vpl.N));
            float cos_i = max(0, dot(wi, normalDepth.xyz));

            dist = max(dist, uDistClamp);

            float G = cos_i * cos_o / (dist * dist);

            vec3 r = reflect(-wi, normalDepth.xyz);

            vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);

            float shadowFactor = ismLookup(vplIdx, Pws);

            fFragColor = fFragColor + shadowFactor * brdf * G * vpl.L;
        }
    }
);


void GLVPLLightingRenderPass::render(const glm::mat4& rcpProjMatrix, const glm::mat4& rcpViewMatrix,
            const glm::vec4& viewport,
            float distClamp, float ismOffset, bool renderIrradiance, bool noShadow,
            const GLVPLData& vplData,
            GLGBuffer& gbuffer, const ScreenTriangle& triangle) {
    m_Program.use();

    uGBuffer.set(gbuffer);

    // Send matrices
    uViewport.set(viewport);
    uInvProjMatrix.set(rcpProjMatrix);

    uVPLCount.set(vplData.getVPLCount());

    uVPLBuffer.set(vplData.getVPLBuffer().getGPUAddress());

    uRcpViewMatrix.set(rcpViewMatrix);
    uRenderIrradiance.set(renderIrradiance);
    uDistClamp.set(distClamp);

    uZNear.set(getZNear());
    uZFar.set(getZFar());

    if(noShadow) {
        uNoShadow.set(true);
    } else {
        uISMData.set(vplData.getVPLISMContainer());

        uVPLISMBuffer.set(vplData.getVPLISMBuffer().getGPUAddress());
        uVPLViewMatrixBuffer.set(vplData.getVPLViewMatrixBuffer().getGPUAddress());

        uISMOffset.set(ismOffset);

        uNoShadow.set(false);
    }

    triangle.render();
}

}
