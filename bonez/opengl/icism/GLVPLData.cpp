#include "GLVPLData.hpp"

namespace BnZ {

void GLVPLData::setVPLs(const VPLContainer& vpls, float ismNormalOffset) {
    m_VPLs.clear();
    m_VPLViewMatrices.clear();

    m_VPLCount = vpls.orientedVPLs.size();

    for(auto idx: range(m_VPLCount)) {
        auto vpl = vpls.orientedVPLs[idx];

        glm::vec4 lightPosition = glm::vec4(convert(vpl.P), 1);
        glm::vec3 lightNormal = convert(vpl.N);

        m_VPLs.emplace_back(lightPosition);
        m_VPLs.emplace_back(glm::vec4(lightNormal, 0));
        m_VPLs.emplace_back(glm::vec4(vpl.L.r, vpl.L.g, vpl.L.b, 0));

        glm::vec3 eye = convert(vpl.P + vpl.N * ismNormalOffset);
        glm::vec3 point = eye + convert(vpl.N);
        glm::vec3 up;
        if(vpl.N.x != 0) {
            up = glm::vec3(-(vpl.N.y + vpl.N.z) / vpl.N.x, 1, 1);
        } else if(vpl.N.y != 0) {
            up = glm::vec3(1, -(vpl.N.x + vpl.N.z) / vpl.N.y, 1);
        } else {
            up = glm::vec3(1, 1, -(vpl.N.x + vpl.N.y) / vpl.N.z);
        }

        m_VPLViewMatrices.emplace_back(glm::lookAt(eye, point, up));
    }

    m_VPLBuffer.setData(m_VPLs, GL_DYNAMIC_DRAW);
    m_VPLBuffer.makeResident(GL_READ_ONLY);

    m_VPLViewMatrixBuffer.setData(m_VPLViewMatrices, GL_DYNAMIC_DRAW);
    m_VPLViewMatrixBuffer.makeResident(GL_READ_ONLY);
}

void GLVPLData::renderISMs(GLISMRenderer& ismRenderer, const GLPoints& sampledScene,
                           float pointMaxSize, bool doPull, bool doPullPush,
                           float pullTreshold, float pushTreshold, uint32_t pullPushNbLevels) {
    ismRenderer.render(sampledScene, m_VPLISMContainer, m_VPLViewMatrices.data(),
                         m_VPLViewMatrices.size(), pointMaxSize, doPull, doPullPush,
                         pullTreshold, pushTreshold, pullPushNbLevels);

    m_VPLISMBuffer.setData(m_VPLISMContainer.getTextureCount(),
                           m_VPLISMContainer.getTextureHandleBufferPtr(),
                           GL_DYNAMIC_DRAW);
    m_VPLISMBuffer.makeResident(GL_READ_ONLY);
}

}
