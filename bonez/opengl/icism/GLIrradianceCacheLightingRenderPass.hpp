#pragma once

#include "bonez/opengl/shaders/GLSLProgramBuilder.hpp"
#include "bonez/opengl/utils/GLutils.hpp"
#include "GLGBuffer.hpp"
#include "GLIrradianceCachingData.hpp"

namespace BnZ {

class GLIrradianceCacheLightingRenderPass {
    struct SplatIrradiancePass {
        static const GLchar* s_VertexShader;
        static const GLchar* s_GeometryShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program = buildProgram(s_VertexShader, s_GeometryShader, s_FragmentShader);

        BNZ_GLUNIFORM(m_Program, glm::mat4, uViewMatrix);
        BNZ_GLUNIFORM(m_Program, glm::mat4, uProjMatrix);
        BNZ_GLUNIFORM(m_Program, glm::vec4, uViewport);
        BNZ_GLUNIFORM(m_Program, glm::mat4, uRcpProjMatrix);
        BNZ_GLUNIFORM(m_Program, glm::mat4, uRcpViewMatrix);

        GLGBufferUniform uGBuffer { m_Program };

        // Output: R, G, B for irradiance estimation times weight, A for weight sum
        GLFramebuffer<1> m_IrradianceEstimationBuffer;
        //GLuint64 m_IrradianceEstimationTextureHandle;
    };

    SplatIrradiancePass m_SplatIrradiancePass;

    struct WeightDivisionPass {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program = buildProgram(s_VertexShader, s_FragmentShader);

        BNZ_GLUNIFORM(m_Program, glm::vec4, uViewport);
        BNZ_GLUNIFORM(m_Program, glm::mat4, uRcpProjMatrix);
        BNZ_GLUNIFORM(m_Program, GLuint64, uIrradianceSampler);
        BNZ_GLUNIFORM(m_Program, bool, uRenderIrradiance);

        GLGBufferUniform uGBuffer { m_Program };
    };

    WeightDivisionPass m_WieghtDivisionPass;

public:
    void setResolution(uint32_t width, uint32_t height);

    void render(const glm::mat4& projMatrix, const glm::mat4& viewMatrix,
                const glm::mat4& rcpProjMatrix, const glm::mat4& rcpViewMatrix,
                const glm::vec4& viewport, bool renderIrradiance,
                const GLGBuffer& gbuffer, const GLIrradianceCachingData& icData,
                const ScreenTriangle& triangle);
};

}
