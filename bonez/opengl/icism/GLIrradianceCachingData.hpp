#pragma once

#include "bonez/opengl/utils/GLutils.hpp"
#include "bonez/opengl/GLISMRenderer.hpp"
#include "GLManyLODRenderPass.hpp"

namespace BnZ {

struct GLIrradianceCachingData {
    static const uint32_t ISM_RESOLUTION = 64;

    struct ICVertex {
        glm::vec4 positionRadius;
        glm::vec4 normal;
        glm::vec4 irradiance;
    };

private:
    gloops::VertexArrayObject m_VAO;
    std::vector<ICVertex> m_CPUData;
    GLBuffer m_GPUData;
    uint32_t m_nICCount;
    GLBuffer m_ISMBuffer;
    GLBuffer m_ViewMatrixBuffer;
    std::vector<glm::mat4> m_ViewMatrices;
    GLISMContainer m_ISMContainer { ISM_RESOLUTION };

public:
    GLIrradianceCachingData() {
        glBindVertexArray(m_VAO.glId());

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);

        glBindBuffer(GL_ARRAY_BUFFER, m_GPUData.glId());
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(ICVertex), GLOOPS_OFFSETOF(ICVertex, positionRadius));
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(ICVertex), GLOOPS_OFFSETOF(ICVertex, normal));
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(ICVertex), GLOOPS_OFFSETOF(ICVertex, irradiance));
    }

    void setData(std::vector<ICVertex> data) {
        std::swap(m_CPUData, data);

        m_GPUData.setData(m_CPUData, GL_DYNAMIC_DRAW);
        m_GPUData.makeResident(GL_READ_WRITE);
        m_nICCount = m_CPUData.size();
    }

    void setViewMatrices(std::vector<glm::mat4> viewMatrices) {
        std::swap(m_ViewMatrices, viewMatrices);

        m_ViewMatrixBuffer.setData(m_ViewMatrices, GL_DYNAMIC_DRAW);
        m_ViewMatrixBuffer.makeResident(GL_READ_ONLY);
    }

    const GLBuffer& getICBuffer() const {
        return m_GPUData;
    }

    uint32_t getIrradianceCacheCount() const {
        return m_nICCount;
    }

    std::vector<ICVertex>& getCPUBuffer() {
        return m_CPUData;
    }

    const GLBuffer& getISMBuffer() const {
        return m_ISMBuffer;
    }

    const GLISMContainer& getISMContainer() const {
        return m_ISMContainer;
    }

    GLISMContainer& getISMContainer() {
        return m_ISMContainer;
    }

    GLBuffer& getViewMatrixBuffer() {
        return m_ViewMatrixBuffer;
    }

    uint32_t getICCount() const {
        return m_CPUData.size();
    }

    void renderISMs(GLISMRenderer& ismRenderer, const GLPoints& sampledScene, float pointMaxSize, bool doPull, bool doPullPush, float pullTreshold,
                    float pushTreshold, uint32_t pullPushNbLevels, bool useCoherentIrradianceCache);

    void renderISMsPacks(GLISMRenderer& ismRenderer, const GLPoints& sampledScene, size_t pointCountPerISM,
                         float pointMaxSize, bool doPull, bool doPullPush, float pullTreshold,
                         float pushTreshold, uint32_t pullPushNbLevels);

    void renderISMsManyLOD(GLISMRenderer& ismRenderer, GLManyLODRenderPass& manyLoDRenderPass,
                           float treshold, uint32_t maxBSHLevel, bool doPull, bool doPullPush, float pullTreshold,
                           float pushTreshold, uint32_t pullPushNbLevels);

    void drawPoints() const {
        glBindVertexArray(m_VAO.glId());
        glDrawArrays(GL_POINTS, 0, m_nICCount);
    }
};

}
