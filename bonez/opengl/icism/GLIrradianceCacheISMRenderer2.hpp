#ifndef _BONEZ_GLIrradianceCacheISMRenderer22_HPP_
#define _BONEZ_GLIrradianceCacheISMRenderer22_HPP_

#include "bonez/scene/Scene.hpp"
#include "opengl/shaders/GLSLProgramBuilder.hpp"
#include "opengl/scene/GLScene.hpp"

#include "bonez/scene/lights/vpls.hpp"

#include <vector>
#include <glm/glm.hpp>

#include "bonez/opengl/utils/GLFramebuffer.hpp"
#include "bonez/opengl/utils/GLTextureBuffer.hpp"
#include "bonez/opengl/utils/GLBuffer.hpp"
#include "bonez/opengl/utils/GLQuery.hpp"

#include "bonez/opengl/analysis/GLVisualDataRenderer.hpp"

#include "bonez/opengl/GLShadowMapRenderer.hpp"
#include "bonez/opengl/GLISMRenderer.hpp"

#include "bonez/opengl/GLResultRenderer.hpp"

#include "bonez/opengl/utils/GLUniform.hpp"

#include <atb.hpp>

#include "GLGBufferRenderPass.hpp"
#include "GLClusteredShadingBufferRenderPass.hpp"
#include "GLDirectLightingRenderPass.hpp"
#include "GLVPLLightingRenderPass.hpp"
#include "GLIrradianceCachingData.hpp"
#include "GLIrradianceCacheLightingRenderPass.hpp"
#include "GLImperfectIrradianceCacheComputePass.hpp"

#include "GLManyLODBSH.hpp"
#include "GLManyLODRenderPass.hpp"

namespace BnZ {

class GLIrradianceCacheISMRenderer2 {
public:
    void setResolution(size_t width, size_t height);

    void setProjectionMatrix(const glm::mat4& projectionMatrix) {
        m_ProjectionMatrix = projectionMatrix;
        m_RcpProjMatrix = glm::inverse(m_ProjectionMatrix);
    }

    void setViewMatrix(const glm::mat4& viewMatrix) {
        m_ViewMatrix = viewMatrix;
        m_RcpViewMatrix = glm::inverse(m_ViewMatrix);
    }

    void setUp();

    void sampleScene(const Scene& scene, const Camera& camera);

    void geometryPass(const GLScene& scene);

    void shadingPass(int x, int y, size_t width, size_t height,
        const VPLContainer& vpls, const Scene& scene, const Camera& camera);

    void drawBuffers(int x, int y, size_t width, size_t height);

    void drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay);

    void displayData(GLVisualDataRenderer& renderer, const Scene& scene);

    void drawISMs(int x, int y, size_t width, size_t height);

    void computeTimings();

    void setPrimaryLightSource(const Vec3f& P, const Col3f& L, const GLScene& glScene);

    void exposeIO(TwBar* bar);

    bool doesRenderIrradiance() const {
        return m_bRenderIrradiance;
    }

    bool doesRenderDirectLight() const {
        return m_bDoDirectLightPass;
    }

    float getDistanceClamp() const {
        return m_fDistClamp;
    }

    void processPixel(int pixelX, int pixelY);

private:
    static const uint32_t TILE_SIZE;
    static const uint32_t ISM_RESOLUTION;

    void computeSamplesFromBSH();
    uint32_t m_BSHLevel = 16;
    GLManyLODRenderPass m_ManyLODRenderPass;
    std::vector<uint32_t> m_SelectedBSHCut;

    std::vector<GLPoint> m_SceneSamples;

    KdTree m_SceneSamplesKdTree;

    // Samples used for rendering
    std::vector<GLPoint> m_NewSceneSamples;

    // This is used to compare displayed ISM to raytraced SM
    uint32_t m_nFrameIdx = 0;
    GLResultRenderer m_ResultRenderer;
    Framebuffer m_Framebuffer = Framebuffer(ISM_RESOLUTION, ISM_RESOLUTION);
    const Scene* m_pScene = nullptr;

    // Selected Irradiance Cache to display its ISM
    GLIrradianceCachingData::ICVertex m_SelectedCache;
    void getClusterCache(uint32_t tileIdx, uint32_t clusterLocalIdx);

    glm::mat4 m_ProjectionMatrix { 1.f };
    glm::mat4 m_RcpProjMatrix { 1.f };
    glm::mat4 m_ViewMatrix { 1.f };
    glm::mat4 m_RcpViewMatrix { 1.f };

    GLPoints m_SampledScene;

    GLGBuffer m_GBuffer;
    GLGBufferRenderPass m_GBufferRenderPass;

    GLClusteredShadingBuffer m_GeometryClustersData;
    GLClusteredShadingBufferRenderer m_GLClusteredShadingBufferRenderer;

    VPLContainer m_VPLContainer;

    GLVPLData m_VPLData;
    void computeVPLData(const VPLContainer& vpls);

    GLIrradianceCachingData m_IrradianceCacheData;
    GLImperfectIrradianceCacheComputePass m_ImperfectIrradianceCacheComputePass;

    void computeIrradianceCachesFromGeometryClusters();
    void computeIrradianceCachingData();
    void computeCPUIrradianceCaches();

    GLVPLLightingRenderPass m_VPLLightingRenderPass;
    GLIrradianceCacheLightingRenderPass m_IrradianceCacheLightingRenderPass;
    GLDirectLightingRenderPass m_DirectLightingRenderPass;

    struct DrawBuffersPassData {
        static const GLchar* s_VertexShader;
        static const GLchar* s_FragmentShader;

        gloops::Program m_Program;

        gloops::Uniform m_uViewport;
        GLGBufferUniform uGBuffer;
        gloops::Uniform m_uGeometryClusterSampler;
        gloops::Uniform m_uZFar;
        gloops::Uniform m_uDataToDisplay;
        gloops::Uniform m_uTileCount;
        gloops::Uniform m_uClusterTilesOffsetsBuffer;
        gloops::Uniform m_uClusterToTileBuffer;

        // Doesn't work ?
        //gloops::Uniform m_uClusterBBoxLowerBuffer;
        //gloops::Uniform m_uClusterBBoxUpperBuffer;

        GLQuery m_TimeElapsedQuery;

        DrawBuffersPassData();
    };

    DrawBuffersPassData m_DrawBuffersPassData;

    GLISMRenderer m_ISMRenderer;
    GLISMDrawer m_ISMDrawer;

    ScreenTriangle m_ScreenTriangle;

    void vplShadingPass(int x, int y, size_t width, size_t height);

    void icShadingPass(int x, int y, size_t width, size_t height);

    void directLightShadingPass(int x, int y, size_t width, size_t height);

    // Input - Outputs

    // In milliseconds
    struct Timings {
        GLQuery m_GeometryPassQuery;
        GLQuery m_ClusteredShadingPassQuery;
        GLQuery m_DirectLightingPassQuery;
        GLQuery m_VPLLightingPassQuery;
        GLQuery m_ICLightingPassQuery;
        GLQuery m_ImperfectICPassQuery;
        GLQuery m_ComputeVPLISMsQuery;
        GLQuery m_ComputeICISMsQuery;

        float geometryPass = 0;
        float clusteredShadingPass = 0;
        float computeIrradiancePass = 0;
        float vplShadingPass = 0;
        float directLightShadingPass = 0;
        float drawBuffersPass = 0;
        float computeVPLISMPass = 0;
        float computeICISMPass = 0;
        float splatICPass = 0;

        float totalTime = 0;

        void updateTotalTime() {
            totalTime = geometryPass + clusteredShadingPass + computeIrradiancePass +
                    vplShadingPass + directLightShadingPass + drawBuffersPass + computeVPLISMPass +
                    computeICISMPass + splatICPass;
        }
    };

    bool m_bDoVPLShadingPass = false;
    bool m_bUseVPLISMs = false;

    bool m_bDoICShadingPass = true;

    bool m_bDoDirectLightPass = false;

    bool m_bRenderIrradiance = true;
    float m_fGamma = 1.f;
    float m_fShadowMapOffset = 0.0001f;
    float m_fDistClamp = 100.f;
    bool m_bUseNormalClustering = true;
    bool m_bShowSpheres = false;
    bool m_bDoPull = false;
    bool m_bDoPullPush = false;
    float m_fPullTreshold = 0.05f;
    float m_fPushTreshold = 0.4f;
    float m_fISMOffset = 0.01f;
    float m_fISMPointMaxSize = 2.f;
    int m_nPullPushNbLevels = -1;
    float m_fISMNormalOffset = 3.f;
    int m_nSelectedISMIndex = -1;
    uint32_t m_nSelectedISMTextureIndex = 0;
    uint32_t m_nSelectedISMLevel = 0;
    uint32_t m_nSceneSampleCount = 65000;
    uint32_t m_nSceneSamplingMode = 0;
    int m_nTileWindow = 3;
    float m_fMaxDist = 100.f;
    float m_fSplatRadiusFactor = 1.f;
    bool m_bUseIrradianceEstimate = false;
    bool m_bUseCoherentIrradianceCache = false;
    bool m_bUseCPUIrradianceCache = true;
    uint32_t m_nPointCountPerIC = 1000;
    uint32_t m_nPointClusterCount = 1000;
    float m_fManyLODTreshold = 1.f;

    Timings m_Timings;
};

}

#endif
