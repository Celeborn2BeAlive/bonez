#include "GLImperfectIrradianceCacheComputePass.hpp"

namespace BnZ {


const GLchar* GLImperfectIrradianceCacheComputePass::s_ComputeShader =
"#version 430\n"
"#extension GL_NV_bindless_texture : enable\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    struct VPL {
        vec3 P;
        vec3 N;
        vec3 L;
    };

    uniform uint uVPLCount;
    uniform VPL* uVPLBuffer;

    uniform uint uICCount;

    struct ICVertex {
        vec4 P; // Position + radius
        vec4 N; // Normal
        vec4 E; // Irradiance
    };

    uniform coherent ICVertex* uICBuffer;

    struct ISMData {
        uint smResolution;
        uint smCountPerTexture;
        uint smCountPerDimension;
    };

    uniform ISMData uISMData;
    uniform coherent sampler2D* uICISMBuffer;
    uniform coherent mat4* uICViewMatrixBuffer;

    uniform float uZNear;
    uniform float uZFar;
    uniform float uISMOffset;
    uniform float uDistClamp;

    layout(local_size_x = 1024) in;

    uint icIdx;
    ICVertex ic;

    bool ismLookup(vec3 Pws) {
        mat4 viewMatrix = uICViewMatrixBuffer[icIdx];

        vec4 Pvs = viewMatrix * vec4(Pws, 1); // Position in view space
        Pvs.z = -Pvs.z; // View the negative hemisphere

        // Paraboloid projection
        Pvs /= Pvs.w;

        vec3 v = Pvs.xyz;

        vec3 projectedPosition;
        projectedPosition.z = Pvs.z;

        float l = length(v);
        v /= l;

        vec3 h = v + vec3(0, 0, 1); // h = k * (x, y, 1)  => x = h.x / h.z   y = h.y / h.z

        float refDepth = (l - uZNear) / (uZFar - uZNear);

        if(refDepth < 0.f) {
            // The point is behind: coarse approximation
            return false;
        }

        // vProjectedPosition is in [-1, 1] x [-1, 1]
        projectedPosition.xy = h.xy / h.z;

        uint texIdx = icIdx / uISMData.smCountPerTexture;

        uint viewIdx = icIdx % uISMData.smCountPerTexture;

        // Dimension of a view in texture space
        float size = 1.f / uISMData.smCountPerDimension;

        // 2D coordinates of the viewport attached to the view
        uint i = viewIdx % uISMData.smCountPerDimension;
        uint j = (viewIdx - i) / uISMData.smCountPerDimension;

        // Coordinates between 0 and 1 in the viewport of the view
        vec2 xy = 0.5 * (projectedPosition.xy + vec2(1, 1));

        // Origin of the viewport of the view in clip space
        vec2 origin = vec2(i, j) * size;

        vec2 texCoords = origin + xy * size;

        float depth = texture(uICISMBuffer[texIdx], texCoords).r;

        return refDepth < depth + uISMOffset;
    }

    vec3 computeIrradiance(VPL vpl, out vec3 wi) {
        if(!ismLookup(vpl.P)) {
            return vec3(0.f);
        }

        wi = vpl.P - ic.P.xyz;
        float dist = length(wi);
        wi /= dist;

        float cos_o = max(0, dot(-wi, vpl.N));
        float cos_i = max(0, dot(wi, ic.N.xyz));

        dist = max(dist, uDistClamp);

        float G = cos_i * cos_o / (dist * dist);
        return G * vpl.L;
    }

    void main() {
        icIdx = uint(gl_GlobalInvocationID.x);

        if(icIdx >= uICCount) {
            return;
        }

        ic = uICBuffer[icIdx];

        vec3 irradiance = vec3(0);

        for(uint i = 0; i < uVPLCount; ++i) {
            VPL vpl = uVPLBuffer[i];

            vec3 wi;
            irradiance += computeIrradiance(vpl, wi);
        }

        uICBuffer[icIdx].E = vec4(irradiance, 0);

        memoryBarrier();
    }

);

void GLImperfectIrradianceCacheComputePass::compute(const GLVPLData& vplData,
                                                    float ismOffset, float distClamp,
                                                    GLIrradianceCachingData &icData) {
    m_Program.use();

    // Set Uniforms
    uVPLCount.set(vplData.getVPLCount());
    uICCount.set(icData.getIrradianceCacheCount());
    uVPLBuffer.set(vplData.getVPLBuffer().getGPUAddress());

    uICBuffer.set(icData.getICBuffer().getGPUAddress());

    // ISMs
    m_uISMData.set(icData.getISMContainer());

    uICISMBuffer.set(icData.getISMBuffer().getGPUAddress());
    uICViewMatrixBuffer.set(icData.getViewMatrixBuffer().getGPUAddress());

    uISMOffset.set(ismOffset);
    uDistClamp.set(distClamp);
    uZNear.set(getZNear());
    uZFar.set(getZFar());

    // Launch compute shader
    uint32_t workGroupSize = 1024;
    uint32_t workItemCount = (icData.getIrradianceCacheCount() / workGroupSize) + (icData.getIrradianceCacheCount() % workGroupSize != 0);
    glDispatchCompute(workItemCount, 1, 1);

    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}

}
