#pragma once

#include "bonez/opengl/utils/GLutils.hpp"
#include "bonez/opengl/GLISMRenderer.hpp"
#include "bonez/scene/lights/vpls.hpp"

namespace BnZ {

class GLVPLData {
    static const uint32_t ISM_RES = 128;

    GLBuffer m_VPLBuffer;
    GLISMContainer m_VPLISMContainer { ISM_RES };

    GLBuffer m_VPLISMBuffer;
    GLBuffer m_VPLViewMatrixBuffer;

    std::vector<glm::vec4> m_VPLs;
    std::vector<glm::mat4> m_VPLViewMatrices;

    uint32_t m_VPLCount = 0;
public:
    const GLBuffer& getVPLBuffer() const {
        return m_VPLBuffer;
    }

    const GLBuffer& getVPLISMBuffer() const {
        return m_VPLISMBuffer;
    }

    const GLBuffer& getVPLViewMatrixBuffer() const {
        return m_VPLViewMatrixBuffer;
    }

    const GLISMContainer& getVPLISMContainer() const {
        return m_VPLISMContainer;
    }

    uint32_t getVPLCount() const {
        return m_VPLCount;
    }

    void setVPLs(const VPLContainer& vpls, float ismNormalOffset);

    void renderISMs(GLISMRenderer& ismRenderer, const GLPoints& sampledScene,
                    float pointMaxSize, bool doPull, bool doPullPush,
                    float pullTreshold, float pushTreshold, uint32_t pullPushNbLevels);
};

}
