#pragma once

#include "bonez/opengl/shaders/GLSLProgramBuilder.hpp"
#include "bonez/opengl/utils/GLutils.hpp"
#include "bonez/opengl/GLShadowMapRenderer.hpp"
#include "GLGBuffer.hpp"

namespace BnZ {

class GLDirectLightingRenderPass {
    static const GLchar* s_VertexShader;
    static const GLchar* s_FragmentShader;

    gloops::Program m_Program = buildProgram(s_VertexShader, s_FragmentShader);

    BNZ_GLUNIFORM(m_Program, glm::vec4, uViewport);
    BNZ_GLUNIFORM(m_Program, glm::mat4, uInvProjMatrix);
    BNZ_GLUNIFORM(m_Program, GLuint64, uShadowMapSampler);
    BNZ_GLUNIFORM(m_Program, glm::vec3, uLightPosition);
    BNZ_GLUNIFORM(m_Program, glm::vec3, uLightIntensity);
    BNZ_GLUNIFORM(m_Program, glm::mat4, uLightViewMatrix);
    BNZ_GLUNIFORM(m_Program, glm::mat4[], uFaceProjectionMatrices);
    BNZ_GLUNIFORM(m_Program, glm::mat4, uRcpViewMatrix);
    BNZ_GLUNIFORM(m_Program, GLfloat, uShadowMapOffset);
    BNZ_GLUNIFORM(m_Program, GLfloat, uDistClamp);
    BNZ_GLUNIFORM(m_Program, bool, uRenderIrradiance);

    GLGBufferUniform uGBuffer { m_Program };

    glm::vec3 m_LightPosition;
    glm::vec3 m_LightIntensity;
    glm::mat4 m_LightViewMatrix;

    static const uint32_t SHADOWMAP_RES = 2048;
    GLCubeShadowMapRenderer m_ShadowMapRenderer;
    GLCubeShadowMapContainer m_ShadowMapContainer;

    bool m_bIsLightSet = false;

public:
    void setLight(glm::vec3 lightPosition, glm::vec3 lightIntensity);

    void computeShadowMap(const GLScene& scene);

    void render(const glm::mat4& rcpProjMatrix, const glm::mat4& rcpViewMatrix,
                const glm::vec4& viewport,
                float distClamp, float smOffset, bool renderIrradiance,
                GLGBuffer& gbuffer, const ScreenTriangle& triangle);
};

}
