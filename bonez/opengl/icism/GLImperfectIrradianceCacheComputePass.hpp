#pragma once

#include "bonez/opengl/shaders/GLSLProgramBuilder.hpp"
#include "bonez/opengl/utils/GLutils.hpp"
#include "bonez/opengl/GLISMRenderer.hpp"
#include "GLVPLData.hpp"
#include "GLIrradianceCachingData.hpp"

namespace BnZ {

class GLImperfectIrradianceCacheComputePass {
    static const GLchar* s_ComputeShader;

    gloops::Program m_Program { buildComputeProgram(s_ComputeShader) };

    BNZ_GLUNIFORM(m_Program, GLuint, uVPLCount);
    BNZ_GLUNIFORM(m_Program, GLuint, uICCount);
    BNZ_GLUNIFORM(m_Program, GLuint64, uVPLBuffer);

    BNZ_GLUNIFORM(m_Program, GLuint64, uICBuffer);

    GLISMRenderer::ISMUniforms m_uISMData { m_Program };

    BNZ_GLUNIFORM(m_Program, GLuint64, uICISMBuffer);
    BNZ_GLUNIFORM(m_Program, GLuint64, uICViewMatrixBuffer);
    BNZ_GLUNIFORM(m_Program, GLfloat, uZNear);
    BNZ_GLUNIFORM(m_Program, GLfloat, uZFar);
    BNZ_GLUNIFORM(m_Program, GLfloat, uISMOffset);
    BNZ_GLUNIFORM(m_Program, GLfloat, uDistClamp);

public:
    void compute(const GLVPLData& vplData,
                 float ismOffset, float distClamp,
                 GLIrradianceCachingData &icData);
};

}
