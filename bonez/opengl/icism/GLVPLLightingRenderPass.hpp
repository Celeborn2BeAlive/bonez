#pragma once

#include "bonez/opengl/shaders/GLSLProgramBuilder.hpp"
#include "bonez/opengl/utils/GLutils.hpp"
#include "GLGBuffer.hpp"
#include "GLVPLData.hpp"

namespace BnZ {

class GLVPLLightingRenderPass {
    static const GLchar* s_VertexShader;
    static const GLchar* s_FragmentShader;

    gloops::Program m_Program { buildProgram(s_VertexShader, s_FragmentShader) };

    BNZ_GLUNIFORM(m_Program, glm::vec4, uViewport);
    BNZ_GLUNIFORM(m_Program, glm::mat4, uInvProjMatrix);
    BNZ_GLUNIFORM(m_Program, unsigned int, uVPLCount);
    BNZ_GLUNIFORM(m_Program, GLuint64, uVPLBuffer);
    BNZ_GLUNIFORM(m_Program, glm::mat4, uRcpViewMatrix);
    BNZ_GLUNIFORM(m_Program, GLfloat, uDistClamp);
    GLGBufferUniform uGBuffer { m_Program };
    BNZ_GLUNIFORM(m_Program, bool, uRenderIrradiance);
    GLISMRenderer::ISMUniforms uISMData { m_Program };
    BNZ_GLUNIFORM(m_Program, GLuint64, uVPLISMBuffer);
    BNZ_GLUNIFORM(m_Program, GLuint64, uVPLViewMatrixBuffer);
    BNZ_GLUNIFORM(m_Program, GLfloat, uZNear);
    BNZ_GLUNIFORM(m_Program, GLfloat, uZFar);
    BNZ_GLUNIFORM(m_Program, GLfloat, uISMOffset);
    BNZ_GLUNIFORM(m_Program, bool, uNoShadow);

public:
    void render(const glm::mat4& rcpProjMatrix, const glm::mat4& rcpViewMatrix,
                const glm::vec4& viewport,
                float distClamp, float ismOffset, bool renderIrradiance, bool noShadow,
                const GLVPLData& vplData,
                GLGBuffer& gbuffer, const ScreenTriangle& triangle);
};

}
