#include "GLManyLODRenderPass.hpp"

namespace BnZ {

const char* GLManyLODRenderPass::s_VertexShader =
"#version 430 core\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

layout(location = 0) in uvec2 aViewNodePair;

out uvec2 vViewNodePair;

void main() {
    vViewNodePair = aViewNodePair;
}

);

const char* GLManyLODRenderPass::s_GeometryShader =
"#version 430 core\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

layout(points) in;
layout(points) out;
layout(max_vertices = 2) out;

layout(binding = 0, offset = 0) uniform atomic_uint uActiveListSize;
layout(binding = 1, offset = 0) uniform atomic_uint uInactiveListSize;

in uvec2 vViewNodePair[];

// Those LOD must be drawn
layout(stream = 0) flat out uvec2 gInactiveViewNodePair;
flat out vec3 vProjectedPosition;
flat out float vDepth;
flat out uvec2 vViewportIndex;

// Those LOD must be refined
layout(stream = 1) out uvec2 gActiveViewNodePair;

struct GPUBSHNode {
    vec4 boundingSphere;
    vec4 boundingNormalCone;
    uvec4 rightChild;
};

uniform GPUBSHNode* uBSH;
uniform float uPointSizeTreshold;
uniform float uShadowMapArea;
uniform mat4* uViewMatrices;

uniform mat4 uMVPMatrix;

struct ISMData {
    uint smResolution;
    uint smCountPerTexture;
    uint smCountPerDimension;
};

uniform ISMData uISMData;

uniform float uZNear;
uniform float uZFar;
//uniform float uPointMaxSize;

uniform bool uIsLastPass;

GPUBSHNode node;
vec3 center_vs;
float pointSize;

bool isValid() {
    mat4 viewMatrix = uViewMatrices[vViewNodePair[0].x];
    center_vs = vec3(viewMatrix * vec4(node.boundingSphere.xyz, 1));
    vec3 wi = center_vs;
    float l = length(wi);

    bool endIt = uIsLastPass || node.rightChild.x == 0;
    bool isOutsideSphere = l * l > node.boundingSphere.w;

    if(endIt || (wi.z < 0 && isOutsideSphere)) {
        wi /= l;

        float normalizedProjectedSolidAngle = 1.f;

        if(isOutsideSphere) {
            normalizedProjectedSolidAngle = (-wi.z) * (1 - cos(atan(sqrt(node.boundingSphere.w) / l)));
        }

        pointSize = sqrt(uShadowMapArea * normalizedProjectedSolidAngle);

        if(endIt) {
            return true;
        }

        return pointSize < uPointSizeTreshold;
    }

    return false;
}

uvec2 leftChild() {
    return uvec2(vViewNodePair[0].x, vViewNodePair[0].y + 1);
}

uvec2 rightChild() {
    return uvec2(vViewNodePair[0].x, node.rightChild.x);
}

void addInactive() {
    atomicCounterIncrement(uInactiveListSize);

    gInactiveViewNodePair = vViewNodePair[0];

    //vec3 aPosition = node.boundingSphere.xyz;
    uint viewIdx = vViewNodePair[0].x;

    vec4 Pvs = vec4(center_vs, 1.f);

    Pvs.z = -Pvs.z; // View the negative hemisphere

    // Paraboloid projection
    Pvs /= Pvs.w;

    vec3 v = Pvs.xyz;
    vProjectedPosition.z = Pvs.z;

    float l = length(v);
    v /= l;

    vec3 h = v + vec3(0, 0, 1); // h = k * (x, y, 1)  => x = h.x / h.z   y = h.y / h.z

    vDepth = (l - uZNear) / (uZFar - uZNear);

    // vProjectedPosition is in [-1, 1] x [-1, 1]
    vProjectedPosition.xy = h.xy / h.z;

    gl_PointSize = max(1, pointSize);

    // Dimension of a view in clip space
    float size = 2.f / uISMData.smCountPerDimension;

    // 2D coordinates of the viewport attached to the view
    uint i = viewIdx % uISMData.smCountPerDimension;
    uint j = (viewIdx - i) / uISMData.smCountPerDimension;
    vViewportIndex = uvec2(i, j);

    // Coordinates between 0 and 1 in the viewport of the view
    vec2 xy = 0.5 * (vProjectedPosition.xy + vec2(1, 1));

    // Origin of the viewport of the view in clip space
    vec2 origin = vec2(-1, -1) + vec2(i, j) * size;

    gl_Position = vec4(origin + xy * size, vDepth, 1);

    /*
    gl_Position = vec4(vProjectedPosition.xy, vDepth, 1);
    gl_ViewportIndex = int(viewIdx);*/

    EmitVertex();
    EndPrimitive();
}

void addActive(uvec2 nodeView) {
    atomicCounterIncrement(uActiveListSize);
    gActiveViewNodePair = nodeView;
    EmitStreamVertex(1);
    EndStreamPrimitive(1);
}

void main() {
    node = uBSH[vViewNodePair[0].y];
    if(isValid()) {
        addInactive();
    } else {
        addActive(leftChild());
        addActive(rightChild());
    }
}

);

const char* GLManyLODRenderPass::s_FragmentShader =
"#version 430 core\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

flat in uvec2 gActiveViewNodePair;

flat in vec3 vProjectedPosition;
flat in float vDepth;
flat in uvec2 vViewportIndex;

struct ISMData {
    uint smResolution;
    uint smCountPerTexture;
    uint smCountPerDimension;
};

uniform ISMData uISMData;

out float fDepth;

void main() {
    if(vProjectedPosition.z >= 0.f) {
        uvec2 pixel = uvec2(gl_FragCoord.xy);
        uvec2 viewportOrigin = vViewportIndex.xy * uISMData.smResolution;

        // Do not write outside the viewport
        if(pixel.x < viewportOrigin.x || pixel.x >= viewportOrigin.x + uISMData.smResolution
                || pixel.y < viewportOrigin.y || pixel.y >= viewportOrigin.y + uISMData.smResolution) {
            discard;
        } else {
            fDepth = vDepth;
        }
    } else {
        discard;
    }
}

);

gloops::Program GLManyLODRenderPass::buildProgram() {
    gloops::Shader vs(GL_VERTEX_SHADER);
    vs.setSource(s_VertexShader);
    if(!vs.compile()) {
        std::cerr << vs.getInfoLog().get() << std::endl;
        throw std::runtime_error("Vertex shader compilation error.");
    }

    gloops::Shader gs(GL_GEOMETRY_SHADER);
    gs.setSource(s_GeometryShader);
    if(!gs.compile()) {
        std::cerr << gs.getInfoLog().get() << std::endl;
        throw std::runtime_error("Geometry shader compilation error.");
    }

    gloops::Shader fs(GL_FRAGMENT_SHADER);
    fs.setSource(s_FragmentShader);
    if(!fs.compile()) {
        std::cerr << fs.getInfoLog().get() << std::endl;
        throw std::runtime_error("Fragment shader compilation error.");
    }

    gloops::Program program;
    program.attachShader(vs);
    program.attachShader(gs);
    program.attachShader(fs);

    const char *outputs[] = { "gInactiveViewNodePair", "gActiveViewNodePair" };
    glTransformFeedbackVaryings(program.glId(), 2, outputs,
                                GL_SEPARATE_ATTRIBS);

    if(!program.link()) {
        std::cerr << program.getInfoLog().get() << std::endl;
        throw std::runtime_error("Transform feedback shader link error.");
    }

    return program;
}

#define MAX_SIZE 512000000 // in bytes

GLManyLODRenderPass::GLManyLODRenderPass() {
    for(auto i = 0u; i < 2; ++i) {
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, m_TFO[i].glId());
        m_LODInactiveListBuffer.bindBase(0, GL_TRANSFORM_FEEDBACK_BUFFER); // stream 0
        m_LODActiveListBuffer[i].bindBase(1, GL_TRANSFORM_FEEDBACK_BUFFER); // stream 1

        m_LODActiveListBuffer[i].setSize(MAX_SIZE, GL_DYNAMIC_DRAW);

        m_LODActiveListVertexArray[i].enableVertexAttrib(0);
        m_LODActiveListVertexArray[i].vertexAttribIOffset(m_LODActiveListBuffer[i].glId(),
                                                          0, 2, GL_UNSIGNED_INT, 0, 0);
    }

    m_LODInactiveListBuffer.setSize(MAX_SIZE, GL_DYNAMIC_DRAW);
    m_LODInactiveListVertexArray.enableVertexAttrib(0);
    m_LODInactiveListVertexArray.vertexAttribIOffset(m_LODInactiveListBuffer.glId(),
                                                     0, 2, GL_UNSIGNED_INT, 0, 0);
}

void GLManyLODRenderPass::setPoints(const std::vector<GLPoint> &points) {
    m_BSH.build(points.data(), points.data() + points.size());
    std::vector<GPUBSHNode> nodes;
    nodes.reserve(m_BSH.getNodes().size());
    for(const auto& node: m_BSH.getNodes()) {
        nodes.emplace_back(node);
    }

    m_GPUBSH.setData(nodes, GL_STATIC_DRAW);
    m_GPUBSH.makeResident(GL_READ_ONLY);
}

void GLManyLODRenderPass::render(GLISMContainer& container,
                                 GLBuffer& viewMatrixBuffer, uint32_t viewMatrixCount,
                                 float treshold, uint32_t maxBSHLevel) {
    uint32_t inputBuffer = 0, outputBuffer = 1;
    bool isFirstTime = true;

    /*
    std::cerr << "Theoritical max size of list: " <<
                 (viewMatrixCount * m_BSH.size() * sizeof(glm::uvec2)) << std::endl;*/

    viewMatrixBuffer.makeResident(GL_READ_ONLY);

    std::vector<glm::uvec2> activeViewNodeList;
    activeViewNodeList.reserve(viewMatrixCount);
    for(auto i = 0u; i < viewMatrixCount; ++i) {
        activeViewNodeList.emplace_back(glm::uvec2(i, 0));
    }
    //std::cerr << "IC COUNT " << viewMatrixCount << std::endl;

    m_LODActiveListBuffer[0].setSubData(activeViewNodeList);

    GLBuffer activeListSizeBuffer;
    GLuint zero = 0;
    activeListSizeBuffer.setData(1, &zero, GL_STATIC_DRAW);
    activeListSizeBuffer.bindBase(0, GL_ATOMIC_COUNTER_BUFFER);

    GLBuffer inactiveListSizeBuffer;
    inactiveListSizeBuffer.setData(1, &zero, GL_STATIC_DRAW);
    inactiveListSizeBuffer.bindBase(1, GL_ATOMIC_COUNTER_BUFFER);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLint currentFBO;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentFBO);

    container.resize(viewMatrixCount);

    // Bind the framebuffer
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());

    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MIN_FILTER,
                           GL_NEAREST);
    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MAG_FILTER,
                           GL_NEAREST);

    glTextureImage2DEXT(m_DepthBuffer.glId(),
                        GL_TEXTURE_2D,
                        0,
                        GL_DEPTH_COMPONENT32F,
                        container.getTextureResolution(),
                        container.getTextureResolution(),
                        0,
                        GL_DEPTH_COMPONENT,
                        GL_FLOAT,
                        nullptr);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, m_DepthBuffer.glId(), 0);

    GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &drawBuffer);

    glEnable(GL_PROGRAM_POINT_SIZE);

    glClearColor(1, 1, 1, 1);
    glViewport(0, 0, container.getTextureResolution(), container.getTextureResolution());

/*
    GLint x;
    glGetIntegerv(GL_MAX_VIEWPORTS, &x);
    std::cerr << "MAX VIEWPORT = " << x << std::endl;

    std::vector<glm::vec4> viewports;
    viewports.reserve(viewMatrixCount);

    for(auto viewIdx = 0u; viewIdx < viewMatrixCount; ++viewIdx) {
        uint i = viewIdx % container.getShadowMapCountPerDimension();
        uint j = (viewIdx - i) / container.getShadowMapCountPerDimension();
        auto w = container.getShadowMapResolution();
        viewports.emplace_back(glm::vec4(i * w, j * w, w, w));
    }

    glViewportArrayv(0, viewports.size(), glm::value_ptr(viewports[0]));
*/
    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_2D, container.getTexture(0).glId(), 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    int32_t maxTreeLevel = maxBSHLevel;

    // BUG: en diminuant le critère, certaines vues n'ont pas de points ??

    {
        m_Program.use();
        uActiveListSize.set(0);
        uInactiveListSize.set(1);
        uBSH.set(m_GPUBSH.getGPUAddress());
        uViewMatrices.set(viewMatrixBuffer.getGPUAddress());
        uPointSizeTreshold.set(treshold);

        uISMData.set(container);
        uZFar.set(getZFar());
        uZNear.set(getZNear());

        float area = (float(embree::pi) / 4.f) * container.getShadowMapResolution() *
                container.getShadowMapResolution();
        uShadowMapArea.set(area);

        while(maxTreeLevel-- > 0) {
            uIsLastPass.set(maxTreeLevel == 0);

            m_LODActiveListVertexArray[inputBuffer].bind();
            glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, m_TFO[outputBuffer].glId());

            glBeginTransformFeedback(GL_POINTS);

            if(isFirstTime) {
                glDrawArrays(GL_POINTS, 0, viewMatrixCount);
                isFirstTime = false;
            } else {
                glDrawTransformFeedbackStream(GL_POINTS, m_TFO[inputBuffer].glId(), 1);
            }

            glEndTransformFeedback();

            std::swap(inputBuffer, outputBuffer);
        }

        glBindVertexArray(0);
    }

    glClearColor(0, 0, 0, 0);

    glDisable(GL_PROGRAM_POINT_SIZE);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, currentFBO);
}


std::vector<uint32_t> GLManyLODRenderPass::render(GLISMContainer& container,
                             GLBuffer& viewMatrixBuffer, uint32_t viewMatrixCount,
                             float treshold, uint32_t viewIndex, uint32_t maxBSHLevel) {
    std::vector<uint32_t> finalList;

    uint32_t inputBuffer = 0, outputBuffer = 1;
    bool isFirstTime = true;

    viewMatrixBuffer.makeResident(GL_READ_ONLY);

    glm::uvec2 viewNode(viewIndex, 0);

    m_LODActiveListBuffer[0].setSubData(0, 1, &viewNode);

    GLBuffer activeListSizeBuffer;
    GLuint zero = 0;
    activeListSizeBuffer.setData(1, &zero, GL_STATIC_DRAW);
    activeListSizeBuffer.bindBase(0, GL_ATOMIC_COUNTER_BUFFER);

    GLBuffer inactiveListSizeBuffer;
    inactiveListSizeBuffer.setData(1, &zero, GL_STATIC_DRAW);
    inactiveListSizeBuffer.bindBase(1, GL_ATOMIC_COUNTER_BUFFER);

    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLint currentFBO;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentFBO);

    container.resize(viewMatrixCount);

    // Bind the framebuffer
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());

    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MIN_FILTER,
                           GL_NEAREST);
    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MAG_FILTER,
                           GL_NEAREST);

    glTextureImage2DEXT(m_DepthBuffer.glId(),
                        GL_TEXTURE_2D,
                        0,
                        GL_DEPTH_COMPONENT32F,
                        container.getTextureResolution(),
                        container.getTextureResolution(),
                        0,
                        GL_DEPTH_COMPONENT,
                        GL_FLOAT,
                        nullptr);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, m_DepthBuffer.glId(), 0);

    GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &drawBuffer);

    glEnable(GL_PROGRAM_POINT_SIZE);

    glClearColor(1, 1, 1, 1);
    uint32_t viewportResolution = container.getShadowMapCountPerDimension() * container.getShadowMapResolution();
    glViewport(0, 0, viewportResolution, viewportResolution);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_2D, container.getTexture(0).glId(), 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    int32_t maxTreeLevel = maxBSHLevel;

    // BUG: en diminuant le critère, certaines vues n'ont pas de points ??

    {
        m_Program.use();
        uActiveListSize.set(0);
        uInactiveListSize.set(1);
        uBSH.set(m_GPUBSH.getGPUAddress());
        uViewMatrices.set(viewMatrixBuffer.getGPUAddress());
        uPointSizeTreshold.set(treshold);

        uISMData.set(container);
        uZFar.set(getZFar());
        uZNear.set(getZNear());

        float area = (float(embree::pi) / 4.f) * container.getShadowMapResolution() *
                container.getShadowMapResolution();
        uShadowMapArea.set(area);

        //GLuint activeListSize = viewMatrixCount;

        while(maxTreeLevel-- > 0) {
            uIsLastPass.set(maxTreeLevel == 0);

            m_LODActiveListVertexArray[inputBuffer].bind();
            glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, m_TFO[outputBuffer].glId());

            glBeginTransformFeedback(GL_POINTS);

            if(isFirstTime) {
                glDrawArrays(GL_POINTS, 0, 1);
                isFirstTime = false;
            } else {
                glDrawTransformFeedbackStream(GL_POINTS, m_TFO[inputBuffer].glId(), 1);
            }

            glEndTransformFeedback();

            std::swap(inputBuffer, outputBuffer);

            uint32_t inactiveListSize;
            inactiveListSizeBuffer.getData(1, &inactiveListSize);
            std::cerr << "ILS = " << inactiveListSize << std::endl;

            inactiveListSizeBuffer.setSubData(0, 1, &zero);

            std::vector<glm::ivec2> inactiveList(inactiveListSize);
            m_LODInactiveListBuffer.getData(inactiveListSize, inactiveList.data());
            for(const auto& viewNode: inactiveList) {
                finalList.emplace_back(viewNode.y);
            }
        }

        glBindVertexArray(0);
    }

    glClearColor(0, 0, 0, 0);

    glDisable(GL_PROGRAM_POINT_SIZE);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, currentFBO);

    return finalList;
}

}
