#include "GLIrradianceCacheISMRenderer.hpp"

#include <memory>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/random.hpp>

#include "bonez/opengl/utils/embree_opengl.hpp"

#include "bonez/common/Progress.hpp"
#include "bonez/common/sampling/Sampler.hpp"

#include "bonez/common/sampling/StratifiedSampler.hpp"
#include "bonez/common/cameras/ParaboloidCamera.hpp"
#include "bonez/common/sampling/patterns.hpp"

#include "bonez/common/data/KdTree.hpp"

#include <stack>

namespace BnZ {

using namespace gloops;

const GLchar* GLIrradianceCacheISMRenderer::DrawBuffersPassData::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    void main() {
        gl_Position = vec4(aPosition, 0, 1);
    }
);

const GLchar* GLIrradianceCacheISMRenderer::DrawBuffersPassData::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(
    uniform vec4 uViewport;

    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
    };

    uniform GBuffer uGBuffer;

    uniform usampler2D uGeometryClusterSampler;

    uniform uint* uClusterTilesOffsetsBuffer;
    //uniform vec3* bClusterBBoxLowerBuffer;
    //uniform vec3* bClusterBBoxUpperBuffer;
    uniform uint* uClusterToTileBuffer;

    layout(std430, binding = 3) buffer ClusterBBoxLowerBuffer {
        vec3 bClusterBBoxLowerBuffer[];
    };

    layout(std430, binding = 4) buffer ClusterBBoxUpperBuffer {
        vec3 bClusterBBoxUpperBuffer[];
    };

    layout(std430, binding = 5) buffer ClusterNormalBuffer {
        vec3 bClusterNormalBuffer[];
    };

    uniform float uZFar;
    uniform uvec2 uTileCount;

    uniform int uDataToDisplay;

    out vec3 fFragColor;

    vec3 randomColor(uint value) {
        return fract(
                    sin(
                        vec3(float(value) * 12.9898,
                             float(value) * 78.233,
                             float(value) * 56.128)
                        )
                    * 43758.5453
                    );
    }

    void main() {
        vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

        ivec2 tileCoords = ivec2(texCoords * vec2(textureSize(uGeometryClusterSampler, 0).xy));
        tileCoords /= 32;

        uint tileIdx = tileCoords.x + tileCoords.y * uTileCount.x;

        if(uDataToDisplay == 0) {
            fFragColor = texture(uGBuffer.normalDepthSampler, texCoords).rgb;
        } else if(uDataToDisplay == 1) {
            fFragColor = vec3(texture(uGBuffer.normalDepthSampler, texCoords).a);
        } else if(uDataToDisplay == 2) {
            fFragColor = texture(uGBuffer.diffuseSampler, texCoords).rgb;
        } else if(uDataToDisplay == 3) {
            fFragColor = texture(uGBuffer.glossyShininessSampler, texCoords).rgb;
        } else if(uDataToDisplay == 4) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            fFragColor = randomColor(uClusterTilesOffsetsBuffer[tileIdx] + geometryCluster);
        } else if (uDataToDisplay == 5) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            vec3 bboxLower = bClusterBBoxLowerBuffer[tileIdx * 1024 + geometryCluster];
            fFragColor = vec3(-bboxLower.z / uZFar);
        } else if (uDataToDisplay == 6) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            vec3 bboxUpper = bClusterBBoxUpperBuffer[tileIdx * 1024 + geometryCluster];
            fFragColor = vec3(-bboxUpper.z / uZFar);
        } else if (uDataToDisplay == 7) {
            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
            vec3 normal = bClusterNormalBuffer[tileIdx * 1024 + geometryCluster];
            fFragColor = normal;

//            int geometryCluster = int(texture(uGeometryClusterSampler, texCoords).r);
//            uint globalID = uClusterTilesOffsetsBuffer[tileIdx] + uint(geometryCluster);
//            fFragColor = randomColor(uClusterToTileBuffer[globalID]);
        }
    }
);

GLIrradianceCacheISMRenderer::DrawBuffersPassData::DrawBuffersPassData():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uViewport(m_Program, "uViewport", true),
    uGBuffer(m_Program),
    m_uGeometryClusterSampler(m_Program, "uGeometryClusterSampler", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uDataToDisplay(m_Program, "uDataToDisplay", true),
    m_uTileCount(m_Program, "uTileCount", true),
    m_uClusterTilesOffsetsBuffer(m_Program, "uClusterTilesOffsetsBuffer", true),
    //m_uClusterBBoxLowerBuffer(m_Program, "bClusterBBoxLowerBuffer", true),
    //m_uClusterBBoxUpperBuffer(m_Program, "bClusterBBoxUpperBuffer", true),
    m_uClusterToTileBuffer(m_Program, "uClusterToTileBuffer", true) {
}

void GLIrradianceCacheISMRenderer::setResolution(size_t width, size_t height) {
    m_GBuffer.init(width, height);
    m_IrradianceCacheLightingRenderPass.setResolution(width, height);
    m_GeometryClustersData.init(width, height);

}

void GLIrradianceCacheISMRenderer::setUp() {

}

static std::vector<uint32_t> kMeanClustering(const std::vector<GLPoint>& points, uint32_t K,
                                             std::vector<GLPoint>& centroids) {
    Random rng;
    Progress progress;

    progress.init("kMeanClustering", 1);

    std::vector<uint32_t> counts;

    for(auto i = 0u; i < K; ++i) {
        uint32_t randomIdx = glm::min(uint32_t(points.size() - 1), uint32_t(rng.getFloat() * points.size()));
        centroids.emplace_back(points[randomIdx]);
        counts.emplace_back(0u);
    }

    auto positionFunctor = [&](uint32_t i) -> Vec3f {
        return convert(centroids[i].position);
    };

    std::vector<uint32_t> pointClusters(points.size(), 0);

    bool hasChange = true;
    uint32_t Nmax = 1;
    uint32_t n = 0;
    while(hasChange && n < Nmax) {
        KdTree kdtree;
        kdtree.build(centroids.size(), positionFunctor);

        parallelProcess(points.size(), [&](uint32_t taskID) {
            float dist;
            pointClusters[taskID] = kdtree.searchNearestNeighbour(convert(points[taskID].position), dist);
        });

        parallelProcess(centroids.size(), [&](uint32_t taskID) {
            centroids[taskID] =  GLPoint { glm::vec3(0.f), glm::vec3(0.f) };
            counts[taskID] = 0;
        });

        for(auto i = 0u; i < points.size(); ++i) {
            auto cluster = pointClusters[i];
            centroids[cluster].position += points[i].position;
            centroids[cluster].normal += points[i].normal;
            counts[cluster] += 1;
        }

        parallelProcess(centroids.size(), [&](uint32_t taskID) {
            auto c = counts[taskID];
            if(c != 0) {
                centroids[taskID].position /= c;
                centroids[taskID].normal /= c;
            } else {
                centroids[taskID].position = glm::vec3(0.f);
                centroids[taskID].normal = glm::vec3(0.f);
            }
        });

        ++n;
    }

    progress.end();

    return pointClusters;
}

void GLIrradianceCacheISMRenderer::sampleScene(const Scene& scene, const Camera& camera) {
    Progress progress;
    progress.init("Scene sampling", 1);

    std::vector<GLPoint> samples;

    uint32_t count = m_nSceneSampleCount;

    /*if(!m_bUseCoherentIrradianceCache) {
        count = m_nSceneSampleCount * m_GeometryClustersData.m_TileCount.x * m_GeometryClustersData.m_TileCount.y;
    }*/

    if(m_nSceneSamplingMode == 0) {
        Sampler sampler;
        for(auto i = 0u; i < count; ++i) {
            SurfacePointRandom rdm(sampler.get1DSample(), sampler.get1DSample(),
                                   sampler.get1DSample(), sampler.get2DSample());
            SurfacePointSample s = scene.geometry.sampleSurfacePoint(rdm);
            samples.emplace_back(convert(s.value.P), convert(s.value.Ns));
        }
    } else {
        float ratio = m_GBuffer.getWidth() / float(m_GBuffer.getHeight());
        uint32_t sppY = embree::ceil(embree::sqrt(count / ratio));
        embree::Random rng;
        StratifiedSampler sampler(sppY * ratio, sppY);
        sampler.init(0, 0, 0, 1, 1);
        IntegratorSample sample;
        while(sampler.getNextSample(sample)) {
            Ray ray = camera.ray(sample.getRasterPosition());
            Intersection I = intersect(ray, scene);
            if(I) {
                Vec2f hemisphereSample;
                createJittered2DPattern(&hemisphereSample, 1, 1, rng);
                Sample3f wo = embree::uniformSampleHemisphere(hemisphereSample.x, hemisphereSample.y, I.Ns);
                Intersection I2 = intersect(secondary_ray(I, wo), scene);
                if(I2) {
                    samples.emplace_back(convert(I2.P), convert(I2.Ns));
                }
            }
        }
    }

    progress.end();

    auto getPosition = [&](uint32_t idx) -> Vec3f { return convert(samples[idx].position); };

    /*KdTree kdTree;
    kdTree.build(samples.size(), getPosition);*/

    for(auto i = 0u; i < samples.size(); ++i) {
        auto P = convert(samples[i].position);
        float distSquared;
        /*uint32_t nearest = kdTree.searchNearestNeighbour(P, distSquared,
            [&](uint32_t j) -> bool { return i != j; });*/
        samples[i].sqrRadius = 100.f;
    }



    // UTILISER QUANTIF PLUTOT QUE K-MEAN CLUSTERING ??

    size_t K = m_nPointClusterCount;
    std::cerr << "K = " << K << std::endl;
    m_ClusterCentroids.clear();
    auto pointClusters = kMeanClustering(samples, K, m_ClusterCentroids);
    std::vector<glm::vec3> clusterColors;
    for(auto i = 0u; i < K; ++i) {
        clusterColors.emplace_back(glm::abs(glm::sphericalRand(1.f)));
    }

    std::vector<glm::vec3> colors;
    for(auto cluster: pointClusters) {
        colors.emplace_back(clusterColors[cluster]);
    }

    std::vector<std::vector<uint32_t>> clusters(K);
    for(auto i = 0u; i < pointClusters.size(); ++i) {
        clusters[pointClusters[i]].emplace_back(i);
    }

    std::swap(m_SceneSamples, samples);
    std::swap(m_SceneSamplesColors, colors);
    std::swap(m_SceneSampleClusters, pointClusters);
    std::swap(m_Clusters, clusters);

    std::cerr << m_SceneSamples.size() << std::endl;

    m_NewSceneSamples = m_SceneSamples;

    m_SampledScene.fill(m_SceneSamples.size(), m_SceneSamples.data());
    m_SampledScene.setColors(m_SceneSamplesColors.data());

    m_pScene = &scene;

    progress.init("BSH building", 1);
    m_ManyLODRenderPass.setPoints(m_NewSceneSamples);
    progress.end();

    computeSamplesFromBSH();
}


void GLIrradianceCacheISMRenderer::geometryPass(const GLScene& scene) {
    {
        GLTimer timer(m_Timings.m_GeometryPassQuery);
        m_GBufferRenderPass.render(m_ProjectionMatrix, m_ViewMatrix, scene, m_GBuffer);
    }

    {
        GLTimer timer(m_Timings.m_ClusteredShadingPassQuery);
        m_GLClusteredShadingBufferRenderer.render(m_RcpProjMatrix, m_RcpViewMatrix,
                                                  m_GBuffer, m_GeometryClustersData, m_bUseNormalClustering);
    }

    computeIrradianceCachesFromGeometryClusters();
}

void GLIrradianceCacheISMRenderer::computeVPLData(const VPLContainer& vpls) {
    m_VPLData.setVPLs(vpls, m_fISMNormalOffset);

    GLTimer timer(m_Timings.m_ComputeVPLISMsQuery);

    if(!m_bDoVPLShadingPass || !m_bUseVPLISMs) {
        return;
    }

    m_SampledScene.fill(m_SceneSamples.size(), &m_SceneSamples[0]);
    m_SampledScene.setColors(&m_SceneSamplesColors[0]);

    m_VPLData.renderISMs(m_ISMRenderer, m_SampledScene, m_fISMPointMaxSize, m_bDoPull, m_bDoPullPush,
                         m_fPullTreshold, m_fPushTreshold, m_nPullPushNbLevels);
}

void GLIrradianceCacheISMRenderer::computeIrradianceCachingData() {
    {
        GLTimer timer(m_Timings.m_ComputeICISMsQuery);

        auto ICCount = m_IrradianceCacheData.getIrradianceCacheCount();

        /*
         * WITH KDTREE + CLUSTERING SELECTION OF POINTS*/
//        {
//            Progress p;
//            p.init("Resampling for each cluster", 1);

//            std::vector<GLPoint> newSamples(ICCount * m_nPointCountPerIC);

//            parallelProcess(ICCount, [&](uint32_t taskID) {
//                Random rng;
//                DiscreteDistribution distrib;

//                for(auto j = 0u; j < m_Clusters.size(); ++j) {
//                    glm::vec3 wi = m_ClusterCentroids[j].position -
//                            glm::vec3(m_IrradianceCacheData.getCPUBuffer()[taskID].positionRadius);
//                    float d = glm::length(wi);
//                    wi /= d;
//                    float solidAngle =
//                            glm::max(0.f, glm::dot(wi, glm::vec3(m_IrradianceCacheData.getCPUBuffer()[taskID].normal)))
//                            * glm::max(0.f, glm::dot(-wi, m_ClusterCentroids[j].normal)) / (d * d);
//                    distrib.add(solidAngle);
//                }

//                auto offset = taskID * m_nPointCountPerIC;
//                auto end = offset + m_nPointCountPerIC;
//                for(auto n = offset; n < end; ++n) {
//                    float r = rng.getFloat(); // pb
//                    auto s = distrib.sample(r);

//                    if(s.pdf == 0.f) {
//                        s.value = 0;
//                    }

//                    uint32_t idx = s.value;

//                    float r2 = rng.getFloat();
//                    uint32_t idx2 = embree::min(m_Clusters[idx].size() - 1, size_t(m_Clusters[idx].size() * r2));

//                    if(n >= newSamples.size()) {
//                        std::cerr << n << " 1/ " << newSamples.size() << std::endl;
//                    }

//                    if(idx >= m_Clusters.size()) {
//                        std::cerr << r << std::endl;
//                        std::cerr << distrib.size() << std::endl;
//                        std::cerr << idx << " 2/ " << m_Clusters.size() << std::endl;
//                    }

//                    if(idx2 >= m_Clusters[idx].size()) {
//                        std::cerr << idx2 << " 3/ " << m_Clusters[idx].size() << std::endl;
//                    }

//                    if(m_Clusters[idx][idx2] >= m_SceneSamples.size()) {
//                        std::cerr << m_Clusters[idx][idx2] << " 4/ " << m_SceneSamples.size() << std::endl;
//                    }

//                    newSamples[n] = m_SceneSamples[m_Clusters[idx][idx2]];
//                }
//            });

//            std::swap(m_NewSceneSamples, newSamples);

//            p.end();
//        }

//        if(m_bDoICShadingPass && !m_bUseCPUIrradianceCache) {
//            GLPoints points;
//            points.fill(m_NewSceneSamples.size(), &m_NewSceneSamples[0]);

//            m_IrradianceCacheData.renderISMsPacks(m_ISMRenderer, points, m_nPointCountPerIC, m_fISMPointMaxSize, m_bDoPull,
//                                              m_bDoPullPush, m_fPullTreshold, m_fPushTreshold,
//                                              m_nPullPushNbLevels);
//        }



        /*
         * WITH MANYLOD
         */
        if(m_bDoICShadingPass && !m_bUseCPUIrradianceCache) {
            m_IrradianceCacheData.renderISMsManyLOD(m_ISMRenderer, m_ManyLODRenderPass, m_fManyLODTreshold, m_BSHLevel, m_bDoPull,
                                                    m_bDoPullPush, m_fPullTreshold, m_fPushTreshold,
                                                    m_nPullPushNbLevels);
        } else if(m_bDoICShadingPass && m_bUseCPUIrradianceCache) {
            computeCPUIrradianceCaches();
        }


        /*
         *
         *SIMPLE
         */
//        m_SampledScene.fill(m_SceneSamples.size(), &m_SceneSamples[0]);
//        m_SampledScene.setColors(&m_SceneSamplesColors[0]);

//        if(m_bDoICShadingPass && !m_bUseCPUIrradianceCache) {
//            m_IrradianceCacheData.renderISMs(m_ISMRenderer, m_SampledScene, m_fISMPointMaxSize, m_bDoPull,
//                                              m_bDoPullPush, m_fPullTreshold, m_fPushTreshold,
//                                              m_nPullPushNbLevels, m_bUseCoherentIrradianceCache);
//        }
    }

    {
        GLTimer timer(m_Timings.m_ImperfectICPassQuery);

        if(m_bDoICShadingPass && !m_bUseCPUIrradianceCache) {
            m_ImperfectIrradianceCacheComputePass.compute(m_VPLData, m_fISMOffset, m_fDistClamp,
                                                          m_IrradianceCacheData);
        }
    }
}

void GLIrradianceCacheISMRenderer::processPixel(int pixelX, int pixelY) {
    auto w = m_GBuffer.getWidth();
    auto h = m_GBuffer.getHeight();

    std::vector<uint32_t> img(m_GBuffer.getWidth() * m_GBuffer.getHeight());
    m_GeometryClustersData.m_GeometryClustersTexture.getImage(0, GL_RED_INTEGER, GL_UNSIGNED_INT, img.data());

    std::vector<GLuint> clusterOffset(m_GeometryClustersData.m_TileCount.x * m_GeometryClustersData.m_TileCount.y);
    m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.makeNonResident();
    m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.getData(clusterOffset.size(), clusterOffset.data());
    m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.makeResident(GL_READ_WRITE);

    auto tile = glm::uvec2(pixelX, pixelY) / TILE_SIZE;
    auto localClusterIdx = img[pixelX + pixelY * w];
    auto globalClusterIdx = clusterOffset[tile.x + tile.y * m_GeometryClustersData.m_TileCount.x] + localClusterIdx;

    m_nSelectedISMIndex = globalClusterIdx;

    getClusterCache(tile.x + tile.y * m_GeometryClustersData.m_TileCount.x, localClusterIdx, m_SelectedCacheP, m_SelectedCacheN);
    m_nFrameIdx = 0;

    m_SelectedBSHCut = m_ManyLODRenderPass.render(m_IrradianceCacheData.getISMContainer(),
                                                  m_IrradianceCacheData.getViewMatrixBuffer(), m_IrradianceCacheData.getICCount(),
                                                  m_fManyLODTreshold, m_nSelectedISMIndex, m_BSHLevel);
}

void GLIrradianceCacheISMRenderer::computeSamplesFromBSH() {
    std::vector<GLPoint> points;
    std::vector<glm::vec3> colors;

    glm::vec3 color;

    m_ManyLODRenderPass.getBSH().depthFirstTraversal([&](uint32_t nodeIdx, const GLManyLODBSH::Node& node, uint32_t level) -> bool {
        if(level == m_BSHLevel) {
            color = uintToColor(nodeIdx);
        }
        if(node.isLeaf()) {
            points.emplace_back(node.sphere.center, node.normal);
            colors.emplace_back(color);
        }
        return true;
    });

    m_SampledScene.fill(points.size(), points.data());
    m_SampledScene.setColors(colors.data());
}

void GLIrradianceCacheISMRenderer::displayData(GLVisualDataRenderer& renderer, const Scene& scene) {
    renderer.setUp();
    const auto& ics = m_IrradianceCacheData.getCPUBuffer();
    for(const auto ic: ics) {
        renderer.renderArrow(
                    convert(ic.positionRadius),
                    convert(ic.normal),
                    Col3f(1, 0, 0), 10.f);
    }

    if(m_nSelectedISMIndex >= 0) {
        renderer.setUp();

        renderer.renderArrow(
                    convert(m_SelectedCacheP),
                    convert(m_SelectedCacheN),
                    Col3f(1, 0, 0), 10.f);


        const auto& nodes = m_ManyLODRenderPass.getBSH().getNodes();
        for(auto i: m_SelectedBSHCut) {
            auto color = uintToColor(i);
            auto c = Col3f(color.r, color.g, color.b);
            renderer.renderCircle(convert(nodes[i].sphere.center), glm::sqrt(nodes[i].sphere.sqrRadius),
                                  c, 2.f);

            if(nodes[i].isLeaf()) {
                auto color = uintToColor(i);
                auto c = Col3f(color.r, color.g, color.b);
                renderer.renderArrow(
                            convert(nodes[i].sphere.center),
                            convert(nodes[i].normal),
                            c, 10.f);
                /*renderer.renderLine(convert(nodes[i].sphere.center),
                                    convert(m_SelectedCacheP), c, c, 1.f);*/
            }
        }
    } else {
        /*
        renderer.setUp();
        //renderer.renderPoints(m_SampledScene);

        for(const auto& n: m_ManyLODRenderPass.getBSH().getNodes()) {
            if(n.isLeaf()) {
                renderer.renderArrow(
                            convert(n.sphere.center),
                            convert(n.normal),
                            Col3f(1, 0, 0), glm::sqrt(n.sphere.sqrRadius));
            }
        }

        m_ManyLODRenderPass.getBSH().depthFirstTraversal([&](uint32_t nodeIdx, const GLManyLODBSH::Node& node, uint32_t level) -> bool {
            if(!node.isLeaf() && level == m_BSHLevel) {
                auto color = uintToColor(nodeIdx);
                renderer.renderCircle(convert(node.sphere.center), glm::sqrt(node.sphere.sqrRadius),
                                      Col3f(color.r, color.g, color.b), 2.f);
                return false;
            }
            return true;
        });*/
    }
}

void GLIrradianceCacheISMRenderer::shadingPass(int x, int y, size_t width, size_t height,
    const VPLContainer& vpls, const Scene& scene, const Camera& camera) {

    sampleScene(scene, camera);

    m_VPLContainer = vpls;

    computeVPLData(m_VPLContainer);
    computeIrradianceCachingData();

    auto depthTest = pushGLState<GL_DEPTH_TEST>();
    depthTest.set(false);

    auto blend = pushGLState<GL_BLEND>();
    blend.set(true);

    auto viewport = pushGLState<GL_VIEWPORT>();
    viewport.set(glm::vec4(x, y, width, height));

    glClear(GL_COLOR_BUFFER_BIT);

    glBlendFunc(GL_ONE, GL_ONE);
    glBlendEquation(GL_FUNC_ADD);

    vplShadingPass(x, y, width, height);

    icShadingPass(x, y, width, height);

    directLightShadingPass(x, y, width, height);
}

void GLIrradianceCacheISMRenderer::vplShadingPass(int x, int y, size_t width, size_t height) {
    GLTimer timer(m_Timings.m_VPLLightingPassQuery);

    if(m_bDoVPLShadingPass) {
        m_VPLLightingRenderPass.render(m_RcpProjMatrix, m_RcpViewMatrix, glm::vec4(x, y, width, height),
                                       m_fDistClamp, m_fISMOffset, m_bRenderIrradiance, !m_bUseVPLISMs,
                                       m_VPLData, m_GBuffer, m_ScreenTriangle);
    }
}

void GLIrradianceCacheISMRenderer::icShadingPass(int x, int y, size_t width, size_t height) {

    GLTimer timer(m_Timings.m_ICLightingPassQuery);

    if(m_bDoICShadingPass) {
        m_IrradianceCacheLightingRenderPass.render(m_ProjectionMatrix, m_ViewMatrix,
                                                   m_RcpProjMatrix, m_RcpViewMatrix,
                                                   glm::vec4(x, y, width, height), m_bRenderIrradiance,
                                                   m_GBuffer, m_IrradianceCacheData, m_ScreenTriangle);
    }
}

void GLIrradianceCacheISMRenderer::directLightShadingPass(int x, int y, size_t width, size_t height) {
    GLTimer timer(m_Timings.m_DirectLightingPassQuery);

    if(m_bDoDirectLightPass) {
        m_DirectLightingRenderPass.render(m_RcpProjMatrix, m_RcpViewMatrix, glm::vec4(x, y, width, height),
                                          m_fDistClamp, m_fShadowMapOffset, m_bRenderIrradiance, m_GBuffer, m_ScreenTriangle);
    }
}

void GLIrradianceCacheISMRenderer::getClusterCache(uint32_t tileIdx, uint32_t clusterLocalIdx, glm::vec3& P, glm::vec3& N) {
    auto width = m_GBuffer.getWidth();
    auto height = m_GBuffer.getHeight();
    auto imageSize = width * height;

    // Compute input buffers
    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<glm::vec4> normals(imageSize);

    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());
    m_GeometryClustersData.m_ClusterNormalBuffer.getData(normals.size(), normals.data());

    uint32_t offset = 1024 * tileIdx;
    uint32_t clusterGlobalIdx = offset + clusterLocalIdx;

    glm::vec3 upper = glm::vec3(bboxUpper[clusterGlobalIdx]), lower = glm::vec3(bboxLower[clusterGlobalIdx]);
    N = glm::vec3(normals[clusterGlobalIdx]);
    P = 0.5f * (upper + lower);
}

void GLIrradianceCacheISMRenderer::computeCPUIrradianceCaches() {
    Progress progress;
    progress.init("Compute CPU Irradiance Caches", 1);

    if(!m_bDoICShadingPass) {
        return;
    }

    std::vector<GLIrradianceCachingData::ICVertex> data(m_IrradianceCacheData.getICCount());

    parallelProcess(m_IrradianceCacheData.getICCount(),
                    [&](uint32_t taskID) {
        Col3f irradiance(0.f);
        auto ic = m_IrradianceCacheData.getCPUBuffer()[taskID];
        RayCounter rc;
        for(const auto& vpl: m_VPLContainer.orientedVPLs) {
            Vec3f wi = vpl.P - convert(glm::vec3(ic.positionRadius));
            float dist = length(wi);
            wi /= dist;

            if(!occluded(shadow_ray(SurfacePoint(convert(glm::vec3(ic.positionRadius)), convert(ic.normal)), wi, dist), *m_pScene, rc)) {
                float cos_o = embree::max(0.f, dot(-wi, vpl.N));
                float cos_i = embree::max(0.f, dot(wi, convert(ic.normal)));

                dist = embree::max(dist, m_fDistClamp);

                float G = cos_i * cos_o / (dist * dist);
                irradiance += G * vpl.L;
            }
        }

        ic.irradiance = glm::vec4(irradiance.r, irradiance.g, irradiance.b, 0);
        data[taskID] = ic;
    });

    m_IrradianceCacheData.setData(data);

    progress.end();
}

void GLIrradianceCacheISMRenderer::computeIrradianceCachesFromGeometryClusters() {
    auto width = m_GBuffer.getWidth();
    auto height = m_GBuffer.getHeight();
    auto imageSize = width * height;
    auto tileCount = m_GeometryClustersData.m_TileCount.x * m_GeometryClustersData.m_TileCount.y;
/*
    // Compute input buffers
    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<glm::vec4> normals(imageSize);
    std::vector<GLuint> clusterCount(tileCount);

    m_GeometryClustersData.m_GClusterCountsBuffer.getData(clusterCount.size(), clusterCount.data());
    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());
    m_GeometryClustersData.m_ClusterNormalBuffer.getData(normals.size(), normals.data());

    std::vector<GLIrradianceCachingData::ICVertex> vertexBuffer;
    std::vector<glm::mat4> viewMatrices;

    uint32_t clusterGlobalIdx = 0;
    for(auto pair: index(clusterCount)) {
        GLuint offset = 1024 * pair.first;
        for(auto i: range(pair.second)) {
            glm::vec3 upper = glm::vec3(bboxUpper[offset + i]), lower = glm::vec3(bboxLower[offset + i]);
            float radius = m_fSplatRadiusFactor * glm::length(upper - lower) * 0.5f;
            glm::vec3 normal = glm::vec3(normals[offset + i]);
            glm::vec3 center = 0.5f * (upper + lower);

            glm::vec3 eye = center + m_fISMNormalOffset * normal;
            glm::vec3 point = eye + normal;
            glm::vec3 up;
            if(normal.x != 0) {
                up = glm::vec3(-(normal.y + normal.z) / normal.x, 1, 1);
            } else if(normal.y != 0) {
                up = glm::vec3(1, -(normal.x + normal.z) / normal.y, 1);
            } else {
                up = glm::vec3(1, 1, -(normal.x + normal.y) / normal.z);
            }

            viewMatrices.emplace_back(glm::lookAt(eye, point, up));

            GLIrradianceCachingData::ICVertex v{ glm::vec4(eye, radius), glm::vec4(normal, 0), glm::vec4(0.f) };

            vertexBuffer.emplace_back(v);
            ++clusterGlobalIdx;
        }
    }

    m_IrradianceCacheData.setData(vertexBuffer);
    m_IrradianceCacheData.setViewMatrices(viewMatrices);
*/

    std::vector<glm::vec4> bboxUpper(imageSize), bboxLower(imageSize);
    std::vector<glm::vec4> repPos(imageSize), repNormal(imageSize);
    std::vector<GLuint> clusterCount(tileCount);

    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.getData(bboxLower.size(), bboxLower.data());
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.getData(bboxUpper.size(), bboxUpper.data());
    m_GeometryClustersData.m_GClusterCountsBuffer.getData(clusterCount.size(), clusterCount.data());
    m_GeometryClustersData.m_ClusterRepPositionBuffer.getData(repPos.size(), repPos.data());
    m_GeometryClustersData.m_ClusterRepNormalBuffer.getData(repNormal.size(), repNormal.data());

    std::vector<GLIrradianceCachingData::ICVertex> vertexBuffer;
    std::vector<glm::mat4> viewMatrices;

    uint32_t clusterGlobalIdx = 0;
    for(auto pair: index(clusterCount)) {
        GLuint offset = 1024 * pair.first;
        for(auto i: range(pair.second)) {

            if((offset + i) % 3 == 0) {
                glm::vec3 upper = glm::vec3(bboxUpper[offset + i]), lower = glm::vec3(bboxLower[offset + i]);
                float radius = m_fSplatRadiusFactor * glm::length(upper - lower) * 0.5f;
                glm::vec3 normal = glm::vec3(repNormal[offset + i]);

                glm::vec3 eye = glm::vec3(repPos[offset + i]) + m_fISMNormalOffset * normal;
                glm::vec3 point = eye + normal;
                glm::vec3 up;
                if(normal.x != 0) {
                    up = glm::vec3(-(normal.y + normal.z) / normal.x, 1, 1);
                } else if(normal.y != 0) {
                    up = glm::vec3(1, -(normal.x + normal.z) / normal.y, 1);
                } else {
                    up = glm::vec3(1, 1, -(normal.x + normal.y) / normal.z);
                }

                viewMatrices.emplace_back(glm::lookAt(eye, point, up));

                GLIrradianceCachingData::ICVertex v{ glm::vec4(eye, radius), glm::vec4(normal, 0), glm::vec4(0.f) };

                vertexBuffer.emplace_back(v);
            }
            ++clusterGlobalIdx;
        }
    }

    m_IrradianceCacheData.setData(vertexBuffer);
    m_IrradianceCacheData.setViewMatrices(viewMatrices);
}

void GLIrradianceCacheISMRenderer::drawBuffer(int x, int y, size_t width, size_t height, int dataToDisplay) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_ACTIVE_TEXTURE> activeTexture;
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uGeometryClusterSampler.set(m_GeometryClustersData.m_GeometryClustersTexture.getTextureHandle());

    m_DrawBuffersPassData.uGBuffer.set(m_GBuffer);

    m_DrawBuffersPassData.m_uTileCount.set(m_GeometryClustersData.m_TileCount);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(dataToDisplay);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);

    //m_DrawBuffersPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
    //m_DrawBuffersPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());

    m_DrawBuffersPassData.m_uClusterTilesOffsetsBuffer.set(m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterToTileBuffer.set(m_GeometryClustersData.m_GClusterToTileBuffer.getGPUAddress());

    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
    m_GeometryClustersData.m_ClusterNormalBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);

    m_ScreenTriangle.render();
}

void GLIrradianceCacheISMRenderer::drawBuffers(int x, int y, size_t width, size_t height) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);

    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLTimer timer(m_DrawBuffersPassData.m_TimeElapsedQuery);

    m_DrawBuffersPassData.m_Program.use();

    m_DrawBuffersPassData.m_uGeometryClusterSampler.set(m_GeometryClustersData.m_GeometryClustersTexture.getTextureHandle());

    m_DrawBuffersPassData.uGBuffer.set(m_GBuffer);

    m_DrawBuffersPassData.m_uTileCount.set(m_GeometryClustersData.m_TileCount);

    //m_DrawBuffersPassData.m_uClusterBBoxLowerBuffer.set(m_ClusterBBoxLowerBuffer.getGPUAddress());
    //m_DrawBuffersPassData.m_uClusterBBoxUpperBuffer.set(m_ClusterBBoxUpperBuffer.getGPUAddress());

    m_DrawBuffersPassData.m_uClusterTilesOffsetsBuffer.set(m_GeometryClustersData.m_GClusterTilesOffsetsBuffer.getGPUAddress());
    m_DrawBuffersPassData.m_uClusterToTileBuffer.set(m_GeometryClustersData.m_GClusterToTileBuffer.getGPUAddress());

    m_GeometryClustersData.m_ClusterBBoxLowerBuffer.bindBase(3, GL_SHADER_STORAGE_BUFFER);
    m_GeometryClustersData.m_ClusterBBoxUpperBuffer.bindBase(4, GL_SHADER_STORAGE_BUFFER);
    m_GeometryClustersData.m_ClusterNormalBuffer.bindBase(5, GL_SHADER_STORAGE_BUFFER);

    float fX = x;
    float fY = y;
    float fW = width;
    float fH = height;

    // Draw normals
    glViewport(x, y, width, height);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY, fW, fH);
    m_DrawBuffersPassData.m_uDataToDisplay.set(0);

    m_ScreenTriangle.render();

    // Draw depth
    glViewport(x + width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(1);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw Cluster node
    glViewport(x + 2 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(2);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster buffer
    glViewport(x + 4 * width, y, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(4);
    m_DrawBuffersPassData.m_uViewport.set(fX + 4 * fW, fY, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow lower point depth buffer
    glViewport(x, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(5);
    m_DrawBuffersPassData.m_uViewport.set(fX, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    // Draw geometry cluster bbow upper point depth buffer
    glViewport(x + width, y - height, width, height);
    m_DrawBuffersPassData.m_uZFar.set(getZFar());
    m_DrawBuffersPassData.m_uDataToDisplay.set(6);
    m_DrawBuffersPassData.m_uViewport.set(fX + fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    glViewport(x + 2 * width, y - height, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(7);
    m_DrawBuffersPassData.m_uViewport.set(fX + 2 * fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();

    glViewport(x + 3 * width, y - height, width, height);
    m_DrawBuffersPassData.m_uDataToDisplay.set(8);
    m_DrawBuffersPassData.m_uViewport.set(fX + 3 * fW, fY - fH, fW, fH);

    m_ScreenTriangle.render();
}

void GLIrradianceCacheISMRenderer::drawISMs(int x, int y, size_t width, size_t height) {
    const GLISMContainer* pContainer = m_bUseVPLISMs ? &m_VPLData.getVPLISMContainer() : &m_IrradianceCacheData.getISMContainer();

    if(pContainer->getTextureCount() != 0) {
        glViewport(x, y, width, height);

        if(m_nSelectedISMIndex < 0) {
            m_ISMDrawer.drawISMTexture(*pContainer, m_nSelectedISMTextureIndex, m_nSelectedISMLevel);
        } else {
            m_ISMDrawer.drawISM(*pContainer, m_nSelectedISMIndex, m_nSelectedISMLevel);

            if(m_nFrameIdx == 0) {
                m_Framebuffer.clear();
            }

            StratifiedSampler sampler(1, 1);
            sampler.init(m_nFrameIdx, 0, 0, pContainer->getShadowMapResolution(), pContainer->getShadowMapResolution());
            IntegratorSample sample;

            glm::vec3 eye = m_SelectedCacheP + m_SelectedCacheN;
            glm::vec3 point = eye + m_SelectedCacheN;
            glm::vec3 up;
            if(m_SelectedCacheN.x != 0) {
                up = glm::vec3(-(m_SelectedCacheN.y + m_SelectedCacheN.z) / m_SelectedCacheN.x, 1, 1);
            } else if(m_SelectedCacheN.y != 0) {
                up = glm::vec3(1, -(m_SelectedCacheN.x + m_SelectedCacheN.z) / m_SelectedCacheN.y, 1);
            } else {
                up = glm::vec3(1, 1, -(m_SelectedCacheN.x + m_SelectedCacheN.y) / m_SelectedCacheN.z);
            }

            glm::mat4 viewMatrix = glm::lookAt(eye, point, up);

            ParaboloidCamera camera(computeEmbreeLocalToWorld(viewMatrix),
                                            pContainer->getShadowMapResolution(), pContainer->getShadowMapResolution());

            while(sampler.getNextSample(sample)) {
                Ray ray = camera.ray(sample);
                Intersection I = intersect(ray, *m_pScene);
                if(I) {
                    float d = embree::distance(I.P, convert(m_SelectedCacheP));
                    m_Framebuffer.accumulate(embree::Vec2i(sample.getPixel()),
                                           Col3f((d - getZNear()) / (getZFar() - getZNear())));
                } else {
                    m_Framebuffer.accumulate(embree::Vec2i(sample.getPixel()), Col3f(1));
                }
            }

            m_ResultRenderer.drawResult(gloops::Viewport(x + width, y, width, height), 1.f, normalize(m_Framebuffer));
            ++m_nFrameIdx;
        }
    } else {
        std::cerr << "No ISM in the container." << std::endl;
    }
}

void GLIrradianceCacheISMRenderer::computeTimings() {
    float scale = 1.f / 1000000;

    m_Timings.geometryPass = m_Timings.m_GeometryPassQuery.waitResult() * scale;
    m_Timings.clusteredShadingPass = m_Timings.m_ClusteredShadingPassQuery.waitResult() * scale;

    m_Timings.computeIrradiancePass = m_Timings.m_ImperfectICPassQuery.waitResult() * scale;
    m_Timings.vplShadingPass = m_Timings.m_VPLLightingPassQuery.waitResult() * scale;
    m_Timings.directLightShadingPass = m_Timings.m_DirectLightingPassQuery.waitResult() * scale;
    m_Timings.drawBuffersPass = m_DrawBuffersPassData.m_TimeElapsedQuery.waitResult() * scale;
    m_Timings.computeVPLISMPass = m_Timings.m_ComputeVPLISMsQuery.waitResult() * scale;
    m_Timings.computeICISMPass =  m_Timings.m_ComputeICISMsQuery.waitResult() * scale;
    m_Timings.splatICPass = m_Timings.m_ICLightingPassQuery.waitResult() * scale;

    m_Timings.updateTotalTime();
}

void GLIrradianceCacheISMRenderer::setPrimaryLightSource(const Vec3f& P, const Col3f& L, const GLScene& glScene) {
    m_DirectLightingRenderPass.setLight(convert(P), convert(L));
    m_DirectLightingRenderPass.computeShadowMap(glScene);
}

void GLIrradianceCacheISMRenderer::exposeIO(TwBar* bar) {
    TwAddSeparator(bar, nullptr, "");

    atb::addVarRW(bar, "Do VPL Shading Pass", m_bDoVPLShadingPass, "");
    atb::addVarRW(bar, "Use VPL ISMs", m_bUseVPLISMs, "");

    TwAddSeparator(bar, nullptr, "");

    atb::addVarRW(bar, "Do IC Shading Pass", m_bDoICShadingPass, "");

    TwAddSeparator(bar, nullptr, "");

    atb::addVarRW(bar, "Do Direct Light Pass", m_bDoDirectLightPass, "");

    TwAddSeparator(bar, nullptr, "");

    atb::addVarRW(bar, "Render irradiance", m_bRenderIrradiance, "");
    atb::addVarRW(bar, "Gamma", m_fGamma, "");
    atb::addVarRW(bar, "Shadow Map Offset", m_fShadowMapOffset, "");
    atb::addVarRW(bar, "Distance clamp", m_fDistClamp, "");
    atb::addVarRW(bar, "Use normal clustering", m_bUseNormalClustering, "");
    atb::addVarRW(bar, "Show GSpheres", m_bShowSpheres, "");
    atb::addVarRO(bar, "Geometry cluster count", m_GeometryClustersData.m_nGClusterCount, "");
    atb::addVarRW(bar, "Do Pull", m_bDoPull, "");
    atb::addVarRW(bar, "Do Pull Push", m_bDoPullPush, "");
    atb::addVarRW(bar, "Pull treshold", m_fPullTreshold, "");
    atb::addVarRW(bar, "Push treshold", m_fPushTreshold, "");
    atb::addVarRW(bar, "Push Push nb levels", m_nPullPushNbLevels, "");
    atb::addVarRW(bar, "ISM Point Max Size", m_fISMPointMaxSize, "");
    atb::addVarRW(bar, "ISM offset", m_fISMOffset, "");
    atb::addVarRW(bar, "ISM normal offset", m_fISMNormalOffset, "");
    atb::addVarRW(bar, "ISM view index", m_nSelectedISMIndex, "");
    atb::addVarRW(bar, "ISM texture index", m_nSelectedISMTextureIndex, "");
    atb::addVarRW(bar, "ISM texture level", m_nSelectedISMLevel, "");
    atb::addVarRW(bar, "Scene Sample Count", m_nSceneSampleCount, "");
    atb::addVarRW(bar, "Scene Sampling Mode", m_nSceneSamplingMode, "");
    atb::addVarRW(bar, "Tile Window", m_nTileWindow, "");
    atb::addVarRW(bar, "Max distance lookup", m_fMaxDist, "");
    atb::addVarRW(bar, "SplatRadiusFactor", m_fSplatRadiusFactor, "");
    atb::addVarRW(bar, "Use irradiance caching", m_bUseIrradianceEstimate, "");
    atb::addVarRW(bar, "Use coherent irradiance cache", m_bUseCoherentIrradianceCache, "");
    atb::addVarRW(bar, "Use CPU Irradiance Caches", m_bUseCPUIrradianceCache, "");
    atb::addButton(bar, "Compute CPU Irradiance Caches",
                   [this]() { computeCPUIrradianceCaches(); m_bUseCPUIrradianceCache = true; }, "");
    atb::addVarRW(bar, "Points Count per IC", m_nPointCountPerIC, "");
    atb::addVarRW(bar, "Point Clusters Count", m_nPointClusterCount, "");
    atb::addVarCB(bar, "BSH cut level", [&](uint32_t level) { m_BSHLevel = level; computeSamplesFromBSH(); },
        [&]() -> uint32_t { return m_BSHLevel; }, "");

    atb::addVarRW(bar, "Many LoD Treshold", m_fManyLODTreshold, "");

    TwAddSeparator(bar, "GLIrradianceCacheISMRenderer::Separator1", "");

    atb::addVarRO(bar, "Geometry Pass", m_Timings.geometryPass, "");
    atb::addVarRO(bar, "Clustered Shading Pass", m_Timings.clusteredShadingPass, "");
    atb::addVarRO(bar, "Compute Irradiance Pass", m_Timings.computeIrradiancePass, "");
    atb::addVarRO(bar, "VPL Shading Pass", m_Timings.vplShadingPass, "");
    atb::addVarRO(bar, "Direct Light Shading Pass", m_Timings.directLightShadingPass, "");
    atb::addVarRO(bar, "Draw Buffers Pass", m_Timings.drawBuffersPass, "");
    atb::addVarRO(bar, "Compute VPL ISMs Pass", m_Timings.computeVPLISMPass, "");
    atb::addVarRO(bar, "Compute Cluster ISMs Pass", m_Timings.computeICISMPass, "");
    atb::addVarRO(bar, "Splat IC Pass", m_Timings.splatICPass, "");
    atb::addVarRO(bar, "Total time", m_Timings.totalTime, "");

    TwAddSeparator(bar, "GLIrradianceCacheISMRenderer::Separator2", "");
}

}


