#pragma once

#include "bonez/opengl/utils/GLutils.hpp"
#include "bonez/opengl/scene/GLScene.hpp"
#include "bonez/opengl/shaders/GLSLProgramBuilder.hpp"

#include "GLGBuffer.hpp"

namespace BnZ {

struct GLClusteredShadingBuffer {
    static const uint32_t TILE_SIZE = 32;

    // Number of tiles in each dimension
    glm::uvec2 m_TileCount = glm::uvec2(0);

    // This texture maps each pixel of the image to the local offset of its cluster.
    // The local offset is an index in the screen-space tile from 0 to the number of clusters in this tile
    // minus 1
    GLTexture2D m_GeometryClustersTexture;

    // This buffer contains for each tile (Tx, Ty) with index Tx + Ty * Sx the number of clusters
    // contains in this tile.
    // It is used by the Count Geometry Clusters Pass to count the total number of clusters
    // In this implementation, the total number of tiles must be less than the number of threads
    // in a work item (1024 on my computer)
    GLBuffer m_GClusterCountsBuffer;

    // This buffer contain for each tile a global offset for the clusters contained in this tile
    // After the CountGeometryClusterPass, let localID be the local cluster ID of a tile (Tx, Ty). Then it's
    // global cluster ID is globalID = m_GClusterTilesOffsetsBuffer[Tx + Ty * Sx] + localID
    GLBuffer m_GClusterTilesOffsetsBuffer;

    // A buffer that will contain the total number of clusters (buffer of size 1)
    GLBuffer m_GClusterTotalCountBuffer;
    // CPU version of the previous buffer
    GLuint m_nGClusterCount = 0;

    GLBuffer m_GClusterToTileBuffer;

    // This buffers contain data computed for each cluster during the find unique geometr cluster pass
    // Let localID be the local index of the cluster (from 0 to 1023) in its tile (Tx, Ty)
    // Then its data are stored at address (Tx + Ty * Sy) * 1024 + localID
    // Indeed, the globalID (from 0 to the number of cluster - 1) is computed during the next pass (count
    // geometry cluster)
    GLBuffer m_ClusterBBoxLowerBuffer;
    GLBuffer m_ClusterBBoxUpperBuffer;
    GLBuffer m_ClusterNormalBuffer;

    GLBuffer m_ClusterRepPositionBuffer;
    GLBuffer m_ClusterRepNormalBuffer;

    void init(uint32_t width, uint32_t height);
};

class GLClusteredShadingBufferRenderer {
    struct FindUniqueClustersPass {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program = buildComputeProgram(s_ComputeShader);

        BNZ_GLUNIFORM(m_Program, GLuint64, uClustersImage);
        BNZ_GLUNIFORM(m_Program, glm::mat4, uInvProjMatrix);
        BNZ_GLUNIFORM(m_Program, glm::mat4, uRcpViewMatrix);
        BNZ_GLUNIFORM(m_Program, glm::vec2, uScreenSize);
        BNZ_GLUNIFORM(m_Program, GLuint64, uClusterCountsBuffer);
        BNZ_GLUNIFORM(m_Program, GLuint64, uClusterBBoxLowerBuffer);
        BNZ_GLUNIFORM(m_Program, GLuint64, uClusterBBoxUpperBuffer);
        BNZ_GLUNIFORM(m_Program, GLuint64, uClusterRepPositionBuffer);
        BNZ_GLUNIFORM(m_Program, GLuint64, uClusterRepNormalBuffer);
        BNZ_GLUNIFORM(m_Program, GLuint64, uClusterNormalBuffer);
        BNZ_GLUNIFORM(m_Program, bool, uUseNormalClustering);

        BNZ_GLUNIFORM(m_Program, GLuint, uSy);
        BNZ_GLUNIFORM(m_Program, float, uHalfFOVY);
        BNZ_GLUNIFORM(m_Program, float, uZNear);
        BNZ_GLUNIFORM(m_Program, float, uZFar);

        GLGBufferUniform uGBuffer { m_Program };
    };

    FindUniqueClustersPass m_FindUniqueClustersPass;

    struct CountClustersPass {
        static const GLchar* s_ComputeShader;

        gloops::Program m_Program = buildComputeProgram(s_ComputeShader);

        BNZ_GLUNIFORM(m_Program, uint, uTileCount);
        BNZ_GLUNIFORM(m_Program, GLuint64, uClusterCountsBuffer);
        BNZ_GLUNIFORM(m_Program, GLuint64, uClusterTilesOffsetsBuffer);
        BNZ_GLUNIFORM(m_Program, GLuint64, uClusterCount);
        BNZ_GLUNIFORM(m_Program, GLuint64, uClusterToTileBuffer);
    };

    CountClustersPass m_CountClustersPass;

public:
    void render(const glm::mat4& rcpProjMatrix, const glm::mat4& rcpViewMatrix,
                const GLGBuffer& gbuffer, GLClusteredShadingBuffer& buffer, bool useNormalClustering);
};

}
