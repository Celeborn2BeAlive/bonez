#pragma once

#include "GLManyLODBSH.hpp"
#include "bonez/opengl/common.hpp"

#include "bonez/opengl/GLISMRenderer.hpp"

namespace BnZ {

class GLManyLODRenderPass {
public:
    GLManyLODRenderPass();

    void setPoints(const std::vector<GLPoint>& points);

    void render(GLISMContainer& container,
                GLBuffer& viewMatrixBuffer, uint32_t viewMatrixCount,
                float treshold, uint32_t maxBSHLevel);

    // Return the cut computed for a given view
    std::vector<uint32_t> render(GLISMContainer& container,
                                 GLBuffer& viewMatrixBuffer, uint32_t viewMatrixCount,
                                 float treshold, uint32_t viewIndex, uint32_t maxBSHLevel);

    const GLManyLODBSH& getBSH() const {
        return m_BSH;
    }
private:
    typedef glm::uvec2 ViewNodePair;

    GLManyLODBSH m_BSH;
    struct GPUBSHNode {
        glm::vec4 boundingSphere;
        glm::vec4 boundingNormalCone;
        glm::uvec4 rightChild;

        GPUBSHNode() = default;

        GPUBSHNode(const GLManyLODBSH::Node& node):
            boundingSphere(node.sphere.center, node.sphere.sqrRadius),
            boundingNormalCone(node.normal, node.normalConeDot),
            rightChild(node.rightChild) {
        }
    };
    GLBuffer m_GPUBSH;

    GLBuffer m_LODActiveListBuffer[2];
    GLVertexArray m_LODActiveListVertexArray[2];

    GLBuffer m_LODInactiveListBuffer;
    GLVertexArray m_LODInactiveListVertexArray;

    GLTransformFeedbackObject m_TFO[2];

    static const char* s_VertexShader;
    static const char* s_GeometryShader;
    static const char* s_FragmentShader;

    static gloops::Program buildProgram();

    gloops::Program m_Program = buildProgram();

    BNZ_GLUNIFORM(m_Program, GLuint, uActiveListSize);
    BNZ_GLUNIFORM(m_Program, GLuint, uInactiveListSize);
    BNZ_GLUNIFORM(m_Program, GLuint64, uBSH);
    BNZ_GLUNIFORM(m_Program, GLfloat, uPointSizeTreshold);
    BNZ_GLUNIFORM(m_Program, GLfloat, uShadowMapArea);
    BNZ_GLUNIFORM(m_Program, GLuint64, uViewMatrices);
    BNZ_GLUNIFORM(m_Program, glm::mat4, uMVPMatrix);


    GLISMRenderer::ISMUniforms uISMData { m_Program };
    BNZ_GLUNIFORM(m_Program, GLfloat, uZNear);
    BNZ_GLUNIFORM(m_Program, GLfloat, uZFar);
    //BNZ_GLUNIFORM(m_Program, GLfloat, uPointMaxSize);
    BNZ_GLUNIFORM(m_Program, bool, uIsLastPass);


    // Used to connext the ISM to the rendering pipeline
    gloops::FramebufferObject m_FBO;
    gloops::TextureObject m_DepthBuffer;
};

}
