#pragma once

#include "bonez/opengl/utils/GLFramebuffer.hpp"

namespace BnZ {

enum GBufferTextureType {
    GBUFFER_NORMAL_DEPTH,
    GBUFFER_DIFFUSE,
    GBUFFER_GLOSSY_SHININESS,
    GBUFFER_TEXTURE_COUNT
};

struct GLGBuffer: public GLFramebuffer<GBUFFER_TEXTURE_COUNT> {
    void init(uint32_t width, uint32_t height) {
        GLTexFormat formats[GBUFFER_TEXTURE_COUNT] = {
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT },
            { GL_RGBA32F, GL_RGBA, GL_FLOAT }
        };
        GLFramebuffer<GBUFFER_TEXTURE_COUNT>::init(width, height, formats,
            { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });

        for(auto i = 0; i < GBUFFER_TEXTURE_COUNT; ++i) {
            glMakeTextureHandleResidentNV(glGetTextureHandleNV(getColorBuffer(i).glId()));
        }
    }
};

struct GLGBufferUniform {
    GLUniform<GLuint64> normalDepthSampler;
    GLUniform<GLuint64> diffuseSampler;
    GLUniform<GLuint64> glossyShininessSampler;

    GLGBufferUniform(const gloops::Program& program):
        normalDepthSampler(program, "uGBuffer.normalDepthSampler"),
        diffuseSampler(program, "uGBuffer.diffuseSampler"),
        glossyShininessSampler(program, "uGBuffer.glossyShininessSampler") {
    }

    void set(const GLGBuffer& gbuffer) {
        normalDepthSampler.set(glGetTextureHandleNV(gbuffer.getColorBuffer(GBUFFER_NORMAL_DEPTH).glId()));
        diffuseSampler.set(glGetTextureHandleNV(gbuffer.getColorBuffer(GBUFFER_DIFFUSE).glId()));
        glossyShininessSampler.set(glGetTextureHandleNV(gbuffer.getColorBuffer(GBUFFER_GLOSSY_SHININESS).glId()));
    }
};

}
