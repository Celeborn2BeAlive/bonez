#include "GLIrradianceCachingData.hpp"

namespace BnZ {

void GLIrradianceCachingData::renderISMs(GLISMRenderer& ismRenderer, const GLPoints& sampledScene, float pointMaxSize, bool doPull, bool doPullPush, float pullTreshold,
                                         float pushTreshold, uint32_t pullPushNbLevels, bool useCoherentIrradianceCache) {
    if(useCoherentIrradianceCache) {
        ismRenderer.renderCoherent(sampledScene, m_ISMContainer, m_ViewMatrices.data(),
                             m_ViewMatrices.size(), pointMaxSize, doPull, doPullPush,
                             pullTreshold, pushTreshold, pullPushNbLevels);
    } else {
        ismRenderer.render(sampledScene, m_ISMContainer, m_ViewMatrices.data(),
                           m_ViewMatrices.size(), pointMaxSize, doPull, doPullPush,
                           pullTreshold, pushTreshold, pullPushNbLevels);
    }

    m_ISMBuffer.setData(m_ISMContainer.getTextureCount(),
                        m_ISMContainer.getTextureHandleBufferPtr(),
                        GL_DYNAMIC_DRAW);

    m_ISMBuffer.makeResident(GL_READ_ONLY);
}

void GLIrradianceCachingData::renderISMsPacks(GLISMRenderer& ismRenderer, const GLPoints& sampledScene, size_t pointCountPerISM,
                     float pointMaxSize, bool doPull, bool doPullPush, float pullTreshold,
                     float pushTreshold, uint32_t pullPushNbLevels) {
    ismRenderer.renderPacks(sampledScene, pointCountPerISM, m_ISMContainer, m_ViewMatrices.data(),
                       m_ViewMatrices.size(), pointMaxSize, doPull, doPullPush,
                       pullTreshold, pushTreshold, pullPushNbLevels);

    m_ISMBuffer.setData(m_ISMContainer.getTextureCount(),
                        m_ISMContainer.getTextureHandleBufferPtr(),
                        GL_DYNAMIC_DRAW);

    m_ISMBuffer.makeResident(GL_READ_ONLY);

}

void GLIrradianceCachingData::renderISMsManyLOD(GLISMRenderer& ismRenderer, GLManyLODRenderPass &manyLoDRenderPass,
                                                float treshold, uint32_t maxBSHLevel,
                                                bool doPull, bool doPullPush, float pullTreshold,
                                                float pushTreshold, uint32_t pullPushNbLevels) {
    manyLoDRenderPass.render(m_ISMContainer, getViewMatrixBuffer(), getICCount(), treshold, maxBSHLevel);
    ismRenderer.pullPush(m_ISMContainer, doPull, doPullPush, pullTreshold,
                         pushTreshold, pullPushNbLevels);

    m_ISMBuffer.setData(m_ISMContainer.getTextureCount(),
                        m_ISMContainer.getTextureHandleBufferPtr(),
                        GL_DYNAMIC_DRAW);

    m_ISMBuffer.makeResident(GL_READ_ONLY);
}

}
