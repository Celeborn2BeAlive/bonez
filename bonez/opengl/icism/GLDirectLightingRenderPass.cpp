#include "GLDirectLightingRenderPass.hpp"

namespace BnZ {

const GLchar* GLDirectLightingRenderPass::s_VertexShader =
"#version 430\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec3 vFarPointViewSpaceCoords;

uniform mat4 uInvProjMatrix;

void main() {
    vec4 farPointViewSpaceCoords = uInvProjMatrix * vec4(aPosition, 1.f, 1.f);
    vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLDirectLightingRenderPass::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

uniform GBuffer uGBuffer;

uniform mat4 uFaceProjectionMatrices[6];
uniform samplerCubeShadow uShadowMapSampler;
uniform float uShadowMapOffset;
uniform float uDistClamp;

uniform vec3 uLightPosition;
uniform vec3 uLightIntensity;
uniform mat4 uLightViewMatrix;

uniform mat4 uRcpViewMatrix;

uniform bool uRenderIrradiance;

in vec3 vFarPointViewSpaceCoords;

out vec3 fFragColor;

int getFaceIdx(vec3 wi) {
    vec3 absWi = abs(wi);

    float maxComponent = max(absWi.x, max(absWi.y, absWi.z));

    if(maxComponent == absWi.x) {
        if(wi.x > 0) {
            return 0;
        }
        return 1;
    }

    if(maxComponent == absWi.y) {
        if(wi.y > 0) {
            return 2;
        }
        return 3;
    }

    if(maxComponent == absWi.z) {
        if(wi.z > 0) {
            return 4;
        }
        return 5;
    }

    return -1;
}

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // Read fragment attributes
    vec4 normalDepth = texture(uGBuffer.normalDepthSampler, texCoords);
    vec3 fragPosition = normalDepth.w * vFarPointViewSpaceCoords;

    vec3 Kd;
    vec4 KsShininess;

    if(uRenderIrradiance) {
        Kd = vec3(1.f / 3.14f);
        KsShininess = vec4(0.f, 0.f, 0.f, 1.f);
    } else {
        Kd = texture(uGBuffer.diffuseSampler, texCoords).rgb;
        KsShininess = texture(uGBuffer.glossyShininessSampler, texCoords);
    }

    vec3 fragPos_ws = vec3(uRcpViewMatrix * vec4(fragPosition, 1));

    vec3 wi = uLightPosition - fragPos_ws;
    float dist = length(wi);
    wi /= dist;

    float cos_i = max(0, dot(wi, normalDepth.xyz));

    dist = max(dist, uDistClamp);

    float G = cos_i / (dist * dist);

    vec3 fragPos_ls = vec3(uLightViewMatrix * vec4(fragPos_ws, 1));
    vec3 shadowRay = normalize(fragPos_ws - uLightPosition);
    int face = getFaceIdx(shadowRay);
    vec4 faceClipSpacePosition = uFaceProjectionMatrices[face] * vec4(fragPos_ls, 1);
    float depthRef = faceClipSpacePosition.z / faceClipSpacePosition.w;
    depthRef = 0.5 * (depthRef + 1) - uShadowMapOffset;

    float shadowFactor = texture(uShadowMapSampler, vec4(shadowRay, depthRef));

    vec3 r = reflect(-wi, normalDepth.xyz);

    vec3 brdf = Kd + KsShininess.rgb * pow(max(0, dot(-normalize(fragPosition), r)), KsShininess.a);
    fFragColor = brdf * shadowFactor * G * uLightIntensity;
}

);

void GLDirectLightingRenderPass::setLight(glm::vec3 lightPosition, glm::vec3 lightIntensity) {
    m_LightPosition = lightPosition;
    m_LightIntensity = lightIntensity;
    m_LightViewMatrix = glm::translate(glm::mat4(1), -m_LightPosition);

    m_bIsLightSet = true;
}

void GLDirectLightingRenderPass::computeShadowMap(const GLScene& scene) {
    if(!m_bIsLightSet) {
        return;
    }

    m_ShadowMapContainer.init(SHADOWMAP_RES, 1);
    m_ShadowMapRenderer.render(scene, m_ShadowMapContainer, &m_LightViewMatrix, 1);

    m_ShadowMapContainer.m_Textures[0].makeTextureHandleResident();
}

void GLDirectLightingRenderPass::render(const glm::mat4& rcpProjMatrix, const glm::mat4& rcpViewMatrix,
                                        const glm::vec4& viewport,
                                        float distClamp, float smOffset, bool renderIrradiance,
                                        GLGBuffer& gbuffer, const ScreenTriangle& triangle) {
    if(!m_bIsLightSet) {
        return;
    }

    m_Program.use();

    uGBuffer.set(gbuffer);

    uViewport.set(viewport);
    uInvProjMatrix.set(rcpProjMatrix);
    uRcpViewMatrix.set(rcpViewMatrix);

    uLightPosition.set(m_LightPosition);
    uLightIntensity.set(m_LightIntensity);
    uLightViewMatrix.set(m_LightViewMatrix);

    if(m_ShadowMapContainer.m_Textures.empty()) {
        uShadowMapSampler.set(0u);
    } else {
        uShadowMapSampler.set(m_ShadowMapContainer.m_Textures[0].getTextureHandle());
    }
    uFaceProjectionMatrices.set(6, m_ShadowMapRenderer.getFaceProjectionMatrices());

    uDistClamp.set(distClamp);
    uShadowMapOffset.set(smOffset);
    uRenderIrradiance.set(renderIrradiance);

    triangle.render();
}

}
