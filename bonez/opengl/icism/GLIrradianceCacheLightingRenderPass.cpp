#include "GLIrradianceCacheLightingRenderPass.hpp"

namespace BnZ {

const GLchar* GLIrradianceCacheLightingRenderPass::SplatIrradiancePass::s_VertexShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

// The input is an irradiance cache
layout(location = 0) in vec4 aICPositionRadius;
layout(location = 1) in vec3 aICNormal;
layout(location = 2) in vec3 aICIrradiance;

out vec4 vICPositionRadius;
out vec3 vICNormal;
out vec3 vICIrradiance;

uniform mat4 uMVPMatrix;

void main() {
    vICPositionRadius = aICPositionRadius;
    vICNormal = aICNormal;
    vICIrradiance = aICIrradiance;

    gl_Position = vec4(aICPositionRadius.xyz, 1);
}

);

const GLchar* GLIrradianceCacheLightingRenderPass::SplatIrradiancePass::s_GeometryShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in vec4 vICPositionRadius[];
in vec3 vICNormal[];
in vec3 vICIrradiance[];

flat out vec4 gICPositionRadius;
flat out vec3 gICNormal;
flat out vec3 gICIrradiance;

uniform mat4 uViewMatrix;
uniform mat4 uProjMatrix;

void main() {
    vec3 P_vs = vec3(uViewMatrix * vec4(vICPositionRadius[0].xyz, 1));

    float w = vICPositionRadius[0].w;

    gICPositionRadius = vICPositionRadius[0];
    gICNormal = vICNormal[0];
    gICIrradiance = vICIrradiance[0];

    gl_Position = uProjMatrix * vec4(P_vs + vec3(-w, -w, 0), 1);

    EmitVertex();

    gICPositionRadius = vICPositionRadius[0];
    gICNormal = vICNormal[0];
    gICIrradiance = vICIrradiance[0];

    gl_Position = uProjMatrix * vec4(P_vs + vec3(w, -w, 0), 1);

    EmitVertex();

    gICPositionRadius = vICPositionRadius[0];
    gICNormal = vICNormal[0];
    gICIrradiance = vICIrradiance[0];

    gl_Position = uProjMatrix * vec4(P_vs + vec3(-w, w, 0), 1);

    EmitVertex();

    gICPositionRadius = vICPositionRadius[0];
    gICNormal = vICNormal[0];
    gICIrradiance = vICIrradiance[0];

    gl_Position = uProjMatrix * vec4(P_vs + vec3(w, w, 0), 1);

    EmitVertex();

    EndPrimitive();
}

);

const GLchar* GLIrradianceCacheLightingRenderPass::SplatIrradiancePass::s_FragmentShader =
"#version 430\n"
"#extension GL_NV_gpu_shader5 : enable\n"
GLOOPS_STRINGIFY(

uniform vec4 uViewport;

struct GBuffer {
    sampler2D normalDepthSampler;
    sampler2D diffuseSampler;
    sampler2D glossyShininessSampler;
};

uniform GBuffer uGBuffer;

flat in vec4 gICPositionRadius;
flat in vec3 gICNormal;
flat in vec3 gICIrradiance;

uniform mat4 uRcpViewMatrix;
uniform mat4 uRcpProjMatrix;

out vec4 fFragIrradianceWeight;

void main() {
    vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

    // The far point for that pixel in view space
    vec4 farPoint_vs = uRcpProjMatrix * vec4(2 * texCoords - vec2(1, 1), 1.f, 1.f);
    farPoint_vs /= farPoint_vs.w;

    // Read fragment attributes
    vec4 normalDepth = texture(uGBuffer.normalDepthSampler, texCoords);
    vec3 P_ws = vec3(uRcpViewMatrix * vec4(normalDepth.w * farPoint_vs.xyz, 1)); // Position of the fragment in world space

    float dist = distance(gICPositionRadius.xyz, P_ws);

    if(dist > gICPositionRadius.w) {
        discard;
    }

    if(dot(normalize(gICPositionRadius.xyz - P_ws), (normalDepth.xyz + gICNormal) * 0.5f) < -0.1f) {
        discard;
    }

    float c = max(0, dot(gICNormal, normalDepth.xyz));

//    if(c < 0) {
//        discard;
//    }

    // Ward weights

    float denominator = max(0.f, dist / gICPositionRadius.w + sqrt(1 - c));
    float weight = 1.f / denominator;

    //float weight = max(0, c * (gICPositionRadius.w - dist));

    //float weight = max(0, c * exp(-dist / gICPositionRadius.w));

    //float x = (gICPositionRadius.w - dist) / gICPositionRadius.w;
    //float weight = max(0, c * x * x);

    fFragIrradianceWeight = weight * vec4(gICIrradiance, 1);
}

);

const GLchar* GLIrradianceCacheLightingRenderPass::WeightDivisionPass::s_VertexShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec2 aPosition;

    out vec3 vFarPointViewSpaceCoords;

    uniform mat4 uRcpProjMatrix;

    void main() {
        vec4 farPointViewSpaceCoords = uRcpProjMatrix * vec4(aPosition, 1.f, 1.f);
        vFarPointViewSpaceCoords = farPointViewSpaceCoords.xyz / farPointViewSpaceCoords.w;

        gl_Position = vec4(aPosition, 0, 1);
    }
);

const GLchar* GLIrradianceCacheLightingRenderPass::WeightDivisionPass::s_FragmentShader =
"#version 330\n"
GLOOPS_STRINGIFY(
    uniform vec4 uViewport;

    struct GBuffer {
        sampler2D normalDepthSampler;
        sampler2D diffuseSampler;
        sampler2D glossyShininessSampler;
    };

    uniform GBuffer uGBuffer;
    uniform sampler2D uIrradianceSampler;

    uniform bool uRenderIrradiance;

    in vec3 vFarPointViewSpaceCoords;

    out vec3 fFragColor;

    void main() {
        vec2 texCoords = (gl_FragCoord.xy - uViewport.xy) / uViewport.zw;

        // Read fragment attributes
        vec3 Kd;

        if(uRenderIrradiance) {
            Kd = vec3(1.f / 3.14f);
        } else {
            Kd = texture(uGBuffer.diffuseSampler, texCoords).rgb;
        }

        vec4 irradianceWeight = texture(uIrradianceSampler, texCoords);

        if(irradianceWeight.a == 0.f) {
            fFragColor = vec3(1, 0, 1);
            return;
        }

        fFragColor = Kd * irradianceWeight.rgb / irradianceWeight.a;
    }
);

void GLIrradianceCacheLightingRenderPass::setResolution(uint32_t width, uint32_t height) {
    GLTexFormat format = { GL_RGBA32F, GL_RGBA, GL_FLOAT };
    m_SplatIrradiancePass.m_IrradianceEstimationBuffer.init(
                width, height, &format, { GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT });

    m_SplatIrradiancePass.m_IrradianceEstimationBuffer.getColorBuffer(0).makeTextureHandleResident();
}

void GLIrradianceCacheLightingRenderPass::render(
        const glm::mat4& projMatrix, const glm::mat4& viewMatrix,
        const glm::mat4& rcpProjMatrix, const glm::mat4& rcpViewMatrix,
        const glm::vec4& viewport, bool renderIrradiance,
        const GLGBuffer& gbuffer, const GLIrradianceCachingData& icData,
        const ScreenTriangle& triangle) {
    {
        auto width = gbuffer.getWidth();
        auto height = gbuffer.getHeight();

        auto depthTest = pushGLState<GL_DEPTH_TEST>();
        depthTest.set(false);

        auto blend = pushGLState<GL_BLEND>();
        blend.set(true);

        auto viewport = pushGLState<GL_VIEWPORT>();
        viewport.set(glm::vec4(0, 0, width, height));

        auto fbo = pushGLState<GL_DRAW_FRAMEBUFFER_BINDING>();
        m_SplatIrradiancePass.m_IrradianceEstimationBuffer.bindForDrawing();

        glClear(GL_COLOR_BUFFER_BIT);

        glBlendFunc(GL_ONE, GL_ONE);
        glBlendEquation(GL_FUNC_ADD);

        m_SplatIrradiancePass.m_Program.use();

        m_SplatIrradiancePass.uGBuffer.set(gbuffer);

        m_SplatIrradiancePass.uProjMatrix.set(projMatrix);
        m_SplatIrradiancePass.uViewMatrix.set(viewMatrix);
        m_SplatIrradiancePass.uViewport.set(glm::vec4(0.f, 0.f, float(width), float(height)));
        m_SplatIrradiancePass.uRcpProjMatrix.set(rcpProjMatrix);
        m_SplatIrradiancePass.uRcpViewMatrix.set(rcpViewMatrix);

        icData.drawPoints();
    }

    {
        m_WieghtDivisionPass.m_Program.use();

        m_WieghtDivisionPass.uGBuffer.set(gbuffer);

        m_WieghtDivisionPass.uRcpProjMatrix.set(rcpProjMatrix);
        m_WieghtDivisionPass.uViewport.set(viewport);
        m_WieghtDivisionPass.uIrradianceSampler.set(
                    m_SplatIrradiancePass.m_IrradianceEstimationBuffer.getColorBuffer(0).getTextureHandle());
        m_WieghtDivisionPass.uRenderIrradiance.set(renderIrradiance);

        triangle.render();
    }
}

}
