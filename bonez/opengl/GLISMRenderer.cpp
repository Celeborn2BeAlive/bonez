#include "GLISMRenderer.hpp"

namespace BnZ {

GLISMContainer::GLISMContainer(uint32_t smRes):
    m_nTextureRes(4096),
    m_nSMRes(smRes),
    m_nSMCountPerDimension(m_nTextureRes / m_nSMRes),
    m_nSMCountPerTexture(m_nSMCountPerDimension * m_nSMCountPerDimension) {


    // Create pull push textures
    uint32_t res = m_nTextureRes / 2;
    for(int s = smRes / 2; s >= 1; s /= 2) {
        m_PullPushTextures.emplace_back();
        const auto& texObject = m_PullPushTextures.back();

        glTextureParameteriEXT(texObject.glId(),
                               GL_TEXTURE_2D,
                               GL_TEXTURE_MIN_FILTER,
                               GL_NEAREST);
        glTextureParameteriEXT(texObject.glId(),
                               GL_TEXTURE_2D,
                               GL_TEXTURE_MAG_FILTER,
                               GL_NEAREST);

        glTextureImage2DEXT(texObject.glId(),
                            GL_TEXTURE_2D,
                            0,
                            GL_R32F,
                            res,
                            res,
                            0,
                            GL_RED,
                            GL_FLOAT,
                            nullptr);

        auto imageHandle = glGetImageHandleNV(texObject.glId(), 0, GL_FALSE, 0, GL_R32F);
        glMakeImageHandleResidentNV(imageHandle, GL_READ_WRITE);

        res /= 2;
    }
}

void GLISMContainer::resize(uint32_t smCount) {
    uint32_t offset = m_Textures.size();
    size_t texCount = smCount / m_nSMCountPerTexture + (smCount % m_nSMCountPerTexture != 0);
    if(texCount > offset) {
        m_Textures.resize(texCount);
        for(auto i = offset; i < m_Textures.size(); ++i) {
            const auto& texObject = m_Textures[i];

            glTextureParameteriEXT(texObject.glId(),
                                   GL_TEXTURE_2D,
                                   GL_TEXTURE_MIN_FILTER,
                                   GL_NEAREST);
            glTextureParameteriEXT(texObject.glId(),
                                   GL_TEXTURE_2D,
                                   GL_TEXTURE_MAG_FILTER,
                                   GL_NEAREST);

            glTextureImage2DEXT(texObject.glId(),
                                GL_TEXTURE_2D,
                                0,
                                GL_R32F,
                                m_nTextureRes,
                                m_nTextureRes,
                                0,
                                GL_RED,
                                GL_FLOAT,
                                nullptr);

            m_TextureHandles.emplace_back(glGetTextureHandleNV(texObject.glId()));
            glMakeTextureHandleResidentNV(m_TextureHandles.back());

            auto imageHandle = glGetImageHandleNV(texObject.glId(), 0, GL_FALSE, 0, GL_R32F);
            glMakeImageHandleResidentNV(imageHandle, GL_READ_WRITE);
        }
    }
}

const GLchar *GLISMRenderer::s_RenderVertexShader =
"#version 430 core\n"
"#extension GL_NV_gpu_shader5: enable\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec3 aPosition;
    layout(location = 1) in vec3 aNormal;

    uniform bool uDoPacked;
    uniform uint uPackSize;

    uniform float uZFar;
    uniform float uZNear;

    out vec3 vProjectedPosition; // Projected position in clip space
    out float vDepth;
    flat out uvec2 vViewportIndex; // Position (i, j) of the viewport in image space

    struct ViewMatrixData {
        const mat4* viewMatrixBuffer;
        uint viewMatrixCount;
    };

    uniform ViewMatrixData uViewMatrixData;

    struct ISMData {
        uint smResolution;
        uint smCountPerTexture;
        uint smCountPerDimension;
    };

    uniform ISMData uISMData;

    uniform float uPointMaxSize;

    void main() {
        // Get the index of the view associated with the input vertex
        uint viewIdx;

        if(uDoPacked) {
            viewIdx = gl_VertexID / uPackSize;
        } else {
            viewIdx = gl_VertexID % uViewMatrixData.viewMatrixCount;
        }

        mat4 viewMatrix = uViewMatrixData.viewMatrixBuffer[viewIdx];

        vec4 Pvs = viewMatrix * vec4(aPosition, 1); // Position in view space

        /*
        if(dot(aNormal, -Pvs.xyz) <= 0.f) {
            gl_Position = vec4(0);
            return;
        }*/

        Pvs.z = -Pvs.z; // View the negative hemisphere

        // Paraboloid projection
        Pvs /= Pvs.w;

        vec3 v = Pvs.xyz;
        vProjectedPosition.z = Pvs.z;

        float l = length(v);
        v /= l;

        vec3 h = v + vec3(0, 0, 1); // h = k * (x, y, 1)  => x = h.x / h.z   y = h.y / h.z

        vDepth = (l - uZNear) / (uZFar - uZNear);

        // vProjectedPosition is in [-1, 1] x [-1, 1]
        vProjectedPosition.xy = h.xy / h.z;

        //float b = 1.f / (uPointMaxSize - 1);
        //float a = uPointMaxSize * b;

        //gl_PointSize = a / (b + vDepth * vDepth);

        gl_PointSize = max(1, uPointMaxSize * exp(-vDepth));

        // Dimension of a view in clip space
        float size = 2.f / uISMData.smCountPerDimension;

        // 2D coordinates of the viewport attached to the view
        uint i = viewIdx % uISMData.smCountPerDimension;
        uint j = (viewIdx - i) / uISMData.smCountPerDimension;
        vViewportIndex = uvec2(i, j);

        // Coordinates between 0 and 1 in the viewport of the view
        vec2 xy = 0.5 * (vProjectedPosition.xy + vec2(1, 1));

        // Origin of the viewport of the view in clip space
        vec2 origin = vec2(-1, -1) + vec2(i, j) * size;

        gl_Position = vec4(origin + xy * size, vDepth, 1);
    }
);

const GLchar *GLISMRenderer::s_RenderFragmentShader =
"#version 430 core\n"
GLOOPS_STRINGIFY(
    in vec3 vProjectedPosition;
    in float vDepth;
    flat in uvec2 vViewportIndex;

    struct ISMData {
        uint smResolution;
        uint smCountPerTexture;
        uint smCountPerDimension;
    };

    uniform ISMData uISMData;

    out float fDepth;

    void main() {
        if(vProjectedPosition.z >= 0.f) {
            uvec2 pixel = uvec2(gl_FragCoord.xy);
            uvec2 viewportOrigin = vViewportIndex.xy * uISMData.smResolution;

            // Do not write outside the viewport
            if(pixel.x < viewportOrigin.x || pixel.x >= viewportOrigin.x + uISMData.smResolution
                    || pixel.y < viewportOrigin.y || pixel.y >= viewportOrigin.y + uISMData.smResolution) {
                discard;
            } else {
                fDepth = vDepth;
            }
        } else {
            discard;
        }
    }
);

const GLchar *GLISMRenderer::s_RenderCoherentVertexShader =
"#version 430 core\n"
"#extension GL_NV_gpu_shader5: enable\n"
GLOOPS_STRINGIFY(
    layout(location = 0) in vec3 aPosition;

    uniform float uZFar;
    uniform float uZNear;

    out vec3 vProjectedPosition; // Projected position in clip space
    out float vDepth;
    flat out uvec2 vViewportIndex; // Position (i, j) of the viewport in image space

    struct ViewMatrixData {
        const mat4* viewMatrixBuffer;
        uint viewMatrixCount;
    };

    uniform ViewMatrixData uViewMatrixData;

    struct ISMData {
        uint smResolution;
        uint smCountPerTexture;
        uint smCountPerDimension;
    };

    uniform ISMData uISMData;

    uniform float uPointMaxSize;

    void main() {
        // Get the index of the view associated with the input vertex
        uint viewIdx = int(gl_InstanceID);
        mat4 viewMatrix = uViewMatrixData.viewMatrixBuffer[viewIdx];

        vec4 Pvs = viewMatrix * vec4(aPosition, 1); // Position in view space
        Pvs.z = -Pvs.z; // View the negative hemisphere

        // Paraboloid projection
        Pvs /= Pvs.w;

        vec3 v = Pvs.xyz;
        vProjectedPosition.z = Pvs.z;

        float l = length(v);
        v /= l;

        vec3 h = v + vec3(0, 0, 1); // h = k * (x, y, 1)  => x = h.x / h.z   y = h.y / h.z

        vDepth = (l - uZNear) / (uZFar - uZNear);

        // vProjectedPosition is in [-1, 1] x [-1, 1]
        vProjectedPosition.xy = h.xy / h.z;

        //float b = 1.f / (uPointMaxSize - 1);
        //float a = uPointMaxSize * b;

        //gl_PointSize = a / (b + vDepth * vDepth);

        gl_PointSize = max(1, uPointMaxSize * exp(-vDepth));

        // Dimension of a view in clip space
        float size = 2.f / uISMData.smCountPerDimension;

        // 2D coordinates of the viewport attached to the view
        uint i = viewIdx % uISMData.smCountPerDimension;
        uint j = (viewIdx - i) / uISMData.smCountPerDimension;
        vViewportIndex = uvec2(i, j);

        // Coordinates between 0 and 1 in the viewport of the view
        vec2 xy = 0.5 * (vProjectedPosition.xy + vec2(1, 1));

        // Origin of the viewport of the view in clip space
        vec2 origin = vec2(-1, -1) + vec2(i, j) * size;

        gl_Position = vec4(origin + xy * size, vDepth, 1);
    }
);

GLISMRenderer::GLISMRenderer():
    m_RenderProgram(buildProgram(s_RenderVertexShader, s_RenderFragmentShader)),
    m_RenderCoherentProgram(buildProgram(s_RenderCoherentVertexShader, s_RenderFragmentShader)),
    m_uZFar(m_RenderProgram, "uZFar", true),
    m_uZNear(m_RenderProgram, "uZNear", true),
    m_uPointMaxSize(m_RenderProgram, "uPointMaxSize", true),
    m_uDoPacked(m_RenderProgram, "uDoPacked", true),
    m_uPackSize(m_RenderProgram, "uPackSize", true),
    m_uZFarCoherent(m_RenderCoherentProgram, "uZFar", true),
    m_uZNearCoherent(m_RenderCoherentProgram, "uZNear", true),
    m_uPointMaxSizeCoherent(m_RenderCoherentProgram, "uPointMaxSize", true),
    m_uViewMatrixData(m_RenderProgram),
    m_uViewMatrixDataCoherent(m_RenderCoherentProgram),
    m_uISMData(m_RenderProgram),
    m_uISMDataCoherent(m_RenderCoherentProgram){
}

void GLISMRenderer::render(const GLPoints& sampledScene, GLISMContainer& container,
            const glm::mat4* viewMatrices, uint32_t viewMatrixCount,
            float pointMaxSize,
            bool doPull, bool doPullPush,
            float pullTreshold, float pushTreshold,
            int nbLevels) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLint currentFBO;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentFBO);

    container.resize(viewMatrixCount);

    m_RenderProgram.use();

    // Send constant uniforms accros all passes
    m_uZFar.set(getZFar());
    m_uZNear.set(getZNear());

    m_uDoPacked.set(false);

    m_uISMData.set(container);

    m_uISMData.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_uISMData.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_uISMData.smResolution.set(container.getShadowMapResolution());

    m_uPointMaxSize.set(pointMaxSize);

    // Bind the framebuffer
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());

    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MIN_FILTER,
                           GL_NEAREST);
    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MAG_FILTER,
                           GL_NEAREST);

    glTextureImage2DEXT(m_DepthBuffer.glId(),
                        GL_TEXTURE_2D,
                        0,
                        GL_DEPTH_COMPONENT32F,
                        container.getTextureResolution(),
                        container.getTextureResolution(),
                        0,
                        GL_DEPTH_COMPONENT,
                        GL_FLOAT,
                        nullptr);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, m_DepthBuffer.glId(), 0);

    GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &drawBuffer);

    glEnable(GL_PROGRAM_POINT_SIZE);

    glClearColor(1, 1, 1, 1);
    uint32_t viewportResolution = container.getShadowMapCountPerDimension() * container.getShadowMapResolution();
    glViewport(0, 0, viewportResolution, viewportResolution);

    uint32_t viewMatrixCountPerPass = container.getShadowMapCountPerTexture();

    // Rendering of ISMs
    for(auto i = 0u; i < container.getTextureCount(); ++i) {
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                               GL_TEXTURE_2D, container.getTexture(i).glId(), 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Send view matrices to the GPU
        auto viewMatrixOffset = i * viewMatrixCountPerPass;
        m_ViewMatrixBuffer.makeNonResident();
        uint32_t viewCount = glm::min(viewMatrixCount, viewMatrixCountPerPass);
        m_ViewMatrixBuffer.setData(viewCount, viewMatrices + viewMatrixOffset, GL_DYNAMIC_DRAW);
        m_ViewMatrixBuffer.makeResident(GL_READ_ONLY);

        m_uViewMatrixData.viewMatrixBuffer.set(m_ViewMatrixBuffer.getGPUAddress());
        m_uViewMatrixData.viewMatrixCount.set(viewCount);

        sampledScene.render();
        viewMatrixCount -= viewMatrixCountPerPass;
    }

    glClearColor(0, 0, 0, 0);

    glDisable(GL_PROGRAM_POINT_SIZE);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, currentFBO);

    pullPush(container, doPull, doPullPush, pullTreshold, pushTreshold, nbLevels);
}

void GLISMRenderer::renderPacks(const GLPoints& sampledScene, uint32_t pointCountPerISM, GLISMContainer& container,
            const glm::mat4* viewMatrices, uint32_t viewMatrixCount,
            float pointMaxSize,
            bool doPull, bool doPullPush,
            float pullTreshold, float pushTreshold,
            int nbLevels) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLint currentFBO;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentFBO);

    container.resize(viewMatrixCount);

    m_RenderProgram.use();

    // Send constant uniforms accros all passes
    m_uZFar.set(getZFar());
    m_uZNear.set(getZNear());

    m_uDoPacked.set(true);
    m_uPackSize.set(pointCountPerISM);

    m_uISMData.set(container);

    m_uISMData.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_uISMData.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_uISMData.smResolution.set(container.getShadowMapResolution());

    m_uPointMaxSize.set(pointMaxSize);

    // Bind the framebuffer
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());

    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MIN_FILTER,
                           GL_NEAREST);
    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MAG_FILTER,
                           GL_NEAREST);

    glTextureImage2DEXT(m_DepthBuffer.glId(),
                        GL_TEXTURE_2D,
                        0,
                        GL_DEPTH_COMPONENT32F,
                        container.getTextureResolution(),
                        container.getTextureResolution(),
                        0,
                        GL_DEPTH_COMPONENT,
                        GL_FLOAT,
                        nullptr);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, m_DepthBuffer.glId(), 0);

    GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &drawBuffer);

    glEnable(GL_PROGRAM_POINT_SIZE);

    glClearColor(1, 1, 1, 1);
    uint32_t viewportResolution = container.getShadowMapCountPerDimension() * container.getShadowMapResolution();
    glViewport(0, 0, viewportResolution, viewportResolution);

    uint32_t viewMatrixCountPerPass = container.getShadowMapCountPerTexture();

    // Rendering of ISMs
    for(auto i = 0u; i < container.getTextureCount(); ++i) {
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                               GL_TEXTURE_2D, container.getTexture(i).glId(), 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Send view matrices to the GPU
        auto viewMatrixOffset = i * viewMatrixCountPerPass;
        m_ViewMatrixBuffer.makeNonResident();
        uint32_t viewCount = glm::min(viewMatrixCount, viewMatrixCountPerPass);
        m_ViewMatrixBuffer.setData(viewCount, viewMatrices + viewMatrixOffset, GL_DYNAMIC_DRAW);
        m_ViewMatrixBuffer.makeResident(GL_READ_ONLY);

        m_uViewMatrixData.viewMatrixBuffer.set(m_ViewMatrixBuffer.getGPUAddress());
        m_uViewMatrixData.viewMatrixCount.set(viewCount);

        sampledScene.render();
        viewMatrixCount -= viewMatrixCountPerPass;
    }

    glClearColor(0, 0, 0, 0);

    glDisable(GL_PROGRAM_POINT_SIZE);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, currentFBO);

    pullPush(container, doPull, doPullPush, pullTreshold, pushTreshold, nbLevels);
}

void GLISMRenderer::renderCoherent(const GLPoints& sampledScene, GLISMContainer& container,
            const glm::mat4* viewMatrices, uint32_t viewMatrixCount,
            float pointMaxSize,
            bool doPull, bool doPullPush,
            float pullTreshold, float pushTreshold,
            int nbLevels) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLint currentFBO;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentFBO);

    container.resize(viewMatrixCount);

    m_RenderCoherentProgram.use();

    // Send constant uniforms accros all passes
    m_uZFarCoherent.set(getZFar());
    m_uZNearCoherent.set(getZNear());

    m_uISMDataCoherent.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_uISMDataCoherent.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_uISMDataCoherent.smResolution.set(container.getShadowMapResolution());

    m_uPointMaxSizeCoherent.set(pointMaxSize);

    // Bind the framebuffer
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());

    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MIN_FILTER,
                           GL_NEAREST);
    glTextureParameteriEXT(m_DepthBuffer.glId(),
                           GL_TEXTURE_2D,
                           GL_TEXTURE_MAG_FILTER,
                           GL_NEAREST);

    glTextureImage2DEXT(m_DepthBuffer.glId(),
                        GL_TEXTURE_2D,
                        0,
                        GL_DEPTH_COMPONENT32F,
                        container.getTextureResolution(),
                        container.getTextureResolution(),
                        0,
                        GL_DEPTH_COMPONENT,
                        GL_FLOAT,
                        nullptr);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, m_DepthBuffer.glId(), 0);

    GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &drawBuffer);

    glEnable(GL_PROGRAM_POINT_SIZE);

    glClearColor(1, 1, 1, 1);
    uint32_t viewportResolution = container.getShadowMapCountPerDimension() * container.getShadowMapResolution();
    glViewport(0, 0, viewportResolution, viewportResolution);

    uint32_t viewMatrixCountPerPass = container.getShadowMapCountPerTexture();

    // Rendering of ISMs
    for(auto i = 0u; i < container.getTextureCount(); ++i) {
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                               GL_TEXTURE_2D, container.getTexture(i).glId(), 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Send view matrices to the GPU
        auto viewMatrixOffset = i * viewMatrixCountPerPass;
        m_ViewMatrixBuffer.makeNonResident();
        uint32_t viewCount = glm::min(viewMatrixCount, viewMatrixCountPerPass);
        m_ViewMatrixBuffer.setData(viewCount, viewMatrices + viewMatrixOffset, GL_DYNAMIC_DRAW);
        m_ViewMatrixBuffer.makeResident(GL_READ_ONLY);

        m_uViewMatrixDataCoherent.viewMatrixBuffer.set(m_ViewMatrixBuffer.getGPUAddress());
        m_uViewMatrixDataCoherent.viewMatrixCount.set(viewCount);

        sampledScene.render(viewCount);
        viewMatrixCount -= viewMatrixCountPerPass;
    }

    glClearColor(0, 0, 0, 0);

    glDisable(GL_PROGRAM_POINT_SIZE);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, currentFBO);

    pullPush(container, doPull, doPullPush, pullTreshold, pushTreshold, nbLevels);
}

void GLISMRenderer::pullPush(GLISMContainer& container, bool doPull, bool doPullPush,
              float pullTreshold, float pushTreshold,
              int nbLevels) {
    if(doPull) {
        for(auto i = 0u; i < container.getTextureCount(); ++i) {
            pullPass(i, container, pullTreshold, nbLevels);
            if(doPullPush) {
                pushPass(i, container, pushTreshold, nbLevels);
            }
        }
    }
}

const GLchar *GLISMRenderer::PullPass::s_ComputeShader =
"#version 430 core\n"
"#extension GL_NV_gpu_shader5: enable\n"
GLOOPS_STRINGIFY(
    layout(local_size_x = 32, local_size_y = 32) in;

    layout(r32f) coherent uniform image2D uInputImage;
    layout(r32f) coherent uniform image2D uOutputImage;

    uniform float uTreshold;

    void main() {
        ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);

        ivec2 outputSize = imageSize(uOutputImage);

        if(pixel.x >= outputSize.x || pixel.y >= outputSize.y) {
            return;
        }

        float tl = imageLoad(uInputImage, pixel * 2).r;
        float tr = imageLoad(uInputImage, pixel * 2 + ivec2(1, 0)).r;
        float bl = imageLoad(uInputImage, pixel * 2 + ivec2(0, 1)).r;
        float br = imageLoad(uInputImage, pixel * 2 + ivec2(1, 1)).r;

        if(tl + tr + bl + br >= 4.f) {
            imageStore(uOutputImage, pixel, vec4(1.f));
        } else {
            float minimum = min(tl, min(tr, min(bl, br)));

            float sum = 0.f;
            uint count = 0;
            if(tl < 1.f && abs(tl - minimum) <= uTreshold) {
                sum += tl;
                ++count;
            }
            if(tr < 1.f && abs(tr - minimum) <= uTreshold) {
                sum += tr;
                ++count;
            }
            if(bl < 1.f && abs(bl - minimum) <= uTreshold) {
                sum += bl;
                ++count;
            }
            if(br < 1.f && abs(br - minimum) <= uTreshold) {
                sum += br;
                ++count;
            }
            sum /= float(count);
            imageStore(uOutputImage, pixel, vec4(sum));
        }

        memoryBarrier();
    }
);

GLISMRenderer::PullPass::PullPass():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uInputImage(m_Program, "uInputImage", true),
    m_uOutputImage(m_Program, "uOutputImage", true),
    m_uTreshold(m_Program, "uTreshold", true) {
    //m_Program.use();
    //m_uInputImage.set(0);
    //m_uOutputImage.set(1);
}

void GLISMRenderer::pullPass(uint32_t texIdx, GLISMContainer& container, float treshold, int nbLevels) {
    m_PullPass.m_Program.use();

    uint32_t pullPushIdx = 0;

    m_PullPass.m_uInputImage.set(container.getImageHandle(texIdx));
    m_PullPass.m_uOutputImage.set(container.getPullPushImageHandle(0));

    /*
    glBindImageTexture(0, container.getTexture(texIdx).glId(), 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32F);
    glBindImageTexture(1, container.getPullPushTexture(0).glId(), 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32F);
*/

    float scale = container.getShadowMapResolution();
    uint32_t outputTexSize = container.getTextureResolution() / 2;

    for(uint32_t s = container.getShadowMapResolution() / 2; s >= 1; s /= 2) {
        if(nbLevels >= 0 && pullPushIdx == nbLevels) {
            break;
        }

        uint32_t blockSize = 32;
        uint32_t blockCount = outputTexSize / blockSize
                + (outputTexSize % blockSize != 0);

        m_PullPass.m_uTreshold.set(treshold / scale);

        glDispatchCompute(blockCount, blockCount, 1);

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

        m_PullPass.m_uInputImage.set(container.getPullPushImageHandle(pullPushIdx));

        if(s > 1) {
            m_PullPass.m_uOutputImage.set(container.getPullPushImageHandle(pullPushIdx + 1));
        }
        pullPushIdx++;
        outputTexSize /= 2;
        scale /= 2.f;
    }
}

const GLchar *GLISMRenderer::PushPass::s_ComputeShader =
"#version 430 core\n"
"#extension GL_NV_gpu_shader5: enable\n"
GLOOPS_STRINGIFY(
    layout(local_size_x = 32, local_size_y = 32) in;

    layout(r32f) coherent uniform image2D uInputImage;
    layout(r32f) coherent uniform image2D uOutputImage;

    uniform uint uInputSMResolution;

    uniform float uTreshold;

    void main() {
        ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);

        ivec2 outputSize = imageSize(uOutputImage);

        if(pixel.x >= outputSize.x || pixel.y >= outputSize.y) {
            return;
        }

        int k = (pixel.x % 2 != 0) ? 1 : -1;
        int l = (pixel.y % 2 != 0) ? 1 : -1;

        ivec2 coarsePixel = pixel / 2;

        vec4 depths = vec4(imageLoad(uInputImage, coarsePixel).r,
                           imageLoad(uInputImage, coarsePixel + ivec2(k, 0)).r,
                           imageLoad(uInputImage, coarsePixel + ivec2(0, l)).r,
                           imageLoad(uInputImage, coarsePixel + ivec2(k, l)).r);
        vec4 weights = vec4(9, 3, 3, 1) / 16.f;

        float diff = depths.x;

        float sum = 0.f;
        float wsum = 0.f;
        uint count = 0;
        if(depths.x < 1.f) {
            sum += weights.x * depths.x;
            wsum += weights.x;
            ++count;
        }
        if(depths.y < 1.f && uInputSMResolution != 1) {
            bool badCondition = (coarsePixel.x % uInputSMResolution == 0 && k == -1)
                    || (coarsePixel.x % uInputSMResolution == (uInputSMResolution - 1) && k == 1);

            if(!badCondition) {
                sum += weights.y * depths.y;
                wsum += weights.y;
                ++count;
            }
        }
        if(depths.z < 1.f && uInputSMResolution != 1) {
            bool badCondition = (coarsePixel.y % uInputSMResolution == 0 && l == -1)
                    || (coarsePixel.y % uInputSMResolution == (uInputSMResolution - 1) && l == 1);

            if(!badCondition) {
                sum += weights.z * depths.z;
                wsum += weights.z;
                ++count;
            }
        }
        if(depths.w < 1.f && uInputSMResolution != 1) {
            bool badCondition = (coarsePixel.x % uInputSMResolution == 0 && k == -1)
                    || (coarsePixel.x % uInputSMResolution == (uInputSMResolution - 1) && k == 1)
                    || (coarsePixel.y % uInputSMResolution == 0 && l == -1)
                    || (coarsePixel.y % uInputSMResolution == (uInputSMResolution - 1) && l == 1);

            if(!badCondition) {
                sum += weights.w * depths.w;
                wsum += weights.w;
                ++count;
            }
        }

        if(count == 0) {
            imageStore(uOutputImage, pixel, vec4(1.f));
        } else {
            float depth = sum / wsum;

//            weights /= wsum;
//            float depth = dot(depths, weights);
//            depth /= wsum;

            float v = imageLoad(uOutputImage, pixel).r;
            //diff = abs(v - diff);

            if(v < 1.0f && (v < diff || abs(v - diff) < uTreshold)) {
                return;
            }
            imageStore(uOutputImage, pixel, vec4(depth));
        }

        memoryBarrier();
    }
);

GLISMRenderer::PushPass::PushPass():
    m_Program(buildComputeProgram(s_ComputeShader)),
    m_uInputImage(m_Program, "uInputImage", true),
    m_uOutputImage(m_Program, "uOutputImage", true),
    m_uTreshold(m_Program, "uTreshold", true),
    m_uInputSMResolution(m_Program, "uInputSMResolution", true) {
}

void GLISMRenderer::pushPass(uint32_t texIdx, GLISMContainer& container, float treshold, int nbLevels) {
    m_PushPass.m_Program.use();

    uint32_t pullPushIdx = container.getPullPushTextureCount() - 1;

    // First input is 1x1 for each SM, so the output is 2x2 for each SM
    // Giving (2*nbSM)x(2*nbSM) for the complete texture
    uint32_t outputTexSize = container.getShadowMapCountPerDimension() * 2;

    uint32_t startRes = 2;
    float scale = 1;//container.getShadowMapResolution() / 2;

    if(nbLevels >= 0) {
        pullPushIdx = nbLevels - 1;
        startRes = 2 * container.getShadowMapResolution() / (1 << nbLevels);
        scale = startRes / 2;
        outputTexSize = container.getShadowMapCountPerDimension() * startRes;
    }

    for(uint32_t s = startRes; s <= container.getShadowMapResolution(); s *= 2) {
        m_PushPass.m_uInputImage.set(container.getPullPushImageHandle(pullPushIdx));

        if(pullPushIdx > 0) {
            m_PushPass.m_uOutputImage.set(container.getPullPushImageHandle(pullPushIdx - 1));
        } else {
            m_PushPass.m_uOutputImage.set(container.getImageHandle(texIdx));
        }

        m_PushPass.m_uTreshold.set(treshold / scale);
        m_PushPass.m_uInputSMResolution.set(s / 2);

        uint32_t blockSize = 32;
        uint32_t blockCount = outputTexSize / blockSize
                + (outputTexSize % blockSize != 0);

        glDispatchCompute(blockCount, blockCount, 1);

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

        pullPushIdx--;
        outputTexSize *= 2;
        scale *= 2.f;
    }

    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}


const GLchar* GLISMDrawer::s_VertexShader =
"#version 430\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

// Origin of the shadow map in texture space
uniform float uSMX;
uniform float uSMY;

// Resolution of the shadow map in texture space
uniform float uRes;

out vec2 vTexCoords;

void main() {
    vTexCoords = 0.5f * (aPosition + vec2(1, 1)); // between 0 and 1
    vTexCoords = vec2(uSMX, uSMY) + vTexCoords * uRes; // In the correct shadow map

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLISMDrawer::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform sampler2D uTexture;

uniform float uRes;

struct ISMData {
    uint smResolution;
    uint smCountPerTexture;
    uint smCountPerDimension;
};

uniform ISMData uISMData;
uniform uint uTexIndex;

vec3 randomColor(uint value) {
    return fract(
                sin(
                    vec3(float(value) * 12.9898,
                         float(value) * 78.233,
                         float(value) * 56.128)
                    )
                * 43758.5453
                );
}

in vec2 vTexCoords;

out vec3 fColor;

void main() {
    uint x = uint(vTexCoords.x * uISMData.smCountPerDimension);
    uint y = uint(vTexCoords.y * uISMData.smCountPerDimension);
    uint viewIdx = uTexIndex * uISMData.smCountPerTexture + y * uISMData.smCountPerDimension + x;

    vec3 color = vec3(1);

    float depth = texture(uTexture, vTexCoords).r;
    fColor = color * depth;
}

);

void GLISMDrawer::drawISMTexture(const GLISMContainer& container,
                    uint32_t texIdx, uint32_t texLvl, int textureUnit) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    glActiveTexture(GL_TEXTURE0 + textureUnit);

    if(texLvl == 0) {
        glBindTexture(GL_TEXTURE_2D, container.getTexture(texIdx).glId());
    } else {
        glBindTexture(GL_TEXTURE_2D, container.getPullPushTexture(texLvl - 1).glId());
    }

    m_Program.use();

    uSMX.set(0.f);
    uSMY.set(0.f);
    uRes.set(1.f);
    uTexture.set(textureUnit);
    uTexIndex.set(texIdx);
    m_uISMData.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_uISMData.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_uISMData.smResolution.set(container.getShadowMapResolution());

    m_ScreenTriangle.render();
}

void GLISMDrawer::drawISM(const GLISMContainer& container,
                          uint32_t ismIdx, uint32_t texLvl, int textureUnit) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    glActiveTexture(GL_TEXTURE0 + textureUnit);

    uint32_t smLocalIdx = ismIdx % container.getShadowMapCountPerTexture();
    uint32_t texIdx = (ismIdx - smLocalIdx) / container.getShadowMapCountPerTexture();

    uint32_t smX = smLocalIdx % container.getShadowMapCountPerDimension();
    uint32_t smY = (smLocalIdx - smX) / container.getShadowMapCountPerDimension();

    if(texLvl == 0) {
        glBindTexture(GL_TEXTURE_2D, container.getTexture(texIdx).glId());
    } else {
        glBindTexture(GL_TEXTURE_2D, container.getPullPushTexture(texLvl - 1).glId());
    }

    m_Program.use();

    float rcp = 1.f / container.getShadowMapCountPerDimension();

    uSMX.set((float) smX * rcp);
    uSMY.set((float) smY * rcp);
    uRes.set(rcp);
    uTexture.set(textureUnit);
    uTexIndex.set(texIdx);
    m_uISMData.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_uISMData.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_uISMData.smResolution.set(container.getShadowMapResolution());

    m_ScreenTriangle.render();
}

void GLISMDrawer::drawISM(const GLISMContainer& container,
                          uint32_t ismIdx, int textureUnit) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    glActiveTexture(GL_TEXTURE0 + textureUnit);

    uint32_t smLocalIdx = ismIdx % container.getShadowMapCountPerTexture();
    uint32_t texIdx = (ismIdx - smLocalIdx) / container.getShadowMapCountPerTexture();

    uint32_t smX = smLocalIdx % container.getShadowMapCountPerDimension();
    uint32_t smY = (smLocalIdx - smX) / container.getShadowMapCountPerDimension();

    glBindTexture(GL_TEXTURE_2D, container.getTexture(texIdx).glId());

    m_Program.use();

    float rcp = 1.f / container.getShadowMapCountPerDimension();

    uSMX.set((float) smX * rcp);
    uSMY.set((float) smY * rcp);
    uRes.set(rcp);
    uTexture.set(textureUnit);
    uTexIndex.set(texIdx);
    m_uISMData.smCountPerDimension.set(container.getShadowMapCountPerDimension());
    m_uISMData.smCountPerTexture.set(container.getShadowMapCountPerTexture());
    m_uISMData.smResolution.set(container.getShadowMapResolution());

    m_ScreenTriangle.render();
}

}
