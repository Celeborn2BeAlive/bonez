#include "GLShadowMapRenderer.hpp"
#include "shaders/GLSLProgramBuilder.hpp"

#include "bonez/opengl/utils/GLutils.hpp"

namespace BnZ {

const GLchar* GLShadowMapDrawer::s_VertexShader =
"#version 430\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

// Origin of the shadow map in texture space
uniform float uSMX;
uniform float uSMY;

// Resolution of the shadow map in texture space
uniform float uResX;
uniform float uResY;

out vec2 vTexCoords;

void main() {
    vTexCoords = 0.5f * (aPosition + vec2(1, 1)); // between 0 and 1
    vTexCoords = vec2(uSMX, uSMY) + vTexCoords * vec2(uResX, uResY); // In the correct shadow map

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLShadowMapDrawer::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform sampler2D uTexture;

in vec2 vTexCoords;

out vec3 fColor;

void main() {
    float depth = texture(uTexture, vTexCoords).r;
    fColor = vec3(depth, depth, depth);
}

);

GLShadowMapDrawer::GLShadowMapDrawer():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uSMX(m_Program, "uSMX", true),
    m_uSMY(m_Program, "uSMY", true),
    m_uResX(m_Program, "uResX", true),
    m_uResY(m_Program, "uResY", true),
    m_uTexture(m_Program, "uTexture", true) {
}

void GLShadowMapDrawer::drawShadowMap(const GLShadowMapContainer& container, uint32_t idx,
                                      int textureUnit) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    glActiveTexture(GL_TEXTURE0 + textureUnit);

    uint32_t smX = idx % container.m_nSMCountX;
    uint32_t smY = ((idx - smX) / container.m_nSMCountX) % container.m_nSMCountY;
    uint32_t texIdx = (idx - smX - smY * container.m_nSMCountX) / container.m_nSMCountPerTexture;

    glBindTexture(GL_TEXTURE_2D, container.m_Textures[texIdx].glId());

    m_Program.use();

    m_uSMX.set((float) smX / container.m_nTexWidth);
    m_uSMY.set((float) smY / container.m_nTexHeight);
    m_uResX.set((float) container.m_nResX / container.m_nTexWidth);
    m_uResY.set((float) container.m_nResY / container.m_nTexHeight);
    m_uTexture.set(textureUnit);

    m_ScreenTriangle.render();
}

const GLchar* GLOmniShadowMapRenderer::s_VertexShader =
"#version 430\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec3 aPosition;

uniform mat4 uMVMatrix;
uniform float uZFar;
uniform float uZNear;

out vec3 vP;
out float vDepth;
flat out uvec2 vViewportIdx;

uniform samplerBuffer uLights;

void main() {
    int N = 2;
    int view = gl_VertexID % (N * N);

    // On calcule uMVMatrix en fonction de view
    // texelFecth(uLight, view);

    vec4 Pvs = uMVMatrix * vec4(aPosition, 1);
    Pvs.z = -Pvs.z; // Negative hemisphere

    Pvs /= Pvs.w;

    vec3 v = Pvs.xyz;
    vP.z = Pvs.z;

    float l = length(v);
    v /= l;

    vec3 h = v + vec3(0, 0, 1); // h = k * (x, y, 1)  => x = h.x / h.z   y = h.y / h.z

    vDepth = (l - uZNear) / (uZFar - uZNear);

    gl_PointSize = min(8, 0.01f / (vDepth * vDepth));

    vP.xy = vec2(h.x / h.z, h.y / h.z);

    float width = 2.f / N;
    float height = 2.f / N;

    int i = view % N;
    int j = (view - i) / N;
    vViewportIdx = uvec2(i, j);

    vec2 xy = 0.5 * (vP.xy + vec2(1, 1)); // Between 0 and 1
    vec2 origin = vec2(-1, -1) + vec2(i, j) * vec2(width, height);

    gl_Position = vec4(origin + xy * vec2(width, height), vDepth, 1);
}

);

const GLchar* GLOmniShadowMapRenderer::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

in vec3 vP;
in float vDepth;
flat in uvec2 vViewportIdx;

out float fDepth;

//out float fDepth[32];

void main() {
    if(vP.z >= 0.f) {

        uvec2 viewportOrigin = vViewportIdx * 64;
        uvec2 pixel = uvec2(gl_FragCoord.xy);

        if(pixel.x < viewportOrigin.x || pixel.x >= viewportOrigin.x + 64
                || pixel.y < viewportOrigin.y || pixel.y >= viewportOrigin.y + 64) {
            discard;
        } else {
            fDepth = vDepth;
        }


    } else {
        discard;
    }
}

);

GLOmniShadowMapRenderer::GLOmniShadowMapRenderer():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uMVMatrix(m_Program, "uMVMatrix", true),
    m_uZFar(m_Program, "uZFar", true),
    m_uZNear(m_Program, "uZNear", true) {
}

// Render count shadow maps with view matrices given in parameter
void GLOmniShadowMapRenderer::render(const GLScene& scene, GLShadowMapContainer& container,
                                 const glm::mat4* viewMatrices, uint32_t count) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLTimer timer(m_ElapsedTimeQuery);

    m_Program.use();

    m_uZFar.set(getZFar());
    m_uZNear.set(getZNear());

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());

    glBindTexture(GL_TEXTURE_2D, m_DepthBuffer.glId());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, container.m_nResX, container.m_nResY, 0,
                 GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, m_DepthBuffer.glId(), 0);

    GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &drawBuffer);

    for(uint32_t i = 0; i < count; ++i) {
        if(i % container.m_nSMCountPerTexture == 0) {
            uint32_t texIdx = i / container.m_nSMCountPerTexture;

            glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                   GL_TEXTURE_2D, container.m_Textures[texIdx].glId(), 0);

            glClear(GL_COLOR_BUFFER_BIT);
        }

        glClear(GL_DEPTH_BUFFER_BIT);

        uint32_t smX = i % container.m_nSMCountX;
        uint32_t smY = ((i - smX) / container.m_nSMCountX) % container.m_nSMCountY;

        glViewport(smX * container.m_nResX, smY * container.m_nResY, container.m_nResX, container.m_nResY);

        m_uMVMatrix.set(viewMatrices[i]);

        scene.render();
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void GLOmniShadowMapRenderer::render(const GLPoints& sampledScene, GLShadowMapContainer& container,
            const glm::mat4* viewMatrices, uint32_t count) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLTimer timer(m_ElapsedTimeQuery);

    m_Program.use();

    m_uZFar.set(getZFar());
    m_uZNear.set(getZNear());

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());

    glBindTexture(GL_TEXTURE_2D, m_DepthBuffer.glId());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, container.m_nResX, container.m_nResY, 0,
                 GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, m_DepthBuffer.glId(), 0);

    GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &drawBuffer);

    glEnable(GL_PROGRAM_POINT_SIZE);

    glClearColor(1, 1, 1, 1);

    for(uint32_t i = 0; i < count; ++i) {
        if(i % container.m_nSMCountPerTexture == 0) {
            uint32_t texIdx = i / container.m_nSMCountPerTexture;

            glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                   GL_TEXTURE_2D, container.m_Textures[texIdx].glId(), 0);

            glClear(GL_COLOR_BUFFER_BIT);
        }

        glClear(GL_DEPTH_BUFFER_BIT);

        uint32_t smX = i % container.m_nSMCountX;
        uint32_t smY = ((i - smX) / container.m_nSMCountX) % container.m_nSMCountY;

        glViewport(smX * container.m_nResX, smY * container.m_nResY, container.m_nResX, container.m_nResY);

        m_uMVMatrix.set(viewMatrices[i]);

        sampledScene.render();
    }

    glClearColor(0, 0, 0, 0);

    glDisable(GL_PROGRAM_POINT_SIZE);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void GLOmniShadowMapRenderer::render(const GLScene& scene, GLCubeShadowMapContainer& container,
                                 const glm::mat4* viewMatrices, uint32_t count) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, true);
    gloops::StateRestorer<GL_VIEWPORT> viewport;

    GLTimer timer(m_ElapsedTimeQuery);

    m_Program.use();

    m_uZFar.set(getZFar());
    m_uZNear.set(getZNear());

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_FBO.glId());

    glBindTexture(GL_TEXTURE_2D, m_DepthBuffer.glId());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, container.m_nRes, container.m_nRes, 0,
                 GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, m_DepthBuffer.glId(), 0);

    GLenum drawBuffer = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &drawBuffer);

    for(uint32_t i = 0; i < count; ++i) {
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                               GL_TEXTURE_CUBE_MAP_POSITIVE_X, container.m_Textures[i].glId(), 0);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glViewport(0, 0, container.m_nRes, container.m_nRes);

        m_uMVMatrix.set(viewMatrices[i]);

        scene.render();
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

const GLchar* GLCubeShadowMapRenderer::s_VertexShader =
"#version 430\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec3 aPosition;

uniform mat4 uProjectionMatrix;
uniform mat4 uViewMatrix;

void main() {
    gl_Position = vec4(aPosition, 1);
}

);

const GLchar* GLCubeShadowMapRenderer::s_GeometryShader =
"#version 430\n"
GLOOPS_STRINGIFY(

layout(triangles) in;
layout(triangle_strip, max_vertices = 18) out;

uniform mat4 uFaceMVPMatrices[6];

void main() {
    for(int face = 0; face < 6; ++face) {
        gl_Layer = face;
        for(int i = 0; i < 3; ++i) {
            gl_Position = uFaceMVPMatrices[face] * gl_in[i].gl_Position;
            EmitVertex();
        }
        EndPrimitive();
    }
}

);

const GLchar* GLCubeShadowMapRenderer::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

void main() {
}

);

GLCubeShadowMapRenderer::GLCubeShadowMapRenderer():
    m_Program(buildProgram(s_VertexShader, s_GeometryShader,
                           s_FragmentShader)),
    m_uFaceMVPMatrices(m_Program, "uFaceMVPMatrices", true) {

    glm::mat4 projectionMatrix = glm::perspective(90.f, 1.f, getZNear(), getZFar());

    m_FaceProjectionMatrices[0] = glm::rotate(glm::rotate(projectionMatrix, -90.f, glm::vec3(0, 1, 0)), 180.f, glm::vec3(0, 0, 1)); // +X
    m_FaceProjectionMatrices[1] = glm::rotate(glm::rotate(projectionMatrix, -270.f, glm::vec3(0, 1, 0)), 180.f, glm::vec3(0, 0, 1)); // -X
    m_FaceProjectionMatrices[2] = glm::rotate(projectionMatrix, -90.f, glm::vec3(1, 0, 0)); // +Y
    m_FaceProjectionMatrices[3] = glm::rotate(projectionMatrix, 90.f, glm::vec3(1, 0, 0)); // -Y
    m_FaceProjectionMatrices[4] = glm::rotate(glm::rotate(projectionMatrix, -180.f, glm::vec3(0, 1, 0)), 180.f, glm::vec3(0, 0, 1)); // +Z
    m_FaceProjectionMatrices[5] = glm::rotate(projectionMatrix, 180.f, glm::vec3(0, 0, 1)); // -Z
}

void GLCubeShadowMapRenderer::render(const GLScene& scene, GLCubeShadowMapContainer& container,
                                 const glm::mat4* viewMatrices, uint32_t count) {
    auto depthTest = pushGLState<GL_DEPTH_TEST>();
    depthTest.set(true);

    auto viewport = pushGLState<GL_VIEWPORT>();
    viewport.set(glm::vec4(0, 0, container.m_nRes, container.m_nRes));

    auto fb = pushGLState<GL_DRAW_FRAMEBUFFER_BINDING>();
    fb.set(m_FBO.glId());

    m_Program.use();

    glm::mat4 faceMVPMatrices[6];

    glDrawBuffer(GL_NONE);

    for(uint32_t idx = 0; idx < count; ++idx) {
        glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, container.m_Textures[idx].glId(), 0);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        for(int i = 0; i < 6; ++i) {
            faceMVPMatrices[i] = m_FaceProjectionMatrices[i] * viewMatrices[idx];
        }
        m_uFaceMVPMatrices.setMatrix4(6, GL_FALSE, glm::value_ptr(faceMVPMatrices[0]));

        GLenum status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
        if(GL_FRAMEBUFFER_COMPLETE != status) {
            std::cerr << gloops::framebufferErrorString(status) << std::endl;
            return;
        }

        scene.render();
    }
}



const GLchar* GLCubeShadowMapDrawer::s_VertexShader =
"#version 430\n"
GLOOPS_STRINGIFY(

layout(location = 0) in vec2 aPosition;

out vec2 vTexCoords;

void main() {
    vTexCoords = aPosition;

    gl_Position = vec4(aPosition, 0, 1);
}

);

const GLchar* GLCubeShadowMapDrawer::s_FragmentShader =
"#version 430\n"
GLOOPS_STRINGIFY(

uniform samplerCube uTexture;
uniform float uZNear;
uniform float uZFar;

in vec2 vTexCoords;

out vec3 fColor;

int getFaceIdx(vec3 wi) {
    vec3 absWi = abs(wi);

    float maxComponent = max(absWi.x, max(absWi.y, absWi.z));

    if(maxComponent == absWi.x) {
        if(wi.x > 0) {
            return 0;
        }
        return 1;
    }

    if(maxComponent == absWi.y) {
        if(wi.y >= 0) {
            return 2;
        }
        return 3;
    }

    if(maxComponent == absWi.z) {
        if(wi.z > 0) {
            return 4;
        }
        return 5;
    }

    return -1;
}

vec3 getFaceColor(int face) {
    switch(face) {
    case 0: // +X
        return vec3(1, 0, 0);
    case 1: // -X
        return vec3(0, 1, 0);
    case 2: // +Y
        return vec3(0, 0, 1);
    case 3: // -Y
        return vec3(1, 1, 0);
    case 4: // +Z
        return vec3(1, 0, 1);
    case 5: // -Z
        return vec3(0, 1, 1);
    }
    return vec3(0, 0, 0);
}

vec3 getDirection() {
    float PI = 3.14;
    float HALF_PI = 0.5 * PI;
    float QUARTER_PI = 0.25 * PI;

    float s = 0.5 * (vTexCoords.x + 1);
    float t = 0.5 * (vTexCoords.y + 1);
    float phi = QUARTER_PI + s * 2 * PI;
    float theta = PI - t * PI;

    return vec3(sin(phi) * sin(theta), cos(theta), cos(phi) * sin(theta));

//    if(vTexCoords.x <= -0.5 && abs(vTexCoords.y) <= 0.33) {
//        // +Z
//        float x = -1 + 4 * (1 + vTexCoords.x);
//        float y = -3 * vTexCoords.y;
//        float z = 1;

//        return vec3(x, y, z);
//    }

//    if(vTexCoords.x <= 0.0 && abs(vTexCoords.y) <= 0.33) {
//        // -X
//        float x = -1;
//        float y = -3 * vTexCoords.y;
//        float z = -1 + 4 * (0.5 + vTexCoords.x);
//        return vec3(x, y, z);
//    }

//    if(vTexCoords.x <= 0.5 && abs(vTexCoords.y) <= 0.33) {
//        // -Z
//        float x = -(-1 + 4 * (0.0 + vTexCoords.x));
//        float y = -3 * vTexCoords.y;
//        float z = -1;
//        return vec3(x, y, z);
//    }

//    if(vTexCoords.x <= 1.0 && abs(vTexCoords.y) <= 0.33) {
//        // +X
//        float x = 1;
//        float y = -3 * vTexCoords.y;
//        float z = -(-1 + 4 * (-0.5 + vTexCoords.x));
//        return vec3(x, y, z);
//    }

//    if(vTexCoords.x > 0.0 && vTexCoords.x <= 0.5 && vTexCoords.y > 0.33) {
//        // +Y
//        float x = -1 + 4 * vTexCoords.x;
//        float y = 1;
//        float z = -1 + 3 * (vTexCoords.y - 0.33);
//        return vec3(x, y, z);
//    }

//    if(vTexCoords.x > 0.0 && vTexCoords.x <= 0.5 && vTexCoords.y <= -0.33) {
//        // -Y
//        float x = -1 + 4 * vTexCoords.x;
//        float y = -1;
//        float z = -(-1 + 3 * (vTexCoords.y + 0.99));
//        return vec3(x, y, z);
//    }

//    return vec3(0, 0, 0);
}

void main() {
    vec3 d = getDirection();
    //fColor = d;
    //return;

    //int face = getFaceIdx(d);
    //fColor = getFaceColor(face);

    //return;

    if(d == vec3(0, 0, 0)) {
        fColor = d;
    } else {
        float depth = texture(uTexture, d).r;
        fColor = vec3((2.f * uZNear) / (uZFar + uZNear - depth * (uZFar - uZNear)));
    }
}

);

GLCubeShadowMapDrawer::GLCubeShadowMapDrawer():
    m_Program(buildProgram(s_VertexShader,
                           s_FragmentShader)),
    m_uTexture(m_Program, "uTexture", true),
    m_uZNear(m_Program, "uZNear", true),
    m_uZFar(m_Program, "uZFar", true) {
}

void GLCubeShadowMapDrawer::drawShadowMap(const GLCubeShadowMapContainer& container, uint32_t idx,
                                      int textureUnit) {
    gloops::CapabilityRestorer depthTest(GL_DEPTH_TEST, false);
    glActiveTexture(GL_TEXTURE0 + textureUnit);

    container.bindForDrawing(idx);

    m_Program.use();

    m_uTexture.set(textureUnit);
    m_uZNear.set(getZNear());
    m_uZFar.set(getZFar());

    m_ScreenTriangle.render();
}


}
