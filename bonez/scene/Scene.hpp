#ifndef _BONEZ_SCENE_SCENE_HPP_
#define _BONEZ_SCENE_SCENE_HPP_

#include <cassert>
#include <vector>
#include <embree/default.h>
#include <embree/rtcore/common/accel.h>
#include <embree/rtcore/common/intersector.h>
#include <embree/rtcore/triangle/triangles.h>
#include <embree/rtcore/bvh4/bvh4_intersector.h>

#include "Intersection.hpp"
#include "shading/Material.hpp"
#include "shading/BRDF.hpp"

#include "geometry/SceneGeometry.hpp"
#include "lights/LightContainer.hpp"
#include "VoxelSpace.hpp"

#include "bonez/common/maths.hpp"

namespace BnZ {

class Scene {
public:
    typedef std::vector<Material> MaterialContainer;

    SceneGeometry geometry;
    MaterialContainer materials;
    std::unique_ptr<VoxelSpace> voxelSpace;
    LightContainer lights;
    
    Scene() {
    }

    Scene(Scene&& scene):
        geometry(std::move(scene.geometry)),
        materials(std::move(scene.materials)),
        voxelSpace(std::move(scene.voxelSpace)),
        lights(std::move(scene.lights)) {
    }

    Scene& operator =(Scene&& scene) {
        geometry = std::move(scene.geometry);
        materials = std::move(scene.materials);
        voxelSpace = std::move(scene.voxelSpace);
        lights = std::move(scene.lights);

        return *this;
    }
    
    void preprocess(const char* accelTy = "default", const char* triTy = "default") {
        std::vector<embree::BuildTriangle> triangles;
        std::vector<embree::BuildVertex> vertices;
        size_t vertexOffset = 0;

        for (size_t meshIdx = 0; meshIdx < geometry.size(); ++meshIdx) {
            size_t vertexCount = geometry[meshIdx].vertexCount();

            for (size_t vertexIdx = 0; vertexIdx < vertexCount; ++vertexIdx) {
                Vec3f position = geometry[meshIdx].getVertex(vertexIdx).position;
                vertices.emplace_back(position[0], position[1], position[2]);
            }

            size_t triangleCount = geometry[meshIdx].triangleCount();

            for (size_t triangleIdx = 0; triangleIdx < triangleCount; ++triangleIdx) {
                const TriangleMesh::Triangle& triangle = geometry[meshIdx].getTriangle(triangleIdx);
                triangles.emplace_back(vertexOffset + triangle.first,
                                       vertexOffset + triangle.second, vertexOffset + triangle.third, meshIdx, triangleIdx);
            }

            vertexOffset += vertexCount;
        }

        m_Accel = embree::rtcCreateAccel(accelTy, triTy, triangles.data(),
                    triangles.size(), vertices.data(), vertices.size(), embree::empty, false);
        m_Intersector = m_Accel->queryInterface<embree::Intersector>();

        /*
        if(topology) {
            topology.clustering().buildVisibilityGraph(*this);
        }*/
    }

    /**
     * Test if a ray is occluded by some geometry of the scene
     */
    bool occluded(const embree::Ray& ray) const {
        return m_Intersector->occluded(ray);
    }

    /**
     * Find the first intersection of the ray with the geometry of the scene
     */
    void intersect(const embree::Ray& ray, embree::Hit& hit) const {
        m_Intersector->intersect(ray, hit);
    }

    /**
     * Return the informations of a surface point defined by a hit computed by intersect.
     */
    Intersection postIntersect(const embree::Ray& ray, const embree::Hit& hit) const {
        assert(hit);
        assert(hit.id0 < (int) geometry.size());
        
        SurfacePoint P = geometry[hit.id0].getSurfacePoint(hit.id1, hit.u, hit.v);
        
        P.material = getMaterial(geometry[hit.id0].getMaterialIndex());
        P.light = geometry[hit.id0].getLight().get();
        
        Col3f Le(0.f);
        if(P.light) {
            Le = P.light->Le(P, -ray.dir);
        }
        
        return Intersection(P, hit, Le);
    }
    
    BRDF shade(const Intersection& intersection) const {
        return BRDF(intersection);
    }
    
    const Material* getMaterial(size_t idx) const {
        if(idx >= materials.size()) {
            return &m_DefaultMaterial;
        }
        return &materials[idx];
    }
    
    /**
     * Given an incoming direction from the outside of the scene, sample the origin of
     * an incoming ray with that direction.
     */
    Sample3f sampleRayOrigin(const Vec3f& wo, const Vec2f& random) const {
        Vec3f center;
        float radius;
        boundingSphere(geometry.boundingBox, center, radius);
        
        Vec2f diskSample = embree::uniformSampleDisk(random, radius);
        
        LinearSpace3f space = coordinateSystem(wo);

        return Sample3f(center + diskSample.x * space.vx + diskSample.y * space.vz - radius * wo,
            embree::rcp(radius * radius * float(embree::pi)));
    }

private:
    // embree data for global illumination
    embree::Ref<embree::Accel> m_Accel;
    embree::Ref<embree::Intersector> m_Intersector;
    
    Material m_DefaultMaterial;
};

// Compute an error offset that must be add along a secondary ray to avoid self-intersections
__forceinline float error(const Intersection& intersection) {
    return embree::max(embree::abs(intersection.hit.t),
                       embree::reduce_max(embree::abs(intersection.P)));
}

// Compute an error offset that must be add along a ray starting from a surface point to avoid self-intersections
__forceinline float error(const Vec3f& position) {
    return embree::reduce_max(embree::abs(position));
}

// Compute an intersection by throwing a ray in the scene
__forceinline Intersection intersect(const embree::Ray& ray, const Scene& scene) {
    embree::Hit hit;
    scene.intersect(ray, hit);

    if (!hit) {
        return Intersection();
    }

    Intersection intersection = scene.postIntersect(ray, hit);

    if (embree::dot(intersection.Ns, -ray.dir) < 0.f) {
        intersection.Ns = -intersection.Ns;
        intersection.light = nullptr;
    }

    return intersection;
}

// Compute a secondary ray that start at an existing intersection for a given direction
// (an error factor is added to the ray's origin to prevent self-intersections)
__forceinline embree::Ray secondary_ray(const Intersection& I, const embree::Vec3f& wi) {
    return embree::Ray(I.P, wi, error(I) * 128.f * float(embree::ulp));
}

__forceinline embree::Ray secondary_ray(const Vec3f& P, const embree::Vec3f& wi) {
    return embree::Ray(P, wi, error(P) * 128.f * float(embree::ulp));
}

// Compute a ray that start at a light point in a given output direction
// (an error factor is added to the ray's origin to prevent self-intersections)
__forceinline embree::Ray light_ray(const Vec3f& position, const embree::Vec3f& wo) {
    return embree::Ray(position, wo, error(position) * 128.f * float(embree::ulp));
}

// Compute a shadow ray that start at an existing intersection for a given direction and a length to the light sample
// (an error factor is added to the ray's origin/arrival to prevent self-intersections)
__forceinline embree::Ray shadow_ray(const Intersection& I, const embree::Vec3f& wi, float length) {
    float err =  error(I);
    return embree::Ray(I.P, wi, err *
                       128.f * float(embree::ulp), length - err * 128.f * float(embree::ulp));
}

__forceinline embree::Ray shadow_ray(const Vec3f& P, const Vec3f& Q) {
    float err =  error(P);
    Vec3f wi = Q - P;
    float length = embree::length(wi);
    wi /= length;
    return embree::Ray(P, wi, err *
                       128.f * float(embree::ulp), length - err * 128.f * float(embree::ulp));
}

__forceinline embree::Ray shadow_ray(const SurfacePoint& P, const embree::Vec3f& wi, float length) {
    float err =  error(P.P);
    return embree::Ray(P.P, wi, err *
                       128.f * float(embree::ulp), length - err * 128.f * float(embree::ulp));
}

// Compute an intersection that result from a bounce in the scene
// (an error factor is added to the ray's origin to prevent self-intersections)
__forceinline Intersection bounce(const Intersection& position, const embree::Vec3f& wi, const Scene& scene) {
    return intersect(secondary_ray(position, wi), scene);
}


__forceinline Intersection intersect(const Ray& ray, const Scene& scene, RayCounter& rayCounter) {
    embree::Hit hit;
    scene.intersect(ray, hit);

    if (!hit) {
        return Intersection();
    }

    Intersection intersection = scene.postIntersect(ray, hit);

    if (embree::dot(intersection.Ns, -ray.dir) < 0.f) {
        intersection.Ns = -intersection.Ns;
        intersection.Le = Col3f(0.f);
    }

    return intersection;
}

__forceinline bool occluded(const Ray& ray, const Scene& scene, RayCounter& rayCounter) {
    return scene.occluded(ray);
}

__forceinline Intersection bounce(const Intersection& position, const embree::Vec3f& wi, const Scene& scene, RayCounter& rayCounter) {
    return intersect(secondary_ray(position, wi), scene, rayCounter);
}

}

#endif
