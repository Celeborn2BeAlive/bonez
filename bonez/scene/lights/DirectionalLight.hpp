#ifndef _BONEZ_LIGHTS_DIRECTIONALLIGHT_HPP_
#define _BONEZ_LIGHTS_DIRECTIONALLIGHT_HPP_

#include "bonez/common/common.hpp"
#include "bonez/scene/Intersection.hpp"

namespace BnZ {

#if 0
class Scene;

class DirectionalLight {
public:
    DirectionalLight(const Col3f& L, const Vec3f& direction):
        L(L), direction(embree::normalize(direction)) {
    }
    
    /**
     * Sample an incoming direction at an intersection point coming from the light source.
     * \param intersection The intersection point.
     * \param out wi Contains the incoming direction at the intersection and its pdf after the call.
     * \param out distance Contains the maximal distance to test visibility after the call.
     */
    __forceinline Col3f sampleWi(const Intersection& intersection, Sample3f& wi, float& distance) const {
        wi = Sample3f(-direction, 1.f);
        distance = embree::inf;
        return L;
    }
    
    Col3f sampleRay(const Scene& scene, float r0, float r1, 
        Sample3f& P, Vec3f& N, Sample3f& wo);
    
    /**
     * Compute an upper bound of the power leaving the light source towards the scene.
     * \return The computed power.
     */
    Col3f power(const Scene& scene) const;

    Col3f L; // The radiance emitted by the light
    Vec3f direction; // The direction of emission
};
#endif
};

#endif
