#include "LightContainer.hpp"
#include "../Scene.hpp"

#include "bonez/scene/lights/vpls.hpp"

namespace BnZ {

void LightContainer::preprocess(const Scene& scene) {
    for(const LightPtr& light: m_Lights) {
        Col3f power = light->getPowerUpperBound(scene);
        m_PowerUpperBound += power;
        m_Distribution.add(embree::luminance(power));
        
        if(light->isGlobalLight()) {
            m_GlobalLights.push_back(light);
        } else {
            m_LocalLights.push_back(light);
            m_LocalDistribution.add(embree::luminance(power));
        }
    }
}

/**
 * Compute an upper bound of the power emitted by the light that affect the
 * scene.
 */
Col3f LightContainer::getPowerUpperBound(const Scene& scene) const {
    return m_PowerUpperBound;
}

/**
 * Sample an incident ray at a surface point for the computation of direct illumination.
 * The sampled ray must be tested against occlusions by the caller.
 */
Col3f LightContainer::sampleIncidentRay(const Scene& scene, const SurfacePoint& point, 
    const IncidentRayRandom& random, RaySample& wi) const {
    Sample1ui emitterSample = m_Distribution.sample(random.light);
    Col3f Le = m_Lights[emitterSample.value]->sampleIncidentRay(scene, point, random, wi);
    wi.pdf *= emitterSample.pdf;
    return Le;
}

/**
 * Compute the pdf of an incident direction at a surface point for the computation of direct illumination.
 */
float LightContainer::pdfIncidentRay(const Scene& scene, const SurfacePoint& point, const Vec3f& wi) const {
    float pdf = 0.f;
    for(size_t i = 0; i < m_Lights.size(); ++i) {
        pdf += m_Distribution.pdf(i) * m_Lights[i]->pdfIncidentRay(scene, point, wi);
    }
    return pdf;
}

/**
 * Sample an exitant direction from the light.
 */
Col3f LightContainer::sampleExitantRay(const Scene& scene, const ExitantRayRandom& random, RaySample& wo) const {
    Sample1ui emitterSample = m_Distribution.sample(random.light);
    Col3f Le = m_Lights[emitterSample.value]->sampleExitantRay(scene, random, wo);
    wo.pdf *= emitterSample.pdf;
    return Le;
}

/**
 * Compute the pdf of an exitant direction from the light.
 */
float LightContainer::pdfExitantRay(const Scene& scene, const Ray& wo) const {
    float pdf = 0.f;
    for(size_t i = 0; i < m_Lights.size(); ++i) {
        pdf += m_Distribution.pdf(i) * m_Lights[i]->pdfExitantRay(scene, wo);
    }
    return pdf;
}

Sample3f LightContainer::samplePoint(const Scene& scene, const LightPointRandom& random) const {
    Sample1ui emitterSample = m_LocalDistribution.sample(random.light);
    Sample3f sample = m_LocalLights[emitterSample.value]->samplePoint(scene, random);
    sample.pdf *= emitterSample.pdf;
    return sample;
}

Col3f LightContainer::Le(const Vec3f& wo) const {
    Col3f L(0.f);
    for(const LightPtr& light: m_GlobalLights) {
        L += light->Le(wo);
    }
    return L;
}

EmitterVPL LightContainer::sampleVPL(const Scene& scene, const VPLRandom& random) const {
    Sample1ui emitterSample = m_Distribution.sample(random.light);
    EmitterVPL vpl = m_Lights[emitterSample.value]->sampleVPL(scene, random);
    if(vpl.type == EmitterVPL::DIRECTIONAL) {
        vpl.directional.L *= embree::rcp(emitterSample.pdf);
    } else {
        vpl.oriented.L *= embree::rcp(emitterSample.pdf);
    }
    return vpl;
}

}
