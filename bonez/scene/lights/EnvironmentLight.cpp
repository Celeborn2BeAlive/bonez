#include "EnvironmentLight.hpp"
#include "../Scene.hpp"

#include "bonez/common/maths.hpp"

#include "bonez/scene/lights/vpls.hpp"

#include <embree/common/sys/stl/array2d.h>

namespace BnZ {

EnvironmentLight::EnvironmentLight(const embree::Ref<embree::Image>& map, Col3f Le):
    m_Map(map), m_Le(Le) {
    if(!m_Map) {
        m_Map = new embree::Image3f(4, 4, Col3f(embree::one));
    }
    
    embree::Array2D<float> importance(m_Map->height, m_Map->width);
    
    float scale = embree::rcp((float) m_Map->height) * (float) embree::pi;
    for(size_t y = 0; y < m_Map->height; ++y) {
        float sin_theta = embree::sin((y + 0.5f) * scale);
        for(size_t x = 0; x < m_Map->width; ++x) {
            importance.set(y, x, sin_theta * embree::luminance(m_Le * m_Map->get(x, y)));
        }
    }
    
    m_Distribution = new embree::Distribution2D(importance, m_Map->width, m_Map->height);
}

/**
 * Compute an upper bound of the power emitted by the light that affect the
 * scene.
 */
Col3f EnvironmentLight::getPowerUpperBound(const Scene& scene) const {
    Vec3f center;
    float radius;
    boundingSphere(scene.geometry.boundingBox, center, radius);
    return m_Le * m_Map->get(m_Map->width * 0.5f, m_Map->height * 0.5f) * float(embree::pi) * embree::sqr(radius);
}

/**
 * Sample an incident ray at a surface point for the computation of direct illumination.
 * The sampled ray must be tested against occlusions by the caller.
 */
Col3f EnvironmentLight::sampleIncidentRay(const Scene& scene, const SurfacePoint& point, 
    const IncidentRayRandom& random, RaySample& wi) const {
    Sample2f s = m_Distribution->sample(random.direction);

    float phi = s.value.x * (float) embree::two_pi * embree::rcp((float) m_Map->width);
    float theta = s.value.y * (float) embree::pi * embree::rcp((float) m_Map->height);
    
    wi.value = shadow_ray(point, sphericalToCartesian(phi, theta), embree::inf);
    wi.pdf = s.pdf * embree::rcp(float(embree::two_pi) * float(embree::pi) * embree::sin(theta));

    Col3f L = m_Map->get(embree::clamp(size_t(s.value.x), size_t(0), size_t(m_Map->width - 1)),
                         embree::clamp(size_t(s.value.y), size_t(0), size_t(m_Map->height - 1)));

    return m_Le * L;
}

/**
 * Compute the pdf of an incident direction at a surface point for the computation of direct illumination.
 */
float EnvironmentLight::pdfIncidentRay(const Scene& scene, const SurfacePoint& point, const Vec3f& wi) const {
    return 0.f;
}

/**
 * Sample an exitant direction from the light.
 */
Col3f EnvironmentLight::sampleExitantRay(const Scene& scene, const ExitantRayRandom& random, RaySample& wo) const {
    Sample2f s = m_Distribution->sample(random.direction);

    float phi = s.value.x * (float) embree::two_pi * embree::rcp((float) m_Map->width);
    float theta = s.value.y * (float) embree::pi * embree::rcp((float) m_Map->height);

    Sample3f wo_dir = Sample3f(-sphericalToCartesian(phi, theta), 
        s.pdf * embree::rcp(float(embree::two_pi) * float(embree::pi) * embree::sin(theta)));
    
    Vec3f center;
    float radius;
    boundingSphere(scene.geometry.boundingBox, center, radius);
    
    Vec2f diskSample = embree::uniformSampleDisk(random.position, radius);
    
    LinearSpace3f space = coordinateSystem(wo_dir.value);

    Sample3f P = Sample3f(center + diskSample.x * space.vx + diskSample.y * space.vz - radius * wo_dir.value,
        embree::rcp(radius * radius * float(embree::pi)));
    
    wo = RaySample(light_ray(P.value, wo_dir.value), wo_dir.pdf * P.pdf);
    
    return m_Le * m_Map->get(embree::clamp(size_t(s.value.x), size_t(0), size_t(m_Map->width - 1)),
        embree::clamp(size_t(s.value.y), size_t(0), size_t(m_Map->height - 1)));
}

/**
 * Compute the pdf of an exitant direction from the light.
 */
float EnvironmentLight::pdfExitantRay(const Scene& scene, const Ray& wo) const {
    return 0.f;
}

/**
 * Return true if the light is a delta light (emits from a unique point or a unique direction)
 */
bool EnvironmentLight::isDeltaLight() const {
    return false;
}

/**
 * Return true if the light is a global light (emits from the outside of the scene)
 */
bool EnvironmentLight::isGlobalLight() const {
    return true;
}

Sample3f EnvironmentLight::samplePoint(const Scene& scene, const LightPointRandom& point) const {
    return Sample3f(Vec3f(0.f), 0.f);
}

/**
 * Return the radiance emitted by the light in the exitant direction wo if wo is unoccluded.
 */
Col3f EnvironmentLight::Le(const Vec3f& wo) const {
    Vec2f s = cartesianToSpherial(-wo);
    
    float x = s.x * (float) embree::one_over_two_pi * m_Map->width;
    float y = s.y * (float) embree::one_over_pi * m_Map->height;
    
    Col3f L = m_Map->get(embree::clamp(size_t(x), size_t(0), size_t(m_Map->width - 1)),
                         embree::clamp(size_t(y), size_t(0), size_t(m_Map->height - 1)));
    
    return m_Le * L;
}

Col3f EnvironmentLight::Le(const SurfacePoint& P, const Vec3f& wo) const {
    return Col3f(0.f);
}

EmitterVPL EnvironmentLight::sampleVPL(const Scene& scene, const VPLRandom& random) const {
    Sample2f s = m_Distribution->sample(random.vpl);

    float phi = s.value.x * (float) embree::two_pi * embree::rcp((float) m_Map->width);
    float theta = s.value.y * (float) embree::pi * embree::rcp((float) m_Map->height);

    Sample3f wo = Sample3f(-sphericalToCartesian(phi, theta), 
        s.pdf * embree::rcp(float(embree::two_pi) * float(embree::pi) * embree::sin(theta)));
    
    Col3f Le = m_Le * m_Map->get(embree::clamp(size_t(s.value.x), size_t(0), size_t(m_Map->width - 1)),
        embree::clamp(size_t(s.value.y), size_t(0), size_t(m_Map->height - 1)));
    
    return EmitterVPL(DirectionalVPL(wo, Le * embree::rcp(wo.pdf)));
}

}
