#ifndef _BONEZ_LIGHT_HPP_
#define _BONEZ_LIGHT_HPP_

#include <memory>
#include "bonez/common/common.hpp"
#include "bonez/scene/SurfacePoint.hpp"

namespace BnZ {

struct IncidentRayRandom {
    float light;
    float triangle;
    Vec2f direction;
    
    IncidentRayRandom(float light, float triangle, Vec2f direction):
        light(light), triangle(triangle), direction(direction) {
    }
};

struct ExitantRayRandom {
    float light;
    float triangle;
    Vec2f position;
    Vec2f direction;
    
    ExitantRayRandom(float light, float triangle, Vec2f position, Vec2f direction):
        light(light), triangle(triangle), position(position), direction(direction) {
    }
};

struct LightPointRandom {
    float light;
    float triangle;
    Vec2f position;
    
    LightPointRandom(float light, float triangle, Vec2f position):
        light(light), triangle(triangle), position(position) {
    }
};

struct VPLRandom {
    float light;
    float triangle;
    Vec2f vpl;
    
    VPLRandom(float light, float triangle, Vec2f vpl):
        light(light), triangle(triangle), vpl(vpl) {
    }
};

class Scene;
class EmitterVPL;

class Light {
public:
    virtual ~Light() {
    }
    
    /**
     * Compute an upper bound of the power emitted by the light that affect the
     * scene.
     */
    virtual Col3f getPowerUpperBound(const Scene& scene) const = 0;
    
    /**
     * Sample an incident ray at a surface point for the computation of direct illumination.
     * The sampled ray must be tested against occlusions by the caller.
     */
    virtual Col3f sampleIncidentRay(const Scene& scene, const SurfacePoint& point, const IncidentRayRandom& random,
        RaySample& wi) const = 0;
    
    /**
     * Compute the pdf of an incident direction at a surface point for the computation of direct illumination.
     */
    virtual float pdfIncidentRay(const Scene& scene, const SurfacePoint& point, const Vec3f& wi) const = 0;
    
    /**
     * Sample an exitant direction from the light.
     */
    virtual Col3f sampleExitantRay(const Scene& scene, const ExitantRayRandom& random, RaySample& wo) const = 0;
    
    /**
     * Compute the pdf of an exitant direction from the light.
     */
    virtual float pdfExitantRay(const Scene& scene, const Ray& wo) const = 0;
    
    /**
     * Return true if the light is a delta light (emits from a unique point or a unique direction)
     */
    virtual bool isDeltaLight() const = 0;
    
    /**
     * Return true if the light is a global light (emits from the outside of the scene);
     */
    virtual bool isGlobalLight() const = 0;
    
    /**
     * Sample a point if the light is not a global light.
     */
    virtual Sample3f samplePoint(const Scene& scene, const LightPointRandom& random) const = 0;
    
    /**
     * Return the radiance emitted by the light in the exitant direction wo if wo is unoccluded and
     * the light is a global light.
     */
    virtual Col3f Le(const Vec3f& wo) const = 0;
    
    /**
     * Return the radiance emitted by the light from a position and in a given exitant direction.
     * Precondition: P must be in the light.
     */
    virtual Col3f Le(const SurfacePoint& P, const Vec3f& wo) const = 0;
    
    /**
     * Sample a VPL on the light source.
     */
    virtual EmitterVPL sampleVPL(const Scene& scene, const VPLRandom& random) const = 0;
    
};

typedef std::shared_ptr<Light> LightPtr;

}

#endif
