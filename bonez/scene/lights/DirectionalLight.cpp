#include "DirectionalLight.hpp"

#include <embree/samplers/shapesampler.h>

#include "bonez/scene/Scene.hpp"
#include "bonez/common/maths.hpp"

namespace BnZ {
#if 0
Col3f DirectionalLight::sampleRay(const Scene& scene, float r0, float r1, 
    Sample3f& P, Vec3f& N, Sample3f& wo) {
    N = direction;
    
    Vec3f center;
    float radius;
    boundingSphere(scene.geometry.boundingBox, center, radius);
    
    Vec2f diskSample = embree::uniformSampleDisk(Vec2f(r0, r1), radius);
    
    LinearSpace3f space = coordinateSystem(N);
    P.value = center + diskSample.x * space.vx + diskSample.y * space.vz - radius * direction;
    P.pdf = embree::rcp(radius * radius * float(embree::pi));
    
    wo = Sample3f(direction, 1.f);
    
    return L;
}

/**
 * Compute an upper bound of the power leaving the light source towards the scene.
 * \return The computed power.
 */
Col3f DirectionalLight::power(const Scene& scene) const {
    Vec3f center;
    float radius;
    boundingSphere(scene.geometry.boundingBox, center, radius);
    return L * float(embree::pi) * embree::sqr(radius);
}
#endif
}
