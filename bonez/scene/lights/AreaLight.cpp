#include "AreaLight.hpp"
#include "vpls.hpp"

namespace BnZ {

/**
 * Compute an upper bound of the power emitted by the light that affect the
 * scene.
 */
Col3f AreaLight::getPowerUpperBound(const Scene& scene) const {
    return m_Le * scene.geometry[m_nMeshIndex].getArea();
}

/**
 * Sample an incident ray at a surface point for the computation of direct illumination.
 * The sampled ray must be tested against occlusions by the caller.
 */
Col3f AreaLight::sampleIncidentRay(const Scene& scene, const SurfacePoint& point, 
    const IncidentRayRandom& random, RaySample& wi) const {
    const TriangleMesh& mesh = scene.geometry[m_nMeshIndex];
    
    size_t triangleIdx = embree::clamp(size_t(random.triangle * mesh.triangleCount()), size_t(0), mesh.triangleCount() - 1);
    float u, v;
    uniformSampleTriangle(random.direction.x, random.direction.y, u, v);
    
    // Get the surface point of the sample
    Vec3f P, N;
    mesh.getSurfacePoint(triangleIdx, u, v, P, N);
    
    // The output direction at the surface point
    Vec3f wo = Vec3f(point.P - P);
    
    float cos_o = embree::dot(wo, N); // cosine times the length of wo
    
    // Test the position of the intersection: it must be on the emission side of the triangle
    if(cos_o <= 0.f) {
        wi.pdf = 0.f;
        return Col3f(embree::zero);
    }
    
    float dist = embree::length(wo); // distance between intersection and I
    
    cos_o /= dist; // cosine between the normal at I and wo
    wo /= dist; // normalize wo
    
    // pdf according to solid angle at the intersection point
    // p(wi) = p(y) * dA(y) / dW(wi) = p(y) * dA(y) / (dA(y) * cos_o / (d * d)) = p(y) * d * d / cos_o
    // p(y) = 1 / (triangleCount * triangleArea)
    float pdf = dist * dist * embree::rcp(cos_o * mesh.triangleCount() * mesh.getTriangle(triangleIdx).area);

    wi = RaySample(shadow_ray(point, -wo, dist), pdf);

    return m_Le;
}

/**
 * Compute the pdf of an incident direction at a surface point for the computation of direct illumination.
 */
float AreaLight::pdfIncidentRay(const Scene& scene, const SurfacePoint& point, const Vec3f& wi) const {
    return 0.f;
}

/**
 * Sample an exitant direction from the light.
 */
Col3f AreaLight::sampleExitantRay(const Scene& scene, const ExitantRayRandom& random, RaySample& wo) const {
    const TriangleMesh& mesh = scene.geometry[m_nMeshIndex];
    
    size_t triangleIdx = embree::clamp(size_t(random.triangle * mesh.triangleCount()), size_t(0), mesh.triangleCount() - 1);
    float u, v;
    uniformSampleTriangle(random.position.x, random.position.y, u, v);
    
    // Get the surface point of the sample
    Vec3f P, N;
    mesh.getSurfacePoint(triangleIdx, u, v, P, N);
    
    Sample3f wo_dir = embree::cosineSampleHemisphere(random.direction.x, random.direction.y, N);
    
    wo = RaySample(light_ray(P, wo_dir.value), wo_dir.pdf * embree::rcp(mesh.triangleCount() * mesh.getTriangle(triangleIdx).area));
    
    return m_Le * embree::dot(N, wo_dir.value);
}

/**
 * Compute the pdf of an exitant direction from the light.
 */
float AreaLight::pdfExitantRay(const Scene& scene, const Ray& wo) const {
    return 0.f;
}

/**
 * Return true if the light is a delta light (emits from a unique point or a unique direction)
 */
bool AreaLight::isDeltaLight() const {
    return false;
}

/**
 * Return true if the light is a global light (emits from the outside of the scene)
 */
bool AreaLight::isGlobalLight() const {
    return false;
}

Sample3f AreaLight::samplePoint(const Scene& scene, const LightPointRandom& random) const {
    const TriangleMesh& mesh = scene.geometry[m_nMeshIndex];
    
    size_t triangleIdx = embree::clamp(size_t(random.triangle * mesh.triangleCount()), size_t(0), mesh.triangleCount() - 1);
    float u, v;
    uniformSampleTriangle(random.position.x, random.position.y, u, v);
    
    // Get the surface point of the sample
    Vec3f P, N;
    mesh.getSurfacePoint(triangleIdx, u, v, P, N);
    
    return Sample3f(P, embree::rcp(mesh.triangleCount() * mesh.getTriangle(triangleIdx).area));
}

/**
 * Return the radiance emitted by the light in the exitant direction wo if wo is unoccluded.
 */
Col3f AreaLight::Le(const Vec3f& wo) const {
    return Col3f(0.f);
}

Col3f AreaLight::Le(const SurfacePoint& P, const Vec3f& wo) const {
    return m_Le;
}

EmitterVPL AreaLight::sampleVPL(const Scene& scene, const VPLRandom& random) const {
    const TriangleMesh& mesh = scene.geometry[m_nMeshIndex];
    
    size_t triangleIdx = embree::clamp(size_t(random.triangle * mesh.triangleCount()), size_t(0), mesh.triangleCount() - 1);
    float u, v;
    uniformSampleTriangle(random.vpl.x, random.vpl.y, u, v);
    
    // Get the surface point of the sample
    Vec3f P, N;
    mesh.getSurfacePoint(triangleIdx, u, v, P, N);
    
    return EmitterVPL(OrientedVPL(P, N, m_Le * mesh.triangleCount() * mesh.getTriangle(triangleIdx).area));
}

}
