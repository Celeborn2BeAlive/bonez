#ifndef _BONEZ_LIGHTCONTAINER_HPP_
#define _BONEZ_LIGHTCONTAINER_HPP_

#include <vector>
#include "bonez/common/common.hpp"
#include "bonez/common/sampling/DiscreteDistribution.hpp"
#include "Light.hpp"

namespace BnZ {

class Scene;
class SurfacePoint;

class LightContainer {
public:
    LightContainer() {
    }

    LightContainer(std::vector<LightPtr>&& lights, const Scene& scene):
        m_Lights(lights), m_PowerUpperBound(0.f) {
        preprocess(scene);
    }
    
    bool empty() const {
        return m_Lights.empty();
    }
    
    size_t size() const {
        return m_Lights.size();
    }
    
    const LightPtr& operator [](size_t idx) const {
        return m_Lights[idx];
    }
    
    /**
     * Compute an upper bound of the power emitted by the light that affect the
     * scene.
     */
    Col3f getPowerUpperBound(const Scene& scene) const;
    
    /**
     * Sample an incident ray at a surface point for the computation of direct illumination.
     * The sampled ray must be tested against occlusions by the caller.
     */
    Col3f sampleIncidentRay(const Scene& scene, const SurfacePoint& point, 
        const IncidentRayRandom& random, RaySample& wi) const;
    
    /**
     * Compute the pdf of an incident direction at a surface point for the computation of direct illumination.
     */
    float pdfIncidentRay(const Scene& scene, const SurfacePoint& point, const Vec3f& wi) const;
    
    /**
     * Sample an exitant direction from the light.
     */
    Col3f sampleExitantRay(const Scene& scene, const ExitantRayRandom& random, RaySample& wo) const;
    
    /**
     * Compute the pdf of an exitant direction from the light.
     */
    float pdfExitantRay(const Scene& scene, const Ray& wo) const;
    
    Sample3f samplePoint(const Scene& scene, const LightPointRandom& random) const;
    
    Col3f Le(const Vec3f& wo) const;
    
    EmitterVPL sampleVPL(const Scene& scene, const VPLRandom& random) const;
    
private:
    void preprocess(const Scene& scene);

    std::vector<LightPtr> m_Lights;
    std::vector<LightPtr> m_GlobalLights;
    std::vector<LightPtr> m_LocalLights;
    DiscreteDistribution m_Distribution;
    DiscreteDistribution m_LocalDistribution;
    Col3f m_PowerUpperBound;
};

}

#endif
