#ifndef _BONEZ_ENVIRONMENTLIGHT_HPP_
#define _BONEZ_ENVIRONMENTLIGHT_HPP_

#include <embree/common/image/image.h>
#include <embree/samplers/distribution2d.h>
#include "Light.hpp"

namespace BnZ {

class EnvironmentLight: public Light {
public:
    EnvironmentLight(const embree::Ref<embree::Image>& map, Col3f Le);
    
    /**
     * Compute an upper bound of the power emitted by the light that affect the
     * scene.
     */
    virtual Col3f getPowerUpperBound(const Scene& scene) const;
    
    /**
     * Sample an incident ray at a surface point for the computation of direct illumination.
     * The sampled ray must be tested against occlusions by the caller.
     */
    virtual Col3f sampleIncidentRay(const Scene& scene, const SurfacePoint& point, 
        const IncidentRayRandom& random, RaySample& wi) const;
    
    /**
     * Compute the pdf of an incident direction at a surface point for the computation of direct illumination.
     */
    virtual float pdfIncidentRay(const Scene& scene, const SurfacePoint& point, const Vec3f& wi) const;
    
    /**
     * Sample an exitant direction from the light.
     */
    virtual Col3f sampleExitantRay(const Scene& scene, const ExitantRayRandom& random, RaySample& wo) const;
    
    /**
     * Compute the pdf of an exitant direction from the light.
     */
    virtual float pdfExitantRay(const Scene& scene, const Ray& wo) const;
    
    /**
     * Return true if the light is a delta light (emits from a unique point or a unique direction)
     */
    virtual bool isDeltaLight() const;

    /**
     * Return true if the light is a global light (emits from the outside of the scene)
     */
    virtual bool isGlobalLight() const;

    /**
     * Sample a point if the light is not a global light.
     */
    virtual Sample3f samplePoint(const Scene& scene, const LightPointRandom& point) const;

    /**
     * Return the radiance emitted by the light in the exitant direction wo if wo is unoccluded.
     */
    virtual Col3f Le(const Vec3f& wo) const;

    virtual Col3f Le(const SurfacePoint& P, const Vec3f& wo) const;

    virtual EmitterVPL sampleVPL(const Scene& scene, const VPLRandom& random) const;

private:
    embree::Ref<embree::Image> m_Map;
    embree::Ref<embree::Distribution2D> m_Distribution;
    Col3f m_Le;
};

}

#endif
