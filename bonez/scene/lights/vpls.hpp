#ifndef _BONEZ_LIGHTS_VPLS_HPP_
#define _BONEZ_LIGHTS_VPLS_HPP_

#include <vector>

#include "bonez/scene/Scene.hpp"

namespace BnZ {

struct OrientedVPL {
    Vec3f P; // Position of the VPL
    Vec3f N; // Normal of the VPL
    Col3f L; // Radiance emitted by the VPL
    
    OrientedVPL(Vec3f P, Vec3f N, Col3f L):
        P(P), N(N), L(L) {
    }
    
    OrientedVPL() {
    }
};

struct DirectionalVPL {
    Vec3f D; // Direction of the VPL
    Col3f L; // Radiance emitted by the VPL
    
    DirectionalVPL(Vec3f D, Col3f L):
        D(D), L(L) {
    }
    
    DirectionalVPL() {
    }
};

struct EmitterVPL {
    enum Type {
        ORIENTED, DIRECTIONAL
    };
    Type type;
    
    OrientedVPL oriented;
    DirectionalVPL directional;
    
    EmitterVPL(const DirectionalVPL& vpl):
        type(DIRECTIONAL), directional(vpl) {
    }
    
    EmitterVPL(const OrientedVPL& vpl):
        type(ORIENTED), oriented(vpl) {
    }
};

struct VPLContainer {
    typedef std::vector<OrientedVPL> OrientedVPLContainer;
    typedef std::vector<DirectionalVPL> DirectionalVPLContainer;
    
    OrientedVPLContainer orientedVPLs;
    DirectionalVPLContainer directionalVPLs;
    
    __forceinline void addVPL(const OrientedVPL& vpl) {
        orientedVPLs.emplace_back(vpl);
    }
    
    __forceinline void addVPL(const DirectionalVPL& vpl) {
        directionalVPLs.emplace_back(vpl);
    }
    
    __forceinline size_t size() const {
        return orientedVPLs.size() + directionalVPLs.size();
    }
    
    __forceinline bool empty() const {
        return orientedVPLs.empty() && directionalVPLs.empty();
    }
    
    __forceinline void clear() {
        orientedVPLs.clear();
        directionalVPLs.clear();
    }
};

/**
 * Compute the geometry term between an intersection and an oriented VPL.
 */
__forceinline float G(const SurfacePoint& I, const OrientedVPL& vpl, Ray& shadowRay) {
    Vec3f wi = vpl.P - I.P;
    float distance = embree::length(wi);
    wi /= distance;
    
    shadowRay = shadow_ray(I, wi, distance);
    
    float cos_i = embree::dot(I.Ns, wi);
    if(cos_i <= 0.f) {
        return 0.f;
    }
    
    float cos_o = embree::dot(vpl.N, -wi);
    if(cos_o <= 0.f) {
        return 0.f;
    }
    
    return cos_i * cos_o * embree::rcp(embree::sqr(distance));
}

__forceinline float Gclamped(const SurfacePoint& I, const OrientedVPL& vpl, float distMin, Ray& shadowRay) {
    Vec3f wi = vpl.P - I.P;
    float distance = embree::length(wi);
    wi /= distance;

    shadowRay = shadow_ray(I, wi, distance);

    float cos_i = embree::dot(I.Ns, wi);
    if(cos_i <= 0.f) {
        return 0.f;
    }

    float cos_o = embree::dot(vpl.N, -wi);
    if(cos_o <= 0.f) {
        return 0.f;
    }

    distance = embree::max(distance, distMin);

    return cos_i * cos_o * embree::rcp(embree::sqr(distance));
}

/**
 * Compute the geometry term between an intersection and a directional VPL.
 */
__forceinline float G(const Intersection& I, const DirectionalVPL& vpl, Ray& shadowRay) {
    Vec3f wi = -vpl.D;

    shadowRay = shadow_ray(I, wi, embree::inf);

    float cos_i = embree::dot(I.Ns, wi);
    if(cos_i <= 0.f) {
        return 0.f;
    }

    return cos_i;
}

__forceinline float G(const Intersection& I, const Vec3f& P, Ray& shadowRay) {
    Vec3f wi = P - I.P;
    float distance = embree::length(wi);
    wi /= distance;

    shadowRay = shadow_ray(I, wi, distance);

    float cos_i = embree::dot(I.Ns, wi);
    if(cos_i <= 0.f) {
        return 0.f;
    }

    return cos_i * embree::rcp(embree::sqr(distance));
}

__forceinline float Gclamped(const Intersection& I, const Vec3f& P, float distMin, Ray& shadowRay) {
    Vec3f wi = P - I.P;
    float distance = embree::length(wi);
    wi /= distance;

    shadowRay = shadow_ray(I, wi, distance);

    float cos_i = embree::dot(I.Ns, wi);
    if(cos_i <= 0.f) {
        return 0.f;
    }

    distance = embree::max(distMin, distance);

    return cos_i * embree::rcp(embree::sqr(distance));
}

}

#endif
