#ifndef _BONEZ_VOXELSPACE_HPP
#define _BONEZ_VOXELSPACE_HPP

#include "VoxelGrid.hpp"
#include "topology/CurvilinearSkeleton.hpp"
#include "topology/CurvSkelClustering.hpp"

#include <memory>

namespace BnZ {

typedef std::vector<Col3f> NodeColors;

class VoxelSpace {
public:
    // Factory function: force dynamic allocation to avoid leaking pointers
    static std::unique_ptr<VoxelSpace> create(Vec3ui size,
                                       AffineSpace3f worldToLocalTransform,
                                       float worldToLocalScale) {
        return std::unique_ptr<VoxelSpace>(new VoxelSpace(size, worldToLocalTransform, worldToLocalScale));
    }

    void setVoxelGrid(VoxelGrid grid) {
        m_VoxelGrid = grid;
    }

    void setSkeleton(CurvilinearSkeleton skel) {
        m_CurvSkel = skel;
        m_CurvSkel.setVoxelSpace(this);
        m_CurvSkelClustering.build(m_CurvSkel);
    }

    const CurvilinearSkeleton& getSkeleton() const {
        return m_CurvSkel;
    }

    CurvilinearSkeleton& getSkeleton() {
        return m_CurvSkel;
    }

    const CurvSkelClustering& getClustering() const {
        return m_CurvSkelClustering;
    }

    CurvSkelClustering& getClustering() {
        return m_CurvSkelClustering;
    }

    const VoxelGrid& getVoxelGrid() const {
        return m_VoxelGrid;
    }

    const AffineSpace3f& getWorldToLocalTransform() const {
        return m_WorldToLocal;
    }

    const AffineSpace3f& getLocalToWorldTransform() const {
        return m_LocalToWorld;
    }

    float getWorldToLocalScale() const {
        return m_fWorldToLocalScale;
    }

    float getLocalToWorldScale() const {
        return m_fLocalToWorldScale;
    }

    Vec3ui getSize() const {
        return m_Size;
    }

    Vec3i getVoxel(Vec3f P) const {
        P = xfmPoint(m_WorldToLocal, P);
        return Vec3i(P.x, P.y, P.z);
    }

    float getVoxelSize() const {
        return m_fLocalToWorldScale;
    }

    Col3f getClusteringNodeColor(GraphNodeIndex idx) const {
        if(idx == UNDEFINED_NODE) return Col3f(0.f);
        return m_ClusterColors[idx];
    }

    Col3f getSkeletonNodeColor(GraphNodeIndex idx) const {
        if(idx == UNDEFINED_NODE) return Col3f(0.f);
        return m_SkelNodeColors[idx];
    }

    const NodeColors& getSkeletonNodeColors() const {
        return m_SkelNodeColors;
    }

    const NodeColors& getClusteringNodeColors() const {
        return m_ClusterColors;
    }

    void setSkeletonNodeColors(NodeColors colors) {
        m_SkelNodeColors = colors;
    }

    void setClusteringNodeColors(NodeColors colors) {
        m_ClusterColors = colors;
    }

private:
    VoxelSpace(Vec3ui size,
               AffineSpace3f worldToLocalTransform,
               float worldToLocalScale):
        m_WorldToLocal(worldToLocalTransform),
        m_LocalToWorld(embree::rcp(m_WorldToLocal)),
        m_fWorldToLocalScale(worldToLocalScale),
        m_fLocalToWorldScale(embree::rcp(m_fWorldToLocalScale)),
        m_Size(size),
        m_nSliceSize(m_Size.x * m_Size.y) {
    }

    AffineSpace3f m_WorldToLocal;
    AffineSpace3f m_LocalToWorld;
    float m_fWorldToLocalScale;
    float m_fLocalToWorldScale; // It is also the size of a voxel in world space
    Vec3ui m_Size; // Dimension of the voxel grid
    size_t m_nSliceSize; // m_Size.x * m_Size.y

    VoxelGrid m_VoxelGrid;
    CurvilinearSkeleton m_CurvSkel;
    CurvSkelClustering m_CurvSkelClustering;

    // Only used for analysis
    NodeColors m_SkelNodeColors;
    NodeColors m_ClusterColors;
};

const NodeColors& getNodeColors(const Skeleton& skeleton, const VoxelSpace& voxelSpace);

void setRandomColors(VoxelSpace& voxelSpace);

void setClusteringColors(VoxelSpace& voxelSpace);

void setDegreeColors(VoxelSpace& voxelSpace);

void setMaxballRadiusColors(VoxelSpace& voxelSpace);

void setMaxballRadiusPerLineColors(VoxelSpace& voxelSpace);

void setMaxballVariancePerLineColors(VoxelSpace& voxelSpace);

void setLocalMaxballExtremasColors(VoxelSpace& voxelSpace);

void setLineMaxballExtremasColors(VoxelSpace& voxelSpace);

void setMeyerWatershedColors(VoxelSpace& voxelSpace);

void setVisibilityClusteringTestColors(VoxelSpace& voxelSpace);

void setCurvatureColors(VoxelSpace& voxelSpace);

template<typename Functor>
inline void iterate6neighbouring(int x, int y, int z, Functor f) {
    f(x - 1, y, z);
    f(x, y - 1, z);
    f(x, y, z - 1);
    f(x + 1, y, z);
    f(x, y + 1, z);
    f(x, y, z + 1);
}

template<typename Functor>
inline void iterate6neighbouring(const Vec3i& voxel, Functor f) {
    iterate6neighbouring(voxel.x, voxel.y, voxel.z, f);
}

}

#endif // _BONEZ_DISCRETESPACE_HPP
