#ifndef _BONEZ_SHADING_COMMON_HPP_
#define _BONEZ_SHADING_COMMON_HPP_

namespace BnZ {

struct ShadingModels {
    /**
     * Source of importance for a given algorithm
     */
    enum ImportanceSource {
        LIGHTS_IMPORTANCE_SOURCE, // The shading model must sample towards the lights
        CAMERA_IMPORTANCE_SOURCE // The shading model must sample towars the camera
    };

    enum {
        ALL                   = 0xFFFFFFFF,    // all BSDF components for the given surface
        DIFFUSE               = 0x000F000F,    // all diffuse BSDFs for the given surface
        DIFFUSE_REFLECTION    = 0x00000001,    // fully diffuse reflection BSDF
        DIFFUSE_REFRACTION    = 0x00010000,    // fully diffuse refraction BSDF
        GLOSSY                = 0x00F000F0,    // all glossy BSDFs for the given surface
        GLOSSY_REFLECTION     = 0x00000010,    // semi-specular reflection BSDF
        GLOSSY_REFRACTION     = 0x00100000,    // semi-specular transmission BSDF
        NONE                  = 0x00000000,    // no BSDF components are set for the given surface
        REFLECTION            = 0x0000FFFF,    // all reflection BSDFs for the given surface
        SPECULAR              = 0x0F000F00,    // all specular BSDFs for the given surface
        SPECULAR_REFLECTION   = 0x00000100,    // perfect specular reflection BSDF
        SPECULAR_REFRACTION   = 0x01000000,    // perfect specular transmission BSDF
        REFRACTION            = 0xFFFF0000     // all transmission BSDFs for the given surface
    };

    typedef unsigned int ScatteringType;
};

}

#endif
