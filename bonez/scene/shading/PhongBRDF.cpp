#include "BRDF.hpp"

#include "bonez/common/common.hpp"

namespace BnZ {

Col3f PhongBRDF::sample(const Vec3f& wo, Sample3f& wi, const Vec2f& s2D) const {
    if (s2D.x < KdProb) {
        // Sample the diffuse component
        Vec2f ss = Vec2f(s2D.x / KdProb, s2D.y);
        
        wi = cosineSampleHemisphere(ss.x, ss.y, P.Ns);
        wi.pdf *= KdProb;
        
        return Kd;
    }

    if (s2D.x < (KdProb + KsProb)) {
        // Sample the specular component
        Vec2f ss = Vec2f((s2D.x - KdProb) / KsProb, s2D.y);
        
        Vec3f R = embree::reflect(wo, P.Ns); // Perfect reflecting direction

        wi = powerCosineSampleHemisphere(ss.x, ss.y, R, shininess);

        if (embree::dot(P.Ns, wi.value) <= 0.f) {
            wi.pdf = 0.f;
            return Col3f(0.f);
        }
        wi.pdf *= KsProb;

        return Ks * embree::pow(embree::dot(wi.value, R), shininess);
    }

    wi = Sample3f(Vec3f(0.f), 1 - (KdProb + KsProb));
    return Col3f(0.f);
}

float PhongBRDF::pdf(const Vec3f& wo, const Vec3f& wi) const {
    return KdProb * cosineSampleHemispherePDF(wi, P.Ns) +
        KsProb * powerCosineSampleHemispherePDF(wi, embree::reflect(wo, P.Ns), shininess);
}

Col3f PhongBRDF::eval(const Vec3f& wi, const Vec3f& wo) const {
    Vec3f R = embree::reflect(wi, P.Ns);
    float dotProduct = embree::max(0.f, embree::dot(R, wo));
    float glossyPow = (dotProduct <= 0.f) ? 0.f : embree::pow(dotProduct, shininess);
    
    return Kd + Ks * glossyPow;
}

}
