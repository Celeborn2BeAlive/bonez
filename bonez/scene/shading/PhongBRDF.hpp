#ifndef _BONEZ_PHONGBRDF_HPP_
#define _BONEZ_PHONGBRDF_HPP_

#include <embree/optics.h>

#include "bonez/scene/SurfacePoint.hpp"
#include "Material.hpp"
#include "bonez/common/common.hpp"

namespace BnZ {

class PhongBRDF {
public:
    PhongBRDF() {
    }

    PhongBRDF(const SurfacePoint& P, const Col3f& Kd, const Col3f& Ks, float shininess):
        P(P)
    {
        this->shininess = shininess;
        this->Kd = Kd * float(embree::one_over_pi);
        this->Ks = Ks * (shininess + 2) * float(embree::one_over_two_pi);
        KdProb = embree::luminance(Kd);
        KsProb = embree::luminance(Ks);
        float sum = KdProb + KsProb;
        //if(sum > 1.f) {
            KdProb /= sum;
            KsProb /= sum;
        //}
    }

    explicit PhongBRDF(const SurfacePoint& P):
        P(P) 
    {
        Vec2f texCoords = P.texCoords;
        shininess = P.material->getShininess(texCoords);
        Col3f Kd = P.material->getKd(texCoords);
        Col3f Ks = P.material->getKs(texCoords);
        this->Kd = Kd * float(embree::one_over_pi);
        this->Ks = Ks * (shininess + 2) * float(embree::one_over_two_pi);
        KdProb = embree::luminance(Kd);
        KsProb = embree::luminance(Ks);
        float sum = KdProb + KsProb;
        //if(sum > 1.f) {
            KdProb /= sum;
            KsProb /= sum;
        //}
    }

    Col3f sample(const Vec3f& wo, Sample3f& wi, const Vec2f& s2D) const;
    
    float pdf(const Vec3f& wo, const Vec3f& wi) const;
    
    Col3f eval(const Vec3f& wi, const Vec3f& wo) const;
    
    Col3f diffuseTerm() const {
        return Kd;
    }
    
    Col3f upperBound(const Vec3f& wo, const BBox3f& bbox) const {
        return Kd + Ks;
    }
    
    Col3f upperBound(const Vec3f& wo) const {
        return Kd + Ks;
    }
    
    SurfacePoint P;
    Col3f Kd;
    Col3f Ks;
    float shininess;
    float KdProb, KsProb;
};

}

#endif
