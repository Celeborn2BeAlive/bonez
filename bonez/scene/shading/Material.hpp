#ifndef _BONEZ_SHADING_MATERIAL_HPP_
#define _BONEZ_SHADING_MATERIAL_HPP_

#include "bonez/common/common.hpp"
#include <embree/common/image/image.h>
#include <embree/textures/texture.h>
#include <embree/textures/nearestneighbor.h>

namespace BnZ {

class TextureImage {
public:
    TextureImage() {
    }

    TextureImage(const embree::Ref<embree::Image>& image):
        texture(new embree::NearestNeighbor(image)), image(image) {
    }
    
    bool operator ==(const TextureImage& texture) const {
        return image == texture.image;
    }

    operator bool() const {
        return texture;
    }

    Col3f get(const Vec2f& p) const {
        return texture->get(p);
    }

    const embree::Ref<embree::Image>& getImage() const {
        return image;
    }
    
    size_t width() const {
        return image->width;
    }
    
    size_t height() const {
        return image->height;
    }
    
private:
    embree::Ref<embree::Texture> texture;
    embree::Ref<embree::Image> image;
};

class Material {
public:
    Material():
        m_Kd(0.f), m_Ks(0.f), m_fShininess(0.f) {
    }

    void setKd(const Col3f& Kd) {
        m_Kd = Kd;
    }

    void setKs(const Col3f& Ks) {
        m_Ks = Ks;
    }

    void setShininess(float shininess) {
        m_fShininess = shininess;
    }

    void setKdTexture(const TextureImage& texture) {
        m_pKdTexture = texture;
    }

    void setKsTexture(const TextureImage& texture) {
        m_pKsTexture = texture;
    }

    void setShininessTexture(const TextureImage& texture) {
        m_pShininessTexture = texture;
    }

    const Col3f& getKd() const {
        return m_Kd;
    }

    const Col3f& getKs() const {
        return m_Ks;
    }

    float getShininess() const {
        return m_fShininess;
    }

    const TextureImage& getKdTexture() const {
        return m_pKdTexture;
    }

    const TextureImage& getKsTexture() const {
        return m_pKsTexture;
    }

    const TextureImage& getShininessTexture() const {
        return m_pShininessTexture;
    }

    Col3f getKd(const Vec2f& texCoords) const {
        Col3f Kd = m_Kd;

        if (m_pKdTexture) {
            Kd *= m_pKdTexture.get(texCoords);
        }

        return Kd;
    }

    Col3f getKs(const Vec2f& texCoords) const {
        Col3f Ks = m_Ks;

        if (m_pKsTexture) {
            Ks *= m_pKsTexture.get(texCoords);
        }

        return Ks;
    }

    float getShininess(const Vec2f& texCoords) const {
        float s = m_fShininess;

        if (m_pShininessTexture) {
            s += m_pShininessTexture.get(texCoords).r;
        }

        return s;
    }

private:
    Col3f m_Kd, m_Ks;
    float m_fShininess;

    TextureImage m_pKdTexture, m_pKsTexture, m_pShininessTexture;
};

}

namespace std {

template<>
struct hash<BnZ::TextureImage> {
    size_t operator ()(const BnZ::TextureImage& image) const {
        return size_t(image.getImage().ptr);
    }
};

}

#endif
