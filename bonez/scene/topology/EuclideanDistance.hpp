#ifndef _BONEZ_EUCLIDEANDISTANCE_HPP_
#define _BONEZ_EUCLIDEANDISTANCE_HPP_

#include <vector>
#include "bonez/scene/Scene.hpp"
#include "CurvilinearSkeleton.hpp"

namespace BnZ {

/**
 * A distance builded on the skeleton nodes based on the visibility from a point.
 */
struct EuclideanDistance {
    typedef float DistanceType;

    std::vector<Vec3f> m_Positions;

    template<typename PositionFunctor>
    EuclideanDistance(const Graph& graph, PositionFunctor positionFunctor) {
        for (GraphNodeIndex i = 0; i < graph.size(); ++i) {
            m_Positions.push_back(positionFunctor(i));
        }
    }

    DistanceType operator()(GraphNodeIndex u, GraphNodeIndex v) const {
        return embree::length(m_Positions[v] - m_Positions[u]);
    }
};

}

#endif
