#include "CurvSkelSensorNetwork.hpp"

namespace BnZ {

void CurvSkelSensorNetwork::build(size_t sensorCountPerNode, const Scene& scene, Random& rng) {
    std::clog << "Compute the curvilinear skeleton sensor network..." << std::endl;
    double start = embree::getSeconds();
    
    const CurvilinearSkeleton& skeleton = scene.voxelSpace->getSkeleton();
    
    if(skeleton.empty()) {
        std::cerr << "CurvSkelSensorNetwork: No Skeleton." << std::endl;
        return;
    }
    
    m_pScene = &scene;
    
    m_SensorsPerSourceNode.resize(skeleton.size());
    m_SensorsPerTargetNode.resize(skeleton.size());
    m_SensorsPerSourceCluster.resize(scene.voxelSpace->getClustering().size());
    m_SensorsPerTargetCluster.resize(scene.voxelSpace->getClustering().size());
    
    for(GraphNodeIndex nodeIdx = 0; nodeIdx < skeleton.size(); ++nodeIdx) {
        CurvilinearSkeleton::Node node = skeleton[nodeIdx];
        for(size_t i = 0; i < sensorCountPerNode; ++i) {
            Sample3f direction = embree::uniformSampleSphere(rng.getFloat(), rng.getFloat());
            Intersection I = intersect(Ray(node.getPosition(), direction.value), scene);
            if(I) {
                GraphNodeIndex targetNodeIdx = skeleton.getNearestNode(I);
                
                Sensor sensor(
                    I,
                    nodeIdx,
                    targetNodeIdx,
                    scene.voxelSpace->getClustering().getNodeClusterIndex(nodeIdx),
                    scene.voxelSpace->getClustering().getNodeClusterIndex(targetNodeIdx)
                );
                
                size_t idx = m_Sensors.size();
                m_Sensors.push_back(sensor);
                
                m_SensorsPerSourceNode[nodeIdx].push_back(idx);
                m_SensorsPerSourceCluster[sensor.sourceClusterIdx].push_back(idx);
                if(sensor.targetNodeIdx) {
                    m_SensorsPerTargetNode[sensor.targetNodeIdx].push_back(idx);
                    m_SensorsPerTargetCluster[sensor.targetClusterIdx].push_back(idx);
                }
            }
        }
    }

    std::clog << "Done. Time = " << (embree::getSeconds() - start) << " seconds." << std::endl;
}

}
