#ifndef _BONEZ_CURVSKELCLUSTERING_HPP_
#define _BONEZ_CURVSKELCLUSTERING_HPP_

#include "CurvilinearSkeleton.hpp"
#include "Skeleton.hpp"

#include "bonez/common/data/KdTree.hpp"
#include "bonez/scene/Intersection.hpp"

namespace BnZ {

class VoxelSpace;
class Scene;

class CurvSkelClustering: public Skeleton {
public:
    struct Cluster {
        GraphNodeIndex representative; // The representative node of the cluster
        uint32_t flag;
        std::vector<GraphNodeIndex> nodes; // The nodes in the cluster
        Vec3f center; // Center of the cluster
        float maxball; // Mean of the maxball contained in the cluster

        Cluster(uint32_t flag): flag(flag) {
        }
    };

    // A pair of clusters associated to a node of the original skeleton, with a weight
    // for each one
    // We have weight1 > weight2
    struct NodeClusterPair {
        GraphNodeIndex cluster1;
        float weight1;
        GraphNodeIndex cluster2;
        float weight2;

        NodeClusterPair():
            cluster1(UNDEFINED_NODE), weight1(0.f),
            cluster2(UNDEFINED_NODE), weight2(0.f) {
        }

        explicit NodeClusterPair(GraphNodeIndex cluster):
            cluster1(cluster), weight1(1.f),
            cluster2(UNDEFINED_NODE), weight2(0.f) {
        }

        NodeClusterPair(GraphNodeIndex c1, float w1, GraphNodeIndex c2, float w2):
            cluster1(c1), weight1(w1),
            cluster2(c2), weight2(w2) {
            // Swap to get the cluster with highest weight first
            if(w2 > w1) {
                std::swap(cluster1, cluster2);
                std::swap(weight1, weight2);
            }

            // Normalize the weights to get a sum of 1
            weight1 = weight1 / (weight1 + weight2);
            weight2 = weight2 / (weight1 + weight2);
        }

        explicit operator bool() const {
            return cluster1 != UNDEFINED_NODE;
        }
    };

    void correctGridPostProcess();

    const Cluster& operator [](GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return m_Clusters[idx];
    }

    virtual const Skeleton::Node getNode(GraphNodeIndex idx) const {
        return { m_Clusters[idx].center, m_Clusters[idx].maxball, m_Clusters[idx].flag };
    }

    Vec3f getVoxelSelector(const Vec3f& position, const Vec3f& normal) const;

    Vec3f getVoxelSelector(const SurfacePoint& point) const {
        return getVoxelSelector(point.P, point.Ns);
    }

    Vec3i getVoxel(const Vec3f& position) const;

    virtual GraphNodeIndex getNearestNode(const Vec3f& P) const {
        assert(m_pSkeleton);

        Vec3i voxel = getVoxel(P);
        if(!m_Grid.contains(voxel)) {
            return UNDEFINED_NODE;
        }
        return m_Grid(voxel.x, voxel.y, voxel.z);
    }

    float nodeCoherence(const Vec3f& P, const Vec3f& N, GraphNodeIndex node) const {
        Vec3f wi = m_Clusters[node].center - P;
        float distance = embree::length(wi);
        wi /= distance;
        float d1 = embree::dot(wi, N);
        if(d1 <= 0.f) {
            return 0.f;
        }
        float distanceFactor = embree::sqr(m_Clusters[node].maxball / distance);
        return distanceFactor;
    }

    virtual GraphNodeIndex getNearestNode(const Vec3f& P, const Vec3f& N) const {
        assert(m_pSkeleton);
        return getNearestNode(getVoxelSelector(P, N));
    }

    virtual GraphNodeIndex getNearestNode(const SurfacePoint& point) const {
        return getNearestNode(point.P, point.Ns);
    }
    
    const Cluster& getCluster(GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return m_Clusters[idx];
    }
    
    const Cluster& getNodeCluster(GraphNodeIndex node) const {
        assert(node != UNDEFINED_NODE);
        return m_Clusters[getNodeClusterIndex(node)];
    }
    
    GraphNodeIndex getNodeClusterIndex(GraphNodeIndex node) const {
        if(node == UNDEFINED_NODE) {
            return UNDEFINED_NODE;
        }
        return m_NodeClusters[node].cluster1;
    }

    NodeClusterPair getNodeClusterPair(GraphNodeIndex node) const {
        if(node == UNDEFINED_NODE) {
            return NodeClusterPair();
        }
        return m_NodeClusters[node];
    }
    
    virtual const AdjacencyList& neighbours(GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return m_Graph[idx];
    }
    
    virtual size_t size() const {
        return m_Clusters.size();
    }
    
    virtual bool empty() const {
        return m_Clusters.empty();
    }

    bool build(const CurvilinearSkeleton& skeleton);

    bool buildWRTCurvature(const CurvilinearSkeleton& skeleton);

    void buildVisibilityGraph(const Scene& scene);

    bool checkVisibility(GraphNodeIndex cluster1, GraphNodeIndex cluster2) const {
        assert(!m_VisibilityGraph.empty());
        if(cluster1 == cluster2) {
            return true;
        }
        GraphNodeIndex first = std::min(cluster1, cluster2);
        GraphNodeIndex second = std::max(cluster1, cluster2);
        return m_VisibilityGraph[first][second - (first + 1)];
    }

    float getVisibilityFactor(GraphNodeIndex cluster1, GraphNodeIndex cluster2) const {
        assert(!m_VisibilityFactors.empty());
        if(cluster1 == cluster2) {
            return true;
        }
        GraphNodeIndex first = std::min(cluster1, cluster2);
        GraphNodeIndex second = std::max(cluster1, cluster2);
        return m_VisibilityFactors[first][second - (first + 1)];
    }

    virtual const Graph& getGraph() const {
        return m_Graph;
    }

    CurvSkelClustering():
        m_pSkeleton(nullptr) {
    }


    struct Edge {
        float minMaxball;
        Vec3f minMaxballPosition;

        Edge():
            minMaxball(embree::inf), minMaxballPosition(0.f) {
        }

        Edge(float minMaxball, Vec3f minMaxballPosition):
            minMaxball(minMaxball), minMaxballPosition(minMaxballPosition) {
        }
    };

    const Edge& getEdge(GraphNodeIndex cluster1, GraphNodeIndex cluster2) const {
        return m_Edges[cluster1][cluster2];
    }

private:
    // m_NodeClusters[i] is the cluster index of the node i
    std::vector<NodeClusterPair> m_NodeClusters;


    std::vector<Cluster> m_Clusters;
    Graph m_Graph; // Graph of the clusters

    std::vector<std::vector<Edge>> m_Edges;

    const CurvilinearSkeleton* m_pSkeleton;

    std::vector<std::vector<bool>> m_VisibilityGraph;
    std::vector<std::vector<float>> m_VisibilityFactors;

    Grid3D<GraphNodeIndex> m_Grid;
    const VoxelSpace* m_pVoxelSpace;
public:
    void setGrid(Grid3D<GraphNodeIndex> grid) {
        m_Grid = grid;
    }

    const Grid3D<GraphNodeIndex>& getGrid() const {
        return m_Grid;
    }

    const VoxelSpace* getVoxelSpace() const {
        return m_pVoxelSpace;
    }
};

typedef std::vector<GraphNodeIndex> ClusterVector;

/**
 * Compute a list of node which are local minimas in their neighbour for the mean maxball radius.
 */
ClusterVector computeLocalMinimas(const CurvSkelClustering& clustering);

Grid3D<GraphNodeIndex> computeGridWrtVoxelGrid(const VoxelGrid& voxelGrid,
                                               const CurvSkelClustering& clustering);

//Grid3D<GraphNodeIndex> computeGridWrtVoxelGridOF(const VoxelGrid& voxelGrid,
//                                                 const CurvSkelClustering& clustering);

//Grid3D<GraphNodeIndex> computeGridWithFMM(const VoxelGrid& voxelGrid,
//                                          const CurvSkelClustering& clustering);

Grid3D<GraphNodeIndex> computeGridWrtVoxelGridUsingCoherence(const VoxelGrid& voxelGrid,
                                                const CurvSkelClustering& clustering);

}

#endif
