#include "CurvSkelImportanceDistribution.hpp"

#include "bonez/common/sampling/patterns.hpp"

namespace BnZ {

void computeCurvSkelImportanceDistribution(CurvSkelImportanceDistribution& distribution,
    size_t viewRayCountX, size_t viewRayCountY, size_t scatteringRayCountX, size_t scatteringRayCountY,
    bool useClustering, const Scene& scene, const Camera& camera, Random& rng, ParamSet* statistics) {
    distribution.init(*scene.voxelSpace, useClustering);
    
    // Samples for the view rays
    size_t viewRayCount = viewRayCountX * viewRayCountY;
    std::unique_ptr<Vec2f[]> viewSamples(new Vec2f[viewRayCount]);
    createJittered2DPattern(viewSamples.get(), viewRayCountX, viewRayCountY, rng);
    
    // Samples for the scattering rays
    size_t scatteringRayCount = scatteringRayCountX * scatteringRayCountY;
    std::unique_ptr<Vec2f[]> scatteringSamples(new Vec2f[scatteringRayCount]);

    float importance = 1.f * embree::rcp((float) viewRayCount * (float) scatteringRayCount);

    for (size_t i = 0; i < viewRayCount; ++i) {
        Ray viewRay = camera.ray(viewSamples[i]);
        Intersection I = intersect(viewRay, scene);

        BRDF brdf = scene.shade(I);

        if (!I) {
            continue;
        }

        Vec3f wo = -viewRay.dir;
        createJittered2DPattern(scatteringSamples.get(), scatteringRayCountX, scatteringRayCountY, rng);

        for (size_t s = 0; s < scatteringRayCount; ++s) {
            Sample3f wi;
            Col3f reflectionTerm = brdf.sample(wo, wi, scatteringSamples[s]);

            if (wi.pdf == 0.f || reflectionTerm == Col3f(0.f)) {
                continue;
            }

            Intersection intersection = intersect(secondary_ray(I, wi.value), scene);
            if (!intersection) {
                continue;
            }
            
            float contrib = importance * embree::luminance(reflectionTerm) * embree::rcp(wi.pdf);

            GraphNodeIndex nodeIdx = scene.voxelSpace->getSkeleton().getNearestNode(intersection);
            
            if (nodeIdx != UNDEFINED_NODE) {
                distribution.accumulate(nodeIdx, contrib);
            }
        }
    }

    distribution.finalize(scene.voxelSpace->getSkeleton(), statistics);
}

}
