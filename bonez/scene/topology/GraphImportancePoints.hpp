#ifndef _BONEZ_GRAPHIMPORTANCEPOINTS_HPP_
#define _BONEZ_GRAPHIMPORTANCEPOINTS_HPP_

#include <cassert>
#include "bonez/common/algorithms/DijkstraAlgorithm.hpp"
#include "bonez/common/data/Graph.hpp"
#include "VisibilityBasedDistance.hpp"
#include "EuclideanDistance.hpp"

namespace BnZ {

/**
 * Represents a set of points that allow us to reach efficiently a 3D point in the scene.
 */
class GraphImportancePoints {
    typedef DijkstraAlgorithm::ShortestPathVector ShortestPathVector;

    std::vector<bool> m_HasImportancePoint;
    ShortestPathVector m_ShortestPaths;
    std::vector<Vec3f> m_ImportancePoints; // entry i is the position of the importance point of the node i

    float m_fMaxDistance; // distance of the farthest node to the importance point along the graph

public:
    GraphImportancePoints() {
    }

    bool hasImportancePoint(GraphNodeIndex index) const {
        return m_HasImportancePoint[index];
    }

    GraphNodeIndex getPredecessor(GraphNodeIndex index) const {
        return m_ShortestPaths[index].predecessor;
    }

    float getMaxDistance() const {
        return m_fMaxDistance;
    }

    float getDistance(GraphNodeIndex index) const {
        return m_ShortestPaths[index].distance;
    }

    const Vec3f& operator [](GraphNodeIndex index) const {
        assert(index < m_ImportancePoints.size() && m_HasImportancePoint[index]);
        return m_ImportancePoints[index];
    }

    void setUndefined(const Graph& graph) {
        m_HasImportancePoint.resize(graph.size(), false);
    }

    void clear() {
        m_HasImportancePoint.clear();
        m_ShortestPaths.clear();
        m_ImportancePoints.clear();
        //m_IsVisibleFromSource.clear();
    }
    
    template<typename PositionFunctor>
    void compute(const Graph& graph, const Scene& scene, const Vec3f& importanceSourcePoint, 
        GraphNodeIndex importanceSourceNode, PositionFunctor positionFunctor) {
        clear();

        m_fMaxDistance = 0.f;
        DijkstraAlgorithm dijkstra;
        
        VisibilityBasedDistance distance(graph, scene, importanceSourcePoint, positionFunctor);
        //EuclideanDistance distance(graph, positionFunctor);
        
        m_ShortestPaths = dijkstra.computeShortestPaths(graph, importanceSourceNode, distance);

        // Compute the importance points
        for (GraphNodeIndex i = 0; i < graph.size(); ++i) {
            GraphNodeIndex predecessor = m_ShortestPaths[i].predecessor;

            if (predecessor >= graph.size()) {
                m_HasImportancePoint.push_back(false);
                m_ImportancePoints.push_back(embree::zero);
                continue;
            }

            if (m_ShortestPaths[i].distance > m_fMaxDistance) {
                m_fMaxDistance = m_ShortestPaths[i].distance;
            }

            Vec3f nodePosition = positionFunctor(i);
            Vec3f barycenter(nodePosition);
            size_t nodeCount = 1; // Number of node along the visible shortest path path

            while (predecessor != importanceSourceNode) {
                Vec3f predecessorPosition = positionFunctor(predecessor);
                Vec3f direction = predecessorPosition - nodePosition;
                float ldir = embree::length(direction);
                direction /= ldir;

                if (scene.occluded(embree::Ray(nodePosition, direction, 128.0f * (float) embree::ulp,
                                               ldir - 128.0f * (float) embree::ulp))) {
                    break;
                }

                barycenter += predecessorPosition;
                ++nodeCount;
                predecessor = m_ShortestPaths[predecessor].predecessor;
            }

            barycenter *= embree::rcp(float(nodeCount));
            m_HasImportancePoint.push_back(true);
            m_ImportancePoints.push_back(barycenter);
        }
    }
};

}

#endif
