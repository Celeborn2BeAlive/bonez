#ifndef _BONEZ_SKELETONS_CURVILINEARSKELETON_HPP_
#define _BONEZ_SKELETONS_CURVILINEARSKELETON_HPP_

#include "bonez/common/common.hpp"
#include "bonez/common/data/Graph.hpp"
#include "bonez/common/data/Grid3D.hpp"

#include "bonez/scene/Intersection.hpp"

#include "Skeleton.hpp"

#include "bonez/scene/VoxelGrid.hpp"

namespace BnZ {

class VoxelSpace;

class CurvilinearSkeleton: public Skeleton {
public:
    class Node {
        Vec3f m_Position; // The position of the node in the scene
        float m_fMaxball; // The squared radius of the maxball of the node (in world space)
    public:
        Node() {
        }

        Node(Vec3f position, float maxball) :
            m_Position(position) , m_fMaxball(maxball) {
        }

        __forceinline Vec3f getPosition() const {
            return m_Position;
        }

        __forceinline float getMaxball() const {
            return m_fMaxball;
        }
    };

    typedef Grid3D<GraphNodeIndex> GridType;
    typedef std::vector<Node> NodeArray;

    CurvilinearSkeleton() {
    }

    void addNode(const Vec3f& position, float maxBall) {
        m_Nodes.push_back({position, maxBall});
    }

    void setGraph(Graph graph) {
        m_Graph = graph;
    }

    virtual const Graph& getGraph() const {
        return m_Graph;
    }
    
    void setGrid(GridType grid) {
        m_Grid = grid;
    }

    const VoxelSpace* getVoxelSpace() const {
        return m_pVoxelSpace;
    }

    void setVoxelSpace(const VoxelSpace* space) {
        m_pVoxelSpace = space;
    }

    const Node& operator[](GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return m_Nodes[idx];
    }

    virtual const Skeleton::Node getNode(GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return { m_Nodes[idx].getPosition(), embree::sqrt(m_Nodes[idx].getMaxball()) };
    }

    virtual size_t size() const {
        return m_Nodes.size();
    }

    virtual bool empty() const {
        return m_Nodes.empty();
    }

    const GridType& getGrid() const {
        return m_Grid;
    }

    Vec3f getVoxelSelector(const Vec3f& position, const Vec3f& normal) const;

    Vec3f getVoxelSelector(const SurfacePoint& point) const {
        return getVoxelSelector(point.P, point.Ns);
    }

    Vec3i getVoxel(const Vec3f& position) const;

    virtual GraphNodeIndex getNearestNode(const Vec3f& position) const {
        Vec3i voxel = getVoxel(position);
        return m_Grid(voxel.x, voxel.y, voxel.z);
    }

    virtual GraphNodeIndex getNearestNode(const Vec3f& position, const Vec3f& normal) const {
        return getNearestNode(getVoxelSelector(position, normal));
    }
 
    virtual GraphNodeIndex getNearestNode(const SurfacePoint& point) const {
        return getNearestNode(getVoxelSelector(point));
    }
    
    virtual const AdjacencyList& neighbours(GraphNodeIndex idx) const {
        assert(idx != UNDEFINED_NODE);
        return m_Graph[idx];
    }

    void blur();

    void blurMaxballs();

private:
    const VoxelSpace* m_pVoxelSpace;
    NodeArray m_Nodes;
    Graph m_Graph;
    GridType m_Grid;
};

typedef std::vector<GraphNodeIndex> NodeVector;

/**
 * Compute a list of node which are local minimas in their neighbour for the maxball radius.
 */
NodeVector computeLocalMinimas(const CurvilinearSkeleton& skeleton);

CurvilinearSkeleton::GridType computeGridWrtVoxelGrid(const VoxelGrid& voxelGrid,
                                                      const CurvilinearSkeleton& skeleton);

}

#endif
