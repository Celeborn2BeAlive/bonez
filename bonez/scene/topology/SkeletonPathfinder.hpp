#ifndef _BONEZ_SKELETONPATHFINDER_HPP_
#define _BONEZ_SKELETONPATHFINDER_HPP_

namespace BnZ {

class SkeletonPathfinder {
public:
    typedef DijkstraAlgorithm::ShortestPathVector ShortestPathVector;

    SkeletonPathfinder(float pskelThreshold):
        m_pScene(0), m_fpskelThreshold(pskelThreshold) {
    }

    bool preprocess(const Vec3f& target, const Scene& scene) {
        if(!scene.voxelSpace) {
            std::cerr << "No topology representation for the scene" << std::endl;
            return false;
        }
        
        GraphNodeIndex targetNode = scene.voxelSpace->getClustering().getNearestNode(target);
        if(!targetNode) {
            std::cerr << "Target point is not associated with any cluster." << std::endl;
            return false;
        }
        
        m_pScene = &scene;
        
        DijkstraAlgorithm dijkstra;
        
        VisibilityBasedDistance distance(scene.voxelSpace->getClustering().getGraph(), scene, target,
            [&scene](GraphNodeIndex index) -> Vec3f {
                return scene.voxelSpace->getClustering()[index].center;
            }
        );
        
        m_ShortestPaths = dijkstra.computeShortestPaths(scene.voxelSpace->getClustering().getGraph(), targetNode, distance);
        
        ClusterVector minimas = computeLocalMinimas(scene.voxelSpace->getClustering());
        std::vector<bool> isMinima(scene.voxelSpace->getClustering().size(), false);
        for(GraphNodeIndex i: minimas) {
            isMinima[i] = true;
        }
        
        m_NearestVisibleMinima.resize(scene.voxelSpace->getClustering().size(), UNDEFINED_NODE);
        
        for(GraphNodeIndex i = 0; i < scene.voxelSpace->getClustering().size(); ++i) {
            Vec3f clusterPosition = scene.voxelSpace->getClustering()[i].center;
            
            GraphNodeIndex predecessor = m_ShortestPaths[i].predecessor;
            
            if(predecessor) {
                do {
                    if(isMinima[predecessor]) {
                        Vec3f predecessorPosition = scene.voxelSpace->getClustering()[predecessor].center;
                        Vec3f direction = predecessorPosition - clusterPosition;
                        float ldir = embree::length(direction);
                        direction /= ldir;

                        if (!scene.occluded(embree::Ray(clusterPosition, direction, 128.0f * (float) embree::ulp,
                                                       ldir - 128.0f * (float) embree::ulp))) {
                            m_NearestVisibleMinima[i] = predecessor;
                            break;
                        }
                    }
                    predecessor = m_ShortestPaths[predecessor].predecessor;
                } while (predecessor != targetNode);
            }
        }
        
        return true;
    }
    
    float pskel(const Intersection& I) const {
        GraphNodeIndex node = m_pScene->voxelSpace->getClustering().getNearestNode(I);
        if(!node) {
            return 0.f;
        }
        
        if(!m_NearestVisibleMinima[node]) {
            return 0.f;
        }
       
        Vec3f center = m_pScene->voxelSpace->getClustering()[m_NearestVisibleMinima[node]].center;
        Vec3f dir = embree::normalize(center - I.P);
        
        return embree::max(0.f, embree::min(m_fpskelThreshold, embree::dot(I.Ns, dir)));
    }
    
    /**
     * Precondition: pskel(I) != 0   -> elsewise, segfault.
     */
    Sample3f sample(const Intersection& I, const Vec2f& sample2D) {
        GraphNodeIndex node = m_pScene->voxelSpace->getClustering().getNearestNode(I);
        
        Vec3f center = m_pScene->voxelSpace->getClustering()[m_NearestVisibleMinima[node]].center;
        float radius = m_pScene->voxelSpace->getClustering()[m_NearestVisibleMinima[node]].maxball;

        Vec2f diskSample = embree::uniformSampleDisk(sample2D, radius);
        
        Vec3f dir = center - I.P;
        float length = embree::length(dir);
        dir /= length;
        LinearSpace3f frame = coordinateSystem(dir);
        
        Vec3f wo = embree::normalize(
            center + frame.vx * diskSample.x + frame.vz * diskSample.y - I.P
        );
        
        if(embree::dot(wo, I.P) <= 0.f) {
            return Sample3f(wo, 0.f);
        }
        
        return Sample3f(
            wo,
            embree::sqr(length) * embree::rcp(embree::dot(dir, wo) * embree::sqr(radius) * float(embree::pi))
        );
    }
    
    /**
     * Precondition: pskel(I) != 0   -> elsewise, segfault.
     */
    float pdf(const Intersection& I, const Vec3f& wo) {
        GraphNodeIndex node = m_pScene->voxelSpace->getClustering().getNearestNode(I);
        
        Vec3f center = m_pScene->voxelSpace->getClustering()[m_NearestVisibleMinima[node]].center;
        float radius = m_pScene->voxelSpace->getClustering()[m_NearestVisibleMinima[node]].maxball;
        
        Vec3f dir = center - I.P;
    
        if(embree::dot(dir, I.Ns) < 0.f) {
            return 0.f;
        }
        
        float length = embree::length(dir);
        dir /= length;
        
        float minCos = length / embree::sqrt(length * length + radius * radius);
        
        if(embree::dot(wo, dir) < minCos) {
            return 0.f;
        }
        
        return embree::sqr(length) * embree::rcp(embree::dot(dir, wo) * embree::sqr(radius) * float(embree::pi));
    }
    
    GraphNodeIndex getNearestVisibleMinima(GraphNodeIndex cluster) const {
        return m_NearestVisibleMinima[cluster];
    }
    
private:
    const Scene* m_pScene;

    float m_fpskelThreshold;

    ShortestPathVector m_ShortestPaths;
    
    // For each node, store the nearest visible minimal clustering according to maxball radius
    // in the direction of the target point
    ClusterVector m_NearestVisibleMinima;
};

}

#endif
