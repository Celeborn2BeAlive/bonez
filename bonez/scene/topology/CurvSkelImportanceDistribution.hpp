#ifndef _BONEZ_SKELETONS_CURVSKELIMPORTANCEDISTRIBUTION_HPP_
#define _BONEZ_SKELETONS_CURVSKELIMPORTANCEDISTRIBUTION_HPP_

#include <vector>
#include <limits>
#include "bonez/common/cameras/Camera.hpp"
#include "bonez/scene/Scene.hpp"
#include "CurvilinearSkeleton.hpp"
#include "CurvSkelClustering.hpp"

namespace BnZ {

/**
 * The skeleton importance distribution is a coarse estimation of the importance of each node for the camera.
 * 
 * A set of 2 length paths are traced through the scene from the camera. For each end point a value is
 * accumulated at the corresponding node. The resultings values for each node are the estimations of
 * the importance of the node.
 */
class CurvSkelImportanceDistribution {
public:
    CurvSkelImportanceDistribution():
        m_fTotalDensity(0.f), m_fMaxDensity(0.f), m_fMinDensity(0.f) {
    }

    bool empty() const {
        return m_ImportanceDensity.empty();
    }

    size_t size() const {
        return m_ImportanceDensity.size();
    }
    
    void init(const VoxelSpace& voxelSpace, bool useClustering) {
        m_bUseClustering = useClustering;
        
        size_t size = voxelSpace.getSkeleton().size();
        if(useClustering) {
            m_SkelClustering = &voxelSpace.getClustering();
            size = m_SkelClustering->size();
        }
        
        m_ImportanceDensity.resize(size, 0.f);
        m_IntersectionCount.resize(size, 0);
        m_nTotalIntersections = 0;
        m_fTotalDensity = 0.f;
        m_fMaxDensity = 0.f;
        m_fMinDensity = 0.f;
        m_fMeanDensity = 0.f;
    }

    void accumulate(GraphNodeIndex idx, float importance) {
        if(m_ImportanceDensity.empty()) {
            return;
        }
        
        size_t index = idx;
        if(m_bUseClustering) {
            index = m_SkelClustering->getNodeClusterIndex(idx);
        }
        
        m_ImportanceDensity[index] += importance;
        ++m_IntersectionCount[index];
        ++m_nTotalIntersections;
    }
    
    void finalize(const CurvilinearSkeleton& skeleton, ParamSet* statistics) {
        if(m_bUseClustering) {
            std::vector<float> newDensity(skeleton.size(), 0.f);
            std::vector<size_t> newIntersectionCount(skeleton.size(), 0);
            
            for(size_t i = 0; i < m_SkelClustering->size(); ++i) {
                const CurvSkelClustering::Cluster& cluster = m_SkelClustering->getCluster(i);
                
                float density = m_ImportanceDensity[i]/* / cluster.nodes.size()*/;
                size_t intersectionCount = m_IntersectionCount[i]/* / cluster.nodes.size()*/;
                
                for(GraphNodeIndex node: cluster.nodes) {
                    newDensity[node] = density;
                    newIntersectionCount[node] = intersectionCount;
                }
            }
            
            std::swap(newDensity, m_ImportanceDensity);
            std::swap(newIntersectionCount, m_IntersectionCount);
        }
        
        computeStatistics(statistics);
    }
    
    void computeStatistics(ParamSet* statistics) {
        m_fMaxDensity = 0.f;
        m_fMinDensity = float(embree::inf);
        m_fTotalDensity = 0.f;

        for (float & value: m_ImportanceDensity) {
            if (value < m_fMinDensity) {
                m_fMinDensity = value;
            }

            if (value > m_fMaxDensity) {
                m_fMaxDensity = value;
            }

            m_fTotalDensity += value;
        }

        m_fMeanDensity = m_fTotalDensity / m_ImportanceDensity.size();
        
        if(statistics) {
            std::clog << "maxImportance: " << m_fMaxDensity << std::endl;
            std::clog << "minImportance: " << m_fMinDensity << std::endl;
            std::clog << "totalImportance: " << m_fTotalDensity << std::endl;
            std::clog << "meanImportance: " << m_fMeanDensity << std::endl;
            
            statistics->set("maxImportance", m_fMaxDensity);
            statistics->set("minImportance", m_fMinDensity);
            statistics->set("totalImportance", m_fTotalDensity);
            statistics->set("meanImportance", m_fMeanDensity);
        }
    }
    
    bool filter(const Scene& scene, const std::string& filterName, ParamSet* statistics) {
        bool returnValue = false;
        
        if(filterName == "identity") {
            returnValue = true;
        }
        else if(filterName == "max") {
            returnValue = maxFilter(scene);
        }
        else if(filterName == "maxBallMax") {
            returnValue = maxBallMaxFilter(scene);
        }
        else if(filterName == "maxBallMean") {
            returnValue = maxBallMeanFilter(scene);
        }
        else if(filterName == "maxBallMedian") {
            returnValue = maxBallMedianFilter(scene);
        }
        else if(filterName == "maxBallOpening") {
            returnValue = maxBallOpeningFilter(scene);
        }
        else if(filterName == "maxBallClosing") {
            returnValue = maxBallClosingFilter(scene);
        }
        else if(filterName == "maxBallOpeningClosing") {
            returnValue = maxBallOpeningFilter(scene) && maxBallClosingFilter(scene);
        }
        else if(filterName == "maxBallClosingOpening") {
            returnValue = maxBallClosingFilter(scene) && maxBallOpeningFilter(scene);
        }
        else if(filterName == "maxBallMeanOpening") {
            returnValue = maxBallMeanFilter(scene) && maxBallOpeningFilter(scene);
        }
        else if(filterName == "maxBallMeanMin") {
            returnValue = maxBallMeanFilter(scene) && maxBallMinFilter(scene);
        }
        else if(filterName == "maxBallMean50") {
            for(size_t i = 0; i < 50; ++i) {
                maxBallMeanFilter(scene);
            }
            returnValue = true;
        }
        else if(filterName == "maxBallMean8") {
            for(size_t i = 0; i < 8; ++i) {
                maxBallMeanFilter(scene);
            }
            returnValue = true;
        } else {
            std::cerr << "Unknow filter: " << filterName << std::endl;
            returnValue = false;
        }
        
        computeStatistics(statistics);
        
        return returnValue;
    }
    
    /**
     * Filter the density by taking the maximal density value on the neighbouring of each node.
     */
    bool maxFilter(const Scene& scene) {
        const CurvilinearSkeleton& skeleton = scene.voxelSpace->getSkeleton();

        if (skeleton.empty()) {
            return false;
        }
        
        std::vector<float> newDensity(skeleton.size());
        for(GraphNodeIndex idx = 0, end = skeleton.size(); idx < end; ++idx) {
            float max = m_ImportanceDensity[idx];
            for(GraphNodeIndex neighbour: skeleton.neighbours(idx)) {
                float value = m_ImportanceDensity[neighbour];
                if(value > max) {
                    max = value;
                }
            }
            newDensity[idx] = max;
        }
        
        std::swap(newDensity, m_ImportanceDensity);
        
        return true;
    }
    
    /**
     * Filter the density by taking the maximal density value on the maximal ball of each node
     */
    bool maxBallMaxFilter(const Scene& scene) {
        const CurvilinearSkeleton& skeleton = scene.voxelSpace->getSkeleton();

        if (skeleton.empty()) {
            return false;
        }
        
        Graph maxBallGraph = computeMaxBallGraph(skeleton);
        
        std::vector<float> newDensity(skeleton.size());
        for(GraphNodeIndex idx = 0, end = skeleton.size(); idx < end; ++idx) {
            float max = m_ImportanceDensity[idx];
            
            for(GraphNodeIndex neighbour: maxBallGraph[idx]) {
                float value = m_ImportanceDensity[neighbour];
                if(value > max) {
                    max = value;
                }
            }
            newDensity[idx] = max;
        }
        
        std::swap(newDensity, m_ImportanceDensity);
        
        return true;
    }
    
    /**
     * Filter the density by taking the maximal density value on the maximal ball of each node
     */
    bool maxBallMinFilter(const Scene& scene) {
        const CurvilinearSkeleton& skeleton = scene.voxelSpace->getSkeleton();

        if (skeleton.empty()) {
            return false;
        }
        
        Graph maxBallGraph = computeMaxBallGraph(skeleton);
        
        std::vector<float> newDensity(skeleton.size());
        for(GraphNodeIndex idx = 0, end = skeleton.size(); idx < end; ++idx) {
            float min = m_ImportanceDensity[idx];
            
            for(GraphNodeIndex neighbour: maxBallGraph[idx]) {
                float value = m_ImportanceDensity[neighbour];
                if(value < min) {
                    min = value;
                }
            }
            newDensity[idx] = min;
        }
        
        std::swap(newDensity, m_ImportanceDensity);
        
        return true;
    }
    
    /**
     * Filter the density by taking the maximal density value on the maximal ball of each node
     */
    bool maxBallMeanFilter(const Scene& scene) {
        const CurvilinearSkeleton& skeleton = scene.voxelSpace->getSkeleton();

        if (skeleton.empty()) {
            return false;
        }
        
        Graph maxBallGraph = computeMaxBallGraph(skeleton);
        
        std::vector<float> newDensity(skeleton.size());
        for(GraphNodeIndex idx = 0, end = skeleton.size(); idx < end; ++idx) {
            float sum = m_ImportanceDensity[idx];
            
            for(GraphNodeIndex neighbour: maxBallGraph[idx]) {
                sum +=  m_ImportanceDensity[neighbour];
            }
            newDensity[idx] = sum / (1 + maxBallGraph[idx].size());
        }
        
        std::swap(newDensity, m_ImportanceDensity);
        
        return true;
    }
    
    bool maxBallMedianFilter(const Scene& scene) {
        const CurvilinearSkeleton& skeleton = scene.voxelSpace->getSkeleton();

        if (skeleton.empty()) {
            return false;
        }
        
        Graph maxBallGraph = computeMaxBallGraph(skeleton);
        
        std::vector<float> newDensity(skeleton.size());
        for(GraphNodeIndex idx = 0, end = skeleton.size(); idx < end; ++idx) {
            std::vector<float> neighbourDensities;
            
            neighbourDensities.push_back(m_ImportanceDensity[idx]);
            
            for(GraphNodeIndex neighbour: maxBallGraph[idx]) {
                neighbourDensities.push_back(m_ImportanceDensity[neighbour]);
            }
            
            std::sort(neighbourDensities.begin(), neighbourDensities.end());
            
            newDensity[idx] = neighbourDensities[neighbourDensities.size() / 2];
        }
        
        std::swap(newDensity, m_ImportanceDensity);
        
        return true;
    }

    bool maxBallClosingFilter(const Scene& scene) {
        // Dilation && Erosion
        return maxBallMaxFilter(scene) && maxBallMinFilter(scene);
    }
    
    bool maxBallOpeningFilter(const Scene& scene) {
        // Erosion && Dilation
        return maxBallMinFilter(scene) && maxBallMaxFilter(scene);
    }
    
    bool maxBallSetVariance(const Scene& scene) {
        const CurvilinearSkeleton& skeleton = scene.voxelSpace->getSkeleton();

        if (skeleton.empty()) {
            return false;
        }
        
        Graph maxBallGraph = computeMaxBallGraph(skeleton);
        
        std::vector<float> newDensity(skeleton.size());
        for(GraphNodeIndex idx = 0, end = skeleton.size(); idx < end; ++idx) {
            float sum = m_ImportanceDensity[idx];
            
            for(GraphNodeIndex neighbour: maxBallGraph[idx]) {
                sum +=  m_ImportanceDensity[neighbour];
            }
            newDensity[idx] = embree::abs(m_ImportanceDensity[idx] - sum / (1 + maxBallGraph[idx].size()));
        }
        
        std::swap(newDensity, m_ImportanceDensity);
        
        return true;
    }

    float getDensity(GraphNodeIndex node) const {
        return m_ImportanceDensity[node];
    }
    
    void setDensity(GraphNodeIndex node, float value) {
        m_ImportanceDensity[node] = value;
    }
    
    size_t getIntersectionCount(GraphNodeIndex node) const {
        return m_IntersectionCount[node];
    }
    
    size_t getTotalIntersectionCount() const {
        return m_nTotalIntersections;
    }

    float getTotalDensity() const {
        return m_fTotalDensity;
    }

    float getMaxDensity() const {
        return m_fMaxDensity;
    }

    float getMinDensity() const {
        return m_fMinDensity;
    }

    float getMeanDensity() const {
        return m_fMeanDensity;
    }

private:
    std::vector<float> m_ImportanceDensity; // For each cluster, a density is record
    std::vector<size_t> m_IntersectionCount; // For each cluster, the number of nearest intersections reached is record
    size_t m_nTotalIntersections;
    float m_fTotalDensity; // The sum of the densities
    float m_fMaxDensity; // The maximal density
    float m_fMinDensity; // The minimal density
    float m_fMeanDensity; // The mean of the densities
    
    bool m_bUseClustering;
    const CurvSkelClustering* m_SkelClustering;
};

void computeCurvSkelImportanceDistribution(CurvSkelImportanceDistribution& distribution,
    size_t viewRayCountX, size_t viewRayCountY, size_t scatteringRayCountX, size_t scatteringRayCountY,
    bool useClustering, const Scene& scene, const Camera& camera, Random& rng, ParamSet* statistics);

}

#endif
