#ifndef _BONEZ_GRAPHIMPORTANCEESTIMATION_HPP_
#define _BONEZ_GRAPHIMPORTANCEESTIMATION_HPP_

#include <vector>

#include "bonez/common/common.hpp"

namespace BnZ {

typedef std::vector<float> GraphImportanceEstimation;

template<typename NearestNodeFunctor>
GraphImportanceEstimation computeImportanceEstimation(
    const Vec2ui& viewRayCount, const Vec2ui& scatteringRayCount, const Scene& scene, const Camera& camera,
    Random& rng, const Graph& graph, NearestNodeFunctor nearestNodeFunctor) {
    GraphImportanceEstimation estimation(graph.size(), 0.f);
    
    // Samples for the view rays
    size_t nViewRayCount = viewRayCount.x * viewRayCount.y;
    std::unique_ptr<Vec2f[]> viewSamples(new Vec2f[nViewRayCount]);
    createJittered2DPattern(viewSamples.get(), viewRayCount.x, viewRayCount.y, rng);
    
    // Samples for the scattering rays
    size_t nScatteringRayCount = scatteringRayCount.x * scatteringRayCount.y;
    std::unique_ptr<Vec2f[]> scatteringSamples(new Vec2f[nScatteringRayCount]);

    float importance = 1.f * embree::rcp((float) nViewRayCount * (float) nScatteringRayCount);

    for (size_t i = 0; i < nViewRayCount; ++i) {
        Ray viewRay = camera.ray(viewSamples[i]);
        Intersection I = intersect(viewRay, scene);

        BRDF brdf = scene.shade(I);

        if (!I) {
            continue;
        }

        Vec3f wo = -viewRay.dir;
        createJittered2DPattern(scatteringSamples.get(), scatteringRayCount.x, scatteringRayCount.y, rng);

        for (size_t s = 0; s < nScatteringRayCount; ++s) {
            Sample3f wi;
            Col3f reflectionTerm = brdf.sample(wo, wi, scatteringSamples[s]);

            if (wi.pdf == 0.f || reflectionTerm == Col3f(0.f)) {
                continue;
            }

            Intersection intersection = intersect(secondary_ray(I, wi.value), scene);
            if (!intersection) {
                continue;
            }
            
            float contrib = importance * embree::luminance(reflectionTerm) * embree::rcp(wi.pdf);

            GraphNodeIndex nodeIdx = nearestNodeFunctor(intersection);
            
            if (nodeIdx != UNDEFINED_NODE) {
                estimation[nodeIdx] += contrib;
            }
        }
    }
    
    return estimation;
}

struct GraphImportanceEstimationStats {
    float sum;
    float max;
    float mean;
    
    GraphImportanceEstimationStats():
        sum(0.f), max(0.f), mean(0.f) {
    }
};

GraphImportanceEstimationStats computeStatistics(const GraphImportanceEstimation& importance) {
    GraphImportanceEstimationStats stats;
    for(float value: importance) {
        stats.sum += value;
        if(value > stats.max) {
            stats.max = value;
        }
    }
    stats.mean = stats.sum / importance.size();
    return stats;
}

}

#endif
