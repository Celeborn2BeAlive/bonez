#ifndef _BONEZ_CURVSKELSENSORNETWORK_HPP_
#define _BONEZ_CURVSKELSENSORNETWORK_HPP_

#include <vector>

#include "bonez/scene/Scene.hpp"
#include "CurvilinearSkeleton.hpp"
#include "CurvSkelClustering.hpp"

namespace BnZ {

class CurvSkelSensorNetwork {
public:
    struct Sensor {
        Intersection I;
        GraphNodeIndex sourceNodeIdx;
        GraphNodeIndex targetNodeIdx;
        GraphNodeIndex sourceClusterIdx;
        GraphNodeIndex targetClusterIdx;
        
        Sensor(
            const Intersection& I,
            GraphNodeIndex sourceNodeIdx,
            GraphNodeIndex targetNodeIdx,
            GraphNodeIndex sourceClusterIdx,
            GraphNodeIndex targetClusterIdx
        ):
            I(I), 
            sourceNodeIdx(sourceNodeIdx), 
            targetNodeIdx(targetNodeIdx), 
            sourceClusterIdx(sourceClusterIdx),
            targetClusterIdx(targetClusterIdx) {
        }
    };
    
    typedef size_t SensorIndex;
    typedef std::vector<Sensor> SensorVector;
    typedef std::vector<SensorIndex> SensorIdxVector;

    CurvSkelSensorNetwork() {
    }
    
    void build(size_t sensorCountPerNode, const Scene& scene, Random& rng);
    
    bool empty() const {
        return m_Sensors.empty();
    }
    
    const Sensor& operator [](size_t idx) const {
        return m_Sensors[idx];
    }

    SensorVector m_Sensors;
    
    std::vector<SensorIdxVector> m_SensorsPerSourceNode;
    std::vector<SensorIdxVector> m_SensorsPerSourceCluster;
    std::vector<SensorIdxVector> m_SensorsPerTargetNode;
    std::vector<SensorIdxVector> m_SensorsPerTargetCluster;

    const Scene* m_pScene;
};

}

#endif
