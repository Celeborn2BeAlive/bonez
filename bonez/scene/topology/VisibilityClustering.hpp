#ifndef _BONEZ_VISIBILITYCLUSTERING_HPP
#define _BONEZ_VISIBILITYCLUSTERING_HPP

#include "CurvSkelClustering.hpp"

namespace BnZ {

struct VisibilityGate {
    uint32_t originCluster, destinationCluster;
    Vec3f center;
    Vec3f N;
    float radius;

    VisibilityGate(uint32_t originCluster, uint32_t destinationCluster, Vec3f center, Vec3f N, float radius):
        originCluster(originCluster), destinationCluster(destinationCluster), center(center), N(N), radius(radius) {
    }
};

class VisibilityCluster: std::vector<GraphNodeIndex> {
    typedef std::vector<GraphNodeIndex> Base;
public:
    using Base::iterator;
    using Base::const_iterator;
    using Base::push_back;
    using Base::emplace_back;
    using Base::operator [];
    using Base::empty;
    using Base::size;
    using Base::begin;
    using Base::end;

    struct NeighbourLink {
        uint32_t neighbour;
        uint32_t gateIdx;

        NeighbourLink(uint32_t neighbour,
                      uint32_t gateIdx):
            neighbour(neighbour),
            gateIdx(gateIdx) {
        }
    };

    typedef std::vector<NeighbourLink> NeighbourVector;

    VisibilityCluster(Col3f color):
        m_bIsTransfertCluster(false), m_Color(color) {
    }

    const NeighbourVector& getNeighbourLinks() const {
        return m_Neighbours;
    }

    void addNeighbour(NeighbourLink link) {
        m_Neighbours.emplace_back(link);
        if(m_Neighbours.size() == 2) {
            if(m_Neighbours[0].neighbour != m_Neighbours[1].neighbour) {
                m_bIsTransfertCluster = true;
            } else {
                m_bIsTransfertCluster = false;
            }
        } else {
            m_bIsTransfertCluster = false;
        }
    }

    const Col3f& getColor() const {
        return m_Color;
    }

    // A transfert cluster has 2, and only 2 different neighbours
    bool isTransfertCluster() const {
        return m_bIsTransfertCluster;
    }

private:
    std::vector<NeighbourLink> m_Neighbours;
    bool m_bIsTransfertCluster;
    Col3f m_Color;
};

class VisibilityClustering: std::vector<VisibilityCluster> {
    typedef std::vector<VisibilityCluster> Base;
public:
    using Base::iterator;
    using Base::const_iterator;
    using Base::empty;
    using Base::size;
    using Base::operator [];
    using Base::begin;
    using Base::end;

    typedef std::vector<VisibilityGate> GateContainer;

    static const int NO_CLUSTER;

    VisibilityClustering() {
    }

    VisibilityClustering(const CurvSkelClustering& clustering):
        m_ClusterOf(clustering.size(), NO_CLUSTER) {
    }

    uint32_t addCluster() {
        uint32_t idx = size();
        emplace_back(Col3f{ m_ColorRNG.getFloat(), m_ColorRNG.getFloat(), m_ColorRNG.getFloat() });
        return idx;
    }

    int getClusterOf(GraphNodeIndex node) const {
        assert(node < m_ClusterOf.size());
        return m_ClusterOf[node];
    }

    void setClusterOf(GraphNodeIndex node, int clusterIdx) {
        assert(node < m_ClusterOf.size());
        assert(clusterIdx < size());
        m_ClusterOf[node] = clusterIdx;
        (*this)[clusterIdx].emplace_back(node);
    }

    void link(uint32_t originCluster, uint32_t destinationCluster, Vec3f center, Vec3f N, float radius) {
        assert(originCluster < size());
        assert(destinationCluster < size());

        uint32_t gateIdx = m_Gates.size();
        m_Gates.emplace_back(originCluster, destinationCluster, center, N, radius);

        (*this)[originCluster].addNeighbour({ destinationCluster, gateIdx });
    }

    const GateContainer& getGates() const {
        return m_Gates;
    }

    const VisibilityGate& getGate(uint32_t idx) const {
        return m_Gates[idx];
    }

    const VisibilityGate& getGate(const VisibilityCluster::NeighbourLink& link) const {
        return m_Gates[link.gateIdx];
    }

private:
    GateContainer m_Gates;
    std::vector<int> m_ClusterOf;
    Random m_ColorRNG;
};

VisibilityClustering buildVisibilityClustering(const CurvSkelClustering& clustering);

void setVisibilityClusteringColors(VoxelSpace& voxelSpace, const VisibilityClustering& vclustering);

}

#endif // _BONEZ_VISIBILITYCLUSTERING_HPP
