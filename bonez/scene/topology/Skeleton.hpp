#ifndef _BONEZ_SKELETON_HPP
#define _BONEZ_SKELETON_HPP

#include "bonez/common/common.hpp"
#include "bonez/common/data/Graph.hpp"

#include <vector>

namespace BnZ {

class SurfacePoint;

class Skeleton {
public:
    static const uint32_t NO_FLAG = 0;
    static const uint32_t HIGH_CURVATURE_FLAG = 1;
    static const uint32_t COVERAGE_FLAG = 2;

    struct Node {
        Vec3f P; //! Position of the node
        float maxball; //! Radius of the maxball centered in the node
        uint32_t flag;

        Node(Vec3f P, float maxball):
            P(P), maxball(maxball), flag(NO_FLAG) {
        }

        Node(Vec3f P, float maxball, uint32_t flag):
            P(P), maxball(maxball), flag(flag) {
        }

        Node(): flag(NO_FLAG) {
        }
    };

    virtual ~Skeleton() {
    }

    virtual size_t size() const = 0;

    virtual bool empty() const = 0;

    virtual GraphNodeIndex getNearestNode(const Vec3f& P) const = 0;

    virtual GraphNodeIndex getNearestNode(const Vec3f& P, const Vec3f& N) const = 0;

    virtual GraphNodeIndex getNearestNode(const SurfacePoint& point) const = 0;

    virtual const Node getNode(GraphNodeIndex index) const = 0;

    virtual const Graph& getGraph() const = 0;

    virtual const AdjacencyList& neighbours(GraphNodeIndex idx) const = 0;

    /*
    virtual float getNodeCoherence(const Vec3f& P, GraphNodeIndex node) const = 0;

    virtual float getNodeCoherence(const Vec3f& P, const Vec3f& N, GraphNodeIndex node) const = 0;

    virtual float getNodeCoherence(const SurfacePoint& point, GraphNodeIndex node) const = 0;*/
};

// A skeleton line is a set of connected nodes such that the degree of each one is 2
struct SkeletonLine {
    std::vector<GraphNodeIndex> nodes; // The nodes;
    GraphNodeIndex extremums[2]; // The two extremum nodes (connected nodes that are not in the line)
    Vec3f extremumPositions[2]; // The positions of the extremum nodes gives the real straight line associated with the skeleton line

    void addNode(GraphNodeIndex n) {
        nodes.push_back(n);
    }
};

class SkeletonLineSet: std::vector<SkeletonLine> {
    void build(const Skeleton& skeleton);

    typedef std::vector<SkeletonLine> Base;
    std::vector<int> m_LineOf;
public:
    static const int NO_LINE;
    static const int CURVATURE_NODE;
    static const int COVERAGE_NODE;

    SkeletonLineSet() {
    }

    SkeletonLineSet(const Skeleton& skeleton):
        m_LineOf(skeleton.size(), NO_LINE) {
        build(skeleton);
    }

    int getLineOf(GraphNodeIndex node) const {
        return m_LineOf[node];
    }

    void setLineOf(GraphNodeIndex node, int line) {
        m_LineOf[node] = line;
    }

    void segmentWrtMaxballCurvature(const Skeleton& skeleton, float maxballScale = 1.f);

    void segmentWrtMaxballCoverage(const Skeleton& skeleton);

    void segmentWrtMaxballVariance(const Skeleton& skeleton);

    using Base::iterator;
    using Base::const_iterator;
    using Base::empty;
    using Base::size;
    using Base::operator [];
    using Base::begin;
    using Base::end;
};

/**
 * Compute the list of the indices of nodes that belongs to the maxball of a given node.
 */
AdjacencyList computeMaxBallAdjacencyList(GraphNodeIndex node,
    const Skeleton& skeleton);

/**
 * Compute a graph MBG from the skeleton where the edge (i, j) is in MBG if j belongs to the maxball
 * of i.
 */
Graph computeMaxBallGraph(const Skeleton& skeleton);

GraphNodeIndex computeBiggestNode(const Skeleton& skeleton);

}

#endif // _BONEZ_SKELETON_HPP
