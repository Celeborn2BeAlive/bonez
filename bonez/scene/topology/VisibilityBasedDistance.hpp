#ifndef _BONEZ_SKELETONS_VISIBILITYBASEDDISTANCE_HPP_
#define _BONEZ_SKELETONS_VISIBILITYBASEDDISTANCE_HPP_

#include <vector>
#include "bonez/scene/Scene.hpp"
#include "CurvilinearSkeleton.hpp"

namespace BnZ {

/**
 * A distance builded on the skeleton nodes based on the visibility from a point.
 */
struct VisibilityBasedDistance {
    typedef size_t DistanceType;

    std::vector<DistanceType> m_Distance;
    std::vector<bool> m_IsVisibleFromSource;

    template<typename PositionFunctor>
    VisibilityBasedDistance(const Graph& graph, const Scene& scene, const Vec3f& point, 
        PositionFunctor positionFunctor) {
        for (GraphNodeIndex i = 0; i < graph.size(); ++i) {
            Vec3f nodePosition = positionFunctor(i);
            Vec3f direction = point - nodePosition;
            float ldir = embree::length(direction);
            direction /= ldir;

            if (scene.occluded(embree::Ray(nodePosition, direction, 128.0f * (float) embree::ulp,
                                           ldir - 128.0f * (float) embree::ulp))) {
                m_Distance.push_back(10);
                m_IsVisibleFromSource.push_back(false);
            }
            else {
                m_Distance.push_back(1);
                m_IsVisibleFromSource.push_back(true);
            }
        }
    }

    bool isVisibleFromSource(GraphNodeIndex u) const {
        return m_IsVisibleFromSource[u];
    }

    DistanceType operator()(GraphNodeIndex u, GraphNodeIndex v) const {
        return m_Distance[v];
    }
};

}

#endif
