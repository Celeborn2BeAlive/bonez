#include "CurvilinearSkeleton.hpp"
#include "bonez/scene/VoxelSpace.hpp"

#include <queue>

namespace BnZ {

Vec3i CurvilinearSkeleton::getVoxel(const Vec3f& position) const {
    return m_pVoxelSpace->getVoxel(position);
}

Vec3f CurvilinearSkeleton::getVoxelSelector(const Vec3f& position, const Vec3f& normal) const {
    return position + m_pVoxelSpace->getVoxelSize() * normal;
}

/**
 * Compute a list of node which are local minimas in their neighbour for the maxball radius.
 */
NodeVector computeLocalMinimas(const CurvilinearSkeleton& skeleton) {
    Graph graph = computeMaxBallGraph(skeleton);
    NodeVector nodes;
    
    for(GraphNodeIndex idx = 0, end = skeleton.size(); idx != end; ++idx) {
        bool isMinima = true;
        for(GraphNodeIndex n: graph[idx]) {
            if(skeleton[n].getMaxball() < skeleton[idx].getMaxball()) {
                isMinima = false;
                break;
            }
        }
        if(isMinima) {
            nodes.push_back(idx);
        }
    }
    
    return nodes;
    
    /*NodeVector nodes;
    
    for(NodeIndex idx = 0, end = skeleton.size(); idx != end; ++idx) {
        bool isMinima = true;
        for(NodeIndex directNeighbour: skeleton.neighbours(idx)) {
            if(skeleton[directNeighbour].getMaxball() <= skeleton[idx].getMaxball()) {
                isMinima = false;
                break;
            }
        }
        if(isMinima) {
            nodes.push_back(idx);
        }
    }
    
    return nodes;*/
}


void CurvilinearSkeleton::blur() {
    NodeArray nodes = m_Nodes;
    for(auto& node: range(size())) {
        Vec3f sumPositions(0.f);
        float sumMaxballs(0.f);
        for(const auto& neighbour: m_Graph[node]) {
            sumPositions += nodes[neighbour].getPosition();
            sumMaxballs += nodes[neighbour].getMaxball();
        }
        sumPositions /= (float) m_Graph[node].size();
        sumMaxballs /= (float) m_Graph[node].size();
        m_Nodes[node] = Node(sumPositions, sumMaxballs);
    }
}

void CurvilinearSkeleton::blurMaxballs() {
    NodeArray nodes = m_Nodes;
    for(auto& node: range(size())) {
        float sumMaxballs(0.f);
        for(const auto& neighbour: m_Graph[node]) {
            sumMaxballs += nodes[neighbour].getMaxball();
        }
        sumMaxballs /= (float) m_Graph[node].size();
        m_Nodes[node] = Node(nodes[node].getPosition(), sumMaxballs);
    }
}

typedef std::pair<Vec3i, float> CompareNode;
struct CompareNodeFunctor {
    bool operator ()(const CompareNode& lhs,
                     const CompareNode& rhs) {
        return lhs.second < rhs.second;
    }
};

#if 0
CurvilinearSkeleton::GridType computeGridWrtVoxelGrid(const VoxelGrid& voxelGrid,
                                                      const CurvilinearSkeleton& skeleton) {
    std::vector<GraphNodeIndex> nodes(skeleton.size());
    std::vector<uint32_t> degrees(skeleton.size());

    CurvilinearSkeleton::GridType grid(skeleton.getGrid().getBBox(),
                                       skeleton.getGrid().getSize(),
                                       skeleton.getGrid().getWorldToGridTransform(),
                                       skeleton.getGrid().getWorldToGridScale());
    grid.fill(UNDEFINED_NODE);

    float scale = skeleton.getGrid().getWorldToGridScale();

    for(auto i: range(skeleton.size())) {
        nodes[i] = i;
        degrees[i] = skeleton.neighbours(i).size();
    }
    std::sort(begin(nodes), end(nodes), [&skeleton, &degrees](GraphNodeIndex i, GraphNodeIndex j) {
        if(skeleton.getNode(i).maxball != skeleton.getNode(j).maxball) {
            return skeleton.getNode(i).maxball > skeleton.getNode(j).maxball;
        }
        return degrees[i] > degrees[j];
    });

    std::priority_queue<CompareNode, std::vector<CompareNode>, CompareNodeFunctor> queue;

    for(auto i: range(skeleton.size())) {
        auto node = skeleton.getNode(nodes[i]);
        Vec3i voxel = skeleton.getGrid().getVoxel(node.P);
        queue.emplace(std::make_pair(voxel, scale * node.maxball));
        grid.setData(voxel, nodes[i]);
    }

    Vec3ui size = skeleton.getGrid().getSize();

    // 6-neighbouring
#define E(v) Vec3i(v.x + 1, v.y, v.z)
#define W(v) Vec3i(v.x - 1, v.y, v.z)
#define N(v) Vec3i(v.x, v.y + 1, v.z)
#define S(v) Vec3i(v.x, v.y - 1, v.z)
#define F(v) Vec3i(v.x, v.y, v.z + 1)
#define B(v) Vec3i(v.x, v.y, v.z - 1)

/*
#define PROCESS_VOXEL \
    Skeleton::Node n = skeleton.getNode(nodeIdx); \
    Vec3f P = xfmPoint(skeleton.getGrid().getGridToWorldTransform(), \
                       Vec3f(neighbour.x, neighbour.y, neighbour.z)); \
    Vec3f N = embree::normalize(xfmNormal(skeleton.getGrid().getGridToWorldTransform(), \
                        Vec3f(normal.x, normal.y, normal.z))); \
    Vec3f wi = embree::normalize(xfmVector(skeleton.getGrid().getGridToWorldTransform(), n.P - P)); \
    float validity = (embree::dot(N, wi) * n.maxball) / embree::distance(n.P, P); \
    if(grid.getData(neighbour) == UNDEFINED_NODE) { \
        grid.setData(neighbour, nodeIdx); \
        queue.push(std::make_pair(neighbour, validity)); \
    } else { \
        Skeleton::Node n2 = skeleton.getNode(grid.getData(neighbour)); \
        Vec3f wi2 = embree::normalize(xfmVector(skeleton.getGrid().getGridToWorldTransform(), n2.P - P)); \
        float validity2 = (embree::dot(N, wi2) * n2.maxball) / embree::distance(n2.P, P); \
        if(validity2 < validity) { \
            grid.setData(neighbour, nodeIdx); \
            queue.push(std::make_pair(neighbour, validity)); \
        } \
    }
*/

#define PROCESS_VOXEL \
    Skeleton::Node n = skeleton.getNode(nodeIdx); \
    Vec3f P = xfmPoint(skeleton.getGrid().getGridToWorldTransform(), \
                       Vec3f(neighbour.x, neighbour.y, neighbour.z)); \
    Vec3f N = embree::normalize(xfmNormal(skeleton.getGrid().getGridToWorldTransform(), \
                        Vec3f(normal.x, normal.y, normal.z))); \
    Vec3f wi = embree::normalize(xfmVector(skeleton.getGrid().getGridToWorldTransform(), n.P - P)); \
    if(grid.getData(neighbour) == UNDEFINED_NODE && embree::dot(N, wi) > 0.f) { \
        grid.setData(neighbour, nodeIdx); \
        queue.push(std::make_pair(neighbour, node.second - 1)); \
    }

    while(!queue.empty()) {
        auto node = queue.top();
        queue.pop();

        if(voxelGrid(node.first.x, node.first.y, node.first.z)) {
            //grid.setData(node.first, UNDEFINED_NODE);
            continue;
        }

        GraphNodeIndex nodeIdx = grid.getData(node.first);

        if(node.first.x > 0) {
            Vec3i neighbour = W(node.first);
            Vec3i normal(1, 0, 0);
            PROCESS_VOXEL
        }

        if(node.first.y > 0) {
            Vec3i neighbour = S(node.first);
            Vec3i normal(0, 1, 0);
            PROCESS_VOXEL
        }

        if(node.first.z > 0) {
            Vec3i neighbour = B(node.first);
            Vec3i normal(0, 0, 1);
            PROCESS_VOXEL
        }

        if(node.first.x < (int)size.x - 2) {
            Vec3i neighbour = E(node.first);
            Vec3i normal(-1, 0, 0);
            PROCESS_VOXEL
        }

        if(node.first.y < (int)size.y - 2) {
            Vec3i neighbour = N(node.first);
            Vec3i normal(0, -1, 0);
            PROCESS_VOXEL
        }

        if(node.first.z < (int)size.z - 2) {
            Vec3i neighbour = F(node.first);
            Vec3i normal(0, 0, -1);
            PROCESS_VOXEL
        }
    }

    return grid;
}
#endif

typedef std::tuple<Vec3i, GraphNodeIndex, float> CompareNode2;
struct CompareNode2Functor {
    bool operator ()(const CompareNode2& lhs,
                     const CompareNode2& rhs) {
        return std::get<2>(lhs) < std::get<2>(rhs);
    }
};

#if 0
CurvilinearSkeleton::GridType computeGridWrtVoxelGrid(const VoxelGrid& voxelGrid,
                                                      const CurvilinearSkeleton& skeleton) {
    std::vector<GraphNodeIndex> nodes(skeleton.size());
    std::vector<uint32_t> degrees(skeleton.size());

    CurvilinearSkeleton::GridType grid(skeleton.getGrid().getBBox(),
                                       skeleton.getGrid().getSize(),
                                       skeleton.getGrid().getWorldToGridTransform(),
                                       skeleton.getGrid().getWorldToGridScale());
    grid.fill(UNDEFINED_NODE);

    float scale = skeleton.getGrid().getWorldToGridScale();

    for(auto i: range(skeleton.size())) {
        nodes[i] = i;
        degrees[i] = skeleton.neighbours(i).size();
    }
    std::sort(begin(nodes), end(nodes), [&skeleton, &degrees](GraphNodeIndex i, GraphNodeIndex j) {
        if(skeleton.getNode(i).maxball != skeleton.getNode(j).maxball) {
            return skeleton.getNode(i).maxball > skeleton.getNode(j).maxball;
        }
        return degrees[i] > degrees[j];
    });

    /*
    std::priority_queue<CompareNode2, std::vector<CompareNode2>, CompareNode2Functor>
            queue;*/
    std::queue<CompareNode2> queue;

    for(auto nodeIndex: nodes) {
        float radius = skeleton.getNode(nodeIndex).maxball * scale;
        float sqrRadius = radius * radius;
        Vec3i center = skeleton.getGrid().getVoxel(skeleton.getNode(nodeIndex).P);
        std::vector<Vec3i> stack;
        stack.emplace_back(center);
        while(!stack.empty()) {
            Vec3i p = stack.back();
            stack.pop_back();

            if(grid.getData(p) == UNDEFINED_NODE) {
                grid.setData(p, nodeIndex);

                if(!voxelGrid(p.x, p.y, p.z)) {
                    iterate6neighbouring(p.x, p.y, p.z, [&](int x, int y, int z) {
                        if(x >= 0 && y >= 0 && z >= 0 && x < (int) grid.getSize().x &&
                                y < (int) grid.getSize().y && z < (int) grid.getSize().z) {
                            Vec3i p(x, y, z);
                            Vec3i v = p - center;
                            if(embree::dot(v, v) <= sqrRadius) {
                                stack.emplace_back(p);
                            } else {
                                queue.push(std::make_tuple(p, nodeIndex, radius));
                            }
                        }
                    });
                }
            }
        }
    }

    while(!queue.empty()) {
        auto t = queue.front();
        queue.pop();

        Vec3i p = std::get<0>(t);
        if(grid.getData(p) == UNDEFINED_NODE) {
            grid.setData(p, std::get<1>(t));

            if(!voxelGrid(p.x, p.y, p.z)) {
                iterate6neighbouring(p.x, p.y, p.z, [&](int x, int y, int z) {
                    if(x >= 0 && y >= 0 && z >= 0 && x < (int) grid.getSize().x &&
                            y < (int) grid.getSize().y && z < (int) grid.getSize().z) {
                        Vec3i p(x, y, z);
                        queue.push(std::make_tuple(p, std::get<1>(t), std::get<2>(t) - 1));
                    }
                });
            }
        }
    }

    return grid;
}
#endif

}
