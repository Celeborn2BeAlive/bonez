#include "VisibilityClustering.hpp"
#include "bonez/common/Progress.hpp"

#include <algorithm>
#include "bonez/common/maths.hpp"

#include "bonez/scene/VoxelSpace.hpp"

namespace BnZ {

const int VisibilityClustering::NO_CLUSTER = -1;

static AdjacencyList computeMaxBallAdjacencyList(GraphNodeIndex node,
    const CurvSkelClustering& skeleton, const VisibilityClustering& clustering) {
    AdjacencyList list;

    Vec3f P = skeleton.getNode(node).P;
    float maxball = skeleton.getNode(node).maxball;

    for(GraphNodeIndex other: range(skeleton.size())) {
        if(clustering.getClusterOf(other) < 0) {
            Vec3f v = skeleton.getNode(other).P - P;
            if(embree::length(v) < maxball) {
                list.push_back(other);
            }
        }
    }

    return list;
}

static void buildVisibilityCluster(GraphNodeIndex nodeIdx, VisibilityClustering& clustering,
                                   const CurvSkelClustering& skeleton, std::vector<uint32_t>& degrees, Random& rng) {
    int clusterIdx = clustering.addCluster();

    std::vector<GraphNodeIndex> stack;
    stack.push_back(nodeIdx);

    float maxMaxball = 0.f;

    while(!stack.empty()) {
        GraphNodeIndex i = stack.back();
        stack.pop_back();

        if(clustering.getClusterOf(i) < 0) {
            clustering.setClusterOf(i, clusterIdx);
            for(GraphNodeIndex j: skeleton.neighbours(i)) {
                --degrees[j];
            }
            Skeleton::Node node = skeleton.getNode(i);
            maxMaxball = std::max(maxMaxball, node.maxball);

            AdjacencyList maxballNeighbours = computeMaxBallAdjacencyList(i, skeleton, clustering);

            for(GraphNodeIndex j: maxballNeighbours) {
                if(clustering.getClusterOf(j) < 0 && j != 162) {
                    stack.push_back(j);
                }
            }

            if(skeleton.getNode(i).flag != Skeleton::HIGH_CURVATURE_FLAG/*true*/) {
                for(GraphNodeIndex j: skeleton.neighbours(i)) {
//                    if(i == 162 && j == 166) {
//                        std::cerr << skeleton.getNode(j).maxball << std::endl;
//                        std::cerr << maxMaxball << std::endl;
//                        std::cerr << (clustering.getClusterOf(j) < 0) << std::endl;
//                        std::cerr << (skeleton.getNode(j).flag != Skeleton::HIGH_CURVATURE_FLAG) << std::endl;
//                        float relative = skeleton.getEdge(i, j).minMaxball / maxMaxball;
//                        std::cerr << relative << std::endl;
//                        std::cerr << (0.7 <= relative && relative <= 1.3) << std::endl;
//                    }

                    if(clustering.getClusterOf(j) < 0/* && (j == 101 || j == 103 || skeleton.getNode(j).flag != Skeleton::HIGH_CURVATURE_FLAG)*/) {
                        //float minMaxball = skeleton.getEdge(i, j).minMaxball;

                        float minMaxball = skeleton.getNode(j).maxball;

                        float relative = minMaxball / maxMaxball;
                        if(0.6 <= relative && relative <= 1.4) {
                            stack.push_back(j);
                        }
                    }
                }
            }
        }
    }
}

VisibilityClustering buildVisibilityClustering(const CurvSkelClustering& skeleton) {
    Progress progress;

    progress.init("Computation of VisibilityClustering", 1);

    Random rng;
    VisibilityClustering clustering { skeleton };

    std::vector<GraphNodeIndex> nodes(skeleton.size());
    std::vector<uint32_t> degrees(skeleton.size());

    for(auto i: range(skeleton.size())) {
        nodes[i] = i;
        degrees[i] = skeleton.neighbours(i).size();
    }
    std::sort(begin(nodes), end(nodes), [&skeleton, &degrees](GraphNodeIndex i, GraphNodeIndex j) {
        if(skeleton.getNode(i).maxball != skeleton.getNode(j).maxball) {
            return skeleton.getNode(i).maxball > skeleton.getNode(j).maxball;
        }
        return degrees[i] > degrees[j];
    });

    for(auto i: nodes) {
        if(clustering.getClusterOf(i) < 0) {
            buildVisibilityCluster(i, clustering, skeleton, degrees, rng);
        }
    }

    for(auto i: range(skeleton.size())) {
        for(auto j: skeleton.neighbours(i)) {
            if(clustering.getClusterOf(i) != clustering.getClusterOf(j)) {
                Vec3f center = 0.5f * (skeleton.getNode(i).P + skeleton.getNode(j).P);
                float radius = embree::min(skeleton.getNode(i).maxball, skeleton.getNode(j).maxball);
                if(skeleton.getEdge(i, j).minMaxball != float(embree::inf)) {
                    center = skeleton.getEdge(i, j).minMaxballPosition;
                    radius = skeleton.getEdge(i, j).minMaxball;
                }
                clustering.link(clustering.getClusterOf(i), clustering.getClusterOf(j), center, normalize(skeleton.getNode(i).P - skeleton.getNode(j).P), radius);
            }
        }
    }

    progress.end();

    std::cerr << "Number of VisibilityClusters = " << clustering.size() << std::endl;

    return clustering;
}

void setVisibilityClusteringColors(VoxelSpace& voxelSpace, const VisibilityClustering& vclustering) {
    NodeColors skelNodeColors;
    NodeColors clusterColors;

    for(auto node: range(voxelSpace.getClustering().size())) {
        int cluster = vclustering.getClusterOf(node);
        if(cluster >= 0) {
            clusterColors.emplace_back(vclustering[cluster].getColor());
        } else {
            clusterColors.emplace_back(Col3f(0.f));
        }
    }

    for(auto node: range(voxelSpace.getSkeleton().size())) {
        GraphNodeIndex cluster = voxelSpace.getClustering().getNodeClusterIndex(node);
        skelNodeColors.emplace_back(clusterColors[cluster]);
    }

    voxelSpace.setSkeletonNodeColors(skelNodeColors);
    voxelSpace.setClusteringNodeColors(clusterColors);
}

}
