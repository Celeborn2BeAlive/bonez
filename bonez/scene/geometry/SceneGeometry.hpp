#ifndef _BONEZ_SCENEGEOMETRY_HPP_
#define _BONEZ_SCENEGEOMETRY_HPP_

#include <vector>
#include <cassert>

#include "TriangleMesh.hpp"
#include "bonez/common/sampling/DiscreteDistribution.hpp"

namespace BnZ {

struct SurfacePointRandom {
    float mesh;
    float triangle;
    float side;
    Vec2f position;

    SurfacePointRandom(float mesh, float triangle, float side, Vec2f position):
        mesh(mesh), triangle(triangle), side(side), position(position) {
    }
};

class SceneGeometry {
    typedef std::vector<TriangleMesh> TriangleMeshContainer;
public:
    typedef TriangleMeshContainer::const_iterator const_iterator;

    BBox3f boundingBox;

    bool empty() const {
        return m_TriangleMeshes.empty();
    }

    size_t size() const {
        return m_TriangleMeshes.size();
    }
    
    void clear() {
        m_TriangleMeshes.clear();
    }

    const TriangleMesh& operator [](size_t idx) const {
        assert(idx < size());
        return m_TriangleMeshes[idx];
    }
    
    const_iterator begin() const {
        return std::begin(m_TriangleMeshes);
    }
    
    const_iterator end() const {
        return std::end(m_TriangleMeshes);
    }

    void addTriangleMesh(const TriangleMesh& mesh) {
        if(m_TriangleMeshes.empty()) {
            boundingBox = mesh.boundingBox;
        } else {
            boundingBox += mesh.boundingBox;
        }
        m_TriangleMeshes.push_back(mesh);
        m_AreaDistribution.add(mesh.getArea());
    }

    SurfacePointSample sampleSurfacePoint(const SurfacePointRandom& random) const {
        Sample1ui meshSample = m_AreaDistribution.sample(random.mesh);
        const TriangleMesh& mesh = m_TriangleMeshes[meshSample.value];
        size_t triangleIdx = embree::clamp(size_t(random.triangle * mesh.triangleCount()), size_t(0), mesh.triangleCount() - 1);
        float u, v;
        uniformSampleTriangle(random.position.x, random.position.y, u, v);
        SurfacePoint point = mesh.getSurfacePoint(triangleIdx, u, v);
        if(random.side >= 0.5f) {
            point.Ns = -point.Ns;
            point.Ng = -point.Ng;
        }
        return SurfacePointSample(point,
                                  meshSample.pdf * (1.f / (2.f * mesh.triangleCount() * mesh.getTriangle(triangleIdx).area)));
    }

private:
    TriangleMeshContainer m_TriangleMeshes;

    //! Distribution of meshes according to their area
    DiscreteDistribution m_AreaDistribution;
};

}

#endif
