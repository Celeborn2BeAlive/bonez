#ifndef _BONEZ_SCENE_TRIANGLEMESH_HPP_
#define _BONEZ_SCENE_TRIANGLEMESH_HPP_

#include <cassert>
#include <vector>

#include "bonez/common/common.hpp"
#include "../Intersection.hpp"
#include "../lights/Light.hpp"

namespace BnZ {

class TriangleMesh {
    float computeTriangleArea(unsigned int first, unsigned int second, unsigned int third) const {
        return embree::length(embree::cross(m_Vertices[second].position - m_Vertices[first].position,
                                            m_Vertices[third].position - m_Vertices[first].position)) / 2.f;
    }
public:
    /**
     * A triangle stores the index of its vertices inside the mesh.
     */
    struct Triangle {
        unsigned int first, second, third;
        float area;

        Triangle() {
        }

        Triangle(unsigned int first, unsigned int second, unsigned int third, float area):
            first(first), second(second), third(third), area(area) {
        }
    };

    struct Vertex {
        Vec3f position, normal;
        Vec2f texCoords;

        Vertex() {
        }

        Vertex(const Vec3f& position, const Vec3f& normal, const Vec2f& texCoords):
            position(position), normal(normal), texCoords(texCoords) {
        }
    };

    typedef std::vector<Vertex> VertexContainer;
    typedef std::vector<Triangle> TriangleContainer;


    TriangleMesh():
        m_Centroid(0, 0, 0), m_nMaterialIndex(embree::inf), m_fTotalArea(0.f) {
    }

    void addVertex(const Vec3f& position, const Vec3f& normal, const Vec2f& texCoords) {
        addVertex(Vertex(position, normal, texCoords));
    }

    void addVertex(const Vertex& vertex) {
        m_Vertices.push_back(vertex);
        m_Centroid += vertex.position;
        if(m_Vertices.size() == 1) {
            boundingBox = BBox3f(vertex.position);
        } else {
            boundingBox.grow(vertex.position);
        }
    }

    void addTriangle(unsigned int first, unsigned int second, unsigned int third) {
        assert(first < vertexCount() && second < vertexCount() && third < vertexCount());
        float area = computeTriangleArea(first, second, third);
        m_fTotalArea += area;
        m_Triangles.push_back(Triangle(first, second, third, area));
    }

    void setMaterialIndex(size_t index) {
        m_nMaterialIndex = index;
    }
    
    void setLight(const LightPtr& light) {
        m_pLight = light;
    }

    size_t triangleCount() const {
        return m_Triangles.size();
    }

    size_t vertexCount() const {
        return m_Vertices.size();
    }

    const Triangle& getTriangle(size_t idx) const {
        assert(idx < triangleCount());
        return m_Triangles[idx];
    }

    const TriangleContainer& getTriangles() const {
        return m_Triangles;
    }

    const Vertex& getVertex(size_t idx) const {
        assert(idx < vertexCount());
        return m_Vertices[idx];
    }

    const VertexContainer& getVertices() const {
        return m_Vertices;
    }

    size_t getMaterialIndex() const {
        return m_nMaterialIndex;
    }
    
    const LightPtr& getLight() const {
        return m_pLight;
    }

    float getArea() const {
        return m_fTotalArea;
    }
    
    void getSurfacePoint(size_t triangleIdx, float u, float v, Vec3f& P, Vec3f& N) const {
        assert(triangleIdx < triangleCount());
        TriangleMesh::Triangle triangle = getTriangle(triangleIdx);
        Vertex first = getVertex(triangle.first), second = getVertex(triangle.second), third = getVertex(triangle.third);
        float w = 1.f - u - v;
        P = w * first.position + u * second.position + v * third.position;
        N = embree::normalize(w * first.normal + u * second.normal + v * third.normal);
        //N = embree::normalize(embree::cross(second.position - first.position, third.position - first.position));
    }

    /**
     * Return a surface point on a triangle based on the barycentric coordinates (1 - u - v, u, v).
     * The point is defined by P = (1 - u - v) * F + u * S + v * T where F, S and T are the vertices of
     * the oriented triangle.
     * For the returned point to be on the triangle, the conditions (u + v) <= 1, u >= 0, v >= 0 must hold.
     */
    SurfacePoint getSurfacePoint(size_t triangleIdx, float u, float v) const {
        assert(triangleIdx < triangleCount());
        TriangleMesh::Triangle triangle = getTriangle(triangleIdx);
        Vertex first = getVertex(triangle.first), second = getVertex(triangle.second), third = getVertex(triangle.third);
        float w = 1.f - u - v;
        Vec3f position = w * first.position + u * second.position + v * third.position;
        Vec3f Ng = embree::normalize(embree::cross(second.position - first.position, third.position - first.position));
        Vec3f Ns = embree::normalize(w * first.normal + u * second.normal + v * third.normal);
        Vec2f texCoords = w * first.texCoords + u * second.texCoords + v * third.texCoords;
        return SurfacePoint(position, Ns, Ng, texCoords);
    }

    Vec3f getCentroid() const {
        return m_Centroid * embree::rcp((float) m_Vertices.size());
    }
    
    BBox3f boundingBox;

private:
    VertexContainer m_Vertices;
    TriangleContainer m_Triangles;

    Vec3f m_Centroid;
    size_t m_nMaterialIndex;
    float m_fTotalArea;
    
    LightPtr m_pLight;
};

}

#endif
