#include "VoxelSpace.hpp"
#include "common/data/ColorMap.hpp"

#include <queue>

namespace BnZ {

const NodeColors& getNodeColors(const Skeleton& skeleton, const VoxelSpace& voxelSpace) {
    if(&skeleton == &voxelSpace.getClustering()) {
        return voxelSpace.getClusteringNodeColors();
    }
    return voxelSpace.getSkeletonNodeColors();
}

NodeColors getRandomColors(const Skeleton& skeleton) {
    return buildRandomColorMap(skeleton.size());
}

NodeColors getClusteringColors(const Skeleton& skeleton,
                               const VoxelSpace& voxelSpace) {
    NodeColors clusterColors = getRandomColors(voxelSpace.getClustering());
    if(&skeleton == &voxelSpace.getClustering()) {
        return clusterColors;
    }
    NodeColors colors;
    for(auto nodeIdx: range(skeleton.size())) {
        colors.emplace_back(clusterColors[voxelSpace.getClustering().getNodeClusterIndex(nodeIdx)]);
    }
    return colors;
}

NodeColors getDegreeNodeColors(const Skeleton& skeleton) {
    size_t max = 0;
    std::vector<Col3f> colors(skeleton.size());
    for(GraphNodeIndex i: range(skeleton.size())) {
        size_t count = skeleton.neighbours(i).size();
        max = std::max(count, max);
        colors[i].r = count;
    }

    for(GraphNodeIndex i: range(skeleton.size())) {
        colors[i] = sample(HEAT_MAP, colors[i].r / max);
    }

    return colors;
}

NodeColors getMaxballRadiusNodeColors(const Skeleton& skeleton) {
    float maxball = 0;
    for(GraphNodeIndex i: range(skeleton.size())) {
        maxball = std::max(maxball, skeleton.getNode(i).maxball);
    }

    std::vector<Col3f> colors;
    for(GraphNodeIndex i: range(skeleton.size())) {
        colors.push_back(sample(HEAT_MAP, skeleton.getNode(i).maxball / maxball));
    }
    return colors;
}

NodeColors getLocalMaxballExtremasNodeColors(const Skeleton& skeleton) {
    std::vector<Col3f> colors;
    for(GraphNodeIndex i: range(skeleton.size())) {
        bool isMaxima = true, isMinima = true;

        for(GraphNodeIndex j: skeleton.neighbours(i)) {
            if(skeleton.getNode(j).maxball > skeleton.getNode(i).maxball) {
                isMaxima = false;
            } else if(skeleton.getNode(j).maxball < skeleton.getNode(i).maxball) {
                isMinima = false;
            }
        }

        if(isMaxima) {
            colors.push_back(Col3f(1, 0, 0));
        } else if(isMinima) {
            colors.push_back(Col3f(0, 1, 0));
        } else {
            colors.push_back(Col3f(0, 0, 0));
        }
    }

    return colors;
}

NodeColors getLineMaxballExtremasNodeColors(const Skeleton& skeleton) {
    NodeColors colors(skeleton.size(), Col3f(0.f));

    SkeletonLineSet lines(skeleton);

    for(const auto& line: index(lines)) {
        float max = 0.f;
        float min = float(embree::inf);
        float variance = 0.f, mean = 0.f;

        std::vector<GraphNodeIndex> nodes = line.second.nodes;
        nodes.emplace_back(line.second.extremums[0]);
        nodes.emplace_back(line.second.extremums[1]);

        for(auto i: nodes) {
            float maxball = skeleton.getNode(i).maxball;
            max = std::max(max, maxball);
            min = std::min(min, maxball);
            mean += maxball;
            variance += maxball * maxball;
        }
        mean /= nodes.size();
        variance = variance / nodes.size() - mean * mean;

        /*
        if(variance < 0.1 * mean * mean) {
            continue;
        }*/

        for(auto i: nodes) {
            float maxball = skeleton.getNode(i).maxball;

            float nodeVariance = embree::sqr(maxball - mean);
            if(nodeVariance < 0.05 * mean * mean) {
                continue;
            }

            if(max == skeleton.getNode(i).maxball) {
                if(min == skeleton.getNode(i).maxball) {
                    colors[i] = Col3f(1, 1, 1);
                } else {
                    colors[i] = Col3f(1, 0, 0);
                }
            } else if(min == skeleton.getNode(i).maxball) {
                colors[i] = Col3f(0, 1, 0);
            }
        }
   }

    return colors;
}

typedef std::pair<GraphNodeIndex, float> CompareNode;

struct CompareNodeFunctor {
    bool operator ()(const std::pair<GraphNodeIndex, float>& lhs,
                     const std::pair<GraphNodeIndex, float>& rhs) {
        return lhs.second < rhs.second;
    }
};

NodeColors getMeyerWatershedNodeColors(const Skeleton& skeleton) {
    SkeletonLineSet lines(skeleton);

    std::vector<GraphNodeIndex> extremas;
    uint32_t nextFlag = 0;
    std::vector<uint32_t> flags(skeleton.size(), nextFlag++);

    for(const auto& line: lines) {
        float max = 0.f;
        float min = float(embree::inf);
        float mean = 0.f, variance = 0.f;

        std::vector<GraphNodeIndex> nodes = line.nodes;
        nodes.emplace_back(line.extremums[0]);
        nodes.emplace_back(line.extremums[1]);

        for(auto i: nodes) {
            float maxball = skeleton.getNode(i).maxball;
            max = std::max(max, maxball);
            min = std::min(min, maxball);
            mean += maxball;
            variance += maxball * maxball;
        }
        mean /= nodes.size();
        variance = variance / nodes.size() - mean * mean;

        for(auto i: nodes) {
            float maxball = skeleton.getNode(i).maxball;

            float nodeVariance = embree::sqr(maxball - mean);
            if(nodeVariance < 0.05 * mean * mean) {
                continue;
            }

            if(max == maxball && min != maxball) {
                extremas.push_back(i);
                flags[i] = nextFlag++;
            } else if(min == skeleton.getNode(i).maxball) {
                extremas.push_back(i);
                flags[i] = nextFlag++;
            }
        }
   }

   std::priority_queue<CompareNode, std::vector<CompareNode>, CompareNodeFunctor> queue;

   for(auto i: extremas) {
       queue.emplace(std::make_pair(i, skeleton.getNode(i).maxball));
   }

   while(!queue.empty()) {
       auto node = queue.top();
       queue.pop();

       for(auto neighbour: skeleton.neighbours(node.first)) {
           if(!flags[neighbour]) {
               flags[neighbour] = flags[node.first];
               queue.push(std::make_pair(neighbour, skeleton.getNode(neighbour).maxball));
           }
       }
   }

   ColorMap colors = buildRandomColorMap(nextFlag);
   NodeColors nodeColors(skeleton.size());

   for(auto node: range(skeleton.size())) {
       nodeColors[node] = colors[flags[node]];
   }

   return nodeColors;
}

NodeColors getVisibilityClusteringTestNodeColors(const Skeleton& skeleton) {
    SkeletonLineSet lines(skeleton);

    std::vector<GraphNodeIndex> extremas;
    uint32_t nextFlag = 0;
    std::vector<float> representativeMaxballs;
    representativeMaxballs.emplace_back(0.f);
    std::vector<uint32_t> flags(skeleton.size(), nextFlag++);

    for(const auto& line: lines) {
        float max = 0.f;
        float min = float(embree::inf);
        float mean = 0.f, variance = 0.f;

        std::vector<GraphNodeIndex> nodes = line.nodes;
        nodes.emplace_back(line.extremums[0]);
        nodes.emplace_back(line.extremums[1]);

        for(auto i: nodes) {
            float maxball = skeleton.getNode(i).maxball;
            max = std::max(max, maxball);
            min = std::min(min, maxball);
            mean += maxball;
            variance += maxball * maxball;
        }
        mean /= nodes.size();
        variance = variance / nodes.size() - mean * mean;

        for(auto i: nodes) {
            float maxball = skeleton.getNode(i).maxball;

            /*
            float nodeVariance = embree::sqr(maxball - mean);
            if(nodeVariance < 0.05 * mean * mean) {
                continue;
            }*/

            if(max == maxball && min != maxball) {
                extremas.push_back(i);
                representativeMaxballs.emplace_back(maxball);
                flags[i] = nextFlag++;
            } else if(min == skeleton.getNode(i).maxball) {
                extremas.push_back(i);
                representativeMaxballs.emplace_back(maxball);
                flags[i] = nextFlag++;
            }
        }
   }

   std::priority_queue<CompareNode, std::vector<CompareNode>, CompareNodeFunctor> queue;

   for(auto i: extremas) {
       queue.emplace(std::make_pair(i, skeleton.getNode(i).maxball));
   }

   while(!queue.empty()) {
       auto node = queue.top();
       queue.pop();

       for(auto neighbour: skeleton.neighbours(node.first)) {
           float maxball = skeleton.getNode(neighbour).maxball;

           float ratio = representativeMaxballs[flags[node.first]] / maxball;

           if(0.5 < ratio && ratio < 1.5 && flags[neighbour] != flags[node.first]) {
               flags[neighbour] = flags[node.first];
               queue.push(std::make_pair(neighbour, skeleton.getNode(neighbour).maxball));
           }
       }
   }

   ColorMap colors = buildRandomColorMap(nextFlag);
   NodeColors nodeColors(skeleton.size());

   for(auto node: range(skeleton.size())) {
       nodeColors[node] = colors[flags[node]];
   }

   return nodeColors;
}

NodeColors getMaxballRadiusPerLineNodeColors(const Skeleton& skeleton) {

    NodeColors colors(skeleton.size());

    SkeletonLineSet lines(skeleton);

    std::vector<float> scale(skeleton.size());
    float max = 0.f;

    for(const auto& line: index(lines)) {
        float max = 0.f;
        float min = float(embree::inf);
        for(auto i: line.second.nodes) {
            max = std::max(max, skeleton.getNode(i).maxball);
            min = std::min(min, skeleton.getNode(i).maxball);
        }

        for(auto i: line.second.nodes) {
            scale[i] = (skeleton.getNode(i).maxball - min) / (max - min);
        }

#if 0
        float mean = 0.f;
        //float variance = 0.f;
        for(auto i: line.second.nodes) {
            float value = skeleton.getNode(i).maxball;
            mean += value;
            //variance += value * value;
        }
        mean /= line.second.nodes.size();
        //variance = embree::abs(variance / line.second.nodes.size() - mean * mean);

        //max = std::max(variance, max);

        float max = 0.f;
        for(auto i: line.second.nodes) {
            float value =  embree::abs(skeleton.getNode(i).maxball - mean);
            max = std::max(max, value);
        }

        for(auto i: line.second.nodes) {
            scale[i] = abs(skeleton.getNode(i).maxball - mean) / max;
        }

        /*
        for(auto i: line.second.nodes) {
            scale[i] = variance;
        }*/
#endif
    }

    max = 1.f;

    for(GraphNodeIndex i: range(skeleton.size())) {
        if(lines.getLineOf(i) < 0) {
            colors[i] = Col3f(1, 1, 1);
        } else {
            colors[i] = sample(HEAT_MAP, scale[i] / max);
        }
    }

    return colors;
}

NodeColors getMaxballVariancePerLineNodeColors(const Skeleton& skeleton) {
    NodeColors colors(skeleton.size());

    SkeletonLineSet lines(skeleton);

    std::vector<float> scale(skeleton.size());
    for(const auto& line: index(lines)) {
        float mean = 0.f;
        for(auto i: line.second.nodes) {
            float value = skeleton.getNode(i).maxball;
            mean += value;
        }
        mean /= line.second.nodes.size();

        float maxVariance = 0.f;
        for(auto i: line.second.nodes) {
            float variance =  embree::sqr(skeleton.getNode(i).maxball - mean);
            maxVariance = std::max(maxVariance, variance);
        }

        for(auto i: line.second.nodes) {
            scale[i] = embree::sqr(skeleton.getNode(i).maxball - mean) / maxVariance;
            /*
            if(maxVariance > 0.2 * mean * mean) {
                scale[i] = 1.f;
            } else {
                scale[i] = 0.f;
            }*/
        }
    }

    for(GraphNodeIndex i: range(skeleton.size())) {
        if(lines.getLineOf(i) < 0) {
            colors[i] = Col3f(1, 1, 1);
        } else {
            colors[i] = sample(HEAT_MAP, scale[i]);
        }
    }

    return colors;
}

NodeColors getCurvatureNodeColors(const Skeleton& skeleton) {
    NodeColors colors(skeleton.size());

    SkeletonLineSet lines(skeleton);

    for(auto i: range(skeleton.size())) {
        if(lines.getLineOf(i) >= 0) {
            SkeletonLine& line = lines[lines.getLineOf(i)];
            Vec3f u = line.extremumPositions[0] - line.extremumPositions[1];

            Vec3f v = skeleton.getNode(i).P - line.extremumPositions[0];
            float d = embree::length(embree::cross(v, u)) / embree::length(u);
            colors[i] = sample(HEAT_MAP, d / skeleton.getNode(i).maxball);
        } else {
            colors[i] = Col3f(1, 1, 1);
        }
    }

    return colors;
}

void setRandomColors(VoxelSpace& voxelSpace) {
    voxelSpace.setSkeletonNodeColors(getRandomColors(voxelSpace.getSkeleton()));
    voxelSpace.setClusteringNodeColors(getRandomColors(voxelSpace.getClustering()));
}

void setClusteringColors(VoxelSpace& voxelSpace) {
    voxelSpace.setSkeletonNodeColors(getClusteringColors(voxelSpace.getSkeleton(), voxelSpace));
    voxelSpace.setClusteringNodeColors(getClusteringColors(voxelSpace.getClustering(), voxelSpace));
}

void setDegreeColors(VoxelSpace& voxelSpace) {
    voxelSpace.setSkeletonNodeColors(getDegreeNodeColors(voxelSpace.getSkeleton()));
    voxelSpace.setClusteringNodeColors(getDegreeNodeColors(voxelSpace.getClustering()));
}

void setMaxballRadiusColors(VoxelSpace& voxelSpace) {
    voxelSpace.setSkeletonNodeColors(getMaxballRadiusNodeColors(voxelSpace.getSkeleton()));
    voxelSpace.setClusteringNodeColors(getMaxballRadiusNodeColors(voxelSpace.getClustering()));
}

void setMaxballRadiusPerLineColors(VoxelSpace& voxelSpace) {
    voxelSpace.setSkeletonNodeColors(getMaxballRadiusPerLineNodeColors(voxelSpace.getSkeleton()));
    voxelSpace.setClusteringNodeColors(getMaxballRadiusPerLineNodeColors(voxelSpace.getClustering()));
}

void setMaxballVariancePerLineColors(VoxelSpace& voxelSpace) {
    voxelSpace.setSkeletonNodeColors(getMaxballVariancePerLineNodeColors(voxelSpace.getSkeleton()));
    voxelSpace.setClusteringNodeColors(getMaxballVariancePerLineNodeColors(voxelSpace.getClustering()));
}

void setLocalMaxballExtremasColors(VoxelSpace& voxelSpace) {
    voxelSpace.setSkeletonNodeColors(getLocalMaxballExtremasNodeColors(voxelSpace.getSkeleton()));
    voxelSpace.setClusteringNodeColors(getLocalMaxballExtremasNodeColors(voxelSpace.getClustering()));
}

void setLineMaxballExtremasColors(VoxelSpace& voxelSpace) {
    voxelSpace.setSkeletonNodeColors(getLineMaxballExtremasNodeColors(voxelSpace.getSkeleton()));
    voxelSpace.setClusteringNodeColors(getLineMaxballExtremasNodeColors(voxelSpace.getClustering()));
}

void setMeyerWatershedColors(VoxelSpace& voxelSpace) {
    voxelSpace.setSkeletonNodeColors(getMeyerWatershedNodeColors(voxelSpace.getSkeleton()));
    voxelSpace.setClusteringNodeColors(getMeyerWatershedNodeColors(voxelSpace.getClustering()));
}

void setVisibilityClusteringTestColors(VoxelSpace& voxelSpace) {
    voxelSpace.setSkeletonNodeColors(getVisibilityClusteringTestNodeColors(voxelSpace.getSkeleton()));
    voxelSpace.setClusteringNodeColors(getVisibilityClusteringTestNodeColors(voxelSpace.getClustering()));
}

void setCurvatureColors(VoxelSpace& voxelSpace) {
    voxelSpace.setSkeletonNodeColors(getCurvatureNodeColors(voxelSpace.getSkeleton()));
    voxelSpace.setClusteringNodeColors(getCurvatureNodeColors(voxelSpace.getClustering()));
}

}
