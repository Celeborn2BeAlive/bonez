#include "AssimpSceneLoader.hpp"

#include <stdexcept>
#include <string>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "bonez/common/common.hpp"
#include <embree/textures/nearestneighbor.h>

namespace BnZ {

static const aiVector3D zero(0.f, 0.f, 0.f);

TextureImage AssimpSceneLoader::loadTextureImage(const embree::FileName& filepath) {
    auto it = m_TextureCache.find(filepath);
    if(it == std::end(m_TextureCache)) {
        return (m_TextureCache[filepath] = TextureImage(embree::loadImage(filepath)));
    }
    return (*it).second;
}

static TriangleMesh buildMesh(const aiMesh* aimesh, size_t materialOffset, size_t numMaterials) {
    TriangleMesh mesh;

    if (aimesh->mMaterialIndex < 0 || aimesh->mMaterialIndex >= numMaterials) {
        throw std::runtime_error("Material index out of range");
    }

    mesh.setMaterialIndex(materialOffset + aimesh->mMaterialIndex);

    for (size_t vertexIdx = 0; vertexIdx < aimesh->mNumVertices; ++vertexIdx) {
        const aiVector3D* pPosition = &aimesh->mVertices[vertexIdx];
        const aiVector3D* pNormal = &aimesh->mNormals[vertexIdx];
        const aiVector3D* pTexCoords = aimesh->HasTextureCoords(0) ? &aimesh->mTextureCoords[0][vertexIdx] : &zero;
        mesh.addVertex(Vec3f((*pPosition)[0], (*pPosition)[1], (*pPosition)[2]),
                       Vec3f((*pNormal)[0], (*pNormal)[1], (*pNormal)[2]), Vec2f((*pTexCoords)[0], (*pTexCoords)[1]));
    }

    for (size_t triangleIdx = 0; triangleIdx < aimesh->mNumFaces; ++triangleIdx) {
        const aiFace& face = aimesh->mFaces[triangleIdx];
        mesh.addTriangle(face.mIndices[0], face.mIndices[1], face.mIndices[2]);
    }

    return mesh;
}

static Material buildMaterial(const aiMaterial* aimaterial, const embree::FileName& dir,
    bool loadTexture, AssimpSceneLoader& loader) {
    Material material;
    aiColor3D color;

    if (AI_SUCCESS == aimaterial->Get(AI_MATKEY_COLOR_DIFFUSE, color)) {
        material.setKd(Col3f(color.r, color.g, color.b));
    }
    else {
        material.setKd(embree::zero);
    }

    aiString path;

    if (loadTexture && (AI_SUCCESS == aimaterial->GetTexture(aiTextureType_DIFFUSE, 0, &path,
                                                             nullptr, nullptr, nullptr, nullptr, nullptr))) {
        material.setKdTexture(loader.loadTextureImage(dir.str() + "/" + path.data));
        //material.setKdTexture(TextureImage(embree::loadImage(dir.str() + "/" + path.data, true)));
    }

    if (AI_SUCCESS == aimaterial->Get(AI_MATKEY_COLOR_SPECULAR, color)) {
        material.setKs(Col3f(color.r, color.g, color.b));
    }
    else {
        material.setKs(embree::zero);
    }

    if (loadTexture && (AI_SUCCESS == aimaterial->GetTexture(aiTextureType_SPECULAR, 0, &path,
                                                             nullptr, nullptr, nullptr, nullptr, nullptr))) {
        material.setKdTexture(loader.loadTextureImage(dir.str() + "/" + path.data));
        //material.setKsTexture(TextureImage(embree::loadImage(dir.str() + "/" + path.data, true)));
    }

    float shininess;

    if (AI_SUCCESS == aimaterial->Get(AI_MATKEY_SHININESS, shininess)) {
        material.setShininess(shininess);
    }
    else {
        material.setShininess(0.f);
    }

    if (loadTexture && (AI_SUCCESS == aimaterial->GetTexture(aiTextureType_SHININESS, 0, &path,
                                                             nullptr, nullptr, nullptr, nullptr, nullptr))) {
        material.setShininessTexture(loader.loadTextureImage(dir.str() + "/" + path.data));
        //material.setShininessTexture(TextureImage(embree::loadImage(dir.str() + "/" + path.data, true)));
    }

    return material;
}

static void loadAssimpScene(const aiScene* aiscene, const embree::FileName& filepath, SceneGeometry& geometry, 
    Scene::MaterialContainer& materials, bool loadTextures, AssimpSceneLoader& loader) {
    size_t materialOffset = materials.size();

    for (size_t materialIdx = 0; materialIdx < aiscene->mNumMaterials; ++materialIdx) {
        Material material = buildMaterial(aiscene->mMaterials[materialIdx], filepath.path(), loadTextures, loader);
        materials.push_back(material);
    }

    for (size_t meshIdx = 0; meshIdx < aiscene->mNumMeshes; ++meshIdx) {
        geometry.addTriangleMesh(buildMesh(aiscene->mMeshes[meshIdx], materialOffset, aiscene->mNumMaterials));
    }
}

void AssimpSceneLoader::loadModel(const embree::FileName& filepath, SceneGeometry& geometry, 
        Scene::MaterialContainer& materials, bool loadTextures) {
    Assimp::Importer importer;
    embree::FileName completePath = m_BasePath + filepath;
    const aiScene* aiscene = importer.ReadFile(completePath.c_str(), aiProcess_Triangulate | aiProcess_GenNormals | aiProcess_FlipUVs);

    if (aiscene) {
        try {
            loadAssimpScene(aiscene, completePath, geometry, materials, loadTextures, *this);
        }
        catch (const std::runtime_error& e) {
            throw std::runtime_error("Assimp loading error on file " + filepath.str() + ": " + e.what());
        }
    }
    else {
        throw std::runtime_error("Assimp loading error on file " + filepath.str() + ": " + importer.GetErrorString());
    }
}

}
