#ifndef _BONEZ_LOADERS_ASSIMP_SCENE_LOADER_HPP_
#define _BONEZ_LOADERS_ASSIMP_SCENE_LOADER_HPP_

#include <unordered_map>
#include <embree/common/sys/filename.h>
#include "bonez/scene/Scene.hpp"

namespace BnZ {

class AssimpSceneLoader {
public:
    AssimpSceneLoader(const embree::FileName& basePath = ""):
        m_BasePath(basePath) {
    }

    /**
     * Load a model file with Assimp library and store it in scene.
     */
    void loadModel(const embree::FileName& filepath, SceneGeometry& geometry, 
        Scene::MaterialContainer& materials, bool loadTextures = true);
        
    TextureImage loadTextureImage(const embree::FileName& filepath);
private:
    typedef std::unordered_map<std::string, TextureImage> TextureCache;

    TextureCache m_TextureCache;
    embree::FileName m_BasePath;
};

}

#endif
