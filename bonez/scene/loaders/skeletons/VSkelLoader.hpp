#ifndef _BONEZ_LOADERS_SKELETONS_VSKEL_LOADER_HPP_
#define _BONEZ_LOADERS_SKELETONS_VSKEL_LOADER_HPP_

#include <embree/common/sys/filename.h>
#include "bonez/scene/VoxelSpace.hpp"

namespace BnZ {

class VSkelLoader {
public:
    VSkelLoader(const embree::FileName& basePath = ""):
        m_BasePath(basePath) {
    }

    std::unique_ptr<VoxelSpace> loadVoxelSpaceAndCurvSkel(const embree::FileName& filepath);
private:
    embree::FileName m_BasePath;
};

}

#endif
