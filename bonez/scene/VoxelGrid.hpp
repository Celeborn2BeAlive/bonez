#ifndef _BONEZ_VOXELGRID_HPP
#define _BONEZ_VOXELGRID_HPP

#include <vector>
#include <cstdint>
#include <embree/common/sys/filename.h>

#include "common/data/Grid3D.hpp"

namespace BnZ {

typedef Grid3D<uint8_t> VoxelGrid;

VoxelGrid loadVoxelGridFromPGM(const embree::FileName& filepath);

}

#endif // _BONEZ_VOXELGRID_HPP
