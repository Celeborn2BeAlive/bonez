//#include "Pink.hpp"

//#include <pink.h>
//#undef depth

//#include <pde_toolbox.h>

//namespace BnZ {

//static xvimage pinkView(VoxelGrid& grid) {
//    xvimage img;
//    img.name = nullptr;
//    img.row_size = grid.width();
//    img.col_size = grid.height();
//    img.depth_size = grid.depth();
//    img.data_storage_type = VFF_TYP_1_BYTE;
//    img.time_size = 1;
//    img.num_data_bands = 1;
//    img.xdim = img.ydim = img.zdim = 0.0;
//    img.xmin = img.ymin = img.zmin = 0;
//    img.xmax = img.ymax = img.zmax = 0;
//    img.image_data = grid.data();
//    return img;
//}

//static xvimage pinkView(Grid3D<uint32_t>& grid) {
//    xvimage img;
//    img.name = nullptr;
//    img.row_size = grid.width();
//    img.col_size = grid.height();
//    img.depth_size = grid.depth();
//    img.data_storage_type = VFF_TYP_4_BYTE;
//    img.time_size = 1;
//    img.num_data_bands = 1;
//    img.xdim = img.ydim = img.zdim = 0.0;
//    img.xmin = img.ymin = img.zmin = 0;
//    img.xmax = img.ymax = img.zmax = 0;
//    img.image_data = grid.data();
//    return img;
//}

//VoxelGrid Pink::openBall(const VoxelGrid& grid, int32_t radius, int32_t connexity) {
//    VoxelGrid result = grid;
//    xvimage img = pinkView(result);
//    if(!lopenball(&img, radius, connexity)) {
//        std::cerr << "lcloseball failed" << std::endl;
//    }

//    return result;
//}

//VoxelGrid Pink::closeBall(const VoxelGrid& grid, int32_t radius, int32_t connexity) {
//    VoxelGrid result = grid;
//    xvimage img = pinkView(result);
//    if(!lcloseball(&img, radius, connexity)) {
//        std::cerr << "lcloseball failed" << std::endl;
//    }
//    return result;
//}

//Grid3D<uint32_t> Pink::distanceGrid(const VoxelGrid& grid, int32_t connexity) {
//    VoxelGrid copy = grid;
//    xvimage img = pinkView(copy);
//    //linverse(&img);
//    Grid3D<uint32_t> result(grid.width(), grid.height(), grid.depth());
//    xvimage resultimg = pinkView(result);
//    ldist(&img, connexity, &resultimg);
//    return result;
//}

//Grid3D<uint32_t> Pink::fmm(const VoxelGrid& voxelGrid, Grid3D<uint32_t> seeds, Grid3D<float> metric) {
//    std::unique_ptr<INT4_TYPE[]> seed_in_buff(new INT4_TYPE[seeds.size()]);
//    std::copy(begin(seeds), end(seeds), seed_in_buff.get());

//    std::unique_ptr<INT4_TYPE[]> seed_out_buff(new INT4_TYPE[seeds.size()]);

//    int32_t dim_buf[] = { int32_t(seeds.width()), int32_t(seeds.height()), int32_t(seeds.depth()) };
//    int32_t dim_length = 3;

//    std::unique_ptr<DBL_TYPE[]> distance_buf(new DBL_TYPE[voxelGrid.size()]);

//    lfmmdist(seed_in_buff.get(), seed_out_buff.get(), dim_buf, dim_length, metric.data(),
//             1, float(embree::inf), distance_buf.get());

//    std::copy(seed_out_buff.get(), seed_out_buff.get() + seeds.size(), begin(seeds));
//    return seeds;
//}

////On effectue la fonction d'ouverture sur une ligne, vers la droite
//void dilate_one_line_right(struct xvimage* image, uint32_t num_ligne, uint32_t num_plan, uint32_t **tab_tri)
//{
//    int32_t max, i, maxcentre, pointeur_fin, pointeur_debut, rs, ps;

//    pointeur_fin=0;
//    pointeur_debut=0;

//    rs=rowsize(image);
//    ps=colsize(image)*rs;

//    max=0; maxcentre=0;

//    //On parcourt toute la ligne
//    for(i=0; i<rs; i++)
//    {
//            //Le carré que l'on dilate actuellement ne doit plus être dilaté
//            if(maxcentre+max == i)
//            {
//                //Si on a dans notre liste un atre carré à dilater, on le prend
//                if(pointeur_fin!=pointeur_debut)
//                {
//                    max=tab_tri[pointeur_debut][0];
//                    maxcentre=tab_tri[pointeur_debut][1];
//                    pointeur_debut++;
//                }
//                //Sinon, on dilate le carré où on est
//                else
//                {
//                    max=ULONGDATA(image)[num_plan*ps+num_ligne*rs+i];
//                    maxcentre=i;
//                }
//            }

//            //Est-ce que le carré où l'on se trouve mérite d'être dilaté plus tard ? On vide le tableau de liste des carres plus petits que lui
//            //Ici, on peut croire que l'algo n'est plus linéaire...
//            //Mais en tout, on ajoute au plus n element dans ce tableau, avec n=taille de ligne
//            //Ici, on ne peut donc qu'en retirer, en tout, que n au plus... donc, on reste linéaire
//            while(pointeur_debut!=pointeur_fin && ULONGDATA(image)[num_plan*ps+num_ligne*rs+i]>=tab_tri[pointeur_fin-1][0])
//            {
//                pointeur_fin--;
//            }

//            //S'il reste un carré plus grand, on vérifie que notre carré aura quand même une extension plus importante
//            if((pointeur_debut!=pointeur_fin) && ((tab_tri[pointeur_fin-1][0] + tab_tri[pointeur_fin-1][1]) < (ULONGDATA(image)[num_plan*ps+num_ligne*rs+i]+i)) )
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[num_plan*ps+num_ligne*rs+i];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//            //Il ne reste pas de carré plus grand... on vérifie que notre carré couvrira certains pixels
//            if((pointeur_debut==pointeur_fin) && (maxcentre+max < ULONGDATA(image)[num_plan*ps+num_ligne*rs+i]+i))
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[num_plan*ps+num_ligne*rs+i];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//        //Si le pixel où l'on est contient un carré plus grand que celui que l'on propage actuellement
//        if(ULONGDATA(image)[num_plan*ps+num_ligne*rs+i]>=max)
//        {
//            //On remplace le carré propagé par celui contenu dans le pixel où on est
//            max=ULONGDATA(image)[num_plan*ps+num_ligne*rs+i];
//            maxcentre=i;
//            pointeur_fin=pointeur_debut; //On oublie tous les max que l'on avait construit dans la liste tab_tri
//        }

//        ULONGDATA(image)[num_plan*ps+num_ligne*rs+i]=max;
//    }
//}



////On effectue la fonction d'ouverture sur une colonne, vers le bas
//void dilate_one_line_bottom(struct xvimage* image, uint32_t num_col, uint32_t num_plan, uint32_t **tab_tri)
//{
//    int32_t max, i, maxcentre, pointeur_fin, pointeur_debut, rs, ps, cs;

//    pointeur_fin=0;
//    pointeur_debut=0;

//    rs=rowsize(image);
//    cs=colsize(image);
//    ps=rs*cs;

//    max=0; maxcentre=0;

//    for(i=0; i<cs; i++)
//    {
//            if(maxcentre+max == i) //The current square should no more be dilated
//            {
//                if(pointeur_fin!=pointeur_debut) //We have another square to dilate
//                {
//                    max=tab_tri[pointeur_debut][0];
//                    maxcentre=tab_tri[pointeur_debut][1];
//                    pointeur_debut++;
//                }
//                else //We have no square to dilate, take current one
//                {
//                    max=ULONGDATA(image)[num_plan*ps+i*rs+num_col];
//                    maxcentre=i;
//                }
//            }

//            //Est-ce que le carré où l'on se trouve mérite d'être dilaté plus tard ? ON vide le tableau de liste des carres plus petits que lui
//            while(pointeur_debut!=pointeur_fin && ULONGDATA(image)[num_plan*ps+i*rs+num_col]>=tab_tri[pointeur_fin-1][0])
//            {
//                pointeur_fin--;
//            }

//            //S'il reste un carré plus grand, on vérifie que notre carré aura quand même une extension plus importante
//            if((pointeur_debut!=pointeur_fin) && ((tab_tri[pointeur_fin-1][0] + tab_tri[pointeur_fin-1][1]) < (ULONGDATA(image)[num_plan*ps+i*rs+num_col]+i)) )
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[num_plan*ps+i*rs+num_col];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//            if((pointeur_debut==pointeur_fin) && (maxcentre+max < ULONGDATA(image)[num_plan*ps+i*rs+num_col]+i))
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[num_plan*ps+i*rs+num_col];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//        if(ULONGDATA(image)[num_plan*ps+i*rs+num_col]>=max) //New square bigger to dilate
//        {
//            max=ULONGDATA(image)[num_plan*ps+i*rs+num_col];
//            maxcentre=i;
//            pointeur_fin=pointeur_debut; //On oublie tous les max que l'on avait construit
//        }

//        ULONGDATA(image)[num_plan*ps+i*rs+num_col]=max;
//    }
//}



////On effectue la fonction d'ouverture sur une profondeur, vers l'arriere
//void dilate_one_line_backward(struct xvimage* image, uint32_t num_col, uint32_t num_ligne, uint32_t **tab_tri)
//{
//    int32_t max, i, maxcentre, pointeur_fin, pointeur_debut, rs, ps, d;

//    pointeur_fin=0;
//    pointeur_debut=0;

//    rs=rowsize(image);
//    d=image->depth_size;
//    ps=rs*colsize(image);

//    max=0; maxcentre=0;

//    for(i=0; i<d; i++)
//    {
//            if(maxcentre+max == i) //The current square should no more be dilated
//            {
//                if(pointeur_fin!=pointeur_debut) //We have another square to dilate
//                {
//                    max=tab_tri[pointeur_debut][0];
//                    maxcentre=tab_tri[pointeur_debut][1];
//                    pointeur_debut++;
//                }
//                else //We have no square to dilate, take current one
//                {
//                    max=ULONGDATA(image)[i*ps+num_ligne*rs+num_col];
//                    maxcentre=i;
//                }
//            }

//            //Est-ce que le carré où l'on se trouve mérite d'être dilaté plus tard ? ON vide le tableau de liste des carres plus petits que lui
//            while(pointeur_debut!=pointeur_fin && ULONGDATA(image)[i*ps+num_ligne*rs+num_col]>=tab_tri[pointeur_fin-1][0])
//            {
//                pointeur_fin--;
//            }

//            //S'il reste un carré plus grand, on vérifie que notre carré aura quand même une extension plus importante
//            if((pointeur_debut!=pointeur_fin) && ((tab_tri[pointeur_fin-1][0] + tab_tri[pointeur_fin-1][1]) < (ULONGDATA(image)[i*ps+num_ligne*rs+num_col]+i)) )
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[i*ps+num_ligne*rs+num_col];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//            if((pointeur_debut==pointeur_fin) && (maxcentre+max < ULONGDATA(image)[i*ps+num_ligne*rs+num_col]+i))
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[i*ps+num_ligne*rs+num_col];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//        if(ULONGDATA(image)[i*ps+num_ligne*rs+num_col]>=max) //New square bigger to dilate
//        {
//            max=ULONGDATA(image)[i*ps+num_ligne*rs+num_col];
//            maxcentre=i;
//            pointeur_fin=pointeur_debut; //On oublie tous les max que l'on avait construit
//        }


//        ULONGDATA(image)[i*ps+num_ligne*rs+num_col]=max;
//    }
//}



////On effectue la fonction d'ouverture sur une ligne, vers la gauche
//void dilate_one_line_left(struct xvimage* image, uint32_t num_ligne, uint32_t num_plan, uint32_t **tab_tri)
//{
//    int32_t max, i, maxcentre, pointeur_fin, pointeur_debut, rs, ps;

//    pointeur_fin=0;
//    pointeur_debut=0;

//    rs=rowsize(image);
//    ps=colsize(image)*rs;

//    max=0; maxcentre=0;

//    for(i=rs-1; i>=0; i--)
//    {
//            if(maxcentre-max == i) //The current square should no more be dilated
//            {
//                if(pointeur_fin!=pointeur_debut) //We have another square to dilate
//                {
//                    max=tab_tri[pointeur_debut][0];
//                    maxcentre=tab_tri[pointeur_debut][1];
//                    pointeur_debut++;
//                }
//                else //We have no square to dilate, take current one
//                {
//                    max=ULONGDATA(image)[num_plan*ps+num_ligne*rs+i];
//                    maxcentre=i;
//                }
//            }

//            //Est-ce que le carré où l'on se trouve mérite d'être dilaté plus tard ? ON vide le tableau de liste des carres plus petits que lui
//            while(pointeur_debut!=pointeur_fin && ULONGDATA(image)[num_plan*ps+num_ligne*rs+i]>=tab_tri[pointeur_fin-1][0])
//            {
//                pointeur_fin--;
//            }

//            //S'il reste un carré plus grand, on vérifie que notre carré aura quand même une extension plus importante
//            if((pointeur_debut!=pointeur_fin) && ((tab_tri[pointeur_fin-1][1] - tab_tri[pointeur_fin-1][0]) > i-(ULONGDATA(image)[num_plan*ps+num_ligne*rs+i])) )
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[num_plan*ps+num_ligne*rs+i];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//            if((pointeur_debut==pointeur_fin) && (maxcentre-max > i-ULONGDATA(image)[num_plan*ps+num_ligne*rs+i]))
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[num_plan*ps+num_ligne*rs+i];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//        if(ULONGDATA(image)[num_plan*ps+num_ligne*rs+i]>=max) //New square bigger to dilate
//        {
//            max=ULONGDATA(image)[num_plan*ps+num_ligne*rs+i];
//            maxcentre=i;
//            pointeur_fin=pointeur_debut; //On oublie tous les max que l'on avait construit
//        }

//        ULONGDATA(image)[num_plan*ps+num_ligne*rs+i]=max;
//    }
//}



////On effectue la fonction d'ouverture sur une colonne, vers le haut
//void dilate_one_line_top(struct xvimage* image, uint32_t num_col, uint32_t num_plan, uint32_t **tab_tri)
//{
//    int32_t max, i, maxcentre, pointeur_fin, pointeur_debut, rs, ps, cs;

//    pointeur_fin=0;
//    pointeur_debut=0;

//    rs=rowsize(image);
//    cs=colsize(image);
//    ps=rs*cs;

//    max=0; maxcentre=0;

//    for(i=cs-1; i>=0; i--)
//    {
//            if(maxcentre-max == i) //The current square should no more be dilated
//            {
//                if(pointeur_fin!=pointeur_debut) //We have another square to dilate
//                {
//                    max=tab_tri[pointeur_debut][0];
//                    maxcentre=tab_tri[pointeur_debut][1];
//                    pointeur_debut++;
//                }
//                else //We have no square to dilate, take current one
//                {
//                    max=ULONGDATA(image)[num_plan*ps+i*rs+num_col];
//                    maxcentre=i;
//                }
//            }

//            //Est-ce que le carré où l'on se trouve mérite d'être dilaté plus tard ? ON vide le tableau de liste des carres plus petits que lui
//            while(pointeur_debut!=pointeur_fin && ULONGDATA(image)[num_plan*ps+i*rs+num_col]>=tab_tri[pointeur_fin-1][0])
//            {
//                pointeur_fin--;
//            }

//            //S'il reste un carré plus grand, on vérifie que notre carré aura quand même une extension plus importante
//            if((pointeur_debut!=pointeur_fin) && ((tab_tri[pointeur_fin-1][1] - tab_tri[pointeur_fin-1][0]) > i-(ULONGDATA(image)[num_plan*ps+i*rs+num_col])) )
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[num_plan*ps+i*rs+num_col];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//            if((pointeur_debut==pointeur_fin) && (maxcentre-max > i-ULONGDATA(image)[num_plan*ps+i*rs+num_col]))
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[num_plan*ps+i*rs+num_col];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//        if(ULONGDATA(image)[num_plan*ps+i*rs+num_col]>=max) //New square bigger to dilate
//        {
//            max=ULONGDATA(image)[num_plan*ps+i*rs+num_col];
//            maxcentre=i;
//            pointeur_fin=pointeur_debut; //On oublie tous les max que l'on avait construit
//        }

//        ULONGDATA(image)[num_plan*ps+i*rs+num_col]=max;
//    }
//}



////On effectue la fonction d'ouverture sur une profondeur, vers l'avant
//void dilate_one_line_forward(struct xvimage* image, uint32_t num_col, uint32_t num_ligne, uint32_t **tab_tri)
//{
//    int32_t max, i, maxcentre, pointeur_fin, pointeur_debut, rs, ps, d;

//    pointeur_fin=0;
//    pointeur_debut=0;

//    rs=rowsize(image);
//    d=image->depth_size;
//    ps=rs*colsize(image);

//    max=0; maxcentre=0;

//    for(i=d-1; i>=0; i--)
//    {
//            if(maxcentre-max == i) //The current square should no more be dilated
//            {
//                if(pointeur_fin!=pointeur_debut) //We have another square to dilate
//                {
//                    max=tab_tri[pointeur_debut][0];
//                    maxcentre=tab_tri[pointeur_debut][1];
//                    pointeur_debut++;
//                }
//                else //We have no square to dilate, take current one
//                {
//                    max=ULONGDATA(image)[i*ps+num_ligne*rs+num_col];
//                    maxcentre=i;
//                }
//            }

//            //Est-ce que le carré où l'on se trouve mérite d'être dilaté plus tard ? ON vide le tableau de liste des carres plus petits que lui
//            while(pointeur_debut!=pointeur_fin && ULONGDATA(image)[i*ps+num_ligne*rs+num_col]>=tab_tri[pointeur_fin-1][0])
//            {
//                pointeur_fin--;
//            }

//            //S'il reste un carré plus grand, on vérifie que notre carré aura quand même une extension plus importante
//            if((pointeur_debut!=pointeur_fin) && ((tab_tri[pointeur_fin-1][1] - tab_tri[pointeur_fin-1][0]) > i-(ULONGDATA(image)[i*ps+num_ligne*rs+num_col])) )
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[i*ps+num_ligne*rs+num_col];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//            if((pointeur_debut==pointeur_fin) && (maxcentre-max > i-ULONGDATA(image)[i*ps+num_ligne*rs+num_col]))
//            {
//                tab_tri[pointeur_fin][0]=ULONGDATA(image)[i*ps+num_ligne*rs+num_col];
//                tab_tri[pointeur_fin++][1]=i;
//            }

//        if(ULONGDATA(image)[i*ps+num_ligne*rs+num_col]>=max) //New square bigger to dilate
//        {
//            max=ULONGDATA(image)[i*ps+num_ligne*rs+num_col];
//            maxcentre=i;
//            pointeur_fin=pointeur_debut; //On oublie tous les max que l'on avait construit
//        }

//        ULONGDATA(image)[i*ps+num_ligne*rs+num_col]=max;
//    }
//}

//Grid3D<uint32_t> Pink::openingFunction(const VoxelGrid& grid, Grid3D<uint32_t> distanceMap) {
//    xvimage distmap = pinkView(distanceMap);

//    uint32_t rs, cs, d, i, j, k, n, **tab, N, l;

//    rs=rowsize(&distmap);
//    cs=colsize(&distmap);
//    d=distmap.depth_size;
//    N=d*cs*rs;


//    //Debut de l'algo


//    //Un tableau qui sera utlisé comme zone memoire temporaire par les algos...
//    //On le réalloue dès que l'on change de méthode de parcours (ligne, colonne, profondeur)
//    //On l'alloue pour le parcours des lignes
//    tab=new uint32_t*[rs];
//    for(i=0; i<rs; i++) {
//        tab[i]=new uint32_t[2];
//        tab[i][0] = 0;
//        tab[i][1] = 0;
//    }


//    //On parcourt les lignes... de gauche à droite, puis droite à gauche
//    for(k=0; k<d; k++)
//        for(j=0; j<cs; j++)
//        {
//            dilate_one_line_right(&distmap, j, k, tab);
//            dilate_one_line_left(&distmap, j, k, tab);
//        }

//    //On détruit le tableau
//    for(i=0; i<rs; i++)
//       delete [] tab[i];
//    delete [] tab;




//    //On réalloue le tableau pour les colonnes
//    tab=new uint32_t*[cs];
//    for(i=0; i<cs; i++) {
//        tab[i]=new uint32_t[2];
//        tab[i][0] = 0;
//        tab[i][1] = 0;
//    }

//    //On parcourt les colonnes, haut n bas, puis bas en haut
//    for(k=0; k<d; k++)
//        for(i=0; i<rs; i++)
//        {
//            dilate_one_line_bottom(&distmap, i, k, tab);
//            dilate_one_line_top(&distmap, i, k, tab);
//        }


//    for(i=0; i<cs; i++)
//       delete [] tab[i];
//    delete [] tab;


//    //If 3d
//    if(d>1)
//    {
//        //Alloue le tableau
//        tab=new uint32_t*[d];
//        for(i=0; i<d; i++) {
//            tab[i]=new uint32_t[2];
//            tab[i][0] = 0;
//            tab[i][1] = 0;
//        }

//        //Parcours des profondeurs, de avant vers arriere, puis inversement
//        for(j=0; j<cs; j++)
//            for(i=0; i<rs; i++)
//            {
//                dilate_one_line_backward(&distmap, i, j, tab);
//                dilate_one_line_forward(&distmap, i, j, tab);
//            }

//        for(i=0; i<d; i++)
//           delete [] tab[i];
//        delete [] tab;
//    }

//    return distanceMap;
//}

//}
