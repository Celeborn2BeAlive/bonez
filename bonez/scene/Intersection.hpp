#ifndef _BONEZ_SCENE_INTERSECTION_HPP_
#define _BONEZ_SCENE_INTERSECTION_HPP_

#include <ostream>
#include <cassert>
#include <embree/default.h>
#include "bonez/common/common.hpp"
#include "shading/Material.hpp"
#include "SurfacePoint.hpp"

namespace BnZ {

/**
 * An Intersection object is a SurfacePoint resulting from the intersection of
 * a given ray with a surface of the scene.
 */
struct Intersection: public SurfacePoint {
    Hit hit; // The hit computed by embree
    Col3f Le; // The radiance emitted by the point along the incident direction
    
    Intersection() {
    }
    
    Intersection(const SurfacePoint& point, const Hit& hit, const Col3f& Le):
        SurfacePoint(point), hit(hit), Le(Le) {
    }
    
    /**
     * Check if the intersection exists.
     */
    operator void*() const {
        return hit ? (void*) 1 : nullptr;
    }
};

inline std::ostream& operator <<(std::ostream& out, const Intersection& i) {
    if (!i) {
        out << "[Intersection undefined]";
    }
    else {
        out << "[Intersection p=" << i.P << " Ng=" << i.Ng << " Ns=" << i.Ns 
            << " texCoords=" << i.texCoords
            << " dist=" << i.hit.t << "]";
    }

    return out;
}

}

#endif
