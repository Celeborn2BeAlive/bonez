#ifndef _BONEZ_SURFACEPOINT_HPP_
#define _BONEZ_SURFACEPOINT_HPP_

#include "bonez/common/maths.hpp"
#include "bonez/common/common.hpp"

namespace BnZ {

class Light;
class Material;

struct SurfacePoint {
    Vec3f P;
    Vec3f Ns, Ng;
    Vec2f texCoords;
    
    const Material* material;
    const Light* light;
    
    SurfacePoint(): material(nullptr), light(nullptr) {
    }

    SurfacePoint(Vec3f P, Vec3f N):
        P(P), Ns(N), Ng(N), texCoords(0.f, 0.f), material(nullptr), light(nullptr) {
    }
    
    SurfacePoint(Vec3f P, Vec3f Ns, Vec3f Ng, Vec2f texCoords):
        P(P), Ns(Ns), Ng(Ng), texCoords(texCoords), material(nullptr), light(nullptr) {
    }
    
    SurfacePoint(Vec3f P, Vec3f Ns, Vec3f Ng, Vec2f texCoords, const Material* material, const Light* light):
        P(P), Ns(Ns), Ng(Ng), texCoords(texCoords), material(material), light(light) {
    }
};

typedef embree::Sample<SurfacePoint> SurfacePointSample;

__forceinline AffineSpace3f coordinateSystem(const SurfacePoint& point) {
    return coordinateSystem(point.P, point.Ns);
}

}

#endif
